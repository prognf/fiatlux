# Fiat Lux : a cellular automata simulation softwware

## Presentation

## Documentation

The documentation of this software can be found at <https://project.inria.fr/fiatlux/>.

## Libraries
Libraries used in this project :  
+ [Ranmar](www.honeylocust.com/RngPack/) for random numbers generation.
+ [Ptolemy II](https://ptolemy.berkeley.edu/) for the live plotting fonctionnalities
+ [Antlr](https://www.antlr.org/) for initialisation language.
 


test
