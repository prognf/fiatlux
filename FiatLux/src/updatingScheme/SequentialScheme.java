package updatingScheme;

import components.arrays.RegularDynArray;
import components.types.IntegerList;
import grafix.gfxTypes.elements.FLPanel;
import grafix.gfxTypes.elements.FLTextArea;
import grafix.gfxTypes.elements.FLTextField;
import topology.basics.SuperTopology;

/** for PDDL reading !!! **/
public class SequentialScheme extends SimpleArrayScheme {


	public final static String NAME= "Sequential";
	static final int COLWIDTH = 10;

	/*--------------------
	 * attributes
	 --------------------*/
	private int m_updateTime;
	private IntegerList m_sequence;
	int m_size;
	SequenceControlPanel m_controlSeq= new SequenceControlPanel();
	FLTextArea m_displaySeq= new FLTextArea(COLWIDTH);

	/*--------------------
	 * construction
	 --------------------*/

	/** */
	public SequentialScheme() {
		super();
	}

	@Override
	public void LinkTo(RegularDynArray in_automaton, SuperTopology in_topo){
		super.LinkTo(in_automaton, in_topo);
		m_size= in_topo.GetSize();
		m_sequence= IntegerList.NintegersFromZero(m_size);
	}




	/*--------------------
	 * Signals
	 * *************************************************************************/

	public void sig_NextStep() {
		int pos = m_sequence.Get(m_updateTime % m_sequence.GetSize());
		m_updateTime++;
		// Macro.DebugNOCR(" " + pos);
		PrepareTransitionCell(pos);
		MakeTransitionCell(pos);
	}


	public void sig_Init() {
		super.sig_Init();	
		m_updateTime=0;
	}

	/*-----------------------------
	 *  get set
	 *------------------------------*/

	public void SetUpdateSequence(IntegerList positionSequence) {
		m_sequence= positionSequence;
		m_updateTime=0;
		Update();
	}



	private void Update() {
		String seqstr=SequenceToCode();
		m_displaySeq.SetText(seqstr);
	}

	

	public FLPanel GetSpecificPanel() {
		FLPanel p = new FLPanel();
		p.AddLabel("Sequence:");
		p.Add(m_controlSeq,m_displaySeq);
		Update();
		return p;
	}

	@Override
	public String GetName() {
		return NAME;
	}

	/** from pos to char */
	private String SequenceToCode() {
		StringBuilder code= new StringBuilder();
		for (int val : m_sequence){
			if (val < 10){
				code.append("").append(val);
			} else if (val >= 10 + 26){
				code.append((char) (val - 10 + 65 + 6));
			} else {
				code.append((char) (val - 10 + 65));
			}			
		}
		return code.toString();
	}
	/** from char to pos (int) "0" -> 0 "A" -> 10 , "B-> 11", to Z **/
	private int FindPos(int ch) {
		if ((48 <= ch ) && (ch <58)){ //0-9
			return ch - 48;
		} else if (ch>=65+26){ // a-z 
			return ch + 10 - 65 - 6;
		} else {//A-Z
			return ch + 10 - 65; 
		}
		//return ch;
	}

	class SequenceControlPanel extends FLTextField {

		SequenceControlPanel(){
			super(COLWIDTH);
		}

		@Override
		public void parseInput(String input) {
			IntegerList l= new IntegerList();
			char [] charseq= input.toCharArray();
			for (char ch  : charseq){
				int val= FindPos(ch);
				//Macro.Debug( " %%%>> " + val );
				l.addVal(val);
			}
			SetUpdateSequence(l);
		}		
	}

	class SequenceDisplay extends FLTextArea {

		public SequenceDisplay(int in_columns) {
			super(in_columns);
		}

	}



}
