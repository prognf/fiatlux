package updatingScheme;

import java.util.ArrayList;

import components.allCells.AlphaUpdatable;
import components.arrays.RegularDynArray;
import main.Macro;
import models.CAmodels.chemical.MultiAmoebaeCell;
import topology.basics.SuperTopology;


/*--------------------
 * An UpdatingScheme which operates on arrays of AlphaUpdatable
 */
abstract public class SimpleArrayScheme extends UpdatingScheme {
	//--------------------------------------------------------------------------
	//- attributes
	//--------------------------------------------------------------------------
	protected ArrayList<AlphaUpdatable> m_array;
	
	//--------------------------------------------------------------------------
	//- construction
	//--------------------------------------------------------------------------

	/** use with LinkTo for construction !!! */
	public SimpleArrayScheme(){

	}

	/** override if needed **/
	public void LinkTo(RegularDynArray automaton, SuperTopology notused){
		super.SetSize(automaton);
		m_array= automaton.GetCastedArray();
	}

	//--------------------------------------------------------------------------
	//- signals
	//--------------------------------------------------------------------------
	
	
	@Override
	public void sig_Init() {
		Macro.PrintInitSignal(5, this);
		ResetArray(); // useful ??
		sig_ResetRandomizer();
	}

	
	@Override
	public void sig_Update() {
		for (AlphaUpdatable cell : m_array) {
			cell.sig_UpdateBuffer();
		}
	}

	/** this signal-method is invoked by the initializers */
	protected void ResetArray() {
		for (AlphaUpdatable cell : m_array) {
			cell.sig_Reset();
		}
	}

	//--------------------------------------------------------------------------
	//- procedures supplied for the two main steps 
	//--------------------------------------------------------------------------

	/** sends signal to cell "cell" */
	final public void MakeTransitionCell(int cell){
		//Macro.Debug("sending transition signal to cell: " + cell);
		GetCell(cell).sig_MakeTransition();
	}

	/** sends signal to cell "cell" */
	final public void PrepareTransitionCell(int cell){
		//Macro.Debug("sending update signal to cell: " + cell);
		GetCell(cell).sig_UpdateBuffer();
	}

	/** sends signal to cell "cell" */
	final public void ResetConflict(int cell){
		((MultiAmoebaeCell)GetCell(cell)).sig_ResetConflict();
	}

	//--------------------------------------------------------------------------
	//- get / set
	//--------------------------------------------------------------------------

	
	final protected ArrayList<AlphaUpdatable> GetArray(){
		return m_array;
	}
	
	/** use with care ! */
	protected final AlphaUpdatable GetCell(int pos) {
		return m_array.get(pos);
	}



}