package updatingScheme;

import components.allCells.OneRegisterIntCell;
import components.arrays.RegularDynArray;
import grafix.gfxTypes.elements.FLPanel;
import main.Macro;
import topology.basics.SuperTopology;

/** for updating in "triangle" : 
 * used by colored Traffic **
 * @author fates
 *
 */
public class TriangularScheme 
extends SimpleArrayScheme {

	final static double DEFAULT_SR = 1.0; // synchrony rate by default
	private static final String NAME = "TriangularScheme";
	private final int m_OutOfStateColorIndex;

	/*--------------------
	 * attributes
	 --------------------*/

	// each automaton has its own random generator 
	//private TimeReadable m_automaton;
	int m_time;


	/*--------------------
	 * Construction
	 --------------------*/

	/** OutOfStatecolorIndex : index of the color when a cell is not updated */
	public TriangularScheme(int OutOfStatecolorIndex) {
		super();
		Macro.print(2, "new " + NAME);
		m_OutOfStateColorIndex= OutOfStatecolorIndex;
	}


	public void LinkTo(RegularDynArray automaton, SuperTopology notused){
		super.LinkTo(automaton, notused);
		//m_automaton= automaton; // only for reading time
	}


	/*--------------------
	 * Get/Set 
	 --------------------*/
	public FLPanel GetSpecificPanel() {
		return new FLPanel();
	}



	/*--------------------
	 * signals
	 --------------------*/

	@Override
	public void sig_Init() {
		super.sig_Init();
		m_time=0;
	}

	@Override
	public void sig_NextStep() {
		int size= GetSize();
		// xI defines the zone of non-updated cells 
		int xI= m_time + 1;
		if (xI>=size){
			xI=size-1;
		}

		for (int pos = xI; pos <  size - xI; pos++) {
			sig_UpdateBufferCell(pos);
		}
		for (int pos = xI; pos < size - xI; pos++) {
			sig_MakeTransitionCell(pos);
		}
		for (int pos=0; pos < xI; pos++){
			castGetCell(pos).InitState(m_OutOfStateColorIndex);
			castGetCell(size - 1 - pos).InitState(m_OutOfStateColorIndex);
		}

	}

	/** sends transition signal to a cell */
	final public void sig_MakeTransitionCell(int pos){
		//Macro.Debug("sending transition signal to cell: " + cell);
		GetCell(pos).sig_MakeTransition();
	}

	/** sends update signal to a cell */
	private void sig_UpdateBufferCell(int pos) {
		GetCell(pos).sig_UpdateBuffer();
	}

	/** private call */
	final private OneRegisterIntCell castGetCell(int pos) {
		return (OneRegisterIntCell)super.GetCell(pos);
	}


	/*--------------------
	 * others
	 --------------------*/	

	@Override
	public String GetName() {
		return NAME;
	}


}
