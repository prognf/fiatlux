package updatingScheme;

import components.arrays.RegularDynArray;
import components.randomNumbers.StochasticSource;
import components.types.FLsignal;
import components.types.Monitor;
import grafix.gfxTypes.elements.FLPanel;
import main.Macro;
import topology.basics.SuperTopology;

/*--------------------
 * an object that is in charge of updating cells
 * @author Nazim Fates
*--------------------*/
abstract public class UpdatingScheme extends StochasticSource 
implements Monitor
{

	public abstract void sig_Init();
	public abstract void sig_NextStep();
	abstract public void sig_Update();
	
	/**
	 * Used to link the Automaton to the Topology
	 * @param in_automaton the automaton
	 * @param in_topo the topology
	 */
	abstract public void LinkTo(RegularDynArray in_automaton, SuperTopology in_topo);
	/**
	 * @return the controller of the updating scheme
	 */
	abstract public FLPanel GetSpecificPanel();
	/**
	 * @return the name of the updating scheme
	 */
	abstract public String GetName();

	//implements IUpdatingScheme /* this interface has the "abstract" part of the class */ {


	public UpdatingScheme() {
		super();
	}

	protected void sig_ResetRandomizer() {
		GetRandomizer().sig_ResetRandomizer(); 	
	}

	@Override
	public void ReceiveSignal(FLsignal sig){
		switch(sig){
		case init:
			sig_Init();
			break;
		case update:
			sig_Update();
			break;
		case nextStep:
			sig_NextStep();
			break;
		}
	}
	
	
	


	





	











	/**
	 * Integer Size for the automaton
	 */
	//TODO : set this private
	protected int m_Lsize;

	
	
	/*---------------------------------------------------------------------------
	  - get & set
	  --------------------------------------------------------------------------*/

	protected FLPanel GetSeedControl() {
		return GetRandomizer().GetSeedControl();
	}

	/**
	 * @return the number of cells
	 */
	protected int GetSize() {
		return m_Lsize;
	}
	
	protected void SetSize(RegularDynArray in_automaton) {
		m_Lsize= in_automaton.GetSize();
		if (m_Lsize==0){
			Macro.FatalError(" Scheme linked to empty automaton !");
		}		
	}

	
	}
