package updatingScheme;

import grafix.gfxTypes.elements.FLPanel;


public class FullyAsynchronousScheme extends SimpleArrayScheme {


	public final static String NAME="FullyAsynchronous";
	/*--------------------
	 * attributes
	 --------------------*/

	public enum UPDATEMODE {Nsteps,oneByone};
	public UPDATEMODE m_updateMode= UPDATEMODE.Nsteps;

	/*--------------------
	 * construction
	 --------------------*/



	/* use with care */
	public FullyAsynchronousScheme() {
		super();
	}

	/*--------------------
	 * Signals
	 --------------------*/
	/** makes N steps **/
	public void sig_NextStep() {
		int Nupdate=(m_updateMode==UPDATEMODE.Nsteps)?GetSize():1;//one or N steps ?
		for (int i= 0; i < Nupdate; i++) {
			int cell = RandomInt(GetSize());
			UpdateOneCell(cell);
		}
	}

	public void UpdateOneCell(int pos){
		PrepareTransitionCell(pos);
		MakeTransitionCell(pos);
	}

	/*--------------------
	 * Get/Set : dynamics method
	 --------------------*/

	@Override
	public String GetName() {
		return NAME;
	}

	public FLPanel GetSpecificPanel() {
		return FLPanel.NewPanel(this.GetSeedControl());
	}



}
