package updatingScheme;

import components.arrays.RegularDynArray;
import grafix.gfxTypes.elements.FLPanel;
import main.Macro;
import topology.basics.PlanarTopology;
import topology.basics.SuperTopology;
import updatingScheme.AlphaScheme.RateControl;


public class CheckerboardScheme extends SimpleArrayScheme {
	public static final String NAME = "CherckerboardScheme";

	final static double DEFAULT_SR = 1.0; // synchrony rate by default
	final static private String S_LABEL = "Synchrony rate (%)";

	/*--------------------
	 * attributes
	 --------------------*/

	// dynamics
	// a real number between 0 and 1
	int m_counter;
	int m_X, m_Y;

	private RateControl m_control;

	/*--------------------
	 * Construction
	 --------------------*/
	public CheckerboardScheme() {
		super();
		Macro.print(4, "new upd. sch.:" + NAME);		
	}

	/** taking into account the grid topology **/
	public void LinkTo(RegularDynArray automaton, SuperTopology topo){
		super.LinkTo(automaton, topo);
		if (topo instanceof PlanarTopology){
			PlanarTopology topo2D= (PlanarTopology)topo;
			m_X= topo2D.GetXsize();
			m_Y= topo2D.GetYsize();
		}
		//Macro.Debug(" CB US linked !");
	}

	/*--------------------
	 * others
	 --------------------*/

	//@Override
	public String GetName() {
		return NAME;
	}

	/*--------------------
	 * Get/Set : dynamics method
	 --------------------*/
	//@Override
	public FLPanel GetSpecificPanel() {

		/*m_control= new RateControl();
		FLPanel p = 
				FLPanel.NewPanel(m_control, GetSeedControl());*/ 
		return null;
	}

	
	

	/*--------------------
	 * signals
	 --------------------*/
	@Override
	public void sig_Init() {
		super.sig_Init();
		m_counter=0;
	}
	
	/** calculates the new states with (a)synchronous dynamics */
	/*
	 * Should be called by daughters to implement sig_NextStep
	 */
	@Override
	public void sig_NextStep() {

		int delta= m_counter%2;
		for (int x = 0; x < m_X; x++) {
			for (int y = 0; y < m_Y; y++) {
				if ((x+y)%2 == delta){
					sig_UpdateBufferCell(x,y);
				}
			}
		}
		for (int x = 0; x < m_X; x++) {
			for (int y = 0; y < m_Y; y++) {
				if ((x+y)%2 == delta){
					sig_MakeTransitionCell(x,y);
				}
			}
		}
		m_counter++;

	} 

	private void sig_UpdateBufferCell(int x, int y) {
		int pos= x + y * m_X;
		PrepareTransitionCell(pos);
	}
	
	private void sig_MakeTransitionCell(int x, int y) {
		int pos= x + y * m_X;
		MakeTransitionCell(pos);
	}

	/* **********************************************************
	 *	 GFX
	 **********************************************************/


	



}
