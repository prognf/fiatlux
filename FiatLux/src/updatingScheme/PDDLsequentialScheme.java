package updatingScheme;

import components.arrays.RegularDynArray;
import components.types.IntegerList;
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.PddlViewer;
import topology.basics.PlanarTopology;
import topology.basics.SuperTopology;

/** for PDDL reading !!! **/
public class PDDLsequentialScheme extends SimpleArrayScheme {


	final static String NAME= "PDDLsequential";
	// for PDDL
	public static final String 
		FILENAMEINIT="INITIAL.txt", 
		FILENAME_LOAD="CELL-UPDATES.txt", 
		FILENAMERULE="RULE.txt";

	
	/*--------------------
	 * attributes
	 --------------------*/
	private int m_updatePos;
	private IntegerList m_sequence;
	PlanarTopology m_topo2d;
	
	PddlViewer m_viewer; // call-back, for highlighting the updating cell
	
	/*--------------------
	 * construction
	 --------------------*/

	/** use with LinkTo */
	public PDDLsequentialScheme(PddlViewer in_viewer) {
		super();
		m_viewer= in_viewer;
	}

	@Override
	public void LinkTo(RegularDynArray in_automaton, SuperTopology in_topo){
		super.LinkTo(in_automaton, in_topo);
		m_topo2d= (PlanarTopology) in_topo;
	}

	
	
	
	/*--------------------
	 * Signals
	 * *************************************************************************/
	
	public void sig_NextStep() {
		int pos = m_sequence.Get(m_updatePos);
		//Macro.DebugNOCR(" " + pos);
		PrepareTransitionCell(pos);
		MakeTransitionCell(pos);
		if (m_updatePos < m_sequence.GetSize() - 1){
			m_updatePos++;
			UpdateViewer();
		} else {
			m_viewer.SetHighLightOff();
		}
		
	}
	
	private void UpdateViewer() {
		int pos= m_sequence.Get(m_updatePos);
		if (m_viewer != null){
			m_viewer.SetPosHighLight(pos);
		}		
	}

	
	/*-----------------------------
	 *  get set
	 *------------------------------*/
	
	public void SetUpdateSequence(IntegerList positionSequence) {
		m_sequence= positionSequence;
		m_updatePos=0;
		UpdateViewer();
	}
	
	

	public FLPanel GetSpecificPanel() {
		return null;
	}
	
	@Override
	public String GetName() {
		return NAME;
	}

	
}
