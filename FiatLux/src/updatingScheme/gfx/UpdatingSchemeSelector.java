package updatingScheme.gfx;

import grafix.gfxTypes.elements.FLList;
import grafix.gfxTypes.elements.FLPanel;
import main.Macro;
import updatingScheme.AlphaScheme;
import updatingScheme.CheckerboardScheme;
import updatingScheme.FullyAsynchronousScheme;
import updatingScheme.SequentialScheme;
import updatingScheme.UpdatingScheme;

public class UpdatingSchemeSelector extends FLPanel {

	
	private static final String DEFAULT = "Default";
	
	private static final String[] UPDSCHLIST = 
		{DEFAULT, AlphaScheme.NAME, FullyAsynchronousScheme.NAME, SequentialScheme.NAME, CheckerboardScheme.NAME};

	
	FLList mF_list = new FLList(UPDSCHLIST);
	
	public UpdatingSchemeSelector(){
		
		//FLPanel LabelAndList= new FLPanel();
		AssociateLabelAndList("Updating scheme", mF_list);
		
	}

	public String GetSelectedUSname() {
		return mF_list.GetSelectedItemName();
	}
	
	
	/** may return null for default upd. sch. **/
	static public UpdatingScheme GetUpdatingSchemeFromName(String name){
		if ((name == null) || (name.equals(DEFAULT)) ){
			return null;
		} else if (name.equals(FullyAsynchronousScheme.NAME)){
			return new FullyAsynchronousScheme();
		} else if (name.equals(AlphaScheme.NAME)){
			return new AlphaScheme();
		}  else if (name.equals(SequentialScheme.NAME)){
			return new SequentialScheme();
		}  else if (name.equals(CheckerboardScheme.NAME)){
			return new CheckerboardScheme();
		}  else{
			Macro.SystemWarning("Updating scheme not found" + name );
			return null;
		}
	}
	
	
}
