package updatingScheme;

import components.types.IntegerList;
import grafix.gfxTypes.controllers.DoubleController;
import grafix.gfxTypes.elements.FLPanel;
import main.FiatLuxProperties;
import main.Macro;
import main.MathMacro;
import models.CAmodels.chemical.MultiAmoebaeCell;


public class AlphaScheme
extends SimpleArrayScheme {

	public static final String NAME = "AlphaScheme";

	final static double DEFAULT_SR = 1.0; // synchrony rate by default
	final static private String S_LABEL = "Synchrony rate (%)";

	/*--------------------
	 * attributes
	 --------------------*/

	// dynamics
	// a real number between 0 and 1
	double m_SynchronyRate;

	private RateControl m_control;

	/*--------------------
	 * Construction
	 --------------------*/
	public AlphaScheme() {
		super();
		Macro.print(4, "new Alpha Updating scheme");
		SetSynchronyRate(DEFAULT_SR); // synchrony by default
	}

	@Override
	public String GetName() {
		return NAME;
	}

	/*--------------------
	 * Get/Set : dynamics method
	 --------------------*/
	
	@Override
	public FLPanel GetSpecificPanel() {
		m_control = new RateControl();
		FLPanel p = FLPanel.NewPanelVertical(m_control, GetSeedControl());
		p.setOpaque(false);
		return p;
	}

	/* reads mode parameter */
	public double GetSynchronyRate() {
		return m_SynchronyRate;
	}

	/** update dynamics parameter */
	public void SetSynchronyRate(final double alpha) {
		Macro.print(4, "Synchrony rate set to : " + alpha);
		if ((alpha<0.) || (alpha>1.)){
			Macro.SystemWarning(" value of alpha out of range :" + alpha);
		}
		m_SynchronyRate = alpha;
		if (m_control!=null){
			m_control.Update();
		}
	}

	
	/*--------------------
	 * signals
	 --------------------*/

	/** calculates the new states with (a)synchronous dynamics 
	 * Should be called by daughters to implement sig_NextStep
	 */
	@Override
	public void sig_NextStep() {
		 int n= GetSize();
		if (Macro.TRACESIGNAL){
			Macro.print("Updating with alpha : " + m_SynchronyRate);
			//Macro.SystemWarning("ALPHA");
		}
		if (n <= 0) {
			Macro.FatalError(" Size associated to the scheme is 0 ! Check previous use of LinkTo ");
		}
		/* *** dynamics choice **** */
		if (m_SynchronyRate >= 1.0) {
			// Macro.Debug("synchronous UPD");
			// *** synchronous updating ****
			for (int cell = 0; cell < n; cell++) {
				PrepareTransitionCell(cell);
			}
			/*int blue = 0 ;
			int red = 0;
			int tot=0;*/
			for (int cell = 0; cell < n; cell++) {
				//TODO : //OC

				/*if(GetCell(cell) instanceof MultiAmoebaeCell){
					tot+=((MultiAmoebaeCell) GetCell(cell)).GetStateI(0);
					if(((MultiAmoebaeCell) GetCell(cell)).GetStateI(0)>0){
						//System.out.println("Cell : "+((MultiAmoebaeCell) GetCell(cell)).m_cellID);
					}

					if (((MultiAmoebaeCell) GetCell(cell)).GetStateI(1)==0 && ((MultiAmoebaeCell) GetCell(cell)).GetStateI(0)>0){
						blue+=((MultiAmoebaeCell) GetCell(cell)).GetStateI(0);
					}
					else if (((MultiAmoebaeCell) GetCell(cell)).GetStateI(1)==1 && ((MultiAmoebaeCell) GetCell(cell)).GetStateI(0)>0){
						red+=((MultiAmoebaeCell) GetCell(cell)).GetStateI(0);
					}
				}*/
				MakeTransitionCell(cell);

			}
			if(GetCell(0) instanceof MultiAmoebaeCell){
				for (int cell = 0; cell < n; cell++) {
					ResetConflict(cell);
				}
			}


			//Macro.print("Blue cells : "+blue+"   Red cells : "+red + "   Total : "+tot);

		} else {
			if (m_SynchronyRate <= 0.0) {
				// *** sequential updating with n random transitions ****
				if (m_SynchronyRate == 0.0) {
					//String updates="updates:[";
					for (int i = 0; i < n; i++) {
						int cell = RandomInt(n);
						PrepareTransitionCell(cell);
						MakeTransitionCell(cell);
						//String strCell= String.format("%02d", cell+1);
						//updates+= strCell + ":";
					}
					//updates+="]";
					//Macro.print(updates);
				} else {
					// Macro.FatalError(" Asynchrony rate is negative !");
					/* *** 1 site is updated **** */
					int cell = RandomInt(n);
					PrepareTransitionCell(cell);
					MakeTransitionCell(cell);
				}

			} else {
				// *** stochastic parallel updating ****
				// Macro.Debug("alpha updating WITH SIZE " + GetSize());
				IntegerList l = new IntegerList();
				for (int cell = 0; cell < GetSize(); cell++) {
					if (RandomEventDouble(m_SynchronyRate)) {
						l.addVal(cell);
						PrepareTransitionCell(cell);                    
					}
				}
				for (int cell : l) {
					MakeTransitionCell(cell);
				}
			}

		}

	}

	/*--------------------
	 *	 GFX
	 **********************************************************/
	class RateControl extends DoubleController {
		public RateControl() {
			super(S_LABEL, DoubleController.CONVERSIONRATE.PERCENT);
			double alpha = FiatLuxProperties.DEF_SYNCHRATE / MathMacro.HUNDRED;
			SetSynchronyRate(alpha);
			Update();
		}

		@Override
		public double ReadDouble() {
			return GetSynchronyRate();
		}

		@Override
		public void SetDouble(final double val) {
			SetSynchronyRate(val);
		}
	}
}