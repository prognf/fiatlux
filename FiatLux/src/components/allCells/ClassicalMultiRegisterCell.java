package components.allCells;

import models.CAmodels.ClassicalMultiRegisterModel;

/* import java.awt.*;
 import javax.swing.*;
 */

/*--------------------
 * classical CA cell : it can only read neighbours state
 * @author Nazim Fates
*--------------------*/
public class ClassicalMultiRegisterCell extends MultiRegisterCell {

	ClassicalMultiRegisterModel m_ClassicalModel;
	
	public ClassicalMultiRegisterCell(ClassicalMultiRegisterModel in_Model) {
		super(in_Model.GetnLayers());
		m_ClassicalModel = in_Model;
	}

	public void sig_UpdateBuffer() {
		int [] newstate= m_ClassicalModel.GetTransitionResult(this); 
		for(int channel = 0; channel < GetLayersNumber(); channel++) {
			SetBufferI(channel, newstate[channel]);
		}
	}

	@Override
	public void sig_MakeTransition() {
		for(int channel = 0; channel < GetLayersNumber(); channel++) {
			SetStateI(channel, GetBufferI(channel));
			//SetBufferI(channel, 0);
		}
	}

	@Override
	public void sig_Reset() {
		for(int channel = 0; channel < GetLayersNumber(); channel++) {
			SetStateI(channel, 0);
			SetBufferI(channel, 0);
		}		
	}
	
	
}
