package components.allCells;

import components.types.IntegerList;
import main.Macro;


/**
 ******************************************************* 
 * a cell that receives 
 * increment / decrement signals (delta)
 * and updates adding or substracting an integer  
 * 
 * @author Nazim Fates 
 * *****************************************************
 */
public abstract class InflReacCell extends OneRegisterIntCell {
	
	protected IntegerList m_signal= new IntegerList(); // list of signals received
	
	/* is the cell stable */
	final public boolean IsStable(){
		Macro.FatalError("Not implemented");
		return false;
	}
	
	/*--------------------
	 * signal methods
	 --------------------*/
	
	/* send signal value to an int */
	final public void SendSignal(int signal) {
		m_signal.addVal(signal);
	}
	
	final protected void ResetSignals(){
		m_signal= new IntegerList();
	}
		
	/*--------------------
	 * protected methods
	 --------------------*/
	
	/* cast method */
	public static InflReacCell[] CellToReceptor(Cell[] in_NeighbArray) {
			InflReacCell[] out_array;
			//Macro.print(4, "castarray operation");
			out_array = new InflReacCell[in_NeighbArray.length];
			for (int pos = 0; pos < in_NeighbArray.length; pos++) {
				out_array[pos] = (InflReacCell) in_NeighbArray[pos];
			}
			return out_array;
	}
	
	
	
	
}
