package components.allCells;

import main.Macro;


/**
 ******************************************************* 
 * branch of OneRegisterCells for transfer of particles 
 * 
 * @author Nazim Fates 
 * *****************************************************
 */
public class ReceptorCell extends OneRegisterIntCell {
	
	/* is the cell stable */
	final public boolean IsStable(){
		Macro.FatalError("Not implemented");
		return false;
	}

	@Override
	public void sig_UpdateBuffer() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void sig_MakeTransition() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void sig_Reset() {
		InitState(0);
		SetBufferState(0);
	}
	
		
}
