package components.allCells;

import main.Macro;
import models.CAmodelsvar.Margolus.MargolusCell;

/* 
 * A tessella is a set of cells used for Partitioned CA 
 * */
abstract public class Tessella implements AlphaUpdatable {

	/*--------------------
	 * Attributes
	 --------------------*/

	private MargolusCell[] m_CellArray;
	private int    m_size;

	/*--------------------
	 * Construction
	 --------------------*/

	public Tessella(MargolusCell[] in_CellArray) {
		//Macro.Debug("T:"+ in_CellArray.length);
		if (in_CellArray.length == 0){			
			Macro.FatalError(" empty init array");
		}
		if (in_CellArray[0]==null){
			Macro.FatalError("array contains null cell !");
		}
		m_CellArray = in_CellArray;
		m_size      = m_CellArray.length;
	}
	
	/*--------------------
	 * others
	 --------------------*/
	
	/**
	 * for noise override if needed
	 */
	public void ShiftStateOnClick(){
		
	}

    /*--------------------
     * Get & Set methods
     --------------------*/
	public MargolusCell GetCell(int cell){
		return m_CellArray[cell];
	}
	
	public int GetSize(){
		return m_size;
	}

	/*--------------------
	 * Signals
	 --------------------*/

	public void sig_Reset() {
		for(MargolusCell cell:m_CellArray){
			cell.sig_Reset();
		}
	}


}
