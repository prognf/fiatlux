package components.allCells; 

import components.arrays.GenericCellArray;
import main.Macro;



//import Ressources.*;

/***********************************************
 * The Cell is a neighboured cell.<br>
 * it has two signals : Reset, UpdateBuffer, MakeTransition
 ***********************************************/ 
public abstract class Cell extends SuperCell
	implements AlphaUpdatable {
	/*--------------------
	 * abstract
	 --------------------*/
	
	private static int CELLIDCOUNTER = 0;
	
	public final int m_cellID = ++CELLIDCOUNTER;
	
	abstract public void SetNeighbourhood(GenericCellArray<SuperCell> neighblist);
	
	abstract public int GetNeighbourhoodSize();
	
	abstract public Cell GetNeighbour(int in_NeighbourPosition);
	
	// state of a cell
	abstract public String GetInfo();
	
	
	/*--------------------
	 * Constructor
	 --------------------*/
	
	/*--------------------
	 * others
	 --------------------*/
	
	/**
	 * for noise override if needed
	 */
	public void ShiftStateOnClick(){}

	
	/*--------------------
	 * Get / Set
	 --------------------*/
	
	boolean isGhost(){
		return false;
	}
	
	
	/*--------------------
	 * testing methods
	 --------------------*/
	final public void print(){
		Macro.print(GetInfo());
	}

}//class 

