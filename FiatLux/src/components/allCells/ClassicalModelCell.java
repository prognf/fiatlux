package components.allCells;

import models.CAmodels.ClassicalModel;

abstract public class ClassicalModelCell extends OneRegisterIntCell  {
	
	final ClassicalModel m_ClassicalModel;

	public ClassicalModelCell(ClassicalModel model) {
		m_ClassicalModel = model;
	}

	final static int ZERO = 0;

	public void sig_Reset() {
		InitState(ZERO);
	}
}
