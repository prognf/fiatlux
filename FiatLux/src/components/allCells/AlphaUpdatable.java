package components.allCells;

public interface AlphaUpdatable {
	
	/**
	 * 
	 */
	void sig_Reset();

	/**
	 * synchronizing cell with its neighbourhood
	 */
	void sig_UpdateBuffer();

	/**
	 * applying local rule 
	 */
	void sig_MakeTransition();
	
	/**
	 * How the cell state changes when clicked on the viewer
	 */
	void ShiftStateOnClick();

}
