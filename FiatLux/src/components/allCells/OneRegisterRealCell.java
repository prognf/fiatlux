package components.allCells;

import main.Macro;

/*--------------------
 * A OneRegisterCell is a cell with one register  
 * @author Nazim Fates
*--------------------*/
public abstract class OneRegisterRealCell extends Cell {

	float m_State;
	float m_buffer;
	
	/* acess to the state of a cell */
	final public float GetState() {
		return m_State;
	}

	/* sets directly the state of a cell */
	final public void SetState(float state) {
		m_State = state;
	}
	
	/* sets the next state of a cell */
	final protected void SetBufferState(float state) {
		m_buffer = state;
	}
	
	/* sets the next state of a cell */
	final protected float  GetBufferState() {
		return m_buffer;
	}

	/* cast method */
	public static OneRegisterRealCell[] CellToOneRegisterReal(Cell[] in_Array) {
			OneRegisterRealCell[] out_array;
			Macro.print(4, "castarray operation");
			out_array = new OneRegisterRealCell[in_Array.length];
			for (int pos = 0; pos < in_Array.length; pos++) {
				out_array[pos] = (OneRegisterRealCell) in_Array[pos];
			}
			return out_array;
	}
	
	@Override
	public String GetInfo() {
		return "cell state :" + GetState() + " Neighb size : "  + GetNeighbourhoodSize();		
	}	



}// class

