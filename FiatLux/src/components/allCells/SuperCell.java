package components.allCells; 

/***********************************************
 ***********************************************/ 
public abstract class SuperCell {

	abstract public void sig_Reset();
	
	/**
	 * Override this to allow saving of the automaton state.
	 * @return a string representing the state of the cell.<br>
	 */
	public String ToSimpleString(){
		return "not implemented";
	}
	
}

