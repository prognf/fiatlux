package components.allCells;

import components.arrays.GenericCellArray;
import main.Macro;


/*--------------------
 * A OneRegisterCell is a cell with one register  
 * @author Nazim Fates
*--------------------*/
public abstract class OneRegisterIntCell extends Cell {

	/** is the cell stable ? */
	abstract public boolean IsStable();
	
	private GenericCellArray<OneRegisterIntCell> m_neighb;
	protected int m_state;
	protected int m_buffer;
		
	public OneRegisterIntCell() {
		super();
	}
	
	/** access to the state of a cell */
	public int GetState() {
		return m_state;
	}
	
	/* sets directly the state of a cell 
	 * used by initialising procedures ONLY */
	public void InitState(int in_State) {
		m_state = in_State;
	}
	
	/** sets the next state of a cell */
	final protected void SetBufferState(int in_State) {
		m_buffer = in_State;
	}
	
	/** sets the next state of a cell */
	final protected int GetBufferState() {
		return m_buffer;
	}

	/** @return the array of all neighbouring states */
	final public int [] GetNeighbourhoodState(){
		int size= GetNeighbourhoodSize();
		int [] neighb_state= new int[size];
		for (int pos=0; pos < size; pos++){
			neighb_state[pos]= ReadNeighbourState(pos);
		}
		return neighb_state;
	}
	
	/*--------------------
	 * others
	 --------------------*/
	
	@Override
	public String ToSimpleString(){
		return "" + m_state;
	}
	
	@Override
	public String GetInfo() {
		return "st:" + GetState() + " neighb. sz.: "  + GetNeighbourhoodSize();		
	}
	
	/*--------------------
	 * NEIGHBOURHOOD methods
	 --------------------*/
	

	/** implicit cast */
	final public void SetNeighbourhood(GenericCellArray<SuperCell> in_NeighbArray) {
		m_neighb = new GenericCellArray<OneRegisterIntCell>(in_NeighbArray);
		//Macro.Debug(" set neighb size "+ m_neighb.GetSize());
	}

	/**
	 * @return the size of the neighbourhood
	 */
	final public int GetNeighbourhoodSize() {
		return m_neighb.size();
	}
	
	/**
	 * @return the i-th neighbour as OneRegisterIntCell
	 */
	public OneRegisterIntCell GetNeighbour(int in_NeighbourPosition) {
		return m_neighb.get(in_NeighbourPosition);
	}
	
	/**
	 * @return the state of the i-th neighbour
	 */
	public int ReadNeighbourState(int in_NeighbourPosition) {
		try{
			return GetNeighbour(in_NeighbourPosition).GetState();
		} catch (IndexOutOfBoundsException exc){
			Macro.FatalError(exc, 
					"neighb. size:" + m_neighb.size() + "  index:" + in_NeighbourPosition);
			Macro.print("This may be caused by irregular (non-torical) boundary conditions" );
			return Macro.ERRORint;
		}
	}

	/**
	 * (not optimal if used several times in one local function)
	 * @return the number of occurrences of state in_state in the neighbourhood
	 */
	final public int CountOccurenceOf(int in_State) {
		int count = 0;
		for (OneRegisterIntCell cell: m_neighb) { 
			if (cell.GetState() == in_State)
				count++;
		}
		return count;
	}

	
	/**
	 * @return sum of states in the neighb.
	 */
	final public int GetSum() {
		int sum = 0;
		for (OneRegisterIntCell cell: m_neighb) {
			sum+= cell.GetState();
		}
		return sum;
	}
	
	/** returns the decimal number corresponding to 
	 * the conversion of binary states of the neighbourhood 
	 * **/ 
	public int GetNeighbourhoodStateCode() {
	//	int neighbSize= GetNeighbourhoodSize();
		int sum=0;
		for (OneRegisterIntCell cell: m_neighb){
			sum *= 2 ;
			sum += cell.GetState();
		}
		
		return sum;
	}
	/**
	 * @return the highest value of state in the neighbourhood
	 * that is strictly lower than UpperLimit
	 * (supposed non-empty)
	 */
	final public int GetMaxNeighbState(int UpperLimit) {
		int maxval = -1;
		for (OneRegisterIntCell cell: m_neighb){
			int current= cell.GetState(); 
			if ( (current < UpperLimit) && ( current > maxval) ){
				maxval= current;
			}
		}
		return maxval;
	}

	/**
	 * Use with caution!
	 * @param state
	 */
	public void SetState(int state) {
		m_state = state;
	}

	public static void IncrementDelta(OneRegisterIntCell targetNeighb) {
		targetNeighb.m_buffer++;
	}
	
	public static void DecrementDelta(OneRegisterIntCell targetNeighb) {
		targetNeighb.m_buffer--;
	}
	
	public static void GiveOneParticle(OneRegisterIntCell source, OneRegisterIntCell target) {
		source.m_buffer--;
		target.m_buffer++;
	}
	

}// class

