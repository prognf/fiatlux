package components.allCells;

import models.CAmodels.ClassicalModel;

/* import java.awt.*;
 import javax.swing.*;
 */

/*--------------------
 * classical CA cell : it can only read neighbours state
 * @author Nazim Fates
*--------------------*/
public class ClassicalCACell extends ClassicalModelCell {

	public ClassicalCACell(ClassicalModel in_Model) {
		super(in_Model);
	}

	public void sig_UpdateBuffer() {
		SetBufferState(m_ClassicalModel.ApplyLocalFunction(this));
	}
	
	/* current state <- buffer state */
	final public void sig_MakeTransition() {
		m_state= m_buffer;
	}

	/*--------------------
	 * get/set methods
	 --------------------*/
	/* is the cell stable */
	final public boolean IsStable(){
		// recomputes the next state and compares it to current state
		this.sig_UpdateBuffer();
		return (m_state == m_buffer);
	}

	/**
	 * for noise override if needed
	 */
	public void ShiftStateOnClick(){
		m_state= 1 - m_state;
	}
}