package components.allCells;


/*--------------------
 * A TwoRegisterCell is a cell with two registers  
 * @author Nazim Fates
*--------------------*/
public abstract class TwoRegisterCell extends Cell {

	protected int m_StateOne,  m_StateTwo;
	protected int m_bufferOne, m_bufferTwo;
	
	/* use this constructor if the cell is simple */
	public TwoRegisterCell() {
	//	super();
	}

	/* acess to the state of a cell */
	final public int GetStateOne() {
		return m_StateOne;
	}
	
	/* acess to the state of a cell */
	final public int GetStateTwo() {
		return m_StateTwo;
	}

	/* sets directly the state of a cell */
	// TODO REMOVE THIS ?
	final public void SetStateOne(int in_State) {
		m_StateOne = in_State;
	}
	
	/* sets directly the state of a cell */
	// TODO REMOVE THIS ?
	final public void SetStateTwo(int in_State) {
		m_StateTwo = in_State;
	}
	
	/* sets the next state of a cell */
	final public void SetBufferOne(int in_State) {
		m_bufferOne = in_State;
	}
	
	/* sets the next state of a cell */
	final public void SetBufferTwo(int in_State) {
		m_bufferTwo = in_State;
	}
	
	/* adding to buffer one */
	final public void AddToBufferOne(int in_difference){
		m_bufferOne += in_difference;
	}
	
	/* adding to buffer one */
	final public void AddToBufferTwo(int in_difference){
		m_bufferTwo += in_difference;
	}
		
	
	/* overridable */
	public void sig_Reset() {
		SetStateOne(0); SetStateTwo(0);
		SetBufferOne(0); SetBufferTwo(0);
	}
	
	/* cast method */
	public static TwoRegisterCell[] CellToTwoRegister(Cell[] in_NeighbArray) {
			TwoRegisterCell[] out_array;
			//Macro.print(4, "2R cast operation");
			out_array = new TwoRegisterCell[in_NeighbArray.length];
			for (int pos = 0; pos < in_NeighbArray.length; pos++) {
				out_array[pos] = (TwoRegisterCell) in_NeighbArray[pos];
			}
			return out_array;
	}
	
	
	@Override
	public String GetInfo() {
		return 
		"cell state 1 :" + GetStateOne() +
		" cell state 2 :" + GetStateTwo() +
		" Neighb size : "  + GetNeighbourhoodSize();		
	}
}// class

