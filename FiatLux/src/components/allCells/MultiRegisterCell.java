package components.allCells;

import components.arrays.GenericCellArray;

/*--------------------
 * cell with n registers 
 * @author Nazim Fates
*--------------------*/
public abstract class MultiRegisterCell extends Cell {
	
	final static int REGONE=0, REGTWO=1;
	
	private 	int[] 	m_State;
	protected 	int[] 	m_buffer; //this so LGCAcell can change buffer directly
	private 	int		m_nregister;
	
	
	/**  constructor */
	public MultiRegisterCell(int in_size) {
		m_nregister   = in_size;
		m_State  = new int[m_nregister];
		m_buffer = new int[m_nregister];
	}
	
	/*--------------------------------------------------------------------------
	 * GETTERS/SETTERS
	 -------------------------------------------------------------------------*/
	
	/**
	 * @return the number of registers
	 */
	final public int GetLayersNumber(){
		return m_nregister;
	}
	
	/** sets directly the state of a cell */
	final public void SetStateI(int register, int in_State) {
		m_State[register] = in_State;
	}
	
	/** access to the state of a cell */
	final public int GetStateI(int register) {
		return m_State[register];
	}

	/** access to all the states of the cells */
	final public int[] GetStateArray() {
		return m_State;
	}
	
	/** sums cell states (for LGCA) */
	final public int GetSumState() {
		int sum = 0;
		for(int register = 0; register < m_nregister; register++) {
			sum += GetStateI(register);
		}
		return sum;
	}
	
	
	
	final public boolean IsEmpty() {
		for (int i = 0; i < m_nregister; i++) {
			if (GetStateI(i) == 1)
				return false;
		}
		return true;
	}

	
	/*--------------------
	 * BUFFER methods
	 --------------------*/
	
	
	/** 
	 * sets the next state of a cell 
	 * it is set by other cells ONLY
	 */
	final public void SetBufferI(int register, int in_State) {
		m_buffer[register] = in_State;
	}
	
	/**
	 * reads the next state of a cell 
	 * */
	final protected int GetBufferI(int register) {
		return m_buffer[register];
	}
	
	/*--------------------
	 * NEIGHBOURHOOD methods
	 --------------------*/
	
	private GenericCellArray<MultiRegisterCell> m_neighbourhood;
	
	/** returns the i-th neighbour  (no control) */
	final public MultiRegisterCell GetNeighbour(int in_NeighbourPosition) {
		return m_neighbourhood.get(in_NeighbourPosition);
	}
	
	/** returns the state of the i-th neighbour  (no control) */
	final public int GetNeighbourStateI(int register, int in_NeighbourPosition) {
		return GetNeighbour(in_NeighbourPosition).GetStateI(register);
	}
	
	@Override
	/**
	 * a MultipleRegisterCell can be only surrounded by similar cells
	 */
	public void SetNeighbourhood(GenericCellArray<SuperCell> in_NeighbArray) {
		m_neighbourhood = new GenericCellArray<MultiRegisterCell>(in_NeighbArray);
	}
	
	
	/** returns the size of the neighbourhood of the cell */
	final public int GetNeighbourhoodSize() {
		return m_neighbourhood.size();
	}
	
	/**
	 * returns the number of occurrences of state in_state in the neighbourhood
	 * of the cell this means it has to ask all the neighbours about their state
	 * (slow)
	 */
	final public int GetRegisterOccurenceOf(int register, int in_State) {
		int count = 0;
		for (MultiRegisterCell cell : m_neighbourhood) {
			if (cell.GetStateI(register) == in_State)
				count++;
		}
		return count;
	}

	@Override
	public String GetInfo() {
		StringBuilder s = new StringBuilder("states:");
		for (int i=0 ; i < GetLayersNumber(); i++){
			s.append("[").append(i).append(":").append(GetStateI(i)).append("] ");
		}
		return s.toString();
	}
	
	/*--------------------
	 * special for two states (see also "Chemical"...)
	 --------------------*/

	
	public void SetStateA(int state) {
		SetStateI(REGONE, state);		
	}
	
	public void SetStateB(int state) {
		SetStateI(REGTWO, state);		
	}
	
	public int GetStateA() {
		return GetStateI(REGONE); 		
	}
	
	public int GetStateB() {
		return GetStateI(REGTWO); 		
	}
	
	public void SetBufferA(int state) {
		SetBufferI(REGONE, state);		
	}
	
	public void SetBufferB(int state) {
		SetBufferI(REGTWO, state);		
	}
	
	public int GetBufferA() {
		return GetBufferI(REGONE); 		
	}
	
	public int GetBufferB() {
		return GetBufferI(REGTWO); 		
	}

}// class

