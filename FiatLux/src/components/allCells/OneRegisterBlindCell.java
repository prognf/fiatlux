package components.allCells;

import components.randomNumbers.FLRandomGenerator;


/*--------------------
 * a cell with no neighbourhood
 * two uses either it has a fixed value or 
 * it updates its state randomly at each time step
 * @author Nazim Fates
*--------------------*/
public class OneRegisterBlindCell extends OneRegisterIntCell {

	
	final FLRandomGenerator m_randomizer;
	final boolean mb_change;
	
	/** fixed state **/
	public OneRegisterBlindCell(int state){
		mb_change= false;
		m_randomizer= null;
		m_state= state;
		SetBufferState(m_state);
	}
	
	/** fixed state **/
	public OneRegisterBlindCell(int initState, FLRandomGenerator in_randomizer){
		mb_change= true;
		m_randomizer= in_randomizer;
		m_state= initState;
		SetBufferState(m_state);
	}

	/* sets directly the state of a cell 
	 * used by initialising procedures ONLY */
	public void InitState(int in_State) {
		// 	nothing
	}
	
	public boolean IsStable() {
		return true;
	}

	public void sig_Reset() {
		// nothing
	}

	public void sig_UpdateBuffer() {
			// nothing	
	}

	public void sig_MakeTransition() {
		if (mb_change){
			m_state=m_randomizer.RandomInt(2);
		} // else : do nothing
	}

	/*public void SetNeighbourhood(ListCell myCellTable) {
		// nothing 
	}*/
	

	final public OneRegisterBlindCell GetNeighbour(int in_NeighbourPosition) {
		return null;
	}

	public static OneRegisterBlindCell GetNewBlindCell(int state, boolean cellChange, 
			FLRandomGenerator randomizer) {
		if (cellChange){
			return new OneRegisterBlindCell(state, randomizer);
		} else {
			return new OneRegisterBlindCell(state);
		}				
	}


}
