package components.types;

import grafix.gfxTypes.controllers.IntController;

/* integer parameter */
public class IntPar {

	public String m_label;
	private int m_val;
	private IntParController m_controller;
	
	
	/* label associated to the parameter */ 
	public IntPar(String label, int def_value){
		m_label= label;
		m_val = def_value;
	}
	
	public void SetVal(int val){
		m_val=val;
		
		if(m_controller != null){
			m_controller.Update();
		}
	}
	
	public int GetVal(){
		return m_val;
	}
	
	public IntController GetControl(){
		if(m_controller == null){
			m_controller =  new IntParController();
		}
		
		return m_controller;
	}
	
	/** default behaviour **/
	class IntParController extends IntController {

		public IntParController() {
			super(m_label);
		}

		@Override
		protected int ReadInt() {
			return m_val;
		}

		@Override
		protected void SetInt(int val) {
			m_val= val;		
		}
		
	}
	
	
}
