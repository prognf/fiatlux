package components.types;

/************************************
 * Class :   DoubleCouple
 * Description: encapsulates two doubles  
 * @author Nazim Fates
 *******************************************/ 
public class DoubleCouple{

    double m_x, m_y;

    public DoubleCouple(double in_x, double in_y){
	m_x= in_x; m_y= in_y;
    }  

    public DoubleCouple(DoubleCouple xy) {
		m_x= xy.m_x;
		m_y= xy.m_y;
	}

	final public double X(){
	return m_x;
    }

    final public double Y(){
	return m_y;
    }

    final public String ToString(){
	return "( " + m_x + " , " + m_y + " ) ";
    }
    
    final public double XY(){
	return m_x * m_y ;
    }
    
    public void add(DoubleCouple xy) {
    	m_x+= xy.m_x;
    	m_y+= xy.m_y;
    }

    static public DoubleCouple add(DoubleCouple xy1, DoubleCouple xy2) {
    	DoubleCouple xy= new DoubleCouple(xy1);
    	xy.add(xy2);
    	return xy;
    }
    
    @Override
    public String toString() {
    	return String.format("(%.3f,%.3f)", m_x,m_y);
    }

}
