package components.types;

import java.util.Objects;

import components.randomNumbers.StochasticSource;
import main.Macro;


/*------------------------------------------------------------------------------
- IntCouple encapsulates two integers
- @author Nazim Fates
------------------------------------------------------------------------------*/
public class IntC  {

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();

	//--------------------------------------------------------------------------
	//- attributes
	//--------------------------------------------------------------------------


	protected int m_x;
	protected int m_y;

	//--------------------------------------------------------------------------
	//- construction
	//--------------------------------------------------------------------------


	/** X and Y */
	public IntC(int in_x, int in_y) {
		m_x = in_x;
		m_y = in_y;
	}

	

	public IntC Duplicate() {
		return new IntC(m_x,m_y);
	}
	
	public IntCm DuplicateM() {
		return new IntCm(m_x,m_y);
	}
	//--------------------------------------------------------------------------
	//- operations
	//--------------------------------------------------------------------------



/** for periodic boundary conditions */
	static final public int AddModulo(int z, int dz, int size){
		return (z + dz + size) % size;
	}

	//--------------------------------------------------------------------------
	//- other  operations
	//--------------------------------------------------------------------------




	//--------------------------------------------------------------------------
	//- mapping
	//--------------------------------------------------------------------------

	/** mapping for finding index **/
	public int Map2Dto1D(int x, int y) {
		return x + y * m_x;
	}

	/** mapping for finding index **/
	public int Map2Dto1D(IntC xypos) {
		return Map2Dto1D(xypos.X(),xypos.Y());
	}

	/** implies creation of new object **/
	public IntC Map1Dto2D(int pos) {
		int x= pos % m_x;
		int y= pos / m_x;
		return new IntC(x,y);
	}


	//--------------------------------------------------------------------------
	//- get & set
	//--------------------------------------------------------------------------

	
	/** returns X */
	final public int X() {
		return m_x;
	}

	/** returns Y */
	final public int Y() {
		return m_y;
	}

	/** printable form */
	final public String ToString() {
		return "(" + m_x + "," + m_y + ")";
	}

	final public String toString(){
		return ToString();
	}


	/** returns product of X & Y */
	final public int prodXY() {
		return m_x * m_y;
	}

	final public boolean IsEqualTo(IntC val){
		return (m_x == val.m_x) && (m_y == val.m_y);
	}

    @Override
	public int hashCode() {
		return Objects.hash(m_x, m_y);
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IntC other = (IntC) obj;
		return IsEqualTo(other);
	}



	public boolean MYequals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IntC)) return false;
        IntC icouple = (IntC) o;
        return IsEqualTo(icouple);
    }
	
	//--------------------------------------------------------------------------
	//- others
	//--------------------------------------------------------------------------




	/** tests if two coordinates have same line or same column */
	public static boolean SameLineOrCol(IntC ixy, IntC jxy) {
		return (ixy.m_x == jxy.m_x) || (ixy.m_y == jxy.m_y);
	}


	/** tests if two coordinates are on the same diagonal */
	public static boolean SameDiag(IntC ixy, IntC jxy) {
		return Math.abs(ixy.m_x - jxy.m_x) == Math.abs (ixy.m_y - jxy.m_y);
	}

	public int ScalarProduct(IntC xy) {
		return m_x * xy.m_x + m_y * xy.m_y;
	}

	/** (0,0) at init */
	public static IntC New() {
		return new IntC(0,0);
	}

	

	/** returns a random couple with coord. in [0,X) [O,Y) */
	public IntC RandomPos(StochasticSource rnd) {
		int x= rnd.RandomInt(m_x), y= rnd.RandomInt(m_y);
		return new IntC(x,y);
	}
	/** min of two coordinates **/
	public int Min() {
		return Math.min(m_x, m_y);
	}

	/** max of two coordinates **/
	public int Max() {
		return Math.max(m_x, m_y);
	}

	
	/** says if a point (x,y) is "inside the grid" **/
	public boolean ZZZisInsideGrid(int x, int y){
		return (x>=0)&&(x<m_x)&&(y>=0)&&(y<m_y);
	}

	/** says if a point (x,y) is "inside the grid" **/
	public boolean isInsideGrid(IntC xy){
		return 	(xy.m_x>=0)&&
				(xy.m_x< m_x) &&
				(xy.m_y>=0) &&
				(xy.m_y< m_y);
	}
	
	/** adds without modifying the arguments **/
	final public static IntC AddNew(IntC xy, IntC dxy) {
		int nx= xy.X() + dxy.m_x, ny= xy.Y() + dxy.m_y;
		return new IntC(nx,ny);
	}

	public void Add(IntC dxy){
		m_x += dxy.X();
		m_y += dxy.Y();
	}

	public void addXY(int dx, int dy) {
		m_x+= dx;
		m_y+= dy;
	}
	
	/** multiplies the values by a factor m**/
	public void multiply(int m) {
		m_x*=m; m_y*=m;
	}
	
	/** multiplies the two values by the two factors mx and my **/
	public void multiplyTwoFactors(int mx, int my) {
		m_x*=mx; m_y*=my;
	}

	/** tells if (x,y) is in the rectangle defined by XYbounds**/
	public boolean inBounds(IntC XYbounds) {
		return (m_x>=0) && (m_y>=0) && (m_x< XYbounds.X()) && (m_y < XYbounds.Y());
	}



	
}
