package components.types;

import java.util.ArrayList;

import initializers.SuperInitializer;
import main.Macro;


public class MonitorList extends ArrayList<Monitor> {

	public void ReceiveSignal(FLsignal sig){
		for (Monitor m : this){
			m.ReceiveSignal(sig);
		}
	}
	
	/** we check that no two initialisers co-exist in the list */ 
	public void Add(Monitor monitor) {
		if (monitor instanceof SuperInitializer) {
			for (Monitor element : this) {
				if (element instanceof SuperInitializer) {
					Macro.FatalError("There was an attempt to add two initialisers in a monitor list");
				}
			}
		} 
		super.add(monitor);
	}

	public void PrintComponents() {
		StringBuilder s = new StringBuilder(" monitor list :");
		
		for (Monitor monitor : this){
			s.append(monitor.getClass().getCanonicalName()).append("::");
		}
		Macro.print(s.toString());
	}
	
	/** DO NOT USE **/
	@Deprecated
	final public boolean add(Monitor monitor){return false;}

}
