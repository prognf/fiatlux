package components.types;

public class Triplet {
	
	public Triplet(int xv, int yv, int zv) {
		x=xv; y=yv; z=zv;
	}

	public int x,y,z;
	
	
	public String toString() {
		return String.format("(%d,%d,%d)", x, y, z);
	}
}
