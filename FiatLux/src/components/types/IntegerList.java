package components.types;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import components.randomNumbers.FLRandomGenerator;
import main.Macro;


//public class IntegerList extends java.util.Vector<Integer> {
public class IntegerList extends java.util.ArrayList<Integer> {
	//	public class IntegerList extends java.util.LinkedList<Integer> {

	/*--------------------
	 * construction
	 --------------------*/

	public IntegerList() {
		super();
	}

	/** n integers equal to val **/
	public IntegerList(int size, int val) {
		super(size);
		for (int i = 0; i < size; i++) {
			addVal(val);
		}
	}

	/** construction with an array of int */
	public IntegerList(int[] in_array) {
		super();
		for (int anIn_array : in_array) {
			addVal(anIn_array);
		}
	}

	public static IntegerList ReadFromFile(String filename){
		return new IntegerList(filename);
	}

	/** reading from file **/
	private IntegerList(String filename) {
		super();
		FLStringList input= new FLStringList(filename);
		for (String number : input){
			int data= FLString.String2Int(number);
			addVal(data);
		}
	}

	/*----------------
	 * basic methods
	 *----------------*/

	/**
	 * Adds an Integer to the list
	 * 
	 */
	public void addVal(int i) {
		add(i);
	}

	public void AddMulti(int ...listval) {
		for (int val: listval){
			addVal(val);
		}
	}

	/** inserts val in position pos */
	public void Insert(int pos, int val){
		add(pos, val);
	}

	/**
	 * Removes an Integer from the list
	 */
	public void RemoveItem(int i) {
		remove(new Integer(i));
	}

	/** removes object in position i ==and returns it== */
	public int RemovePos(int i) {
		int x = Get(i);
		remove(i);
		return x;
	}

	/** removes object in position i*/
	public void RemoveLast() {
		RemovePos(size()-1);
	}

	/** returns the ith element * */
	public int Get(int i) {
		return get(i);
	}


	public void Set(int i, int val){
		set(i, val);
	}

	/** returns the number of elements * */
	final public int GetSize() {
		return size();
	}

	/** contains an int ? */
	final public boolean Contains(int val){
		return contains(val);
	}

	public int IndexOf(int rcode) {
		return indexOf(rcode);
	}

	/** dichotomic search in a sorted list */
	final public boolean ContainsDicho(int val){
		if (GetSize() == 0){
			/* empty list */
			return false ;
		} else {
			int binf=0, bsup= GetSize() - 1;
			int m=(binf + bsup) / 2;
			//Macro.Debug(binf + "::" + bsup + ":"+ m +  " ;" + Get(m) + " val: " + val);
			while (binf < bsup ){
				/*if (Get(m) == val ){
				return true; 
			} else */
				if (Get(m) < val){
					binf= m + 1;
				} else {
					bsup= m;
				}
				m=(binf + bsup) / 2;
				//Macro.Debug(binf + "::" + bsup + ":"+ m +  " ;" + Get(m) + " val: " + val);
			}

			return (Get(bsup) == val);
		}
	}


	/*--------------------
	 * maths methods
	 --------------------*/

	/** assumes entries are binary : 
	 * TODO can be improved **/
	public int toIntByDecimalConversion() {
		int sum=0, mult=1;
		for (int val : this){
			sum+= val * mult;
			mult *= 2;
		}
		return sum;
	}

	/*--------------------
	 * get methods
	 --------------------*/


	/** first of a list */
	final public int GetFirst() {
		return Get(0);
	}

	/** first of a list */
	final public int GetLast() {
		return Get(GetSize()-1);
	}

	/** greatest of a list */
	final public int GetMax() {
		int max = Get(0);
		for (int i = 1; i < size(); i++) {
			int current = Get(i);
			if (current > max) {
				max = current;
			}
		}
		return max;
	}

	/** smallest of a list */
	final public int GetMin() {
		int min = Get(0);
		for (int i = 1; i < size(); i++) {
			int current = Get(i);
			if (current < min) {
				min = current;
			}
		}
		return min;
	}

	/*--------------------
	 * other methods
	 --------------------*/

	/** returns one element in list at random */
	final public int ChooseOneAtRandom(FLRandomGenerator in_rand) {
		return Get ( in_rand.RandomInt(GetSize()) );
	}

	/** all integers in [a,b] (boundaries included) **/
	public static IntegerList Interval(int a, int b){
		IntegerList list = new IntegerList();
		for (int i = a; i <= b; i++) {
			list.addVal(i);
		}
		return list;
	}

	/** integers from 0 to n-1 (included) */
	public static IntegerList NintegersFromZero(int n){
		return Interval(0, n-1);
	}

	/** integers from 1 to n (included) */
	public static IntegerList NintegersFromOne(int n){
		return Interval(1, n);
	}

	
	/** removes one element and returns it **/
	public int extractOneRandom(FLRandomGenerator rand) {
		int pos=rand.RandomInt(size());
		int val=this.remove(pos);
		return val;
	}

	/** returns a NON-SORTED list with k randomly chosen elements in the original list 
	 * BE CAREFUL THE SELECTED ELEMENTS ARE REMOVED FROM ORIGINAL LIST !!*/
	public IntegerList extractStronglyKRandomElements(int k, FLRandomGenerator in_rand){
		if (k > size()){
			Macro.SystemWarning(" Extracting more lements than the list size; setting k = " + k);
			k= size();
		}
		IntegerList newlist= new IntegerList();
		for (int i = 0; i < k; i++) {
			int index = in_rand.RandomInt( this.size() );
			newlist.addVal(this.Get(index));
			this.remove(index);
		}
		return newlist;
	}

	
	
	
	/** returns a NON-SORTED list with k randomly 
	 * chosen integers between 0 and n-1 */
	public static IntegerList getKamongN(int k, int n, FLRandomGenerator in_rand) {
		if (k > n) {
			Macro.SystemWarning(" Check call to  GetKAmongNFirst : k < n ");
		}
		IntegerList list = IntegerList.NintegersFromZero(n);
		return list.extractStronglyKRandomElements(k, in_rand);		
	}

	
	/** returns a list with k randomly 
	 * chosen integers between 0 and n-1 
	 * we proceed by REMOVAL */
	public static IntegerList GetKamongNindicesByRemoval(int k, int n, FLRandomGenerator in_rand) {
		if (k > n) {
			Macro.FatalError(" Check call to  GetKAmongNFirst : k < n ");
		}
		IntegerList list = IntegerList.NintegersFromZero(n);
		// removal of elements
		for (int i = 0; i < n - k; i++) {
			int sz = list.size();
			int index = in_rand.RandomInt(sz);
			list.RemovePos(index);
		}
		return list;
	}


	/*--------------------
	 * I/O methods
	 --------------------*/

	/** formatted print */
	public String ToString(){
		String str = "S[" + size() + "] : {";
		for (Integer integer : this) {
			str += integer + ", ";
		}
		str= str.substring(0,str.length()-2);
		str+="}";
		return str;
	}

	/** formatted print */
	public void Print() {
		Macro.printNOCR("[");
		for (Integer integer : this) {
			Macro.printNOCR(integer + ":");
		}
		Macro.print("]");
	}

	/** "raw" print */
	public void prnRaw() {
		Macro.printNOCR("");
		for (Integer integer : this) {
			Macro.printNOCR(integer + " ");
		}
		Macro.print("");
	}


	/*static private IntegerList ForEachLoop(IntegerList inputlist){
		IntegerList outlist = new IntegerList();
		for (Integer element : inputlist){
			if (element.intValue() % MOD ==0 ){
				outlist.Add(element);
			}
		}
		return outlist;
	}*/


	/** DELIBERATE use of iterators ! 
	 * for perf tests only **/
	static private IntegerList OddNumbersIterator(IntegerList inputlist){
		IntegerList outlist = new IntegerList();

		// For a set or list
		for (Integer element : inputlist) {
			if (element % MOD == 0) {
				outlist.addVal(element);
			}
			//Macro.fPrintNOCR("::" + element);
		}
		return outlist;
	} 

	/** for perf tests only **/
	static private IntegerList OddNumbersForLoopRemovePos(IntegerList inputlist){
		IntegerList outlist = new IntegerList();

		// For a set or list
		for (int i=0; i<inputlist.GetSize(); i++) {
			int element= inputlist.Get(i);
			if (element % MOD == 0){
				outlist.addVal(element);
			}
			//Macro.fPrintNOCR("::" + element);
		}
		return outlist;
	} 

	/** adding two lists */
	public void addList(IntegerList newnodes) {
		this.addAll(newnodes);
	}

	public int Sum() {
		int sum=0;
		for (int element: this){
			sum += element;
		}
		return sum;
	}

	/** counts the number of occurrence of a given int **/
	public int CountOccurence(int val) {
		int count=0;
		for (int element: this){
			if( element == val) {
				count++;
			}
		}
		return count;
	}

	/** creates a list with some initial values in it **/
	public static IntegerList GetNewListWithInit(int ... values) {
		IntegerList list= new IntegerList();
		for (int data : values){
			list.add(data);
		}
		return list;
	}

	/** useful ? see if clone() is not sufficient ... */ 
	public IntegerList Copy() {
		IntegerList  lst = new IntegerList();
		lst.addAll(this);
		return lst;
	}


	/** adds {val} to element at position {index} */
	public void AddToVal(int index, int val) {
		int oldval= Get(index);
		Set(index, oldval + val);
	}

	/** adds 1 to element at position {index} */
	public void IncrementValAtPos(int index) {
		AddToVal(index, 1);
	}


	/*------------------------
	 * conversions
	 *------------------------*/
	/** conversion to string with whitespace separation **/
	public String ToOneLine() {
		String out="";
		for (int val : this) {
			out+= val + " ";
		}
		return out;
	}

	/** conversion to Array **/
	public int[] ToIntArray() {
		int size= GetSize();
		int [] out = new int[size];
		for (int i=0; i< size; i++){
			out[i]= Get(i);
		}
		return out;
	}

	/*------------------------
	 * permutations
	 *------------------------*/

	public void PermuteVal(int pos1, int pos2) {
		int val= Get(pos1);
		Set(pos1, Get(pos2));
		Set(pos2, val);
	}

	/** list of Integer lists with indices from 0 to level - 1 */ 
	public static ArrayList<IntegerList> GetPermutations(int level) {
		IntegerList lst=  NintegersFromZero(level);
		return GetPermutationsOf( lst );
	}

	// ???
	private static ArrayList<IntegerList> GetPermutationsOf(IntegerList lstElem) {
		// Macro.FormatDebug(" list to permute :  " + lstElem);
		ArrayList<IntegerList> biglst= new ArrayList<IntegerList>();
		if (lstElem.size()<=1){
			biglst.add(lstElem);
			return biglst;
		}
		for (int pos=0; pos < lstElem.size(); pos++){
			IntegerList copy= (IntegerList)lstElem.clone(); 
			int elem= copy.RemovePos(pos);
			biglst.addAll(AddToBigList(elem, GetPermutationsOf(copy)) );
		}
		return biglst;
	}

	// ???
	private static ArrayList<IntegerList> AddToBigList(int elem, ArrayList<IntegerList> bigList) {
		for (IntegerList lst : bigList){
			lst.add(0,elem); // adds first
		}
		return bigList;
	}

	public static void TestGetPermutations(){
		Macro.PrintEmphasis(" test permutations ");

		ArrayList<IntegerList> perm = GetPermutations(3);
		String str= FLString.ToStringWithCR(perm);
		Macro.print(str);
	}

	public void PermuteElements(int index1, int index2) {
		int val1= Get(index1);
		this.Set(index1,Get(index2));
		this.Set(index2, val1);
	}


	/*--------------------
	 * small methods
	 --------------------*/


	public static int findIndexMinVal(List<Integer> intlist) {
		int minpos= 0;
		int minval= intlist.get(0);
		for (int i=1; i< intlist.size();i++) {
			int curval= intlist.get(i);
			if (curval<minval) {
				minpos= i;
				minval= curval;
			}
		}
		return minpos;
	}

	public static IntegerList GetShiftedList(List<Integer> extr, int minpos) {
		IntegerList shiftList= new IntegerList();
		for (int i=0; i< extr.size();i++) {
			int shiftindex=  (i+minpos)%extr.size() ;
			shiftList.add( extr.get(shiftindex) );
		}
		return shiftList;
	}
	/*---------
	 * SORTING methods
	 --------------------*/


	// this allows us to do a lexicographic sort (to identify new cycles)
	final static public Comparator<IntegerList> COMPARELEXICOGRAPHIC= new Comparator<IntegerList>() {

		/** 1 means list1 > list2 */
		public int compare(IntegerList list1, IntegerList list2) {
			//Macro.fDebug(" comparing %s and %s ", list1, list2);
			return compareFromIndex(list1, list2,0);
		}

		private int compareFromIndex(IntegerList list1, IntegerList list2, int index) {
			if ( (list1.size()==index) && (list2.size()==index) ){
				return 0;
			} else if (list1.size()==index){
				return -1;
			} else if  (list2.size()==index){
				return 1;
			} else if (list1.get(index)<list2.get(index)) {
				return -1;
			} else if (list1.get(index)>list2.get(index)) {
				return 1;
			} else {
				return compareFromIndex(list1, list2,index+1);
			}
		}
	};

	public void Sort() {
		Collections.sort(this);
	}


	/** Bubble sort **/
	public void SortBubble(){

		boolean sorted= false;
		while (!sorted){
			sorted= true;
			for (int i=1; i < GetSize(); i++){
				int current= Get(i-1);
				int next = Get(i);
				if (current > next){
					// swap (i-1,i);
					Set(i-1,next);
					Set(i,current);
					sorted= false;
				}
			}
		}
	}



	/*---------
	 * other methods
	 --------------------*/


	/** I/O : writes data on one column **/
	public void WriteToFileCol(String filename) {
		FLStringList wrt= FLStringList.FromArrayList(this);
		wrt.WriteToFile(filename);
	}

	/*--------------------
	 * test methods
	 --------------------*/

	final static int MOD=3;
	final static int TESTSIZE=1000000;
	static boolean PRINT=false;

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();

	final static int [] TT1= new int [] { -99, 0, 1, 8};
	final static int [] TT2= new int [] { -99, -1, 1, 2,5};

	public static void DoTest(){
		Macro.BeginTest(CLASSNAME);
		FLRandomGenerator rand = FLRandomGenerator.GetGlobalRandomizer();
		IntegerList l = new IntegerList();
		for (int i = 0; i < 20; i++) {
			l.addVal(i * 10);
		}
		l.RemoveItem(22);
		l.RemoveItem(30);
		l.Print();

		int x= -5, y= 200, z=130;
		Macro.print(" l Contains: " 
				+ " " + x + "->" + l.Contains(x)  
				+ " " + y + "->" + l.Contains(y) 
				+ " " + z + "->" + l.Contains(z) );
		/* dicho search test */
		Macro.print(" l Contains: " 
				+ " " + x + "->" + l.ContainsDicho(x)  
				+ " " + y + "->" + l.ContainsDicho(y) 
				+ " " + z + "->" + l.ContainsDicho(z) );
		Macro.print( "*contains 50:" + l.ContainsDicho(50) + " index : " + l.IndexOf(50));
		Macro.print( "*contains 57:" + l.ContainsDicho(57) + " index : " + l.IndexOf(57));
		/*  */
		IntegerList l3 = getKamongN(7, 7, rand);
		l3.Insert(3,-99);
		l3.Print();
		Macro.print(" Min :" + l3.GetMin() + " Max :" + l3.GetMax());
		l3.SortBubble();
		Macro.print(" sorting..."); 
		l3.Print(); 
		Macro.print(" adding a list...");
		IntegerList l2= IntegerList.NintegersFromZero(3);
		l3.addAll(l2);
		Macro.print(" to string: " + l3.toString() );
		l3.Print();
		Macro.print(" extracting 3 random elements in the list...");
		IntegerList l4 = l3.extractStronglyKRandomElements(3, rand);
		l4.Print();

		IntegerList l5a= new IntegerList(TT1);
		IntegerList l5b= new IntegerList(TT2);
		Macro.fPrint("res:%d", IntegerList.COMPARELEXICOGRAPHIC.compare(l5a, l5b));
	}

	static public void DoPerfTest() {
		IntegerList numbers= IntegerList.NintegersFromZero(TESTSIZE);
		// iterators test
		Macro.BeginTest("for each");
		IntegerList l6 = OddNumbersForLoopRemovePos(numbers);
		Macro.EndTest();

		Macro.BeginTest("iterators");
		IntegerList l4 = OddNumbersIterator(numbers);
		Macro.EndTest();

		Macro.BeginTest("position");
		IntegerList l5 = OddNumbersForLoopRemovePos(numbers);
		Macro.EndTest();

		if (PRINT){
			l4.Print();
			l5.Print();
			l6.Print();
		}
	}


	//--------------------------------------------------------------------------
	//-test
	//--------------------------------------------------------------------------



	public static void main(String[] argv) {
		DoTest();
	}

	


} // end IntegerList
