package components.types;


/*--------------------
 * interface 
  * @author : Nazim Fates
*--------------------*/
public interface DoubleControlable {

	double GetDoubleValue();

	void SetDoubleValue(double newval);

		
}
