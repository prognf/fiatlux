package components.types;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import components.randomNumbers.FLRandomGenerator;
import main.Macro;

public class FLStringList extends java.util.ArrayList<String> {
	final static String SIZE_TAG = "sz", PAR_TAG = "v";
	/*--------------------
	 * attributes
	 --------------------*/

	// heritage from java.util.ArrayList

	/*--------------------
	 * construction
	 --------------------*/

	public FLStringList() {
		super();
	}

	public static FLStringList FromArrayList(ArrayList<?> list) {
		FLStringList newlist = new FLStringList();
		for (Object obj : list) {
			newlist.Add(obj.toString());
		}
		return newlist;
	}

	public static FLStringList fromArray(Object[] list) {
		FLStringList newlist = new FLStringList();
		for (Object obj : list) {
			newlist.Add(obj.toString());
		}
		return newlist;
	}

	/** reading from file : use OpenFromFile instead **/
	@Deprecated
	public FLStringList(String filename) {
		try{
			ReadInputFromFile(filename);
		} catch (Exception e) {
			e.printStackTrace();
			Macro.SystemWarning("Error when reading file :" + filename);
		}
	}


	/** opens an existing file ; reads it and builds object **/
	public static FLStringList OpenFile(String filename) {
		FLStringList list= new FLStringList();
		try{
			list.ReadInputFromFile(filename);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			Macro.SystemWarning("** File not found **:" + filename + "current path:"  + Macro.GetCurrentDir());
			return null;
		}
		catch (Exception e) {
			e.printStackTrace();
			Macro.SystemWarning("Error when reading file :" + filename);
			return null;
		}
		if (list.size()==0) {
			Macro.SystemWarning("Empty file loaded:" + filename);
		}
		return list;
	}


	/** Adds a String to the list **/
	public void Add(String s) {
		add(s);
	}
	
	/** Adds a formatted String to the list **/
	public void formatAdd(String sFormat, Object ... args) {
		add(String.format(sFormat, args));	
	}

	/** Adds a sequence of strings to the list **/
	public void Add(String ... strings) {
		for (String s : strings){
			Add(s);
		}
	}

	/**
	 * Removes a String from the list */
	public void Remove(String s) {
		remove(s);
	}

	/** first element of the list */
	public String GetFirstItem(){
		return Get(0);
	}

	/** last element of the list */
	public String GetLastItem(){
		return Get(size() - 1 );
	}


	/** returns the string at pos i * */
	final public String Get(int i) {
		return get(i);
	}

	final public int GetSize() {
		return size();
	}

	/*------------
	 * 
	 */

	/** converts into doubles **/
	public ArrayList<Double> toDoubleList() {
		ArrayList<Double> out = new ArrayList<Double>();
		for ( String read : this){
			double d = FLString.String2Double(read);
			out.add(d);
		}
		return out;
	}

	/** converts into doubles **/
	public double[] toDoubleArray() {
		int size= GetSize();
		double[] out = new double [size] ;
		for ( int i=0; i< size; i++){
			String read= Get(i);
			double d = FLString.String2Double(read);
			out[i]= d;
		}
		return out;

	}


	/**
	 * finds the first position of a string in list ; return -1 if not found
	 * seems never called !
	 */
	public int findFirstPosEquals(String in_str) {
		int pos = -1;
		boolean flag = false;
		Iterator<String> iter = this.listIterator();
		while (iter.hasNext() && (!flag)) {
			pos++;
			String s = iter.next();
			flag =s.equals(in_str);
		}
		return (flag ? pos : -1);
	}

	
	/**
	 * positionof the first elements which CONTAINS a given key in a list ; return -1 if not found
	 */
	public int findFirstPosContains(String key) {
		int pos = -1;
		boolean flag = false;
		Iterator<String> iter = this.listIterator();
		while (iter.hasNext() && (!flag)) {
			pos++;
			String s = iter.next();
			flag =s.contains(key);
		}
		return (flag ? pos : -1);
	}
	/**
	 * says if at least one of the members of the list contains a given
	 * substring
	 */
	public boolean Contains(String in_substr) {
		boolean flag = false;
		String s;
		Iterator<String> iter = this.listIterator();
		while (iter.hasNext() && (!flag)) {
			s = iter.next();
			flag =(s.contains(in_substr));
			// -1 is the convention for chain not found
		}
		return flag;
	}

	/** returns data in a list of String */
	public String[] ToArray() {
		int s = size();
		String[] array = new String[s];
		for (int i = 0; i < s; i++) {
			array[i] = Get(i);
		}
		return array;
	}


	/** returns data in a single String */
	public String ToOneString() {
		StringBuilder output= new StringBuilder();
		for (int i = 0; i < size(); i++) {
			output.append(Get(i)).append(" ");
		}
		return output.toString();
	}

	@Override
	public String toString() {
		return ToOneString();
	}

	/*--------------------
	 * I/O methods
	 --------------------*/

	/**
	 * Sorts the Strings into ascending order
	 */
	public void Sort() {
		Collections.sort(this);
	}


	/** reads from a **Non-empty** file 
	 * @throws IOException */
	public void ReadInputFromFile(String in_path) throws IOException  {
		BufferedReader b = new BufferedReader(new InputStreamReader(
				new FileInputStream(in_path)));
		String s = b.readLine();
		while (s != null) {
			this.Add(s);
			//Macro.Debug("read in file :"+s);
			s = b.readLine();
		}
		b.close();
	}
	
	/** reads a file in the --internal-- filesystem 
	 * @throws IOException */
	public void ReadFileInInternalSystem(String pathToressource, String relpath) throws IOException  {
		ClassLoader loader = ClassLoader.getSystemClassLoader();
		//String PATHDEBUG="models/CAmodelsvar/aperiodicTiling/dataTile/Xenv-superH.txt";
		InputStream fis = loader.getResourceAsStream(pathToressource + relpath);
		FLfileReader read = new FLfileReader(fis);
		String ln;
		do { // go to envlp desc.
			ln= read.readLn();
			this.Add(ln);
		} while (ln!=null);  // EOF ??
		
	}

	/** writes the list to file ******* */
	public void WriteToFile(String filename) {

		if (filename==null){
			Macro.SystemWarning("The filename provided is null !, using default name output.out");
			filename= "output.out";
		}
		try {
			int size = GetSize();
			PrintWriter q = new PrintWriter(new FileOutputStream(filename),
					false /* no adding */);
			// writing each entry
			for (String str : this) {
				q.println(str);
			}
			// we close
			q.close();
			Macro.print(4, "Wrote : " + filename + " (nlines:" + size + ")");
		} catch (Exception e) {
			e.printStackTrace();
			Macro.FatalError(this, "WriteToFile","Error when writing");
		}
	}


	public static void AppendToFile(String filename, String lineToAppend) {
		try {
			BufferedWriter output = 
					new BufferedWriter(new FileWriter(filename, true));
			output.write(lineToAppend+"\n");
			output.close();
		} catch (IOException e) {
			e.printStackTrace();
			Macro.SystemWarning("error in append to file");
		}
	}
	/** writes the list to file and path ******* */
	public void zzzWriteToFile(String in_PathName, String in_FileName) {
		// Needs at least a filename.
		if (in_FileName==null){
			Macro.FatalError("The filename provided is null !!");
		}

		//if folders doesn't exist, create it !
		if (in_PathName != null
				&& ! in_PathName.equals("")
				&& ! new File(in_PathName).exists()) {
			new File(in_PathName).mkdir();
		}

		WriteToFile(in_PathName + in_FileName);
	}

	/** object -> string ** */
	public void AddData(DVector onesamplevalue) {
		// conversion
		String newline = onesamplevalue.ToString();
		//Macro.Debug(newline);
		this.Add( newline );
	}

	public void Print() {
		int s = this.size();
		Macro.print("Size [" + s + "] ");
		for (String str : this) {
			Macro.print("" + str);
		}
	}

	/** formats all data in one string */
	public String GetInOneLine() {
		StringBuilder inline = new StringBuilder();
		inline.append("[").append(size()).append("] ");
		for (String read : this) {
			inline.append(" : ").append(read);
		}
		return inline.toString();
	}

	/** prints all data as it is */
	public void PrintRaw() {
		for (String read : this) {
			Macro.print(""+  read + "");
		}
	}

	/** prints all data in one string */
	public void PrintOnOneLine() {
		Macro.print(GetInOneLine());
	}

	
	/*--------------------
	 * test
	 --------------------*/

	final static int NLINE= 3000;
	final static int SZLINE= 50;
	final static FLRandomGenerator m_rand = FLRandomGenerator.GetGlobalRandomizer();

	static public void TestWriteFile(){
		FLStringList list= new FLStringList();
		for (int line=0; line < NLINE; line++){
			StringBuilder str= new StringBuilder("hi ha hop");
			for (int s=0; s < SZLINE; s++){
				int i = m_rand.RandomInt(10);
				str.append(i);
			}
			list.Add(str.toString());
		}
		list.WriteToFile("listTest.txt");
	}

	static public void DoTest() {
		Macro.BeginTest("StringList");
		FLStringList l = new FLStringList();
		l.Add("alpha");
		l.Add("beta");
		l.Add("gamma");
		l.add("delta");

		// test Remove
		l.Remove("beta");
		l.Remove("houhou");

		// test I/O
		Macro.print("Current dir :" + Macro.GetCurrentDir());
		l.WriteToFile("ListTest.lst");
		FLStringList l2 = FLStringList.OpenFile("ListTest.lst");
		l2.Print();

		// test Contains
		Macro.print(" contains pha: " + l2.Contains("pha"));
		Macro.print(" contains ga: " + l2.Contains("ga"));
		Macro.print(" contains meu: " + l2.Contains("meu"));

		// test FindFirstPos
		Macro.print("alpha is at pos: " + l2.findFirstPosEquals("alpha"));
		Macro.print("beta is at pos: " + l2.findFirstPosEquals("beta"));
		Macro.print("delta is at pos: " + l2.findFirstPosEquals("delta"));

		Macro.EndTest();
	}

	public static FLStringList ReadFile(String filename) throws IOException  {
		FLStringList newlist = new FLStringList();
		newlist.ReadInputFromFile(filename);
		return newlist;
	}

	/** opens and adds if file exists 
	 * other wise creates an empty list
	 */
	public static FLStringList OpenAppendMode(String filename) {
		FLStringList list = new FLStringList();
		if (Macro.FileExists(filename)){
			try {
				list.ReadInputFromFile(filename);
			} catch (IOException e) {
				Macro.FatalError(" File not found [this error should not happen]");
				e.printStackTrace();
			}
		} 
		return list;
	}

	public void fAdd(String sFormat, Object ... args) {
		Add(String.format(sFormat, args));		
	}

	public static FLStringList OpenFileInsideSysFile(String pathtoressource, String relpath) {
		FLStringList list= new FLStringList();
		try{
			list.ReadFileInInternalSystem(pathtoressource,relpath);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			Macro.SystemWarning("**File not found**: " + relpath + "  in internal filesystem: "  + pathtoressource);
			return null;
		}
		catch (Exception e) {
			e.printStackTrace();
			Macro.SystemWarning("Error when reading file :" + relpath + "in internal filesystem:"  + pathtoressource);
			return null;
		}
		if (list.size()==0) {
			Macro.SystemWarning("Empty file loaded:" + relpath + "in internal filesystem:"  + pathtoressource);
		}
		return list;
	}

	
	
	/*	public static void main(String[] argv) {
		DoTest();
		//TestWriteFile();
	}*/
} // end IntegerList
