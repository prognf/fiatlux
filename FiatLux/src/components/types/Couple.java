package components.types;

/** simple wrapper class **/
public class Couple<T> {

	public Couple(T valA, T valB) {
		e1= valA;
		e2= valB;
	}
	public T e1;
	public T e2;
	
}
