package components.types; 

import main.Macro;
import main.MathMacro;

/*--------------------
 * This class is here to store the configuration of an ECA
 * @author Alexis Ballier
--------------------*/ 


public class Configuration{
	public int[] state, next_state;
	int length;
	int n;
	int border_left, border_right;

	/** basic constructor **/
	public Configuration(){
		border_left = border_right = 0;
	}

	/** Constructor which sets the borders to left and right **/
	public Configuration(int left, int right){
		border_left = left;
		border_right = right;
	}

	/** Sets the state to conf, this does not include the borders **/
	public void SetState(int[] conf){
		n = conf.length;
		length=n+2;
		state = conf;
		next_state = new int[n];
	}

	/** With a binary coded state **/
	public void SetState(int conf, int len){
		SetState(MathMacro.Decimal2BinaryTab(conf, len));
	}


	/** Applies the rule with the binary coded transi transition **/
	public int ECAStep(int transi, int[] rule){
		int k = 0;
		// decodes transi
		int[] trans_bin = MathMacro.Decimal2BinaryTab(transi, n);
		for(int i = 0; i < n; i++){
			if(trans_bin[i] > 0){
				//transition
				next_state[i] = rule[4*GetState(i-1) + 2 * GetState(i) + GetState(i+1)];
				k++;
			}
			// no transition
			else next_state[i] = state[i];
		}
		// Swap buffers
		int[] temp = state;
		state = next_state;
		next_state = temp;
		return k; // The number of times we applied the rule (number of 1s in trans_bin)
	}


	public void Print(){
		for(int i = 0; i < length;i++)
			Macro.printNOCR(""+ GetState(i) + " ");
		Macro.CR();
	}

	public int GetState(int i){
		if(i == -1) return border_left;
		if(i == n) return border_right;
		return state[i];
	}
	/*--------------------
	 * test methods
	--------------------*/

	public static void DoTest(){
		Macro.BeginTest("Configuration");
		Configuration ma_conf = new Configuration();
		int[] state = {0,1,0,1,0,0};
		ma_conf.SetState(state);	ma_conf.Print();
		ma_conf.ECAStep(1, MathMacro.Decimal2BinaryTab(18,8));
		ma_conf.Print();
		Macro.EndTest();
	}

}
