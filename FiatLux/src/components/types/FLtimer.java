package components.types;

public class FLtimer implements Monitor {

	int m_time;
	
	public void sig_Reset(){
		m_time=0;
	}
	
	public void sig_NextStep(){
		m_time++;
	}
	
	public int GetTime(){
		return m_time;
	}

	@Override
	public void ReceiveSignal(FLsignal sig) {
		switch(sig){
		case init:
			sig_Reset();
			break;
		case nextStep:
			sig_NextStep();
			break;
		case update:
			break;
		}
		
	}
	
	
	public String toString(){
		return "time: "+ m_time;
	}
	
}
