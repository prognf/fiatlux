package components.types;

import grafix.gfxTypes.controllers.FLCheckBox;

/** holds a boolean and provides a display that can control it 
 * the display may or may not exist*/
public class BooleanPar {

	public String m_label;
	private boolean m_val;
	FLCheckBox m_controller;


	/** label associated to the parameter */ 
	public BooleanPar(String label, boolean def_value){
		m_label= label;
		m_val = def_value;
		m_controller= null;
	}

	public void SetVal(boolean val){
		m_val=val;
		if (m_controller!=null){
			UpdateDisplay();
		}
	}

	private void UpdateDisplay() {
		m_controller.setSelected(m_val);
	}


	public boolean GetVal(){
		return m_val;
	}

	public FLCheckBox GetControl(){
		if (m_controller==null){
			m_controller= new BasicParController();
		}
		return m_controller;
	}



	class BasicParController extends FLCheckBox {

		public BasicParController() {
			super(m_label);
			setSelected(m_val);
		}

		@Override
		protected void SelectionOn() {
			m_val= true;
		}

		@Override
		protected void SelectionOff() {
			m_val= false;
		}


	}


}
