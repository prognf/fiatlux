package components.types;

import main.Macro;

/**********************************************
 * The class SVector encapsulates 
 * an array of Doubles of arbitrary size
 * @author Nazim Fates
 *******************************************/ 


public class SVector
{
	
	final static String SIZE_TAG="sz", COORD_TAG="c";
	
	/*--------------------
	 * Attributes
	--------------------*/
	
	int m_size;
	String [] m_array;
	
	/*--------------------
	 * Constructor
	--------------------*/
	
	public SVector(int size){
		m_size = size;
		m_array= new String[size];
	}
	
	
	/*--------------------
	 * Get / Set
	--------------------*/
	
	final public void SetVal(int pos, String value){
		m_array[pos]= value;
	}
	
	final public void SetVal(int pos, double value){
		m_array[pos]= "" + value;
	}
	
	public String GetVal(int pos){
		return m_array[pos];
	}
	
	/*--------------------
	 * others
	--------------------*/
	
	// simple concatenation of the args
	public String ToLine(){
		StringBuilder s= new StringBuilder();
		for (int i=0; i< m_size; i++){
			s.append(GetVal(i)).append("  ");
		}
		return s.toString();
	}
	
	/** for tex arrays **/
	public String ToTex(){
		StringBuilder s= new StringBuilder(GetVal(0));
		for (int i=1; i< m_size; i++){
			s.append(" & ").append(GetVal(i));
		}
		s.append(" \\" + "\\"); // equivalent to \\
		return s.toString();
	}
	
	/* convets to as tring with whitespace separator */
	public String ToString(){
		StringBuilder s= new StringBuilder();
		for (int i=0; i< m_size; i++){
			s.append(GetVal(i)).append(FLString.WHTSPC);
		}
		return s.toString();
	}
	
	
	
	
	public static void DoTest(){
		Macro.BeginTest("SVector");
		SVector v1= new SVector(4);
		v1.SetVal(0, "alpha" );
		v1.SetVal(1, "! BETA !");
		v1.SetVal(2, "Gamma");
		v1.SetVal(3, "hong hong");
		
		Macro.EndTest();
	}
	
	
}
