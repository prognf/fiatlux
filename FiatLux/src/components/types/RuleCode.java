package components.types;

import java.math.BigInteger;
import java.util.ArrayList;

import main.MathMacro;
import models.CAmodels.tabled.ECAlookUpTable;
import models.CAmodels.tabled.LookUpTableBinary;

public class RuleCode implements Comparable<RuleCode>{

	public static final RuleCode NULLRULE = new RuleCode(0);

	public static final RuleCode DEFAULTECACODE = new RuleCode(30);
	
	private Long m_Wcode;

	/** cons. from long value, standard case **/
	public RuleCode(long W) {
		m_Wcode= W;
	}

	public static RuleCode FromInt(int ruleNum) {
		return new RuleCode((long) ruleNum);
	}

	/** cons. from string **/
	public static RuleCode FromString(String sParsed) {
		long W= FLString.String2Long(sParsed);
		return new RuleCode(W);
	}

	public static RuleCode FromECAtcode(String Tcode) {
		return ECAlookUpTable.TcodeToWcode(Tcode);
	}

	public long GetCodeAsLong() {
		return m_Wcode;
	}
	
	/** says if current code is smaller than "bound" **/
	public boolean IsInBounds(long bound) {
		return m_Wcode<bound ;
	}
	
	public String toString(){
		return "" + m_Wcode;
	}
	
	/** specific for ECAs **/
	public String toTcode() {
		return ECAlookUpTable.GetTransitionCodePadded(this);
	}
	
	
	public int[] ToBinaryArray(int tabsize) {
		return MathMacro.Decimal2BinaryTab(m_Wcode, tabsize);
	}
	
	/** for sorting operations */
	@Override
	public int compareTo(RuleCode otherW) {
		return m_Wcode.compareTo(otherW.m_Wcode);
	}
	
	/** symmetric rules by exchanging L/R, O/1 and both operations 
	 * K : is neighbsize (3 for ECA)**/
	static public ArrayList<RuleCode> GetSymmetrics(RuleCode W, int K){
		int sz= MathMacro.TwoToPowerIntResult(K);
		LookUpTableBinary lookUpTable= new LookUpTableBinary(sz);
		lookUpTable.SetRule(W);
		ArrayList<RuleCode> symmetrics= new ArrayList<RuleCode>();
		int [] LRruleBit = new int [sz];
		int [] ZOruleBit = new int [sz];
		int [] CRruleBit = new int [sz];
		
		for (int bitpos=0; bitpos < sz ; bitpos++){
			int res= lookUpTable.getBitTable(bitpos);
			int symBitRefl= (int)GetSymBitPosLR(bitpos,K); // reversal of neighb here !! TODO: check CAST
			int symBitConj= sz - 1 - bitpos;
			int symBitReCo= sz -1 - symBitRefl;
			//Macro.fDebug(" %d SYM-> %d ", bitpos, symBitConj);
			LRruleBit[symBitRefl]= res; //
			ZOruleBit[symBitConj]= 1 - res; 
			CRruleBit[symBitReCo]= 1 - res; 
			
		}
		RuleCode 
			Wrefl= MathMacro.BinaryTab2RuleCode(LRruleBit),
			Wconj= MathMacro.BinaryTab2RuleCode(ZOruleBit),
			Wreco= MathMacro.BinaryTab2RuleCode(CRruleBit);
		symmetrics.add( W );
		symmetrics.add( Wrefl);
		symmetrics.add( Wconj);
		symmetrics.add( Wreco);
		return symmetrics;
	}
	
	/** symmetric rules by exchanging L/R, O/1 and both operations 
	 * K : is neighbsize (3 for ECA)**/
	static public String GetStrBigIntSymmetrics(RuleCode W, int K){
		int sz= MathMacro.TwoToPowerIntResult(K);
		LookUpTableBinary lookUpTable= new LookUpTableBinary(sz);
		lookUpTable.SetRule(W);
		int [] LRruleBit = new int [sz];
		int [] ZOruleBit = new int [sz];
		int [] CRruleBit = new int [sz];
		
		for (int bitpos=0; bitpos < sz ; bitpos++){
			int res= lookUpTable.getBitTable(bitpos);
			int symBitRefl= (int)GetSymBitPosLR(bitpos,K); // reversal of neighb here !! TODO: check CAST
			int symBitConj= sz - 1 - bitpos;
			int symBitReCo= sz -1 - symBitRefl;
			//Macro.fDebug(" %d SYM-> %d ", bitpos, symBitConj);
			LRruleBit[symBitRefl]= res; //
			ZOruleBit[symBitConj]= 1 - res; 
			CRruleBit[symBitReCo]= 1 - res; 
			
		}
		BigInteger
			Wrefl= MathMacro.BinaryTab2BigInt(LRruleBit),
			Wconj= MathMacro.BinaryTab2BigInt(ZOruleBit),
			Wreco= MathMacro.BinaryTab2BigInt(CRruleBit);
		String str="";
		str+= W +":";
		str+=  Wrefl +":";
		str+= Wconj +":";
		str+= Wreco +":";
		return str;
	}
	
	/** from transition i=Tr(x,y,z) to transition i'=Tr(1-x,1-y,1-z)
	 */
	private int GetSymBitPosZO(int bitpos, int neighbsize) {
		// decomposing bit pos in binary
		int [] bit = MathMacro.Decimal2BinaryTab(bitpos, neighbsize);
		int [] newbit = new int [neighbsize];
		// building new code
		for (int i=0; i < neighbsize; i++){
			newbit[i]= 1-bit[i];
		}
		return (int)MathMacro.BinaryTab2Decimal(newbit);
	}

	/** from transition i=Tr(x,y,z) to transition i'=Tr(z,y,x)
	 */
	static private int GetSymBitPosLR(int bitpos, int neighbsize) {
		// decomposing bit pos in binary
		int [] bit = MathMacro.Decimal2BinaryTab(bitpos, neighbsize);
		int [] newbit = new int [neighbsize];
		// building new code
		for (int i=0; i < neighbsize; i++){
			newbit[i]= bit[neighbsize - 1 - i];
		}
		int newindex= (int)MathMacro.BinaryTab2Decimal(newbit);
		//Macro.fDebug("oldinex:%d newindex:%d",bitpos,newindex);
		return newindex;
	}


	/** canonical representation of four symmetrics with minimal rep. first **/
	static public ArrayList<RuleCode> GetCanonicalSymmetrics(RuleCode W, int K){
		ArrayList<RuleCode> family= GetSymmetrics(W, K);
		RuleCode minrep= (RuleCode) MathMacro.FindMin(family);
		return GetSymmetrics(minrep, K);
	}
	
	/** canonical representation of four symmetrics with minimal rep. first **/
	public String GetCanonicalSymmetricsAsString(int K){
		ArrayList<RuleCode> family= GetCanonicalSymmetrics(this, K);
		String str= String.format("(%s,%s,%s,%s)",
				family.get(0), family.get(1), family.get(2), family.get(3));
		return str;
	}

	
}
