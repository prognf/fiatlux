package components.types;

import components.randomNumbers.FLRandomGenerator;
import components.randomNumbers.StochasticSource;
import main.Macro;

/** TODO : add control in setVal **/
public class ProbaPar extends DoublePar {

	public ProbaPar(String label, double def_value) {
		super(label, def_value);
		if ((def_value<0.) || (def_value>1.)){
			Macro.SystemWarning(" proba initialised with value not in [0,1] ");
		}
	}
	
	public String toString(){
		return ""+m_val;
	}
 
	/**
	 * realises a Bernoulli law whose parameter is the value held by the object
	 */
	public boolean Bernoulli(StochasticSource rnd) {
		return Bernoulli(rnd.GetRandomizer());
	}

	/**
	 * realises a Bernoulli law whose parameter is the value held by the object
	 */
	public boolean Bernoulli(FLRandomGenerator rndgen) {
		return (rndgen.RandomDouble()< GetVal());
	}
}
