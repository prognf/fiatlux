package components.types;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import main.Macro;

/** don't forget to close after use */
public class FLfileReader extends BufferedReader {

	private FLfileReader(String filename) throws FileNotFoundException {
		super(new InputStreamReader(
				new FileInputStream(filename)));
		m_filename= filename;
	}

	String m_filename="noname";

	
	/* special for reading resources in project **/
	public FLfileReader(InputStream is) {
		super(new InputStreamReader(is));
	}
	
	
	/** public constructor **/
	public static FLfileReader open(String filename) {
		try {
			Macro.print(4, "reading:"+filename);
			return new FLfileReader(filename);
		} catch (IOException e) {
			Macro.FatalError(e,"could not open:"+ filename);
			return null;
		}
	}
	

	public String readLn() {
		String s=null;
		try {
			s = readLine();
		} catch (IOException e) {
			Macro.FatalError(e,"error when reading file:"+ m_filename);
		}
		return s;
	}


	public void Close() {
		try {
			super.close();
		} catch (IOException e) {
			Macro.FatalError(e,"error when closing file:"+ m_filename);
		}
	}


	public static String openFileAsOneLine(String filename) {
		FLfileReader read= open(filename);
		String oneline= read.readLn();
		read.Close();
		return oneline;
	}
	
	
}
