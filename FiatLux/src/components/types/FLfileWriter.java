package components.types;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Set;

import main.Macro;

public class FLfileWriter extends BufferedWriter {

	String m_filename="noname";
	public int m_nlines=0;

	private FLfileWriter(String filename, boolean appendmode) throws IOException {
		super(new FileWriter(filename, appendmode));
		m_filename= filename;
	}

	private FLfileWriter(String filename) throws IOException {
		this(filename, false);
	}
	
	/** public constructor **/
	public static FLfileWriter open(String filename) {
		return open(filename,false);
	}

	/** public constructor **/
	public static FLfileWriter open(String filename, boolean appendMode) {
		try {
			return new FLfileWriter(filename, appendMode);
		} catch (IOException e) {
			Macro.FatalError(e,"could not open:"+ filename);
			return null;
		}
	}
	

	/** main function to write strings **/
	public void addline(String  outputline) {
		try {
			//Macro.Debug("writing line:"+outputline);
			this.write(outputline+"\n");
			m_nlines++;
		} catch (IOException e) {
			e.printStackTrace();
			Macro.SystemWarning("error when writing to file:"+ m_filename + " line:" + outputline);
		}
	}
	
	public void wrtlineFlush(String outputline) {
		addline(outputline);
		try {
			flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void wrtlineNOCR(String outputline) {
		try {
			this.write(outputline);
		} catch (IOException e) {
			e.printStackTrace();
			Macro.SystemWarning("error when writing to file:"+ m_filename + " line:" + outputline);
		}
	}
	
	public void CR() {
		try {
			this.write("\n");
			m_nlines++;
		} catch (IOException e) {
			e.printStackTrace();
			Macro.SystemWarning("error when writing to file:"+ m_filename);
		}
	}
	
	/** appends one line to a file **/
	public static void appendToFile(String filename, String sline) {
		FLfileWriter f= FLfileWriter.open(filename, true);
		f.addline(sline);
		f.Close();
		
	}
	
	public String getFileName() {
		return m_filename;
	}

	

	public void Close() {
		// we close
		try {
			close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Macro.print(4, "Wrote : " + m_filename + " (nlines:" + m_nlines + ")");
	}

	/** writes a file with only one line **/
	public static void WriteOneLine(String filename, String line) {
		FLfileWriter f= FLfileWriter.open(filename, false);
		f.addline(line);
		f.Close();
	}

	
	/** writes a set of printable elements in a file **/
	public static <Z extends Object> void writeSet(String filename, Set<Z> objectset) {
		FLfileWriter outfile= FLfileWriter.open(filename);
		for (Object obj : objectset) {
			outfile.addline(obj.toString());
		}
		outfile.Close();
	}

	

	
}
