package components.types;

import grafix.gfxTypes.controllers.DoubleController;

/** control of  a parameter  of type double */
public class DoublePar {

	public String m_label;
	public double m_val;
	private ParController m_controller;
	
	
	/** label associated to the parameter + default value */ 
	public DoublePar(String label, double def_value){
		m_label= label;
		m_val = def_value;
	}
	
	public void SetVal(double val){
		m_val=val;
		
		if(m_controller != null){
			m_controller.Update();
		}
		
	}
	
	final public double GetVal(){
		return m_val;
	}
	
	public DoubleController GetControl(){
		if(m_controller == null){
			m_controller = new ParController();
		}
		
		return this.m_controller;
	}
	
	class ParController extends DoubleController {

		public ParController() {
			super(m_label);
		}

		@Override
		protected double ReadDouble() {
			return m_val;
		}

		@Override
		protected void SetDouble(double val) {
			m_val= val;		
		}
		
	}
	
	
}
