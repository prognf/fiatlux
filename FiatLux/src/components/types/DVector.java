package components.types;

import main.Macro;

/*--------------------
 * encapsulates an array of doubles of arbitrary size
 * 
 * @author Nazim Fates
*--------------------*/

public class DVector {

	final static String SIZE_TAG = "sz", COORD_TAG = "c";

	/*--------------------
	 * Attributes
	 --------------------*/

	double[] m_val;

	/*--------------------
	 * Constructor
	 --------------------*/

	public DVector(int size) {
		m_val = new double[size];
	}
	
	public DVector (double ...dval){
		m_val = dval;
	}

	/* XML */
/*	public DVector(String in_str) {
		try {
			String [] data = in_str.split(" ");
			m_size = data.length;
			m_val = new double[m_size];
			for (int i = 0; i < m_size; i++) {
				double dval = Double.parseDouble(data[i]); 
				SetVal(i, dval);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Macro.print(" DVector construction failed with input: "	+ in_str);
			Macro.ExitSystem(-1);
		}
	}*/

	/*--------------------
	 * Get / Set
	 --------------------*/

	public int GetSize() {
		return m_val.length;
	}

	final public void SetVal(int pos, double value) {
		m_val[pos] = value;
	}
	
	final public void AddVal(int pos, double value) {
		m_val[pos] += value;
	}
	
	final public void FactorVal(int pos, double value) {
		m_val[pos] *= value;
	}

	public double GetVal(int pos) {
		return m_val[pos];
	}

	/** adds a new val at the end of the array (re-creates the array) * */
	/*public void AddLastVal(double in_value) {
		double[] temp = new double[m_size + 1];
		for (int i = 0; i < m_size; i++) {
			temp[i] = m_val[i];
		}
		temp[m_size] = in_value;
		m_val = temp;
		m_size++;
	}*/

	/*--------------------
	 * calculus
	 --------------------*/
	

	/*--------------------
	 * others
	 --------------------*/

	public String ToStringWithPrecision(int precision) {
		StringBuilder output = new StringBuilder();
		for (double dval : m_val) {
			 output.append(FLString.DoublePrecisionN(dval, precision)).append(" ");
		}
		return output.toString();
	}
	
	/* string output with space separators */
	public String ToString() {
		StringBuilder output = new StringBuilder();
		for (double dval : m_val) {
			output.append(dval).append(" ");
		}
		return output.toString();
	}
	
	/* OLD public String ToXML() {
		String output = Macro.GetTaggedInt(m_size, SIZE_TAG) + "  ";
		for (int i = 0; i < m_size; i++) {
			String sval = COORD_TAG + i;
			output += Macro.GetTaggedDouble(GetVal(i), sval) + "  ";
		}
		return output;
	}*/

	public static void DoTest() {
		Macro.BeginTest("DVector");
		DVector v1 = new DVector(4);
		v1.SetVal(0, -4.0);
		v1.SetVal(1, -98887.9);
		v1.SetVal(2, -0.0000000023);
		v1.SetVal(3, 45.899494949);

		String test = "-4.0 -98887.9 -2.3E-9 45.899494";
		Macro.print("You should see :" + test);


		Macro.EndTest();
	}

	

}
