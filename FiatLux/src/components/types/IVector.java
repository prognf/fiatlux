package components.types;

import main.Macro;


public class IVector {

	/*--------------------
	 * Attributes
	 --------------------*/

	int m_size;
	int[] m_val;

	public IVector(int [] initval){
		m_size = initval.length;
		m_val = initval;
	}


	public IVector(int size) {
		//m_size=3; WHAAAAAAAAAT ???
		m_size = size;
		m_val = new int[m_size];
	}


	/*--------------------
	 * Get / Set
	 --------------------*/



	final public void SetVal(int pos, int value) {
		m_val[pos] = value;
	}

	public int GetVal(int pos) {
		return m_val[pos];
	}

	public int GetSize() {
		return m_val.length;
	}

	final public void SetValues(int [] value){
		if (m_size != value.length ){
			Macro.FatalError("SetValues called with wrong size :" + 
					value.length + " expected : " + m_size);		
		}
		for (int i=0; i < m_size; i++){
			SetVal(i, value[i]);
		}
	}

	/*--------------------
	 * others
	 --------------------*/

	/* string output with space separators */
	public String toString() {
		StringBuilder output = new StringBuilder();
		for (int i = 0; i < m_size; i++) {
			output.append(GetVal(i)).append("  ");
		}
		return output.toString();
	}

	final static String ClassName= Thread.currentThread().getStackTrace()[1].getClassName();
	
	/**
	 * TEST 
	 */
	public static void main(String[] argv) {
		Macro.BeginTest(ClassName);
		int val [] = { 11, 20, -30, 50};
		IVector v1 = new IVector(val);
		Macro.print(v1.toString());
		v1.SetVal(0, 4);
		v1.SetVal(1, -3);
		//v1.SetVal(2, 2);
		v1.SetVal(3, 5);
		Macro.print(v1.toString());			
		
		Macro.EndTest();
	}


	public void IncrementPos(int pos) {
		m_val[pos]++;		
	}

	public void DecrementPos(int pos) {
		m_val[pos]--;		
	}


	public void AddVal(int pos, int val) {
		m_val[pos]+= val;		
	}
}
