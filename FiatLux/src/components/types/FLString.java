package components.types;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import main.Macro;
import main.MathMacro;
import models.CAmodelsvar.aperiodicTiling.AperiodicTilingSystem;

/*
 * This class is only for string static methods
 * 
 */
final public class FLString {


	// CONSTANTS
	public final static  String EMPTYSTRING = "";
	public final static  String WHTSPC= " ";
	// for interpretation of real numbers in file names & codes
	public final static char DOTSEPARATOR='.';
	public final static char NEWSEPARATOR='p';

	//--------------------------------------------------------------------------
	//- basic parsing methods
	//--------------------------------------------------------------------------


	final public static int String2Int(String s) {
		return Integer.valueOf(s);
	}

	final public static long String2Long(String s) {
		return Long.valueOf(s);
	}

	final public static double String2Double(String s) {
		return Double.valueOf(s);
	}

	final public static float String2Float(String s) {
		return Float.valueOf(s);
	}

	/** parses three int in a string, returns a vector of size 3 **/
	public static int [] ParseThreeNumbers(String strParsed) {
		int [] out= new int[3];
		//String strNeighb="(11,-222bbb-333 444)";
		Pattern p = Pattern.compile("[^\\d-]*([-?\\d]+)[^\\d-]+([-?\\d]+)[^\\d-]+([-?\\d]+)[^\\d-]*");
		Matcher m = p.matcher(strParsed);
		if (m.find()){		
			String a= m.group(1), b= m.group(2), c= m.group(3);
			out[0]= FLString.String2Int(a);
			out[1]= FLString.String2Int(b);
			out[2]= FLString.String2Int(c);
			//Macro.fDebug("%s|%s|%s", a,b,c);
			return out;
		} else {
			Macro.SystemWarning("Failed to parse :" + strParsed);
			return null;
		}
	}

	/** parses k integers in a string, returns a vector of size k 
	 * we assume k>1 **/
	public static int [] parse_k_numbers(int k, String strParsed) {
		assert(k>1);
		int [] out= new int[k];
		String nonDecimalOpt="[^\\d-]*", nonDecimal="[^\\d-]+",decimalGroup="([-?\\d]+)";
		String strPattern= nonDecimalOpt;
		for (int i=0; i<k; i++) {
			// last non-decimal separation is optional
			strPattern += decimalGroup + ( (i==k-1)?nonDecimalOpt:nonDecimal );
		}
		Pattern p = Pattern.compile(strPattern);
		Matcher m = p.matcher(strParsed);
		if (m.find()){
			for (int i=0; i<k; i++) {
				String r= m.group(i+1); // BEWARE the index!
				out[i]= FLString.String2Int(r);
			}
		} else {
			Macro.SystemWarning("Failed to parse :" + strParsed);
			return null;
		}

		return out;
	}


	/** parses three a string which contains int **/
	public static IntegerList ParseNumbers(String strParsed) {
		IntegerList out= new IntegerList(); 

		Pattern p = Pattern.compile("[^\\d-]*([-?\\d]+)");
		Matcher m = p.matcher(strParsed);

		while (m.find()){		
			String a= m.group(1);
			int val= FLString.String2Int(a);
			//Macro.fDebug("val read:%d", val);
			out.addVal(val);
		} 
		return out;
	}


	//--------------------------------------------------------------------------
	//-  one-block parsing methods
	//--------------------------------------------------------------------------

	/** one-block string to parse with a tag */
	public static String SParseWithControl(String in_str, String in_tag) {
		String s2 = CheckAndRemoveTag(in_str,in_tag);
		if (s2 == null){
			FLString.FailedParsingError("string", in_str, in_tag);
			return "PARSE_ERROR";
		}
		return s2;
	}

	/** one-block string to parse with a tag */
	public static char CParseWithControl(String in_str, String in_tag) {
		String s2 = CheckAndRemoveTag(in_str,in_tag);
		if (s2 == null){
			FLString.FailedParsingError("char", in_str, in_tag);
			return '?';
		}
		return s2.charAt(0);
	}

	/** one-block string to parse with a tag */
	public static double DParseWithControl(String in_str, String in_tag) {
		try {
			String s2 = CheckAndRemoveTag(in_str,in_tag);
			return String2Double(s2);
		} catch (Exception e) {
			e.printStackTrace();
			FLString.FailedParsingError("Double", in_str, in_tag);
			return 0;
		}
	}

	/** one-block string to parse with a tag */
	public static int IParseWithControl(String in_str, String in_tag) {
		try {
			String s2 = CheckAndRemoveTag(in_str,in_tag);
			return String2Int(s2);
		} catch (Exception e) {
			e.printStackTrace();
			FLString.FailedParsingError("Integer", in_str, in_tag);
			return 0;
		}
	}

	/** one-block string to parse with a tag */
	public static long LParseWithControl(String in_str, String in_tag) {
		try {
			String s2 = CheckAndRemoveTag(in_str,in_tag);
			return String2Long(s2);
		} catch (Exception e) {
			e.printStackTrace();
			FLString.FailedParsingError("Integer", in_str, in_tag);
			return 0;
		}
	}

	/**
	 * returns null if the string does not contain inTag
	 */
	private static String CheckAndRemoveTag(String toParse, String parsingTag) {
		int len = parsingTag.length();
		int tagBeginIndex= toParse.indexOf(parsingTag);
		int indexEndTag= tagBeginIndex + len;
		String s = toParse.substring(tagBeginIndex, indexEndTag);
		if (s.equals(parsingTag) ){
			int indexEndParsing=toParse.indexOf(" ", indexEndTag + 1); // find next blank space
			if (indexEndParsing==-1){
				indexEndParsing= toParse.length();
			}
			return 	toParse.substring(indexEndTag, indexEndParsing);
		} else {			
			return null; // extracting tag failed
		}
	}


	//--------------------------------------------------------------------------
	//-  parsing methods
	//--------------------------------------------------------------------------


	/** parses a string */
	public static String SParseInString(String in_str, String in_tag) {
		try {
			return GetTokenInString(in_str, in_tag);

		} catch (Exception e) {
			e.printStackTrace();
			FLString.FailedParsingError("String", in_str, in_tag);
			return null;
		}
	}

	/** parses a string */
	public static int IParseInString(String in_str, String in_tag) {	 
		try {
			String parsed = GetTokenInString(in_str, in_tag);
			return  String2Int(parsed);

		} catch (Exception e) {
			e.printStackTrace();
			FLString.FailedParsingError("Integer", in_str, in_tag);
			return Integer.MIN_VALUE;
		}
	}

	/** parses a string */
	public static double DParseInString(String in_str, String in_tag) {	 
		try {
			String parsed = GetTokenInString(in_str, in_tag);
			return  String2Double(parsed);

		} catch (Exception e) {
			e.printStackTrace();
			FLString.FailedParsingError("Double", in_str, in_tag);
			return Double.NEGATIVE_INFINITY;
		}
	}

	/** finds a particular "token"= tag string **/
	static private String GetTokenInString(String in_str, String in_tag) {
		String s_result = null;
		int beg = in_str.indexOf(in_tag);
		int lenstr = in_str.length(), lentag = in_tag.length();
		// beginning of string
		if (beg < 0) {
			Macro.print(in_tag + " was not found in " + in_str);
			FLString.FailedParsingError("???", in_str, in_tag);
		}
		// end of string
		int end = in_str.indexOf(" ",beg+lentag);
		if (end < 0) {
			end = lenstr;
		}
		s_result = in_str.substring(beg+lentag, end);

		return s_result;
	}

	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------

	/** completes with zeros **/
	static public String FormatIntWithZeros(int x, int nchar) {
		StringBuilder format= new StringBuilder();
		for (int i=0; i < nchar; i++){
			format.append("0");
		}
		java.text.DecimalFormat df = new java.text.DecimalFormat(format.toString());
		df.getDecimalFormatSymbols().setDecimalSeparator(' ');
		return df.format(x);
	}

	/** formats number with a given length (here 3) **/  
	static public String Decimal2String(long x) {
		java.text.DecimalFormat df = new java.text.DecimalFormat("000");
		df.getDecimalFormatSymbols().setDecimalSeparator(' ');
		return df.format(x);
	}

	/** formats number with a given length (here 3) **/  
	static public String Decimal2String(long x, int formatlen) {
		String nzeros= multiplyChar('0', formatlen);
		java.text.DecimalFormat df = new java.text.DecimalFormat(nzeros);
		df.getDecimalFormatSymbols().setDecimalSeparator(' ');
		return df.format(x);
	}

	/** returns a String formed of n times char c **/
	static public String multiplyChar(char c, int n) {
		StringBuilder out= new StringBuilder();
		for (int i=0; i<n;i++) {
			out.append(c);
		}
		return out.toString();
	}


	/** formatting */
	static public String Decimal2String(double x) {
		java.text.DecimalFormat df = new java.text.DecimalFormat("#####00.00");
		//df.getDecimalFormatSymbols().setDecimalSeparator('.');
		DecimalFormatSymbols dfs = df.getDecimalFormatSymbols();
		dfs.setDecimalSeparator('.');
		df.setDecimalFormatSymbols(dfs);
		String s = df.format(x);
		s = s.replace(',', '.');
		return s;
	}

	static public String Double2String(double f) {
		java.text.DecimalFormat df = new java.text.DecimalFormat("####.####");
		DecimalFormatSymbols dfs = df.getDecimalFormatSymbols();
		dfs.setDecimalSeparator('.');
		df.setDecimalFormatSymbols(dfs);
		String s = df.format(f);
		s = s.replace('.', 'p');
		return s;

	}

	/* returns a string with n digits */
	static public String DoublePrecisionN(double x, int n) {
		StringBuilder formatstring = new StringBuilder("##.");
		for (int i = 0; i < n; i++) {
			formatstring.append("0");
		}
		java.text.DecimalFormat df = new java.text.DecimalFormat(formatstring.toString());
		df.getDecimalFormatSymbols().setDecimalSeparator('.');
		String s = df.format(x);
		s = s.replace(',', '.');
		return s;
	}

	/* returns a string with three digits */
	static public String DoublePrecision3(double x) {
		java.text.DecimalFormat df = new java.text.DecimalFormat("##.000");
		df.getDecimalFormatSymbols().setDecimalSeparator('.');
		String s = df.format(x);
		s = s.replace(',', '.');
		return s;
	}

	static public String DoublePrecision4(double x) {
		java.text.DecimalFormat df = new java.text.DecimalFormat("##.0000");
		df.getDecimalFormatSymbols().setDecimalSeparator('.');
		String s = df.format(x);
		s = s.replace(',', '.');
		return s;
	}

	/*--------------------
	 * parsing methods
	 --------------------*/

	/** name of the calling object (e.g., buttonname), input : string with error */  
	public static void ParsingError(String FieldName, String input) {
		Macro.print("button:" + FieldName + " parse error :" + input);		
	}

	/** parses int */
	final public static int ParseInt(String in_st) {
		return Integer.parseInt(in_st);
	}

	/** parses long */
	final public static long ParseLong(String in_st) {
		return Long.parseLong(in_st);
	}

	/** parses double */
	final public static double ParseDouble(String in_st) {
		return Double.parseDouble(in_st);
	}

	/** parses double */
	final public static float ParseFloat(String in_st) {
		return Float.parseFloat(in_st);
	}

	/** input 0.123 ; output "Op123" */
	public static String ReplaceDotByLetterP(String realvalue) {
		return realvalue.replace(FLString.DOTSEPARATOR,FLString.NEWSEPARATOR);		
	}

	public static String ReplaceDotByLetterP(double val) {
		return ReplaceDotByLetterP(""+val);
	}


	public static String FormatNumericLine(Number ... valArg) {
		StringBuilder out= new StringBuilder();
		for (Object val : valArg){
			out.append(val).append(" ");
		}
		return out.toString();
	}

	/** time difference in ms made readable */
	public static String TimeFormat(java.util.Date date1, java.util.Date date0) {
		long dt= date1.getTime() - date0.getTime();
		long ms = dt % 1000;		dt = dt / 1000 ;
		long sec = dt % 60;      dt = dt / 60;
		long min = dt % 60 ;      dt = dt / 60;
		long  hour = dt ;

		String output="";
		if (hour>0){
			output +=  hour + " h. ";
		} 
		if (min>0){
			output += min + " min. ";
		}
		if(sec>10){
			output += sec + " s. "; 
		} else {
			if (sec>0){
				output += sec + " s. "; 
			}
			output += ms + " ms."; 
		}
		return output;

	}

	final public static BigDecimal String2BigDecimal(String s) {
		/*double dval= String2Double(s);    
		String s2 = DEFAULT_DECIMAL_FORMAT.format(dval);
		BigDecimal bd = new BigDecimal (s2);*/
		BigDecimal bd= new BigDecimal(s);
		return bd;
	}

	public static void FailedParsingError(String ExpectedType, String parsed,
			String key) {
		Macro.FatalError(ExpectedType + " parsing failed with input :<"
				+ parsed + "> expected key :<" + key + ">");
	}

	/*--------------------
	 * formatting / parsing methods
	 --------------------*/



	public static final NumberFormat DEFAULT_DECIMAL_FORMAT =
			new DecimalFormat ("#.0#################");



	/**
	 * Remove all tags from a string, where the string is parsed as *SEP+TAG+TAGSEP+VALUE+SEP...*.
	 * @param toParse the String to parse
	 * @param sep		the separator between tags (eg " ")
	 * @param tagsep	the separator between the tag and its value (eg "=")
	 * @return
	 */
	public static String RemoveAllTags(String toParse, String sep, String tagsep) {
		StringBuilder res = new StringBuilder();
		String[] tags = toParse.split(sep);

		for (String tag : tags) {
			if(tag.contains(tagsep)) 
				res.append(tag.split(tagsep)[1]).append(" ");
		}
		return res.toString();
	}

	public static BigDecimal BIParseWithControl(String in_str, String in_tag) {
		try {
			String s2 = CheckAndRemoveTag(in_str,in_tag);
			return String2BigDecimal(s2);
		} catch (Exception e) {
			e.printStackTrace();
			FailedParsingError("BigInteger", in_str, in_tag);
			return null;
		}
	}

	/*--------------------
	 * removes the extension of a file if ../ is present a the beginning of the
	 * string, there is a risk of confusion
	 --------------------*/
	static public String RemoveExtension(String s) {
		int point_pos = s.lastIndexOf(FLString.STR_POINT);
		int slash_pos = s.lastIndexOf(FLString.STR_SLASH);
		if ((point_pos > 0) && (slash_pos < point_pos)) {
			return s.substring(0, point_pos);
		} else {
			return s;
		}
	}

	/** ******************* System procedures ************** */
	public final static String STR_POINT = ".";
	public final static String STR_SLASH = "/";

	/*--------------------
	 * string methods
	 --------------------*/


	/* finds a string in an array
	 * convention -1 if not found */
	public static int FindRankStringList(String target, String [] data){
		int rank=0;
		while ((rank < data.length) && (!data[rank].equals(target)) ){
			rank++;			
		}
		if (rank==data.length) rank= Macro.NOTFOUND;
		return rank;
	}

	/* association of strings according to arrays 
	 * this method searches a key in the first array
	 * then looks the index found 
	 * and returns the element of the second array at this index*/
	public static String FindAssociatedStringArray(
			String key, 
			String [] input_array, String [] output_array){
		assert (input_array.length==output_array.length);
		Macro.fDebug(" compare %d and %d",input_array.length, output_array.length);
		int index= FindRankStringList(key, input_array);
		Macro.fDebug(" key %s in array %s ?",key, FLString.toString(input_array));
		
		return output_array[index];
	}


	/** converting an array list to a string : no separator **/
	public static String ToStringNoSep(ArrayList<?> dataArray) {
		StringBuilder s= new StringBuilder();
		for (Object r : dataArray) {
			s.append(r);
		}
		s.append("");
		return s.toString();
	}

	/** converting an array list to a string : with separator **/
	public static String ToStringSep(ArrayList<?> dataArray, String separator) {
		StringBuilder s= new StringBuilder();
		for (Object r : dataArray) {
			s.append(r+ separator);
		}
		s.append("");
		return s.toString();
	}

	/** converting an array list to a string **/
	public static String ToStringWithCR(ArrayList<?> dataArray) {
		StringBuilder s= new StringBuilder("--start List of size : " + dataArray.size() + "-- \n");
		for (Object r : dataArray) {
			s.append(r).append("\n");
		}
		s.append("--end List-- \n");
		return s.toString();
	}


	/** from array to one string **/
	public static String ToString(int [] dataArray) {
		StringBuilder s= new StringBuilder("[");
		for (Object val : dataArray) {
			s.append(val).append(":");
		}
		s.append("]");
		return s.toString();
	}

	/** from array to one string **/
	public static String ToString(double [] dataArray) {
		StringBuilder s= new StringBuilder("[");
		for (Object val : dataArray) {
			s.append(val).append(":");
		}
		s.append("]");
		return s.toString();
	}


	/** from array to one string **/
	public static String toString(String [] dataArray) {
		StringBuilder s= new StringBuilder("[");
		for (Object val : dataArray) {
			s.append(val).append(":");
		}
		s.append("]");
		return s.toString();
	}
	
	/** from array to one string **/
	public static String ToString(Object [] dataArray) {
		StringBuilder s= new StringBuilder();
		for (Object val : dataArray) {
			s.append(val).append(" ");
		}
		return s.toString();
	}

	/** from array to one string with separation char**/
	public static String ToString(Object [] dataArray, String sep) {
		StringBuilder s= new StringBuilder("" + sep);
		for (Object val : dataArray) {
			s.append(val).append(sep);
		}
		return s.toString();
	}

	/** from array to FLStringList **/
	public static FLStringList ToStringList(Object [] dataArray) {
		FLStringList lst= new FLStringList();
		for (Object val : dataArray) {
			lst.add(val.toString());
		}
		return lst;
	}
	
	/** from array to FLStringList **/
	public static String [] toStringArray(Object [] dataArray) {
		int N= dataArray.length;
		String [] array= new String[N];
		for (int i=0; i<N ; i++) {
			array[i]=dataArray[i].toString();
		}
		return array;
	}

	/** splits with whitespaces and removes blanks **/
	public static FLStringList ParseString(String data) {
		String [] read= data.split(" ");
		FLStringList out = new FLStringList();
		for (String str : read){
			//Macro.Debug(" parsing : [" + str + "]");
			if (!str.isEmpty()){
				out.Add(str);
			}
		}
		return out;
	}

	/** produces a string with len zeros **/
	final public static String NcopiesOfChar(int len, char c) {
		StringBuilder s= new StringBuilder();
		for (int k = 0; k < len; k++) { s.append(c); }
		return s.toString();
	}

	/** produces a string with len zeros **/
	final public static String NcopiesOfStr(int N, String str) {
		StringBuilder s= new StringBuilder();
		for (int k = 0; k < N; k++) { s.append(str); }
		return s.toString();
	}

	/** copied from the internet */
	public static String repeatStr(String str, int n) {
		return new String(new char[n]).replace("\0", str);
	}






	/** SPECIAL I/O 
	 * reads from a **Non-empty** file 
	 * @throws IOException */
	public static String ReadFileInOneLine(String filename) {
		String output="";
		BufferedReader b;
		try {
			b = new BufferedReader(new InputStreamReader(
					new FileInputStream(filename)));
			String s = b.readLine();
			while (s != null) {
				output+= s+ " ";
				s = b.readLine();
			}
			b.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return output;
	}

	/** counts the number of occurrences of a char in a string **/
	public static int countOccurences(char c, String string) {
		int count = 0;
		for (int i = 0; i < string.length(); i++) {
			if (string.charAt(i) == c) {
				count++;
			}
		}
		return count;
	}




	public static void DoTest() {

		Macro.BeginTest("Macro");

		Macro.print(FormatIntWithZeros(45,5));

		String [] data= {"coucou", "hugues", "nazim" };
		int i1= FindRankStringList("nazim", data);
		int i2= FindRankStringList("coucouz", data);
		Macro.print(i1 + ":" + i2);
		String s = RemoveExtension("../aaaabbbb/cccc/dd.ext");
		Macro.print(s);
		Macro.print(" file exists :" + Macro.FileExists("test_hong.tmp"));

		FLStringList list= ParseString(" hi   ha  ho");
		list.PrintOnOneLine();

		int [] parseInt= ParseThreeNumbers("28 6,-76");
		String pI= MathMacro.ArrayToString2(parseInt);
		Macro.print(" parsed 3 int:" + pI);

		IntegerList ridlistofint= ParseNumbers("{bc3aaa 4 -7b 33");
		Macro.print(" parsed list:"+ridlistofint);

		Macro.EndTest();
	}

	static public void main(String [] arg){
		//DoTest();
		AperiodicTilingSystem.main(arg);
	}



}
