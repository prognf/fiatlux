package components.types;

import main.Macro;


/*------------------------------------------------------------------------------
- IntCouple encapsulates two integers
- @author Nazim Fates
------------------------------------------------------------------------------*/
public class IntCm extends IntC {

	final static String CLASSNAME2= new Macro.CurrentClassGetter().getClassName();
	
	public IntCm(int x, int y) {
		super(x,y);
	}
	
	/** (0,0) at init */
	public static IntCm New() {
		return new IntCm(0,0);
	}

	/** relative variation */
	final public void Add(IntC dxy) {
		m_x += dxy.X();
		m_y += dxy.Y();
	}

	/** relative variation */
	final public void Add(int dx, int dy) {
		m_x += dx;
		m_y += dy;
	}

	/** relative variation */
	public void AddToricalBoundaries(IntC dxy, IntC XYsize) {
		m_x = AddModulo(m_x, dxy.X(), XYsize.X());
		m_y = AddModulo(m_y, dxy.Y(), XYsize.Y());
	}

	/** relative variation from one position 
	 * 	with closed boundary conditions */
	public void AddFromPosClosedBC(IntC xypos, IntC dxy,
			IntC XYsize) {
		int 	newx= xypos.X() + dxy.X(),
				newy= xypos.Y() + dxy.Y();		
		m_x=((newx>=0) && (newx< XYsize.m_x))? newx : xypos.X();
		m_y=((newy>=0) && (newy< XYsize.m_y))? newy : xypos.Y();
	}

	/** "folds" with torical b.c. **/
	public void TorusTransform(IntC XYdim){
		m_x = (m_x + XYdim.m_x) % XYdim.m_x;
		m_y = (m_y + XYdim.m_y) % XYdim.m_y;
	}
	
	public void Mult(double d) {
		m_x *= d;
		m_y *= d;
	}

	//--------------------------------------------------------------------------
	//- get & set
	//--------------------------------------------------------------------------

	public void SetVal(int x, int y) {
		m_x= x; m_y= y;		
	}

	public void SetVal(IntC xy) {
		m_x= xy.m_x;
		m_y= xy.m_y;
	}

	
	public void CompareAndUpdateToMin(IntC XYcurrent) {
		if (XYcurrent.X() < m_x){
			m_x= XYcurrent.X();
		}
		if (XYcurrent.Y() < m_y){
			m_y= XYcurrent.Y();
		}
	}

	public void CompareAndUpdateToMax(IntC XYcurrent) {
		if (XYcurrent.X() > m_x){
			m_x= XYcurrent.X();
		}
		if (XYcurrent.Y() > m_y){
			m_y= XYcurrent.Y();
		}
	}
	
	/** used for "2D loops " **/
	public boolean AtEnd(IntC XY) {
		return (m_y>=XY.m_y);
	}

	/** used for "2D loops " **/
	public void Increment(IntC XY) {
		if (m_x < XY.m_x - 1 ){
			m_x++;
		} else {
			m_x=0;
			m_y++;
		}
	}
	
	/** adds only if the new coordinate is in the grid
	 * acts independently for each coordinate 
	 */
	public void AddBlockingBorders(int dx, int dy, IntC gridSize) {
		int newx= m_x + dx, newy= m_y +dy;
		if ((newx>=0)&&(newx<gridSize.m_x)){
			m_x= newx;
		}
		if ((newy>=0)&&(newy<gridSize.m_y)){
			m_y= newy;
		}
	}

	/** adds only if the new coordinate is in the grid
	 * acts independently for each coordinate 
	 */
	public void AddBlockingBorders(IntC Dxy, IntC gridSize) {
		AddBlockingBorders(Dxy.X(), Dxy.Y(), gridSize);
	}
	
	/** relative variation from one position */
	public void AddFromPosTorical(IntC dxy, IntC XYsize) {
		AddToricalBoundaries(dxy, XYsize);
	}
	
	/** creates new object with values multiplied by a constant **/
	public static IntCm Product(int mult, IntC xy) {
		return new IntCm(mult * xy.m_x, mult* xy.m_y);
	}
	
	public IntCm Duplicate() {
		return new IntCm(m_x,m_y);
	}
	
	public static IntCm GetNewObjectWithDummyVals() {
		return new IntCm(Integer.MIN_VALUE, Integer.MIN_VALUE);
	}

	public static IntCm WidestInterval() {
		return new IntCm(Integer.MAX_VALUE, Integer.MAX_VALUE);
	}
	
}
