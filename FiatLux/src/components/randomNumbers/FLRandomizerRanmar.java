package components.randomNumbers;

import components.randomNumbers.edu.cornell.lassp.houle.RngPack.Ranmar;
import main.Macro;


public class FLRandomizerRanmar extends FLRandomGenerator{

	
	// main linked object RANMAR algorithm
	Ranmar m_Randomizer;
	
	protected FLRandomizerRanmar(int seed) {
		super(seed);
	}

	/* called by superconstructor */
	@Override
	protected void CreateNewRandomizer(int in_seed){
		Macro.print(8, "New " + this.getClass().getSimpleName() + " with seed " + in_seed);
		SetSeedNonFixed(in_seed);
		m_Randomizer = new Ranmar(in_seed);	
		UpdateDisplay();
	}
	

	final public int RandomInt(int in_range) {
		return m_Randomizer.choose(0,in_range);
	}
	
	final public double RandomDouble() {
		return m_Randomizer.raw();
	}

	@Override
	public void Reset(int seed) {
		// there exists no reset in the class RanMar
		// so we need to create a new object
		CreateNewRandomizer(seed);	
	}
	
	public static void main (String[] args){
	}
}
