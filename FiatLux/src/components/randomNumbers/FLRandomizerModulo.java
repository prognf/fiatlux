package components.randomNumbers;

import main.Macro;

public class FLRandomizerModulo extends FLRandomGenerator {

	final static int RANDMOD= 2147483647;
	private long m_localvar ;
	
	@Override
	protected void CreateNewRandomizer(int in_seed) {
		Macro.print(4, "creating new " + this.getClass().getSimpleName() + " seed:" + in_seed);
		m_localvar= in_seed;
	}

	@Override
	public int RandomInt(int in_range) {
		m_localvar = (m_localvar * 16807 ) % RANDMOD;
		return (int)m_localvar % in_range;
	}
	
	@Override
	public double RandomDouble() {
		Macro.FatalError("not implemented yet");
		return 0;
	}	

	@Override
	public void Reset(int seed) {
		m_localvar= seed;
	}
	
	
}
