package components.randomNumbers;

import components.types.IntC;
import components.types.ProbaPar;

/** use this class with heritage **/
abstract public class StochasticSource {

	
	final private FLRandomGenerator m_randomizer;

	public StochasticSource() {
		m_randomizer= FLRandomGenerator.GetNewRandomizer();
	}
	
	/*---------------------------------------------------------------------------
	  - get & set
	  --------------------------------------------------------------------------*/

	public FLRandomGenerator GetRandomizer() {
		return m_randomizer;
	}	
	
	/** returns an int between 0 and range-1 **/
	public final int RandomInt(int range) {
		return GetRandomizer().RandomInt(range);
	}
	
	/** returns an intcouple with each value between 0 and range-1 **/
	public final IntC RandomIntC(int rangeX, int rangeY) {
		return new IntC( RandomInt(rangeX),RandomInt(rangeY) );
	}
	
	/** returns true or false with equiprobability **/
	public final boolean RandomTrueFalse() {
		return RandomInt(2)==0;
	}
	
	

	public final double RandomDouble() {
		return GetRandomizer().RandomDouble();
	}
	
	public final boolean RandomEventInt(int proba100) {
		return GetRandomizer().RandomEventPercent(proba100);
	}

	public final boolean RandomEventDouble(double proba) {
		return GetRandomizer().RandomEventDouble(proba);
	}
	
	public final boolean RandomEventDouble(ProbaPar probaPar) {
		return RandomEventDouble(probaPar.GetVal());
	}
	
	public final int RandomBernoulli(ProbaPar probaPar) {
		return RandomBernoulli(probaPar.GetVal());
	}

	public final int RandomBernoulli(double proba) {
		return RandomEventDouble(proba)?1:0;
	}
	
	public int GetSeed() {
		return GetRandomizer().GetSeed();
	}

	public void SetSeed(int seed) {
		GetRandomizer().setSeedFixedAndReset(seed);
	}

	
	
	/** returns a float between 0 and range 
	 * USE ONLY IF FLOAT IS NEEDED !! (otherwise use double) */
	final protected float RandomFloat(float range){
		return GetRandomizer().RandomFloat(range);
	}
	
}
