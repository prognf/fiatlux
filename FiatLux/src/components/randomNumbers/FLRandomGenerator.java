package components.randomNumbers;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map.Entry;

import components.types.BooleanPar;
import components.types.IntC;
import grafix.gfxTypes.controllers.ILabelUpdatable;
import grafix.gfxTypes.elements.FLPanel;
import main.Macro;
import main.MathMacro;


/*--------------------
 * The class FLRandomGenerator implements a random generator
 *--------------------*/

abstract public class FLRandomGenerator {

	/*--------------------------------------------------------------------
	 * STATIC FIELDS
	 * ------------------------------------------------------------------*/

	/** (static) GLOBAL seed management*/
	final private static java.util.Random m_SeedGenerator= 
			new java.util.Random();

	final private static FLRandomGenerator m_globalRandomizer=
			GetNewRandomizer();

	/*--------------------------------------------------------------------
	 * ATTRIBUTES
	 * ------------------------------------------------------------------*/

	
	/** a display to control the seed */
	protected ILabelUpdatable m_seedDisplay = null;

	/** the current seed , may be changed*/
	private int m_seed;

	/**
	 * this field controls the behaviour when sig_Reset signal is received
	 * do we reset to a fix seed or not ?
	 */
	final BooleanPar m_FixResetToSeed; 

	private static final String TXTFIXSEED = "fix";

	/*--------------------------------------------------------------------
	 * STATIC ONLY
	 * ------------------------------------------------------------------*/

	/** changes the GLOBAL seed of the seed generator !! **/
	public static void SetDefaultSeed(final long seed) {
		m_SeedGenerator.setSeed(seed);
	}

	/** provides a new seed */
	public final static int GetNewSeed() {
		return Math.abs(m_SeedGenerator.nextInt());
	}
	
	/** access to a randomizer 
	 * (if no control on the randomizer is needed) **/
	public static FLRandomGenerator GetGlobalRandomizer() {
		return m_globalRandomizer;
	}

	/** creates a new randomizer in an object */
	public static FLRandomGenerator GetNewRandomizer() {
		int seed= GetNewSeed();
		return new FLRandomizerRanmar(seed); 
	}
	
	/*--------------------------------------------------------------------
	 * CONSTRUCTOR
	 * ------------------------------------------------------------------*/



	protected FLRandomGenerator() {
		this(GetNewSeed());
	}

	protected FLRandomGenerator(final int seed) {
		m_seed= seed;
		CreateNewRandomizer(seed);
		m_FixResetToSeed= new BooleanPar(TXTFIXSEED, false); // seed not fixed by default
	}

	/** should be called only by constructors*/
	abstract protected void CreateNewRandomizer(int in_seed);

	/** reset randomizer with current seed called by sig_ResetRandomizer*/
	abstract protected void Reset(int seed);

	/*--------------------------------------------------------------------
	 * RANDOM - VALUE GENERATION
	 * ------------------------------------------------------------------*/

	/** @return a random integer between 0 and Range-1 */
	abstract public int RandomInt(int range);

	
	/** tosses a fair coin : true or false with equal proba. */
	final public boolean RandomToss(){
		return RandomInt(2)==0;
	}


	/** @return a random double between 0 and 1 */
	abstract public double RandomDouble();

	/** @return random between 0 and in_arg 
	 * cast involved : dangerous ???? */
	@Deprecated
	final public float RandomFloat(final float in_arg) {
		return RandomFloat() * in_arg;
	}

	/** @return random between 0 and 1 
	 * cast involved : dangerous ???? */
	@Deprecated
	final public float RandomFloat() {
		return ((float) RandomDouble());
	}

	/** returns two integers between 0 and Range-1 */
	final public IntC RandomIntCouple(final int in_rangeX, final int in_rangeY) {
		return new IntC(RandomInt(in_rangeX), RandomInt(in_rangeY));
	}

	/*--------------------------------------------------------------------
	 * RANDOM - EVENT GENERATION
	 * ------------------------------------------------------------------*/

	/** random event with probability arg */
	final public boolean RandomEventDouble(final double in_arg) {
		if (in_arg >= 1)
			return true;
		else 
			return (RandomDouble() < in_arg);
	}

	/** random event with probability arg / 100 */
	final public boolean RandomEventPercent(final int in_arg) {
		if (in_arg >= 100)
			return true;
		else 
			return (RandomInt(100) < in_arg);
	}

	/*--------------------------------------------------------------------
	 * RANDOM - ARRAY/LIST/TABLE
	 * ------------------------------------------------------------------*/


	/**
	 * Shuffles the values of an integer array according to
	 * <a href="http://wikipedia.fr/...">Durstenfeld</a>'s
	 * algorithm.
	 * @param array the array to shuffle
	 * @return the shuffled array
	 */
	final public int[] Shuffle(
			int[] array) {
		int size = array.length;
		int rand = RandomInt(MathMacro.Factorial(size));
		for (int i = size-1; i > 1; i--) {
			int j = rand%i;
			int tmp = array[j];
			array[j] = array[i];
			array[i] = tmp;
			rand /= i;
		}
		return array;
	}


	final public Object pickRandom(ArrayList<?> list) {
		int size = list.size();
		return list.get(RandomInt(size));
	}

	/** negative weights are not counted **/
	final public Object pickObject(
			ArrayList<Object> objects,
			ArrayList<Double> weights) {

		double total = 0;
		for (Double value : weights) {
			total += value >= 0? value:0;
		}

		double rand = RandomDouble()*total;

		for (int i = 0; i < weights.size(); i++) {
			double value = weights.get(i);
			if (value>0) {
				if (rand > value) {
					rand-= value;
				} else {
					return objects.get(i);
				}
			}
		}
		Macro.FatalError("This should never happen");
		return null;
	}

	final public Object pickObject(
			Hashtable<?, Double> table) {

		double total = 0;
		for (Entry<?, Double> entry : table.entrySet()) {
			double value = entry.getValue();
			if (value >= 0) {
				total +=value;
			}
		}

		double rand = RandomDouble()*total;

		for (Entry<?, Double> entry : table.entrySet()) {
			double value = entry.getValue();
			if (value > 0) {
				if (rand>value) {
					rand -= value;
				} else {
					return entry.getKey();
				}
			}
		}
		Macro.FatalError("This should never happen");
		return null;
	}

	/*--------------------------------------------------------------------
	 * seed control
	 * ------------------------------------------------------------------*/

	/**
	 * used by sig_Init() it is interesting to see it a signal as there is not
	 * necessarily an effect
	 */
	final public void sig_ResetRandomizer() {
		if (m_FixResetToSeed.GetVal()) {
			Reset(m_seed);
		} else {
			Reset(GetNewSeed());
		}
	}

	public FLPanel GetSeedControl() {
		return new RandomizerSeedControl(this);
	}
	public int GetSeed() {
		return m_seed;
	}

	/** sets the seed without any other action **/
	public void setSeedFixedAndReset(final int seed) {
		m_seed = seed;
		Reset(m_seed);		// to apply changes
		m_FixResetToSeed.SetVal(true);
	}

	/** sets the seed without any other action **/
	protected void SetSeedNonFixed(int seed) {
		m_seed = seed;		
	}

	/*--------------------------------------------------------------------
	 *others
	 * ------------------------------------------------------------------*/


	protected void UpdateDisplay() {
		if (m_seedDisplay != null) {
			m_seedDisplay.updateLabel(String.valueOf(m_seed));
		}
	}

	/**
	 * Assigns a new display for the generator seed
	 */
	public void SetNewDisplay(ILabelUpdatable display) {
		m_seedDisplay = display;
		this.UpdateDisplay();
	}

	public String toString() {
		return "" + m_seed;
	}

	/*--------------------
	 * Test
     --------------------*/

	final static int BINS = 4, STATS = 10000000;

	public static void DoTest() {

		FLRandomGenerator rand =
				new FLRandomizerRanmar(280676);
		// new FLRandomizerJava();
		// new FLRandomizerModulo();

		Macro.BeginTest(rand.getClass().getSimpleName());

		int[] counter = new int[BINS];
		for (int i = 0; i < STATS; i++) {
			// double d= rand.RandomDouble();
			int number = rand.RandomInt(BINS);
			counter[number]++;
		}
		for (int bin = 0; bin < BINS; bin++) {
			Macro.print("bin: " + bin + " counts: " + counter[bin]);
		}

		Macro.EndTest();
	}

	



}
