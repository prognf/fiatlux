package components.randomNumbers;

import java.awt.Color;
import java.awt.GridBagConstraints;

import javax.swing.JComponent;

import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.MacroGFX;
import grafix.gfxTypes.controllers.ILabelUpdatable;
import grafix.gfxTypes.elements.FLActionButton;
import grafix.gfxTypes.elements.FLPanel;
import grafix.gfxTypes.elements.FLTextArea;


/*--------------------
 * controls an initializer with one parameter
 * 
 * @author Nazim Fates
*--------------------*/

public class RandomizerSeedControl extends FLPanel  {

	/*--------------------
	 * attributes
	 --------------------*/

	//final static private String TXT="fix";//  "Keep seed";
	public static final String S_SEED = "S";
	private static final Color COLORBUTTON = FLColor.c_lightgrey;

	
	final FLRandomGenerator m_randomizer;

	final ChangeSeedButton mc_ChangeSeed= new ChangeSeedButton();
	final SeedDisplay mc_SeedDisplay= new SeedDisplay();

	/*--------------------
	 * Constructor
	 --------------------*/
	public RandomizerSeedControl(FLRandomGenerator in_init){
		m_randomizer = in_init;
		m_randomizer.SetNewDisplay(mc_SeedDisplay);
		
		SetGridBagLayout();
		GetGridBagC().fill=
			GridBagConstraints.HORIZONTAL;
		AddGridBag(mc_ChangeSeed, 0, 0);
		AddGridBag(mc_SeedDisplay, 1, 0);
		JComponent resetSeedControl= m_randomizer.m_FixResetToSeed.GetControl();
		AddGridBag(resetSeedControl, 2, 0);
		
		//mc_ChangeSeed.setBackground(COLORBUTTON);
		//mc_SeedDisplay.setBackground(COLORBUTTON);
		mc_ChangeSeed.setBackground(COLORBUTTON);

		resetSeedControl.setOpaque(false);
		mc_SeedDisplay.setOpaque(false);
		setOpaque(false);
		this.setBackground(COLORBUTTON);
	}

	class ChangeSeedButton extends FLActionButton{

		public ChangeSeedButton() {
			super(S_SEED);
		}

		@Override
		public void DoAction() {
			int newseed= MacroGFX.ReadIntegerInWindow();
			if (newseed!=0) {
				m_randomizer.setSeedFixedAndReset(newseed);		
			}
		}
	}
	/*--------------------
	 * actions
	 --------------------*/
	
	class SeedDisplay extends FLTextArea implements ILabelUpdatable{

		public SeedDisplay() {
			super(8);
		}

		@Override
		public void updateLabel(String s) {
			this.setText(s);
		}
	}

	

}
