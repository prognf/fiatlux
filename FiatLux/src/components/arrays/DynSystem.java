package components.arrays;

import components.types.FLsignal;
import components.types.Monitor;
import main.Macro;
import updatingScheme.UpdatingScheme;

/*--------------------
 * holds the current state of the system  
 * + an updating scheme (mandatory)
 * Rq: codes also 2D arrays with a proper mapping
 *--------------------*/
abstract public class DynSystem extends SuperSystem 
implements Monitor, TimeReadable
/* TimeReadable will be used to read time in the updating scheme for instance **/
{

	/*--------------------
	 * Abstract methods
	 --------------------*/

	/** called by sig_Init */
	abstract protected void ResetComponents();

	/*--------------------
	 * Attributes
	 --------------------*/
	// size info
	private int m_Lsize;	

	// time info
	private int m_CurrentTime = 0;

	// holds the updating scheme
	private UpdatingScheme m_updatingScheme;

	/*--------------------
	 * Constructor
	 --------------------*/


	protected DynSystem(int in_Lsize) {
		m_Lsize = in_Lsize;
		m_updatingScheme= null; // DANGER !
	}

	/*--------------------
	 * signals
	 --------------------*/
	@Override
	/** processing signals **/
	public void ReceiveSignal(FLsignal sig){
		switch(sig){
		case init:
			m_CurrentTime = 0;
			ResetComponents(); // TODO is this one necessary?
			break;
		case update:
			break;
		case nextStep:
			// time update
			m_CurrentTime++;
			break;
		}
		if (m_updatingScheme!=null) {
			m_updatingScheme.ReceiveSignal(sig);
		}
	}

	/** this signal-method is invoked by the initializers */
	public void sig_Init() {
		ReceiveSignal(FLsignal.init);
	}

	/** calculates the new states with (a)synchronous dynamics 
	 * transmits the signal to the updating scheme */
	public void sig_NextStep() {
		//Macro.Debug("dyn array Next step");
		//Macro.Debug("updating scheme " + m_UpdatingScheme.GetName());
		ReceiveSignal(FLsignal.nextStep);
	}

	public void sig_Update() {
		ReceiveSignal(FLsignal.update);
	}


	/*--------------------
	 * Get & Set methods
	 --------------------*/


	/** size = nb of cells */
	final public int GetSize() {
		return m_Lsize;
	}

	@Override
	final public int GetTime() {
		return m_CurrentTime;
	}



	/*--------------------
	 * updating scheme
	 --------------------*/


	public void SetUpdatingScheme(UpdatingScheme updatingScheme) {
		Macro.print(5, this, "Setting UpdatingScheme: " + updatingScheme.GetName());
		m_updatingScheme=updatingScheme;
	}


}
