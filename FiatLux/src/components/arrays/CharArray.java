package components.arrays;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import components.types.FLfileReader;
import initializers.CAinit.BinaryInitializer;

/*--------------------
 * ArrayList of character, representing a configuration
 * Used when creating a initial configuration with StringInitializer
 * @author Oceane Chaze
 *--------------------*/

public class CharArray extends ArrayList<Character> {

    private int m_width, m_height; //dim of cell grid
    private int m_nbStates;
    ArrayList<Character> m_code;//used to transfer char to int (give a weight to each useable character)


    public CharArray(int width, int height, int nbStates){
        m_width = width;
        m_height = height;
        m_nbStates = nbStates;
        m_code = new ArrayList<>();
        for(char c :"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray()){
            m_code.add(c);
        }
        //Initialize the array with '0'
        for(int i=0;i<width*height;i++){
            this.add('0');
        }
    }
    public CharArray(String path, int width, int height, int nbStates, boolean rle){
        m_width = width;
        m_height = height;
        m_nbStates = nbStates;
        m_code = new ArrayList<>();
        for(char c :"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray()){
            m_code.add(c);
        }
        for(int i=0;i<width*height;i++){
            this.add('0');
        }
        FLfileReader file = FLfileReader.open(path);

        if(rle){
            String line = file.readLn();
            String toParse = "";
            while(line != null && (line.charAt(0)=='#' || line.charAt(0)=='x' )){
                line=file.readLn();
            }
            while(line!=null){
                toParse += line;
                line = file.readLn();
            }
            Pattern p1 = Pattern.compile("([\\d*[a-zA-Z]]+)\\$*");
            Matcher match = p1.matcher(toParse);
            int i = 0;//height
            while (match.find() && i<m_height) {
                Pattern p2 = Pattern.compile("([\\d]*)([a-zA-Z])");
                Matcher m2 = p2.matcher(match.group(1));
                int j = 0;//width
                while(m2.find()){
                    int n = 1;
                    if(!(m2.group(1).equals(""))){
                        n = Integer.parseInt(m2.group(1));
                    }
                    int k = 0;
                    while(k<n && j<m_height){
                        char c;
                        c=(m2.group(2).equals("b"))?'0':'1';
                        Set(j,m_height-i-1,c);
                        k++;
                        j++;
                    }
                }
                i++;
            }
        }

        else{
            String str = file.readLn();
            for(int i=0;i<height;i++){
                if(str!=null){
                    String tab[] = str.split(" ");
                    for(int j = 0; j<width;j++){
                        if(j<tab.length){
                            if(!(tab[j].equals("."))){
                                this.Set(j,height-1-i,tab[j].charAt(0));
                            }
                        }
                    }
                    str = file.readLn();
                }
            }
        }


        file.Close();


    }

    /* ---------------    *
     *     Get and Set    *
     * ------------------- */

    public char Get(int x, int y){
        int i,j;
        i= (x<m_width)? x % m_width : x ;
        j= (y<m_height)? y % m_height : x ;
        return this.get(i+j*m_height);
    }

    /** Set character c at place x, y **/
    public void Set(int x, int y, char c){
        int i,j;
        i = x%m_width;
        j = y%m_height;
        char d = m_code.get(m_code.indexOf(c)%m_nbStates);
        this.set(i+j*m_height,d);
    }

    /* ---------------    *
     *     FIGURES        *
     * draw known forms   *
     * ------------------- */

    /** Draw a line on a row of char c at x,y of length length  **/
    public void SmallLine(int x, int y, int length, char c){
        for(int i=0;i<length;i++){
            Set(x+i,y, c);
        }
    }
    /** Draw a line of char c at x,y of length length  **/
    public void SmallColumn(int x, int y, int length, char c){
        for(int i=0;i<length;i++){
            Set(x,y+i, c);
        }
    }

    /** Draw a line of state c at y **/
    public void Line(int y, char c){
        SmallLine(0,y,m_width, c);
    }
    public void CenterLine(char c){
        SmallLine(0,m_height/2,m_width, c);
    }
    /** Draw a column of state c at x **/
    public void Column(int x, char c){
        SmallColumn(x,0,m_height, c);
    }
    public void CenterColumn(char c){
        SmallColumn(m_width/2,0,m_height, c);
    }

    /** Draw a rectangle of state c with origin x, y and size width, height **/
    public void Rectangle(int x, int y, int width, int height, char c){

        for(int i =0;i<width;i++){
            SmallColumn(x+i,y,height, c);
        }
    }
    /** Draw a rectangle of state c with origin x, y and size width, height **/
    public void CenterRectangle(int width, int height, char c){

        for(int i =0;i<width;i++){
            SmallColumn(m_width/2-width/2+i,m_height/2-height/2,height, c);
        }
    }

    /** Draw a rectangle frame of state c with origin x, y and size width, height **/
    public void Frame(int x, int y, int width, int height, char c){
        if(height!=0 && width!=0){
            SmallLine(x,y,width,c);
            SmallColumn(x,y,height,c);
            SmallLine(x,y+height-1,width,c);
            SmallColumn(x+width-1,y,height,c);
        }
    }
    public void CenterFrame(int width, int height, char c){
        if(height!=0 && width!=0){
            SmallLine((m_width-width)/2,(m_height-height)/2,width,c);
            SmallColumn((m_width-width)/2,(m_height-height)/2,height,c);
            SmallLine((m_width-width)/2,(m_height-height)/2+height-1,width,c);
            SmallColumn((m_width-width)/2+width-1,(m_height-height)/2,height,c);
        }

    }

    /** Create a checks of char c and '0' at x,y with dimensions width, height **/
    public void SmallCheck(int x, int y, int width, int height, char c){
        SmallCheck(x, y, width, height ,c ,'O');
    }
    /** Create a checks of char c and d at x,y with dimensions width, height **/
    public void SmallCheck(int x, int y, int width, int height, char c, char d){
        for(int i=0;i<width;i++){
            for(int j=0;j<height;j++){
                if(i%2==j%2){
                    Set(x+i,y+j, c);
                }
                else{
                    Set(x+i,y+j, d);
                }
            }
        }
    }
    public void CenterCheck(int width, int height, char c, char d){
        SmallCheck((m_width-width)/2,(m_height-height)/2,width,height,c,d);
    }
    /** Create a checks of char c and '0' on the entire grid **/
    public void Check(char c){
        SmallCheck(0,0,m_width,m_height, c);
    }
    /** Create a checks of char c and d on the entire grid **/
    public void Check(char c, char d){
        SmallCheck(0,0,m_width,m_height, c, d);
    }

    public void Bernoulli(double proba, char c, char d){
        BinaryInitializer bin = new BinaryInitializer();
        for (int i=0;i<m_width;i++){
            for (int j=0;j<m_height;j++){
                if(bin.RandomEventDouble(proba)){
                    Set(i,j,c);
                }
                else{
                    Set(i,j,d);
                }
            }
        }
    }


    /* ---------------    *
    *     OPERATIONS      *
    * ------------------- */

    public void And(CharArray array){
        int index;
        for (int i=0;i<m_width*m_height;i++) {
            index = Math.min(m_code.indexOf(this.get(i)), m_code.indexOf(array.get(i)));
            this.set(i, m_code.get(index));
        }
    }
    public void Or(CharArray array){
        int index;
        for (int i=0;i<m_width*m_height;i++) {
            index = Math.max(m_code.indexOf(this.get(i)), m_code.indexOf(array.get(i)));
            this.set(i, m_code.get(index));
        }
    }
    public void Xor(CharArray array){
        int index;
        for (int i=0;i<m_width*m_height;i++) {
            index = (m_code.indexOf(this.get(i)) + m_code.indexOf(array.get(i)))%m_nbStates;
            this.set(i, m_code.get(index));
        }
    }

}
