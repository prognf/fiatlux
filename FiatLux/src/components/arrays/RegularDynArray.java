package components.arrays;

import java.util.ArrayList;

import components.allCells.OneRegisterIntCell;
import components.allCells.SuperCell;
import initializers.CAinit.OneRegisterSettableSystem;
import main.Macro;


/*--------------------
 * holds an array of cells
 *--------------------*/
public class RegularDynArray extends DynSystem  implements OneRegisterSettableSystem {

	/*--------------------
	 * Attributes
	 --------------------*/

	private final GenericCellArray<SuperCell> m_cellarray ;

	/*--------------------
	 * array methods
	 --------------------*/

	/** First mode of use 
	 * casting an array list from generic type to subtype **/
	public <Z extends SuperCell> GenericCellArray<Z> GetArrayAsCastedList() {
		return new GenericCellArray<Z>( this );
	}

	/** second mode of use **/
	public GenericCellArray<SuperCell> GetArrayListForWiring() {
		return m_cellarray;
	}

	/*--------------------
	 * Constructor
	 --------------------*/

	/** it is assumed that the cell array has been created externally **/
	public RegularDynArray(GenericCellArray<SuperCell> cellarray) {
		super(cellarray.GetSize());
		m_cellarray = cellarray;
	}


	@Override
	/** sends reset signal to all cells 
	 called by sig_Init */
	protected void ResetComponents(){
		for (int pos=0; pos < GetSize(); pos++){
			SuperCell cell = GetCell(pos);
			cell.sig_Reset();
		}
	}


	/*--------------------
	 * Get / Set
	 --------------------*/

	final public SuperCell GetCell(int pos) {
		return m_cellarray.get(pos);
	}

	/** used only by TopologyManager **/
	public void SetCellArray(int pos, SuperCell in_newCell) {
		if ( pos >= GetSize()){
			Macro.FatalError("Exceeding (fixed) array size ");
		}
		SuperCell cell = in_newCell;
		m_cellarray.set(pos, cell);
	}

	public void PrintCompactInfo(){
		for (int pos=0; pos < GetSize(); pos++){
			OneRegisterIntCell cell= (OneRegisterIntCell) GetCell(pos);
			Macro.printNOCR(" : " + cell.GetState() ) ;
		}
		Macro.SkipLines(1);
	}

	public void PrintStateInfo(){
		Macro.print(getGlobalStateAsString());
		Macro.CR();
	}

	/** works only with OneRegisterIntCell **/
	public String getGlobalStateAsString() {
		return getGlobalStateAsString("");
	}
	
	/** works only with OneRegisterIntCell **/
	public String getGlobalStateAsString(String formatter) {
		if (!(GetCell(0) instanceof OneRegisterIntCell)){
			Macro.SystemWarning("cells are not of type OneRegisterIntCell");
			return null;
		}
		StringBuilder s= new StringBuilder();
		for (int pos=0; pos < GetSize(); pos++){
			OneRegisterIntCell cell = (OneRegisterIntCell)GetCell(pos);
			s.append("").append(cell.GetState()).append(formatter);
		}
		return s.toString();
	
	}

	/** works only with OneRegisterIntCell RETINA Special **/
	public String GetStateAsStringTEMP() {
		SuperCell cellTest= GetCell(0);
		if (cellTest instanceof OneRegisterIntCell){
			StringBuilder s= new StringBuilder();
			for (int pos=0; pos < GetSize(); pos++){
				OneRegisterIntCell cell = (OneRegisterIntCell)GetCell(pos);
				s.append("").append(cell.GetState()).append(";");
			}
			return s.toString();
		} else {
			Macro.SystemWarning("cells are not of type OneRegisterIntCell");
			return null;
		}
	}
	
	/** works only with OneRegisterIntCell **/
	public String GetStateAsFormattedString() {
		SuperCell cellTest= GetCell(0);
		if (cellTest instanceof OneRegisterIntCell){
			StringBuilder s= new StringBuilder();
			for (int pos=0; pos < GetSize(); pos++){
				OneRegisterIntCell cell = (OneRegisterIntCell)GetCell(pos);
				s.append(cell.GetState()).append(";");
			}
			return s.toString();
		} else {
			Macro.SystemWarning("cells are not of type OneRegisterIntCell");
			return null;
		}
	}

	public <XYZ> ArrayList<XYZ> GetCastedArray() {
		ArrayList<XYZ> casted = new ArrayList<XYZ>(GetSize());
		for (Object cell : m_cellarray){
			@SuppressWarnings("unchecked")
			XYZ newtypeCell= (XYZ) cell; 
			casted.add(newtypeCell);
		}
		return casted;
	}

	@Override
	/** will not always work **/
	public void setState(int pos, int state) {
		OneRegisterIntCell cell = (OneRegisterIntCell)GetCell(pos);
		cell.InitState(state);
	}

	@Override
	public int getSize() {
		return GetSize();
	}

}
