package components.arrays;


import components.randomNumbers.FLRandomGenerator;
import components.types.IntC;

/** holds size and time **/
public class GridSystem extends SuperSystem {

	protected final IntC m_XYsize;  	// dimensions
	private int m_localtime;

	public GridSystem(IntC XYsize) {
		m_XYsize= XYsize;
	}

	protected final int Map2Dto1DoffsetBC(int x, int y, IntC dxy) {
		int 
		newx= IntC.AddModulo(x, dxy.X(), m_XYsize.X()),
		newy= IntC.AddModulo(y, dxy.Y(), m_XYsize.Y());
		return Map2Dto1D(newx, newy);
	}

	protected final int Map2Dto1D(int x, int y) {
		return m_XYsize.Map2Dto1D(x,y);
	}
	
	public IntC GetXYsize() {
		return m_XYsize;
	}
	
	public int Xsize() {
		return GetXYsize().X();
	}
	
	public int Ysize() {
		return GetXYsize().Y();
	}
	
	public int GetNsize() {
		return m_XYsize.prodXY();
	}
	
	
	
	/** called by sig_Init **/	
	protected void RandomizerInit() {
		FLRandomGenerator rand = GetRandomizer();
		if (rand  !=  null){
			rand.sig_ResetRandomizer();
		}		
	}
	
	
	/** final, but may change to "override if needed" **/
	final public void sig_Update() {
		
	}

	/** override if needed **/
	public void sig_Init() {
		m_localtime=0; // TimeReset();
		RandomizerInit();
	}
	
	public int GetTime() {
		return m_localtime;
	}

	protected void TimeClick(){
		m_localtime++;
	}
	
}
