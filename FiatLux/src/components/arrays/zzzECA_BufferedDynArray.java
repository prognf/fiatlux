package components.arrays;

/*--------------------
 * BufferedECAautomaton allows speedy computations of asynchornous ECA  
 * @author Nazim Fates
 ***********************************************/
import java.util.Date;

import components.types.FLString;
import main.Macro;
import main.MathMacro;

/*--------------------
 * Contains an optimization of asynchornous ECA consisting in testing if the
 * transition is active before launching the random number generator
 * 
 * @author Nazim Fates
*--------------------*/
public class zzzECA_BufferedDynArray extends zzzECA_DynArray {

	final static String m_ClassName = "BufferedECAautomaton";
	
	// here are the cells
	protected int[] m_Array_current; // only a pointer
	protected int[] m_Array_even; // buffer for even times
	protected int[] m_Array_odd; // buffer for odd times

	// here is the transition table
	final static int OCTOPUS = 8;
		
	protected int[] m_TransitionTable = new int[OCTOPUS];

	/*--------------------
	 * construction
	 --------------------*/

	/** 1D ECA Constructor * */
	public zzzECA_BufferedDynArray(int in_Xsize) {
		
		super(in_Xsize);
		
		m_Array_even = new int[ GetSize() ];
		m_Array_odd = new int[ GetSize() ];
		m_Array_current= m_Array_even;
		// rule should also be set
	}

	/*--------------------
	 * Dynamics
	 --------------------*/
	/* as time is considered zero */
	public void InitSetCell(int i, int state) {
		m_Array_current[i] = state;
	}

	final private int IndexBase2(int leftindex, int meindex, int rightindex){
		return  m_TransitionTable[ m_Array_current[leftindex] + 
		2 * m_Array_current[meindex] +
		4 * m_Array_current[rightindex] ];
	}


	/**
	 * main metod
	 */
	final public void OneStepBeyond(){
		int[] next;
		if (GetTime() % 2 == 0) {
			// current= m_Array_even;
			next = m_Array_odd;
		} else {
			// current= m_Array_odd;
			next = m_Array_even;
		}
		// cells for indices 1 to n-2
		for (int i = 1; i < GetSize() - 1; i++) {
			/* ALGO 1 */
			int m_nextstate = IndexBase2(i-1,i,i+1);
			if ((GetCellState(i) == m_nextstate) || 
				BernoulliAlpha()) {
				next[i] = m_nextstate;
			} else {
				next[i] = 1 -m_nextstate;
			}
		}
		int lastindex= GetSize() - 1;
		// cell 0 updated "normally"
		if (BernoulliAlpha()) {
			next[0] = IndexBase2(lastindex,0,1);
		} else {
			next[0] = GetCellState(0);
		}
		// cell n-1 updated "normally"
		if (BernoulliAlpha()) {
			next[lastindex] = IndexBase2(lastindex-1, lastindex,0);
		} else {
			next[lastindex] = GetCellState(lastindex);
		}
		// incrementing MCS time
		m_Array_current = next;
		super.sig_NextStep();
	}

	private boolean BernoulliAlpha() {
		// TODO USE RANDOM GEN HERE
		return false;
	}

	/*--------------------
	 * Get / Set
	 --------------------*/

	

	/** get density * */
	final public double GetDensity() {
		int count = 0;
		for (int i = 0; i < GetSize(); i++) {
			count += m_Array_current[i];
		}
		return (count / (double) GetSize());
	}

	/** reading the state : implements the array * */
	final public int GetCellState(int i) {
		return m_Array_current[i];
	}

	final public void SetRule(int in_RuleNum) {
		// classical Wolfram
		int[] WTransitionTable = MathMacro.Decimal2BinaryTab(in_RuleNum, OCTOPUS);
		// our notation
		for (int i = 0; i < OCTOPUS; i++) {
			m_TransitionTable[i] = WTransitionTable[i];
			// m_TransitionTable[i]= WTransitionTable[ CORRESP[i] ];
		}
		Macro.print(4, "Rule set to :" + in_RuleNum);
	}

	/*--------------------
	 * Test
	 --------------------*/
	
	public static void main(String[] argv) {
		DoTest();
	}
	
	
	final static int STEP=1000, SIZE=10000, RULE=50, REFTIME_MS=400;
	final static double ALPHA=.6;
	public static void DoTest() {
		
		zzzECA_DynArray spid = new zzzECA_BufferedDynArray(SIZE);
		//spid.SetSynchronyRate(ALPHA);
		Date t0 = Macro.GetTime();

		spid.SetRule(RULE);
		boolean fixedCI = true, fixedH= true;
		//spid.Init(0.5, fixedCI, fixedH);
		//spid.Print();
		for (int i = 0; i <STEP; i++) {
			spid.OneStepBeyond();
		}

		Date t1 = Macro.GetTime();
		long dt = t1.getTime() - t0.getTime();
		String s_perf = "Perf. ratio: " +  
		 FLString.DoublePrecisionN(MathMacro.HUNDRED * REFTIME_MS / dt,1) + " %";
		Macro.print(
				"Time for 10 MegaOp.: "+ FLString.TimeFormat(t1,t0) + s_perf);

	}


}
