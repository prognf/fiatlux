package components.arrays;

import java.util.ArrayList;

import components.allCells.SuperCell;

/*--------------------
 * ArrayList with Z-Cells
 * @author Nazim Fates, Olivier Boure
*--------------------*/
public class GenericCellArray<Z extends SuperCell> extends ArrayList<Z> {

	
	public GenericCellArray() {
		super();
	}
	
	
	public GenericCellArray(int size) {
		super(size);
		for (int i=0 ; i< size; i++){
			add(i,null);
		}
	}
	
	public GenericCellArray(GenericCellArray<SuperCell> list) {
		super(list.size());
		for (SuperCell c : list){
			@SuppressWarnings("unchecked")
			Z newtypeCell= (Z) c; 
			add(newtypeCell);
		}
	}
	
	public GenericCellArray(RegularDynArray in_automaton) {
		this(in_automaton.GetArrayListForWiring());
	}

	public int GetSize() {
		return size();
	}
	
	
	public Z GetCell(int pos) {
		return get(pos);
	}
	
	
	

}

