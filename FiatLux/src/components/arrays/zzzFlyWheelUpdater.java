package components.arrays;


import components.randomNumbers.StochasticSource;


public class zzzFlyWheelUpdater extends StochasticSource {

	
	int m_updateindex;
	int m_size;
	boolean[] m_transition;
	double m_alpha;
	
	/** 1D constructor */
	public zzzFlyWheelUpdater(int dim) {
		m_size = dim;
		m_transition = new boolean[m_size];
		m_updateindex = 0;
	}

	
	/* shifts the transition array to the left */
	public void LeftShift(){
		boolean cell0= m_transition[0];
		for (int i=0; i< m_size-1;i++){
			m_transition[i]= m_transition[i+1];
		}
		m_transition[m_size-1]= cell0;
	}
	
	/* transition with or without shift */
	final public boolean [] GetTransition(boolean shift){
		return GetParallelTransition(shift);
	}
	
	public boolean[] GetParallelTransition(boolean shift) {
		// array construction
		for (int i = 0; i < m_size; i++) {
				m_transition[i]= RandomEventDouble(m_alpha); 
		}
		if (shift)
			this.LeftShift();
		return m_transition;
	}
	
	public boolean[] GetSequentialTransition() {
		// array construction
		for (int i = 0; i < m_size; i++) {
			if (i == m_updateindex ) {
				m_transition[i] = true;
			} else {
				m_transition[i] = false;
			}
		}
		// time updating
		m_updateindex++;
		m_updateindex = m_updateindex % m_size;
		return m_transition;
	}

	public double GetAlpha() {
		return m_alpha;
	}

	public void SetAlpha(double alpha) {
		this.m_alpha = alpha;
	}

	

}
