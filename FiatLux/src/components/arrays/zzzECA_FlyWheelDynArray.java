package components.arrays;

import main.Macro;
import main.MathMacro;

/*--------------------
* Contains an fly wheel optimization of asynchronous ECA
* @author Nazim Fates
****--------------------*/
public class zzzECA_FlyWheelDynArray extends zzzECA_DynArray {
	
	/*--------------------
	 * attributes
	 --------------------*/
	final static String m_ClassName="FlyWheelECAautomaton";
	final static int [] CORRESP= {0, 1, 4, 5, 2, 3, 6, 7};
	final static int OCTOPUS = 8;

	// here is the transition table
	protected int[] m_TransitionTable = new int[OCTOPUS];
	// 	here are the cells
	protected int[] m_Array;
	/*--------------------
	 *  1D Constructor 
	 --------------------*/
	public zzzECA_FlyWheelDynArray(int in_Xsize) {
		super(in_Xsize);
		Macro.print(4, "fly wheel ECA creation...");
	
		// space
		m_Array = new int[GetSize()];
	}
	
	/*--------------------
	 * implementations
	 --------------------*/
	protected void InitSetCell(int i, int state) {
		m_Array[i]= state;
	}
	
	
	
	/*--------------------
	 * Main methods
	 --------------------*/
	
	/* left shift of the current configuration */
	public void LeftShift(){
		int cell0= GetCell(0);
		for (int i=0; i< GetSize()-1; i++){
			SetCell(i, GetCell(i+1));
		}
		SetCell(GetSize()-1,cell0);
		
	}
	
	/* updates with Bernoulli style */
	public void OneStepBeyond() {
		boolean [] transition = new boolean [GetSize()];
		for (int i=0; i< GetSize(); i++){
			transition[i]= BernoulliAlpha(); //true; //false;//(((i+m_time)%2)==0);
		}
		//transition[(m_time%GetSize())]= true;
		CalculateNextState(transition);
	}

	private boolean BernoulliAlpha() {
		// TODO rewrite ???
		return false;
	}

	/* updates cells one after the other */
	public void OneStepBeyond(zzzFlyWheelUpdater in_updater){
		boolean [] transition = in_updater.GetTransition(SHIFTUPDATE);
		CalculateNextState(transition);
	}
	/**
	 * main function 
	 */
	final public void CalculateNextState(boolean [] in_transition) {

		// IMPORTANT : we need to save this value !!!
		int st_CellZero= GetCell(0); 
		
		// wheel init & cell 0
		int st= 2 * GetCell(GetSize()-1) + 
				4 * GetCell(0) + 
					GetCell(1);
		if (in_transition[0]){
			SetCell(0, m_TransitionTable[st]);
		}
		// cell indices from 1 to n-2
		for (int i=1; i< GetSize()-1; i++){
			st= 4*(st%2) + 2* (st / 4) + GetCell(i+1);
			if (in_transition[i]){ 
					SetCell(i, m_TransitionTable[st]);
			}
		}
		// cell index n-1
		if (in_transition[GetSize()-1]){
			st= 4*(st%2) + 2* (st / 4) + st_CellZero; // we compute it only if we make the transition
			SetCell(GetSize()-1, m_TransitionTable[st]);
		}
		super.sig_NextStep();
	}
	/*--------------------
	 * Get / Set
	 --------------------*/

	final public void SetRule(int in_RuleNum) {
		// classical Wolfram
		int[] WTransitionTable = MathMacro.Decimal2BinaryTab(in_RuleNum, OCTOPUS);
		// our notation
		for (int i = 0; i < OCTOPUS; i++) {
			// applying permutation to Wolfram's transition table
			m_TransitionTable[i]= WTransitionTable[ CORRESP[i] ]; 
		}
		Macro.print(4, this, "Rule set to :" + in_RuleNum);
	}
	
	/* Get State */
	final private int GetCell(int i) {
		return m_Array[i];
	}
	
	/* Set State */
	final private void SetCell(int i, int new_state){
		m_Array[i]= new_state;
	}
	
	
	/** get density * */
	final public double GetDensity() {
		int count = 0;
		for (int i = 0; i < GetSize(); i++) {
			count += m_Array[i];
		}
		return (count / (double) GetSize());
	}


	/** reading the state : implements the array * */
	final public int GetCellState(int n) {
		return m_Array[n];
	}


	public void Print() {
		Macro.printNOCR(GetTime() + ":");
		for (int i = 0; i < GetSize(); i++) {
			int st = GetCellState(i);
			if (st == 0) {
				Macro.printNOCR(" O");
			} else {
				Macro.printNOCR(" X");
			}

		}
		Macro.print(" d : " + GetDensity());
		//Macro.print();
	}

	/*--------------------
	 * Test
	 --------------------*/

	final static boolean SHIFTUPDATE= 
		true;
		//false;
	private static final String CLASSNAME = "AutomatonECAFlyWheel";
	
	public static void DoTest() {
			
		Macro.BeginTest(CLASSNAME);
		/************ test *********/
		// static pars
		int ECA= 128;
		int size= 50;
		int time= 20;
		double dini=0.5;
		boolean fixed=true, fixedH= true;
		
		// dynamix pars
		double alpha=1.6282;
		zzzFlyWheelUpdater updater = new zzzFlyWheelUpdater(size);
		updater.SetAlpha(alpha);
		
		// automaton creation
		zzzECA_FlyWheelDynArray spid1= new zzzECA_FlyWheelDynArray(size);
		spid1.SetSynchronyRate(alpha);
		spid1.SetRule(ECA);
		
		// init
		//TODO spid1.Init(dini, fixed, fixedH);
		// shifting
		spid1.LeftShift();
		
		spid1.Print();		
			
		for (int i = 0; i < time; i++) {
			spid1.OneStepBeyond(updater);
			spid1.Print();
		}
		/*********** END TEST **********/
		Macro.EndTest();
		
		
	}


	

	

	
	
	
}
