package components.arrays;

import main.Macro;


/*--------------------
 * abstract class SimpleAutomaton contains reset, advance and read methods 
 * (Buffered, Flywheel, etc.) 
*--------------------*/
public abstract class zzzECA_DynArray extends DynSystem 
implements OneRegisterReadable
{
	
	final static boolean DEFAULT_FIXED_HISTORY= false;
	private static final int ZERO = 0;
	
	public int GetState(int i){
		return GetCellState(i);
	}
	
	/*--------------------
	 * abstract
	 --------------------*/
	abstract protected void InitSetCell(int i, int state);
	abstract public int GetCellState(int i);
	abstract public double GetDensity();
	abstract public void SetRule(int Wcode);
	abstract public void OneStepBeyond();
	
	/*--------------------
	 * attributes
	 --------------------*/

	//	 synchrony rate
	double m_alpha = 1.0;

	
	/*--------------------
	 * construction
	 --------------------*/
	
	public zzzECA_DynArray(int in_Lsize) {
		super(in_Lsize);
	}
	
	/*--------------------
	 * init procedure
	 --------------------*/
	
	@Override
	/** sends reset signal to all cells 
	 called by sig_Init */
	final protected void ResetComponents(){
		for (int i=0; i < GetSize(); i++){
			InitSetCell(i, ZERO);
		}
	}
	
	
	/*--------------------
	 * asynchronous updating
	 --------------------*/

	/** setting the synchrony rate * */
	public void SetSynchronyRate(double in_alpha) {
		m_alpha = in_alpha;
		Macro.print(4,"Synchrony rate set to: " + m_alpha);
	}
	
	
	/*b**************************************************************************
	 * Get / Set
	 --------------------*/
	
	public void Print() {
		Macro.printNOCR(GetTime() + ":");
		for (int i = 0; i < GetSize(); i++) {
			int st = GetCellState(i);
			if (st == 0) {
				Macro.printNOCR(" O");
			} else {
				Macro.printNOCR(" X");
			}

		}
		Macro.print(" d : " + GetDensity());
		Macro.CR();
	}
	

}
