package components.arrays;


public interface TimeReadable {

	int GetTime();
	
}
