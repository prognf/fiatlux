package components.arrays;

/** a collection of OneRegister objects with accessible state 
 * used by measuring devices */
public interface OneRegisterReadable   {

	/** pos : index of the component */
    int GetState(int pos);
		
	int GetSize();
}
