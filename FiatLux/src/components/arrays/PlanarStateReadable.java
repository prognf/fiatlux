package components.arrays;

/* a collection of OneRegister objects with accessible state */
public interface PlanarStateReadable extends OneRegisterReadable  {

	int GetNeighbourhoodSize(int index);
	
	int [] GetNeighbourhoodState(int index);
			
	/* local autocorrelation */
    double GetAutoCorrelation4Of(int index);
	
}
