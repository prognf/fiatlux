package components.arrays;

import components.types.IntC;
import grafix.viewers.LineAutomatonViewer;
import main.Macro;
import topology.basics.SuperTopology;

public class TimeBuffer {

	// convention for simplifying the drawing
	public final static int RESET_STATE= SuperTopology.GHOSTSTATE;

	// for "feeding" the buffer
	LineAutomatonViewer m_source ; 
	
	// buffer
	int [][] m_buffer; 			// stores the values read from source
	final int m_Lsize, m_Tsize;		// constants
	private int m_bufferTime ;  		// current buffer time

	/*--------------------
	 * construction
	 --------------------*/

	public TimeBuffer(int Tsize, int Lsize, LineAutomatonViewer in_source) {
		m_Tsize  = Tsize;
		m_Lsize  = Lsize;
		m_source = in_source;
		//m_time= in_time;

		// memory allocation
		m_buffer = new int[m_Tsize][m_Lsize];
		for (int t = 0; t < m_Tsize; t++) {
			m_buffer[t] = new int[m_Lsize];
		}
		//sig_Init();
	}

	/*--------------------
	 * signals
	 --------------------*/
	
	/** updates current buffer line from source */
	private void UpdateFromSource(){
		m_source.UpdateBuffer(m_buffer[m_bufferTime]);
	}

	public void sig_Upate() {
		UpdateFromSource();
	}

	/** cleans the buffer = puts all cells to RESET_STATE */
	public void sig_Init() {
		for (int t=0; t<m_Tsize; t++){
			for(int cell=0; cell<m_Lsize;cell++){
				m_buffer[t][cell]= RESET_STATE;
			}
		}
		m_bufferTime= -1 ;
		sig_FeedLine() ;
	}

	/** synchronizes with linked automaton */
	public void sig_FeedLine() {
		m_bufferTime =  (m_bufferTime + 1) % GetTsize();
		UpdateFromSource();
	}

	/*--------------------
	 * public  methods
	 --------------------*/

	/** -> buffer Tsize  */
	final public int GetTsize() {
		return m_Tsize;
	}
	/** -> buffer Lsize */
	final public int GetLsize() {
		return m_Lsize;
	}

	final public IntC GetLTsize(){
		return new IntC(m_Lsize, m_Tsize);
	}

	final public int GetTime(){
		return m_bufferTime;
	}

	/** radar mode comes back to initial position */
	public int GetBufferCellStateRadar(int buffer_line, int n) {  
		try {
			int t_buffer =  GetTsize() - buffer_line - 1 ;
			return m_buffer[t_buffer][n];
		} catch (Exception e) {
			Macro.FatalError(e,
					" Invalid call to GetBufferCellState T, N " + buffer_line
					+ " , " + n);
			return -1;
		}
	}

	/**
	 * reading 
	 */
	final public int GetBufferCellState(int buffer_line, int in_pos) {  // GetAutomatonTime()
		try {
			int pos = (in_pos + GetLsize())%GetLsize();
			int t_buffer =  
					(m_bufferTime +  (GetTsize()- buffer_line)) % GetTsize();
			return m_buffer[t_buffer][pos];
		} catch (Exception e) {
			Macro.FatalError(e,
					" Invalid call to GetBufferCellState T, N " + buffer_line
					+ " , " + in_pos);
			return -1;
		}
	}


	
	/* called by mouse-clicks for flipping */
	public void ChangeBufferCurrentGenerationCellState(int x, int newstate) {
		m_buffer[m_bufferTime][x]= newstate;
	}

	
}


/*--------------------
 * saves a space-time diagram (T configurations) in PGM format used by
 * SimulationSampler in_CompFactor is the temporal compression factor
 --------------------*/

/*public void zzzSaveBufferPGM(double in_CompFactor, String in_PathOutFile) {
	final int BORDERWIDTH = 5;
	final int GREY_CODE = 1, BLACK_CODE = 2, WHITE_CODE = 0;
	FLStringList out = new FLStringList();
	out.Add("P2  #PGM RAW format ");
	int X = m_Lsize;
	int T = GetTsize();
	double dt = 1. / in_CompFactor;

	// building list of indices of lines to use
	IntegerList list_indices = new IntegerList();
	for (double t = 0; t < (double) T - Macro.EPSILON; t += dt) {
		list_indices.addVal((int) t);
		// DEBUG
		// Macro.print( "line index " + (int) t + " t : " + t );
	}
	int numlines = list_indices.GetSize();
	int max_index = list_indices.Get(numlines - 1);
	// we add a shift in order to reach index T-1 at the end
	int shift_t = (T - 1) - max_index;
	Macro.print(3, " T " + T + " dt " + dt + " st " + shift_t + " n "
			+ numlines);
	// header
	out.Add("" + (X + 2 * BORDERWIDTH) + " " + (numlines + 2 * BORDERWIDTH)
			+ " # Xsize, Ysize ");
	out.Add("2 # MaxVal ");

	// border lines
	StringBuilder borderline = new StringBuilder();
	for (int x = 0; x < X + 2 * BORDERWIDTH; x++) {
		borderline.append("" + GREY_CODE + " ");
	}
	StringBuilder bordercolumn = new StringBuilder();
	for (int i = 0; i < BORDERWIDTH; i++) {
		bordercolumn.append("" + GREY_CODE + " ");
	}
	// upper border
	for (int i = 0; i < BORDERWIDTH; i++) {
		out.Add(borderline.toString());
	}
	// body
	for (int i = 0; i < numlines; i++) {
		int noshift_index = list_indices.Get(i);
		int line_index = noshift_index + shift_t;
		StringBuilder line = new StringBuilder(bordercolumn.toString()); // left border
		for (int x = 0; x < X; x++) {
			int color = GREY_CODE; // default color (grey)
			int state = GetBufferCellState(line_index, x);
			// color code
			if (state == 1) {
				color = WHITE_CODE;
			}
			if (state == 0) {
				color = BLACK_CODE;
			}
			line.append("").append(color).append(" ");
		}
		line.append(bordercolumn); // right border
		out.Add(line.toString());
	}// for
	for (int i = 0; i < BORDERWIDTH; i++) {
		out.Add(borderline.toString());
	}
	// footer
	out.Add("#end file");
	// writing file
	out.WriteToFile(in_PathOutFile);
}
*/

