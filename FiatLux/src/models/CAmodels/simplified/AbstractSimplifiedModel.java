package models.CAmodels.simplified;

import java.awt.Color;

import components.types.IntC;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import models.CAmodels.binary.BinaryModel;

public abstract class AbstractSimplifiedModel extends BinaryModel {
    private PaintToolKit m_palette = PaintToolKit.EmptyPalette();

    public void addState(Color c) {
        m_palette.AddColor(new FLColor(c));
    }

    @Override
    public PaintToolKit GetPalette() {
        if (m_palette.GetSize() == 0) {
            setStates();
        }

        return m_palette;
    }

    public IntC getDefaultGridSize() {
        return new IntC(100, 100);
    }

    /* TODO : why is this here ??**/
    public int getBorder() {
        return 0;
    }
    public double getScreenSize() { return 550.0; }

    static public int getCountOf(int[] neighborsStates, int searched) {
        int count = 0;
        for (int n : neighborsStates) {
            if (n == searched) {
                count++;
            }
        }
        return count;
    }

    public void setStates() {
        addState(Color.WHITE);
        addState(Color.BLUE);
    }
}
