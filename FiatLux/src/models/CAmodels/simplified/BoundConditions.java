package models.CAmodels.simplified;

/**
 * @purpose Simplify the TopologyCoder names for editable models
 */
public class BoundConditions {
    public final static String Toric = "T", Free = "F";
}
