package models.CAmodels.simplified;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Collections;

import javax.swing.JOptionPane;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;

import components.types.IntC;
import experiment.samplers.CAsimulationSampler;
import grafix.gfxTypes.MacroGFX;
import grafix.windows.SimulationWindowSingle;
import main.Macro;
import topology.basics.SamplerInfo;
import topology.basics.SamplerInfoLinear;
import topology.basics.SamplerInfoPlanar;
import topology.basics.TopologyCoder;

/**
 * NG: A helper class to compile java code
 */
public class FLJavaCompiler {
    private JavaCompiler m_compiler;
    private ClassFileManager m_fileManager;
    private FLjavaSource m_source;

    boolean checkvalidity=false; //set true for debugging
    
    public FLJavaCompiler(FLjavaSource source) {
        m_compiler = ToolProvider.getSystemJavaCompiler();
        if (m_compiler==null){ // TO DEBUG !!!
        	Macro.SystemWarning(" COULD NOT FIND SYSTEM JAVA COMPILER... JAVA \"HOME\" IS :"+ System.getProperty( "java.home"));
        }
        Macro.print(" using local JAVA COMPILER... JAVA \"HOME\" IS :"+ System.getProperty( "java.home"));
        StandardJavaFileManager stdMgr= m_compiler.getStandardFileManager(null, null, null);
		m_fileManager = new ClassFileManager(stdMgr);
        m_source = source;
    }

    public void compile() {
		compile(checkvalidity);
    }

    public void compile(boolean check) {
        StringWriter errorsWritter = new StringWriter();

        Iterable<? extends JavaFileObject> fileObjects = Collections.singletonList(m_source);

        m_compiler.getTask(errorsWritter, m_fileManager, null, null, null, fileObjects).call();
        try {
            errorsWritter.close();
        } catch (IOException ex) {
            System.err.println(ex);
        } catch(Exception e) {
        	Macro.SystemWarning(e, "non handled compitation error");
        }

        if (!errorsWritter.toString().equals("")) {
            JOptionPane.showMessageDialog(null,
                    "Errors:" + System.lineSeparator() + errorsWritter.toString(),
                    "Compilation errors", JOptionPane.ERROR_MESSAGE
            );
        } else if (check) {
            JOptionPane.showMessageDialog(null,
                    "Model check successful",
                    "Code check", JOptionPane.INFORMATION_MESSAGE
            );
        }
    }

    public void execute() {
        try {
        	// this is the main point
            AbstractSimplifiedModel model = (AbstractSimplifiedModel) 
            		m_fileManager.getClassLoader(javax.tools.StandardLocation.CLASS_PATH).loadClass(m_source.getCompleteName()).newInstance();

            SimulationWindowSingle exp;
            try {
                IntC gridSize = model.getDefaultGridSize();
                IntC cellPixSize = new IntC((int) (model.getScreenSize() / (double) gridSize.X()), model.getBorder());

                SamplerInfo sampInfo;
                if (TopologyCoder.getTopologyDimension(model.GetDefaultAssociatedTopology()) == 1) {
                    sampInfo = new SamplerInfoLinear(gridSize.X(), gridSize.Y(), cellPixSize, model, model.GetDefaultAssociatedTopology());
                } else {
                    sampInfo = new SamplerInfoPlanar(gridSize, cellPixSize, model, model.GetDefaultAssociatedTopology());
                }

                // sampInfo.updatingSchemeName = m_SelectUS.GetSelectedUSname();

                CAsimulationSampler sampler = sampInfo.GetSimulationSampler();

                exp = new SimulationWindowSingle(MacroGFX.FLSIMULATION, sampler);
                int dim = sampler.GetTopologyDimension();
                if (dim == 1){	// special setting for dimension one
                    exp.SetIncrementStep(sampler.GetTsize() - 1);
                }

                exp.BuildAndDisplay();
                exp.sig_Init();
            } catch (Exception e) {
                Macro.UserWarning(e, "Could not create Simulation Sampler !");
            }
        } catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null,
                    "Class not found: " + ex.toString(),
                    "ClassNotFoundException", JOptionPane.ERROR_MESSAGE
            );
        } catch (InstantiationException ex) {
            JOptionPane.showMessageDialog(null,
                    "Unable to instantiate the class: " + ex.toString(),
                    "InstantiationException", JOptionPane.ERROR_MESSAGE
            );
        } catch (IllegalAccessException ex) {
            JOptionPane.showMessageDialog(null,
                    "IllegalAccessException: " + ex.toString(),
                    "IllegalAccessException", JOptionPane.ERROR_MESSAGE
            );
        }
    }
}
