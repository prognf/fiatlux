package models.CAmodels.simplified;

import java.awt.Color;

import grafix.gfxTypes.FLColor;

public class zzzNGhighlightColors {
    public static Color
        RED = FLColor.RED,
        BLUE = FLColor.BLUE,
        GREEN = FLColor.GREEN,
        BLACK = FLColor.BLACK,
        WHITE = FLColor.WHITE,
        YELLOW = FLColor.YELLOW,
        ORANGE = FLColor.ORANGE,
        BROWN = FLColor.c_brown,
        PURPLE = FLColor.c_purple;
}
