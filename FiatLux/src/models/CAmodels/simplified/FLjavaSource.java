package models.CAmodels.simplified;

import java.net.URI;

import javax.tools.JavaFileObject;
import javax.tools.SimpleJavaFileObject;

public class FLjavaSource extends SimpleJavaFileObject {
    private String m_completeName;
    private String m_name;
    private String m_code;

    public FLjavaSource(String name, String code, String completeName) {
        super(URI.create("string:///" + name.replace('.','/') + JavaFileObject.Kind.SOURCE.extension), JavaFileObject.Kind.SOURCE);
        m_code = code;
        m_completeName = completeName;
        m_name = name;
    }

    @Override
    public CharSequence getCharContent(boolean ignoreEncodingErrors) {
        return m_code;
    }

    public String getName() {
        return m_name;
    }

    public String getCompleteName() {
        return m_completeName;
    }
}
