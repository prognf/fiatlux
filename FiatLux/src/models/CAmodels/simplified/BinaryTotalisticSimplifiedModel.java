package models.CAmodels.simplified;

import components.allCells.OneRegisterIntCell;

public abstract class BinaryTotalisticSimplifiedModel extends AbstractSimplifiedModel {
    public static int ALIVE = 1, DEAD = 0;

    @Override
    public int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
        return ApplyLocalFunction(in_Cell.CountOccurenceOf(ALIVE), in_Cell.CountOccurenceOf(DEAD), in_Cell.GetState());
    }

    public abstract int ApplyLocalFunction(int nbAlive, int nbDead, int previousState);
}
