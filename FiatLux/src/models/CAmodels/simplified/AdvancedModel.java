package models.CAmodels.simplified;

import components.allCells.OneRegisterIntCell;

public abstract class AdvancedModel extends AbstractSimplifiedModel {
    @Override
    public int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
        int[] neighboors = new int[in_Cell.GetNeighbourhoodSize()];

        for (int i = in_Cell.GetNeighbourhoodSize() - 1; i >= 0; i--) {
            neighboors[i] = in_Cell.GetNeighbour(i).GetState();
        }

        return ApplyLocalFunction(neighboors, in_Cell.GetState());
    }

    public abstract int ApplyLocalFunction(int[] neighbors, int previousState);
}
