package models.CAmodels.simplified;

/**
 * @purpose Simplify the TopologyCoder names for editable models
 * move this to topology package ?
 */
public class Topologies {
    public static String
            GKL = "GK",
            Moore9 = "M9",
            Moore8 = "M8",
            MooreR5 = "MR5",
            vonNeumann4 ="V4",
            vonNeumann5 = "V5",
            WCE = "WCE3",
            Circular = "CI",
            Margolus = "MA",
            ExtendedMoore = "EM",
            RandomMoore = "RDM",
            Hexagonal = "HX",
            Sultra = "SLTR",
            LineR1 = "L1",
            LineR2 = "L2",
            LineR3 = "L3",
            LineR4 = "L4",
            OuterLineR1 = "OL1",
            Cross="X4",
            LVOORHEES="LVOORHEES",
            Toom = "T3",
            ExtendedToom = "T6",
            ToomInv = "TINV";
}
