package models.CAmodels.typing;

public interface IMaxStateAwareModel {
    int GetMaxStateValue();
    int getMaxStabilityCols();
    int getMaxStatusRows();
}
