package models.CAmodels.typing;

import components.allCells.TwoRegisterCell;

public interface ITransitionResultAwareModel {
    int[] GetTransitionResult(TwoRegisterCell in_Cell);
}
