package models.CAmodels.autotile;

import initializers.CAinit.OneRegisterInitializer;

public class BuildEvilKinitializer extends OneRegisterInitializer {

	@Override
	protected void SubInit() {
		int X= GetXsize();
		int Y= GetYsize();
		for (int y=1; y<Y; y++) {
			int state= ((y-1)%4)+1;
				InitStateXY(0, y, state);
		}
		for (int x=1; x<X; x++) {
			int state= ((x)%4)+1;
				InitStateXY(x, 0, state);
		}

	}


	/** overrides */
/*	public FLPanel GetSpecificPanel() {
//		FLPanel panel = FLPanel.NewPanel();
		return panel;
	}
*/
	
}
