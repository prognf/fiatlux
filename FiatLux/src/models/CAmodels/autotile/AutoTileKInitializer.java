package models.CAmodels.autotile;

import grafix.gfxTypes.IntField;
import grafix.gfxTypes.elements.FLPanel;
import initializers.CAinit.OneRegisterInitializer;
import main.Macro;

public class AutoTileKInitializer extends OneRegisterInitializer {

	final static int K=AutoTileKModel.K;
	final static int DEFMARGIN=3; // margin of "security" where pavement is not modified  
	private static final int DEFINITSTYLE = 1;

	IntField m_initStyle= new IntField("Init Style",3,DEFINITSTYLE);
	IntField m_margin= new IntField("margin",3,DEFMARGIN);

	@Override
	protected void SubInit() {
		int X= GetXsize();
		int Y= GetYsize();
		switch (m_initStyle.GetValue()){
		case 1:
			SquareInOut(X,Y,true);
			break;
		case 2:
			SquareInOut(X,Y,false);
			break;
		case 3:
			DiabolusInTessela(X,Y); // special
			break;
		case 4:
			threeColorInit(X,Y);
			break;

		default:
			Macro.print("wrong init case in AutoTileKinit");
		}
	}

	private void threeColorInit(int X, int Y) {
		for (int y=0 ; y<Y; y++) {
			for (int x=0; x<X; x++) {
				int q= (y%2==0)?
						x%3:(x+2)%3;
				InitStateXY(x, y, q);
			}
		}
		
	}

	/** init method 1 **/
	private void DiabolusInTessela(int X, int Y) {
		int Xc = X / 2, Yc= Y/ 2;
		for (int y=0 ; y<Yc; y++) {
			for (int x=0; x<X; x++) {
				int st= (x+2*y) % 4;
				InitStateXY(x, Yc + y, st);
			}
		}
		for (int y=0 ; y<Yc-1; y++) {
			for (int x=0; x<X; x++) {
				int st= (x+ 2*y  + 1) % 4;
				InitStateXY(x, Yc - y - 2 , st);
			}
		}
		int y=1;
		for (int x=0 ; x<Xc; x++) {
			int st= (x+ 2*y  + 1) % 4;
			InitStateXY(x, Yc - 1 , st);
		}
		for (int x=Xc ; x<X; x++) {
			int st= (x+2*y) % 4;
			InitStateXY(x, Yc - 1 , st);
		}
	}

	/** empty square or random states inside**/
	protected void SquareInOut(int X, int Y, boolean randomInside) {
		for (int y=0; y <Y; y++) {
			for (int x=0; x<X;x++) {
				int state= (x+2*y)%K;
				InitStateXY(x, y, state);
			}
		}
		int M= m_margin.GetValue();
		for (int y=M; y <Y-M; y++) {
			for (int x=M; x<X-M;x++) {
				int state= randomInside?
						RandomInt(K):0;
				InitStateXY(x, y, state);
			}
		}
	}
	


	/** overrides */
	public FLPanel GetSpecificPanel() {
		FLPanel panel = FLPanel.NewPanel();
		panel.SetBoxLayoutY();
		panel.Add(m_initStyle, m_margin,
				this.GetRandomizer().GetSeedControl());
		panel.setOpaque(false);
		return panel;
	}


}
