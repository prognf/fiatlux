package models.CAmodels.autotile;

import java.awt.Color;
import java.awt.Polygon;
import java.awt.event.MouseEvent;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import components.arrays.RegularDynArray;
import components.types.IntC;
import grafix.gfxTypes.FLColor;
import grafix.viewers.OneRegisterAutomatonViewer;
import main.Macro;

public class AutoTileKViewer extends OneRegisterAutomatonViewer 
{

	private static final Color COLHIGHLIGHT = FLColor.c_black, 
			COLEVIL=FLColor.c_lightgrey;
	private static final Color COL_LINESBETWEENCELLS_STRNG = FLColor.c_black;
	private static final Color COL_LINESBETWEENCELLS_LIGHT = FLColor.c_lightgrey;

	private static final float RATIOCIRCLE = 0.3F;
	private static final boolean DRAWEVIL = false;

	Polygon N, E, S, W;

	public AutoTileKViewer(IntC in_CellSize, IntC in_GridSize,
			RegularDynArray in_Automaton) {
		super(in_CellSize, in_GridSize, in_Automaton);

		N= new Polygon();		E= new Polygon();
		S= new Polygon();		W= new Polygon();
		super.ComputeFourHeadCenteredTriangles(N,E,S,W); // init four triangles
		m_StabilityDisplay= true;
		//	Macro.Debug("ZZZZZZZZZZZZZz");
	}

	/**
	 * Override this if post drawing is needed
	 * @param gc the Graphics2D
	 */
	@Override
	protected void DrawPostEffect() {
		Macro.Debug("POST XXXXXXXXXXXXXXXX");
		int X= getXsize(), Y=getYsize();
		// drawing balls
		SetColor(COLHIGHLIGHT);
		for (int y=0; y<Y; y++){
			for (int x=0; x<X; x++){
				if (DRAWEVIL) {
					SetColor(COLEVIL);
					if (!isLocallyEvil(x,y)){
						super.DrawCellCircle(x, y, RATIOCIRCLE);
					}
				}
				SetColor(COLHIGHLIGHT);
				if (hasAdjacencyError(x,y)){
					SetColor(FLColor.black);
					super.DrawCellCircle(x, y, RATIOCIRCLE);
				}
			}
		}
		//DrawArrows();
	}

	private void DrawArrows() {
		int X= getXsize(), Y=getYsize();
		// drawing arrows
		for (int y=0; y<Y; y++){
			for (int x=0; x<X; x++){

				int Xpos = getXpos(x);
				int Ypos = getYpos(y);
				int 
				q1= GetCellStateXY(x, y), 
				q2= GetCellStateXY(x, y+1),
				q3= GetCellStateXY(x+1, y);
				int dx=(q3-q1+3)%3;
				int dy=(q2-q1+3)%3;

				if ( dx==1 ){
					SetColor(COL_LINESBETWEENCELLS_LIGHT);
					super.translateOrigin(Xpos+m_CellDist, Ypos);	
					FillPolygon(N);
					ResetOrigin();	
				} else if ( dx==2 ){
					SetColor(COL_LINESBETWEENCELLS_STRNG);
					super.translateOrigin(Xpos+m_CellDist, Ypos+m_CellDist);	
					m_g2D.fillRect(-1, -m_SquareSize, 3, m_SquareSize);
					FillPolygon(S);
					ResetOrigin();
				}
				if ( dy==1 ){
					SetColor(COL_LINESBETWEENCELLS_STRNG);
					super.translateOrigin(Xpos, Ypos);	
					m_g2D.fillRect(0, -1, m_SquareSize, 3);
					FillPolygon(W);
					ResetOrigin();
				} else if ( dy==2 ){
					SetColor(COL_LINESBETWEENCELLS_LIGHT);
					super.translateOrigin(Xpos+m_CellDist, Ypos);	
					FillPolygon(E);
					ResetOrigin();	
				}
				/*if (hasAdjacencyError(x,y) && hasAdjacencyError(x,y+1)){
					super.
				}*/
			}
		}

	}

	private boolean hasAdjacencyError(int x, int y) {
		int st= GetCellStateXY(x,y);
		boolean error= 
				(st==GetCellStateXY(x+1, y))||
				(st==GetCellStateXY(x, y+1))||
				(st==GetCellStateXY(x, y-1))||
				(st==GetCellStateXY(x-1, y));
		return error;
	}

	private boolean isLocallyEvil(int x, int y) {
		int st= GetCellStateXY(x,y);
		int stW=GetCellStateXY(x+1, y);
		int stN=GetCellStateXY(x, y+1);
		int stS=GetCellStateXY(x, y-1);
		int stE=GetCellStateXY(x-1, y);
		Integer [] col= new Integer [] {st, stW, stN, stS, stE};
		Set<Integer> colset= new HashSet<Integer>( Arrays.asList(col) );
		return colset.size()==4;
	}


	private int GetCellStateXY(int x, int y) {
		return GetCell(x, y).GetState();
	}


	/** OVERRIDE IF NEEDED */
	@Override
	public void ProcessMouseEventXY(int x, int y, MouseEvent event) {
		int newq= (GetCell(x,y).GetState() + 1)%4;
		//Macro.Debug("coucoucoucou");
		GetCell(x, y).InitState(newq);
		this.UpdateView();
	}
}