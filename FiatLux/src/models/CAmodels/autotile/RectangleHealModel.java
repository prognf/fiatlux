package models.CAmodels.autotile;

import components.allCells.Cell;
import components.allCells.ClassicalCACell;
import components.allCells.OneRegisterIntCell;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import initializers.SuperInitializer;
import main.Macro;
import main.tables.PlotterSelectControl;
import models.CAmodels.ClassicalModel;
import topology.basics.TopologyCoder;
import topology.zoo.NESW_TM;

/*--------------------
 * models excitable media
 *--------------------*/
public class RectangleHealModel extends ClassicalModel {

	final public static String NAME="HealMaignan"; 
	final static int LIM=1; // neighb. required to transmit excitation
	private static final 
	FLColor [] COL={ 
		FLColor.c_white, 
		FLColor.c_grey1, FLColor.c_grey2, FLColor.c_grey3,
		FLColor.c_yellow, FLColor.c_orange, FLColor.c_brown,
		FLColor.c_blue2, 
		FLColor.c_red, FLColor.c_cyan, 
		FLColor.c_green, FLColor.c_darkgreen, FLColor.c_black,
		FLColor.c_superRed, FLColor.c_lightcyan,
		FLColor.c_purple};

	final static int  
	NEUTRAL=0, 
	A=1, B=2, C=3, 
	RA=4, RB=5, RC=6, 
	INITCENTER=7, 
	FRONT1=8, PRECIPITATE=9,
	PARADOX1=10, ERROR=11, COMPLETION=12,
	FRONT2= 13, PARADOX2=14,
	SOLID=15;


	/*--------------------
	 * Attributes
	 --------------------*/

	OneRegisterIntCell m_currentcell;

	/*--------------------
	 * Constructor
	 --------------------*/

	public RectangleHealModel() {
		super();
		testLoop(A);
		testLoop(B);
		testLoop(C);
	}	

	/*--------------------
	 * main method
	 --------------------*/



	private void testLoop(int q) {
		Macro.fPrint("%d %d %d ", q, statePlus(q), stateMinus(q));
	}

	/** Greenberg-Hastings model: simple reac-diff model */
	public final int ApplyLocalFunction(OneRegisterIntCell cell) {
		return maignan(cell);
	}

	/*--------------------
	 * overrides & implementations
	 --------------------*/

	public SuperInitializer GetDefaultInitializer() {
		return new HydraInitializer();
	}

	@Override
	public Cell GetNewCell() {
		ClassicalModel mod = this;
		return new ClassicalCACell(mod);
	}

	@Override
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_FV4;
	}


	/*--------------------
	 * Get & Set methods
	 --------------------*/

	private int maignan(OneRegisterIntCell cell) {
		m_currentcell= cell;
		int state = cell.GetState();
		int countF= count(FRONT2) /*+ count(FRONT2)*/;
		//int countN= count(NEUTRAL);
		switch(state){
		case INITCENTER:
			return INITCENTER;
		case NEUTRAL:
			if (count(INITCENTER)>=2){// immediate collision
				return PRECIPITATE;
			}
			if (countF==4){ //collision "type 1"
				return PRECIPITATE;
			}
			if (present(INITCENTER))
				return FRONT1;
			if (present(FRONT2)){ // front 2 ensures propagation
				return FRONT1;
			}
			return NEUTRAL;
		case FRONT1:
			return FRONT2;
		case FRONT2:
			if (present(PARADOX1)||present(PARADOX2)||present(ERROR))
				return PARADOX1;
		
			int countReverse= count(RA)+count(RB)+count(RC);
			if (countReverse>0){
				return PARADOX1;
			}
			if (present(SOLID)){ // to prevent propagation of precipitation
				return PARADOX1;
			}
			if (count(FRONT2)>0) //collision "type 2"
				return PRECIPITATE; 
			int nPropagating= count(A) + count(B) + count(C);
			boolean precipate=(nPropagating>=3); // case to deal with strange positions (3,1)
			boolean clean= !present((PRECIPITATE));
			if (precipate && clean)
				return PRECIPITATE;
			else {
				return distancePlusOne();
				//return FRONT2;
			}
		case A:
			return revert(A)?RA:completion(A);
		case B:
			return revert(B)?RB:completion(B); 
		case C: 
			return revert(C)?RC:completion(C); 
		case RA:
			return RA;
		case RB:
			return RB;
		case RC:
			return RC;
		case PRECIPITATE:
			return SOLID;
		case SOLID:
			return SOLID;
		case PARADOX1:
			return PARADOX2;
		case PARADOX2:
			if (correctionPossible(A)) return A;
			if (correctionPossible(B)) return B;
			if (correctionPossible(C)) return C;
			if (surroundedByReturn()) return ERROR; // to avoid eternal blinking
			return PARADOX1; // important : go back to state 1 (synchro)
		case ERROR:
			return ERROR;
		case COMPLETION:
			return COMPLETION;
		default:
			Macro.FatalError("case problem:"  + state);
			return Macro.ERRORint;
		}
	}

	/** special for PARADOX 2**/
	private boolean surroundedByReturn() {
		int countNeighbValid=count(PARADOX2)+count(COMPLETION)+count(SOLID)+count(RA)+count(RB)+count(RC);
		return !present(A)&&!present(B)&&!present(C)&&(countNeighbValid==4);
	}

	private int distancePlusOne() {
		int distPlusOne= ERROR;
		if (present(INITCENTER))
			return A;
		if (present(A)&& !present(B)&& !present(C)){
			distPlusOne= B;
		}
		if (present(B)&& !present(C)&& !present(A)){
			distPlusOne= C;
		}
		if (present(C)&& !present(A)&& !present(B)){
			distPlusOne= A;
		}
		return distPlusOne;
	}

	/** completion to have enclosing rectangle **/
	private int completion(int q) {
		int sum= count(INITCENTER)+count(RA)+count(RB)+count(RC)+ count(SOLID)+count(COMPLETION);
		return (sum>=2)?COMPLETION:q;
	}

	/** says if "paradox" can be corrected **/
	private boolean correctionPossible(int q) {
		return !present(q) && present(stateMinus(q)) && !present(statePlus(q));
	}

	private boolean revert(int q) {
		return present(PRECIPITATE) || propagateReverse(q);
	}

	/** says if backwards propagation needs to be applied **/
	private boolean propagateReverse(int q) {
		switch (q) {
		case A:
			return present(RB);
		case B:
			return present(RC);
		case C:
			return present(RA);
		default:
			Macro.FatalError("zinzin");
			return Macro.ERRORboolean;
			}
	}

	/** case of immediate retropropagation **/
	private boolean paradox(int q) {
		return present(q) && present(statePlus(q)) && present(stateMinus(q)) && nofront() && (!present(PARADOX1));
	}

	/** no front is present **/
	private boolean nofront() {
		return !(present(FRONT2)/*||present(FRONT2)*/); 
	}

	private boolean collide(int q){
		
		return present(q) && (!present(NEUTRAL) && nofront());
	}

	/** advance in the loop A,B,C **/
	private int statePlus(int q){
		return (q%3)+1;
	}

	/** back in the loop A,B,C **/
	private int stateMinus(int q){
		return ((q+1)%3)+1;
	}

	/** approximation : count 2 only **/
	private boolean insafecontact(int state) {
		int other1=-1, other2=-1;
		switch(state){
		case RA:
			other1=RB; other2=RC; break;
		case RB:
			other1=RC; other2=RA; break;
		case RC:
			other1=RA; other2=RB; break;
		}
		return present(state) && (!present(other1)) && (!present(other2));
	}

	private boolean oppositeNeutralsNonNeutrals() {
		if (m_currentcell.GetNeighbourhoodSize()<4)
			return false;
		int 
		sN= m_currentcell.ReadNeighbourState(NESW_TM.NORTH),
		sE= m_currentcell.ReadNeighbourState(NESW_TM.EAST),
		sS= m_currentcell.ReadNeighbourState(NESW_TM.SOUTH),
		sW= m_currentcell.ReadNeighbourState(NESW_TM.WEST);
		return ( 
				((sN==NEUTRAL)&&(sS==NEUTRAL))||
				((sE==NEUTRAL)&&(sW==NEUTRAL))   );
	}

	private int loop() {
		if (present(A))	return B;
		if (present(B))	return C;
		if (present(C))	return A;
		return NEUTRAL;
	}

	final private int count(int state) {
		return m_currentcell.CountOccurenceOf(state);
	}

	final private boolean present(int state) {
		return m_currentcell.CountOccurenceOf(state)>0;
	}

	@Override
	public String [] GetPlotterSelection2D() {
		return PlotterSelectControl.REACDIFFSELECT;
	}

	/*--------------------
	 * GFX
	 --------------------*/
	/** overrides */
	@Override
	public PaintToolKit GetPalette() {
		PaintToolKit palette= new PaintToolKit(COL);
		return palette;
	}

	/*public FLPanel GetSpecificPanel() {
		FLPanel panel= FLPanel.NewPanel();
		return panel;
	}*/





}
