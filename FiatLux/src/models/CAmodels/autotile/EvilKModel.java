package models.CAmodels.autotile;

import java.util.HashSet;
import java.util.Set;

import components.allCells.OneRegisterIntCell;
import components.arrays.RegularDynArray;
import components.types.IntC;
import components.types.IntegerList;
import components.types.ProbaPar;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.viewers.AutomatonViewer;
import initializers.SuperInitializer;
import models.CAmodels.ClassicalModel;
import topology.basics.PlanarTopology;
import topology.basics.SuperTopology;
import topology.basics.TopologyCoder;

/*--------------------
 * models excitable media
 *--------------------*/
public class EvilKModel extends ClassicalModel {

	final public static String NAME="EvilK"; 
	private static final 
	FLColor [] COL={ 
			FLColor.c_white, FLColor.c_lightgreen, FLColor.c_red, 
			FLColor.c_black, FLColor.c_blue, FLColor.c_cyan};
    static final int K=3;
	
	// case k=3
	private final ProbaPar PROBACHG = new ProbaPar("probachg", 0.1);

	/*--------------------
	 * Attributes
	 --------------------*/

	/*--------------------
	 * Constructor
	 --------------------*/

	public EvilKModel() {
		super();


	}	

	/*--------------------
	 * main method
	 --------------------*/



	/** Greenberg-Hastings model: simple reac-diff model */
	public final int ApplyLocalFunction(OneRegisterIntCell cell) {
		//
		return autotileKStates(cell);
		//return autotile3States(cell);
	}

	/*--------------------
	 * overrides & implementations
	 --------------------*/

	public SuperInitializer GetDefaultInitializer() {
		return new AutoTileKInitializer();
	}

	@Override
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TV4;
	}

	@Override
	/** specific viewer */
	public AutomatonViewer GetPlaneAutomatonViewer(
			IntC cellPixSize, 
			SuperTopology in_TopologyManager, RegularDynArray in_Automaton) {	
		 PlanarTopology topo2D = (PlanarTopology) in_TopologyManager;
		AutomatonViewer av= new AutoTileKViewer(cellPixSize, topo2D.GetXYsize(), in_Automaton);
		av.SetPalette(GetPalette());
		return av;
	}
	/*--------------------
	 * Get & Set methods
	 --------------------*/

	/** special case k=3 */
	private int autotile3States(OneRegisterIntCell cell) {
		//OneRegisterIntCell cellN=cell.GetNeighbour(0);
		int state = cell.GetState();
		int 
		N= cell.ReadNeighbourState(0),
		E= cell.ReadNeighbourState(1),
		S= cell.ReadNeighbourState(2),
		W= cell.ReadNeighbourState(3);
		int [] count = new int[K];
		count[N]++;
		count[E]++;
		count[S]++;
		count[W]++;
		if (count[state]==0) {  // no conflict
			return state;
		} 
		// high level of constraints
		if (count[state]>=2) { 
			return RandomInt(K);
		}
		//
		IntegerList nonPresentStates= new IntegerList(); 
		for (int i=0; i<K; i++) {
			if (count[i]==0) 
				nonPresentStates.add(i);
		}
		int newstate=state;
		if (nonPresentStates.isEmpty()) {
			if (RandomEventDouble(PROBACHG)){
				newstate= RandomInt(K);
			}
		} else {
			newstate= nonPresentStates.ChooseOneAtRandom(this.GetRandomizer()); //FIXEME
		}
		return newstate;
	}



	/** k=4,5 */
	private int autotileKStates(OneRegisterIntCell cell) {
		int state = cell.GetState();
		int 
		N= cell.ReadNeighbourState(0),
		E= cell.ReadNeighbourState(1),
		S= cell.ReadNeighbourState(2),
		W= cell.ReadNeighbourState(3);
		int [] count = new int[K];
		count[N]++;
		count[E]++; 
		count[S]++;
		count[W]++;
		Set<Integer> statesPresent= new HashSet<Integer>();
		statesPresent.add(state);
		statesPresent.add(N);
		statesPresent.add(E);
		statesPresent.add(S);
		statesPresent.add(W);
		
		
		if ( (count[state]==0) && (statesPresent.size()==4) ) {  // no conflict
			return state;
		} 
		// conflict
		IntegerList nonPresentStates= new IntegerList(); 
		for (int i=0; i<K; i++) {
			if (count[i]==0) 
				nonPresentStates.add(i);
		}
		int newstate;
		if (nonPresentStates.isEmpty()) {
			newstate= RandomInt(K);
		} else {
			newstate= nonPresentStates.ChooseOneAtRandom(this.GetRandomizer()); //FIXEME
		}
		return newstate;
	}


	/** k= 5*/
	/*private int autotileFiveStates(OneRegisterIntCell cell) {
		int state = cell.GetState();
		int 
		N= cell.ReadNeighbourState(0),
		E= cell.ReadNeighbourState(1),
		S= cell.ReadNeighbourState(2),
		W= cell.ReadNeighbourState(3);
		int [] count = new int[5];
		count[N]++;
		count[E]++;
		count[S]++;
		count[W]++;
		if ( (state==S) || (state==W) ){
			int newstate= minNonOccupiedState(count);
			return newstate;
		} else {
			return state;
		}

	}*/



	private int minNonOccupiedState(int[] count) {
		for (int i=0; i<5; i++) {
			if (count[i]==0)
				return i;
		}
		return 0;
	}

	/*--------------------
	 * GFX
	 --------------------*/
	/** overrides */
	/*
	public FLPanel GetSpecificPanel() {
//		FLPanel panel= FLPanel.NewPanel(this.GetRandomizer().GetSeedControl());
//		return panel;
	}*/

	/** overrides */
	@Override
	public PaintToolKit GetPalette() {
		PaintToolKit palette= new PaintToolKit(COL);
		return palette;
	}


}
