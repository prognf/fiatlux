package models.CAmodels.autotile;

public class SSTnode {

	final int m_x, m_y;
	private int m_incoming, m_outgoing; // counting links 
	final boolean m_isBorder;
	
	int m_state; // for "tokens"
	
	public SSTnode(int x, int y, boolean isborder) {
		m_x = x;
		m_y = y;
		m_outgoing=0;
		m_incoming=0;
		m_isBorder= isborder;
	}

	public String toString(){
		return String.format("(%d,%d)|>%d,%d>|",m_x,m_y,m_incoming,m_outgoing);
	}
	
	boolean isbalanced(){
		return (m_incoming==m_outgoing);
	}
	
	boolean hasOutOdd(){
		return m_outgoing%2==1;
	}

	public boolean hasInOdd() {
		return m_incoming%2==1;
	}
	
	public void diffOut(int d) {
		m_outgoing+=d;		
	}
	
	public void diffIn(int d) {
		m_incoming+=d;	
	}

	public boolean hasmoreInthanOut() {
		return m_incoming > m_outgoing;
	}

	public boolean isconnected() {
		return (m_incoming != 0)|| (m_outgoing!=0);
	}

	public void addToken(){
		m_state=1;
	}
	
	public void removeToken(){
		m_state=0;
	}

	public boolean hasToken() {
		return m_state==1;
	}
	
}
