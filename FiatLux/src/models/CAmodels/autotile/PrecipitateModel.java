package models.CAmodels.autotile;

import components.allCells.Cell;
import components.allCells.ClassicalCACell;
import components.allCells.OneRegisterIntCell;
import components.arrays.RegularDynArray;
import components.types.IntC;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.viewers.AutomatonViewer;
import initializers.SuperInitializer;
import main.Macro;
import models.CAmodels.ClassicalModel;
import topology.basics.PlanarTopology;
import topology.basics.SuperTopology;
import topology.basics.TopologyCoder;
import topology.zoo.NESW_TM;

/*--------------------
 * models excitable media
 *--------------------*/
public class PrecipitateModel extends ClassicalModel {

	final public static String NAME="Precipitate"; 
	final static int LIM=1; // neighb. required to transmit excitation
	private static final 
	FLColor [] COL={ 
		FLColor.c_white, 
		FLColor.c_grey1, FLColor.c_grey2, FLColor.c_grey3,
		FLColor.c_yellow, FLColor.c_orange, FLColor.c_brown,
		FLColor.c_blue, 
		FLColor.c_red, FLColor.c_cyan, 
		FLColor.c_green, FLColor.c_yellow, 
		FLColor.c_medred,
		FLColor.c_black};

	final static int  
	NEUTRAL=0, 
	A=1, B=2, C=3, 
	INITCENTER=7, 
	FRONT=8, PRECIPITATE=9,
	PARADOX=10, SOLID=11, 
	COMPLETION=12,
	ERROR= 13;


	/*--------------------
	 * Attributes
	 --------------------*/

	OneRegisterIntCell m_currentcell;

	/*--------------------
	 * Constructor
	 --------------------*/

	public PrecipitateModel() {
		super();
		testLoop(A);
		testLoop(B);
		testLoop(C);
	}	

	/*--------------------
	 * main method
	 --------------------*/



	private void testLoop(int q) {
		Macro.fPrint("%d %d %d ", q, statePlus(q), stateMinus(q));
	}

	/** Greenberg-Hastings model: simple reac-diff model */
	public final int ApplyLocalFunction(OneRegisterIntCell cell) {
		return maignan(cell);
	}

	/*--------------------
	 * overrides & implementations
	 --------------------*/

	public SuperInitializer GetDefaultInitializer() {
		return new HydraInitializer();
	}

	@Override
	public Cell GetNewCell() {
		ClassicalModel mod = this;
		return new ClassicalCACell(mod);
	}

	@Override
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TV4;
	}


	/*--------------------
	 * Get & Set methods
	 --------------------*/

	private int maignan(OneRegisterIntCell cell) {
		m_currentcell= cell;
		int state = cell.GetState();
		int countF= count(FRONT) /*+ count(FRONT2)*/;
		//int countN= count(NEUTRAL);
		switch(state){
		case INITCENTER:
			return INITCENTER;
		case NEUTRAL:
			return neutralCase();
		case FRONT:
			return frontCase();
		case A:
			return A;
		case B:
			return B; 
		case C: 
			return C; 
		case PRECIPITATE:
			return SOLID;
		case PARADOX:
			if (present(INITCENTER)) return A; // immediate correction
			if (correctionPossible(A)) return A;
			if (correctionPossible(B)) return B;
			if (correctionPossible(C)) return C;
			return PARADOX; // a paradox can be corrected "later"
		case SOLID:
			return SOLID;
		case COMPLETION:
			return COMPLETION;
		case ERROR:
			return ERROR;
		default:
			Macro.FatalError("case problem:"  + state);
			return Macro.ERRORint;
		}
	}

	private int neutralCase() {
		boolean precip1= (count(INITCENTER)>=2);// sandwich situation
		boolean precip2= breakCase();
		boolean precip3= (count(FRONT)==4); // collision "type 1" (2,2)
		if (precip1 || precip2 || precip3)
			return PRECIPITATE;
		if (present(INITCENTER)||(present(FRONT))){ // front 2 ensures propagation
			return FRONT;
		}
		return NEUTRAL;
	}

	private boolean breakCase() {
		int 
		sN= m_currentcell.ReadNeighbourState(NESW_TM.NORTH),
		sE= m_currentcell.ReadNeighbourState(NESW_TM.EAST),
		sS= m_currentcell.ReadNeighbourState(NESW_TM.SOUTH),
		sW= m_currentcell.ReadNeighbourState(NESW_TM.WEST);
		boolean c1= (sN==sS) && (sE==sW);
		boolean c2= ((sN==NEUTRAL) && (sE==FRONT))||((sN==FRONT) && (sE==NEUTRAL));
		return c1 && c2;
	}

	/** FRONT -> X **/
	private int frontCase() {
		int nPropagating= count(A) + count(B) + count(C);

		if (count(FRONT)>=2){ // "abnormal" situation 
			// collision "type 2" only when we are not next to INITCENTER
			return PRECIPITATE; 
		}
		boolean precipate=(nPropagating>=3);
		boolean clean= absent(PRECIPITATE)||absent(SOLID);
		if (/* DEBUG */ false && precipate && clean)
			return PRECIPITATE;
		else {
			return distancePlusOne();
		}
	}



	/** FRONT case **/
	private int distancePlusOne() {
		int distPlusOne= PARADOX;  //default case if no correction is possible
		if (present(INITCENTER))
			return A;
		if (present(A)&& !present(B)&& !present(C)){
			distPlusOne= B;
		}
		if (present(B)&& !present(C)&& !present(A)){
			distPlusOne= C;
		}
		if (present(C)&& !present(A)&& !present(B)){
			distPlusOne= A;
		}
		return distPlusOne;
	}

	/** says if "paradox" can be corrected **/
	private boolean correctionPossible(int q) {
		// was third condition needed ??
		return !present(q) && present(stateMinus(q)) /*&& !present(statePlus(q)) */;
	}

	/** case of immediate retropropagation **/
	private boolean paradox(int q) {
		return present(q) && present(statePlus(q)) && present(stateMinus(q)) && nofront() && (!present(PARADOX));
	}

	/** no front is present **/
	private boolean nofront() {
		return !(present(FRONT)/*||present(FRONT2)*/); 
	}


	/** advance in the loop A,B,C **/
	private int statePlus(int q){
		return (q%3)+1;
	}

	/** back in the loop A,B,C **/
	private int stateMinus(int q){
		return ((q+1)%3)+1;
	}

	

	/*--------------------
	 * basis
	 --------------------*/

	final private int count(int state) {
		return m_currentcell.CountOccurenceOf(state);
	}

	/** count **/
	private boolean cnt(int q, int n) {
		return count(q)==n;
	}

	final private boolean present(int state) {
		return m_currentcell.CountOccurenceOf(state)>0;
	}

	private boolean absent(int q) {
		return !present(q);
	}

	/*--------------------
	 * GFX
	 --------------------*/
	/** overrides */
	@Override
	public PaintToolKit GetPalette() {
		PaintToolKit palette= new PaintToolKit(COL);
		return palette;
	}

	/** override 
	 * 2D Viewer : called by simulation sampler  */
	public AutomatonViewer GetPlaneAutomatonViewer(
			IntC cellPixSize, 
			SuperTopology in_TopologyManager, RegularDynArray in_Automaton) {	
		PlanarTopology topo2D = (PlanarTopology) in_TopologyManager;
		AutomatonViewer av= new RectangleMaignanViewer(cellPixSize, topo2D.GetXYsize(), in_Automaton);
		av.SetPalette(GetPalette());
		return av;
	}

}
