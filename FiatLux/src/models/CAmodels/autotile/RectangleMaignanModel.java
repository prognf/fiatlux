package models.CAmodels.autotile;

import components.allCells.Cell;
import components.allCells.ClassicalCACell;
import components.allCells.OneRegisterIntCell;
import components.arrays.RegularDynArray;
import components.types.IntC;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.viewers.AutomatonViewer;
import initializers.SuperInitializer;
import main.Macro;
import models.CAmodels.ClassicalModel;
import topology.basics.PlanarTopology;
import topology.basics.SuperTopology;
import topology.basics.TopologyCoder;
import topology.zoo.NESW_TM;

/*--------------------
 * models excitable media
 *--------------------*/
public class RectangleMaignanModel extends ClassicalModel {

	final public static String NAME="Maignan"; 
	final static int LIM=1; // neighb. required to transmit excitation
	private static final 
	FLColor [] COL={ 
		FLColor.c_white, 
		FLColor.c_grey1, FLColor.c_grey2, FLColor.c_grey3,
		FLColor.c_yellow, FLColor.c_orange, FLColor.c_brown,
		FLColor.c_blue, 
		FLColor.c_red, FLColor.c_cyan, 
		FLColor.c_green, FLColor.c_darkgreen, 
		FLColor.c_medred,
		FLColor.c_black};

	final static int  
	NEUTRAL=0, 
	A=1, B=2, C=3, 
	RA=4, RB=5, RC=6, 
	INITCENTER=7, 
	FRONT=8, PRECIPITATE=9,
	PARADOX=10, SOLID=11, 
	COMPLETION=12,
	ERROR= 13;


	/*--------------------
	 * Attributes
	 --------------------*/

	OneRegisterIntCell m_currentcell;

	/*--------------------
	 * Constructor
	 --------------------*/

	public RectangleMaignanModel() {
		super();
		testLoop(A);
		testLoop(B);
		testLoop(C);
	}	

	/*--------------------
	 * main method
	 --------------------*/



	private void testLoop(int q) {
		Macro.fPrint("%d %d %d ", q, statePlus(q), stateMinus(q));
	}

	/** Greenberg-Hastings model: simple reac-diff model */
	public final int ApplyLocalFunction(OneRegisterIntCell cell) {
		return maignan(cell);
	}

	/*--------------------
	 * overrides & implementations
	 --------------------*/

	public SuperInitializer GetDefaultInitializer() {
		return new HydraInitializer();
	}

	@Override
	public Cell GetNewCell() {
		ClassicalModel mod = this;
		return new ClassicalCACell(mod);
	}

	@Override
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_FV4;
	}


	/*--------------------
	 * Get & Set methods
	 --------------------*/

	private int maignan(OneRegisterIntCell cell) {
		m_currentcell= cell;
		int state = cell.GetState();
		int countF= count(FRONT) /*+ count(FRONT2)*/;
		//int countN= count(NEUTRAL);
		switch(state){
		case INITCENTER:
			return INITCENTER;
		case NEUTRAL:
			if (count(INITCENTER)>=2)  // sandwich situation
				return PRECIPITATE;
			if (countF==4) //collision "type 1"
				return PRECIPITATE;
			if (present(INITCENTER)||(present(FRONT))){ // front 2 ensures propagation
				return FRONT;
			}
			return NEUTRAL;
		case FRONT:
			return frontCase();
		case A:
			return revert(A)?RA:completion(A);
		case B:
			return revert(B)?RB:completion(B); 
		case C: 
			return revert(C)?RC:completion(C); 
		case RA:
			return RA;
		case RB:
			return RB;
		case RC:
			return RC;
		case PRECIPITATE:
			return SOLID;
		case PARADOX:
			if (present(INITCENTER)) return A; // immediate correction
			if (correctionPossible(A)) return A;
			if (correctionPossible(B)) return B;
			if (correctionPossible(C)) return C;
			return PARADOX; // a paradox can be corrected "later"
		case SOLID:
			return SOLID;
		case COMPLETION:
			return COMPLETION;
		case ERROR:
			return ERROR;
		default:
			Macro.FatalError("case problem:"  + state);
			return Macro.ERRORint;
		}
	}

	/** FRONT -> X **/
	private int frontCase() {
		int nPropagating= count(A) + count(B) + count(C);
		
		boolean 
			safeProp1= cnt(FRONT,1) && cnt(PARADOX,1) && cnt(NEUTRAL,2),
			safeProp2= cnt(FRONT,2) && cnt(PARADOX,1),
			safeProp3= cnt(FRONT,1) && cnt(PARADOX,1) && cnt(NEUTRAL,1),
			safeProp4= cnt(FRONT,1) && cnt(PARADOX,1) && (nPropagating==1),
			safeProp5= false;//cnt(FRONT,1) && cnt(PARADOX,2) ;
		
		if (safeProp1 || safeProp2|| safeProp3 || safeProp4 || safeProp5) 
			return PARADOX;
		
		if (present(PRECIPITATE)||present(SOLID)){
			return PARADOX;
		}
		if (count(FRONT)>0){ // "abnormal" situation 
			if (count(INITCENTER)==1) 
				return PARADOX;
			else	 // collision "type 2" only when we are not next to INITCENTER
				return PRECIPITATE; 
		}
		/*
		// see case X-X and case X-0-X case X-X-X
		if (present(PARADOX)&&(count(FRONT)<=2)) 
			return PARADOX;
	*/
		boolean precipate=(nPropagating>=3);
		boolean clean= absent(PRECIPITATE)||absent(SOLID);
		if (/* DEBUG */ false && precipate && clean)
			return PRECIPITATE;
		else {
			return distancePlusOne();
		}
	}



	/** FRONT case **/
	private int distancePlusOne() {
		int distPlusOne= PARADOX;  //default case if no correction is possible
		if (present(INITCENTER))
			return A;
		if (present(A)&& !present(B)&& !present(C)){
			distPlusOne= B;
		}
		if (present(B)&& !present(C)&& !present(A)){
			distPlusOne= C;
		}
		if (present(C)&& !present(A)&& !present(B)){
			distPlusOne= A;
		}
		return distPlusOne;
	}

	/** completion to have enclosing rectangle **/
	private int completion(int q) {
		int sum= count(INITCENTER)+count(RA)+count(RB)+count(RC)+ count(SOLID)+count(COMPLETION);
		return (sum>=2)?COMPLETION:q;
	}

	/** says if "paradox" can be corrected **/
	private boolean correctionPossible(int q) {
		// was third condition needed ??
		return !present(q) && present(stateMinus(q)) /*&& !present(statePlus(q)) */;
	}

	private boolean revert(int q) {
		return present(PRECIPITATE) || propagateReverse(q);
	}

	/** says if backwards propagation needs to be applied **/
	private boolean propagateReverse(int q) {
		switch (q) {
		case A:
			return present(RB);
		case B:
			return present(RC);
		case C:
			return present(RA);
		default:
			Macro.FatalError("zinzin");
			return Macro.ERRORboolean;
		}
	}

	/** case of immediate retropropagation **/
	private boolean paradox(int q) {
		return present(q) && present(statePlus(q)) && present(stateMinus(q)) && nofront() && (!present(PARADOX));
	}

	/** no front is present **/
	private boolean nofront() {
		return !(present(FRONT)/*||present(FRONT2)*/); 
	}

	private boolean collide(int q){

		return present(q) && (!present(NEUTRAL) && nofront());
	}

	/** advance in the loop A,B,C **/
	private int statePlus(int q){
		return (q%3)+1;
	}

	/** back in the loop A,B,C **/
	private int stateMinus(int q){
		return ((q+1)%3)+1;
	}

	/** approximation : count 2 only **/
	private boolean insafecontact(int state) {
		int other1=-1, other2=-1;
		switch(state){
		case RA:
			other1=RB; other2=RC; break;
		case RB:
			other1=RC; other2=RA; break;
		case RC:
			other1=RA; other2=RB; break;
		}
		return present(state) && (!present(other1)) && (!present(other2));
	}

	private boolean oppositeNeutralsNonNeutrals() {
		if (m_currentcell.GetNeighbourhoodSize()<4)
			return false;
		int 
		sN= m_currentcell.ReadNeighbourState(NESW_TM.NORTH),
		sE= m_currentcell.ReadNeighbourState(NESW_TM.EAST),
		sS= m_currentcell.ReadNeighbourState(NESW_TM.SOUTH),
		sW= m_currentcell.ReadNeighbourState(NESW_TM.WEST);
		return ( 
				((sN==NEUTRAL)&&(sS==NEUTRAL))||
				((sE==NEUTRAL)&&(sW==NEUTRAL))   );
	}


	/*--------------------
	 * basis
	 --------------------*/

	final private int count(int state) {
		return m_currentcell.CountOccurenceOf(state);
	}

	/** count **/
	private boolean cnt(int q, int n) {
		return count(q)==n;
	}
	
	final private boolean present(int state) {
		return m_currentcell.CountOccurenceOf(state)>0;
	}

	private boolean absent(int q) {
		return !present(q);
	}

	/*--------------------
	 * GFX
	 --------------------*/
	/** overrides */
	@Override
	public PaintToolKit GetPalette() {
		PaintToolKit palette= new PaintToolKit(COL);
		return palette;
	}
	
	/** override 
	 * 2D Viewer : called by simulation sampler  */
	public AutomatonViewer GetPlaneAutomatonViewer(
			IntC cellPixSize, 
			SuperTopology in_TopologyManager, RegularDynArray in_Automaton) {	
		 PlanarTopology topo2D = (PlanarTopology) in_TopologyManager;
		AutomatonViewer av= new RectangleMaignanViewer(cellPixSize, topo2D.GetXYsize(), in_Automaton);
		av.SetPalette(GetPalette());
		return av;
	}

}
