package models.CAmodels.autotile;

import java.util.ArrayList;
import java.util.List;

import components.randomNumbers.StochasticSource;
import components.types.IntC;
import components.types.IntPar;
import components.types.IntegerList;
import main.Macro;
import models.CAmodels.autotile.SSTedge.DIRHV;
/*-------------------------------------------------------------------------------
- 
------------------------------------------------------------------------------*/
public class SSTgraph extends StochasticSource {

	private static final int YINITCOND = 4;

	//--------------------------------------------------------------------------
	//- attributes
	//--------------------------------------------------------------------------

	final int m_X, m_Y;

	final SSTedge [][] m_edgeHL, m_edgeVL;
	final ArrayList<SSTedge> m_edgeListHV;


	SSTnode [][] m_array;
	//final ArrayList<APnode> m_nodeList;

	IntegerList m_xinside, m_yinside, 
	m_xrangeP, m_yrangeP, m_xrangeE, m_yrangeE;

	// for applying the rules synchronously
	ArrayList<SSTedge> m_edgeLsetblue, m_edgeLsetwhite;
	ArrayList<SSTnode> m_nodeLaddtoken, m_nodeLdeltoken;

	IntPar m_Yinitcond;


	//--------------------------------------------------------------------------
	//- construction
	//--------------------------------------------------------------------------
	public SSTgraph(IntC XYsize) {
		m_X= XYsize.X();
		m_Y= XYsize.Y();
		m_array= new SSTnode[m_X+1][m_Y+1];
		m_edgeHL= new SSTedge[m_X][m_Y];
		m_edgeVL= new SSTedge[m_X][m_Y];
		m_edgeListHV= new ArrayList<SSTedge>();

		initBufferLists();
		initHelperLists();
		initPoints();
		initEdges();

		m_Yinitcond= new IntPar("Yinit", YINITCOND);
	}

	private void initBufferLists(){
		m_edgeLsetblue= new ArrayList<SSTedge>();
		m_edgeLsetwhite=  new ArrayList<SSTedge>();
		m_nodeLaddtoken= new ArrayList<SSTnode>();
		m_nodeLdeltoken= new ArrayList<SSTnode>();
	}


	private void initHelperLists() {
		m_xinside= IntegerList.Interval(1, m_X-1);
		m_yinside= IntegerList.Interval(1, m_Y-1);
		m_xrangeP= IntegerList.Interval(0, m_X);
		m_yrangeP= IntegerList.Interval(0, m_Y);
		m_xrangeE= IntegerList.Interval(0, m_X-1);
		m_yrangeE= IntegerList.Interval(0, m_Y-1);

	}

	private void initPoints() {
		for (int y : m_yinside){
			for (int x: m_xinside){
				addnode(x,y,false);
			}
		}
		// E,W borders
		for (int y : m_yinside){
			addnode(0,y,true);
			addnode(m_X,y,true);
		}
		// N?S borders
		for (int x : m_xinside){
			addnode(x,0,true);
			addnode(x,m_Y,true);
		}

	}

	private void initEdges() {
		// H edges
		for (int y : m_yinside ){
			for (int x : m_xrangeE){
				addNewEdge(x,y,DIRHV.H);
			}
		}
		// V edges
		for (int x : m_xinside){
			for (int y : m_yrangeE){
				addNewEdge(x,y,DIRHV.V);
			}
		}
	}

	private void addnode(int x, int y, boolean b) {
		SSTnode newnode= new SSTnode(x, y, b);
		m_array[x][y]= newnode;
	}

	private void addNewEdge(int x, int y, DIRHV type) {
		SSTnode 
		source= getpoint(x,y),
		destination= (type==DIRHV.H)?getpoint(x+1,y):getpoint(x, y+1);
		if (destination==null){
			Macro.fPrint(" bad destination, source: %s",source);
		}
		SSTedge niouedge= new SSTedge(source, destination, type);
		if (type==DIRHV.H) {
			m_edgeHL[x][y]= niouedge;
		} else {
			m_edgeVL[x][y]= niouedge;
		}
		m_edgeListHV.add(niouedge);
	}
	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------


	/*** INITIAL CONDITION **/
	public void sig_Init(){
		initPoints();
		initEdges();
		initBufferLists();
		getNode(m_X-1, 1).addToken();   // corner S-E 
		
		//entering
		getEdgeH(0, YINITCOND+1).flipDirState(); //West
		getEdgeV(m_X-2,m_Y-1).flipDirState();//South
		//outgoing
		getEdgeH(m_X-1, m_Yinitcond.GetVal()).flipDirState(); //West
		getEdgeV(2,0).flipDirState(); //South
		
	}

	public void initRandom(){
		for (SSTedge edge : m_edgeListHV){
			if (RandomTrueFalse()){
				edge.flipDirState();
			}
		}
	}


	/**
	 * DYNAMICS : applying one step
	 */
	void onestep(){
		//Macro.fDebug("--- new step --- ");
		for (int y : m_yinside){
			for (int x : m_xinside){
				applyUpdateRule(x,y);		
			}
		}//for
		checkNocontradiction();

		for (SSTedge edge : m_edgeLsetblue){
			//Macro.fDebug("edge to change:%s", edge);
			edge.setDir(true);
		}
		for (SSTedge edge : m_edgeLsetwhite){
			//Macro.fDebug("edge to change:%s", edge);
			edge.setDir(false);
		}
		// adding tokens
		for (SSTnode node : m_nodeLaddtoken){
			node.addToken();
		}
		for (SSTnode node : m_nodeLdeltoken){
			node.removeToken();
		}
		initBufferLists();
	}


	private void checkNocontradiction() {
		checkIntersectionEmpty(m_nodeLaddtoken, m_nodeLdeltoken); 
		checkIntersectionEmpty(m_edgeLsetblue, m_edgeLsetwhite);
		//Macro.FatalError(" contradiction in the lists of edges or nodes [add & remove] !!");
	}

	public <T>void checkIntersectionEmpty(List<T> list1, List<T> list2) {
		for (T t : list1) {
			if(list2.contains(t)) {
				Macro.FatalError(" NON EMPTY INTERSECTION ! element:"+ t);
			}
		}
	}

	/** 
	 * MAIN DYNAMICS 
	 * **/
	private void applyUpdateRule(int x, int y) {
		SSTnode node= getNode(x, y);
		/*if (borderSouth(x, y)){
			ruleBorderSouthGoLeft(x,y);
		} else*/ {
			//boolean nodeLefttoken= getNode(x-1,y).hasToken();
			if (node.hasToken() && node.hasmoreInthanOut()){
				ruleAlignPath(x,y);
			} else {
				ruleApplyPropagation(x, y);
			}
		}
	}

	private void ruleAlignPath(int x, int y) {
		SSTnode node= getNode(x, y), nodeLeft= getNode(x-1,y), 
				nodeAbove=getNode(x,y+1);
		SSTedge 
		edgeVleft= getEdgeV(x-1,y), edgeVright= getEdgeV(x,y),
		edgeHabove= getEdgeH(x-1,y+1);
		m_edgeLsetwhite.add(edgeVright);
		m_edgeLsetblue.add(edgeVleft);
		//m_nodeLdeltoken.add(node);
		m_nodeLaddtoken.add(nodeLeft);
		if (!edgeHabove.isBlue()){ // "normal" case
			m_nodeLaddtoken.add(nodeAbove);
		} else { // "corner" case
			m_edgeLsetwhite.add(edgeHabove);
		}
	}

	private void ruleBorderSouthGoLeft(int x, int y) {
		SSTnode node= getNode(x, y), nodeAbove= getNode(x,y+1);
		SSTedge edgeVleft= getEdgeV(x-1,y), edgeVabove= getEdgeV(x,y);

		if (node.hasmoreInthanOut()/* && edgeVabove.isBlue() */){
			//Macro.fDebug(" EDGE ABOVE: %s", edgeVabove);
			m_edgeLsetwhite.add(edgeVabove);
			m_edgeLsetblue.add(edgeVleft);
			m_nodeLaddtoken.add(nodeAbove);
		}	
	}

	/** adds the edges that change by propagation rule 
	 * @param addToken **/
	private void ruleApplyPropagation(int x, int y) {
		SSTnode node= getNode(x, y), nodeLeft= getNode(x-1,y);
		SSTedge edgeSouthV= getEdgeV(x, y-1);
		if ( !nodeLeft.hasToken() && node.hasmoreInthanOut()){
			boolean eastoccupied= getEdgeH(x, y).m_dirstate || borderEast(x,y);
			if (!eastoccupied){
				m_edgeLsetblue.add( getEdgeH(x, y) );
			} else {
				if ( borderSouth(x, y) ){
					Macro.FatalError(" should not be border south !!!");
				} else {
					if (!edgeSouthV.isBlue()) {
						m_edgeLsetblue.add( edgeSouthV );
					}
				}  
			}
		}
	}


	public boolean borderEast(int x, int y) {
		return (x==m_X-1); 
	}

	public boolean borderSouth(int x, int y) {
		return (y==1);
	}


	/*private void initLineH(int y, boolean reg) {
		for (int x : m_xrangeE){
			addNewEdge(x, y, DIRHV.H);
			getEdgeH(x,y).setDir(reg);
		}
	}

	private void initLineV(int x, boolean reg) {
		for (int y : m_yrangeE ){
			addNewEdge(x, y, DIRHV.V);
			getEdgeV(x,y).setDir(reg);
		}
	}*/


	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------


	public SSTnode getNode(int x, int y) {
		return m_array[x][y];
	}

	/** for the viewer's constructor **/
	public IntC getXYsizegridpoint(){
		return new IntC(m_X+1,m_Y+1);
	}

	private SSTnode getpoint(int x, int y) {
		return m_array[x][y];
	}

	public SSTedge getEdgeH(int x, int y) {
		return m_edgeHL[x][y];
	}

	public SSTedge getEdgeV(int x, int y) {
		return m_edgeVL[x][y];
	}

	public SSTedge getEdgeH(IntC xy) {
		return getEdgeH(xy.X(),xy.Y());
	}

	public SSTedge getEdgeV(IntC xy) {
		return getEdgeV(xy.X(),xy.Y());
	}

	private void setEdgeHdir(IntC xy, boolean reg) {
		getEdgeH(xy).m_dirstate = reg;
	}

	private void setEdgeVdir(IntC xy, boolean reg) {
		getEdgeV(xy).m_dirstate = reg;
	}

	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------

	public void printState(){
		Macro.CR();
		for (int y=m_Y-1; y>0; y--){ // backwards 
			Macro.print(shapeDataV(y));
			Macro.print(shapeDataH(y));
		}
		Macro.print(shapeDataV(0));
		/*for (APnode node : m_nodeList){
			if (node.isconnected()){
				Macro.fPrint( " active:%s",node);
			}
		}*/
	}


	private String shapeDataH(int yline){
		StringBuilder sout= new StringBuilder();
		for (int x : m_xrangeE){
			SSTedge edge= getEdgeH(x,yline);
			sout.append(".").append(edge.m_dirstate ? ">" : "<");
		}
		sout.append(".");
		return sout.toString();
	}

	private String shapeDataV(int yline){
		StringBuilder sout= new StringBuilder(" ");
		for (int x : m_xinside){
			SSTedge edge= getEdgeV(x,yline);
			sout.append(" ").append(edge.m_dirstate ? "V" : "A");
		}
		sout.append(" ");
		return sout.toString();
	}



}
