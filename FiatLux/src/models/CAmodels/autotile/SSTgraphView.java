package models.CAmodels.autotile;

import java.awt.Color;
import java.awt.Polygon;

import architecture.multiAgent.tools.DIR;
import components.types.IntC;
import grafix.gfxTypes.FLColor;
import grafix.viewers.GridViewer;
import models.CAmodels.autotile.SSTedge.DIRHV;

public class SSTgraphView extends GridViewer {

	private static final float RATIOCIRCLE = 0.4F;
	private static final float RATIOTRIANGLE = .5F;
	private static final Color[] COLARROWS = 
		{ FLColor.c_grey2, FLColor.c_darkblue,
		FLColor.c_darkblue, FLColor.c_grey2 };
	private static final Color BACKGROUND = FLColor.c_grey0;
	private static final FLColor COL1BLACK = FLColor.c_black,
			COL2WHITE = FLColor.c_white;
	// tokens
	private static final Color COLTOKEN = FLColor.c_red;
	// showing border south
	private static final Color BORDERSOUTHCOL = FLColor.c_green;

	Polygon mN = new Polygon(), mE = new Polygon(), mS = new Polygon(),
			mW = new Polygon();
	Polygon[] TABTRIANGLE = { mN, mE, mS, mW };

	final IntC m_gridDim;
	final SSTgraph m_graph;

	public SSTgraphView(IntC cellPixSize, SSTgraph graph) {
		super(cellPixSize, graph.getXYsizegridpoint());
		m_gridDim = graph.getXYsizegridpoint();
		int len = (int) (m_CellDist * RATIOTRIANGLE);
		computeFourTrianglesInSq(mN, mE, mS, mW, len);// init four triangles
		m_graph = graph;
	}

	public void computeFourTrianglesInSq(Polygon tN, Polygon tE, Polygon tS,
			Polygon tW, int lensq) {
		//Macro.Debug("computing four triangles...");
		int l = lensq, h = lensq / 2;
		computeTriangle(tN, lensq, 0, 0, l, 0, h, l);
		computeTriangle(tE, lensq, 0, 0, l, h, 0, l);
		computeTriangle(tS, lensq, 0, l, l, l, h, 0);
		computeTriangle(tW, lensq, l, l, l, 0, 0, h);
	}

	private void computeTriangle(Polygon p, int len, int x1, int y1, int x2,
			int y2, int x3, int y3) {
		p.addPoint(x1, len - y1);
		p.addPoint(x2, len - y2);
		p.addPoint(x3, len - y3);
	}

	@Override
	protected void DrawSystemState() {

		SetColor(BACKGROUND);
		DrawBackground();
		// nodes
		for (int y : m_graph.m_yrangeP) {
			for (int x : m_graph.m_xrangeP) {
				SSTnode node =m_graph.getNode(x, y);
				if (node!=null){
					drawnode(node);	
				}
			}
		}

		// H lines
		for (int y : m_graph.m_yinside) {
			for (int x : m_graph.m_xrangeE) {
				drawdir(x, y, DIRHV.H);
			}
		}
		// V lines
		for (int x : m_graph.m_xinside) {
			for (int y : m_graph.m_yrangeE) {
				drawdir(x, y, DIRHV.V);
			}
		}
	}

	private void drawnode(SSTnode node) {
		boolean colchoice =
				// m_graph.getNode(x,y).hasOutOdd();
				// m_graph.getNode(x,y).hasInOdd();
				node.hasmoreInthanOut();
		SetColor(colchoice ? COL2WHITE : COL1BLACK);
		DrawCellCircle(node.m_x, node.m_y, RATIOCIRCLE);
		if (node.hasToken()){
			SetColor(COLTOKEN);
			DrawCellCircle(node.m_x, node.m_y, RATIOCIRCLE / 2);
		}
		// those cells for which borderSouth is true are identified 
		// with a green circle
		SetColor(BORDERSOUTHCOL);
		if (m_graph.borderSouth(node.m_x,node.m_y)){
			DrawCellCircle(node.m_x, node.m_y, RATIOCIRCLE / 3);
		}

	}

	private void drawdir(int x, int y, DIRHV orientation) {
		IntC xy = new IntC(x, y);
		IntC dxy;
		// triangles
		int len = m_CellDist / 4;
		if (orientation == DIRHV.H) {
			dxy = new IntC(3 * len, len);
		} else {
			dxy = new IntC(len, -len);
		}
		DIR dir;
		if (orientation == DIRHV.H) {
			dir = m_graph.getEdgeH(x, y).m_dirstate ? DIR.EAST : DIR.WEST;
		} else {
			dir = m_graph.getEdgeV(x, y).m_dirstate ? DIR.SOUTH : DIR.NORTH;
		}
		int pos = dir.ordinal();
		SetColor(COLARROWS[pos]);
		IntC xypos = super.getXYpos(x, y);
		xypos.Add(dxy);
		super.translateOrigin(xypos);

		super.FillPolygon(TABTRIANGLE[pos]);
		ResetOrigin(); // to reset the Translate !!
	}

}
