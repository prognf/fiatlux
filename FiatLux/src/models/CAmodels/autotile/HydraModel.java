package models.CAmodels.autotile;

import components.allCells.Cell;
import components.allCells.ClassicalCACell;
import components.allCells.OneRegisterIntCell;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import initializers.SuperInitializer;
import main.Macro;
import main.tables.PlotterSelectControl;
import models.CAmodels.ClassicalModel;
import topology.basics.TopologyCoder;
import topology.zoo.Moore8TM;

/*--------------------
 * models excitable media
 *--------------------*/
public class HydraModel extends ClassicalModel {

	final public static String NAME="HYDRA"; 
	final static int LIM=1; // neighb. required to transmit excitation
	private static final 
	FLColor [] COL={ 
		FLColor.c_white, FLColor.c_grey1, FLColor.c_red, 
		FLColor.c_black, FLColor.c_blue, FLColor.c_cyan};

	final static int  NEUTRAL=0, REFRACT=1, PROPAGATE=2, 
			INITCENTER=3, BOUNCE=4, ECHO=5;

	/*--------------------
	 * Attributes
	 --------------------*/



	// Dynamically processing of the palette when changing the num of states
	private PaintToolKit m_palette; 

	/*--------------------
	 * Constructor
	 --------------------*/

	public HydraModel() {
		super();
	}	

	/*--------------------
	 * main method
	 --------------------*/



	/** Greenberg-Hastings model: simple reac-diff model */
	public final int ApplyLocalFunction(OneRegisterIntCell cell) {
		return hydraOne(cell);
	}

	/*--------------------
	 * overrides & implementations
	 --------------------*/

	public SuperInitializer GetDefaultInitializer() {
		return new HydraInitializer();
	}

	@Override
	public Cell GetNewCell() {
		ClassicalModel mod = this;
		return new ClassicalCACell(mod);
	}

	@Override
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_FM8;
	}


	/*--------------------
	 * Get & Set methods
	 --------------------*/


	private int hydraOne(OneRegisterIntCell cell) {
		int state = cell.GetState();
		int INeighb= cell.CountOccurenceOf(INITCENTER);
		int pNeighb= cell.CountOccurenceOf(PROPAGATE);
		int bNeighb= cell.CountOccurenceOf(BOUNCE);
		int eNeighb= cell.CountOccurenceOf(ECHO);
		switch(state){
		case BOUNCE:
			return BOUNCE;
		case INITCENTER:
			return INITCENTER;
		case NEUTRAL:
			if (INeighb>0){ // from init to propagate
				return PROPAGATE;
			}
			if (pNeighb+bNeighb>3){
				return BOUNCE;
			}
			if (cell.GetNeighbourhoodSize()==8){ // non-boundary cell
				int 
				sN= cell.ReadNeighbourState(Moore8TM.N),
				sS= cell.ReadNeighbourState(Moore8TM.S),
				sE= cell.ReadNeighbourState(Moore8TM.E),
				sW= cell.ReadNeighbourState(Moore8TM.W);
				if ( // "abnormal" case
						((sN==PROPAGATE)&&(sS==PROPAGATE)) ||
						((sE==PROPAGATE)&&(sW==PROPAGATE)) ){
					return BOUNCE;
				} else{
					boolean burn =	(pNeighb >= LIM );
					return burn? PROPAGATE : 0; // fresh cell burns
				}		
			}
		case PROPAGATE:
			if (pNeighb==5){
				return BOUNCE;
			} else 
				return REFRACT;
		case REFRACT:
			if (bNeighb+eNeighb>0){
				return ECHO;
			} 
			return REFRACT;
		case ECHO:
			//if (eNeighb>0)
			//	return 
			return ECHO;
		default:
			Macro.FatalError("case problem");
			return Macro.ERRORint;
		}
	}



	@Override
	public String [] GetPlotterSelection2D() {
		return PlotterSelectControl.REACDIFFSELECT;
	}

	/*--------------------
	 * GFX
	 --------------------*/
	/** overrides */
	@Override
	public PaintToolKit GetPalette() {
		m_palette= new PaintToolKit(COL);
		return m_palette;
	}




	/*public FLPanel GetSpecificPanel() {
		FLPanel panel= FLPanel.NewPanel();
		return panel;
	}*/





}
