package models.CAmodels.autotile;

import components.allCells.Cell;
import components.allCells.ClassicalCACell;
import components.allCells.OneRegisterIntCell;
import components.arrays.RegularDynArray;
import components.types.IntC;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.viewers.AutomatonViewer;
import initializers.SuperInitializer;
import initializers.CAinit.BinaryInitializer;
import main.Macro;
import models.CAmodels.ClassicalModel;
import topology.basics.PlanarTopology;
import topology.basics.SuperTopology;
import topology.basics.TopologyCoder;
import topology.zoo.Moore8TM;

/*--------------------
 * models excitable media
 *--------------------*/
public class RectangleAreaModel extends ClassicalModel {

	final public static String NAME="RectangleArea"; 
	private static final 
	FLColor [] COL={ 
		FLColor.c_white, FLColor.c_red,
		FLColor.c_brown, 
		FLColor.c_blue, FLColor.c_green,
		FLColor.c_black /*anomaly*/};
	/*, FLColor.c_orange, FLColor.c_brown,
		FLColor.c_blue, 
		FLColor.c_red, FLColor.c_cyan, 
		FLColor.c_green, FLColor.c_darkgreen, 
		FLColor.c_medred,
		FLColor.c_black};*/

	final static int  
	NEUTRAL=0, INIT=1, WALL=2, 
	NWc=3, SEc=4,
	ANOMALY=5;


	/*--------------------
	 * Attributes
	 --------------------*/

	OneRegisterIntCell m_currentcell;

	/*--------------------
	 * Constructor
	 --------------------*/

	public RectangleAreaModel() {
		super();
	}	

	/*--------------------
	 * main method
	 --------------------*/


	/** Greenberg-Hastings model: simple reac-diff model */
	public final int ApplyLocalFunction(OneRegisterIntCell cell) {
		return rectangleArea(cell);
	}

	/*--------------------
	 * overrides & implementations
	 --------------------*/

	public SuperInitializer GetDefaultInitializer() {
		BinaryInitializer init= new BinaryInitializer();
		init.SetInitRate(0.01);
		return init;
	}

	@Override
	public Cell GetNewCell() {
		ClassicalModel mod = this;
		return new ClassicalCACell(mod);
	}

	@Override
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TM8;
	}


	/*--------------------
	 * Get & Set methods
	 --------------------*/

	private int rectangleArea(OneRegisterIntCell cell) {
		m_currentcell= cell;
		int 
			stSE= cell.GetNeighbour(Moore8TM.SE).GetState(),
			stNW= cell.GetNeighbour(Moore8TM.NW).GetState();
				
		int state = cell.GetState();
		int countI= count(INIT) ;
		switch(state){
		case INIT:
			if (present(INIT)) // an INIT should not see an y other INIT
				return ANOMALY;
			if ( present(NWc) && present(ANOMALY) )
				return NWc;   //movement of NWc
			return INIT;
		case NEUTRAL:
			if (countI>1) {
				return ANOMALY;
			}
			if (stSE==INIT){
				return NWc;
			}
			if (stNW==INIT){
				return SEc;
			}
			if (countI==1){
				return WALL;
			}
			return NEUTRAL;
		case WALL:
			return WALL;
		case NWc:
			return NWc;
		case SEc:
			return SEc;
		case ANOMALY:
			return ANOMALY;
		default:
			Macro.FatalError("case problem:"  + state);
			return Macro.ERRORint;
		}
	}



	/*--------------------
	 * basis
	 --------------------*/

	final private int count(int state) {
		return m_currentcell.CountOccurenceOf(state);
	}

	final private boolean present(int state) {
		return m_currentcell.CountOccurenceOf(state)>0;
	}

	private boolean absent(int q) {
		return !present(q);
	}

	/*--------------------
	 * GFX
	 --------------------*/
	/** overrides */
	@Override
	public PaintToolKit GetPalette() {
		PaintToolKit palette= new PaintToolKit(COL);
		return palette;
	}
	
	/** override 
	 * 2D Viewer : called by simulation sampler  */
	public AutomatonViewer GetPlaneAutomatonViewer(
			IntC cellPixSize, 
			SuperTopology in_TopologyManager, RegularDynArray in_Automaton) {	
		 PlanarTopology topo2D = (PlanarTopology) in_TopologyManager;
		AutomatonViewer av= new RectangleMaignanViewer(cellPixSize, topo2D.GetXYsize(), in_Automaton);
		av.SetPalette(GetPalette());
		return av;
	}


}
