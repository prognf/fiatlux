package models.CAmodels.autotile;

import components.types.FLString;
import grafix.gfxTypes.IntField;
import grafix.gfxTypes.StringTextField;
import grafix.gfxTypes.controllers.OnOffControl;
import grafix.gfxTypes.elements.FLPanel;
import initializers.CAinit.OneRegisterInitializer;

public class HydraInitializer extends OneRegisterInitializer {
	final static String CLASSNAME= Thread.currentThread().getStackTrace()[1].getClassName();

	public enum INITSTYLE { POINTSLINE, SPARSE, CENTERPOINT }
	public INITSTYLE m_InitStyle = INITSTYLE.POINTSLINE; 
	private static final int DEF_DEPARTURES = 2;
//	private static final int BORDERSATE = 0;

	private static final int DEFDX = 2, DEFDY=0;
	private static final boolean DEFINISTYLE = false; // true = rnd

	/*--------------------
	 * attributes
	 * 	 --------------------*/
	int m_dim = -1; // 1D or 2D ?

	StringTextField m_initCommand;
	
	
	
	IntField mF_FireNum = new IntField("departures:", 3, DEF_DEPARTURES);
	/*IntField mF_dx= new IntField("dx:", 3, DEFDX);
	IntField mF_dy= new IntField("dy:", 3, DEFDY);*/
	OnOffControl mF_rndIni= new OnOffControl("rnd ini",DEFINISTYLE);
	
	/*--------------------
	 * construction
	 --------------------*/

	public HydraInitializer() {
		m_initCommand= new StringTextField(10, "I=1 X=2 Y=3");
	}

	/*--------------------
	 * implementations
	 --------------------*/


	public void SubInit() {
		int X=GetXsize(), Y= GetYsize();
		int q=RectangleMaignanModel.INITCENTER;
		// points
		if (!mF_rndIni.IsFirstItemSelected()){
			for (int i=0; i < mF_FireNum.GetValue(); i++){
				int dx= RandomInt(3*X/4), dy= RandomInt(3*Y/4);
				InitStateXY(X/4+dx, Y/4+dy,q);
			}	
		} else {
			ApplyInitCommand();
		}
	}

	private void ApplyInitCommand() {
		int X=GetXsize(), Y= GetYsize();
		int q=RectangleMaignanModel.INITCENTER;
		String scommand= m_initCommand.GetText();
		int dx= FLString.IParseWithControl(scommand, "X="),
			dy=	FLString.IParseWithControl(scommand, "Y=");
		int X0= (X-dx)/2, Y0= (Y-dy)/2;
		InitStateXY(X0, Y0,q);
		InitStateXY(X0+dx, Y0+dy,q);
	}


	public FLPanel GetSpecificPanel() {
		FLPanel p = FLPanel.NewPanelVertical(mF_FireNum, mF_rndIni, m_initCommand);
		FLPanel Control = FLPanel.NewPanel(p, GetRandomizer());
		p.setOpaque(false);
		Control.setOpaque(false);
		return Control;
	}

	/*--------------------
	 * Other methods
	 --------------------*/


	public void SetInitCond(String input){
		m_initCommand.parseInput(input);
	}



}// end class
