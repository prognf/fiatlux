package models.CAmodels.autotile;

import main.Macro;

public class SSTedge {


	enum DIRHV { H, V }

    final SSTnode m_nA, m_nB;
	final DIRHV m_type;
	boolean m_dirstate;  // true = direct ; false = inverse [eqv. to "isBlue" ]

	public SSTedge(SSTnode source, SSTnode destination, DIRHV type) {
		if ((source==null)||(destination==null)){
			Macro.FatalError(" null source or dest. ");
		}
		m_nA = source;
		m_nB = destination;
		m_type= type;
	}

	public String toString(){
		String sdir=""+(m_dirstate?'+':'-');
		return String.format(" %s--[%s]-->%s ", m_nA, sdir, m_nB);
	}

	/** important for dynamics **/
	public void flipDirState() {
		//Macro.fDebug("flipping edge %s with state %b", this, m_dirstate);
		int d= m_dirstate?-1:1;

		if (m_type==DIRHV.H){
			diffO(m_nA,d);
			diffI(m_nB,d);
		} else {
			diffI(m_nA,d);
			diffO(m_nB,d);
		}
		m_dirstate=!m_dirstate;
	}

	private void diffO(SSTnode node, int d) {
		node.diffOut(d);
	}

	private void diffI(SSTnode node, int d) {
		node.diffIn(d);
	}

	/** calls flipdir **/
	public void setDir(boolean dir) {
		if (m_dirstate!= dir){
			flipDirState();
		}
	}

	public boolean isBlue() {
		return m_dirstate; 
	}
}
