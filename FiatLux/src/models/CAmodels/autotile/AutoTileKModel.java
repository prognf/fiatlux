package models.CAmodels.autotile;

import components.allCells.OneRegisterIntCell;
import components.types.IntegerList;
import components.types.ProbaPar;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import initializers.CAinit.OneRegisterInitializer;
import models.CAmodels.ClassicalModel;
import topology.basics.TopologyCoder;

/*--------------------
 * models excitable media
 *--------------------*/
public class AutoTileKModel extends ClassicalModel {

	final public static String NAME="AutotileK"; 
	final static int CVAL=150;
	//FLColor [] COL={ FLColor.c_white, new FLColor(255,CVAL,CVAL), new FLColor(100,250,100), new FLColor(CVAL,CVAL,255), 
//			FLColor.c_black, FLColor.c_black};
	
FLColor [] COL={ FLColor.c_white, FLColor.c_orange, FLColor.c_darkred, FLColor.c_darkgreen, 
					FLColor.c_black, FLColor.c_black};
					
	static final int THREE=3, FOUR = 4, FIVE=5;
    static final int K=FOUR;
	
    //99FF99
    //FF6666
    
    
    //9999FF  ~purple
    //00FF00 green
    //FF6666 darkorange
    //FFFF66 light yellow
    
	// case k=3
	private final ProbaPar PROBACHG = new ProbaPar("probachg", 0.1);

	/*--------------------
	 * Attributes
	 --------------------*/

	/*--------------------
	 * Constructor
	 --------------------*/

	public AutoTileKModel() {
		super();
	}	

	/*--------------------
	 * main method
	 --------------------*/



	/** Greenberg-Hastings model: simple reac-diff model */
	public final int ApplyLocalFunction(OneRegisterIntCell cell) {
		//
		return autotileKStates(cell);
		//return autotile3States(cell);
	}

	/*--------------------
	 * overrides & implementations
	 --------------------*/

	public OneRegisterInitializer GetDefaultInitializer() {
		return new AutoTileKInitializer();
	}

	@Override
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TV4;
	}

	//@Override
	/** specific viewer */
	/*public AutomatonViewer GetPlaneAutomatonViewer(
			IntC cellPixSize, 
			SuperTopology in_TopologyManager, RegularDynArray in_Automaton) {	
		 PlanarTopology topo2D = (PlanarTopology) in_TopologyManager;
		AutomatonViewer av= new AutoTileKViewer(cellPixSize, topo2D.GetXYsize(), in_Automaton);
		av.SetPalette(GetPalette());
		return av;
	}*/
	/*--------------------
	 * Get & Set methods
	 --------------------*/

	/** special case k=3 */
	private int autotile3States(OneRegisterIntCell cell) {
		//OneRegisterIntCell cellN=cell.GetNeighbour(0);
		int state = cell.GetState();
		int 
		N= cell.ReadNeighbourState(0),
		E= cell.ReadNeighbourState(1),
		S= cell.ReadNeighbourState(2),
		W= cell.ReadNeighbourState(3);
		int [] count = new int[K];
		count[N]++;
		count[E]++;
		count[S]++;
		count[W]++;
		if (count[state]==0) {  // no conflict
			return state;
		} 
		// high level of constraints
		if (count[state]>=2) { 
			return RandomInt(K);
		}
		//
		IntegerList nonPresentStates= new IntegerList(); 
		for (int i=0; i<K; i++) {
			if (count[i]==0) 
				nonPresentStates.add(i);
		}
		int newstate=state;
		if (nonPresentStates.isEmpty()) {
			if (RandomEventDouble(PROBACHG)){
				newstate= RandomInt(K);
			}
		} else {
			newstate= nonPresentStates.ChooseOneAtRandom(this.GetRandomizer()); //FIXEME
		}
		return newstate;
	}



	/** k=4,5 */
	private int autotileKStates(OneRegisterIntCell cell) {
		int state = cell.GetState();
		int 
		N= cell.ReadNeighbourState(0),
		E= cell.ReadNeighbourState(1),
		S= cell.ReadNeighbourState(2),
		W= cell.ReadNeighbourState(3);
		int [] count = new int[K];
		count[N]++;
		count[E]++;
		count[S]++;
		count[W]++;
		if (count[state]==0) {  // no conflict
			return state;
		} 
		// conflict
		IntegerList nonPresentStates= new IntegerList(); 
		for (int i=0; i<K; i++) {
			if (count[i]==0) 
				nonPresentStates.add(i);
		}
		int newstate;
		if (nonPresentStates.isEmpty()) {
			newstate= RandomInt(K);
		} else {
			newstate= nonPresentStates.ChooseOneAtRandom(this.GetRandomizer()); //FIXEME
		}
		return newstate;
	}


	/** k= 5*/
	/*private int autotileFiveStates(OneRegisterIntCell cell) {
		int state = cell.GetState();
		int 
		N= cell.ReadNeighbourState(0),
		E= cell.ReadNeighbourState(1),
		S= cell.ReadNeighbourState(2),
		W= cell.ReadNeighbourState(3);
		int [] count = new int[5];
		count[N]++;
		count[E]++;
		count[S]++;
		count[W]++;
		if ( (state==S) || (state==W) ){
			int newstate= minNonOccupiedState(count);
			return newstate;
		} else {
			return state;
		}

	}*/



	private int minNonOccupiedState(int[] count) {
		for (int i=0; i<5; i++) {
			if (count[i]==0)
				return i;
		}
		return 0;
	}

	/*--------------------
	 * GFX
	 --------------------*/
	/** overrides */
	/*
	public FLPanel GetSpecificPanel() {
//		FLPanel panel= FLPanel.NewPanel(this.GetRandomizer().GetSeedControl());
//		return panel;
	}*/

	/** overrides */
	@Override
	public PaintToolKit GetPalette() {
		PaintToolKit palette= new PaintToolKit(COL);
		return palette;
	}


}
