package models.CAmodels.autotile;

import components.allCells.OneRegisterIntCell;
import components.types.IntegerList;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import initializers.SuperInitializer;
import models.CAmodels.ClassicalModel;
import topology.basics.TopologyCoder;

/*--------------------
 * models excitable media
 *--------------------*/
// states 0: empty ; 1,2,3,4
public class BuildEvilKModel extends ClassicalModel {

	final public static String NAME="BuildEvilK"; 
	private static final 
	FLColor [] COL={ 
			FLColor.c_white, 
			FLColor.c_yellow, FLColor.c_red, 
			FLColor.c_green, FLColor.c_blue, FLColor.c_cyan};
	static final int K=4;


	/*--------------------
	 * Attributes
	 --------------------*/

	/*--------------------
	 * Constructor
	 --------------------*/

	public BuildEvilKModel() {
		super();
	}	

	/*--------------------
	 * main method
	 --------------------*/

	/** Greenberg-Hastings model: simple reac-diff model */
	public final int ApplyLocalFunction(OneRegisterIntCell cell) {
		return localFunctionComp(cell);
	}

	/*--------------------
	 * overrides & implementations
	 --------------------*/

	public SuperInitializer GetDefaultInitializer() {
		return new BuildEvilKinitializer();
	}

	@Override
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TV4;
	}

	//@Override
	/** specific viewer */
	/*public AutomatonViewer GetPlaneAutomatonViewer(
			IntC cellPixSize, 
			SuperTopology in_TopologyManager, RegularDynArray in_Automaton) {	
		PlanarTopology topo2D = (PlanarTopology) in_TopologyManager;
		//AutomatonViewer av= new AutoTileKViewer(cellPixSize, topo2D.GetXYsize(), in_Automaton);
		
		av.SetPalette(GetPalette());
		return av;
	}*/
	/*--------------------
	 * Get & Set methods
	 --------------------*/

	/** special case k=3 */
	private int localFunctionComp(OneRegisterIntCell cell) {

		//OneRegisterIntCell cellN=cell.GetNeighbour(0);
		int C = cell.GetState();
		if (C>0) 
			return C;
		//C==0
		int 
		S= cell.ReadNeighbourState(2),
		W= cell.ReadNeighbourState(3);
		IntegerList absent= IntegerList.NintegersFromOne(4);
		absent.RemoveItem(C);
		absent.RemoveItem(S);
		absent.RemoveItem(W);
		//present.remove(new Integer(0));
		if (absent.size()==1) {
			return absent.Get(0);
		} else {
			if ((S>0) && (W>0)) {
				return absent.ChooseOneAtRandom(this.GetRandomizer());
			} else {
				return C;
			}
		}
	}

	/*--------------------
	 * GFX
	 --------------------*/
	/** overrides */
	/*
	public FLPanel GetSpecificPanel() {
//		FLPanel panel= FLPanel.NewPanel(this.GetRandomizer().GetSeedControl());
//		return panel;
	}*/

	/** overrides */
	@Override
	public PaintToolKit GetPalette() {
		PaintToolKit palette= new PaintToolKit(COL);
		return palette;
	}


}
