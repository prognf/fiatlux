package models.CAmodels.autotile;

import java.awt.event.MouseEvent;

import components.arrays.RegularDynArray;
import components.types.IntC;
import grafix.viewers.MouseInteractive;
import grafix.viewers.OneRegisterAutomatonViewer;

/* USEFUL CLASS ?????*/
public class RectangleMaignanViewer extends OneRegisterAutomatonViewer 
implements MouseInteractive
{

	public RectangleMaignanViewer(IntC cellPixSize, IntC gridSize,
			RegularDynArray automaton) {
		super(cellPixSize, gridSize, automaton);
		ActivateMouseFollower(this);
	}

	/** OVERRIDE IF NEEDED */
	@Override
	public void ProcessMouseEventXY(int x, int y, MouseEvent event) {
		//Macro.Debug("coucoucoucou");
		GetCell(x, y).InitState(RectangleMaignanModel.INITCENTER);
		this.UpdateView();
	}

}