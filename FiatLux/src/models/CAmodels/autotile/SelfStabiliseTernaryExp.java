package models.CAmodels.autotile;

import components.types.IntC;
import grafix.gfxTypes.elements.FLPanel;
import grafix.windows.ChronosWindow;
import main.Macro;

public class SelfStabiliseTernaryExp extends ChronosWindow {

	
	private static final IntC XYsize = new IntC(8,6);
	private static final IntC CellPixSize= new IntC(50, 0);

	
	final SSTgraph m_graph;
	
	public SelfStabiliseTernaryExp(SSTgraph graph, IntC CellPixSize) {
		super("auto-corection exercise");
		m_graph= graph;
		SSTgraphView viewer= new SSTgraphView(CellPixSize, graph);
		//FLFrame frame= FLFrame.NewFrame("test graph", viewer);
		FLPanel p = new FLPanel();
		p.SetBoxLayoutY();
		FLPanel p2= FLPanel.NewPanel(
				m_graph.m_Yinitcond.GetControl(),GetDynamicsPanel(), GetControlPanel());
		p.Add(p2,viewer);
		this.addPanel(p);
		this.packAndShow();
	}

	@Override
	protected void sig_Init() {
		m_graph.sig_Init();
	}

	@Override
	protected void sig_NextStep() {
		m_graph.onestep();

	}

	@Override
	protected void sig_UpdateView() {
		this.repaint();
		m_graph.printState();
	}

	@Override
	protected void sig_Close() {
		// TODO Auto-generated method stub

	}

	
	//--------------------------------------------------------------------------
	//- MAIN
	//--------------------------------------------------------------------------
	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();

	/*public static void main(String [] argv) throws InterruptedException{

		SSTgraph graph= new SSTgraph(XYsize);
		//graph.initRandom();
		//graph.initLineH(1, true);
		//graph.initLineV(1, true);
		graph.printState();

		//graph.initParticular();


	//	graph.getEdgeH(XYsize.X()-1, 1).flipDirState(); //right
		SelfStabiliseTernaryExp wndw= new SelfStabiliseTernaryExp(graph, CellPixSize);
		wndw.sig_Init();
		wndw.setVisible(true);
		
		//Macro.Debug("HOUUUUU");
	}*/

}
