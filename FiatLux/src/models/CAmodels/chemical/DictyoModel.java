package models.CAmodels.chemical;

import components.allCells.Cell;
import components.arrays.RegularDynArray;
import components.types.IntC;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.RegularAutomatonViewer;
import initializers.SuperInitializer;
import main.Macro;
import main.tables.PlotterSelectControl;
import models.CAmodels.CellularModel;
import topology.basics.PlanarTopology;
import topology.basics.SuperTopology;
import topology.basics.TopologyCoder;

/*--------------------
 * Dictyo Model
*--------------------*/
public class DictyoModel extends CellularModel {

    public final static String NAME = "Dictyo";
    public final static int OBSTACLE = -99; // convention : obstacles are coded
                                            // in negative pop

    final public static double
            EMISSION_PROBA = 0.01,
            WAVE_TRANSMIT_PROBA = 1,
            AGITATION_RATE = 0.0;

    /*--------------------
     * attributes
     --------------------*/

    DictyoModelControl m_control;
    DictyoViewer m_Viewer;
    private int MAX_EXCITATION = 2; // default M value

    double m_pT = WAVE_TRANSMIT_PROBA, m_pE = EMISSION_PROBA,
            m_pA = AGITATION_RATE;

    double INHIBITION_INCREASE = 0.0;
    double INHIBITION_DECREASE = INHIBITION_INCREASE / 100;

    /*--------------------
     * construction
    --------------------*/

    public DictyoModel() {
    }

    /* gets Prm */
    final public double GetAgitationP() {
        return m_pA;
    }

    @Override
    public String GetDefaultAssociatedTopology() {
        return TopologyCoder.s_FM8;
    }

    /* gets Pem */
    final public double GetEmissionP() {
        return m_pE;
    }

    final public int GetExcited() {
        return MAX_EXCITATION;
    }

    public double getInhibitionDecrease() {
        return INHIBITION_DECREASE;
    }

    public double getInhibitionIncrease() {
        return INHIBITION_INCREASE;
    }

    /* overrides */
    @Override
    public Cell GetNewCell() {
        return new DictyoCell(this);
    }

    /* overrides */
    @Override
    public SuperInitializer GetDefaultInitializer() {
        DictyoInitializer di = new DictyoInitializer(this);
        di.SetInitStyle(DictyoInitializer.AMOEBAE);
        return di;
    }

    /* overrides */
    @Override
    public PaintToolKit GetPalette() {
        int num = GetExcited();
        FLColor[] outcol = new FLColor[num + 4];
        for (int i = 0; i < num; i++) {
            int q = i * FLColor.MAX / 2 / num;
            int red = FLColor.MAX;
            int green = FLColor.MAX - q;
            int blue = FLColor.MAX - 2 * q;
            outcol[i] = new FLColor(red, green, blue);
        }
        outcol[num] = new FLColor(200, 50, 20);
        outcol[num + 1] = FLColor.c_darkgreen2; // <=LIMIT
        outcol[num + 2] = FLColor.c_darkergreen; // >LIMIT
        outcol[num + 3] = FLColor.c_darkblue; // obstacles
        return new PaintToolKit(outcol);
    }

    /** SPECIAL : explain this ??
     * overrides 2D Viewer : called by simulation sampler
     */
    @Override
    public RegularAutomatonViewer GetPlaneAutomatonViewer(final IntC in_SquareDim,
            final SuperTopology in_TopologyManager, final RegularDynArray in_Automaton) {
        PlanarTopology topo2D = (PlanarTopology) in_TopologyManager;
        // default selection
        m_Viewer = 
        		new DictyoViewer(in_SquareDim, topo2D.GetXYsize(), 
        				in_Automaton, this);
      
        UpdatePalette();
      
        return m_Viewer;
    }

    /*--------------------
     * overrides
     --------------------*/
    @Override
    public String[] GetPlotterSelection2D() {
        return PlotterSelectControl.DICTYOSELECT;
    }

    /* overrides */
    @Override
    public FLPanel GetSpecificPanel() {
        m_control = new DictyoModelControl(this);
        m_control.setOpaque(false);
        return m_control;
    }

    /* gets Ptr */
    final public double GetTransmissionP() {
        return m_pT;
    }

    /* sets Prm */
    final public void SetAgitationP(final double in_val) {
        Macro.print(4, "setting Prm to " + in_val);
        m_pA = in_val;
    }

    /* sets Pem */
    final public void SetEmissionP(final double in_val) {
        Macro.print(4, "setting Pem to " + in_val);
        m_pE = in_val;
    }

    final public void SetExcitationValue(final int Mvalue) {
        MAX_EXCITATION = Mvalue;
    }

    public void setInhibitionDecrease(final double value) {
        INHIBITION_DECREASE = value;
    }

    public void setInhibitionIncrease(final double value) {
        INHIBITION_INCREASE = value;
    }

    /*--------------------
     * static
    --------------------*/

    /* sets 3 pars at a time */
    final public void SetPemPtrPrm(final double in_val, final double in_val2, final double in_val3) {
        SetEmissionP(in_val);
        SetTransmissionP(in_val2);
        SetAgitationP(in_val3);
    }

    /* sets Ptr */
    final public void SetTransmissionP(final double in_val) {
        Macro.print(4, "setting Ptr to " + in_val);
        m_pT = in_val;
    }

    public void UpdatePalette() {
        m_Viewer.SetPalette(GetPalette());
    }


}
