package models.CAmodels.chemical;

import components.randomNumbers.RandomizerSeedControl;
import grafix.gfxTypes.controllers.DoubleController;
import grafix.gfxTypes.controllers.IntController;
import grafix.gfxTypes.elements.FLPanel;


public class DictyoModelControl extends FLPanel {

    /*--------------------
     * controls
     --------------------*/

    /**
     * Excitation level (Mlevel) Controller
     * 
     */

    class ExcitationLevelControl extends IntController {

        protected ExcitationLevelControl() {
            super(TXT_MVAL, LEN);
        }

        @Override
        protected int ReadInt() {
            return m_Model.GetExcited();
        }

        @Override
        protected void SetInt(final int val) {
            m_Model.SetExcitationValue(val);
            m_Model.UpdatePalette();
        }

    }

    /**
     * Firing Rate Controller
     * 
     */
    class PemControl extends DoubleController {

        public PemControl() {
            super(TXT_PEM, DoubleController.CONVERSIONRATE.PERCENT);
        }

        @Override
        public double ReadDouble() {
            return m_Model.GetEmissionP();
        }

        @Override
        public void SetDouble(final double val) {
            m_Model.SetEmissionP(val);
        }

    }

    /**
     * pA Controller
     * 
     */
    class PrmControl extends DoubleController {

        public PrmControl() {
            super(TXT_PRM, DoubleController.CONVERSIONRATE.PERCENT);
        }

        @Override
        public double ReadDouble() {
            return m_Model.GetAgitationP();
        }

        @Override
        public void SetDouble(final double val) {
            m_Model.SetAgitationP(val);
        }

    }

    /**
     * Transmission Rate (pT) Controller
     * 
     */
    class PtrControl extends DoubleController {

        public PtrControl() {
            super(TXT_PTR, DoubleController.CONVERSIONRATE.PERCENT);
        }

        @Override
        public double ReadDouble() {
            return m_Model.GetTransmissionP();
        }

        @Override
        public void SetDouble(final double val) {
            m_Model.SetTransmissionP(val);
        }

    }

    /*--------------------
     * constants
     --------------------*/

    final static String TXT_PEM = "Firing rate (%)",
                        TXT_PTR = "pT(%)",
                        TXT_PRM = "pA (%)",
                        TXT_MVAL = "Mlevel",
                        TXT_II = "Inhib. Inc. (%)",
                        TXT_ID = "Inhib. Dec. (%)";
    final static double HUNDRED = 100.;
    final static int LEN = 3;
    /*--------------------
     * attributes
     --------------------*/

    DictyoModel m_Model;

    PemControl mF_Pem;

    PtrControl mF_Ptr;

    PrmControl mF_Prm;

    /**
     * Inhibition increase
     */
    DoubleController mF_Ii;

    /**
     * Inhibition decrease
     */
    DoubleController mF_Id;

    ExcitationLevelControl mF_Mval = null;

    public DictyoModelControl(final DictyoModel in_Model) {
        super();
        setOpaque(false);

        m_Model = in_Model;
        mF_Pem = new PemControl();
        mF_Ptr = new PtrControl();
        mF_Prm = new PrmControl();
        mF_Mval = new ExcitationLevelControl();

        mF_Ii = new DoubleController(TXT_II, DoubleController.CONVERSIONRATE.PERCENT) {
            @Override
            protected double ReadDouble() {
                return m_Model.getInhibitionIncrease();
            }

            @Override
            protected void SetDouble(final double val) {
                m_Model.setInhibitionIncrease(val);
            }
        };
        mF_Id = new DoubleController(TXT_ID, DoubleController.CONVERSIONRATE.PERCENT) {
            @Override
            protected double ReadDouble() {
                return m_Model.getInhibitionDecrease();
            }

            @Override
            protected void SetDouble(final double val) {
                m_Model.setInhibitionDecrease(val);
            }
        };
        FLPanel	p1= new FLPanel();
        p1.Add(mF_Pem, mF_Ptr, mF_Prm, mF_Ii);
        FLPanel	p2= new FLPanel();
        p2.Add(mF_Id, new RandomizerSeedControl(m_Model.GetRandomizer()));
        this.SetBoxLayoutY();
        this.Add(p1,p2,mF_Mval);
    }

}
