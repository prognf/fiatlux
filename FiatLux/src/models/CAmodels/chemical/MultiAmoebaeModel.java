package models.CAmodels.chemical;

import components.allCells.SuperCell;
import components.arrays.RegularDynArray;
import components.types.DoublePar;
import components.types.IntC;
import components.types.IntPar;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.MultiRegisterViewer;
import grafix.viewers.RegularAutomatonViewer;
import initializers.SuperInitializer;
import models.CAmodels.CellularModel;
import topology.basics.PlanarTopology;
import topology.basics.SuperTopology;
import topology.basics.TopologyCoder;

/*--------------------
 * MultiAmoebae model, implemented for Amaury Saint-Jore
 * @Author : Océane Chazé
 *--------------------*/

public class MultiAmoebaeModel  extends CellularModel {

    /*private double m_Pa=0.1;
    private double m_lambda = 0.01;
    private int m_M = 2;
    private double m_Prep = 0.5;
    private int m_fullCell = 2;*/

    private DoublePar m_Pa;//Agitation probability
    private DoublePar m_lambda;//excitation rate
    private IntPar m_M;//level of excitment
    private DoublePar m_Prep;//probability of flee
    private IntPar m_fullCell;//number of amoebae when the cell is considererd as full

    private MultiRegisterViewer viewer;

    public final static String NAME="Multi Amoebae";

    public MultiAmoebaeModel(){
        super();
        m_Pa = new DoublePar("Pa",0.1);
        m_lambda = new DoublePar("lambda", 0.01);
        m_M = new IntPar("M",2);
        m_Prep = new DoublePar("Prep",0.5);
        m_fullCell = new IntPar("cell full",2);
    }

    /* overrides
     * 2D Viewer : called by simulation sampler  */
    @Override
    public RegularAutomatonViewer GetPlaneAutomatonViewer(IntC in_SquareDim,
                                                          SuperTopology in_TopologyManager, RegularDynArray in_Automaton) {
        // default selection
        if(in_TopologyManager instanceof PlanarTopology){
            MultiRegisterViewer viewer = new MultiAmoebaeViewer(in_SquareDim, ((PlanarTopology) in_TopologyManager).GetXYsize(), in_Automaton);
            viewer.SetPalette( GetPalette() );
            return viewer;
        }
        return null;
    }

    @Override
    public SuperInitializer GetDefaultInitializer() {
        return new MultiAmoebaeInitializer(m_M);
    }

    @Override
    public String GetDefaultAssociatedTopology() {
        return TopologyCoder.s_TM8;
    }

    @Override
    public SuperCell GetNewCell() {
        //return new MultiAmoebaeCell(m_Pa,m_lambda,m_M,m_Prep,m_fullCell);
        return new MultiAmoebaeCell(m_Pa,m_lambda,m_M,m_Prep,m_fullCell);
    }

    /* overrides */
    public PaintToolKit GetPalette(){
        PaintToolKit m_Color = new PaintToolKit(9);
        m_Color.SetColor(0, FLColor.c_white);
        m_Color.SetColor(1, FLColor.c_blue);
        m_Color.SetColor(2, FLColor.c_red);
        m_Color.SetColor(3, FLColor.c_darkblue);
        m_Color.SetColor(4, FLColor.c_darkred);
        m_Color.SetColor(5,FLColor.c_lightgreen);
        m_Color.SetColor(6,FLColor.c_green);
        m_Color.SetColor(7,FLColor.c_yellow);
        m_Color.SetColor(8,FLColor.c_orange);
        return m_Color;
        //return PaintToolKit.GetCompleteRainbow(3);
    }

    @Override
    public FLPanel GetSpecificPanel() {
        FLPanel pane = new FLPanel();
        /*DoublePar agitation = new DoublePar("Agitation probability",m_Pa);
        DoublePar excitationRate = new DoublePar("lambda",m_lambda);
        IntPar excitationLevel = new IntPar("M",m_M);
        DoublePar fleeProba = new DoublePar("Prep",m_Prep);
        IntPar fullCell = new IntPar("fullCell",m_fullCell);*/

        pane.Add(m_Pa,m_lambda,m_M,m_Prep,m_fullCell);
        pane.setOpaque(false);
        return pane;
    }
}
