package models.CAmodels.chemical;

import components.arrays.RegularDynArray;
import components.types.IntC;
import grafix.viewers.MultiRegisterViewer;

/*--------------------
 * View of the MultiAmoebae model
 * @Author : Océane Chazé
 *--------------------*/

public class MultiAmoebaeViewer extends MultiRegisterViewer {

    public MultiAmoebaeViewer(IntC in_CellSize, IntC in_GridSize, RegularDynArray automaton) {
        super(in_CellSize, in_GridSize, automaton);
    }

    //Cells Colors
    @Override
    public int GetCellColorNumXY(int x, int y) {
        int state = GetStateIXY(0,x,y);//nb Amoebae
        if(state==0 && GetStateIXY(2,x,y)==0){//No amoebae and excitation in this cell
            return 0;
        }
        if(state==1){
            if(GetStateIXY(1,x,y)==-1){
                return 5;
            }
            return GetStateIXY(1,x,y)+1;
        }
        if(state>=2){
            return GetStateIXY(1,x,y)+3;
        }


        if(GetStateIXY(2,x,y)>0){//the cell is excited and there is no amoebae on it
            return GetStateIXY(3,x,y)*2+GetStateIXY(2,x,y)+4;
        }
        /*if(state==2){
            return GetStateIXY(1,x,y)+3;
        }*/
        else {
            if (GetStateIXY(2, x, y) > 0) {
                if(GetStateIXY(3,x,y)>=0){
                    return GetStateIXY(3,x,y)*2+GetStateIXY(2,x,y)+4;
                }

            }
        }



        return 0;

    }
}
