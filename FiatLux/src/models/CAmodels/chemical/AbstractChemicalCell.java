package models.CAmodels.chemical;

import components.allCells.SuperCell;
import components.allCells.TwoRegisterCell;
import components.arrays.GenericCellArray;


/*--------------------
 * State 1 for population , State 2 for chemical
*--------------------*/
public abstract class AbstractChemicalCell extends TwoRegisterCell {

       /**
     * Activity of the cell : multiplicator on the reaction probability.
     */
    double m_inhibation;
    double m_inhibation_in;
    double m_inhibation_out;

    /*--------------------
     * particular methods
     --------------------*/
  
    
    public void AddInhibation(final double inhibation) {
        m_inhibation_in += inhibation;
    }

    /* setting buffer 2 (will be used as a difference buffer) */
    final protected void AddToNextChemicalDiff(final int level) {
        super.AddToBufferTwo(level);
    }

    /* -1 in pop next state */
    final protected void DecrementNextDPop() {
        super.AddToBufferOne(-1);
    }

    /* reading buffer 2 */
    final protected int GetChemical() {
        return super.GetStateTwo();
    }

    /* reading buffer 1 */
    final protected int GetPop() {
        return super.GetStateOne();
    }

    /* +1 in pop next state */
    final protected void IncrementNextDPop() {
        super.AddToBufferOne(1);
    }

    public void RemoveInhibation(final double inhibation) {
        m_inhibation_out += inhibation;
    }

    
    /*--------------------
     * NEIGHBOURHOOD methods
     --------------------*/
    private GenericCellArray<AbstractChemicalCell> m_Neighbourhood ;

    /* a AbstractDictyoCell can be only surrounded by similar cells */
    public void SetNeighbourhood(GenericCellArray<SuperCell> in_NeighbArray) {
        m_Neighbourhood = new GenericCellArray<AbstractChemicalCell>(in_NeighbArray);
    }
  
    /* returns the i-th neighbour (no control) */
    @Override
    final public AbstractChemicalCell GetNeighbour(final int in_NeighbourPosition) {
    	AbstractChemicalCell cell= m_Neighbourhood.get(in_NeighbourPosition);
    	return cell;
    }

    /* returns the size of the neighbourhood of the cell */
    @Override
    final public int GetNeighbourhoodSize() {
        return m_Neighbourhood.GetSize();
    }

    /** a cell surrounded by cells of the same type */
    public void zzzSetNeighbourhood(final GenericCellArray<AbstractChemicalCell> in_NeighbArray) {
        m_Neighbourhood = in_NeighbArray;
    }

    /* setting buffer 2 */
    final protected void SetNextChemical(final int level) {
        super.SetBufferTwo(level);
    }

    @Override
    public void sig_Reset() {
        super.sig_Reset();
        m_inhibation = 0;
        m_inhibation_in = 0;
        m_inhibation_out = 0;
    }

 

    
}
