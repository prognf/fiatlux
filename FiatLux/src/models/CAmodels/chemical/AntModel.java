package models.CAmodels.chemical;

import components.allCells.Cell;
import components.arrays.RegularDynArray;
import components.types.IntC;
import grafix.gfxTypes.PaintToolKit;
import grafix.viewers.RegularAutomatonViewer;
import grafix.viewers.TwoRegisterViewer;
import initializers.SuperInitializer;
import models.CAmodels.CellularModel;
import topology.basics.PlanarTopology;
import topology.basics.SuperTopology;
import topology.basics.TopologyCoder;

/*--------------------
 * This ant Model is a CFG the model is one of simple diffusion 
 * **--------------------*/
public class AntModel extends CellularModel {

	public final static int SATURATION_VAL= 200, NUMSTATE=100; 
	public final static String NAME="Ant";

	public AntModel() {
		super();
	}
	
	/* overrides
	 * 2D Viewer : called by simulation sampler  */
	@Override
	public RegularAutomatonViewer GetPlaneAutomatonViewer(IntC in_SquareDim, 
			SuperTopology in_TopologyManager, RegularDynArray in_Automaton) {
		PlanarTopology topo2D= (PlanarTopology)in_TopologyManager;
		// default selection
		TwoRegisterViewer av = new AntViewer(in_SquareDim, topo2D.GetXYsize(), in_Automaton);
		av.SetPalette( GetPalette() );
		return av;
	}
	
	public SuperInitializer GetDefaultInitializer() {
		return new AntInitializer();
	}

	public Cell GetNewCell() {
		return new AntCell(this);
	}
	
	/* overrides */
	public PaintToolKit GetPalette(){
		return PaintToolKit.GetRainbow(NUMSTATE);
	}

	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.DEFAULT_TOPO2D;
	}

	@Override
	public String [] GetPlotterSelection2D() {
		return null;
	}

}
