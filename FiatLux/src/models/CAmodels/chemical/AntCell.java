package models.CAmodels.chemical;


/* A DictyoDiffCell contains a CFG Cell 
 * with state = # of occupants 
 * and Vstate(0) = level of diffusion product */
class AntCell extends AbstractChemicalCell {

	static int QEMISSION = 20000; // each time step quantitity released

	static int EMISSION_PROBA = 1;

	static double EVAPORATION_FACTOR = 0.0; // fraction of evap. at each time step

	static double DIFFUSION_FACTOR = 0.25; // fraction of the product diffused

	static int MAXLEVEL = 100; // max concentration

	private int m_Npointer; // index that designates one neighbour

	AntModel m_model;

	/*--------------------
	 * construction
	 --------------------*/

	private AntCell(){}

	public AntCell(AntModel antModel) {
		m_model= antModel;
	}

	/*--------------------
	 * main function 
	 --------------------*/

	/* main */
	public void sig_UpdateBuffer() {
		DoMorphogeneRelease();
		DoDiffusion();
		DoMoves();

	}

	/* 1 and 2 : CFG */ 
	public void sig_MakeTransition() {
		m_StateOne += m_bufferOne; m_bufferOne= 0;
		m_StateTwo += m_bufferTwo; m_bufferTwo= 0; 		
	}


	/*--------------------
	 * sub functions 
	 --------------------*/



	/* release of chemicals */
	protected void DoMorphogeneRelease() {
		int state = GetPop();
		if ((state > 0) && (m_model.GetRandomizer().RandomEventPercent(EMISSION_PROBA))) {
			this.AddToNextChemicalDiff( QEMISSION );
		}
	}



	/* diffusion part */
	protected void DoDiffusion() {
		// TODO : why randomize pointer ??? FOR THE EQUALITY CASE !!!
		int Nsize = GetNeighbourhoodSize();
		// we compute the total quantity that will diffuse
		int thislevel = GetChemical();
		int q = (int)(DIFFUSION_FACTOR * (double)thislevel)  / Nsize;
		this.RandomizePointer();
		for (int k = 0; k < Nsize; k++) {
			AbstractChemicalCell target = GetPointedNeighbour();
			target.AddToNextChemicalDiff( q );
			this.AddToNextChemicalDiff( -q );
			this.AdvancePointer();
		}
		// simple evaporation
		this.AddToNextChemicalDiff( - (int) (EVAPORATION_FACTOR * (double)thislevel) );

	}

	protected void DoMoves() {
		// state transition
		int state = GetPop();
		if (state > 0) {
			AntCell selectedNeighb = GetSelectedNeighbour();
			selectedNeighb.IncrementNextDPop();
			this.DecrementNextDPop();
		}
	}

	/* where are we going next step ? */
	final protected AntCell GetSelectedNeighbour() {

		// we select a neighbour randomly to start the test of all neighbours
		this.RandomizePointer();
		AntCell CellWithMaximumLevel = this.GetPointedNeighbour();
		int maxlevel = CellWithMaximumLevel.GetChemical();

		for (int k = 0; k < GetNeighbourhoodSize() - 1; k++) {
			this.AdvancePointer();
			AntCell pointedCell = this.GetPointedNeighbour();
			int pointedCellLevel = pointedCell.GetChemical();
			if (pointedCellLevel > maxlevel) {
				CellWithMaximumLevel = pointedCell;
				maxlevel = pointedCellLevel;
			}

		}

		return CellWithMaximumLevel;
	}

	/* puts the pointer on random location */
	final private void RandomizePointer() {
		int size = GetNeighbourhoodSize();
		if (size > 0)
			m_Npointer = m_model.GetRandomizer().RandomInt(size);
	}

	/* which cell is pointed ? : cast here */
	final private AntCell GetPointedNeighbour() {
		return (AntCell)super.GetNeighbour(m_Npointer);
	}

	/* advance with rotation */
	final private void AdvancePointer() {
		m_Npointer = (++m_Npointer) % GetNeighbourhoodSize();
	}




}
