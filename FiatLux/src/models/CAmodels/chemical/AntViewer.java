package models.CAmodels.chemical;

import components.arrays.RegularDynArray;
import components.types.IntC;
import grafix.viewers.TwoRegisterViewer;


/*--------------------
 * enables the display
 * 
 * @author Nazim Fates
 *********************************************************************/
public class AntViewer extends TwoRegisterViewer {

	/*--------------------
	 * Constructor
	 --------------------*/

	public AntViewer(IntC in_CellSize, IntC in_GridSize, RegularDynArray in_Automaton) {
		super(in_CellSize, in_GridSize, in_Automaton);
	}

		
	/*--------------------
	 * 
	 --------------------*/

	public final int GetCellColorNumXY(int x, int y) {
		int color = 0;
		int state = GetStateOneXY(x, y);
		int chem = 	GetStateTwoXY(x, y);
		if (state == 0) {
			if (chem == 0) {
				color = 0;
			} else {
				if (chem >= AntModel.SATURATION_VAL) {
					color = AntModel.NUMSTATE; // saturation
				} else {
					color = chem * AntModel.NUMSTATE / AntModel.SATURATION_VAL;
				}
			}
		} else {

			color = AntModel.NUMSTATE + 1 ; // HACK !
		}
		return color;

	}

}
