package models.CAmodels.chemical;

import components.allCells.Cell;
import components.allCells.ClassicalCACell;
import components.allCells.OneRegisterIntCell;
import grafix.gfxTypes.FLColor;
import initializers.SuperInitializer;
import initializers.CAinit.CyclicInitializer;
import main.Macro;
import models.CAmodels.ClassicalModel;

/*--------------------
 * this model is used to study influence zones in Dicty it is a simple algo for
 * creating "pyramidal landscapes" out of points
*--------------------*/
public class InfluenceZoneModel extends ClassicalModel {

	final static String NAME="InfluenceZone";
	
	/*--------------------
	 * overrides & implementations
	 --------------------*/

	public String GetDefaultAssociatedTopology() {
		return "TN5";
	}

	
	 public SuperInitializer GetDefaultInitializer(){ return new CyclicInitializer(); }
	
	
	

	public Cell GetNewCell() {
		ClassicalModel mod = this;
		return new ClassicalCACell(mod);
	}

	/*--------------------
	 * Attributes
	 --------------------*/
	static private FLColor[] myColor;

	public static final int NUMSTATE = 100;

	/*--------------------
	 * Constructor
	 --------------------*/
	public InfluenceZoneModel() {

		myColor = new FLColor[NUMSTATE + 1];
		myColor[0] = new FLColor(0, 0, 0);
		for (int i = 1; i <= NUMSTATE; i++) {
			int r = (377 * i) % 256;
			int g = (197 * i) % 256;
			int b = (241 * i) % 256;
			myColor[i] = new FLColor(r, g, b);
		}
	}

	/*--------------------
	 * Get & Set methods
	 --------------------*/

	/* used with perfect cells */
	public final int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
		int oldstate = in_Cell.GetState();
		int newstate = oldstate;
		/* we change state only if we are zero */
		if (oldstate == 0) {
			int sz = in_Cell.GetNeighbourhoodSize();
			int max = in_Cell.ReadNeighbourState(0);
			for (int k = 1; k < sz; k++) {
				int st = in_Cell.ReadNeighbourState(k);
				if (st > max)
					max = st;
			}
			// we change state only if not quiescent
			if (max > 0)
				newstate = max + 1;
		} // if
		return newstate;
	}

	public final FLColor GetColor(int in_state) {
		try {
			return myColor[in_state];
		} catch (Exception ecol) {
			Macro
					.FatalError("Problem in the color management of CyclicModel. State :"
							+ in_state);
			return null;
		}
	}

	
	/*--------------------
	 * Other methods
	 --------------------*/

}
