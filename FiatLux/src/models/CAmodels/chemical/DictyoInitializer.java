package models.CAmodels.chemical;

import components.types.IntC;
import grafix.gfxTypes.controllers.IntController;
import grafix.gfxTypes.elements.FLPanel;
import initializers.CAinit.TwoRegisterPlaneInitializer;
import main.Macro;

/*--------------------
 * reates initial conditions for DictyoModel
 * 
 * @author Nazim Fates
*--------------------*/
public class DictyoInitializer extends TwoRegisterPlaneInitializer {

    class InitDensControl extends IntController {

        public InitDensControl() {
            super("Init Dens (in%)");
        }

        @Override
        protected int ReadInt() {
            return m_InitDens;
        }

        @Override
        protected void SetInt(final int val) {
            m_InitDens = val;
        }

    }

    class NamoebaeControl extends IntController {

        public NamoebaeControl() {
            super("Namoebae");
        }

        @Override
        protected int ReadInt() {
            return m_Namoebae;
        }

        @Override
        protected void SetInt(final int val) {
            m_Namoebae = val;
        }

    }

    /*--------------------
     * construction
     --------------------*/

    class ObstControl extends IntController {

        public ObstControl() {
            super("Obstacles");
        }

        @Override
        protected int ReadInt() {
            return m_obstacles;
        }

        @Override
        protected void SetInt(final int val) {
            m_obstacles = val;
        }

    }

    final static int
            DEF_DENSITY = 10, DEF_AMOEBAE = 0, DEF_REACDIFF = 10;
    final public static int
            AMOEBAE = 0, INIT_10PERCENT = 1, INIT_EXCITED = 2,
            INIT_UNIFORM = 3, SQUARE = 4, CLUSTERS2 = 5;

    // default parameters for cluster initialisation
    private int clusterDistance = 50; // cells
    private int cluster0_size = 1; // cells
    private int cluster1_size = 1; // cells

    // main pars
    private int m_InitDens = DEF_DENSITY;
    private int m_Namoebae = DEF_AMOEBAE;
    private int m_InitStyle = AMOEBAE;
    // private IntField mF_InitStyle = new IntField("InitStyle:", 3, AMOEBAE);
    public static final int OBST_NONE = 0, OBST_REGULAR = 1, OBST_RANDOM = 2;
    IntController mF_Obstacles, mF_Namoebae, mF_InitDens;

    final static int DEF_OBSTACLES = 0;

    int m_obstacles = DEF_OBSTACLES; // obstacles by default

    final static int L_OBSTACLES_REGULAR = 20;

    DictyoModel m_model; // for reading the M value

    final static int SQUARELEN = 10;

    public DictyoInitializer(final DictyoModel in_Model) {
        super();
        m_model = in_Model;
    }

    private void ClustersInit() {
        // arrange clusters
        int width = GetXsize();
        int height = GetYsize();

        int midX = width / 2;
        int midY = height / 2;

        int clust0Side = (int) java.lang.Math.ceil(java.lang.Math.sqrt(cluster0_size));
        int clust1Side = (int) java.lang.Math.ceil(java.lang.Math.sqrt(cluster1_size));

        int c0centerX = midX - (clusterDistance / 2) - clust0Side;
        int c1centerX = midX + (clusterDistance / 2) + clust1Side;
        int c0centerY = midY;
        int c1centerY = midY;

        // fill regions
        int count0 = cluster0_size;
        int count1 = cluster1_size;

        int dx, dy;

        dx = -clust0Side / 2;
        dy = -clust0Side / 2;
        while (count0 > 0) {
            count0--;
            SetPopulationXY(new IntC(c0centerX + dx, c0centerY + dy), 1);
            dx++;
            if (dx >= clust0Side / 2) {
                dx = -clust0Side / 2;
                dy++;
            }
        }

        dx = -clust1Side / 2;
        dy = -clust1Side / 2;
        while (count1 > 0) {
            count1--;
            SetPopulationXY(new IntC(c1centerX + dx, c1centerY + dy), 1);
            dx++;
            if (dx >= clust1Side / 2) {
                dx = -clust1Side / 2;
                dy++;
            }
        }

    }

    private void DensParticulesInit() {

        int dens = m_InitDens;
        /* particules density init */
        for (int pos = 0; pos < GetSize(); pos++) {
            if (RandomEventInt(dens) == true)
                if (!IsObstacle(pos)) {
                    SetPopulation(pos, 1);
                }
        }
        /* particules number init */
        for (int count = 0; count < m_Namoebae; count++) {
            int pos = RandomPos();
            if (!IsObstacle(pos)) {
                SetPopulation(pos, 1);
            }
        }

        /*
         * CAMERA NIKOS (fix) if(m_model.GetUseCamera()) { // clear all for(int
         * pos = 0; pos < GetLsize(); pos++) { SetPopulation(pos, 0); } }
         */
    }

   

	/*--------------------
     * implementations
     --------------------*/

    /* implementation */
    @Override
    public FLPanel GetSpecificPanel() {
        mF_InitDens = new InitDensControl();
        mF_Namoebae = new NamoebaeControl();
        mF_Obstacles = new ObstControl();

        FLPanel p = FLPanel.NewPanelVertical(mF_InitDens, mF_Namoebae, mF_Obstacles);
        p.AddRand(GetRandomizer());
        p.setOpaque(false);
        return p;
    }

    private boolean IsObstacle(final int pos) {
        return super.GetCellStateOne(pos) == DictyoModel.OBSTACLE;
    }

    private void PutObstaclesRandom() {
        int margin = 5;
        int n_ylines = GetYsize() / 10;
        int n_xlines = GetXsize() / 10;
        int xlen = GetXsize() - 2 * margin;
        int ylen = GetYsize() - 2 * margin;

        for (int count = 0; count < n_ylines; count++) {
            int x = margin + RandomInt(xlen);
            int y1 = margin + RandomInt(ylen / 2);
            int y2 = y1 + RandomInt(ylen / 2);

            for (int y = y1; y < y2; y++) {
                SetPopulationXY(x - 1, y, DictyoModel.OBSTACLE);
                SetPopulationXY(x, y, DictyoModel.OBSTACLE);
                SetPopulationXY(x + 1, y, DictyoModel.OBSTACLE);
            }
        }

        for (int count = 0; count < n_xlines; count++) {
            int y = margin + RandomInt(ylen);
            int x1 = margin + RandomInt(xlen / 2);
            int x2 = x1 + RandomInt(xlen / 2);

            for (int x = x1; x < x2; x++) {
                SetPopulationXY(x, y - 1, DictyoModel.OBSTACLE);
                SetPopulationXY(x, y, DictyoModel.OBSTACLE);
                SetPopulationXY(x, y + 1, DictyoModel.OBSTACLE);
            }
        }

    }

    private void PutObstaclesRegular() {
        final int DX = L_OBSTACLES_REGULAR / 2, DY = L_OBSTACLES_REGULAR / 2;
        for (int x = 0; x < GetXsize(); x++) {
            for (int y = 0; y < GetYsize(); y++) {
                /*
                 * boolean xputobstacle1= ((x+DX) % L_OBSTACLES_REGULAR == 0) ||
                 * ((x+DX+1) % L_OBSTACLES_REGULAR == 0); boolean
                 * yputobstacle1=((y+DY) % L_OBSTACLES_REGULAR != 0); boolean
                 * xputobstacle2= ((y+DY) % L_OBSTACLES_REGULAR == 0) ||
                 * ((y+DY+1) % L_OBSTACLES_REGULAR == 0); boolean
                 * yputobstacle2=((x+DX) % L_OBSTACLES_REGULAR != 0); boolean
                 * putobst= (( xputobstacle1 && yputobstacle1) || (
                 * xputobstacle2 && yputobstacle2));
                 */

                boolean putobstX = ((x + DX) % L_OBSTACLES_REGULAR == 0) ||
                        ((x + DX + 1) % L_OBSTACLES_REGULAR == 0);
                boolean putobstY = ((y + DY) % L_OBSTACLES_REGULAR == 0) ||
                        ((y + DY + 1) % L_OBSTACLES_REGULAR == 0);
                boolean putobst = putobstX || putobstY;
                boolean dontputx = ((x + DX + L_OBSTACLES_REGULAR / 2) % L_OBSTACLES_REGULAR == 0);
                boolean dontputy = ((y + DY + L_OBSTACLES_REGULAR / 2) % L_OBSTACLES_REGULAR == 0);
                if (putobst && !dontputx && !dontputy) {
                    SetPopulationXY(x, y, DictyoModel.OBSTACLE);
                }

            }
        }

    }

    private void ReacDiffInit() {
        /* particules density init */
        for (int pos = 0; pos < GetSize(); pos++) {
            int dens = DEF_REACDIFF;
            SetPopulation(pos, 0); // no amoeba on cell pos
            if (RandomEventInt(dens) == true) {
                SetExcited(pos);
            }
        }
    }

    private void SetChemical(final int pos, final int level) {
        super.SetCellStateTwo(pos, level);
    }

    public void SetClusterParameters(final int m0size, final int m1size, final int dist) {
        cluster0_size = m0size;
        cluster1_size = m1size;
        clusterDistance = dist;
    }

    public void setClusterParams(final int c0, final int c1, final int d) {
        cluster0_size = c0;
        cluster1_size = c1;
        clusterDistance = d;
    }

    private void SetExcited(final int pos) {
        super.SetCellStateTwo(pos, m_model.GetExcited());
    }

    public void SetInitDens(final int in_dens100) {
        m_InitDens = in_dens100;
    }

    /*--------------------
     * Get / Set
     --------------------*/
    public void SetInitStyle(final int in_style) {
        m_InitStyle = in_style;
    }

    public void SetNamoebae(final int nAmoebae) {
        m_Namoebae = nAmoebae;
    }

    public void SetObstacles(final int obstaclesType) {
        m_obstacles = obstaclesType;
    }

    private void SetPopulation(final int pos, final int pop) {
        super.SetCellStateOne(pos, pop);
    }

    /* direct acess allowed : DANGEROUS ! */
    public void SetPopulationXY(final int xpos, final int ypos, final int pop) {
        super.SetCellStateOneXY(xpos, ypos, pop);
    }

    public void SetPopulationXY(final IntC xypos, final int pop) {
        this.SetPopulationXY(xypos.X(), xypos.Y(), pop);
    }

    private void SquareInit() {
        int X = GetXsize() / 2, Y = GetYsize() / 2;
        for (int x = 0; x < SQUARELEN; x++) {
            for (int y = 0; y < SQUARELEN; y++) {
                SetPopulationXY(new IntC(X + x, Y + y), 1);
            }
        }
    }

    /* implementation */
    @Override
    protected void SubInit() {

        // in any case, we put first the obstacles in order to create particles
        // outside obstacles
        switch (m_obstacles) {
            case OBST_NONE:
                // do nothing
                break;
            case OBST_REGULAR:
                PutObstaclesRegular();
                break;
            case OBST_RANDOM:
                PutObstaclesRandom();
                break;
            default:
                Macro.FatalError("bad obstacles value");
        }

        // ClustersInit();

        switch (m_InitStyle) {
            case DictyoInitializer.AMOEBAE:
                DensParticulesInit();
                break;
            case DictyoInitializer.INIT_10PERCENT:
                SetInitDens(10);
                SetNamoebae(0);
                DensParticulesInit();
                break;
            case DictyoInitializer.INIT_EXCITED:
                ReacDiffInit();
                break;
            case DictyoInitializer.INIT_UNIFORM:
                UniformReacDiffInit();
                break;
            case SQUARE:
                SquareInit();
                break;
            case CLUSTERS2:
                ClustersInit();
                break;
            default:
                break;
        }

    }

   
    private void UniformReacDiffInit() {
        for (int pos = 0; pos < GetSize(); pos++) {
            SetPopulation(pos, 0); // no amoeba on cell pos
            int state = RandomInt(m_model.GetExcited() + 1);
            SetChemical(pos, state);
        }
    }

}
