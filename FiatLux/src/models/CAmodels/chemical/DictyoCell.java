package models.CAmodels.chemical;

import components.arrays.GenericCellArray;
import components.types.IntegerList;
import main.Macro;

/*--------------------
 * for DictyoA Model
 *--------------------*/
public class DictyoCell extends AbstractChemicalCell {

	// waves
	final private static int NEUTRAL = 0;

	// states
	final static int NOMOVE = -1;
	// moves
	final static int FREECELL_LIMIT = 1; // maximum nb of individuals for a
	// "normal" cell (<MAXPOP = free)

	// cellular actions
	final static int action_stay = 0, excite_by_amoeba = 1,
			excite_by_neighb = 2, decrease = 3;

	/*--------------------
	 * test
    --------------------*/
	public static void DoTest() {
		DictyoModel model = new DictyoModel();
		DictyoCell center = new DictyoCell(model), 
				c1 = new DictyoCell(model), c2 = new DictyoCell(model);
		GenericCellArray<DictyoCell> neighb = 
				new GenericCellArray<DictyoCell>(2);
		neighb.add(c1);
		neighb.add(c2);
		//FIXME center.SetNeighbourhood(neighb);
		c1.SetStateOne(2);
		c1.SetStateTwo(28);
		c2.SetStateOne(1);
		c2.SetStateTwo(0);
		int n = center.GetNeighbourhoodSize();
		int l1 = center.GetNeighbour(0).GetPop();
		int l2 = center.GetNeighbour(0).GetChemical();
		// int x =

		Macro.print("n:" + n + " l1:" + l1 + " l2: " + l2);

	}

	int m_action;

	DictyoModel m_model;

	/*--------------------
	 * construction
     --------------------*/

	public DictyoCell(final DictyoModel in_model) {
		super();
		m_model = in_model;
	}

	/* manages the moves of the amoebae */
	private void DoMoves() {

		if (GetPop() > 0) {
			// for (int agent=0; agent < GetPop(); agent++) {
			// Macro.Debug(""); Macro.LDebug("move...");
			int selectedNeighb = GetSelectedNeighbour();
			if (selectedNeighb != NOMOVE) {
				// transition
				AbstractChemicalCell targetNeighb = GetNeighbour(selectedNeighb);
				targetNeighb.IncrementNextDPop();
				DecrementNextDPop();

				// If we are moving because we are attracted by a wavefront...
				targetNeighb.AddInhibation(m_model.getInhibitionIncrease());

				// Increase the other cell's inhibition
				targetNeighb.AddInhibation(m_inhibation);

				// Decrease our own inhibation
				RemoveInhibation(m_inhibation);
			}
		}//
	}

	/** for each amoeba we give a proba of initiating a wave */
	protected void DoWaveInitiation() {
		if (GetPop() > 0) {
			for (int agent = 0; agent < GetPop(); agent++) {
				// Multiply it with the cell activity
				double pE = m_model.GetEmissionP() * (1 - m_inhibation);
				if (IsNeutral() && m_model.RandomEventDouble(pE)) {
					m_action = excite_by_amoeba;
				}
			}
		}
	}

	/** TODO should be called before DoWaveInitiation */
	protected void DoWavePropagation() {
		int level = GetChemical();
		/*
		 * DEBUG if ((level<0)||(level>EXCITED)){
		 * Macro.FatalError("pb with cell level !"); }
		 */
		if (level == NEUTRAL) {
			// for all neighbours...
			double pT = m_model.GetTransmissionP();
			if (HasAnExcitedNeighbour() && m_model.RandomEventDouble(pT)) {
				m_action = excite_by_neighb;
			}
		} else /* if (level > NEUTRAL) */{
			m_action = decrease;
		}
	}

	final int GetMaxExcitation() {
		return m_model.GetExcited();
	}

	/** selection of a neighbour one at random among excited ones */
	private int GetSelectedNeighbour() {
		int godir = NOMOVE;
		IntegerList PossibleNeighb = new IntegerList();
		double pA = m_model.GetAgitationP();
		if (pA > Macro.EPSILON && m_model.RandomEventDouble(pA)) {
			// random move to free cell
			for (int i = 0; i < GetNeighbourhoodSize(); i++) {
				if (IsFreeNeighb(i)) {
					PossibleNeighb.addVal(i);
				}
			}
		} else {
			// if the cell is not neutral it will not move
			// OLD if (GetChemical() == NEUTRAL) {
			int mystate = GetChemical();
			{
				// this.PrintNeighb(); // DEBUG
				for (int i = 0; i < GetNeighbourhoodSize(); i++) {
					boolean selectNeighbInList= (mystate==0) && IsExcitedFreeNeighb(i);
					// OLD 
					// OLD if ((mystate - GetNeighbour(i).GetChemical() == -1) &&
					// OLD       IsFreeNeighb(i) && m_model.GetRandomizer().RandomEventDouble(.5)) {
					if (selectNeighbInList){ 
						PossibleNeighb.addVal(i);
					}
				}// for
			} // if
		}
		// if more than one
		if (PossibleNeighb.GetSize() > 0) {
			// select one at random
			godir = PossibleNeighb.ChooseOneAtRandom(m_model.GetRandomizer());
		}
		return godir;
	}

	/** do I have an excited neighbour ? */
	final private boolean HasAnExcitedNeighbour() {
		int excitation = 0;
		// for (int pos=0; pos < this.GetNeighbourhoodSize() ; pos+= 2){
		// excitation = excitation || (GetNeighbour(pos).GetChemical() ==
		// GetMaxExcitation());
		// }

		for (int i = 0; i < GetNeighbourhoodSize(); i++) {
			if (GetNeighbour(i).GetChemical() == GetMaxExcitation()) {
				excitation++;
			}
		}
		return (excitation > 0);
	}

	final private boolean IsExcitedFreeNeighb(final int i) {
		return (GetNeighbour(i).GetChemical() == GetMaxExcitation()) && IsFreeNeighb(i);
	}

	final private boolean IsFreeNeighb(final int i) {
		return (GetNeighbour(i).GetPop() < FREECELL_LIMIT) && (GetNeighbour(i).GetPop() != DictyoModel.OBSTACLE);
	}

	final private boolean IsNeutral() {
		return (GetChemical() == NEUTRAL);
	}

	/** TODO : hack */
	final private boolean IsObstacle() {
		return GetPop() == DictyoModel.OBSTACLE;
	}

	/** direct access to buffers ! 1= CFG, 2=CA */
	@Override
	public void sig_MakeTransition() {

		// Handle leaving agents
		if (GetPop() > 0)
			m_inhibation -= m_inhibation_out / GetPop();
		m_inhibation_out = 0;

		m_StateOne += m_bufferOne;
		m_bufferOne = 0;
		m_StateTwo = m_bufferTwo;

		// Handle entering agents
		if (GetPop() > 0)
			m_inhibation += m_inhibation_in / GetPop();
		m_inhibation_in = 0;

		m_inhibation -= m_model.getInhibitionDecrease();

		// Ensures it is in [0,1]
		m_inhibation = Math.min(Math.max(m_inhibation, 0), 1);
		// Finally, let's reduce this inhibation a bit.

	}

	/* DEBUG */
	/*
	 * private void PrintNeighb() { for (int i = 0; i < GetNeighbourhoodSize();
	 * i++) { Macro.LDebug("|" + GetNeighbour(i).GetChemical() ); } }
	 */

	/*--------------------
	 * MAIN
     --------------------*/

	/* main function */
	@Override
	public void sig_UpdateBuffer() {

		m_action = action_stay;
		if (!IsObstacle()) { // -1 => obstacle
			DoWavePropagation();
			DoWaveInitiation();

			if ((m_action == excite_by_neighb) || (m_action == excite_by_amoeba)) {
				SetNextChemical(GetMaxExcitation());
			}
			if (m_action == decrease) {
				SetNextChemical(GetChemical() - 1);
			}
			// we move only if no excitation BY AMOEBA
			// if (m_action != excite_by_amoeba){
			DoMoves();
			// }

		}
	}

}
