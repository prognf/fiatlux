package models.CAmodels.chemical;

import java.awt.Dimension;

import components.types.IntPar;
import grafix.gfxTypes.DoubleField;
import grafix.gfxTypes.IntField;
import grafix.gfxTypes.elements.FLPanel;
import initializers.CAinit.BinaryInitializer;
import initializers.CAinit.MultiRegisterPlaneInitializer;


/*--------------------
 * Initialisation of the MultiAmoebae model
 *
 * @Author : Océane Chazé
 *--------------------*/

public class MultiAmoebaeInitializer extends MultiRegisterPlaneInitializer {
    DoubleField mF_InitDens = new DoubleField("Rate of amoebae:", 5, 0.1);
    IntField mF_nbAmoebae = new IntField("Number of amoebae:", 5,5);
    //RadioButton dens;
    //RadioButton number = new RadioButton("Number");
    IntPar m_M;

    public MultiAmoebaeInitializer(IntPar m){
        super();
        m_M = m;
    }



    public FLPanel GetSpecificPanel() {
        FLPanel p = new FLPanel();
        p.SetBoxLayoutY();
        p.add(mF_InitDens);
        p.Add(mF_nbAmoebae);
        p.Add(m_M);
        p.setPreferredSize(new Dimension(400,100));
        //dens  = new RadioButton("Density");
        //p.Add(dens);
        //p.Add(number);
        p.setOpaque(false);
        return p;
    }
    @Override
    protected void SubInit() {
        double density = mF_InitDens.GetValue();
        BinaryInitializer bin = new BinaryInitializer();
        if(mF_nbAmoebae.GetValue()!=0){
            for(int i=0;i<mF_nbAmoebae.GetValue();i++){
                int rdm = bin.RandomInt(GetLsize());
                while(GetCellStateI(rdm,0)>0){
                    rdm = bin.RandomInt(GetLsize());
                }
                SetCellStateI(rdm,0,1);
                SetCellStateI(rdm,1,bin.RandomInt(m_M.GetVal()));
            }
            //Test

            /*SetCellStateI(456,0,1);
            SetCellStateI(456,1,0);

            SetCellStateI(517,0,1);
            SetCellStateI(517,1,0);
            SetCellStateI(513,0,1);
            SetCellStateI(513,1,0);
            SetCellStateI(523,0,1);
            SetCellStateI(523,1,1);
            SetCellStateI(527,0,1);
            SetCellStateI(527,1,1);

            SetCellStateI(584,0,1);
            SetCellStateI(584,1,1);*/
            //\TEST
        }

        /* particules setting */
        else{
            for (int count = 0; count < GetLsize(); count++) {
            /*if(count == 855 || count == 634){
                super.SetCellStateI(count,0,1);
                super.SetCellStateI(count,1,bin.RandomInt(2));
            }Test initializer*/
                if(bin.RandomEventDouble(density)){
                    super.SetCellStateI(count,0,1);
                    super.SetCellStateI(count,1,bin.RandomInt(m_M.GetVal()));
                }
            }
        }
    }
}
