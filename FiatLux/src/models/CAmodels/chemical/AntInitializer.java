package models.CAmodels.chemical;

import grafix.gfxTypes.IntField;
import grafix.gfxTypes.elements.FLPanel;
import initializers.CAinit.TwoRegisterPlaneInitializer;

/*--------------------
 * Initializer for ants model
 * 
 * @author Nazim Fates
*--------------------*/
class AntInitializer extends TwoRegisterPlaneInitializer {

	
	IntField mF_InitDens = new IntField("Ants:", 3, 10);
		
	public FLPanel GetSpecificPanel() {
		FLPanel p = new FLPanel();
		p.add(mF_InitDens);
		p.setOpaque(false);
		return p;
	}

	protected void SubInit() {
		//int size = GetLsize();
		int ants = mF_InitDens.GetValue();
		/* particules setting */
		for (int count = 0; count < ants; count++) {
			int pos= RandomPos();
			//SetPopulation(pos, 1);
			SetChemical(pos,AntCell.QEMISSION); // DEBUG
		}

	}
	
	protected void SubInit2() {
		//int size = GetLsize();
		int ants = mF_InitDens.GetValue();
		/* particules setting */
		for (int count = 0; count < ants; count++) {
			int pos= RandomPos();
			SetPopulation(pos, 1);
			//SetChemical(pos,10000); // DEBUG
		}

	}

	private void SetChemical(int pos, int level) {
		super.SetCellStateTwo(pos, level);
	}
	
	/*private void SetChemicalXY(int xpos, int ypos, int level) {
		super.SetCellStateTwoXY(xpos, ypos, level);
	}*/

	private void SetPopulation(int pos,  int pop) {
		super.SetCellStateOne(pos, pop);		
	}

	/*private void SetPopulationXY(int xpos, int ypos, int pop) {
		super.SetCellStateOneXY(xpos, ypos, pop);		
	}*/
	
	
	
	
}
