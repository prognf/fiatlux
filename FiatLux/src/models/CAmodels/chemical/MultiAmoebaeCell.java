package models.CAmodels.chemical;

import java.util.ArrayList;

import components.allCells.MultiRegisterCell;
import components.types.DoublePar;
import components.types.IntPar;
import initializers.CAinit.BinaryInitializer;

/*--------------------
 * for MultiAmoebaeCell
 * Contains 4 register :
 *      - for the number of amoebae on the cell (0)
 *      - for the state of the amoebae          (1)
 *      - for the excitation of the cell        (2)
 *      - for the state of the cell             (3)
 *
 * Buffer : register 0 and 2 are relatives
 *
 * @Author : Océane Chazé
 *--------------------*/

public class MultiAmoebaeCell extends MultiRegisterCell {

    /*--------------------
	 * attribute
	 --------------------*/

    //m_state = state of the cell
    //m_buffer = differences of states in cell between two iterations

    private DoublePar m_Pa;//Agitation probability
    private DoublePar m_lambda;//excitation rate
    private IntPar m_M;//level of excitment
    private DoublePar m_Prep;//probability of flee
    private IntPar m_fullCell;//number of amoebae when the cell is considererd as full
    private boolean m_conflict;
    private MultiAmoebaeCell m_toCell;//cell where the amoebae will be send

    /*--------------------
	 * construction
	 --------------------*/

    public MultiAmoebaeCell(DoublePar Pa, DoublePar lambda, IntPar M, DoublePar Prep, IntPar fullCell){
        super(4);
        m_Pa = Pa;
        m_lambda = lambda;
        m_M = M;
        m_Prep = Prep;
        m_fullCell = fullCell;
        m_toCell = null;
        m_conflict = false;
        SetBufferI(0,0);
        SetBufferI(1,-1);
        SetBufferI(2,0);
        SetBufferI(3,-1);
    }

    /*--------------------
	 * Signals
	 --------------------*/

    @Override
    public void sig_UpdateBuffer() {

        AmoebaeMoves();
        CellExcitation();

    }

    @Override
    public void sig_MakeTransition() {
        if(m_conflict){
            SetBufferI(0,0);
            SetBufferI(1,-1);
        }
        if(m_toCell!=null){

            if(m_toCell.GetConflict()){
                SetBufferI(0,GetBufferI(0)+1);
                SetBufferI(1,GetStateI(1));
            }
        }

        SetStateI(1,GetBufferI(1));
        SetStateI(0,GetStateI(0)+GetBufferI(0));

        SetStateI(2,GetStateI(2)+GetBufferI(2));
        SetStateI(3,GetBufferI(3));

        //Reset cell buffer + destination cell
        SetBufferI(0,0);
        SetBufferI(1,-1);
        SetBufferI(2,0);
        SetBufferI(3,-1);
        m_toCell=null;

    }

    public void sig_ResetConflict(){
        m_conflict=false;
    }

    @Override
    public void sig_Reset() {
        SetStateI(0,0);
        SetStateI(1,-1);
        SetStateI(2,0);
        SetStateI(3,-1);
    }

    /*--------------------
	 * Major methods
	 --------------------*/
    /** Amoebae moves */
    private void AmoebaeMoves(){
        BinaryInitializer bin = new BinaryInitializer();
        if(GetStateI(0)>0){
            if(bin.RandomEventDouble(m_Pa.GetVal())){
                ArrayList<MultiAmoebaeCell> free = GetFreeNeighbours();
                if(free.size()>0){
                    m_toCell = free.get(bin.RandomInt(free.size()));
                }
                else{//No free neighbour
                    m_toCell=this;
                }
            }
            else if(GetStateI(2)==0 && getE().size()>0){

                ArrayList<MultiAmoebaeCell> excited = getE();
                m_toCell = excited.get(bin.RandomInt(excited.size()));
            }//to follow waves
            else if(GetStateI(2)==0 && getS().size()>0 && getF().size()>0 && bin.RandomEventDouble(m_Prep.GetVal())){//todo : add proba Prep
                ArrayList<MultiAmoebaeCell> free = getF();
                m_toCell = free.get(bin.RandomInt(free.size()));
            }

            else {
                m_toCell = this;
            }

            if(m_toCell.GetBufferI(1)!=-1 && m_toCell.GetBufferI(1)!=GetStateI(1)){
                m_toCell.SetConflict();
            }
            m_toCell.SetBufferI(1,GetStateI(1));
            SetBufferI(0,GetBufferI(0)-1);
            m_toCell.SetBufferI(0,m_toCell.GetBufferI(0)+1);

            if(GetStateI(0)>1){//Pas de passage à -1 lors du départ d'une amibe si deux amibes
                SetBufferI(1,GetStateI(1));
            }
        }
    }

    /** set excitation of cells **/
    private void CellExcitation(){
        if(GetStateI(2)==0){//cell not excited
            BinaryInitializer bin = new BinaryInitializer();
            int n = GetExcitations().size();
            int excitation;
            if(n>0){
                excitation = GetExcitations().get(bin.RandomInt(n));
            }
            else{
                excitation = -1;
            }

            if(GetStateI(0)>0 && bin.RandomEventDouble(m_lambda.GetVal())){//amoebae on cell and excitedAmobae
                SetBufferI(2,m_M.GetVal());
                SetBufferI(3,GetStateI(1));

            }
            else if (excitation>=0){
                SetBufferI(2,m_M.GetVal());
                SetBufferI(3,excitation);

            }
        }
        else if (GetStateI(2)>0){
            SetBufferI(2,-1);
            SetBufferI(3,GetStateI(3));
        }
        else{
            SetBufferI(2,0);
        }
    }

    private boolean IsFreeNeighbour(MultiAmoebaeCell neighbour){
        if(neighbour.GetStateI(0)<m_fullCell.GetVal() && (neighbour.GetStateI(1)==-1 || neighbour.GetStateI(1)==GetStateI(1))){
            return true;
        }
        else{
            return false;
        }
    }



    /*--------------------
	 * get/set
	 --------------------*/

    public boolean GetConflict(){
        return m_conflict;
    }
    public void SetConflict(){
        m_conflict = true;
    }


    /** types of excitations in the neighbourhood */
    private ArrayList<Integer> GetExcitations(){
        ArrayList<Integer> excitations = new ArrayList<>();
        for(int i=0; i<GetNeighbourhoodSize(); i++){
            if(GetNeighbour(i).GetStateI(2)==m_M.GetVal() && !excitations.contains(GetNeighbour(i).GetStateI(3))){
                excitations.add(GetNeighbour(i).GetStateI(3));
            }
        }
        return excitations;
    }

    /*--------------------
	 * get of different sets of neighbour
	 --------------------*/
    /** Get Free Neighbours : no amoebae on the cell or amoebae of same type and less than m_M*/
    private ArrayList<MultiAmoebaeCell> GetFreeNeighbours(){
        ArrayList<MultiAmoebaeCell> free = new ArrayList();
        for(int i =0 ; i<GetNeighbourhoodSize(); i++){
            if(this.IsFreeNeighbour((MultiAmoebaeCell)GetNeighbour(i))){
                free.add((MultiAmoebaeCell)GetNeighbour(i));
            }
        }
        return free;
    }

    /** Get E : free amoebae and same type of excitation than type of amoebae*/
    private ArrayList<MultiAmoebaeCell> getE(){
        ArrayList<MultiAmoebaeCell> free = GetFreeNeighbours();
        ArrayList<MultiAmoebaeCell> e = GetFreeNeighbours();
        for(MultiAmoebaeCell cell : free){
            if(!(cell.GetStateI(2)==m_M.GetVal() && cell.GetStateI(3)==GetStateI(1))){
                e.remove(cell);
            }
        }
        return e;

    }

    /** Get S : different type of excitation than type of amoebae*/
    private ArrayList<MultiAmoebaeCell> getS(){
        ArrayList<MultiAmoebaeCell> s = new ArrayList<>();
        for(int i=0;i<GetNeighbourhoodSize();i++){
            if(GetNeighbour(i).GetStateI(2)==m_M.GetVal() && GetNeighbour(i).GetStateI(3)!=GetStateI(1)){
                s.add((MultiAmoebaeCell)GetNeighbour(i));
            }
        }
        return s;
    }

    /** Get F : free amoeabae and not different type of excitation than type of amoebae*/
    private ArrayList<MultiAmoebaeCell> getF(){
        ArrayList<MultiAmoebaeCell> free = GetFreeNeighbours();
        ArrayList<MultiAmoebaeCell> f = GetFreeNeighbours();
        for(MultiAmoebaeCell cell : free){
            if((cell.GetStateI(2)==m_M.GetVal() && cell.GetStateI(3)!=GetStateI(1))){
                //System.out.println("oups, not same type");
                f.remove(cell);
            }
        }
        return f;

    }
}
