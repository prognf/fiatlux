package models.CAmodels.chemical;

import components.types.IntegerList;
import main.Macro;

/*--------------------
 * for DictyoA Model
*--------------------*/
public class TestAmybiaCell extends AbstractChemicalCell {


	final int GetMaxExcitation(){
		return m_model.GetExcited();
	}


	//	 waves
	final private static int NEUTRAL = 0;
	// states
	final static int NOMOVE = -1;

	// moves
	final static int FREECELL_LIMIT=1; // maximum nb of individuals for a "normal" cell (<MAXPOP = free)

	// cellular actions
	final static int action_stay=0, excite_by_amoeba=1, excite_by_neighb=2, decrease=3;
	int m_action;

	DictyoModel m_model;

	/*--------------------
	 * construction
	 --------------------*/

	public TestAmybiaCell(DictyoModel in_model) {
		super();
		m_model = in_model;
	}

	/*--------------------
	 * MAIN
	 --------------------*/


	/* main function */
	public void sig_UpdateBuffer() {	

		m_action= action_stay;
		if (! IsObstacle() ) { // -1 => obstacle
			DoWavePropagation();
			DoWaveInitiation();

			if ((m_action==excite_by_neighb )||(m_action==excite_by_amoeba)){			
				this.SetNextChemical(GetMaxExcitation());
			}  
			if (m_action == decrease){
				this.SetNextChemical( GetChemical() - 1);  
			}
			// we move only if no excitation BY AMOEBA
			//if (m_action != excite_by_amoeba){
			DoMoves(); 
			//}

		}
	}

	/* direct access to buffers ! 1= CFG, 2=CA*/
	public void sig_MakeTransition() {
		m_StateOne += m_bufferOne; m_bufferOne= 0;
		m_StateTwo = m_bufferTwo; 
	}

	/* for each amoeba we give a proba of initiating a wave */
	protected void DoWaveInitiation() {
		if (GetPop()>0){
			for (int agent=0; agent <GetPop(); agent++) {
				double pE= m_model.GetEmissionP();
				if ( IsNeutral() && m_model.RandomEventDouble(pE) ){
					m_action= excite_by_amoeba;
				}				
			}
		}
	}

	final private boolean IsNeutral(){
		return ( GetChemical()== NEUTRAL);
	}

	/* TODO : hack */
	final private boolean IsObstacle(){
		return GetPop() == DictyoModel.OBSTACLE ; 
	}


	/* TODO should be called before DoWaveInitiation */ 
	protected void DoWavePropagation() {
		int level = GetChemical();
		/* DEBUG
		 * if ((level<0)||(level>EXCITED)){
			Macro.FatalError("pb with cell level !");
		}*/
		if (level == NEUTRAL) {
			// for all neighbours...
			double pT= m_model.GetTransmissionP();
			if (HasAnExcitedNeighbour() && m_model.RandomEventDouble(pT)){
				m_action= excite_by_neighb;
			}
		} else /*if (level > NEUTRAL)*/ {
			m_action= decrease;
		}
	}

	/* do I have an excited neighbour ? */
	final private boolean HasAnExcitedNeighbour(){
		boolean excitation = false;
	//for (int pos=0; pos < this.GetNeighbourhoodSize() ; pos+= 2){
	//	excitation = excitation || (GetNeighbour(pos).GetChemical() == GetMaxExcitation()); 
	//}
		
		for (int i = 0; i < this.GetNeighbourhoodSize(); i++) {
			excitation = excitation || (GetNeighbour(i).GetChemical() == GetMaxExcitation()); 
		}
		return excitation;
	} 
	/* manages the moves of the amoebae */
	private void DoMoves() {

		if (GetPop()>0){
			//for (int agent=0; agent < GetPop(); agent++) {
			//Macro.Debug(""); Macro.LDebug("move...");
			int selectedNeighb = GetSelectedNeighbour();
			if (selectedNeighb != NOMOVE) {
				// transition
				AbstractChemicalCell targetNeighb = GetNeighbour(selectedNeighb);
				targetNeighb.IncrementNextDPop();
				this.DecrementNextDPop();
			}
		}//
	}

	final private boolean IsFreeNeighb(int i){
		return (GetNeighbour(i).GetPop() < FREECELL_LIMIT) && (GetNeighbour(i).GetPop()!=DictyoModel.OBSTACLE);
	}

	final private boolean IsExcitedFreeNeighb(int i){
		return (GetNeighbour(i).GetChemical() == GetMaxExcitation()) && IsFreeNeighb(i);
	}


	/* selection of a neighbour one at random among excited ones */
	private int GetSelectedNeighbour() {
		int godir = NOMOVE;
		IntegerList PossibleNeighb = new IntegerList();
		double pA= m_model.GetAgitationP();
		if (m_model.RandomEventDouble(pA) ){
			// random move to free cell
			for (int i = 0; i < GetNeighbourhoodSize(); i++) {
				if (IsFreeNeighb(i)) {
					PossibleNeighb.addVal(i);
				}
			}
		} else {
			// if the cell is not neutral it will not move
			if (GetChemical() == NEUTRAL) {
				//this.PrintNeighb(); // DEBUG
				for (int i = 0; i < GetNeighbourhoodSize(); i++) {
					if (IsExcitedFreeNeighb(i)){					
						PossibleNeighb.addVal(i);
					}
				}//for
			} //if
		} 	
		// if more than one
		if (PossibleNeighb.GetSize()>0){
			// select one at random
			godir = PossibleNeighb.ChooseOneAtRandom(m_model.GetRandomizer());					
		}
		return godir;
	}

	/* DEBUG */
	/*private void PrintNeighb() {
		for (int i = 0; i < GetNeighbourhoodSize(); i++) {
			Macro.LDebug("|" + GetNeighbour(i).GetChemical() );
		}
	}*/
	
	/*--------------------
	 * test
	--------------------*/
	public static void DoTest() {
		DictyoModel model = new DictyoModel();
		TestAmybiaCell 	center = new TestAmybiaCell(model), 
		c1= new TestAmybiaCell(model), c2 = new TestAmybiaCell(model);
		TestAmybiaCell [] neighb = {c1,c2};
	//FIXME	center.SetNeighbourhood( neighb );
		c1.SetStateOne(2);
		c1.SetStateTwo(28);
		c2.SetStateOne(1);
		c2.SetStateTwo(0);
		int n= center.GetNeighbourhoodSize();
		int l1= center.GetNeighbour(0).GetPop();
		int l2= center.GetNeighbour(0).GetChemical();
		//int x = 

		Macro.print( "n:" + n + " l1:" + l1 + " l2: "+ l2);

	}

	




}

