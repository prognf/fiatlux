package models.CAmodels.chemical;

import java.awt.event.MouseEvent;

import components.arrays.RegularDynArray;
import components.types.IntC;
import grafix.gfxTypes.controllers.FLCheckBox;
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.MouseInteractive;
import grafix.viewers.TwoRegisterViewer;
import main.Macro;


/*--------------------
 * A DictyoPlaneAutomatonViewer enables the display of a Dityo model in 2D
 * 
 * @author Nazim Fates
*--------------------*/
public class DictyoViewer extends TwoRegisterViewer 
implements MouseInteractive {

    final static String TXT_HIDESHOW = "hide amoebae";
    
    final boolean ini_AmoebaeDisplay = true;
    /*--------------------
     * Attributes
     --------------------*/

    DictyoModel m_Model; // for call-back

    DiplayAmoebaeControl mF_DisplayAmoebae;
   
    boolean m_showAmoebae = ini_AmoebaeDisplay;


    /*--------------------
     * Other methods
     --------------------*/

    final static int COLOR_LIMIT = DictyoCell.FREECELL_LIMIT;

    /*--------------------
     * Constructor
     --------------------*/

    public DictyoViewer(final IntC in_CellSize, final IntC in_GridSize,
            final RegularDynArray in_Automaton, final DictyoModel in_Model) {
        super(in_CellSize, in_GridSize, in_Automaton);
        m_Model = in_Model;
        ActivateMouseFollower(this);
        // Macro.print(4,"creating special dictyo viewer");
    }

    /** returns the state of a cell */
    @Override
	public
    final int GetCellColorNumXY(final int x, final int y) {
        int color = 0;
        int pop = GetStateOneXY(x, y);
        int chem = GetStateTwoXY(x, y);
        color = chem;
        // pop
        if (pop == DictyoModel.OBSTACLE) {
            color = m_Model.GetExcited() + 3; // obstacle code
        } else if (m_showAmoebae && (pop != 0)) {
            // one amoeba
            if (pop <= COLOR_LIMIT) {
                color = m_Model.GetExcited() + 1;
            } else if (pop > COLOR_LIMIT) {
                color = m_Model.GetExcited() + 2;
            } else {
                Macro.FatalError("bad population !!! cell " + x + ":" + y + "pop :" + pop);
            }
        }
        return color;

    }

    /*--------------------
     * special
     --------------------*/

    /*
     * overrides
     */
    @Override
    public FLPanel GetSpecificPanel() {
        // Macro.Debug("viewerpanel");
        mF_DisplayAmoebae = new DiplayAmoebaeControl();
     
        // DO NOT REMOVE: camera related
        // mF_CalibrateCamera.AddActionListener(mF_CalibrateCamera);
        // mF_Start = new StartStreaming();
        // mF_Start.AddActionListener(mF_Start);

        // mF_Stop = new StopStreaming();
        // mF_Stop.AddActionListener(mF_Stop);

        FLPanel specificpanel = new FLPanel();
        specificpanel.add(mF_DisplayAmoebae);
        // DO NOT REMOVE: camera related
        // specificpanel.add(mF_UseCamera);
        // specificpanel.add(mF_CalibrateCamera);
        // specificpanel.add(mF_Start);
        // specificpanel.add(mF_Stop);
        return specificpanel;
    }

    // returns if a cell stable
    @Override
    protected boolean IsStableXY(final int x, final int y) {
        Macro.FatalError("Not implemented");
        return false;
    }

    /*--------------------
     * mapping methods
     --------------------*/

    /** puts an obstacle on a clicked cell */
    @Override
    final public void ProcessMouseEventXY(final int x, final int y, final MouseEvent event) {
        // Macro.Debug("x" + x + "y" + y);
        SetStateOneXY(x, y, DictyoModel.OBSTACLE);
        UpdateView();
    }

    class DiplayAmoebaeControl extends FLCheckBox {

        public DiplayAmoebaeControl() {
            super(TXT_HIDESHOW, !ini_AmoebaeDisplay);
            setOpaque(false);
        }

        @Override
        protected void SelectionOff() {
            m_showAmoebae = true;
        }

        @Override
        protected void SelectionOn() {
            m_showAmoebae = false;
        }

    }
    
}
