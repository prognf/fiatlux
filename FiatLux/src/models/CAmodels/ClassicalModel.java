package models.CAmodels; 
import components.allCells.Cell;
import components.allCells.ClassicalCACell;
import components.allCells.OneRegisterBlindCell;
import components.allCells.OneRegisterIntCell;

/*******************************************
 * A ClassicalModel is a Model that defines 
 * a function GetTransitionResult(Cell)
 * @author Nazim Fates
 *******************************************/

public abstract class ClassicalModel extends CellularModel { 
	
	static int countNewCell =0;
	
	abstract public int ApplyLocalFunction(OneRegisterIntCell in_Cell);
	
	public Cell GetNewCell(){ 
			return new ClassicalCACell(this); 
	}   
	
	@Override
	public Cell GetNewBlindCell(int state, boolean cellChange){ 
		return OneRegisterBlindCell.GetNewBlindCell(state,cellChange,GetRandomizer()); 
	}
	
	
	/** override if necessary
	 * max state can be used for graphical display **/
	public int GetMaxStateValue(){
		return -1;
	}
	
	
	/* INHERITED  
	 * 2D Viewer : called by simulation sampler  */
	/*public AutomatonViewer GetPlaneAutomatonViewer(
			IntC in_SquareDim, 
			SuperTopology in_TopologyManager, RegularDynArray in_Automaton) {
	}*/
	
}