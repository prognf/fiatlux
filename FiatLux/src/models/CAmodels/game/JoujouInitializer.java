package models.CAmodels.game;

import components.allCells.MultiRegisterCell;
import components.arrays.SuperSystem;
import grafix.gfxTypes.IntField;
import grafix.gfxTypes.elements.FLPanel;
import initializers.CAinit.ArrayInitializer;
import topology.basics.PlanarTopology;
import topology.basics.SuperTopology;

/*--------------------
 * Initializer of the model Identity
*--------------------*/

public class JoujouInitializer extends ArrayInitializer {
	/*--------------------
	 * Attributes
	 --------------------*/
	int msz_x, msz_y;
	
	/*--------------------
	 * construction
	 --------------------*/
	
	@Override
	public void LinkTo(SuperSystem system, SuperTopology topo){
		super.LinkTo(system, topo);
		// CAST
		PlanarTopology topo2D= (PlanarTopology)topo;
		msz_x= topo2D.GetXsize();
		msz_y= topo2D.GetYsize();
	}
	
	public int GetXsize(){
		return msz_x;
	}
	
	public int GetYsize(){
		return msz_y;
	}
	
	/* mapping */
	protected void SetAutomatonStatexy(int x, int y, int register, int state){
		SetAutomatonState(x + y * GetXsize(),register, state);
	}
	
	/* cell is the cell index */
	protected void SetAutomatonState(int cell, int register, int state){
		MultiRegisterCell c = (MultiRegisterCell)GetCell(cell);
		c.SetStateI(register,state);
	}
	
	final static int INITNOISE = 4;

	final static int INITFRAC = 5;

	IntField m_Field = new IntField("Init Style: (0)=random (1)=half/half", 1);

	public FLPanel GetSpecificPanel() {
		FLPanel panel = FLPanel.NewPanel(m_Field,this.GetRandomizer());
		panel.setOpaque(false);
		return panel;
	}

	/** main method * */
	public void SubInit() {
		switch (m_Field.GetValue()) {
		case 0:
			InitUni();
			break;
		case 1:
			break;
		case 2:
			break;

		}
	}
	
	final static int INIT_MORPHO= 1000;

	private void InitUni() {
		int Xsize = GetXsize();
		int Ysize = GetYsize();
		for (int x = 0; x < Xsize; x++) {
			for (int y = 0; y < Ysize; y++) {
				int state= RandomInt(4);
				SetAutomatonStatexy(x, y, JoujouCell.REGISTERSTATE, state);
				SetAutomatonStatexy(x, y, JoujouCell.REGISTERACTIVATION, 1);
			}
		}
		SetAutomatonStatexy(Xsize/2, Ysize/2, JoujouCell.REGISTERACTIVATION, 1);
	}

	
	
}// end class

