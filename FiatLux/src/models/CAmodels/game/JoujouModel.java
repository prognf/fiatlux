package models.CAmodels.game;

import components.allCells.Cell;
import components.arrays.RegularDynArray;
import components.types.IntC;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.RegularAutomatonViewer;
import initializers.SuperInitializer;
import models.CAmodels.CellularModel;
import topology.basics.PlanarTopology;
import topology.basics.SuperTopology;
import topology.basics.TopologyCoder;


/*--------------------
 rotor-like model with cascades ; from a game seen on the Internet...
 *--------------------*/
public class JoujouModel extends CellularModel {

	public final static String NAME = "JoujouModel";

	/*--------------------
	 * overrides & implementations
	 --------------------*/


	public SuperInitializer GetDefaultInitializer() {
		return new JoujouInitializer();
	}

	public Cell GetNewCell() {
		return new JoujouCell();
	}

	/* overloading this method */
	public FLPanel GetSpecificPanel() {
		FLPanel p = new FLPanel();
		return p;
	}

	@Override
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TV4;
	}

	/* overrides
	 * 2D Viewer : called by simulation sampler  */
	@Override
	public RegularAutomatonViewer GetPlaneAutomatonViewer(IntC cellPixSize, 
			SuperTopology topology, RegularDynArray automaton) {
		PlanarTopology topo2D= (PlanarTopology)topology;
		// default selection
		JoujouViewer av = new JoujouViewer(cellPixSize, topo2D.GetXYsize(), automaton);
		av.SetPalette( GetPalette() );
		return av;
	}

	/*--------------------
	 * Attributes
	 --------------------*/

	static private PaintToolKit m_palette;

	/*--------------------
	 * Constructor
	 --------------------*/
	public JoujouModel() {
		m_palette= new PaintToolKit();
		m_palette.AddColorM( 
				FLColor.c_white, FLColor.c_lightgreen, FLColor.c_lightblue, FLColor.c_medyellow );
	}

	/*--------------------
	 * Get & Set methods
	 --------------------*/

	/* painting procedure */
	final public PaintToolKit GetPalette() {
		return m_palette;
	}

}
