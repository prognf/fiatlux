package models.CAmodels.game;

import java.awt.Polygon;

import components.arrays.RegularDynArray;
import components.types.IntC;
import grafix.gfxTypes.FLColor;
import grafix.viewers.MultiRegisterViewer;


/*--------------------
 * @author Nazim Fates
 *--------------------*/
public class JoujouViewer extends MultiRegisterViewer 
/*implements MouseInteractive*/ 
{

	private static final int FOUR = 4;
	//Polygon N= new Polygon(), E=new Polygon(), S=new Polygon(), W=new Polygon();
	Polygon mN= new Polygon(),mE= new Polygon(),mS= new Polygon(),mW= new Polygon();
	Polygon [] TABTRIANGLE= { mN, mE, mS, mW};
	


	public JoujouViewer(IntC in_CellSize, IntC in_GridSize,	RegularDynArray automaton) {
		super(in_CellSize, in_GridSize, automaton);
	//	mN= new Polygon();		mE= new Polygon();
	//	mS= new Polygon();		mW= new Polygon();
		super.ComputeFourTriangles(mN,mE,mS,mW);//init four triangles
	}


	@Override
	protected boolean IsStableXY(int x, int y) {
		return false;
	}


	@Override
	public int GetCellColorNumXY(int x, int y) {
		return super.GetStateAXY(x, y);
	}

	@Override
	protected void DrawSystemState() {
		super.DrawUniformCellularBackGround(FLColor.c_white);
		
		// activated cells
		
		for (int x = 0; x < getXsize(); x++) {
			for (int y = 0; y < getYsize(); y++) {
				int state = GetStateAXY(x, y); 
				int activation= GetStateBXY(x, y); 
				//squares
				if (activation==1){//activated
					SetColor(FLColor.c_green);
				} else {
					SetColor(GetColor(state));
				}
				DrawSquare(x, y);
				
				// triangles
				int 	Xpos = getXpos(x),
						Ypos = getYpos(y);
				super.translateOrigin(Xpos, Ypos);
				
				SetColor(FLColor.c_black);
				FillTwoTriangles(state);
				ResetOrigin();	// to reset the Translate !!
				
			}
		}

		//	DrawPostEffect();
	}

	
	private void FillTwoTriangles(int state) {
		FillPolygon(TABTRIANGLE[state]);
		int next= (state + 1) % FOUR;
		FillPolygon(TABTRIANGLE[next]);
	}


}
