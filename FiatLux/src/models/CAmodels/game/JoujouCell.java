package models.CAmodels.game;

import components.allCells.MultiRegisterCell;
import topology.zoo.NESW_TM;


/*--------------------
 * Cell of the model (contains the tr. rule)
*--------------------*/
class JoujouCell extends MultiRegisterCell {

	private static final int N_LAYERS = 2;

	/*--------------------
	 * Attributes
	 --------------------*/

	
	/*--------------------
	 * Constructor
	 --------------------*/

	public JoujouCell() {
		super(N_LAYERS);
	}

	/*--------------------
	 * private methods
	 --------------------*/

	/*--------------------
	 *  methods
	 --------------------*/


	public void sig_Reset() {
		SetBufferI(0, 0);
		SetBufferI(1, 0);
		SetStateA(0);
		SetStateB(0);
		
	}

	final static int 
	NORTHD=NESW_TM.NORTH, EASTD= NESW_TM.EAST, 
	SOUTHD= NESW_TM.SOUTH, WESTD= NESW_TM.WEST;

	static final int REGISTERSTATE=0, REGISTERACTIVATION = 1;

	static final int FOUR = 4; 
	
	public void sig_UpdateBuffer() {
		
		int state= GetStateA();
		boolean activation= (GetStateB()==1);
		int newstate= activation?rotate(state):state;
		SetBufferA(newstate);
		boolean nextactivation= false;
		for (int dir=0; dir < FOUR; dir++){
			nextactivation = nextactivation || ActivateFromDir(dir,state);
		}
		SetBufferB(nextactivation?1:0);
	}

	/** reads current states and prepares transition to time t+1 */
	private boolean ActivateFromDir(int dir, int mystate) {
		int neighbState= GetNeighbourStateI(REGISTERSTATE, dir);
		int StateAc= GetNeighbourStateI(REGISTERACTIVATION, dir);
		return (StateAc==1) && iamlinked(dir,mystate) && (neighblinked(dir,neighbState));
	}

	/** condition A**/
	private boolean iamlinked(int dir, int mystate) {
		int diff= FOUR + mystate -dir;
		return (mystate==dir) || (diff==3);
	}

	/** condition B**/
	private boolean neighblinked(int dir, int neighbState) {
		int diff= FOUR + neighbState -dir; 
		return (diff%FOUR == 2) || (diff%FOUR == 1);
	}

	public void sig_MakeTransition() {
		SetStateA(GetBufferA());
		SetStateB(GetBufferB());
	}

	private int rotate(int state) {
		return (state+1)%FOUR;
	}



}
