package models.CAmodels.ternary;

import components.allCells.Cell;
import components.allCells.ClassicalCACell;
import components.allCells.OneRegisterIntCell;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import initializers.SuperInitializer;
import models.CAmodels.ClassicalModel;
import topology.basics.TopologyCoder;

/*--------------------
 * models excitable media
*--------------------*/
public class Life2SpeciesModel extends ClassicalModel {

	final public static String NAME="Life2Species"; 
		
	
	/*--------------------
	 * overrides & implementations
	 --------------------*/

	public SuperInitializer GetDefaultInitializer() {
		return new ThreeStateInitializer();
	}
	
	@Override
	public Cell GetNewCell() {
		ClassicalModel mod = this;
		return new ClassicalCACell(mod);
	}
	
	@Override
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TM8;
	}
		
	/*--------------------
	 * Attributes
	 --------------------*/
	
	
	
	/*--------------------
	 * Constructor
	 --------------------*/
	
	public Life2SpeciesModel() {
		super();
	}	
	
	/*--------------------
	 * main method
	 --------------------*/

	public final int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
		int n1= in_Cell.CountOccurenceOf(1);
		int n2= in_Cell.CountOccurenceOf(2);		
		int state= in_Cell.GetState();
		
		if (state==3){//neutral
			if (RandomEventDouble(1))
				return 1+ RandomInt(2);
			else 
				return 3;
		}
		
		boolean newlife1= LifeTransition(state,n1), 
				newlife2= LifeTransition(state,n2);
		int newstate;
		if (newlife2){
			   if (newlife1){
				   newstate = 3;
			   } else {
				   newstate =2;
			   }
		} else if (newlife1){
			   newstate = 1;
		} else {
			   newstate =0;
		}
		   
		return newstate;
	}
	
	private boolean LifeTransition(int state, int n) {
		return 	((state==0) && (n == 3)) ||
		   		((state!=0) && ((n ==2)||(n==3)));
		//return ((n) > 2);
	}

	

	/*--------------------
	 * Get & Set methods
	 --------------------*/

	/* overrides */
	public PaintToolKit GetPalette() {
		FLColor[] palette = new FLColor[4];
		palette[0] = FLColor.c_black;
		palette[1] = FLColor.c_yellow;
		palette[2] = FLColor.c_green;
		palette[3] = FLColor.c_red;
		return new PaintToolKit(palette);
	}
	
	
	/*--------------------
	 * Other methods
	 --------------------*/

}
