package models.CAmodels.ternary;

import java.awt.event.FocusEvent;

import components.types.FLString;
import components.types.RuleCode;
import grafix.gfxTypes.controllers.IntController;
import grafix.gfxTypes.elements.FLPanel;
import grafix.gfxTypes.elements.FLTextArea;
import grafix.gfxTypes.elements.FLTextField;
import main.Macro;
import models.CAmodels.tabled.ECAlookUpTable;


/*--------------------
 * controls ternary
*--------------------*/
public class TernaryTripleECAcontrol extends FLPanel {

	final static String DEF_LABEL = "ECA:";
	final static int LENWCODE=4*3, LENTCODE = 6;
	private static final RuleCode 
		WcodeA = new RuleCode(10),
		WcodeB = new RuleCode(110),
		WcodeC = new RuleCode(210);

	// main attribute
	ECAlookUpTable m_ecaTableA, m_ecaTableB, m_ecaTableC;

	// GFX
	ECAcodeParser m_InputFieldA = new ECAcodeParser(0); // inner class
	ECAcodeParser m_InputFieldB = new ECAcodeParser(1);
	ECAcodeParser m_InputFieldC = new ECAcodeParser(2);
   
    RuleCode m_ruleCode4= new RuleCode(304);
    MixedTransitionsCodeParser m_inputFieldMixed=
    		new MixedTransitionsCodeParser();
	FLTextArea m_WcodeDisplay = new FLTextArea(LENWCODE);
	//FLTextArea m_TcodeDisplay = new FLTextArea(LENTCODE); 
	TernaryTableModel m_model;
	
	/*--------------------
	 * Constructor
	 --------------------*/

	public TernaryTripleECAcontrol(TernaryTableModel model) {
		m_model=model;
		m_ecaTableA = new ECAlookUpTable(WcodeA);
		m_ecaTableB = new ECAlookUpTable(WcodeB);
		m_ecaTableC = new ECAlookUpTable(WcodeC);
		
		
		AddLabel("3ECA control");
		Add(m_InputFieldA, m_InputFieldB, m_InputFieldC, m_inputFieldMixed); 
		Add(m_WcodeDisplay);
		Update();
	}


	/*--------------------
	 * MAIN FUNCTION
	 --------------------*/

	/** sets the rule */
	public void SetModelCode(RuleCode Wcode, int id) {
		if (Wcode.IsInBounds(256)) {
			switch (id) {
			case 0:
				m_ecaTableA.SetRule(Wcode); 
				processA(m_ecaTableA,0); break;
			case 1:
				m_ecaTableB.SetRule(Wcode); 
				processA(m_ecaTableB,1); break;
			case 2:
				m_ecaTableC.SetRule(Wcode); 
				processA(m_ecaTableC,2); break;
			default:
				Macro.FatalError(" zozomistake");
			}
			Update();
		} else {
			Macro.print("error : Wcode out of bounds");
		}
	}

	/* sets the ternary model table for ECAlookUpTable
	 *8 times,  one transition at a time **/
	private void processA(ECAlookUpTable ecaTable, int delta) {
		//DEBUG
		for (int i=0; i<8; i++) {
			int x=i%2, y= (i/2)%2, z=(i/4)%2; // in {0,1}
			x=(x+delta)%3;
			y=(y+delta)%3;
			z=(z+delta)%3;
			int lutpos=9*z+3*y+x;
			int trans3=m_model.GetBitTable(lutpos);
			int newval= (ecaTable.getBitTable(i)+delta)%3;
			Macro.fPrint("%d : %d%d%d %d::%d=>%d", 
					i,z,y,x,lutpos,trans3,newval);
			
			m_model.setTransitionOneByOne(lutpos, newval);
		}
	}


	protected void Update(){
		RuleCode 
		WcodeA = m_ecaTableA.GetRuleCode(),
		WcodeB = m_ecaTableB.GetRuleCode(),
		WcodeC = m_ecaTableC.GetRuleCode();
		String scode=String.format("%s::%s::%s",WcodeA,WcodeB,WcodeC);		
		m_WcodeDisplay.SetText(scode);
		//m_TcodeDisplay.SetText(Tcode)
	}

	/** parsing ECA code */
	class ECAcodeParser extends FLTextField {

		int m_id; // 01/12/20
		
		ECAcodeParser(int id) {
			super(LENTCODE);
			m_id=id;
		}

		public void focusLost(FocusEvent e) {}
		
		public void parseInput(String input) {
			RuleCode Wcode;
			// entry is Tcode
			if ( ECAlookUpTable.IsRegularTcode(input)) {
				Wcode = ECAlookUpTable.TcodeToWcode(input);
				SetModelCode(Wcode,m_id);
			} else {
			// entry is Wcode
				try{
					int code = Integer.parseInt(input);
					SetModelCode(new RuleCode(code),m_id);
				} catch (NumberFormatException e){
					FLString.ParsingError(DEF_LABEL, input);
				}
			}
		}
	}

	static int [] toTernary(int val) {
		int [] res= new int[6];
		for (int i=0; i<6; i++) {
			res[i]=val%3;
			val/=3;
		}
		//String tostr= FLString.ToString(res);
		return res;
	}
	
	
	static int [] Yp= {1,2,0,2,0,1};
	static int [] Zp= {2,1,2,0,1,0};
	
	class MixedTransitionsCodeParser extends IntController {

		public MixedTransitionsCodeParser() {
			super("mixedTr. code");
		}

		@Override
		protected int ReadInt() {
			return (int)m_ruleCode4.GetCodeAsLong();
		}

		@Override
		protected void SetInt(int val) {
			m_ruleCode4= new RuleCode(val);
			int [] tab3= toTernary(val);
			for (int i=0; i< 6; i++) {
				int x=i/2, y = Yp[i], z=Zp[i];
				int lutpos= 9*z+3*y+x;
				m_model.setTransitionOneByOne(lutpos, tab3[i]);
			}
		}
		
	}
	
}
