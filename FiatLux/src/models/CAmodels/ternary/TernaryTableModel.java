package models.CAmodels.ternary;

import components.allCells.OneRegisterIntCell;
import components.types.RuleCode;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.elements.FLPanel;
import initializers.SuperInitializer;
import main.Macro;
import main.MathMacro;
import models.CAmodels.tabled.TabledModelNstate;
import topology.basics.TopologyCoder;


/*--------------------
 * a three-state model with a look-up table
*--------------------*/
public class TernaryTableModel extends TabledModelNstate 
{

	public final static String NAME="GeneralTernary";
	private static final int K = 3, DEFLUTSIZE=MathMacro.Power(K, 3);
	private static Long WCODEDEF= 123456789L;
	
	QuarkViewControlSquares m_view; // view of the model, updatable
	//QuarkViewText m_view;
	QuarkWcodeControl m_QcodeControl;
	
	/*--------------------
	 * Constructor
	 --------------------*/
	
	/** base 3 **/
	public TernaryTableModel(){
		super(3,DEFLUTSIZE);
		SetRuleByWcode(new RuleCode(WCODEDEF));
	}
	
	/** setting the table vals **/
	public void SetRuleByListOfTransitions(String listOfTransitions) {
		GetLookUpTable().SetRuleByListOfTransitions(listOfTransitions);
		Macro.PrintEmphasis(" tbl" );
		GetLookUpTable().PrintTable();
	}
	
	/*--------------------
	 * overrides
	 --------------------*/
	
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TLR1;
	}
	
	/*--------------------
	 * Abstract method definition
	 --------------------*/
	
	/** MAIN FUNCTION */
	public int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
		int NeighbourhoodNum = 0;
		for (int i=0; i< in_Cell.GetNeighbourhoodSize(); i++){
			NeighbourhoodNum*= K;
			NeighbourhoodNum+= in_Cell.ReadNeighbourState(i);
		}
		return GetTransitionResult(NeighbourhoodNum);
	}
	
	public SuperInitializer GetDefaultInitializer() { 
		return new ThreeStateInitializer();
	}
	
	/*--------------------
	 * Get / Set
	 --------------------*/
	
	public String GetScode() {
		return GetLookUpTable().GetScode();
	}
	
	//@Override
	public FLPanel GetSpecificPanel() {
		m_QcodeControl= new QuarkWcodeControl(this);
		//m_view= new QuarkViewText(this);
		m_view= new QuarkViewControlSquares(this);
		m_view.showInNewFrame("QUARK.C");
		return FLPanel.NewPanel(m_QcodeControl);
	}			
	
	/* overrides */
	public PaintToolKit GetPalette() {
		FLColor[] palette = new FLColor[4];
		palette[0] = FLColor.c_black;
		palette[1] = FLColor.c_yellow;
		palette[2] = FLColor.c_grenat;
		palette[3] = FLColor.c_red;
		return new PaintToolKit(palette);
	}
	
	/*--------------------
	 * setting the LUT
	 --------------------*/
	public void setTransitionOneByOne(int lutpos, int newval) {
		this.GetLookUpTable().setTransition(lutpos, newval);
		Update();
	}

	public void Update() {
		if (m_view!=null) {
			this.m_view.Update();
		}
		if (m_QcodeControl!=null) {
			m_QcodeControl.m_init=true;
			m_QcodeControl.Update();
		}
		
	}


	/* special for QUARK **/
	public int calculateQuarkCode() {
		return 0;
	}
	
	/*--------------------
	 * testing
	 --------------------*/
	
	public static void DoTest() {		
	}

	


}
