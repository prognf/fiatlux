package models.CAmodels.ternary;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JComponent;

import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.elements.FLLabel;
import grafix.gfxTypes.elements.FLPanel;
import main.Macro;

class QuarkViewControlSquares extends FLPanel 
implements MouseListener {
	
	// from the QUARK order to the LUT order
	public static int [] TRANSORDER= {0,1,3,4,2,6,8,5,7};
	
	private static final String LABEL = "QUARK TABLE";
	private static final int LRECTANGLE = 20;
	
	protected TernaryTableModel m_model;
	ActiveRectangle[] m_activeRect; // clickable rect
	private PaintToolKit m_palette; //3 colors

	
	public QuarkViewControlSquares(TernaryTableModel in_model) {
		m_model= in_model;
		this.SetGridBagLayout();
		m_activeRect= new ActiveRectangle[27];
		for (int bitPos=0 ; bitPos < 27; bitPos++){
			m_activeRect[bitPos] = new ActiveRectangle(bitPos);
			m_activeRect[bitPos].addMouseListener(this);
		}
		
		m_palette=m_model.GetPalette();
		setUpDisplay();
		
	}	
	
	
	final void setUpDisplay() {
		for (int i=0; i<9; i++) {
			int lutpos= TRANSORDER[i];
			int lutpos1= shift(lutpos,1), lutpos2= shift(lutpos,2);
			//int x=lutpos%3, y=(lutpos/3)%3, z=(lutpos/9)%3; // z should always be zero here
			FLLabel 
				lbl1= FLLabel.newObject( getTextTransition(lutpos) ),
				lbl2= FLLabel.newObject( getTextTransition(lutpos1) ),
				lbl3= FLLabel.newObject( getTextTransition(lutpos2) );
			this.AddGridBag(lbl1, 0, i);
			this.AddGridBag(m_activeRect[lutpos], 1, i);
			this.AddGridBag(lbl2, 2, i);
			this.AddGridBag(m_activeRect[lutpos1], 3, i);
			this.AddGridBag(lbl3, 4, i);
			this.AddGridBag(m_activeRect[lutpos2], 5, i);
		}
		
	}
	
	
	
	/* control */
	private void shiftState(int lutpos) {
		int lutpos1= shift(lutpos,1), lutpos2= shift(lutpos,2);
		shiftStateOneTime(lutpos);
		shiftStateOneTime(lutpos1);
		shiftStateOneTime(lutpos2);
	}
	
	private void shiftStateOneTime(int lutpos) {
		int q=m_model.GetBitTable(lutpos);
		int q2=(q+1)%3;
		m_model.setTransitionOneByOne(lutpos, q2);
		Update();
	}

	private int shift(int lutpos, int delta) {
		int x=lutpos%3, y=(lutpos/3)%3, z=(lutpos/9)%3;
		x=(x+delta)%3;
		y=(y+delta)%3;
		z=(z+delta)%3;
		return 9*z+3*y+x;
	}
	/* formatting transition i */
	public String getTextTransition(int lutpos) {
		int x=lutpos%3, y=(lutpos/3)%3, z=(lutpos/9)%3; 
		String tr=String.format("(%2d) %d%d%d", lutpos, z,y,x);
		return tr;
	}


	/* --------------------- MOUSE LISTENER -----------------------------*/

	public void mousePressed(MouseEvent e) {
	}

	public void mouseReleased(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mouseClicked(MouseEvent e) {
		if (e.getSource() instanceof ActiveRectangle ){
			int lutpos= ((ActiveRectangle)e.getSource()).m_lutpos;
			shiftState(lutpos);
		} else {
			Macro.print("not in ACTIVE RECT...");
		}
	}

	/* --------------------- GFX -----------------------------*/
	private static final Dimension DIM = new Dimension(LRECTANGLE, LRECTANGLE);

	class ActiveRectangle extends JComponent {

		public int m_lutpos;

		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			int q= m_model.GetBitTable(m_lutpos);
			Color col= m_palette.GetColor(q); //0,1,2
			g.setColor(col);
			g.fillRect(0,0,LRECTANGLE-1,LRECTANGLE-1);
		}

		ActiveRectangle(int lutpos){
			m_lutpos= lutpos;
			setPreferredSize(DIM);
			setVisible(true);
		}

	}
	
	public void Update() {
		for (int i=0; i<27; i++) {
			m_activeRect[i].repaint();
		}
	}

}
