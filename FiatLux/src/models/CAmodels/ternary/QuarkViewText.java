package models.CAmodels.ternary;

import grafix.gfxTypes.elements.FLPanel;
import grafix.gfxTypes.elements.FLTextArea;
import models.CAmodels.tabled.LookUpTableNstate;

class QuarkViewText extends FLPanel {
	
	private static final String LABEL = "QUARK TABLE TEXT";
	protected TernaryTableModel m_model;
	FLTextArea m_transitionsDisplay= new FLTextArea(8,33);
	
	public QuarkViewText(TernaryTableModel in_model) {
		m_model= in_model;
		this.Add(m_transitionsDisplay);
		Update();
	}	
	
	static int [] TRANSORDER= {0,1,3,4,2,6,8,5,7};
	
	
	final void Update() {
		String s="";
		for (int i=0; i<9; i++) {
			int lutpos= TRANSORDER[i];
			int lutpos1= shift(lutpos,1), lutpos2= shift(lutpos,2);
			//int x=lutpos%3, y=(lutpos/3)%3, z=(lutpos/9)%3; // z should always be zero here
			s+=getTextTransition(lutpos)+" | " + 
			getTextTransition(lutpos1) + " | " + getTextTransition(lutpos2) + "\n";
		}
		m_transitionsDisplay.SetText(s);
	}
	private int shift(int lutpos, int delta) {
		int x=lutpos%3, y=(lutpos/3)%3, z=(lutpos/9)%3;
		x=(x+delta)%3;
		y=(y+delta)%3;
		z=(z+delta)%3;
		return 9*z+3*y+x;
	}
	/* formatting transition i */
	public String getTextTransition(int lutpos) {
		int x=lutpos%3, y=(lutpos/3)%3, z=(lutpos/9)%3; 
		int newq= m_model.GetBitTable(lutpos);
		String tr=String.format("%2d) %d%d%d->%d", lutpos, z,y,x, newq);
		return tr;
	}

	final private LookUpTableNstate GetLUT() {
		return m_model.GetLookUpTable();
	}
	




}
