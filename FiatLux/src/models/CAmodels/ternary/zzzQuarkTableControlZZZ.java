package models.CAmodels.ternary;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JComponent;

import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.elements.FLLabel;
import grafix.gfxTypes.elements.FLPanel;
import models.CAmodels.tabled.LookUpTableNstate;

class zzzQuarkTableControlZZZ extends FLPanel 
implements MouseListener {
	//abstract public void RefreshDisplay();

	private static final int LRECTANGLE = 20;
	final static int TEXT_WIDTH = 15, LEN_CODE=4;

	int m_size; // number of bits of the table

	protected TernaryTableModel m_model;

	ActiveRectangle[] m_activeRect;
	private PaintToolKit m_palette; //3 colors

	public zzzQuarkTableControlZZZ(TernaryTableModel in_model) {
		m_model= in_model;
		m_size = GetLUT().GetSize();

		m_palette=m_model.GetPalette();

		// squares that control each cell of the transition table
		m_activeRect = new ActiveRectangle[m_size];
		for (int bitPos=0 ; bitPos < m_size; bitPos++){
			m_activeRect[bitPos] = new ActiveRectangle(bitPos);
			m_activeRect[bitPos].addMouseListener(this);
		}
		constructGFX();
	}	

	private void constructGFX() {
		FLPanel lineTabcontrol= new FLPanel();
		lineTabcontrol.SetGridBagLayout();
		
		for (int lutpos=0 ; lutpos < m_size; lutpos++){
			FLPanel squareAndLabel= new FLPanel();
			squareAndLabel.SetGridBagLayout();
			
			
			FLLabel bitNum= new FLLabel(getTextTransition(lutpos));
			squareAndLabel.AddGridBag(bitNum,0,0);
			squareAndLabel.AddGridBag(m_activeRect[lutpos],1,0);
			// adding the component + the mouse listener
			lineTabcontrol.AddGridBag(  squareAndLabel, 0,lutpos);
			
		}
		Add(lineTabcontrol);
	}
	
	/* formatting transition i */
	public String getTextTransition(int i) {
		int bit= m_model.GetBitTable(i);
		int val=i;
		int x=val%3; val/=3;
		int y=val%3; val/=3;
		int z=val%3;
		String tr=String.format("%d) %d%d%d->%d", i,x,y,z,bit);
		return tr;
	}

	final private LookUpTableNstate GetLUT() {
		return m_model.GetLookUpTable();
	}

	protected void UpdateAllCellTableControls() {
		for (ActiveRectangle bitControl : m_activeRect){
			bitControl.repaint();
		}
	}


	public void mousePressed(MouseEvent e) {
	}

	public void mouseReleased(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
		//eventOutput("Mouse entered", e);
	}

	public void mouseExited(MouseEvent e) {
		//eventOutput("Mouse exited", e);
	}

	public void mouseClicked(MouseEvent e) {
		if (e.getSource() instanceof ActiveRectangle ){
			int lutpos= ((ActiveRectangle)e.getSource()).m_lutpos;
			m_model.setTransitionOneByOne(lutpos, 0);//FIXME BUG

		} else{
			// ???
		}

	}

	/* --------------------- GFX -----------------------------*/
	private static final Dimension DIM = new Dimension(LRECTANGLE, LRECTANGLE);

	class ActiveRectangle extends JComponent {

		public int m_lutpos;

		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			int q= m_model.GetBitTable(m_lutpos);
			Color col= m_palette.GetColor(q); //0,1,2
			g.setColor(col);
			g.fillRect(0,0,LRECTANGLE-1,LRECTANGLE-1);
		}

		ActiveRectangle(int lutpos){
			m_lutpos= lutpos;

			setPreferredSize(DIM);
			setVisible(true);
		}

	}

	/* updates the display **/
	public void RefreshDisplay() {
		UpdateAllCellTableControls();
	}

	/** TODO **/
	public void Update() {
		// TODO Auto-generated method stub
		
	}

}
