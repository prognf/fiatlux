package models.CAmodels.ternary;

import grafix.gfxTypes.controllers.IntController;


/*--------------------
 * controls ternary
 *--------------------*/
public class QuarkWcodeControl extends IntController {

	final static String DEF_LABEL = "Quark[0-575]:";

	// attributes
	TernaryTableModel m_model;
	public boolean m_init= false;

	/*--------------------
	 * Constructor
	 --------------------*/

	public QuarkWcodeControl(TernaryTableModel model) {
		super(DEF_LABEL);
		m_model=model;
		Update();
	}


	/*--------------------
	 * MAIN FUNCTION
	 --------------------*/

	/* sets the LUT of the corresponding ternary model **/
	public void setQuarkCode(int qcode){
		int [] quarklut= codeQuarkToTab(qcode);
		setQuark(quarklut);
	}

	/** codeW -> tab **/
	public int[] codeQuarkToTab(int codeQ) {
		int [] tabtrans= new int [8];
		for (int i=0; i< 6; i++) {
			tabtrans[i]=codeQ%2;
			codeQ/=2;
		}
		tabtrans[6]=codeQ%3;
		codeQ/=3;
		tabtrans[7]=codeQ%3;
		return tabtrans;
	}
	
	static int [] LUTPOS= {1,3,4,2,6,8,5,7};
	
	/** codeW -> tab **/
	public int tabToQuarkCode() {
		int W=0;
		int mult=1;
		int lutpos;
		for (int i=0; i< 6; i++) { // eight ints, last two are between 0 and 2
			lutpos= LUTPOS[i];
			int bit=m_model.GetBitTable(lutpos);
			if (bit==2) // code : 2 is "1" in the code sum
				bit=1;
			W += bit*mult;
			mult*=2;
		}
		lutpos=LUTPOS[6];
		W+=m_model.GetBitTable(lutpos)*mult;
		mult*=3;
		lutpos=LUTPOS[7];
		W+=m_model.GetBitTable(lutpos)*mult;
		return W;
	}

	private void setQuark(int [] transTable) {
		setTripleT(0,0,0,0);
		setTripleT(0,0,1,transTable[0]*1); //0,1
		setTripleT(0,1,0,transTable[1]*1);
		setTripleT(0,1,1,transTable[2]*1);
		setTripleT(0,0,2,transTable[3]*2); //0,2
		setTripleT(0,2,0,transTable[4]*2);
		setTripleT(0,2,2,transTable[5]*2);
		setTripleT(0,1,2,transTable[6]);
		setTripleT(0,2,1,transTable[7]);
	}


	/** mind the order : z,y,x **/
	private void setTripleT(int z, int y, int x, int tval) {
		setTrans(z,y,x,tval);
		setTrans(z+1,y+1,x+1,tval+1);
		setTrans(z+2,y+2,x+2,tval+2);
	}

	private void setTrans(int z, int y, int x, int tval) {
		int lutpos=9*(z%3)+3*(y%3)+(x%3);
		m_model.setTransitionOneByOne(lutpos, (tval%3));
	}

	@Override
	protected int ReadInt() {
		if (!m_init)
			return -1;
		else
		  return tabToQuarkCode();
	}

	@Override
	protected void SetInt(int val) {
		setQuarkCode(val);
	}
	

}
