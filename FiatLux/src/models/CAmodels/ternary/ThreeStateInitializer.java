package models.CAmodels.ternary;

import grafix.gfxTypes.DoubleField;
import grafix.gfxTypes.elements.FLPanel;
import initializers.CAinit.OneRegisterInitializer;
import main.MathMacro;

/*--------------------
 * Initializer of the model
 *--------------------*/
class ThreeStateInitializer extends OneRegisterInitializer {
	/*--------------------
	 * attributes
	 --------------------*/

	final static double DEF_RATE= 60;
	private int m_NSTATE = 2;
	DoubleField m_Field = new DoubleField("Init Rate (in %)", 4, DEF_RATE);

	/*--------------------
	 * implementations
	 --------------------*/

	public FLPanel GetSpecificPanel() {
		return m_Field;
	}

	// n particles at the beginning
	protected void SubInit() {
		int size = GetLsize();
		double rate= m_Field.GetValue() / MathMacro.HUNDRED;
		for (int cell = 0; cell < size; cell++) {
			int state=0;
			if (RandomEventDouble(rate)){
				state= 1+ RandomInt(m_NSTATE);
			}
			InitState(cell, state); 
		}

	}

	/*--------------------
	 * get / set
	 --------------------*/

	public void SetNState(int N){
		m_NSTATE= N;
	}


}// end class
