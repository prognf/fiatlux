package models.CAmodels.ternary;

import components.types.RuleCode;
import grafix.gfxTypes.elements.FLFrame;
import grafix.gfxTypes.elements.FLPanel;
import grafix.gfxTypes.elements.FLTextArea;
import grafix.gfxTypes.elements.FLTextField;

/* view: lookup table in a separate frame
 * control: inputfield 
 */
public class TernaryTableViewControl extends FLPanel {

	final static String LEGEND = "LUT:";
	final static int LENLUT = 27;

	ReadLutField m_InputField = new ReadLutField(); // inner class
	FLTextArea m_ScodeDisplay = new FLTextArea(LENLUT);
	FLTextArea m_transitionsDisplay= new FLTextArea(LENLUT+1,7);

	private TernaryTableModel m_model;

	/*--------------------
	 * Constructor
	 --------------------*/

	/** shows inside a frame at creation **/
	public TernaryTableViewControl(TernaryTableModel model) {
		m_model = model;
		this.SetBoxLayoutY();
		AddLabel(LEGEND);
		add(m_InputField);
		add(m_ScodeDisplay);
		showInFrame();
		Update();
	}
	
	public void showInFrame() {
		/*FLFrame frame= new FLFrame("transition table");
		frame.addPanel(m_transitionsDisplay);
		frame.packAndShow();*/
		FLFrame frm2= new FLFrame("LUT clickable");
		frm2.addPanel(new zzzQuarkTableControlZZZ(m_model));
		frm2.packAndShow();
	}

	/*--------------------
	 * MAIN FUNCTION
	 --------------------*/

	/** sets the number and updates the transition code */
	public void SetRuleByListOfTransitions(String Scode) {
		m_model.SetRuleByListOfTransitions(Scode);
		Update();
	}

	protected void Update(){
		String Scode = m_model.GetScode();
		m_ScodeDisplay.SetText(Scode);
		
		String tab="";
		for (int i=0; i<LENLUT; i++) {
			tab+=getTextTransition(i)+"\n";
		}
		RuleCode Wcode= m_model.GetLookUpTable().GetRuleCode();
		tab+= ":***" + Wcode + ":";
		m_transitionsDisplay.SetText(tab);
	}
	
	/* formatting transition i */
	public String getTextTransition(int i) {
		int bit= m_model.GetBitTable(i);
		int val=i;
		int x=val%3; val/=3;
		int y=val%3; val/=3;
		int z=val%3;
		String tr=String.format("%d) %d%d%d->%d", i,x,y,z,bit);
		return tr;
	}

	/* processing user input */
	class ReadLutField extends FLTextField {

		ReadLutField() {
			super(LENLUT);
		}

		public void parseInput(String input) {
			SetRuleByListOfTransitions(input);
		}
	}

}