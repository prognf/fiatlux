package models.CAmodels; 
import components.allCells.Cell;
import components.allCells.ClassicalMultiRegisterCell;
import main.Macro;


/*******************************************
 * A ClassicalModel is a Model that defines 
 * a function GetTransitionResult(Cell)
 * @author Nazim Fates
 *******************************************/

public abstract class ClassicalMultiRegisterModel extends CellularModel{ 
	
	abstract public int GetnLayers(); // how many layers in the model ?
	abstract public int [] GetTransitionResult(ClassicalMultiRegisterCell in_Cell); // transition function
	
	public Cell GetNewCell(){ 
		return new ClassicalMultiRegisterCell(this); 
	}   
	
	@Override
	public Cell GetNewBlindCell(int state, boolean cellChange){ 
		 Macro.SystemWarning(" Multi-register Blind cell is not implemented - please use regular borders");
		 return null;
	}

	 
	
}
