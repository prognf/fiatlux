package models.CAmodels.stochastic;

import components.allCells.OneRegisterIntCell;
import components.types.DoublePar;
import grafix.gfxTypes.elements.FLPanel;
import models.CAmodels.binary.BinaryModel;

/*--------------------
 * Model: Majority Model Description: classical majority without noise
 * 
*--------------------*/
public class NotIsingModel extends BinaryModel {

	public static final String NAME = "NotIsing";
	final static int ZERO=0, ONE=1;
	DoublePar m_Noise= new DoublePar("Noise:", 0.1);


	public NotIsingModel() {
		super();
	}

	public final int ApplyLocalFunction(OneRegisterIntCell inCell){
		return NotIsingRule(inCell);
	}


	public int NotIsingRule(OneRegisterIntCell inCell) {
		int nAlive = inCell.CountOccurenceOf(ONE);
		int nDead = inCell.CountOccurenceOf(ZERO);
		if (nAlive==0){
			return ZERO;
		} else if (nDead==0){
			return ONE;
		} else {
			int maj=(nAlive > nDead)?ONE:
				((nAlive < nDead)?
						ZERO:
							inCell.GetState());
			boolean noise= RandomEventDouble(m_Noise.GetVal());
			return noise ? 1-maj : maj;
		}

	}
	
	@Override
	public FLPanel GetSpecificPanel(){
		return m_Noise.GetControl();
	}
	
}

