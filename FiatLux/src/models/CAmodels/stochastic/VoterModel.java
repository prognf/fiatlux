package models.CAmodels.stochastic;

import components.allCells.OneRegisterIntCell;
import components.types.DoublePar;
import grafix.gfxTypes.elements.FLPanel;
import models.CAmodels.binary.BinaryModel;

/*--------------------
 * Model: Majority Model Description: classical majority without noise
 * 
*--------------------*/
public class VoterModel extends BinaryModel {

	public static final String NAME = "Voter";
	final static int ZERO=0, ONE=1;
	DoublePar m_Noise= new DoublePar("Noise:", 0.0);


	public VoterModel() {
		super();
	}

	public final int ApplyLocalFunction(OneRegisterIntCell inCell){
		return voterRule(inCell);
	}


	final public int voterRule(OneRegisterIntCell inCell) {
		int N = inCell.GetNeighbourhoodSize();
		int neighb= RandomInt(N);
		int q = inCell.GetNeighbour(neighb).GetState();
		boolean noise= RandomEventDouble(m_Noise.GetVal());
			return noise ? 1-q : q;
		
	}
	
	@Override
	public FLPanel GetSpecificPanel(){
		return m_Noise.GetControl();
	}
	
}

