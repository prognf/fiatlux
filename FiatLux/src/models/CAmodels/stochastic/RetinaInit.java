package models.CAmodels.stochastic;

import components.types.IntPar;
import grafix.gfxTypes.elements.FLPanel;
import initializers.CAinit.OneRegisterInitializer;

public class RetinaInit extends OneRegisterInitializer {

	
	private static final String TXTNSTATES = "Nstates ini";
	private RetinaModel m_model;
	IntPar m_nstates = new IntPar(TXTNSTATES,3);

	RetinaInit(RetinaModel model){
		m_model= model;
		//m_nstates.SetVal(m_model.GetStatesNum());
	}
	
	@Override
	protected void SubInit() {
		for (int i = 0; i < GetLsize(); i++) {
			int colnum= RandomInt(m_nstates.GetVal());
			InitState(i, colnum);
		}
	}
	
	public FLPanel GetSpecificPanel(){
		return FLPanel.NewPanel(m_nstates, this.GetRandomizer());
	}

}
