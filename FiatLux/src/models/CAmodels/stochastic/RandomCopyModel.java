package models.CAmodels.stochastic;

import components.allCells.Cell;
import components.allCells.ClassicalCACell;
import components.allCells.OneRegisterIntCell;
import grafix.gfxTypes.PaintToolKit;
import initializers.SuperInitializer;
import initializers.CAinit.UniformInit;
import models.CAmodels.ClassicalModel;
import topology.basics.TopologyCoder;

/*--------------------
 * choose one neighbour at random and copy its state
 *--------------------*/

public class RandomCopyModel extends ClassicalModel {

	public static final String NAME = "RandomCopyZZP";
	public static final int NCOLOR = 2;
	private static final boolean DEFMODE = true;

	/*--------------------
	 * overrides & implementations
	 --------------------*/

	public final int ApplyLocalFunction(OneRegisterIntCell in_Cell) {

		int neighbSize= in_Cell.GetNeighbourhoodSize();
		int selected = RandomInt(neighbSize);
		int newstate= in_Cell.ReadNeighbourState(selected);
		if (m_PhaseSynchMode){
			newstate = (newstate + 1)%NCOLOR;
		}
		return newstate;
	}

	public SuperInitializer GetDefaultInitializer() {
		UniformInit init=  new UniformInit();
		init.SetStateRange(NCOLOR);
		return init;
	}

	public Cell GetNewCell() {
		return new ClassicalCACell(this);
	}

	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TLR1;
	}

	/*--------------------
	 * Attributes
	 --------------------*/

	boolean m_PhaseSynchMode= DEFMODE; 

	/*--------------------
	 * Constructor
	 --------------------*/
	public RandomCopyModel() {
		super();
	}

	/*--------------------
	 *  Get & Set methods
	 --------------------*/


	/* overrides */
	public PaintToolKit GetPalette() {
		//return PaintToolKit.GetLeLorrainPalette(CYCLELEN);
		return PaintToolKit.GetRandomPaletteSafeWeb(NCOLOR, this.GetRandomizer());
		//return PaintToolKit.GetRandomPalette(CYCLELEN);
	}

	/*--------------------
	 *  Other methods
	 --------------------*/
}
