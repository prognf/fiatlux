package models.CAmodels.stochastic;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import components.types.IntegerList;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.elements.FLFrame;
import main.Macro;

public class SimonHantai extends JPanel {

	private static final String FILENAME = "AlainCauchie-1.jpg";


	private static final double BX=200, EX=500, SX = (EX-BX)/130;


	private static final String COLORDATFILE = "color-130.data";


	private BufferedImage m_img= null;
    int m_X, m_Y;
	IntegerList m_col;


	public SimonHantai() {

	}

	private void Display(){
		FLFrame frame = new FLFrame("Simon Hantai");
		frame.setSize(530, 630);
		frame.addPanel(this);
		frame.Show();
	}

	private void ReadColorsFromDatFile() {
		m_col= IntegerList.ReadFromFile(COLORDATFILE);
	}

	private void ReadColorsFromCauchie() {
		try {
			m_img = ImageIO.read(new File(FILENAME));
		} catch (IOException e) {
			Macro.FatalError("file not found" + FILENAME);
		}
		m_X= m_img.getWidth();
		m_Y= m_img.getHeight();
		m_col = new IntegerList();
		for (double x= BX; x < EX; x+= SX){
			int xread= (int)x;
			int rgb = m_img.getRGB(xread, m_Y/2);
			m_col.addVal(rgb);
			//LineV(xread,rgb);
		}
		this.repaint();
		m_col.Print();
		//check this one (modified)
		m_col.WriteToFileCol("color-130.data");
	}

	private void LineV(int x, int rgb) {
		for (int y=0; y< m_Y; y++){
			m_img.setRGB(x, y, rgb);
		}
	}

	private PaintToolKit GetPalette() {
		PaintToolKit pal= new PaintToolKit();
		ReadColorsFromDatFile();
		for (int rgb : m_col){
			int r = (rgb >> 16) & 0xFF;
			int g = (rgb >> 8) & 0xFF;
			int b = (rgb & 0xFF);
			String out= String.format("adding col: %d %d %d",r,g,b);
			Macro.print(out);
			pal.AddColorRGB(r, g, b);
		}
		return pal;
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(m_img, 0, 0, null);
	}

	public static PaintToolKit GetAlainChauchiePalette(){
		SimonHantai h= new SimonHantai();
		h.ReadColorsFromDatFile();
		return h.GetPalette();
	}





	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();

	public static void main(String[] argv) {
		Macro.BeginTest(CLASSNAME);
		SimonHantai h= new SimonHantai();
		h.ReadColorsFromCauchie();
		Macro.EndTest();
	}





}
