package models.CAmodels.stochastic;

import java.io.File;

import javax.swing.JFileChooser;

import components.allCells.OneRegisterIntCell;
import components.types.FLString;
import components.types.FLStringList;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.IconManager;
import grafix.gfxTypes.controllers.MultiDoubleController;
import grafix.gfxTypes.elements.FLActionButton;
import grafix.gfxTypes.elements.FLFrame;
import grafix.gfxTypes.elements.FLPanel;
import initializers.SuperInitializer;
import initializers.CAinit.BinaryInitializer;
import main.Macro;
import main.MathMacro;
import models.CAmodels.binary.BinaryModel;
import topology.basics.TopologyCoder;

/*--------------------
 * a binary model with a look-up table
*--------------------*/
public class ProbabilisticLineR2Model extends BinaryModel {

	public final static String NAME = "StochosLineR2";
	private static final int SNEIGHB = 5, SZ = 32;

	final double[] m_transitionProba;

	/*--------------------
	 * Constructor
	 --------------------*/

	public ProbabilisticLineR2Model() {
		m_transitionProba = new double[SZ];
		// 0,1 quiescent
		m_transitionProba[SZ - 1] = 1.;
		for (int tr = 1; tr < SZ - 1; tr++) { // init random rule
			m_transitionProba[tr] = 0;
			// String sbin = MathMacro.Int2BinaryString(tr, SZ);
			// int nones = FLString.countOccurences('1', sbin);
			// m_transitionProba[tr] = nones / (double) 5;
			// m_transitionProba[tr]= RandomDouble();
		}
		//printTable();
	}
	
	/** construct from file **/
	public ProbabilisticLineR2Model(String filename) {
		this();
		loadRuleFromFile(filename);
	}

	/*--------------------
	 * definitions
	 --------------------*/

	public SuperInitializer GetDefaultInitializer() {
		// return new NpointsInitializer();
		// return new BinaryCodeInitializer();
		return new BinaryInitializer();
	}

	/*--------------------
	 * Abstract method definition
	 --------------------*/

	/** MAIN FUNCTION */
	public int ApplyLocalFunction(OneRegisterIntCell cell) {
		int neighbIndex = 0;

		// left neighb. (index 0) has the most significant bit
		for (int i = 0; i < SNEIGHB; i++) {
			neighbIndex = neighbIndex | cell.ReadNeighbourState(SNEIGHB - i - 1) << i;
		}
		double probatoOne = m_transitionProba[neighbIndex];
		return this.RandomEventDouble(probatoOne) ? 1 : 0;
	}

	/*--------------------
	 * GFX
	 --------------------*/
	@Override
	public FLPanel GetSpecificPanel() {
		FLPanel container = new FLPanel();
		container.Add(new LoadRuleButton(), new SetRuleButton(), this.GetRandomizer().GetSeedControl());
		return container;
	}
	
	/*--------------------
	 * Get / Set
	 --------------------*/

	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TLR2;
	}

	public void printTable() {
		for (int i = 0; i < SZ; i++) {
			String binary = MathMacro.Int2BinaryString(i, SNEIGHB);
			Macro.fPrint("transition:%s proba:%.3f", binary, m_transitionProba[i]);
		}
	}

	/** button to load a rule from a file **/
	class LoadRuleButton extends FLActionButton {

		private static final String LOADRULE = "LoadRule";

		public LoadRuleButton() {
			super(LOADRULE);
		}

		@Override
		public void DoAction() {
			JFileChooser fc = new JFileChooser();
			File workingDirectory = new File(System.getProperty("user.dir"));
			fc.setCurrentDirectory(workingDirectory);
			
			int returnValue = fc.showOpenDialog(this);
			if(returnValue==JFileChooser.APPROVE_OPTION) {
				File file = fc.getSelectedFile();
				loadRuleFromFile( file.getName() );
				
			}
		}
	}
	
	/** loads a rule from a file **/
	public void loadRuleFromFile(String filename) {
		Macro.fPrint("Loading rule from file:%s ...", filename);
		FLStringList filetransitions= FLStringList.OpenFile(filename);
		for (int i=0; i<SZ; i++) {
			String line=filetransitions.Get(i);
			double val= FLString.ParseDouble(line);
			if (val<0.) 
				val=0;
			if (val>1.)
				val=1;
			Macro.fPrint("read transition i:%d val:%.3f input:%s", i, val, line);
			m_transitionProba[i]= val; //no inversion
		}
	}

	
	class ProbaSetter extends MultiDoubleController {
		@Override
		protected double ReadValArg(int pos) {
			return m_transitionProba[pos];
		}

		@Override
		protected void SetValArg(int pos, double val) {
			m_transitionProba[pos]= val;
			//updateTableDisplay();
		}

		@Override
		protected String[] getParamLabels() {
			String [] tab= new String[SZ];
			for (int i=0; i<SZ; i++) {
				tab[i]="p"+i;
			}
			return tab;
		}
	}
	
	/* more controls in a separate window */
	class SetRuleButton extends FLActionButton {

		private static final String LABEL_RULESETTINGS = "Set";

		//boolean m_default;
		public SetRuleButton() {
			super(LABEL_RULESETTINGS);
			setIcon(IconManager.getUIIcon("configMin"));
			defineAsPanelButtonStylized(FLColor.c_lightred);
		}

		@Override
		public void DoAction() {
			ProbaSetter ruleControl = new ProbaSetter();
			//ruleControl.setAlignmentX(CENTER_ALIGNMENT);
			FLFrame frame = new FLFrame("rule setting");
			frame.addPanel(ruleControl);
			frame.packAndShow();
		}
	}

}
