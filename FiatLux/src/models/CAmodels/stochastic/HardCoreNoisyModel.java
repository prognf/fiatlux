package models.CAmodels.stochastic;

import components.allCells.OneRegisterIntCell;
import components.types.ProbaPar;
import grafix.gfxTypes.elements.FLPanel;
import initializers.SuperInitializer;
import initializers.CAinit.BinaryInitializer;
import main.MathMacro;
import models.CAmodels.ClassicalModel;
import topology.basics.TopologyCoder;

/*--------------------
 * choose one neighbour at random and copy its state
 *--------------------*/

public class HardCoreNoisyModel extends ClassicalModel {

	public static final String NAME = "HardCoreNoisy";

	ProbaPar mp_lambda= new ProbaPar("lambda", .1);
	ProbaPar mp_alpha= new ProbaPar("alpha", .1);
	ProbaPar mp_beta= new ProbaPar("beta", .1);
	
	
	/*--------------------
	 * overrides & implementations
	 --------------------*/
	
	public final int ApplyLocalFunction(OneRegisterIntCell cell) {
		//int o= cell.GetState();
		double		lambda= mp_lambda.GetVal();
		int n1= cell.CountOccurenceOf(1);
		if (n1==0){
			double alpha= mp_alpha.GetVal();
			return Barycentre(alpha,lambda);
		} else { //o=1
			double beta= mp_beta.GetVal();
			double w= MathMacro.Power(beta, n1);
			return Barycentre(1.,lambda*w);
		}
		
		
	}
	
	/*
	public final int PrimalApplyLocalFunction(OneRegisterIntCell cell) {

		int o= cell.GetState();
		int n1= cell.CountOccurenceOf(1);
		if (o==0){
			if (n1==0){
				return RandomEventDouble(mp_b)?1:0;
			} else {
				return 0;
			}
		} else { //o=1
			if (n1==0){ // 1 should survive
				return RandomEventDouble(mp_s)?0:1;
			} else   // 1 violates constraint
				return 0;
		}
	}*/

	private int Barycentre(double weight0, double weight1) {
		double pone= weight1/ (weight0 + weight1);	
		return RandomEventDouble(pone)?1:0;
	}

	public SuperInitializer GetDefaultInitializer() {
		return new BinaryInitializer();
	}

	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TV4;
	}

	public FLPanel GetSpecificPanel(){
		//FLPanel p= FLPanel.NewPanel(mp_b.GetControl(), mp_s.GetControl());
		mp_alpha.GetControl().setOpaque(false);
		mp_beta.GetControl().setOpaque(false);
		mp_lambda.GetControl().setOpaque(false);
		mp_alpha.GetControl().minimize();
		mp_beta.GetControl().minimize();
		mp_lambda.GetControl().minimize();
		FLPanel p= FLPanel.NewPanel(mp_alpha, mp_beta, mp_lambda);
		return FLPanel.NewPanel(p, this.GetRandomizer());
	}
	
	/*--------------------
	 * Attributes
	 --------------------*/


	/*--------------------
	 * Constructor
	 --------------------*/
	public HardCoreNoisyModel() {
		super();
	}

	/*--------------------
	 *  Get & Set methods
	 --------------------*/


	//
	/*public PaintToolKit GetPalette() {
			//return PaintToolKit.GetRandomPalette(NCOLOR);
		//return PaintToolKit.GetRandomPaletteSafeWeb(NCOLOR, this.GetRandomizer());
	}*/



	/*--------------------
	 *  Other methods
	 --------------------*/
}
