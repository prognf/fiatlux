package models.CAmodels.stochastic;

import components.allCells.OneRegisterIntCell;
import components.types.BooleanPar;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.elements.FLPanel;
import initializers.SuperInitializer;
import models.CAmodels.ClassicalModel;
import topology.basics.TopologyCoder;

/*--------------------
 * choose one neighbour at random and copy its state
 *--------------------*/

public class RetinaModel extends ClassicalModel {

	public enum colorMode { RND, LELORRAIN, CAUCHIE, FROMFILE }

    private static final colorMode COLORMODE = colorMode.FROMFILE;

	public static final String NAME = "Retina";


	private static final boolean DEFMODE = true;
	private static final String PALETTEFILENAME = "palette.txt", TXTCYCLE="cycle?";

	private static final int DEFCYCLELEN = 3;

	/*--------------------
	 * Attributes
	 --------------------*/


	BooleanPar m_PhaseSynchMode= new BooleanPar(TXTCYCLE, true); 
	public colorMode m_colorMode= colorMode.LELORRAIN; //colorMode.RND;// colorMode.FROMFILE;
	private int m_states= DEFCYCLELEN;
	
	//special for reading palette in file
	private PaintToolKit m_paletteFromFile;


	/*--------------------
	 * Constructor
	 --------------------*/
	public RetinaModel() {
		super();
	}

	private RetinaModel(String filename){
		this();
		m_paletteFromFile= PaintToolKit.LoadPalette(filename);
		m_states= m_paletteFromFile.GetSize();
	}
	
	/** special Sultra & B */
	public static RetinaModel NewModelFromFile(){
		RetinaModel model= new RetinaModel(PALETTEFILENAME);
		return model;
	}
	
	/*--------------------
	 * overrides & implementations
	 --------------------*/


	public final int ApplyLocalFunction(OneRegisterIntCell in_Cell) {

		int neighbSize= in_Cell.GetNeighbourhoodSize();
		int selected = RandomInt(neighbSize);
		int newstate= in_Cell.ReadNeighbourState(selected);
		if (m_PhaseSynchMode.GetVal()){
			newstate = (newstate + 1) % m_states;
		}
		return newstate;
	}

	public SuperInitializer GetDefaultInitializer() {
		return new RetinaInit(this);
	}

	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TLR1;
	}


	/*--------------------
	 *  Get & Set methods
	 --------------------*/
	public int GetStatesNum() {
		return m_states;
	}

	@Override
	public PaintToolKit GetPalette() {
		//Macro.Debug (" color code is " + COLORMODE);
		switch (m_colorMode){
		case RND:
			return PaintToolKit.GetRandomPaletteSafeWeb(DEFCYCLELEN, this.GetRandomizer());
		case LELORRAIN:
			return PaintToolKit.GetLeLorrainPalette(DEFCYCLELEN);
		case CAUCHIE:
			return SimonHantai.GetAlainChauchiePalette();
		case FROMFILE:
			return m_paletteFromFile;
		default:
			return null; 
		}
	}

	public FLPanel GetSpecificPanel(){
		return FLPanel.NewPanel(m_PhaseSynchMode.GetControl(), this.GetRandomizer().GetSeedControl());
	}



	/*--------------------
	 *  Other methods
	 --------------------*/
}
