package models.CAmodels.stochastic;

import components.allCells.OneRegisterIntCell;
import grafix.gfxTypes.controllers.DoubleController;
import grafix.gfxTypes.elements.FLPanel;
import models.CAmodels.binary.BinaryModel;
import topology.basics.TopologyCoder;
import updatingScheme.FullyAsynchronousScheme;
import updatingScheme.UpdatingScheme;

/*--------------------
 * Ising Model...
 * 
*--------------------*/
public class IsingModel extends BinaryModel  {

	final static String NAME = "Ising";
	
	private static final String S_LABEL = "Temperature";
	final static int ZERO=0, ONE=1;
	final static double DEF_TEMP=1;
	
	
	public IsingModel() {
		super();
	}

	@Override
	public UpdatingScheme GetDefaultUpdatingScheme() {
		return new FullyAsynchronousScheme();
	}
	
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_FM8;
	}
	
	
	public final int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
		//double r= Macro.RandomEventDouble(m_NoiseRate);
		int n1 = in_Cell.CountOccurenceOf(ONE);
		int n0 = in_Cell.CountOccurenceOf(ZERO);
		int state = in_Cell.GetState();
		int deltaE= (2 * state - 1) * (n1 - n0 );
		if ( deltaE < 0 ){
			return 1 - state ;  // flip
		} else {
			double proba= Math.exp(-deltaE / m_NoiseRate /*T*/ );
			if (RandomEventDouble(proba)){
				return 1 - state ;  // flip
			} else {
				return state;
			}
		}
	}
	
	

	/*--------------------
	 *	 Noise Control
	**********************************************************/
	
	double m_NoiseRate = DEF_TEMP;
	class RateControl extends DoubleController {
		

		public RateControl() {
			super(S_LABEL);			
		}

		@Override
		public double ReadDouble() {
			return m_NoiseRate;
		}

		@Override
		public void SetDouble(double val) {
			m_NoiseRate= val;
		}
	}
	
	
	
	public FLPanel GetSpecificPanel() {
		return new RateControl();
	}
	
	
}
