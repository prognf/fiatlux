package models.CAmodels.stochastic;

import initializers.CAinit.OneRegisterInitializer;

public class RandomCopyInit extends OneRegisterInitializer {

	@Override
	protected void SubInit() {
		for (int i = 0; i < GetLsize(); i++) {
			int colnum= RandomInt(RandomCopyModel.NCOLOR);
			InitState(i, colnum);
		}
	}

}
