package models.CAmodels.stochastic;

import components.allCells.OneRegisterIntCell;
import components.types.RuleCode;
import grafix.gfxTypes.controllers.DoubleController;
import grafix.gfxTypes.elements.FLPanel;
import main.tables.PlotterSelectControl;
import models.CAmodels.binary.BinaryModel;
import models.CAmodels.tabled.ECAcontrol;
import models.CAmodels.tabled.ECAlookUpTable;
import topology.basics.TopologyCoder;

/*--------------------
 * ECA model : this part extends the abstract part 
 **--------------------*/
public class DiploidECAmodel extends BinaryModel  {

	public final static String NAME="DiploidECA";

	private static final String TXT_WEIGHTRULE2 = "Weight(R2)";
	private static final String TXTECA1 = "ECA1", TXTECA2="ECA2";

	private static final int DEFRULE1=0, DEFRULE2 = 51;
	private static final double DEF_PROBA = 0.25;

	/*--------------------
	 * attributes
	 --------------------*/

	ECAlookUpTable m_LookUpTable1, m_LookUpTable2;
	double m_proba;

	/*--------------------
	 * Constructor
	 --------------------*/

	public DiploidECAmodel(RuleCode Wcode1, RuleCode Wcode2) {
		m_proba= DEF_PROBA;
		m_LookUpTable1=  new ECAlookUpTable(Wcode1);
		m_LookUpTable2=  new ECAlookUpTable(Wcode2);
	}

	/*--------------------
	 * external control
	 --------------------*/

	public DiploidECAmodel() {
		this(RuleCode.FromInt(DEFRULE1),RuleCode.FromInt(DEFRULE2));
	}

	/*--------------------
	 * Abstract method definition
	 --------------------*/

	/* MAIN FUNCTION */
	public int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
		int NeighbourhoodByte = 
				in_Cell.ReadNeighbourState(2)
				| in_Cell.ReadNeighbourState(1) << 1
				| in_Cell.ReadNeighbourState(0) << 2;
		boolean Choice= RandomEventDouble(m_proba);
		return Choice?
				m_LookUpTable2.getTransitionResult(NeighbourhoodByte):
					m_LookUpTable1.getTransitionResult(NeighbourhoodByte);
	}

	/*--------------------
	 * definitions
	 --------------------*/

	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TLR1;
	}

	@Override
	public FLPanel GetSpecificPanel() {
		ECAcontrol c1= new ECAcontrol(m_LookUpTable1, TXTECA1);
		ECAcontrol c2= new ECAcontrol(m_LookUpTable2, TXTECA2);
		c1.setOpaque(false);
		c2.setOpaque(false);
		ProbaControl lambda= new ProbaControl();
		FLPanel p = FLPanel.NewPanel(c1, c2, lambda);
		p.Add(this.GetRandomizer().GetSeedControl());
		return  p;
	}

	@Override
	public String [] GetPlotterSelection1D() {
		return PlotterSelectControl.ECAselect;
	}  

	/*--------------------
	 * Get / Set
	 --------------------*/

	public void SetWeightRule2(double val) {
		m_proba= val;
	}

	/*--------------------
	 * inner classes
	 --------------------*/

	class ProbaControl extends DoubleController {
		public ProbaControl() {
			super(TXT_WEIGHTRULE2);
		}
		@Override
		protected double ReadDouble() {
			return m_proba;
		}
		@Override
		protected void SetDouble(double val) {
			SetWeightRule2(val);			
		}
	}

	/*--------------------
	 * testing
	 --------------------*/
	public static void DoTest() {		

	}

}
