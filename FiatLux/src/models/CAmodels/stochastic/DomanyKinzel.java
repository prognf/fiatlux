package models.CAmodels.stochastic;

import components.allCells.OneRegisterIntCell;
import components.types.DoublePar;
import grafix.gfxTypes.elements.FLPanel;
import main.Macro;
import models.CAmodels.binary.BinaryModel;

/*--------------------
 * Model: Majority Model Description: classical majority without noise
 * 
*--------------------*/
public class DomanyKinzel extends BinaryModel {

	public static final String NAME = "DomanyKinzel";
	final static int ZERO=0, ONE=1;
	DoublePar 
	m_p1= new DoublePar("p1:", 0.1),
	m_p2= new DoublePar("p2:", 0.1);


	public DomanyKinzel() {
		super();
	}

	public final int ApplyLocalFunction(OneRegisterIntCell inCell){
		int 
		x = inCell.ReadNeighbourState(0),
		y = inCell.ReadNeighbourState(2);
		int sum = x+y;
		switch (sum){
		case 0:
			return ZERO;
		case 1:
			double p1= m_p1.GetVal();
			return RandomEventDouble( p1 )?ONE:ZERO;
		case 2:
			double p2= m_p1.GetVal();
			return RandomEventDouble( p2 )?ONE:ZERO;
		default:
			Macro.FatalError(" problem with state sum !");
			return Macro.ERRORint;
		}

	}

	@Override
	public FLPanel GetSpecificPanel(){
		FLPanel p = FLPanel.NewPanel();
		p.Add(m_p1.GetControl(), m_p2.GetControl(), this.GetRandomizer().GetSeedControl());
		return p;
	}

}

