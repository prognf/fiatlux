package models.CAmodels.stochastic;

import java.io.File;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JFileChooser;

import components.allCells.OneRegisterIntCell;
import components.types.FLString;
import components.types.FLStringList;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.IconManager;
import grafix.gfxTypes.controllers.MultiDoubleController;
import grafix.gfxTypes.elements.FLActionButton;
import grafix.gfxTypes.elements.FLFrame;
import grafix.gfxTypes.elements.FLLabel;
import grafix.gfxTypes.elements.FLPanel;
import grafix.gfxTypes.elements.FLTextArea;
import grafix.gfxTypes.elements.FLTextField;
import initializers.SuperInitializer;
import initializers.CAinit.BinaryInitializer;
import main.Macro;
import main.MathMacro;
import models.CAmodels.binary.BinaryModel;
import topology.basics.TopologyCoder;

/*--------------------
 * a binary model with a look-up table
 *--------------------*/
public class ProbabilisticLineR1Model extends BinaryModel {

	public final static String NAME = "StochosLineR1";
	private static final int SNEIGHB = 3, SZ = 8;

	
	final double[] m_transitionProba;
	FLTextArea m_ruleDisplay;
	private static final int CODE_INPUT_LEN = 20;// for entering the probas
	private static final String TEXT_RULEPROB_DISPLAY = "tr. prob.: ";
	
	/*--------------------
	 * Constructor
	 --------------------*/

	public ProbabilisticLineR1Model() {
		m_transitionProba = new double[SZ];
		// 0,1 quiescent
		m_transitionProba[SZ - 1] = 1.;
		for (int tr = 1; tr < SZ - 1; tr++) { // init random rule
			m_transitionProba[tr] = 0;
			String sbin = MathMacro.Int2BinaryString(tr, SZ);
			int nones = FLString.countOccurences('1', sbin);
			m_transitionProba[tr] = nones / (double) SNEIGHB;
			//m_transitionProba[tr]= RandomDouble();
		}
		//printTable();
	}

	/** construct from file **/
	public ProbabilisticLineR1Model(String filename) {
		this();
		loadRuleFromFile(filename);
	}

	/*--------------------
	 * definitions
	 --------------------*/

	public SuperInitializer GetDefaultInitializer() {
		return new BinaryInitializer();
	}

	/*--------------------
	 * Abstract method definition
	 --------------------*/

	/** MAIN FUNCTION */
	public int ApplyLocalFunction(OneRegisterIntCell cell) {
		int neighbIndex = 0;
		// left neighb. (index 0) has the most significant bit
		for (int i = 0; i < SNEIGHB; i++) {
			neighbIndex = neighbIndex | cell.ReadNeighbourState(SNEIGHB - i - 1) << i;
		}
		double probaToOne = m_transitionProba[neighbIndex];
		return this.RandomBernoulli(probaToOne);
	}

	/*--------------------
	 * GFX
	 --------------------*/
	@Override
	public FLPanel GetSpecificPanel() {
		FLPanel containerA = new FLPanel();
		containerA.Add(new SetRuleButton(), new LoadRuleButton(), new FLLabel("in:"), new ParseSECALR1code());
		containerA.Add(this.GetRandomizer().GetSeedControl());		
		m_ruleDisplay= new FLTextArea(32, FLColor.c_lightyellow);
		m_ruleDisplay.SetFontMid();
		updateTableDisplay();
		FLPanel container= 
				FLPanel.NewPanelVertical(containerA, m_ruleDisplay);
		return container;
	}

	/*--------------------
	 * Get / Set
	 --------------------*/

	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TLR1;
	}

	public void printTable() {
		for (int i = 0; i < SZ; i++) {
			String binary = MathMacro.Int2BinaryString(i, SNEIGHB);
			Macro.fPrint("transition:%s proba:%.3f", binary, m_transitionProba[i]);
		}
	}

	private void updateTableDisplay() {
		String s=TEXT_RULEPROB_DISPLAY;
		DecimalFormat df = 
				new DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
		df.setMaximumFractionDigits(5); 
		for (int i = 0; i < SZ; i++) {
			s+=df.format(m_transitionProba[i])+" ";
		}
		m_ruleDisplay.SetText(s);
	}
	
	/** button to load a rule from a file **/
	class LoadRuleButton extends FLActionButton {

		private static final String LOADRULE = "LoadRule";

		public LoadRuleButton() {
			super(LOADRULE);
		}

		@Override
		public void DoAction() {
			JFileChooser fc = new JFileChooser();
			File workingDirectory = new File(System.getProperty("user.dir"));
			fc.setCurrentDirectory(workingDirectory);

			int returnValue = fc.showOpenDialog(this);
			if(returnValue==JFileChooser.APPROVE_OPTION) {
				File file = fc.getSelectedFile();
				loadRuleFromFile( file.getName() );

			}
		updateTableDisplay();	
		}
	}

	/** loads a rule from a file **/
	public void loadRuleFromFile(String filename) {
		Macro.fPrint("Loading rule from file:%s ...", filename);
		FLStringList filetransitions= FLStringList.OpenFile(filename);
		for (int i=0; i<SZ; i++) {
			String line=filetransitions.Get(i);
			double val= FLString.ParseDouble(line);
			if (val<0.) 
				val=0;
			if (val>1.)
				val=1;
			Macro.fPrint("i:%d val : %.3f str: %s", i, val, line);
			m_transitionProba[i]= val; // no inversion
		}
	}


	class ParseSECALR1code extends FLTextField {

		protected ParseSECALR1code() {
			super(CODE_INPUT_LEN);
		}

		@Override
		public void parseInput(String strin) {
			Macro.fPrint(" parsing : %s", strin);
			String PATT="(\\d\\.?[\\d]*)[^\\d]*";
			String PATT8= FLString.NcopiesOfStr(SZ, PATT);
			Pattern p = Pattern.compile(PATT8);
			Matcher m = p.matcher(strin);
			if (m.find()){
				try {
					//step A. parsing
					double [] phi= new double[SZ];
					for (int i=0; i<SZ;i++) {
						String sreal= m.group(i+1);
						phi[i]=FLString.ParseDouble(sreal);
						//Macro.fPrint(" parsed:%s %.4f", sreal, dval);
					}
					// step B. copy, to avoid partial assignment of values in case of failure
					for (int i=0; i<SZ;i++) {
						m_transitionProba[i]=phi[i]; // copy
					}
					Macro.fPrint(" parsed:%s", MathMacro.arraydoubleToString(phi, ";"));
					updateTableDisplay();
				} catch (Exception e) {
					Macro.fPrint("failed parsing; matcher :%s",PATT8);
				}
			} else {
				Macro.fPrint(" could not parse expr:%s", strin);
				Macro.fPrint("PARSER:%s", p.toString());
			}
		}
		
	}

	
	class ProbaSetter extends MultiDoubleController {
		@Override
		protected double ReadValArg(int pos) {
			return m_transitionProba[pos];
		}

		@Override
		protected void SetValArg(int pos, double val) {
			m_transitionProba[pos]= val;
			updateTableDisplay();
		}

		@Override
		protected String[] getParamLabels() {
			String [] tab= new String[SZ];
			for (int i=0; i<SZ; i++) {
				tab[i]="p"+i;
			}
			return tab;
		}
	}

	
	/* more controls in a separate window */
	class SetRuleButton extends FLActionButton {

		private static final String LABEL_RULESETTINGS = "Set";

		//boolean m_default;
		public SetRuleButton() {
			super(LABEL_RULESETTINGS);
			setIcon(IconManager.getUIIcon("configMin"));
			defineAsPanelButtonStylized(FLColor.c_lightred);
		}

		@Override
		public void DoAction() {
			ProbaSetter ruleControl = new ProbaSetter();
			//ruleControl.setAlignmentX(CENTER_ALIGNMENT);
			FLFrame frame = new FLFrame("rule setting");
			frame.addPanel(ruleControl);
			frame.packAndShow();
		}
	}

	
	
}//class
