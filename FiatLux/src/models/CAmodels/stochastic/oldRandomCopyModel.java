package models.CAmodels.stochastic;

import components.allCells.Cell;
import components.allCells.ClassicalCACell;
import components.allCells.OneRegisterIntCell;
import grafix.gfxTypes.PaintToolKit;
import initializers.SuperInitializer;
import models.CAmodels.ClassicalModel;
import topology.basics.TopologyCoder;

/*--------------------
 * choose one neighbour at random and copy its state
 *--------------------*/

public class oldRandomCopyModel extends ClassicalModel {

	final static int NCOLOR=10;
	public static final String NAME = "RandomCopyQQ";

	/*--------------------
	 * overrides & implementations
	 --------------------*/
	

	public final int ApplyLocalFunction(OneRegisterIntCell in_Cell) {

		int neighbSize= in_Cell.GetNeighbourhoodSize();
		int selected = RandomInt(neighbSize);
		int newstate= in_Cell.ReadNeighbourState(selected);
		return newstate;
	}

	public SuperInitializer GetDefaultInitializer() {
		return new RandomCopyInit();
	}

	public Cell GetNewCell() {
		return new ClassicalCACell(this);
	}

	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TLR1;
	}

	/*--------------------
	 * Attributes
	 --------------------*/
	/*--------------------
	 * Constructor
	 --------------------*/
	public oldRandomCopyModel() {
		super();
	}

	/*--------------------
	 *  Get & Set methods
	 --------------------*/


	@Override
	public PaintToolKit GetPalette() {
			//return PaintToolKit.GetRandomPalette(NCOLOR);
		return PaintToolKit.GetRandomPaletteSafeWeb(NCOLOR, this.GetRandomizer());
	}



	/*--------------------
	 *  Other methods
	 --------------------*/
}
