package models.CAmodels.tests;

import components.allCells.OneRegisterIntCell;
import models.CAmodels.binary.BinaryModel;
import models.CAmodels.simplified.BoundConditions;
import models.CAmodels.simplified.Topologies;

public class BootstrapModel extends BinaryModel{
	public static final String NAME = "Bootstrap";

	public String GetDefaultAssociatedTopology() {
		return BoundConditions.Toric + Topologies.vonNeumann4;
	}

	@Override
	public int ApplyLocalFunction(OneRegisterIntCell cell) {
		int st = cell.GetState();
		if(st==1)
		return 1;
		int n1= cell.CountOccurenceOf(1);
		return n1>1?1:0;
	}

	
}
