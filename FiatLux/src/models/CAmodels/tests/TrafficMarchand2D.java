package models.CAmodels.tests;

import components.allCells.OneRegisterIntCell;
import initializers.SuperInitializer;
import initializers.CAinit.BinaryInitializer;
import models.CAmodels.ClassicalModel;
import models.CAmodels.simplified.BoundConditions;
import models.CAmodels.simplified.Topologies;

public class TrafficMarchand2D extends ClassicalModel{
	public static final String NAME = "TrafficM";

	final static int E=2, NE=1, N=0, W=6, SW=5, S=4, NW=7, SE=3; // Moore8 TOPO
	final static int ZERO = 0, ONE = 1;

	public String GetDefaultAssociatedTopology() {
		return BoundConditions.Toric + Topologies.Moore8;
	}

	@Override
	public int ApplyLocalFunction(OneRegisterIntCell c) {
		//return moveNfirstNEsecond(c);
		return moveNfirstEsecond(c);
		
	}

	private int moveNfirstNEsecond(OneRegisterIntCell c) {
		int st = c.GetState();
		if (st == ZERO){
			return ( (NST(c,S) == ONE ) || 
					((NST(c,W) == ONE) && (NST(c,SW) == ONE)) ) ? ONE : ZERO;
		} else { // 1
			return ( (NST(c,N) == ZERO ) || 
					((NST(c,E) == ZERO) && (NST(c,NE) == ZERO)) ) ? ZERO : ONE;		
		}
	}
	
	private int moveNfirstEsecond(OneRegisterIntCell c) {
		int st = c.GetState();
		if (st == ZERO){
			return ( (NST(c,S) == ONE ) || 
					((NST(c,W) == ONE) && (NST(c,NW) == ONE)) ) ? ONE : ZERO;
		} else { // 1
			return ( (NST(c,N) == ZERO ) || 
					((NST(c,E) == ZERO) && (NST(c,SE) == ZERO)) ) ? ZERO : ONE;		
		}
	}

	static final private int NST(OneRegisterIntCell c, int neighbPos) {
		return c.ReadNeighbourState(neighbPos);
	}
	
	@Override
	public SuperInitializer GetDefaultInitializer() {
		BinaryInitializer init = new BinaryInitializer();
		return init;
	}
	


	
	
}
