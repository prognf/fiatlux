package models.CAmodels.tests;

import components.arrays.RegularDynArray;
import components.types.IntC;
import grafix.viewers.MultiRegisterViewer;

public class LifeWithoutDeathMultiRegisterViewer extends MultiRegisterViewer {

	public LifeWithoutDeathMultiRegisterViewer(IntC in_CellSize, IntC in_GridSize, RegularDynArray in_Automaton) {
		super(in_CellSize, in_GridSize, in_Automaton);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int GetCellColorNumXY(int x, int y) {
		if(GetStateAXY(x, y) == 0) {
			return 0;
		} else {
			int time = GetStateBXY(x, y);
			return (time < 256) ? time+1 : 256;
		}
	}

}
