package models.CAmodels.tests;

import components.allCells.OneRegisterIntCell;
import components.arrays.RegularDynArray;
import components.types.IntC;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.viewers.AutomatonViewer;
import initializers.SuperInitializer;
import initializers.CAinit.BinaryInitializer;
import models.CAmodels.ClassicalModel;
import models.CAmodels.simplified.BoundConditions;
import models.CAmodels.simplified.Topologies;
import topology.basics.PlanarTopology;
import topology.basics.SuperTopology;

public class LifeWithoutDeathModel extends ClassicalModel{
	public static final String NAME = "LifeWithoutDeath";

	public String GetDefaultAssociatedTopology() {
		return BoundConditions.Toric + Topologies.Moore8;
	}

	@Override
	public int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
		int st = in_Cell.GetState();
		if(st % 2 == 0) {
			int aliveSum = 0;
			for(int state : in_Cell.GetNeighbourhoodState()) {
				if(state % 2 != 0) {
					aliveSum++;
				}
			}
			if(aliveSum == 3 || aliveSum == 8) {
				return st + 1;
			}
			return st + 2;
		} else {
			return st;
		}
	}

	@Override
	public SuperInitializer GetDefaultInitializer() {
		BinaryInitializer init = new BinaryInitializer();
		return init;
	}
	
	/*--------------------
	 * GFX
	 --------------------*/
	/** overrides */
	@Override
	public PaintToolKit GetPalette() {
		PaintToolKit palette= PaintToolKit.GetRainbow(256);
		palette.SetColor(0, FLColor.c_black);
		return palette;
	}
	
	@Override
	/** specific viewer */
	public AutomatonViewer GetPlaneAutomatonViewer(
			IntC cellPixSize, 
			SuperTopology in_TopologyManager, RegularDynArray in_Automaton) {	
		 PlanarTopology topo2D = (PlanarTopology) in_TopologyManager;
		AutomatonViewer av= new LifeWithoutDeathViewer(cellPixSize, topo2D.GetXYsize(), in_Automaton);
		av.SetPalette(GetPalette());
		return av;
	}
}
