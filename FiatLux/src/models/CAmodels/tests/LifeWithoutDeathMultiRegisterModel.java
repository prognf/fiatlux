package models.CAmodels.tests;

import components.allCells.ClassicalMultiRegisterCell;
import components.arrays.RegularDynArray;
import components.types.IntC;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.viewers.AutomatonViewer;
import initializers.SuperInitializer;
import models.CAmodels.ClassicalMultiRegisterModel;
import topology.basics.PlanarTopology;
import topology.basics.SuperTopology;
import topology.basics.TopologyCoder;

public class LifeWithoutDeathMultiRegisterModel extends ClassicalMultiRegisterModel{

	private static final int NLAYERS = 2;
	
	public final static String NAME= "LifeWithoutDeathMultiRegister";
	
	@Override
	public int GetnLayers() {return NLAYERS;}

	@Override
	public int[] GetTransitionResult(ClassicalMultiRegisterCell in_Cell) {
		int[] nextStates = new int[2];
		int nSize = in_Cell.GetNeighbourhoodSize();
		int count = 0;
		for(int i = 0; i < nSize; i++) {
			if(in_Cell.GetNeighbourStateI(0, i) == 1) {
				count++;
			}
		}
		if(in_Cell.GetStateI(0) == 1) {
			if(count == 0) {
				nextStates[0] = 0;
				nextStates[1] = 0;
			} else {
				nextStates[0] = 1;
				nextStates[1] = in_Cell.GetStateI(1)+1;
			}
		} else {
			if(count == 3 || count == 8) {
				nextStates[0] = 1;
				nextStates[1] = 0;
			}
		}
		return nextStates;
	}

	@Override
	public SuperInitializer GetDefaultInitializer() {
		LifeWithoutDeathMultiRegisterInitializer init = new LifeWithoutDeathMultiRegisterInitializer();
		return init;
	}
	
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_FM8;
	}
	
	@Override
	public PaintToolKit GetPalette() {
		PaintToolKit palette= PaintToolKit.GetRainbow(256);
		palette.SetColor(0, FLColor.c_black);
		return palette;
	}
	
	@Override
	/** specific viewer */
	public AutomatonViewer GetPlaneAutomatonViewer(
			IntC cellPixSize, 
			SuperTopology in_TopologyManager, RegularDynArray in_Automaton) {	
		 PlanarTopology topo2D = (PlanarTopology) in_TopologyManager;
		AutomatonViewer av= new LifeWithoutDeathMultiRegisterViewer(cellPixSize, topo2D.GetXYsize(), in_Automaton);
		av.SetPalette(GetPalette());
		return av;
	}

}
