package models.CAmodels.tests;

import components.allCells.OneRegisterIntCell;
import initializers.SuperInitializer;
import initializers.CAinit.BinaryInitializer;
import models.CAmodels.ClassicalModel;
import models.CAmodels.simplified.BoundConditions;
import models.CAmodels.simplified.Topologies;

public class ParityProblemModel extends ClassicalModel{
	public static final String NAME = "ParityPr";

	public String GetDefaultAssociatedTopology() {
		return BoundConditions.Toric + Topologies.LineR2;
	}

	@Override
	public int ApplyLocalFunction(OneRegisterIntCell cell) {
		int 
		a= cell.ReadNeighbourState(0), b= cell.ReadNeighbourState(1), 
		c= cell.ReadNeighbourState(2), d=cell.ReadNeighbourState(3), e= cell.ReadNeighbourState(4);
		boolean A1=  (b==1) && (c==0) && (d==0);// && (e==1) ;
		boolean A2=  (a==1) && (b==0) && (c==0);// && (d==1) ;
		boolean B1=  (b==0) && (c==1) && (d==0) && (e==1) ;
		boolean B2=  (a==0) && (b==1) && (c==0) && (d==1) ;
		boolean C1=  (b==1) && (c==0) && (d==1) && (e==0) ;
		boolean C2=  (a==1) && (b==0) && (c==1) && (d==0) ;

		
		if (A1)
			return 1;
		if (A2)
			return 1;
		if (B1)
			return 0;
		if (B2)
			return 1;	
		if (C1)
			return 1;
		if (C2)
			return 0;
		return c;
	}

	@Override
	public SuperInitializer GetDefaultInitializer() {
		BinaryInitializer init = new BinaryInitializer();
		return init;
	}
	


	
	
}
