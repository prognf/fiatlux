package models.CAmodels.tests;

import components.types.IntC;
import initializers.CAinit.MultiRegisterPlaneInitializer;

public class LifeWithoutDeathMultiRegisterInitializer extends MultiRegisterPlaneInitializer {
	
		private IntC m_XYsize;
		private double m_ProbaToOne = 0.02;
		private static final byte NSTATES = 2, StateInit = 1;

	@Override
	protected void SubInit() {
		Uniform();		
	}

	private void Uniform() {
		for (int x = 0; x < GetXsize(); x++) {
			for(int y = 0; y < GetYsize(); y++) {
				int st= RandomEventDouble(m_ProbaToOne)?StateInit:0; 
				SetCellStateXYregister(x, y, 0, st);
				SetCellStateXYregister(x, y, 1, 0);
			}
		}
	}
}
