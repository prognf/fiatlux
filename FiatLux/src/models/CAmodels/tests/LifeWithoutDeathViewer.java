package models.CAmodels.tests;

import components.allCells.OneRegisterIntCell;
import components.arrays.RegularDynArray;
import components.types.IntC;
import grafix.viewers.OneRegisterAutomatonViewer;

public class LifeWithoutDeathViewer extends OneRegisterAutomatonViewer{

	public LifeWithoutDeathViewer(IntC in_CellSize, IntC in_GridSize, RegularDynArray in_Automaton) {
		super(in_CellSize, in_GridSize, in_Automaton);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int GetCellColorNumXY(int x, int y) {
		OneRegisterIntCell cell = GetCell(x,y);
		int state = cell.GetState();
		if(state % 2 == 0) {
			return 0;
		} else {
			int time = state/2;
			return (time < 256) ? time+1 : 256;
		}
	}
}
