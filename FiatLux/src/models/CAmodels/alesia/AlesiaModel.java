package models.CAmodels.alesia;

import grafix.gfxTypes.FLColor;
import models.CAmodels.ClassicalModel;
import topology.basics.TopologyCoder;

abstract public class AlesiaModel extends ClassicalModel {

	public static final int W= 2;
	
	public AlesiaModel(){
		//GetRandomizer().SetSeedFixed(26);
	}
	
	// states
	public static final int 
			EMPTY=0, GAULOIS = 1, 
			FRONT=2, FRONT_STAR=3, 
			ROMAN=4, 
			GAULOIS_S=5, 
			GAULOIS_F=6 ;
	

	public static final FLColor[] palette = 
		{FLColor.c_white, 
		FLColor.c_blue, // A 
		FLColor.c_brown, FLColor.c_orange, // F, FS 
		FLColor.c_green, //B
		FLColor.c_grey2, // R
		FLColor.c_black, // V
		FLColor.c_yellow}; // VS    

	//public static final int Gamewidth = 2; // W

	final public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TLR1;			
	}	



}
