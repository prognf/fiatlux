package models.CAmodels.alesia;

import components.types.IntPar;
import grafix.gfxTypes.elements.FLPanel;
import initializers.CAinit.OneRegisterInitializer;

public class GateCAinitializer extends OneRegisterInitializer implements AlesiaSettable {
	private static final String TXTA = "tA", TXTF="f", TXTB="tB";
	private static final int DEFVALA = 10, DEFVALF=4, DEFVALB=8;

	int W= AlesiaModel.W;
	IntPar m_pA, m_pF, m_pB;
	//final AlesiaModel m_model;
	public enum INITMODE { ONLYONE }
    //final INITMODE m_initmode;


	public GateCAinitializer() {
		//m_initmode= ini;
		//m_model= alesiaModel;
		m_pA= new IntPar(TXTA, DEFVALA);
		m_pF= new IntPar(TXTF, DEFVALF);
		m_pB= new IntPar(TXTB, DEFVALB);

	}


	@Override
	protected void SubInit() {
		int 
		pA= m_pA.GetVal(), 
		pB= m_pB.GetVal(), 
		pf= m_pF.GetVal();
		InitInterval(0,GetLsize(),0);
		InitInterval(0, pA,  AlesiaModelQiqiHouhou.A);
		InitInterval(pA, pA+pf, AlesiaModelQiqiHouhou.F);
		InitInterval(pA+pf, pA+pf+pB, AlesiaModelQiqiHouhou.B);		
	}

	/** puts cells of index in [a;b( in state val **/
	private void InitInterval(int a, int b, int val) {
		for (int pos=a; pos <b;pos++){
			InitState(pos, val);
		}		
	}

	@Override
	public FLPanel GetSpecificPanel() {
		FLPanel panel = FLPanel.NewPanelVertical(m_pA.GetControl(), m_pF.GetControl(), m_pB.GetControl());
		panel.setOpaque(false);
		return panel;
	}

	/** A,F,B cells is the number of A,F & B cells in the init. cond. **/
	public void SetInput(int Acells, int Fcells, int Bcells) {
		//Macro.fDebug("pA:%d fr:%d pB:%d ", Acells, Fcells, Bcells);
		m_pA.SetVal(Acells);
		m_pF.SetVal(Fcells);  
		m_pB.SetVal(Bcells);		
	}


}
