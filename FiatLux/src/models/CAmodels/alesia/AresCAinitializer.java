package models.CAmodels.alesia;

import grafix.gfxTypes.elements.FLPanel;
import initializers.CAinit.OneRegisterInitializer;

public class AresCAinitializer extends OneRegisterInitializer
implements AlesiaSettable {
	int m_pA, m_pB, m_front;
	
	public AresCAinitializer(){
		super();	
	}

	public void SetState(int pA, int front, int pB){
		m_pA= pA;
		m_front= front;
		m_pB= pB;
	}

	@Override
	protected void SubInit() {
		InitInterval(0,GetLsize(),0);
		InitInterval(0,m_pA,AlesiaModel.GAULOIS);
		InitInterval(0,m_pA,AlesiaModel.GAULOIS);
		InitInterval(m_pA,m_pA+m_front,AlesiaModel.FRONT);
		InitInterval(m_pA+m_front,m_pA+m_front+m_pB,AlesiaModel.ROMAN);		
	}

	private void InitInterval(int a, int b, int val) {
		for (int pos=a; pos <b;pos++){
			InitState(pos, val);
		}		
	}

	@Override
	public FLPanel GetSpecificPanel() {
		return null;
	}

	public void SetInput(int myVal, int front, int opponentVal) {
		//Macro.Debug("front : " + front);
		m_pA= myVal - 1;
		m_front= front;
		m_pB= opponentVal - 1;		
	}
}
