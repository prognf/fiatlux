package models.CAmodels.alesia;

import components.allCells.OneRegisterIntCell;
import grafix.gfxTypes.controllers.DoubleController;
import grafix.gfxTypes.elements.FLPanel;
import models.CAmodels.binary.BinaryModel;

/*--------------------
 * synchronisation problem
 * @author Nazim Fates 
 *****--------------------*/
public class AlesiaBinomialCAmodel extends BinaryModel {


	public static final String NAME = "FirstAlesia";
	public static final String TXT = "rho";
	private static final double DEF_PVAL = 0.99;
	/*--------------------
	 * Attributes
	 --------------------*/

	double m_pDamping = DEF_PVAL;

	/*--------------------
	 * overrides & implementations
	 --------------------*/


	/** MAIN FUNCTION  **/
	final public int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
		//Macro.Debug("grn");
		int state= in_Cell.GetState();
		if (state==1){
			boolean keep = RandomEventDouble(m_pDamping);
			return keep ? 1 : 0;
		} else
			return state;
	}

	/* overloading this method */
	public FLPanel GetSpecificPanel() {
		return new pControl();
	}
	/*--------------------
	 * Constructor
	 --------------------*/

	public AlesiaBinomialCAmodel() {
		
	}

	/*--------------------
	 * Get / set
	 --------------------*/

	public void SetDamping(double in_val){
		m_pDamping= in_val;
	}


	/*--------------------
	 * GFX
	 --------------------*/

	class pControl extends DoubleController{

		public pControl() {
			super(TXT);
		}

		@Override
		protected double ReadDouble() {
			return m_pDamping;
		}

		@Override
		protected void SetDouble(double val) {
			SetDamping(val);		
		}

	}

}
