package models.CAmodels.alesia;

import components.allCells.OneRegisterIntCell;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.controllers.DoubleController;
import grafix.gfxTypes.elements.FLPanel;
import initializers.SuperInitializer;
import main.Macro;

/*  ACRI - 2018 study */
public class AlesiaModelQiqiHouhou extends AlesiaModel {

	final public static int E=0, A=1, F=2, FS=3, B=4, R=5, V=6, VS=7;

	// empty constructor for GFX menu selection
	public AlesiaModelQiqiHouhou() {
		super();
	}

	public static final String NAME = "AlesiaQ2H2";
	public static final String TXTPVOL = "pVol", TXTPTRANS="pTrans", TXTPABS="pAbs";
	private static final double DEF_PV = 0.28, DEF_PT=.35, DEF_PA=0.;

	double m_pVolontary = DEF_PV;
	double m_pTransmit = DEF_PT;
	double m_pAbsorb= DEF_PA;


	/** MAIN FUNCTION  **/
	final public int ApplyLocalFunction(OneRegisterIntCell cell) {
		return RenePlus(cell);
		//return firstAttemptWithOpponentFiltered(cell);
	}


	private int RenePlus(OneRegisterIntCell cell) {
		int state= cell.GetState();
		int right= cell.ReadNeighbourState(2);
		switch (state){
		case A:
			return RandomEventDouble(m_pVolontary)?V:R;
		case F:
		case FS:
			boolean toStar=((right==B) && RandomEventDouble(m_pTransmit)) || 
						   ((right==FS) && (!RandomEventDouble(m_pAbsorb))  );
			return toStar?FS:F;		
		case B:
			return (right==E)?E:B;
		case R:
			return ( (right==FS)||(right==VS) )?V:R; 
		case V:
		case VS:
			return ( (right==FS)||(right==VS) )?VS:V;
		//case VS:
		case E:
			return E;
		}
		// default case
		return Macro.ERRORint;
	}

	//------------------------------
	// overrides & implementations
	//-s----------------------------/

	/** overriding */
	public final PaintToolKit GetPalette() {
		PaintToolKit p = new PaintToolKit(palette);
		return p;
	}

	@Override
	public SuperInitializer GetDefaultInitializer() { 
		GateCAinitializer init= new GateCAinitializer();
		return init;
	}

	
	/** controls of the model **/
	public FLPanel GetSpecificPanel() {
		return FLPanel.NewPanel( 
				new pVolControl(), new pTransControl(), new pAbsControl() );
	}

	/*--------------------
	 * Get / set
	 --------------------*/
	public void setpVpTpA(double pV,double pT, double pA) {
		m_pVolontary= pV;
		m_pTransmit= pT;
		m_pAbsorb= pA;
	}

	/*--------------------
	 * GFX
	 --------------------*/
	class pVolControl extends DoubleController{
		public pVolControl() {
			super(TXTPVOL);
		}
		@Override
		protected double ReadDouble() {
			return m_pVolontary;
		}
		@Override
		protected void SetDouble(double val) {
			m_pVolontary= val;		
		}
	}
	class pTransControl extends DoubleController{
		public pTransControl() {
			super(TXTPTRANS);
		}
		@Override
		protected double ReadDouble() {
			return m_pTransmit;
		}
		@Override
		protected void SetDouble(double val) {
			m_pTransmit= val;		
		}
	}
	class pAbsControl extends DoubleController{
		public pAbsControl() {
			super(TXTPABS);
		}
		@Override
		protected double ReadDouble() {
			return m_pAbsorb;
		}
		@Override
		protected void SetDouble(double val) {
			m_pAbsorb= val;		
		}
	}
}
