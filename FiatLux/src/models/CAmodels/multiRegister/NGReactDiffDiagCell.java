package models.CAmodels.multiRegister;

import components.allCells.SuperCell;
import components.allCells.TwoRegisterCell;
import components.arrays.GenericCellArray;

public class NGReactDiffDiagCell extends TwoRegisterCell {
    private NGReactDiffDiagModel model;
    private GenericCellArray<NGReactDiffDiagCell> neighbourhood;

    public NGReactDiffDiagCell(NGReactDiffDiagModel m) {
        super();
        model = m;
    }

    public int CountOccurenceOf(int state) {
        int count = 0;

        for (NGReactDiffDiagCell n : neighbourhood) {
            if (n.GetStateOne() == state) {
                count++;
            }
        }

        return count;
    }

    public boolean hasUnstableNeighbours() {
        int limit = (int) (NGReactDiffDiagModel.NB_STABILISATION_STEPS * 0.67);

        if (GetStateTwo() < limit && GetStateOne() != NGReactDiffDiagModel.ALERT_A) {
            return true;
        }

        for (NGReactDiffDiagCell cell : neighbourhood) {
            if (cell.GetStateTwo() < limit && cell.GetStateOne() != NGReactDiffDiagModel.ALERT_A) {
                return true;
            }
        }

        return  false;
    }

    @Override
    public void SetNeighbourhood(GenericCellArray<SuperCell> neighblist) {
        neighbourhood = new GenericCellArray<>(neighblist);
    }

    @Override
    public int GetNeighbourhoodSize() {
        return neighbourhood.GetSize();
    }

    @Override
    public NGReactDiffDiagCell GetNeighbour(int in_NeighbourPosition) {
        return neighbourhood.get(in_NeighbourPosition);
    }

    @Override
    public void sig_UpdateBuffer() {
        int[] res = model.GetTransitionResult(this);

        SetBufferOne(res[0]);
        SetBufferTwo(res[1]);
    }

    @Override
    public void sig_MakeTransition() {
        m_StateOne = m_bufferOne;
        m_StateTwo = m_bufferTwo;
    }
}
