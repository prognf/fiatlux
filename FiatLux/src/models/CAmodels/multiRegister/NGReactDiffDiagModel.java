package models.CAmodels.multiRegister;

import components.allCells.Cell;
import components.arrays.RegularDynArray;
import components.types.IntC;
import components.types.ProbaPar;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.RegularAutomatonViewer;
import initializers.SuperInitializer;
import models.CAmodels.CellularModel;
import topology.basics.PlanarTopology;
import topology.basics.SuperTopology;
import topology.basics.TopologyCoder;

/**
 * @author Nicolas Gauville
 */
public class NGReactDiffDiagModel extends CellularModel {
    final public static String NAME = "ReacDiff-Diag";

    public static final int NB_STABILISATION_STEPS = 50;

    public static final int NORMAL = 0;
    public static final int EXCITED = 1;
    public static final int REFRACTORY = 2;
    public static final int FAILURE = 3;
    public static final int ALERT_A = 4;
    public static final int ALERT_B = 5;

    private ProbaPar m_pFailure;
    private ProbaPar m_pRepair;
    private ProbaPar m_pTr;

    private boolean enable_b_failure = true;

    public NGReactDiffDiagModel() {
        super();

        m_pFailure = new ProbaPar("pFailure", 0.0);
        m_pRepair = new ProbaPar("pRepair", 0.0);
        m_pTr = new ProbaPar("pTr", 0.02);
    }

    public void setProbabilities(double failure, double repair, double pTr) {
        m_pFailure.SetVal(failure);
        m_pRepair.SetVal(repair);
        m_pTr.SetVal(pTr);
    }

    public int[] GetTransitionResult(NGReactDiffDiagCell in_Cell) {
        int[] res = new int[2];

        int state = in_Cell.GetStateOne();
        int newState = state;

        if (state != FAILURE && RandomEventDouble(m_pFailure)) {
            newState = FAILURE;
        } else if (state == FAILURE) {
            if (RandomEventDouble(m_pRepair)) {
                newState = getRandomColor();
            } else {
                newState = FAILURE;
            }
        } else {
            if (in_Cell.CountOccurenceOf(ALERT_B) > 0) {
                newState = ALERT_B;
            } else if (in_Cell.CountOccurenceOf(ALERT_A) > 0 && !enable_b_failure) {
                newState = ALERT_A;
            } else if (state == ALERT_A && enable_b_failure) {
                if (in_Cell.CountOccurenceOf(ALERT_A) >= 4) {
                    newState = ALERT_B;
                } else if (in_Cell.CountOccurenceOf(EXCITED) > 0 && RandomEventDouble(m_pTr)) {
                    newState = EXCITED;
                }
            } else if (state == NORMAL) {
                if (in_Cell.CountOccurenceOf(EXCITED) > 0 && RandomEventDouble(m_pTr)) {
                    newState = EXCITED;
                }
            } else if (state == EXCITED) {
                newState = REFRACTORY;
            } else if (newState == REFRACTORY) {
                newState = NORMAL;
            }
        }

        res[0] = newState;

        if (newState == FAILURE || newState == ALERT_A || newState == ALERT_B) {
            res[1] = NB_STABILISATION_STEPS;
        } else {
            res[1] = (state == newState ? Math.min(NB_STABILISATION_STEPS, in_Cell.GetStateTwo() + 1) : 0);

            if (res[1] == NB_STABILISATION_STEPS) {
                res[0] = ALERT_A;
                res[1] = NB_STABILISATION_STEPS;
            }
        }

        return res;
    }

    @Override
    public Cell GetNewCell(){
        return new NGReactDiffDiagCell(this);
    }

    @Override
    public RegularAutomatonViewer GetPlaneAutomatonViewer(IntC cellPixSize, SuperTopology topology, RegularDynArray automaton) {
        PlanarTopology topo2D = (PlanarTopology) topology;
        NG3StatesViewer av = new NG3StatesViewer(cellPixSize, topo2D.GetXYsize(), automaton, NB_STABILISATION_STEPS, FAILURE, ALERT_A, ALERT_B);
        av.SetPalette(GetPalette());

        return av;
    }

    @Override
    public PaintToolKit GetPalette() {
        PaintToolKit palette = PaintToolKit.EmptyPalette();

        double
                NOP=0, MAX = 1, Q13=  MAX / 3.0, Q14=  MAX / 4.0,
                Q23= 2.0*MAX/3.0, Q34= 3.0*MAX/4.0, Q45= 4.0*MAX/5.0, Q56= 5.0*MAX/6.0, Q67 = 6.0* MAX / 7.0;

        addGradient(palette, 1, 1, 1, NB_STABILISATION_STEPS);
        addGradient(palette, MAX, NOP, NOP, NB_STABILISATION_STEPS);
        addGradient(palette, MAX, Q23, Q13, NB_STABILISATION_STEPS);

        palette.AddColorRGB(0, 0, 0); // Q DEF
        palette.AddColorRGB(200, 200, 0); // ALERT A
        palette.AddColorRGB(200, 0, 200); // ALERT B
        return palette;
    }

    private int getRandomColor() {
        return RandomEventDouble(1. / 3.) ? NORMAL : (RandomEventDouble(0.5) ? EXCITED : REFRACTORY);
    }

    private void addGradient(PaintToolKit palette, double r, double g, double b, int steps) {
        for (int i = 0; i < steps; i++) {
            int coeff = (int) (((double) i / (double) steps) * 200.0);
            palette.AddColorRGB((int) (r * 200 + (1 - r) * coeff), (int) (g * 200 + (1 - g) * coeff), (int) (b * 200 + (1 - b) * coeff));
        }
    }

    @Override
    public SuperInitializer GetDefaultInitializer() {
        return new NG3StateInitializer(NORMAL, EXCITED, REFRACTORY);
    }

    @Override
    public String GetDefaultAssociatedTopology() {
        return TopologyCoder.s_TM8;
    }

    @Override
    public FLPanel GetSpecificPanel() {
        FLPanel p = FLPanel.NewPanel(m_pFailure.GetControl(), m_pRepair.GetControl(), m_pTr.GetControl());
        p.setOpaque(false);
        return p;
    }
}
