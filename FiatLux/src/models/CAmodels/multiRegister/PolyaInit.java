package models.CAmodels.multiRegister;

import grafix.gfxTypes.elements.FLPanel;
import initializers.CAinit.MultiRegisterInitializer;

class PolyaInit extends MultiRegisterInitializer {
	

	/*--------------------
	 * abstract methods definition
	 --------------------*/
	public void SubInit() {
		Init();
	}

	private void Init() {
		int size = GetLsize();
		for (int cell = 0; cell < size; cell++) {
			SetCellStateI(cell, 0, 1);
			SetCellStateI(cell, 1, 2);
		}
	}
	public FLPanel GetSpecificPanel() {
		FLPanel p = new FLPanel();
		return p;
	}

	
	/*--------------------
	 * Other methods
	 --------------------*/

}// end class
