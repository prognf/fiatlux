package models.CAmodels.multiRegister;

import components.allCells.SuperCell;
import components.allCells.TwoRegisterCell;
import components.arrays.GenericCellArray;

public class NGReactDiffClassifCell extends TwoRegisterCell {
    private NGReactDiffClassifModel model;
    private GenericCellArray<NGReactDiffClassifCell> neighbourhood;

    public NGReactDiffClassifCell(NGReactDiffClassifModel m) {
        super();
        model = m;
    }

    public int CountOccurenceOf(int state) {
        int count = 0;

        for (NGReactDiffClassifCell n : neighbourhood) {
            if (n.GetStateOne() == state) {
                count++;
            }
        }

        return count;
    }

    public int CountOccurenceOfSameCounter() {
        int count = 0;
        int my = GetStateTwo();

        for (NGReactDiffClassifCell n : neighbourhood) {
            if (n.GetStateTwo() == my) {
                count++;
            }
        }

        return count;
    }

    public int GetMostFrequentStateTwo() {
        // ArrayList<StateCount> list = new ArrayList<StateCount>();
        return 0;
    }

    @Override
    public void SetNeighbourhood(GenericCellArray<SuperCell> neighblist) {
        neighbourhood = new GenericCellArray<>(neighblist);
    }

    @Override
    public int GetNeighbourhoodSize() {
        return neighbourhood.GetSize();
    }

    @Override
    public NGReactDiffClassifCell GetNeighbour(int in_NeighbourPosition) {
        return neighbourhood.get(in_NeighbourPosition);
    }

    @Override
    public void sig_UpdateBuffer() {
        int[] res = model.GetTransitionResult(this);

        SetBufferOne(res[0]);
        SetBufferTwo(res[1]);
    }

    @Override
    public void sig_MakeTransition() {
        m_StateOne = m_bufferOne;
        m_StateTwo = m_bufferTwo;
    }

    class StateCount {
        private int state;
        private int nb;

        public StateCount(int s) {
            state = s;
            nb = 1;
        }

        public void add() {
            nb++;
        }

        public int getState() {
            return state;
        }

        public int getNb() {
            return nb;
        }
    }
}
