package models.CAmodels.multiRegister;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import components.arrays.RegularDynArray;
import components.types.IntC;
import grafix.gfxTypes.elements.FLList;
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.TwoRegisterViewer;

public class NGBinaryViewer extends TwoRegisterViewer {
    private FLList list;
    private int nbStabilisationSteps;
    private int failure;
    private int alertA;
    private int alertB;

    public NGBinaryViewer(IntC in_CellSize, IntC in_GridSize, RegularDynArray automaton) {
        super(in_CellSize, in_GridSize, automaton);
        nbStabilisationSteps = NGMichaosDiagModel.NB_STABILISATION_STEPS;
        failure = NGMichaosDiagModel.FAILURE;
        alertA = NGMichaosDiagModel.ALERT_A;
        alertB = NGMichaosDiagModel.ALERT_B;
    }

    public NGBinaryViewer(IntC in_CellSize, IntC in_GridSize, RegularDynArray automaton, int nb, int fail, int aA, int aB) {
        super(in_CellSize, in_GridSize, automaton);
        nbStabilisationSteps = nb;
        failure = fail;
        alertA = aA;
        alertB = aB;
    }

    @Override
    public int GetCellColorNumXY(int x, int y) {
        int s1 = GetStateOneXY(x, y);
        int s2 = GetStateTwoXY(x, y);
        int mode = list.GetSelectedItemRank();

        if (s1 == failure) {
            return nbStabilisationSteps * 2;
        } else if (s1 == alertA) {
            return nbStabilisationSteps * 2 + 1;
        } else if (s1 == alertB) {
            return nbStabilisationSteps * 2 + 2;
        }


        if (mode == 1) {
            return s1 * nbStabilisationSteps;
        } else if (mode == 2) {
            return s2;
        }

        return Math.min(nbStabilisationSteps * 2 + 1, s1 * nbStabilisationSteps + Math.min(s2, nbStabilisationSteps - 1));
    }

    public FLPanel GetSpecificPanel() {
        FLPanel p = new FLPanel();
        String[] names = {"Composition", "Color", "Stability"};
        list = new FLList(names, new Listener(this));
        p.Add(list);

        return	p;
    }

    class Listener implements ListSelectionListener {
        private NGBinaryViewer viewer;

        Listener(NGBinaryViewer v) {
            viewer = v;
        }

        @Override
        public void valueChanged(ListSelectionEvent e) {
            viewer.UpdateView();
        }
    }
}
