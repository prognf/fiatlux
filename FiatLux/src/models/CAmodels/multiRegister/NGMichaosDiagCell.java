package models.CAmodels.multiRegister;

import components.allCells.SuperCell;
import components.allCells.TwoRegisterCell;
import components.arrays.GenericCellArray;
import models.CAmodels.CellularModel;
import models.CAmodels.typing.ITransitionResultAwareModel;

public class NGMichaosDiagCell extends TwoRegisterCell {
    private CellularModel model;
    private GenericCellArray<NGMichaosDiagCell> neighbourhood;

    public NGMichaosDiagCell(CellularModel m) {
        super();

        model = m;
    }

    public int CountOccurenceOf(int state) {
        int count = 0;

        for (NGMichaosDiagCell n : neighbourhood) {
            if (n.GetStateOne() == state) {
                count++;
            }
        }

        return count;
    }

    public boolean hasUnstableNeighbours() {
        int limit = (int) (NGMichaosDiagModel.NB_STABILISATION_STEPS * 0.67);

        if (GetStateTwo() < limit && GetStateOne() != NGMichaosDiagModel.ALERT_A) {
            return true;
        }

        for (NGMichaosDiagCell cell : neighbourhood) {
            if (cell.GetStateTwo() < limit && cell.GetStateOne() != NGMichaosDiagModel.ALERT_A) {
                return true;
            }
        }

        return  false;
    }

    @Override
    public void SetNeighbourhood(GenericCellArray<SuperCell> neighblist) {
        neighbourhood = new GenericCellArray<>(neighblist);
    }

    @Override
    public int GetNeighbourhoodSize() {
        return neighbourhood.GetSize();
    }

    @Override
    public NGMichaosDiagCell GetNeighbour(int in_NeighbourPosition) {
        return neighbourhood.get(in_NeighbourPosition);
    }

    @Override
    public void sig_UpdateBuffer() {
        int[] res = ((ITransitionResultAwareModel) model).GetTransitionResult(this);

        SetBufferOne(res[0]);
        SetBufferTwo(res[1]);
    }

    @Override
    public void sig_MakeTransition() {
        m_StateOne = m_bufferOne;
        m_StateTwo = m_bufferTwo;
    }
}
