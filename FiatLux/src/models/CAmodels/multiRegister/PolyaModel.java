package models.CAmodels.multiRegister;

import components.allCells.Cell;
import components.allCells.ClassicalMultiRegisterCell;
import components.allCells.MultiRegisterCell;
import components.arrays.RegularDynArray;
import components.types.IntC;
import components.types.ProbaPar;
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.LineAutomatonViewer;
import initializers.SuperInitializer;
import models.CAmodels.ClassicalMultiRegisterModel;
import topology.basics.LinearTopology;
import topology.basics.TopologyCoder;

/*--------------------
 * ???
*--------------------*/
public class PolyaModel extends ClassicalMultiRegisterModel {

	private static final int N_LAYERS = 2;
	final public static String NAME="PolyaModel";
	private static final double DEF_alpha = 0.8;
	private ProbaPar m_alpha;

	/*--------------------
	 * Constructor
	 --------------------*/
	public PolyaModel() {
			m_alpha= new ProbaPar("alpha", DEF_alpha);
	}
	
	/*--------------------
	 * overrides & implementations
	 --------------------*/

	@Override
	public int GetnLayers() {
		return N_LAYERS;
	}

	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TLR1;
	}

	public SuperInitializer GetDefaultInitializer() {
		return new PolyaInit();
	}

	public Cell GetNewCell() {
		return new ClassicalMultiRegisterCell(this);
	}

	public FLPanel GetSpecificPanel(){
		return FLPanel.NewPanel(m_alpha.GetControl(), this.GetRandomizer());
	}
	
	/*--------------------
	 * constants
	 --------------------*/

	/*--------------------
	 * Attributes
	 --------------------*/ 



	@Override
	public int[] GetTransitionResult(ClassicalMultiRegisterCell cell) {
		MultiRegisterCell 
		cellL= cell.GetNeighbour(0),
		cellR= cell.GetNeighbour(2);

		int s= cell.GetStateI(0);
		int n= cell.GetStateI(1);
		int sL= cellL.GetStateI(0);
		int nL= cellL.GetStateI(1);
		int sR= cellR.GetStateI(0);
		int nR= cellR.GetStateI(1);

		double probaC= s / (double)n;
		double probaL= sL / (double)nL;
		double probaR= sR / (double)nR;
		
		double alpha= m_alpha.GetVal();
		double peff= (1. -  alpha) * probaC + (alpha) * (1. - probaR) ;
		//double peff= (1-  alpha) * probaC + (alpha/2.) * (2- probaL - probaR) ; 
		boolean red = RandomEventDouble(peff);
		int [] result= new int[2];
		result[0]=red?s+1:s;
		result[1]= n+1;
		return result;
	}

	/*--------------------
	 * Get & Set methods
	 --------------------*/


	/** override
	 * 1D Viewer : called by simulation sampler */
	public LineAutomatonViewer GetLineAutomatonViewer(IntC SquareDim, 
		LinearTopology topo1D, RegularDynArray automaton, int Tsize) {
		LineAutomatonViewer av = 
				new PolyaViewer(SquareDim, topo1D, automaton, Tsize);
		//av.SetPalette( GetPalette() );
		return av;
	}

	/*--------------------
	 * Other methods
	 --------------------*/

	
}
