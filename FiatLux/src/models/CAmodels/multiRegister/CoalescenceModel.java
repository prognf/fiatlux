package models.CAmodels.multiRegister;

import components.allCells.ClassicalMultiRegisterCell;
import components.arrays.RegularDynArray;
import components.types.IntC;
import components.types.RuleCode;
/*-------------------------------------------------------------------------------
- to study the coalescence phenomena
------------------------------------------------------------------------------*/
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.LineAutomatonViewer;
import initializers.SuperInitializer;
import main.Macro;
import models.CAmodels.ClassicalMultiRegisterModel;
import models.CAmodels.tabled.ECAmodel;
import topology.basics.LinearTopology;
import topology.basics.TopologyCoder;


public class CoalescenceModel extends ClassicalMultiRegisterModel {

	private static final int NLAYERS = 2;
	
	public final static String NAME= "coalescentECA";
	
	
	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();
	private static final RuleCode WDEFAULT = new RuleCode(62);
	//--------------------------------------------------------------------------
	//- attributes
	//--------------------------------------------------------------------------

	ECAmodel m_model;

	//--------------------------------------------------------------------------
	//- construction
	//--------------------------------------------------------------------------

	public CoalescenceModel(){
		this(WDEFAULT);
	}
	
	public CoalescenceModel(RuleCode W){
		m_model = ECAmodel.NewRuleDefault();
		m_model.SetRule(W);
	}
	
	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------


	@Override
	public int GetnLayers() {
		return NLAYERS;
	}

	@Override
	public int[] GetTransitionResult(ClassicalMultiRegisterCell cell) {
		int 
		valLayer0= m_model.ApplyLocalFunctionForCoalescence(cell, 0),
		valLayer1= m_model.ApplyLocalFunctionForCoalescence(cell, 1);
		int[] tabVal= {valLayer0, valLayer1};
		return tabVal;
	}

	@Override
	public SuperInitializer GetDefaultInitializer() {
		return new CoalescenceInitializer();
	}

	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------

	public FLPanel GetSpecificPanel(){
		return FLPanel.NewPanel(m_model.GetSpecificPanel());
	}
	
	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------

	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TLR1;
	}

	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------

	/** override
	 * 1D Viewer : called by simulation sampler */
	public LineAutomatonViewer GetLineAutomatonViewer(IntC SquareDim, 
		LinearTopology topo1D, RegularDynArray automaton, int Tsize) {
		LineAutomatonViewer av = 
				new CoalescenceViewer(SquareDim, topo1D, automaton, Tsize);
		//av.SetPalette( GetPalette() );
		return av;
	}


}
