package models.CAmodels.multiRegister;

import components.allCells.Cell;
import components.allCells.ClassicalMultiRegisterCell;
import components.arrays.RegularDynArray;
import components.types.IntC;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.viewers.RegularAutomatonViewer;
import initializers.SuperInitializer;
import main.Macro;
import models.CAmodels.ClassicalMultiRegisterModel;
import topology.basics.PlanarTopology;
import topology.basics.SuperTopology;
import topology.basics.TopologyCoder;

/*--------------------
 * ???
*--------------------*/
public class DictyoTCAmodel extends ClassicalMultiRegisterModel {

	private static final int N_LAYERS = 3;
	final static int r_STATE= 0, r_DIR=1, r_CHEM=2; // register for cells
	final public static String NAME="Dictyo-TCA";
	public static final int NEIGHBSIZE = 4, MIDNEIGHBSIZE = 2;

	public static final int MAXCHEMICAL=5;

	/*--------------------
	 * overrides & implementations
	 --------------------*/

	@Override
	public int GetnLayers() {
		return N_LAYERS;
	}

	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TV4;
	}

	public SuperInitializer GetDefaultInitializer() {
		return new DictyoTCAinit();
	}

	public Cell GetNewCell() {
		return new ClassicalMultiRegisterCell(this);
	}

	public final PaintToolKit GetPalette(){
		return new PaintToolKit(PALETTE);
	}

	/* overrides
	 * 2D Viewer : called by simulation sampler  */
	public RegularAutomatonViewer GetPlaneAutomatonViewer(IntC in_SquareDim, 
			SuperTopology in_TopologyManager, RegularDynArray in_Automaton) {
		PlanarTopology topo2D= (PlanarTopology)in_TopologyManager;
		// default selection
		DictyoTCAViewer av = new DictyoTCAViewer(in_SquareDim, topo2D.GetXYsize(), in_Automaton);
		av.SetPalette( GetPalette() );
		return av;
	}

	/*--------------------
	 * constants
	 --------------------*/

	final public static int
	E0  =  0,	E1  =  1,	E2  =  2,
	A1  =  3,
	R0= 4 , R1=5, 
	P2=6,
	ST0 = 7,  ST1 = 8,
	OB= 9, AG= 10 ;

	final public static FLColor [] PALETTE = 
	{	FLColor.c_white, FLColor.c_white, FLColor.c_white, //E2
		FLColor.c_purple, //A1
		FLColor.c_red,   FLColor.c_green, // R0, R1
		FLColor.c_blue,  // P2
		FLColor.c_darkblue, FLColor.c_blue2, // ST0, ST1
		FLColor.c_blue,//obstacle
		FLColor.c_orange  //aggregate
	};
	private static final int MOVEPROBA = 100;
	private static final double PROBAEMIT = 0.01;

	/*--------------------
	 * Attributes
	 --------------------*/ 

	int [] result= new int [N_LAYERS];


	/*--------------------
	 * Constructor
	 --------------------*/
	public DictyoTCAmodel() {

	}

	/*--------------------
	 * Get & Set methods
	 --------------------*/


	private boolean GoodReception(ClassicalMultiRegisterCell in_Cell, int neighb) {
		int neighbstate= in_Cell.GetNeighbourStateI(r_STATE, neighb);
		int dirneighb= in_Cell.GetNeighbourStateI(r_DIR, neighb);
		return (neighbstate == R0) && (dirneighb == ((neighb) % NEIGHBSIZE));
	}

	/* used with perfect cells */
	@Override
	final public int[] GetTransitionResult(ClassicalMultiRegisterCell in_Cell) {
		int oldstate = in_Cell.GetStateI(r_STATE);
		int newstate = -1;
		int newdir = in_Cell.GetStateI(r_DIR);
		switch(oldstate) {
		case E0: {
			int count=0;
			for(int i=0; i<NEIGHBSIZE; i++) { // all directions
				// DEBUG Macro.Debug("|" + in_Cell.GetBufferI(r_DIR));
				if (GoodReception(in_Cell,i)) {
					count++;
					newdir = i;
				}
			}
			if (count==1){
				newstate = A1;
				newdir = (newdir + MIDNEIGHBSIZE) % NEIGHBSIZE;
			} else {
				newstate = E1; //accept or conflict
			}

		} break;
		case E1: 
			newstate = E2;break;
		case A1:
			newstate = P2; break;
		case E2: 
			newstate = E0; break;
		case R0: 
			newstate = R1; break;
		case R1: 
			if (TransactionOccurs(in_Cell)){ 
				newstate = E2; break; 
			} else {  
				newstate = P2; break;
			}
		case P2:			
			if (in_Cell.GetRegisterOccurenceOf(r_STATE, AG)>0){
				//newstate = R0; break;  
				newstate = AG; break; // aggregation process !
			} else {
				newdir = GetNewDir(in_Cell);
				if (RandomEventInt(MOVEPROBA) && (newdir >= 0)){ 
					newstate = R0; break;	// MOVE
				} else{
					newstate = ST0; break;  // STAY
				}				  				
			}
		case ST0: newstate = ST1; break;
		case ST1: newstate = P2; break;
		case OB: newstate = R0; break; 
		case AG: newstate = AG; break;
		default: 
			Macro.FatalError(this,"state doesn't exist "+oldstate);
		}

		// we keep old val
		int chemical = in_Cell.GetStateI(r_CHEM);
		switch (oldstate){
		case OB: 
		case AG:
			chemical = 0; break;
		case P2:
			chemical = GetNewChemical(in_Cell, true); break;
		case E2:
			// we update val
			chemical = GetNewChemical(in_Cell, false); break;
		}


		result= new int [] {newstate,newdir,chemical}; 
		return result;
	}

	int [] m_ExcitedNieghbs= new int[NEIGHBSIZE]; 

	/* TODO OPTIM : note that the excited cells are detected twice !*/
	private int GetNewDir(ClassicalMultiRegisterCell in_Cell) {
		int size= in_Cell.GetNeighbourhoodSize();
		int selected_dir = -1;//RandomInt(size);
		if (in_Cell.GetStateI(r_CHEM)== 0){ // if we are neutral
			int shiftdir = RandomInt(size);
			int idir=0;		
			boolean found = false;
			while ( !found & (idir < size)){			
				int newdir = (shiftdir + idir ) % NEIGHBSIZE;
				if (in_Cell.GetNeighbourStateI(r_CHEM, newdir)==MAXCHEMICAL){
					found=true;
					selected_dir= newdir;
				}
				idir++;			
			}
		}
		// we have to invert directions
		if (selected_dir>=0){
			selected_dir = (selected_dir + MIDNEIGHBSIZE) % NEIGHBSIZE;
		}
		return selected_dir; //RandomInt(NEIGHBSIZE-1);
	}

	final private int RuleForNeutral(ClassicalMultiRegisterCell in_Cell){
		return (in_Cell.GetRegisterOccurenceOf(r_CHEM, MAXCHEMICAL) >0)?MAXCHEMICAL:0;
	}

	/* simulation of the reaction-diffusion process */
	final private int GetNewChemical(
			ClassicalMultiRegisterCell in_Cell,
			boolean ConsiderEmission){
		int oldchemical = in_Cell.GetStateI(r_CHEM);
		if  (oldchemical == 0){
			if (ConsiderEmission && RandomEventDouble(PROBAEMIT)){
				return MAXCHEMICAL;
			} else{
				return RuleForNeutral(in_Cell); 
			}
		} else {
			return oldchemical - 1;
		}
	}

	private boolean TransactionOccurs(ClassicalMultiRegisterCell in_Cell) {
		int targetdir = (in_Cell.GetStateI(r_DIR) + MIDNEIGHBSIZE) % NEIGHBSIZE  ;
        int stateneighb= in_Cell.GetNeighbourStateI(r_STATE, targetdir);
		int dirneighb=  in_Cell.GetNeighbourStateI(r_DIR, targetdir);
		//Macro.DebugNOCR("*M*" + targetdir + ":"+ stateneighb + "," + dirneighb);
		return
		(stateneighb == A1 ) &
		(dirneighb == targetdir);
	}

	/*--------------------
	 * Other methods
	 --------------------*/

	
}
