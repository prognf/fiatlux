package models.CAmodels.multiRegister;

import components.arrays.RegularDynArray;
import components.types.IntC;
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.TwoRegisterViewer;

public class NG2RulesStatesViewer extends TwoRegisterViewer {
    public NG2RulesStatesViewer(IntC in_CellSize, IntC in_GridSize, RegularDynArray automaton) {
        super(in_CellSize, in_GridSize, automaton);
    }

    @Override
    public int GetCellColorNumXY(int x, int y) {
        return GetStateOneXY(x, y);
    }

    public FLPanel GetSpecificPanel() {
        return	new FLPanel();
    }
}
