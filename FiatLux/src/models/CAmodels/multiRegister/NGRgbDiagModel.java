package models.CAmodels.multiRegister;

import java.awt.Component;

import components.allCells.Cell;
import components.allCells.TwoRegisterCell;
import components.arrays.RegularDynArray;
import components.types.IntC;
import components.types.ProbaPar;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.controllers.DoubleController;
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.RegularAutomatonViewer;
import initializers.SuperInitializer;
import models.CAmodels.CellularModel;
import models.CAmodels.typing.ITransitionResultAwareModel;
import topology.basics.PlanarTopology;
import topology.basics.SuperTopology;
import topology.basics.TopologyCoder;

/**
 * @author Nicolas Gauville
 * Variante du Modèle RGB
 */
public class NGRgbDiagModel extends CellularModel implements ITransitionResultAwareModel {
    final public static String NAME = "RGB-Diag";

    public static final int NB_STABILISATION_STEPS = 100;

    public static final int R = 0;
    public static final int G = 1;
    public static final int B = 2;
    public static final int FAILURE = 3;
    public static final int ALERT_A = 4;
    public static final int ALERT_B = 5;

    private boolean enable_b_failure = true;

    private ProbaPar m_pFailure;
    private ProbaPar m_pRepair;
    private ProbaPar m_pApplyRule;
    private ProbaPar m_pTr;

    public NGRgbDiagModel() {
        super();

        m_pFailure = new ProbaPar("pFailure", 0.0);
        m_pRepair = new ProbaPar("pRepair", 0.0);
        m_pApplyRule = new ProbaPar("pApplyRule", 1.0);
        m_pTr = new ProbaPar("eps", 0.07); // 0.02
    }

    public void setEpsilon(double epsilon) {
        m_pTr.SetVal(epsilon);
    }

    public void setProbabilities(double failure, double repair, boolean enable_b, double pTr) {
        m_pFailure.SetVal(failure);
        m_pRepair.SetVal(repair);
        m_pTr.SetVal(pTr);
        enable_b_failure = enable_b;
    }

    public int[] GetTransitionResult(TwoRegisterCell cell) {
        NGRgbDiagCell in_Cell = (NGRgbDiagCell) cell;
        int[] res = new int[2];

        int state = in_Cell.GetStateOne();
        int newState = state;

        if (state != FAILURE && RandomEventDouble(m_pFailure)) {
            newState = FAILURE;
        } else if (state == FAILURE) {
            if (RandomEventDouble(m_pRepair)) {
                newState = getRandomColor();
            } else {
                newState = FAILURE;
            }
        } else {
            int nextA = 0;

            switch(state) {
                case R:
                    nextA = G;
                    break;
                case G:
                    nextA = B;
                    break;
                case B:
                    nextA = R;
                    break;
            }

            int countA = in_Cell.CountOccurenceOf(nextA);
            if (countA > 2 || (countA > 1 && RandomEventDouble(m_pTr))) {
                newState = nextA;
            }
            /*if (countA > 0) {
                newState = nextA;
            }*/
        }

        res[0] = newState;

        if (newState == FAILURE || newState == ALERT_A || newState == ALERT_B) {
            res[1] = NB_STABILISATION_STEPS;
        } else {
            res[1] = (state == newState ? Math.min(NB_STABILISATION_STEPS, in_Cell.GetStateTwo() + 1) : 0);

            if (in_Cell.CountOccurenceOf(ALERT_B) > 0) {
                res[0] = ALERT_B;
                res[1] = NB_STABILISATION_STEPS;
            } else if (res[1] == NB_STABILISATION_STEPS) {
                res[0] = ALERT_A;
                res[1] = NB_STABILISATION_STEPS;
            }
        }

        if (state == ALERT_A && enable_b_failure) {
            if (in_Cell.CountOccurenceOf(ALERT_A) < 4) {
            //    res[0] = getRandomColor();
            //    res[1] = 0;
            } else {
                res[0] = ALERT_B;
            }
        }

        return res;
    }

    @Override
    public Cell GetNewCell(){
        return new NGRgbDiagCell(this);
    }

    @Override
    public RegularAutomatonViewer GetPlaneAutomatonViewer(IntC cellPixSize, SuperTopology topology, RegularDynArray automaton) {
        PlanarTopology topo2D = (PlanarTopology) topology;
        NG3StatesViewer av = new NG3StatesViewer(cellPixSize, topo2D.GetXYsize(), automaton);
        av.SetPalette(GetPalette());

        return av;
    }

    @Override
    public PaintToolKit GetPalette() {
        PaintToolKit palette = PaintToolKit.EmptyPalette();

        addGradient(palette, 1, 0, 0, NB_STABILISATION_STEPS);
        addGradient(palette, 0, 1, 0, NB_STABILISATION_STEPS);
        addGradient(palette, 0, 0, 1, NB_STABILISATION_STEPS);

        palette.AddColorRGB(0, 0, 0); // Q DEF
        palette.AddColorRGB(200, 0, 200); // ALERT A
        palette.AddColorRGB(200, 0, 200); // ALERT B

        return palette;
    }

    private int getRandomColor() {
        return RandomEventDouble(1. / 3.) ? R : (RandomEventDouble(0.5) ? G : B);
    }

    private void addGradient(PaintToolKit palette, int r, int g, int b, int steps) {
        for (int i = 0; i < steps; i++) {
            int coeff = (int) (((double) i / (double) steps) * 200.0);
            palette.AddColorRGB(r * 200 + (1 - r) * coeff, g * 200 + (1 - g) * coeff, b * 200 + (1 - b) * coeff);
        }
    }

    @Override
    public SuperInitializer GetDefaultInitializer() {
        return new NG3StateInitializer(R, G, B, FAILURE);
    }

    @Override
    public String GetDefaultAssociatedTopology() {
        return TopologyCoder.s_TM8;
    }

    @Override
    public FLPanel GetSpecificPanel() {
        FLPanel p = FLPanel.NewPanel(m_pFailure.GetControl(), m_pRepair.GetControl(), m_pApplyRule.GetControl(), m_pTr.GetControl());
        for (Component c : p.getComponents()) {
            ((DoubleController) c).minimize();
        }
        p.setOpaque(false);
        return p;
    }
}
