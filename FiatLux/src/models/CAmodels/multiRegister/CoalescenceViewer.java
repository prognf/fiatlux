package models.CAmodels.multiRegister;

import components.allCells.MultiRegisterCell;
import components.arrays.GenericCellArray;
import components.arrays.RegularDynArray;
import components.types.IntC;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.controllers.Radio3Control;
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.LineAutomatonViewer;
import topology.basics.LinearTopology;

/*--------------------
 * enables the display of a 1D automaton the model passed
 * in the constructor is used to interpret the colors
 * 
 * @author Nazim Fates
 *--------------------*/
public class CoalescenceViewer extends LineAutomatonViewer {


	final static FLColor COLW= FLColor.c_white, COLB= FLColor.c_darkblue;

	private static final PaintToolKit PALETTEAGREEMENT = 
			new PaintToolKit(new FLColor []{COLW, FLColor.c_darkgreen, FLColor.c_red, FLColor.c_grey1});
	private static final PaintToolKit PALETTELAYERA = new PaintToolKit(new FLColor []{COLW, COLB, COLW, COLB});
	private static final PaintToolKit PALETTELAYERB = new PaintToolKit(new FLColor []{COLW, COLW, COLB, COLB});
	private static final PaintToolKit PALETTEUSELESS = new PaintToolKit(new FLColor []{COLW, COLW, COLW, COLW });


	private final PaintToolKit m_palette;

	GenericCellArray<MultiRegisterCell> m_array ;

	DisplayLayerControl m_display;
	boolean m_displayA=true, m_displayB=true;


	public CoalescenceViewer(IntC squareDim, LinearTopology topo1D,
			RegularDynArray automaton, int tsize) {
		super(squareDim, topo1D, automaton, tsize);
		m_array=  new GenericCellArray<MultiRegisterCell>(automaton);

		m_palette= PALETTEAGREEMENT;
		this.SetPalette(m_palette);
		m_display= new DisplayLayerControl();
	}

	@Override
	public void UpdateBuffer(int[] timeBuffer) {
		int Lsize= timeBuffer.length;
		for(int pos=0; pos< Lsize; pos++) {
			int 
			state0= m_array.GetCell(pos).GetStateI(0),
			state1= m_array.GetCell(pos).GetStateI(1);

			timeBuffer[pos] = 2*state1 + state0 ;
		}

	}

	@Override
	protected void FlipCellState(int x) {
		//Macro.Debug("Kloupa :" + x);
		MultiRegisterCell cell= m_array.GetCell(x);
		int sa= cell.GetStateI(0), sb= cell.GetStateI(1);
		if (m_displayA){
			sa= 1 - sa;
		}
		if (m_displayB){
			sb= 1 - sb;
		}
		cell.SetStateA(sa);
		cell.SetStateB(sb);
		UpdateView();
	}

	protected void UpdateDisplayColours() {
		boolean layA= m_displayA, layB= m_displayB;
		PaintToolKit pal= layA?(layB?PALETTEAGREEMENT:PALETTELAYERA):(layB?PALETTELAYERB:PALETTEUSELESS);
		this.SetPalette(pal);
		UpdateView();
	}


	@Override
	public FLPanel GetSpecificPanel(){
		FLPanel p = FLPanel.NewPanel(m_display);
		return p;
	}

	private void SetTo(boolean b1, boolean b2) {
		m_displayA=b1;
		m_displayB=b2;
		UpdateDisplayColours();
	}
	
	public void DisplayAOnly(){
		SetTo(true,false);
	}
	
	public void DisplayBOnly(){
		SetTo(false,true);
	}
	
	public void DisplayBoth(){
		SetTo(true,true);
	}
	
	
	
	class DisplayLayerControl extends Radio3Control {

		final static int DEFSELECT=2;
	
		public DisplayLayerControl() {
			super("display", "A", "A+B", "B");
			this.SetSelection(DEFSELECT);
		}

		@Override
		protected void Action1() {
			DisplayAOnly();
		}

		@Override
		protected void Action2() {
			DisplayBoth();
		}

		@Override
		protected void Action3() {
			DisplayBOnly();
		}

	}

}
