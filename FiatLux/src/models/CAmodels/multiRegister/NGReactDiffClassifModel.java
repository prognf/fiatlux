package models.CAmodels.multiRegister;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Comparator;

import components.allCells.Cell;
import components.arrays.RegularDynArray;
import components.types.IntC;
import components.types.ProbaPar;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.RegularAutomatonViewer;
import initializers.SuperInitializer;
import models.CAmodels.CellularModel;
import topology.basics.PlanarTopology;
import topology.basics.SuperTopology;
import topology.basics.TopologyCoder;

/**
 * @author Nicolas Gauville
 */
public class NGReactDiffClassifModel extends CellularModel {
    final public static String NAME = "ReacDiff-Classif";

    public static final int N = 0;
    public static final int E = 1;
    public static final int R1 = 2;
    public static final int R2 = 3;
    public static final int D = 4;
    public static final int V = 5;
    public static final int M = 6;
    public static final int TA = 30;
    public static final int TB = 300;

    private ProbaPar m_pTr1;
    private ProbaPar m_pTr2;

    public NGReactDiffClassifModel() {
        super();

        m_pTr1 = new ProbaPar("pTr1", 0.5);
        m_pTr2 = new ProbaPar("pTr2", 0.2);
    }

    public void setProbabilities(double failure, double repair, double pTr) {
        m_pTr1.SetVal(pTr);
        m_pTr2.SetVal(pTr);
    }

    public static int countStateRotating(int search, NGReactDiffClassifCell inCell) {
        int n1 = -1, n2 = -1, n3 = -1;

        ArrayList<NGReactDiffClassifCell> ngb = new ArrayList<>();

        // Toom neighboors (in Moore Neighboorhood)
        ngb.add(0, inCell.GetNeighbour(1));
        ngb.add(1, inCell.GetNeighbour(0));
        ngb.add(2, inCell.GetNeighbour(3));

        // Others
        ngb.add((int) ((ngb.size()) * Math.random()), inCell.GetNeighbour(5));
        ngb.add((int) ((ngb.size()) * Math.random()), inCell.GetNeighbour(7));
        ngb.add((int) ((ngb.size()) * Math.random()), inCell.GetNeighbour(2));
        ngb.add((int) ((ngb.size()) * Math.random()), inCell.GetNeighbour(4));
        ngb.add((int) ((ngb.size()) * Math.random()), inCell.GetNeighbour(6));
        ngb.add((int) ((ngb.size()) * Math.random()), inCell.GetNeighbour(8));


        for (NGReactDiffClassifCell n : ngb) {
            int status = n.GetStateOne();

            if (status != D) {
                if (n1 == -1) {
                    n1 = status;
                } else if (n2 == -1) {
                    n2 = status;
                } else if (n3 == -1) {
                    n3 = status;
                    break;
                }
            }
        }

        return (n1 == search ? 1 : 0) + (n2 == search ? 1 : 0) + (n3 == search ? 1 : 0);
    }

    public int[] GetTransitionResult(NGReactDiffClassifCell in_Cell) {
        int[] res = new int[2];

        int state = in_Cell.GetStateOne();
        int count = in_Cell.GetStateTwo();
        int newState = state;

        // Règle pour contrer l'asynchronisme du compteur
        // On prend le second état majoritaire parmi le voisinage
        ArrayList<Point> list = new ArrayList<>();
        list.add(new Point(count, 1));
        int nbNeighboors = in_Cell.GetNeighbourhoodSize();
        for (int i = 0; i < nbNeighboors; i++) {
            int nState = in_Cell.GetNeighbour(i).GetStateOne();
            int nCount = in_Cell.GetNeighbour(i).GetStateTwo();

            if (nState != D) {
                boolean found = false;
                for (Point p : list) {
                    if (p.x == nCount) {
                        p.y++;
                        found = true;
                        break;
                    }
                }

                if (!found) {
                    list.add(new Point(nCount, 1));
                }
            }

            list.sort(new Comparator<Point>() {
                @Override
                public int compare(Point a, Point b) {
                    if (a.y < b.y) {
                        return 1;
                    } else if (a.y > b.y) {
                        return -1;
                    }
                    return 0;
                }
            });

            count = (int) list.get(0).getX();
        }

        count++;
        if (count > TA) {
            count = -TB;
        }

        if (state != D) {
            if (count > 0) {
                // Première règle (Greenberg-Hastings avec voisinage de VN)
                if (state == E) {
                    newState = R1;
                } else if (state == R1) {
                    newState = R2;
                } else if (state == R2) {
                    newState = N;
                } else if (state == N) {
                    int nbD = in_Cell.CountOccurenceOf(D);
                    int nbE = in_Cell.CountOccurenceOf(E);
                    if ((nbD > 0 && RandomEventDouble(m_pTr1)) || (nbE > 0 && RandomEventDouble(m_pTr2))) {
                        newState = E;
                    }
                }
            } else if (count == -TB) {
                // Conversion de l'état (GH vers Classif), pas de prise en compte du voisinage
                if (state == N) {
                    newState = M; // les cellules normales deviennent mortes
                } else if (state == E || state == R1 || state == R2) {
                    newState = V; // les cellules excitées et réfractaires deviennent vivantes
                }
            } else if (count == 0) {
                // Conversion de l'état (Classif vers GH), pas de prise en compte du voisinage
                if (state == V || state == M) {
                    newState = N;
                }
            } else {
                // Seconde règle (classification de la densité : majorité avec voisinage de Toom)
                int nbV = countStateRotating(V, in_Cell);
                int nbM = countStateRotating(M, in_Cell);

                if (nbV > nbM) {
                    newState = V;
                } else if (nbM > nbV) {
                    newState = M;
                } else {
                    newState = state;
                }
            }
        }

        res[0] = newState;
        res[1] = count;

        return res;
    }

    @Override
    public Cell GetNewCell(){
        return new NGReactDiffClassifCell(this);
    }

    @Override
    public RegularAutomatonViewer GetPlaneAutomatonViewer(IntC cellPixSize, SuperTopology topology, RegularDynArray automaton) {
        PlanarTopology topo2D = (PlanarTopology) topology;
        NG2RulesStatesViewer av = new NG2RulesStatesViewer(cellPixSize, topo2D.GetXYsize(), automaton);
        av.SetPalette(GetPalette());

        return av;
    }

    @Override
    public PaintToolKit GetPalette() {
        PaintToolKit palette = PaintToolKit.EmptyPalette();

        palette.AddColor(FLColor.c_white); // N
        palette.AddColor(FLColor.c_red); // E
        palette.AddColor(FLColor.c_orange); // R1
        palette.AddColor(FLColor.c_darkorange); // R2
        palette.AddColor(FLColor.c_black); // D
        palette.AddColor(FLColor.c_orange); // V
        palette.AddColor(new FLColor(0, 150, 153)); // M

        return palette;
    }

    @Override
    public SuperInitializer GetDefaultInitializer() {
        return new NG3StateInitializer(N, N, N, D);
    }

    @Override
    public String GetDefaultAssociatedTopology() {
        return TopologyCoder.s_TM9;
    }

    @Override
    public FLPanel GetSpecificPanel() {
        return FLPanel.NewPanel(m_pTr1.GetControl(), m_pTr2.GetControl());
    }
}
