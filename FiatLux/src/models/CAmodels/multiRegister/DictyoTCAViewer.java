package models.CAmodels.multiRegister;

import java.awt.event.MouseEvent;

import components.allCells.MultiRegisterCell;
import components.arrays.GenericCellArray;
import components.arrays.RegularDynArray;
import components.types.IntC;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.viewers.RegularAutomatonViewer;
import main.Macro;
import topology.basics.SuperTopology;

/*--------------------
 * enables the display
 * 
 * @author Nazim Fates
 *********************************************************************/
public class DictyoTCAViewer extends RegularAutomatonViewer {

	/*--------------------
	 * Attributes
	 --------------------*/

	GenericCellArray<MultiRegisterCell> m_array;
	PaintToolKit m_ChemicalPalette = new PaintToolKit(null);
	/*--------------------
	 * Constructor
	 --------------------*/

	public DictyoTCAViewer(IntC in_CellSize, IntC in_Gridsize, RegularDynArray in_Automaton){
		super(in_CellSize, in_Gridsize);
		m_array = new GenericCellArray<MultiRegisterCell>(in_Automaton);
		m_ChemicalPalette.SetColorsRedShift(DictyoTCAmodel.MAXCHEMICAL);
	}

	/*--------------------
	 * main methods
	 --------------------*/



	/** main component */
	@Override
	protected void DrawSystemState() {
		for (int x = 0; x < getXsize(); x++){
			for (int y = 0; y < getYsize(); y++) {
				MultiRegisterCell cell = GetCellXY(x, y);				
				int state = cell.GetStateI(DictyoTCAmodel.r_STATE); 
				FLColor col;
				if (state != SuperTopology.GHOSTSTATE) {
					if (state <= DictyoTCAmodel.E2 ){ // chemical val color
						int chemical = GetCellXY(x, y).GetStateI(DictyoTCAmodel.r_CHEM);
						col= m_ChemicalPalette.GetColor(chemical);
					} else {
						col = GetColor(state);	
					}
					
				} else {
					// color for "ghost" cells (holes)
					col = m_GhostColor;
				}				
				SetColor(col);								
				DrawSquare(x, y);	
			}//for y
		}//for x
	}

	/*--------------------
	 * Other methods
	 --------------------*/

	/* returns the state of a cell */
	// NO LONGER CALLED !!
	public final int GetCellColorNumXY(int x, int y) {
		return -999;
	}

//	returns if a cell stable
	protected boolean IsStableXY(int x, int y){
		Macro.FatalError("Not implemented");
		return false;
	}

	/*--------------------
	 * mapping methods
	 --------------------*/

	/*final protected MultiRegisterCell GetCell(int cell) {
		return m_Array[cell];
	}*/

	final protected MultiRegisterCell GetCellXY(int x, int y) {
		return m_array.GetCell( Map2D(x,y) );
	}

	void PrnCell(MultiRegisterCell c){
		int state= c.GetStateI(DictyoTCAmodel.r_STATE);
		int dir= c.GetStateI(DictyoTCAmodel.r_DIR);
	}

	/** print info  */
	public final void ProcessMouseEventXY(int x, int y, MouseEvent event) {
		MultiRegisterCell c = GetCellXY(x,y);
		//PrnCell(c); Macro.DebugNOCR(":::");
		for (int neighb=0; neighb<DictyoTCAmodel.NEIGHBSIZE; neighb++){
			PrnCell(c.GetNeighbour(neighb));
		}
		//Macro.Debug("<");
	}

	/*--------------------
	 * test 
	 --------------------*/

	/*public void paintComponent(Graphics g) {
		CheckConfig();
		super.paintComponent(g);
	}*/

/*	void CheckConfig(){
		for (int x=0; x < GetXsize(); x++){
			for (int y=0; y < GetYsize(); y++){
				int state= m_Array[Map2D(x,y)].GetBufferI(DictyoTCAmodel.r_STATE);
				int dir= m_Array[Map2D(x,y)].GetBufferI(DictyoTCAmodel.r_DIR);
				//Macro.DebugNOCR( ":" + state + "," + dir);
			}
		}
		//Macro.Debug("END");
	}*/
}
