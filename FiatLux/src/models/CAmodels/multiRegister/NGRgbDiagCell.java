package models.CAmodels.multiRegister;

import components.allCells.SuperCell;
import components.allCells.TwoRegisterCell;
import components.arrays.GenericCellArray;
import models.CAmodels.CellularModel;
import models.CAmodels.typing.ITransitionResultAwareModel;

public class NGRgbDiagCell extends TwoRegisterCell {
    private CellularModel model;
    private GenericCellArray<NGRgbDiagCell> neighbourhood;

    public NGRgbDiagCell(CellularModel m) {
        super();

        model = m;
    }

    public int CountOccurenceOf(int state) {
        int count = 0;

        for (NGRgbDiagCell n : neighbourhood) {
            if (n.GetStateOne() == state) {
                count++;
            }
        }

        return count;
    }

    public boolean hasUnstableNeighbours() {
        int limit = (int) (NGRgbDiagModel.NB_STABILISATION_STEPS * 0.67);

        if (GetStateTwo() < limit && GetStateOne() != NGRgbDiagModel.ALERT_A) {
            return true;
        }

        for (NGRgbDiagCell cell : neighbourhood) {
            if (cell.GetStateTwo() < limit && cell.GetStateOne() != NGRgbDiagModel.ALERT_A) {
                return true;
            }
        }

        return  false;
    }

    @Override
    public void SetNeighbourhood(GenericCellArray<SuperCell> neighblist) {
        neighbourhood = new GenericCellArray<>(neighblist);
    }

    @Override
    public int GetNeighbourhoodSize() {
        return neighbourhood.GetSize();
    }

    @Override
    public NGRgbDiagCell GetNeighbour(int in_NeighbourPosition) {
        return neighbourhood.get(in_NeighbourPosition);
    }

    @Override
    public void sig_UpdateBuffer() {
        int[] res = ((ITransitionResultAwareModel) model).GetTransitionResult(this);

        SetBufferOne(res[0]);
        SetBufferTwo(res[1]);
    }

    @Override
    public void sig_MakeTransition() {
        m_StateOne = m_bufferOne;
        m_StateTwo = m_bufferTwo;
    }
}
