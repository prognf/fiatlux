package models.CAmodels.multiRegister;

import javax.swing.JCheckBox;

import components.allCells.MultiRegisterCell;
import grafix.gfxTypes.IntField;
import grafix.gfxTypes.elements.FLPanel;
import initializers.CAinit.MultiRegisterPlaneInitializer;

class DictyoTCAinit extends MultiRegisterPlaneInitializer {
	
	MultiRegisterCell [] m_Array; 
	int msz_x, msz_y;
	

	final static int DEFPOPSIZE=1, DEFSQUARELEN=0, DEFDENS=0;
	private static final int 
		STATIC = DictyoTCAmodel.AG,
		PARTICLE= DictyoTCAmodel.P2,
		EMPTY= DictyoTCAmodel.E2,
		OBSTACLE= DictyoTCAmodel.OB;

	IntField mF_Density = new IntField("Density (%):", 3, DEFDENS);
	IntField mF_PopSize = new IntField("Population size:", 3, DEFPOPSIZE);
	IntField mF_SquareLen = new IntField("Square Len:", 3, DEFSQUARELEN);

	JCheckBox mF_Obstacle= new JCheckBox("Obstacle");

	/*--------------------
	 * abstract methods definition
	 --------------------*/
	public void SubInit() {
		ZeukodInit();
		//StaticInit();
	}

	public FLPanel GetSpecificPanel() {
		FLPanel p = new FLPanel();
		p.Add(mF_Density, mF_PopSize, mF_SquareLen);
		p.Add(mF_Obstacle);
		p.setOpaque(false);
		return p;
	}

	/*--------------------
	 * Other methods
	 --------------------*/
	private void SetAutomatonState(int cell, int state) {
		SetCellStateI(cell, DictyoTCAmodel.r_STATE, state);		
	}

	private void SetAutomatonStateXY(int x, int y, int state) {
		SetCellStateXYregister(x, y, DictyoTCAmodel.r_STATE, state);
	}
	
	private void ZeukodInit() {
		int size = GetLsize(), Xsize= GetXsize(), Ysize= GetYsize();

		// density init
		for (int cell = 0; cell < size; cell++) {
			if (RandomEventInt(mF_Density.GetValue())) {
				SetAutomatonState(cell, PARTICLE);
			} else {
				SetAutomatonState(cell, EMPTY);
			}	
			int dir= 3;//RandomInt(DictyoTCAmodel.NEIGHBSIZE);
			SetCellStateI(cell, DictyoTCAmodel.r_DIR, dir);
			int chem= 0;//RandomEventInt(1)?DictyoTCAmodel.MAXCHEMICAL:0;
			SetCellStateI(cell, DictyoTCAmodel.r_CHEM, chem);
		}
	
		StaticBorders(Xsize, Ysize);
		
		// pop init
		for (int pop = 0; pop < mF_PopSize.GetValue();) {
			int pos = RandomPos();
			int current = GetCellStateI(pos, DictyoTCAmodel.r_STATE);
			if ( current == EMPTY){
				pop++;
				SetAutomatonState(pos, PARTICLE);
			}
		}		
	
		int L=mF_SquareLen.GetValue();
		int X= GetXsize()/2 - L/2, Y= GetYsize()/2 -  L/2 ; 
		for (int dx=0; dx < L ; dx++){
			for (int dy=0; dy < L ; dy++){
				SetAutomatonStateXY(X+dx, Y+dy,PARTICLE);
			}
		}

		if (mF_Obstacle.isSelected()){
			for (int dx=0; dx < L ; dx++){
				SetAutomatonStateXY(X+dx, Y- 3,OBSTACLE);

			}
		}
	}

	
	

	private void StaticBorders(int Xsize, int Ysize) {
		// borders with static cells
		for (int x=0; x<Xsize; x++){
			SetAutomatonStateXY(x, 0, STATIC);
			SetAutomatonStateXY(x, Ysize-1, STATIC);
		}
		for (int y=0; y<Ysize; y++){
			SetAutomatonStateXY(0, y, STATIC);
			SetAutomatonStateXY(Xsize-1, y, STATIC);
		}		
	}

	/*private void StaticInit(){
		int X= GetXsize(), Y= GetYsize();
		for (int dx=0; dx < X ; dx++){
			for (int dy=0; dy < Y ; dy++){
				if ((dx%2 == 0)&&(dy%2 == 0)){
					SetAutomatonStateXY(dx, dy,PARTICLE);
				} else {
					SetAutomatonStateXY(dx, dy,EMPTY);
				}
			}
		}

	}*/

}// end class
