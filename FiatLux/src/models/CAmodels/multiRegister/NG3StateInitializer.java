package models.CAmodels.multiRegister;

import components.types.IntegerList;
import grafix.gfxTypes.DoubleField;
import grafix.gfxTypes.elements.FLPanel;
import initializers.CAinit.TwoRegisterPlaneInitializer;

/**
 * @author Nicolas Gauville
 * Rgb-model variant initializer
 */
public class NG3StateInitializer extends TwoRegisterPlaneInitializer {
    DoubleField mF_KoNum = new DoubleField("% ko cells:", 3, 0.0);

    private int state1;
    private int state2;
    private int state3;
    private int deadState;

    public NG3StateInitializer() {
        super();

        state1 = 0;
        state2 = 1;
        state3 = 2;
    }

    public NG3StateInitializer(int a, int b, int c) {
        super();

        state1 = a;
        state2 = b;
        state3 = c;
        deadState = 3;
    }

    public NG3StateInitializer(int a, int b, int c, int ds) {
        super();

        state1 = a;
        state2 = b;
        state3 = c;
        deadState = ds;
    }

    public FLPanel GetSpecificPanel() {
        FLPanel p = FLPanel.NewPanel(mF_KoNum);
        p.setOpaque(false);
        FLPanel Control = FLPanel.NewPanel(p, GetRandomizer());
        Control.setOpaque(false);
        return Control;
    }

    public void setKOCells(double value) {
        mF_KoNum.SetDouble(value);
    }

    @Override
    protected void SubInit() {
        for (int i = 0; i < GetSize(); i++) {
            SetCellStateOne(i, RandomEventDouble(1./3.) ? state1 : (RandomEventDouble(0.5) ? state2 : state3));
            SetCellStateTwo(i, 0);
        }

        InitRegOneKRandom((int) (mF_KoNum.GetValue() * (double) GetSize()), deadState);

    }

    private void InitRegOneKRandom(int k, int state) {
        IntegerList posList= RandomKpositions(k);
        for (int pos : posList) {
            SetCellStateOne(pos, state);
        }
    }
}
