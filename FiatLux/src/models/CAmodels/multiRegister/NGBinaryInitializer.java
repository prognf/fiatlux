package models.CAmodels.multiRegister;

import components.types.IntegerList;
import grafix.gfxTypes.DoubleField;
import grafix.gfxTypes.elements.FLPanel;
import initializers.CAinit.TwoRegisterPlaneInitializer;

/**
 * @author Nicolas Gauville
 * Binary initializer
 */
public class NGBinaryInitializer extends TwoRegisterPlaneInitializer {
    DoubleField mF_KoNum = new DoubleField("ko cells:", 3, 0.1);

    private int state1;
    private int state2;
    private int failure;

    public NGBinaryInitializer() {
        super();

        state1 = 0;
        state2 = 1;
        failure = 2;
    }

    public NGBinaryInitializer(int a, int b, int f) {
        super();

        state1 = a;
        state2 = b;
        failure = f;
    }

    public FLPanel GetSpecificPanel() {
        FLPanel p = FLPanel.NewPanel(mF_KoNum);
        FLPanel Control = FLPanel.NewPanel(p, GetRandomizer());
        p.setOpaque(false);
        Control.setOpaque(false);
        return Control;
    }

    public void setKOCells(double value) {
        mF_KoNum.SetDouble(value);
    }

    @Override
    protected void SubInit() {
        for (int i = 0; i < GetSize(); i++) {
            SetCellStateOne(i, RandomEventDouble(0.5) ? state1 : state2);
            SetCellStateTwo(i, 0);
        }

        InitRegOneKRandom((int) (mF_KoNum.GetValue() * (double) GetSize()), failure);

    }

    private void InitRegOneKRandom(int k, int state) {
        IntegerList posList= super.RandomKpositions(k);
        for (int pos : posList) {
            SetCellStateOne(pos, state);
            SetCellStateTwo(pos, NGMichaosDiagModel.NB_STABILISATION_STEPS);
        }
    }
}
