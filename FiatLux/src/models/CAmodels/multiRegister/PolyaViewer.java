package models.CAmodels.multiRegister;

import components.allCells.MultiRegisterCell;
import components.arrays.GenericCellArray;
import components.arrays.RegularDynArray;
import components.types.IntC;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.controllers.Radio3Control;
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.LineAutomatonViewer;
import main.Macro;
import topology.basics.LinearTopology;

/*--------------------
 * enables the display of a 1D automaton the model passed
 * in the constructor is used to interpret the colors
 * it implements OenRegisterReadable by saying what are the current colors
 * @author Nazim Fates
 *--------------------*/
public class PolyaViewer extends LineAutomatonViewer {

	final static int MODULO= 10000;
	int m_count=0;

	private static final int DEF_SELECT = 2;

	final static PaintToolKit 
	one= PaintToolKit.GetRainbow(FLColor.MAX+1), 
	two= PaintToolKit.GetBWpalette(FLColor.MAX+1), 
	three= PaintToolKit.GetRandomPalette(FLColor.MAX+1);


	GenericCellArray<MultiRegisterCell> m_array ;
	Radio3Control m_PaletteControl; 
	RegularDynArray m_automaton;

	public PolyaViewer(IntC in_CellSize, 
			LinearTopology in_topo, RegularDynArray in_automaton, int in_Tsize) {
		super(in_CellSize, in_topo, in_automaton, in_Tsize);

		//Macro.Debug(" ki ki ki Polya viewer init...");
		//Macro.Debug("houhou");
		zozoSetUpPalette(one);

		m_automaton= in_automaton;
		m_array = new GenericCellArray<MultiRegisterCell>(in_automaton);

		m_PaletteControl = new PaletteControl(); // inner class

	}




	@Override
	public void UpdateBuffer(int[] timeBuffer) {
		for (int pos=0 ; pos < timeBuffer.length ; pos++){
			MultiRegisterCell cell = m_array.GetCell(pos);
			int s= cell.GetStateI(0);
			int n= cell.GetStateI(1);
			float p= s/(float)n;

			timeBuffer[pos]= StateToColorCode(p);
		}
		
		if (m_count % MODULO == 0){
			PrintEvenOddSums();
		}
		m_count++;
	}


	private void PrintEvenOddSums() {
		double sumEven=0, sumOdd=0;
		int N= m_array.GetSize();
		StringBuilder str = new StringBuilder("[");
		for (int pos=0 ; pos < N ; pos++){
			MultiRegisterCell cell = m_array.GetCell(pos);
			int s= cell.GetStateI(0);
			int t= cell.GetStateI(1);
			double p= s/(double)t;
			str.append(String.format(" %.3f ", p));
			if (pos%2==0){
				sumEven += p;
			} else{
				sumOdd += p;
			}
		}
		str.append(" ]");
		sumEven /= (N/2);
		sumOdd /= (N/2);
		double dE= sumEven, dO= sumOdd;
		int time= m_automaton.GetTime();
		Macro.fPrint("t:%d sum even:%.3f  odd:%.3f sum:%.3f",time, dE, dO, dE+dO);
		Macro.print(str.toString());
		//Macro.print(" even : " + sumEven + " odd:" + sumOdd);
	}




	// nothing serious here...
	@Override
	protected void FlipCellState(int pos) {
		MultiRegisterCell cell = m_array.GetCell(pos);
		int s= cell.GetStateI(0);
		int n= cell.GetStateI(1);
		cell.SetStateI(0, n-s);
	}

	/** overrides  */
	public FLPanel GetSpecificPanel(){
		FLPanel p = new FLPanel();
		p.Add(super.GetSpecificPanel(), m_PaletteControl);
		return	p;	
	}

	final int StateToColorCode(float in_state) {
		return (int)(in_state * FLColor.MAX) ;
	}


	private void zozoSetUpPalette(PaintToolKit paint) {
		//Macro.Debug("houhouhou setting up new palette... ");
		SetPalette( paint);
		UpdateView();
	}

	class PaletteControl extends Radio3Control {

		PaletteControl(){
			super("palette:","rainbow","black'n'white","random");
			SetSelection(DEF_SELECT);
		}

		@Override
		protected void Action1() {zozoSetUpPalette(one);}

		@Override
		protected void Action2() {zozoSetUpPalette(two);}

		@Override
		protected void Action3() {zozoSetUpPalette(three);}

	}


}
