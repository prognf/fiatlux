package models.CAmodels.multiRegister;

import components.allCells.Cell;
import components.allCells.TwoRegisterCell;
import components.arrays.RegularDynArray;
import components.types.IntC;
import components.types.ProbaPar;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.RegularAutomatonViewer;
import initializers.SuperInitializer;
import models.CAmodels.CellularModel;
import models.CAmodels.typing.ITransitionResultAwareModel;
import topology.basics.PlanarTopology;
import topology.basics.SuperTopology;
import topology.basics.TopologyCoder;

/**
 * @author Nicolas Gauville
 */
public class NGMichaosDiagModel extends CellularModel implements ITransitionResultAwareModel {
    final public static String NAME = "Michaos-Diag";

    public static final int NB_STABILISATION_STEPS = 50;

    public static final int DEADCELL = 0;
    public static final int LIVINGCELL = 1;
    public static final int FAILURE = 2;
    public static final int ALERT_A = 3;
    public static final int ALERT_B = 4;

    private int _bornMin = 2;
    private int _bornMax = 6;
    private int _surviveMin = 3;
    private int _surviveMax = 6;

    private ProbaPar m_pFailure;
    private ProbaPar m_pRepair;
    private ProbaPar m_pRule;

    public NGMichaosDiagModel() {
        super();

        m_pFailure = new ProbaPar("pFailure", 0.0);
        m_pRepair = new ProbaPar("pRepair", 0.0);
        m_pRule = new ProbaPar("pRule", 0.9);
    }

    public void setProbabilities(double failure, double repair, double pTr) {
        m_pFailure.SetVal(failure);
        m_pRepair.SetVal(repair);
        m_pRule.SetVal(pTr);
    }

    public int[] GetTransitionResult(TwoRegisterCell cell) {
        NGMichaosDiagCell in_Cell = (NGMichaosDiagCell) cell;

        int[] res = new int[2];

        int state = in_Cell.GetStateOne();
        int newState = state;

        int nAlive = in_Cell.CountOccurenceOf(LIVINGCELL);

        if (state != FAILURE && RandomEventDouble(m_pFailure)) {
            newState = FAILURE;
        } else if (state == FAILURE) {
            if (RandomEventDouble(m_pRepair)) {
                newState = RandomEventDouble(0.5) ? DEADCELL : LIVINGCELL;
            } else {
                newState = FAILURE;
            }
        } else if (in_Cell.CountOccurenceOf(ALERT_B) > 0) {
            newState = ALERT_B;
        } else if (RandomEventDouble(m_pRule)) {
            if (state == DEADCELL && (nAlive >= _bornMin && nAlive <= _bornMax)) {
                newState = LIVINGCELL;
            } else if (state == LIVINGCELL && !(nAlive >= _surviveMin && nAlive <= _surviveMax)) {
                newState = DEADCELL;
            }
            /*if (nAlive > 0) {
                newState = LIVINGCELL;
            } else {
                newState = DEADCELL;
            }*/
        } else {
            int nDead = in_Cell.CountOccurenceOf(DEADCELL);

            if (nAlive > nDead) {
                newState = LIVINGCELL;
            } else {
                newState = DEADCELL;
            }
            // newState = DEADCELL;
        }

        res[0] = newState;

        if (newState == FAILURE || newState == ALERT_A || newState == ALERT_B) {
            res[1] = NB_STABILISATION_STEPS;
        } else {
            res[1] = (state == newState ? Math.min(NB_STABILISATION_STEPS, in_Cell.GetStateTwo() + 1) : 0);

            if (res[1] == NB_STABILISATION_STEPS) {
                res[0] = ALERT_A;
                res[1] = NB_STABILISATION_STEPS;
            }

            if (in_Cell.CountOccurenceOf(ALERT_A) >= 4) {
                res[0] = ALERT_B;
            }
        }

        return res;
    }

    @Override
    public Cell GetNewCell(){
        return new NGMichaosDiagCell(this);
    }

    @Override
    public RegularAutomatonViewer GetPlaneAutomatonViewer(IntC cellPixSize, SuperTopology topology, RegularDynArray automaton) {
        PlanarTopology topo2D = (PlanarTopology) topology;
        NGBinaryViewer av = new NGBinaryViewer(cellPixSize, topo2D.GetXYsize(), automaton, NB_STABILISATION_STEPS, FAILURE, ALERT_A, ALERT_B);
        av.SetPalette(GetPalette());

        return av;
    }

    @Override
    public PaintToolKit GetPalette() {
        PaintToolKit palette = PaintToolKit.EmptyPalette();

        addGradient(palette, 0.5, 0.5, 0.5, NB_STABILISATION_STEPS);
        addGradient(palette, 0.3, 0.3, 1, NB_STABILISATION_STEPS);
        palette.AddColorRGB(0, 0, 0); // Q DEF
        palette.AddColorRGB(200, 200, 0); // ALERT A
        palette.AddColorRGB(200, 0, 200); // ALERT B
        return palette;
    }

    private void addGradient(PaintToolKit palette, double r, double g, double b, int steps) {
        for (int i = 0; i < steps; i++) {
            int coeff = (int) (((double) i / (double) steps) * 200.0);
            palette.AddColorRGB((int) (r * 200 + (1 - r) * coeff), (int) (g * 200 + (1 - g) * coeff), (int) (b * 200 + (1 - b) * coeff));
        }
    }

    @Override
    public SuperInitializer GetDefaultInitializer() {
        return new NGBinaryInitializer(LIVINGCELL, DEADCELL, FAILURE);
    }

    @Override
    public String GetDefaultAssociatedTopology() {
        return TopologyCoder.s_TM8;
    }

    @Override
    public FLPanel GetSpecificPanel() {
        FLPanel p = FLPanel.NewPanel(m_pFailure.GetControl(), m_pRepair.GetControl(), m_pRule.GetControl());
        p.setOpaque(false);
        return p;
    }
}
