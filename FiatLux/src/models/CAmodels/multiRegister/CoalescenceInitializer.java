package models.CAmodels.multiRegister;

import initializers.CAinit.MultiRegisterInitializer;

public class CoalescenceInitializer extends MultiRegisterInitializer {
	@Override
	protected void SubInit() {
		InitUniform();
	}

	private void InitUniform() {
		int size = GetLsize();
		for (int cell = 0; cell < size; cell++) {
			int 
			state0 = RandomInt(2),
			state1 = RandomInt(2);
			SetCellStateI(cell, 0, state0);
			SetCellStateI(cell, 1, state1);
		}
	}
}
