package models.CAmodels.decentralisedDiagnosis;

import components.allCells.Cell;
import components.allCells.ClassicalCACell;
import components.allCells.OneRegisterIntCell;
import components.types.ProbaPar;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.controllers.IntController;
import grafix.gfxTypes.elements.FLList;
import grafix.gfxTypes.elements.FLPanel;
import initializers.SuperInitializer;
import main.tables.PlotterSelectControl;
import models.CAmodels.ClassicalModel;
import topology.basics.TopologyCoder;

/**
 * @author Nicolas Gauville
 * Variante du Modèle RGB
 */
public class NGRgbDiffModel extends ClassicalModel {
    final public static String NAME = "RGB-a";
    public SuperInitializer GetDefaultInitializer() {
        return new NGRgbInitializer();
    }

    public static final int R = 0;
    public static final int G = 1;
    public static final int B = 2;
    public static final int FAILURE = 3;
    public static final int ALERT = 4;

    private static final String[] modes = {"RGB-a", "RGB-ai"};
    private FLList selector;

    private int p_a = 3;

    private ProbaPar m_pFailure;
    private ProbaPar m_pRepair;
    private IntController m_cA = new AControl();

    public NGRgbDiffModel() {
        super();

        m_pFailure = new ProbaPar("pFailure", 0.0);
        m_pRepair = new ProbaPar("pRepair", 0.0);
        selector = new FLList(modes);
    }

    public void setParameter(int a) {
        setParameter(a, true);
    }

    public void setParameter(int a, boolean sym) {
        p_a = a;
        selector.SelectItem(sym ? 0 : 1);
    }

    @Override
    public Cell GetNewCell() {
        ClassicalModel mod = this;
        return new ClassicalCACell(mod);
    }

    @Override
    public String GetDefaultAssociatedTopology() {
        return TopologyCoder.s_TM8;
    }

    public final int ApplyLocalFunction(OneRegisterIntCell cell) {
        int state = cell.GetState();
        int newState = state;
        int mode = selector.GetSelectedItemRank();

        if (state == ALERT && cell.CountOccurenceOf(ALERT) > 3) {
            return ALERT;
        }

        if (state != FAILURE && RandomEventDouble(m_pFailure)) {
            newState = FAILURE;
        } else if (state == FAILURE) {
            if (RandomEventDouble(m_pRepair)) {
                newState = RandomEventDouble(1./3.) ? R : (RandomEventDouble(0.5) ? G : B);
            } else {
                newState = FAILURE;
            }
        } else {
            int next = 0;
            int nextC = 0;

            switch (state) {
                case R:
                    next = G;
                    nextC = B;
                    break;
                case G:
                    next = B;
                    nextC = R;
                    break;
                case B:
                    next = R;
                    nextC = G;
                    break;
            }

            int count;

            if (mode == 0) {
                count = cell.CountOccurenceOf(next);
            } else {
                count = cell.CountOccurenceOf(nextC);
            }

            if (count > p_a) {
                newState = next;
            }
        }

        return newState;
    }

    @Override
    public String [] GetPlotterSelection2D() {
        return PlotterSelectControl.RGB_SELECT;
    }

    @Override
    final public int GetMaxStateValue() {
        return 3;
    }

    @Override
    public PaintToolKit GetPalette() {
        PaintToolKit palette = PaintToolKit.EmptyPalette();
        palette.AddColorRGB(200, 0, 0);
        palette.AddColorRGB(0, 200, 0);
        palette.AddColorRGB(0, 0, 200);
        palette.AddColorRGB(0, 0, 0);
        palette.AddColorRGB(100, 100, 100);
        return palette;
    }

    public FLPanel GetSpecificPanel() {
        FLPanel p = FLPanel.NewPanel(m_pFailure.GetControl(), m_pRepair.GetControl(), m_cA, selector);
        p.setOpaque(false);
        return p;
    }

    class AControl extends IntController {
        protected AControl() {
            super("A", 8);
        }
        protected int ReadInt() {
            return p_a;
        }
        protected void SetInt(int val) {
            setParameter(val);
        }
    }
}
