package models.CAmodels.decentralisedDiagnosis;

import grafix.gfxTypes.DoubleField;
import grafix.gfxTypes.elements.FLPanel;
import initializers.CAinit.OneRegisterInitializer;

/**
 * @author Nicolas Gauville
 * Initializer for NGRDCModel
 */
public class NGRDCInitializer extends OneRegisterInitializer {
    DoubleField mF_KoNum = new DoubleField("% ko cells:", 3, 0.1);
    NGRDCModel m_model;

    public NGRDCInitializer(NGRDCModel in_model) {
        m_model = in_model;
    }

    public void SubInit() {
        InitStateKRandom((int) (mF_KoNum.GetValue() * (double) GetSize()), NGRDCModel.S_FAILING);
    }

    public FLPanel GetSpecificPanel() {
        FLPanel p = FLPanel.NewPanel(mF_KoNum);
        FLPanel Control = FLPanel.NewPanel(p, GetRandomizer());
        p.setOpaque(false);
        Control.setOpaque(false);
        return Control;
    }
}
