package models.CAmodels.decentralisedDiagnosis;

import components.allCells.OneRegisterIntCell;
import components.types.ProbaPar;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.elements.FLPanel;
import initializers.SuperInitializer;
import initializers.CAinit.BinaryInitializer;
import models.CAmodels.ClassicalModel;
import topology.basics.TopologyCoder;

/**
 * @author Nicolas Gauville
 * Larger Than Life Modem
 */
public class NGChaosMajorityModel extends ClassicalModel {
	public final static int DEADCELL = 0, LIVINGCELL = 1, FAILURE = 2;
	public static final String NAME = "MiChaos";

	private ProbaPar m_pRule;
	private ProbaPar m_pFailure;
	private ProbaPar m_pRepair;
	// 2 4 2 4 - 2 6 3 6
	private int _bornMin = 2; // 2
	private int _bornMax = 6; // 4
	private int _surviveMin = 3; // 2
	private int _surviveMax = 4; // 4

	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TV5;
	}

	public NGChaosMajorityModel() {
		super();

		m_pRule = new ProbaPar("pRule", 0.9);
		m_pFailure = new ProbaPar("pFailure", 0.0);
		m_pRepair = new ProbaPar("pRepair", 0.0);
	}

	public void setRules(int bornMin, int bornMax, int surviveMin, int surviveMax) {
		_bornMin = bornMin;
		_bornMax = bornMax;
		_surviveMin = surviveMin;
		_surviveMax = surviveMax;
	}

	public final int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
		int nAlive = in_Cell.CountOccurenceOf(LIVINGCELL);// - in_Cell.CountOccurenceOf(FAILURE);
		int predstate = in_Cell.GetState();

		if (predstate != FAILURE && RandomEventDouble(m_pFailure)) {
			return FAILURE;
		} else if (predstate == FAILURE) {
			if (RandomEventDouble(m_pRepair)) {
				return RandomEventDouble(0.5) ? DEADCELL : LIVINGCELL;
			}
			return FAILURE;
		}

		if (RandomEventDouble(m_pRule)) {
			if (predstate == DEADCELL && (nAlive >= _bornMin && nAlive <= _bornMax)) {
				return LIVINGCELL;
			} else if (predstate == LIVINGCELL && !(nAlive >= _surviveMin && nAlive <= _surviveMax)) {
				return DEADCELL;
			}
		} else {
			int nDead = in_Cell.CountOccurenceOf(DEADCELL);

			if (nAlive > nDead) {
				return LIVINGCELL;
			} else {
				return DEADCELL;
			}
		}

		return predstate;
	}

	public SuperInitializer GetDefaultInitializer() {
		return new BinaryInitializer();
	}

	@Override
	public PaintToolKit GetPalette() {
		PaintToolKit palette = PaintToolKit.EmptyPalette();
		palette.AddColor(FLColor.c_white);
		palette.AddColor(FLColor.c_blue);
		palette.AddColor(FLColor.c_black);
		return palette;
	}

	public FLPanel GetSpecificPanel() {
		FLPanel p = FLPanel.NewPanel(m_pFailure.GetControl(), m_pRepair.GetControl(), m_pRule.GetControl());
		p.setOpaque(false);
		return p;
	}
}
