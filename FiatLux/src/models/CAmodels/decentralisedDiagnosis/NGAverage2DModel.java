package models.CAmodels.decentralisedDiagnosis;

import components.allCells.Cell;
import components.allCells.ClassicalCACell;
import components.allCells.OneRegisterIntCell;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.elements.FLPanel;
import initializers.SuperInitializer;
import models.CAmodels.ClassicalModel;
import models.CAmodels.typing.IMaxStateAwareModel;
import topology.basics.TopologyCoder;

/**
 * @author Nicolas Gauville
 * Little experiment on 2d average model
 * mainly to test fiatlux, model not yet completely debugged
 */
public class NGAverage2DModel extends ClassicalModel implements IMaxStateAwareModel {
    final public static String NAME = "Average2D";
    public SuperInitializer GetDefaultInitializer() {
        return new NGAverage2DInitializer(this);
    }

    @Override
    public Cell GetNewCell() {
        ClassicalModel mod = this;
        return new ClassicalCACell(mod);
    }

    @Override
    public String GetDefaultAssociatedTopology() {
        return TopologyCoder.s_FV4;
    }

	private int m_status_rows = 100;
    private int m_stability_cols = 10;
    private int m_MAXSTATE = m_status_rows * m_stability_cols;

    public NGAverage2DModel() {
        super();
    }

    public final int ApplyLocalFunction(OneRegisterIntCell cell) {
        int state = cell.GetState();

        if (state < m_MAXSTATE) { // Only for OK cells
            int neighbourhoodSize = cell.GetNeighbourhoodSize();
            int currentCol = getStateCol(state);
            int currentLine = getStateLine(state);
            int average = 0;

            for (int i = 0; i < neighbourhoodSize; i++) {
                int neighboorState = cell.GetNeighbour(i).GetState();
                int neighboorLine = getStateLine(neighboorState);

                average += neighboorLine;
            }

            average = Math.round((float)average / (float) neighbourhoodSize);
            if (average == currentLine) {
                state = getStateId(Math.min(average, m_status_rows - 1), Math.min(m_stability_cols - 1, currentCol + 1));
            } else {
                state = getStateId(Math.min(average, m_status_rows - 1), 0);
            }

            state = Math.min(state, m_MAXSTATE - 1);
        }

        return state;
    }

    private int getStateCol(int state) {
        return state % m_stability_cols;
    }

    private int getStateLine(int state) {
        return Math.floorDiv(state, m_status_rows);
    }
    private int getStateId(int ns, int nd) {
        return m_status_rows * ns + nd;
    }

    @Override
    public String [] GetPlotterSelection2D() {
        return null;
    }

    @Override
    final public int GetMaxStateValue() {
        return m_MAXSTATE;
    }
    final public int getMaxStabilityCols() { return m_stability_cols; }
    final public int getMaxStatusRows() { return m_status_rows; }

    @Override
    public PaintToolKit GetPalette() {
        PaintToolKit m_palette = PaintToolKit.EmptyPalette();
        m_palette.Set2DGradient(m_stability_cols, m_status_rows);
        return m_palette;
    }

    public FLPanel GetSpecificPanel() {
        return FLPanel.NewPanel();
    }
}
