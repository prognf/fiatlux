package models.CAmodels.decentralisedDiagnosis;

import components.allCells.OneRegisterIntCell;
import components.types.ProbaPar;
import grafix.gfxTypes.elements.FLPanel;
import initializers.SuperInitializer;
import models.CAmodels.binary.BinaryModel;
import topology.basics.TopologyCoder;

/*--------------------
 * Model: applies on various neighb.
 *--------------------*/
public class BiasedQuiescentModelGeneric extends BinaryModel {

	public static final String NAME = "BQ-generic";
	final static int ZERO=0, ONE=1;
	
	ProbaPar m_p= new ProbaPar("p:", 0.2);
	
	public BiasedQuiescentModelGeneric() {
		super();
	}

	public final int ApplyLocalFunction(OneRegisterIntCell inCell){
		int q= inCell.GetState();
		if (q==QuorumDModel.D) // error state
			return QuorumDModel.D;
		
		int 	n0= inCell.CountOccurenceOf(QuorumDModel.N), 
				n1= inCell.CountOccurenceOf(QuorumDModel.A)+inCell.CountOccurenceOf(QuorumDModel.D);
		if (n1 ==0)	
			return QuorumDModel.N;
		else if (n0==0) 
			return QuorumDModel.A;
		else {
			return RandomBernoulli(m_p); //A=1 ; N==0
		}
	}
	
	@Override
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TV5;
	}
	
	@Override
	public SuperInitializer GetDefaultInitializer() {
		return new QuorumDInit();
	}
	
	@Override
	public FLPanel GetSpecificPanel(){
		FLPanel p = FLPanel.NewPanel();
		p.Add(m_p.GetControl(), this.GetRandomizer().GetSeedControl());
		return p;
	}

	public void setPval(double val) {
		m_p.SetVal(val);
	}
}

