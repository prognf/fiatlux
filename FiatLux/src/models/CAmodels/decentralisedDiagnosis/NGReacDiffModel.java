package models.CAmodels.decentralisedDiagnosis;

import components.allCells.Cell;
import components.allCells.ClassicalCACell;
import components.allCells.OneRegisterIntCell;
import components.types.ProbaPar;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.elements.FLPanel;
import initializers.SuperInitializer;
import main.tables.PlotterSelectControl;
import models.CAmodels.ClassicalModel;
import topology.basics.TopologyCoder;

/**
 * Another version of the ReacDiff (Greenberg-Hastings) model, but with failures
 * @author Nicolas GAUVILLE
 */
public class NGReacDiffModel extends ClassicalModel {
	final public static String NAME="NG-Reac-Diff";
	final static int LIM = 1;

	/**
	 * States
	 */
	public final static int NORMAL = 0, REFRACTORY = 1, FAILURE = 2, EXCITED = 3;

	public SuperInitializer GetDefaultInitializer() {
		return new NGReacDiffInitializer(this);
	}

	@Override
	public Cell GetNewCell() { return new ClassicalCACell(this); }

	@Override
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_FV4;
	}

	protected int m_MAXSTATE = 3;
	private ProbaPar m_pFailure;
	private ProbaPar m_pRepair;
	private ProbaPar m_pExcitation;

	public NGReacDiffModel() {
		super();
		m_pFailure = new ProbaPar("pFailure", 0.0);
		m_pRepair = new ProbaPar("pRepair", 0.0);
		m_pExcitation = new ProbaPar("pExcitation", 1.0);
	}

	public void setPExcitation(double p) {
		m_pExcitation.SetVal(p);
	}

	public final int ApplyLocalFunction(OneRegisterIntCell cell) {
		int state = cell.GetState();

		if (state != FAILURE && RandomEventDouble(m_pFailure)) {
			return FAILURE;
		} else if (state == FAILURE) {
			if (RandomEventDouble(m_pRepair)) {
				return NORMAL;
			}
			return FAILURE;
		}

		if (state == NORMAL) {
			return (cell.CountOccurenceOf(EXCITED) > LIM - 1 && RandomEventDouble(m_pExcitation)) ? EXCITED : NORMAL;
		} else if (state == EXCITED) {
			return REFRACTORY;
		}

		return NORMAL;
	}

	@Override
	final public int GetMaxStateValue() {
		return m_MAXSTATE;
	}

	@Override
	public String [] GetPlotterSelection2D() {
		//Macro.Debug("plotttter!");
		return PlotterSelectControl.REACDIFFSELECT;
	}

	@Override
	public PaintToolKit GetPalette() {
		PaintToolKit palette = PaintToolKit.EmptyPalette();
		palette.AddColor(FLColor.c_white);
		palette.AddColor(FLColor.c_brown);
		palette.AddColor(FLColor.c_black);
		palette.AddColor(FLColor.c_orange);
		return palette;
	}

	public FLPanel GetSpecificPanel() {
		FLPanel p = FLPanel.NewPanel(
			m_pFailure.GetControl(),
			m_pRepair.GetControl(),
			m_pExcitation.GetControl()
		);
		p.setOpaque(false);
		return p;
	}
}
