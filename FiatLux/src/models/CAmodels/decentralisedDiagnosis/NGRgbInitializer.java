package models.CAmodels.decentralisedDiagnosis;

import grafix.gfxTypes.DoubleField;
import grafix.gfxTypes.elements.FLPanel;
import initializers.CAinit.OneRegisterInitializer;

/**
 * @author Nicolas Gauville
 * Initializer pour le modèle RGB (distribution aléatoire équiprobable R-G-B + cellules défaillantes)
 */
public class NGRgbInitializer extends OneRegisterInitializer {
    private static final int DEF_KO_CELLS = 0;

    DoubleField mF_KoNum = new DoubleField("% ko cells:", 3, DEF_KO_CELLS);

    public void SubInit() {
        for (int i = 0; i < GetLsize(); i++) {
            InitState(i, RandomEventDouble(1./3.) ? 0 : (RandomEventDouble(0.5) ? 1 : 2));
        }

        InitStateKRandom((int) (mF_KoNum.GetValue() * (double) GetSize()), 3);
    }

    public void setKoCells(double koCells) {
        mF_KoNum.SetDouble(koCells);
    }

    public FLPanel GetSpecificPanel() {
        FLPanel p = FLPanel.NewPanel(mF_KoNum);
        FLPanel Control = FLPanel.NewPanel(p, GetRandomizer());
        Control.setOpaque(false);
        p.setOpaque(false);
        return Control;
    }
}
