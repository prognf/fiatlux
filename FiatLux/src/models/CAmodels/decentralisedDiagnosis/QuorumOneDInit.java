package models.CAmodels.decentralisedDiagnosis;

import components.types.FLString;
import grafix.gfxTypes.elements.FLPanel;
import grafix.gfxTypes.elements.FLTextField;
import initializers.CAinit.OneRegisterInitializer;

public class QuorumOneDInit extends OneRegisterInitializer {

	protected String m_input="L10";
	
	InitParser m_initParser= new InitParser();
	
	public FLPanel GetSpecificPanel(){
		FLPanel p = FLPanel.NewPanel();
		p.AddGridBagY(m_initParser); 
		return p;
	}

	@Override
	protected void SubInit() {
		oneSeed();
	}

	
	private void oneSeed() {
		InitState(0, QuorumOneDLacaModel.D);
		int L= FLString.IParseWithControl(m_input, "L");
		for (int pos=1; pos<L; pos++ ){
				InitState(pos, QuorumOneDLacaModel.A );
		}
	}

	public void setInput(String input) {
		m_input = input;
	}
		
	class InitParser extends FLTextField {

		protected InitParser() {
			super(5);
		}

		@Override
		public void parseInput(String input) {
			if (input.length()>0){
				setInput(input);
			}
		}

	}	

	
}
