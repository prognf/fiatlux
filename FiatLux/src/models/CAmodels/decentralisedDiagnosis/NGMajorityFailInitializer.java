package models.CAmodels.decentralisedDiagnosis;

import grafix.gfxTypes.DoubleField;
import grafix.gfxTypes.elements.FLPanel;
import initializers.CAinit.OneRegisterInitializer;

/**
 * @author Nicolas Gauville
 */
public class NGMajorityFailInitializer extends OneRegisterInitializer {
    DoubleField mF_KoNum = new DoubleField("% ko cells:", 3, 0.1);
    DoubleField mF_AlNum = new DoubleField("% alive cells:", 3, 0.5);

    public NGMajorityFailInitializer() {
        super();
    }

    public NGMajorityFailInitializer(double ko, double alive) {
        super();
        mF_KoNum.SetDouble(ko);
        mF_AlNum.SetDouble(alive);
    }

    public void SubInit() {
      //initUniform();
    	initSquare();
    }

    private void initSquare() {
    	int L= GetXsize() / 3;
		for (int x=0; x < L; x++){
			for (int y=0; y < L ; y++){
				InitStateXY(L+x, L+
						y, NGMajorityFailModel.ONE);
			}
		}
	}

	private void initUniform() {
    	   double aliveProbability = mF_AlNum.GetValue();

           for (int i = 0; i < GetLsize(); i++) {
               InitState(i, RandomEventDouble(aliveProbability) ? NGMajorityFailModel.ONE : NGMajorityFailModel.ZERO);
           }

           InitStateKRandom((int) (mF_KoNum.GetValue() * (double) GetSize()), NGMajorityFailModel.FAIL);
		
	}

	public void setProportions(double prctAlive, double prctFail) {
        mF_AlNum.SetDouble(prctAlive);
        mF_KoNum.SetDouble(prctFail);
    }

    public FLPanel GetSpecificPanel() {
        FLPanel p = FLPanel.NewPanelVertical(mF_AlNum, mF_KoNum);
        FLPanel Control = FLPanel.NewPanel(p, GetRandomizer());
        p.setOpaque(false);
        Control.setOpaque(false);
        return Control;
    }
}
