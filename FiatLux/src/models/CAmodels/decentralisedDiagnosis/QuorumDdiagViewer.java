package models.CAmodels.decentralisedDiagnosis;

import components.allCells.OneRegisterIntCell;
import components.arrays.RegularDynArray;
import components.types.IntC;
import grafix.viewers.OneRegisterAutomatonViewer;

public class QuorumDdiagViewer extends OneRegisterAutomatonViewer {

	public static final int NCOLS = 64;


	public QuorumDdiagViewer(IntC in_CellSize, IntC in_GridSize, RegularDynArray in_Automaton) {
		super(in_CellSize, in_GridSize, in_Automaton);
	}
	
	
	//@Override
	public int zGetCellColorNumXY(int x, int y) {
		//int x2=(x-y+GetXsize())%GetXsize(), y2= y; 
		int x2=x, y2= y; 
		
		OneRegisterIntCell cell = GetCell(x2,y2);
		int state = cell.GetState();
		if (state == 2){
			return 1;
		}
		if(state == 1) {
			int t= GetTime();
			return 1 + ((x+y+t)% (NCOLS-1)); //  shift to 1
		} else {
			return 0;
		}
	}

	public int GetCellColorNumXY(int x, int y) {
		return GetCell(x, y).GetState();
	}
	
}
