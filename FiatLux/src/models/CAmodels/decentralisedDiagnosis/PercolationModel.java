package models.CAmodels.decentralisedDiagnosis;

import components.allCells.OneRegisterIntCell;
import grafix.gfxTypes.controllers.DoubleController;
import grafix.gfxTypes.elements.FLPanel;
import models.CAmodels.binary.BinaryModel;
import topology.basics.TopologyCoder;
import updatingScheme.FullyAsynchronousScheme;
import updatingScheme.UpdatingScheme;

/*--------------------
 * Ising Model...
 * 
*--------------------*/
public class PercolationModel extends BinaryModel  {

	public final static String NAME = "Perco2";
	
	private static final String S_LABEL = "proba p";
	final static int ZERO=0, ONE=1;
	
	
	public PercolationModel() {
		super();
	}

	@Override
	public UpdatingScheme GetDefaultUpdatingScheme() {
		return new FullyAsynchronousScheme();
	}
	
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TLR1;
	}


	public final int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
		int n0= in_Cell.CountOccurenceOf(ZERO);
		int n1= in_Cell.CountOccurenceOf(ONE);
		//Macro.Debug(n0+"::"+n1+"||");
		if (n1==0)
			return ZERO;
		if (n0==1)
			return RandomEventDouble(m_probaP)?ZERO:ONE;
		if (n0==0)
			return RandomEventDouble(m_probaP)?ONE:ZERO;
		return -1;
	}
	
	

	/*--------------------
	 *	 Noise Control
	 **********************************************************/

	double m_probaP = 0.5;
	class RateControl extends DoubleController {
		

		public RateControl() {
			super(S_LABEL);			
		}

		@Override
		public double ReadDouble() {
			return m_probaP;
		}

		@Override
		public void SetDouble(double val) {
			m_probaP= val;
		}
	}
	
	
	
	public FLPanel GetSpecificPanel() {
		return new RateControl();
	}
	
	
}
