package models.CAmodels.decentralisedDiagnosis;

import components.allCells.OneRegisterIntCell;
import grafix.gfxTypes.controllers.DoubleController;
import grafix.gfxTypes.elements.FLPanel;
import models.CAmodels.binary.BinaryModel;
import topology.basics.TopologyCoder;
import updatingScheme.CheckerboardScheme;
import updatingScheme.UpdatingScheme;

/*--------------------
 * synchronisation problem
 * @author Nazim Fates 
 *****--------------------*/
public class Q2Rmodel extends BinaryModel {


	public static final String NAME = "Q2R";
	public static final String TXT = "proba to flip when eq.";
	private static final double DEF_PVAL = 1.0;
	/*--------------------
	 * Attributes
	 --------------------*/

	double m_pShift = DEF_PVAL;

	/*--------------------
	 * overrides & implementations
	 --------------------*/
	@Override
	public UpdatingScheme GetDefaultUpdatingScheme(){
		return new CheckerboardScheme();
	}

	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TV4;			
	}	

	/** MAIN FUNCTION  **/
	final public int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
		int state= in_Cell.GetState();
		int
		zero = in_Cell.CountOccurenceOf(0),
		one = in_Cell.CountOccurenceOf(1);
		if (zero==one){
			boolean flipCoin = RandomEventDouble(m_pShift);
			return flipCoin ? 1-state : state;
		} else {
			return state;
		}
	}




	/* overloading this method */
	public FLPanel GetSpecificPanel() {
		return new p_ShiftControl();
	}
	/*--------------------
	 * Constructor
	 --------------------*/

	public Q2Rmodel() {
	}

	/*--------------------
	 * Get / set
	 --------------------*/

	public void SetpShift(double in_val){
		m_pShift= in_val;
	}


	/*--------------------
	 * GFX
	 --------------------*/

	class p_ShiftControl extends DoubleController{

		public p_ShiftControl() {
			super(TXT);
		}

		@Override
		protected double ReadDouble() {
			return m_pShift;
		}

		@Override
		protected void SetDouble(double val) {
			SetpShift(val);		
		}

	}

}
