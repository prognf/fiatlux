package models.CAmodels.decentralisedDiagnosis;

import components.allCells.OneRegisterIntCell;
import components.types.ProbaPar;
import grafix.gfxTypes.elements.FLPanel;
import initializers.SuperInitializer;
import models.CAmodels.binary.BinaryModel;
import topology.basics.TopologyCoder;

/*--------------------
 * rech on Quorum D 
 *--------------------*/
public class QuorumOneDLacaModel extends BinaryModel  {

	public final static String NAME = "QuorumOneDLaca";
	
	final static int N=0 /* N : neutral */, A= 1 /* A : alert*/, D=2 /* default*/;
	
	ProbaPar m_probaG = new ProbaPar("gamma", 0.2);
	ProbaPar m_probaB = new ProbaPar("beta", 0.9);
	
	public QuorumOneDLacaModel() {
		super();
	}

	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TLR1;
	}
	
	public SuperInitializer GetDefaultInitializer() {
		return new QuorumOneDInit();
	}
	
	public final int ApplyLocalFunction(OneRegisterIntCell cell) {
		return QuorumFrontLaca(cell);
		//return RegularQuorumFront(cell);
	}
	
	

	/*--------------------
	 *	 Noise Control
	**********************************************************/
	
	private int QuorumFrontLaca(OneRegisterIntCell cell) {
		int 
		x= cell.ReadNeighbourState(0), 
		y= cell.ReadNeighbourState(1),
		z= cell.ReadNeighbourState(2);
		if (y==D)
			return D;
		if (x==D)
			return RandomEventDouble(m_probaG)?A:N;
		if (y==N) {
			if (x==A)
				return  RandomEventDouble(m_probaB)?A:N;
			else 
				return N;
		} 
		if (y==A) {
			return x;
		}
		return -1;
	}
	
	public FLPanel GetSpecificPanel(){
		FLPanel p= FLPanel.NewPanel();
		p.Add( m_probaG, m_probaB );
		return p;
	}
	
	/** override if needed */
	/*public PaintToolKit GetPalette(){
		PaintToolKit p = PaintToolKit.GetRandomPalette(QuorumDdiagViewer.NCOLS); 
		p.SetColor(0, FLColor.c_white);
		return p;
	}*/
	
	
	//@Override
	/** specific viewer */
	/*public AutomatonViewer GetAutomatonViewer(
			IntC cellPixSize, 
			SuperTopology topo, RegularDynArray automaton) {	
		 PlanarTopology topo2D = (PlanarTopology) topo;
		AutomatonViewer av= new QuorumDdiagViewer(cellPixSize, topo2D.GetXYsize(), automaton);
		av.SetPalette(GetPalette());
		return av;
	}*/
	

}
