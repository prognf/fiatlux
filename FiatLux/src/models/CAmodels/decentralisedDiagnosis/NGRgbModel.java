package models.CAmodels.decentralisedDiagnosis;

import components.allCells.Cell;
import components.allCells.ClassicalCACell;
import components.allCells.OneRegisterIntCell;
import components.types.ProbaPar;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.controllers.IntController;
import grafix.gfxTypes.elements.FLPanel;
import initializers.SuperInitializer;
import main.tables.PlotterSelectControl;
import models.CAmodels.ClassicalModel;
import topology.basics.TopologyCoder;

/**
 * @author Nicolas Gauville
 * Modèle RGB-GD
 */
public class NGRgbModel extends ClassicalModel {
    final public static String NAME = "RGB";
    public SuperInitializer GetDefaultInitializer() {
        return new NGRgbInitializer();
    }

    public static final int R = 0;
    public static final int G = 1;
    public static final int B = 2;
    public static final int FAILURE = 3;

    private int p_a = 3;
    private int p_b = 0;
    private boolean p_asymetric_failure_type = true;

    private ProbaPar m_pFailure;
    private ProbaPar m_pRepair;

    private IntController m_cA = new AControl();
    private IntController m_cB = new BControl();

    public NGRgbModel() {
        super();

        m_pFailure = new ProbaPar("pFailure", 0.0);
        m_pRepair = new ProbaPar("pRepair", 0.0);
    }

    @Override
    public Cell GetNewCell() {
        ClassicalModel mod = this;
        return new ClassicalCACell(mod);
    }

    @Override
    public String GetDefaultAssociatedTopology() {
        return TopologyCoder.s_TM8;
    }

    public void setParameters(int a, int b, boolean asymetric) {
        p_a = a;
        p_b = b;
        p_asymetric_failure_type = asymetric;
    }

    public void setParameters(int a, int b, double pFail) {
        p_a = a;
        p_b = b;
        m_pFailure.SetVal(pFail);
    }

    public void setParameters(int a, int b) {
        setParameters(a, b, false);
    }

    public final int ApplyLocalFunction(OneRegisterIntCell cell) {
        int state = cell.GetState();

        if (state != FAILURE && RandomEventDouble(m_pFailure)) {
            return FAILURE;
        } else if (state == FAILURE) {
            if (RandomEventDouble(m_pRepair)) {
                if (p_asymetric_failure_type) {
                    return B;
                }
                return RandomEventDouble(1./3.) ? R : (RandomEventDouble(0.5) ? G : B);
            }
            return FAILURE;
        }

        int nextA = 0;
        int nextB = 0;

        switch(state) {
            case R:
                nextA = G;
                nextB = B;
                break;
            case G:
                nextA = B;
                nextB = R;
                break;
            case B:
                nextA = R;
                nextB = G;
                break;
        }

        int countA = cell.CountOccurenceOf(nextA);
        int countB = cell.CountOccurenceOf(nextB);

        if (p_asymetric_failure_type) {
            countA = Math.max(0, countA - 2 * cell.CountOccurenceOf(FAILURE));
            countB = Math.max(0, countB - 2 * cell.CountOccurenceOf(FAILURE));
        }

        if (countA > p_a) {
            return nextA;
        } else if (countB > p_b) {
            return nextB;
        }

        return state;
    }

    @Override
    public String [] GetPlotterSelection2D() {
        return PlotterSelectControl.RGB_SELECT;
    }

    @Override
    final public int GetMaxStateValue() {
        return 3;
    }

    @Override
    public PaintToolKit GetPalette() {
        PaintToolKit palette = PaintToolKit.EmptyPalette();
        palette.AddColorRGB(200, 0, 0);
        palette.AddColorRGB(0, 200, 0);
        palette.AddColorRGB(0, 0, 200);
        palette.AddColorRGB(0, 0, 0);
        return palette;
    }

    public FLPanel GetSpecificPanel() {
        m_cA.minimize();
        m_cB.minimize();
        FLPanel p = FLPanel.NewPanel(m_pFailure.GetControl(), m_pRepair.GetControl(), m_cA, m_cB);
        p.setOpaque(false);
        return p;
    }

    class AControl extends IntController {
        protected AControl() {
            super("A", 8);
        }
        protected int ReadInt() {
            return p_a;
        }
        protected void SetInt(int val) {
            setParameters(val, p_b);
        }
    }

    class BControl extends IntController {
        protected BControl() {
            super("B", 8);
        }
        protected int ReadInt() {
            return p_b;
        }
        protected void SetInt(int val) {
            setParameters(p_a, val);
        }
    }
}
