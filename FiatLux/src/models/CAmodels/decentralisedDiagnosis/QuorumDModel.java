package models.CAmodels.decentralisedDiagnosis;

import components.allCells.OneRegisterIntCell;
import components.types.DoublePar;
import grafix.gfxTypes.elements.FLPanel;
import initializers.SuperInitializer;
import main.Macro;
import models.CAmodels.ClassicalModel;
import topology.basics.TopologyCoder;

/*--------------------
 * choose one neighbour at random and copy its state
 *--------------------*/

public class QuorumDModel extends ClassicalModel {

	public static final String NAME = "QuorumD";

	public final static int N = 0, A = 1, D = 2;
	private DoublePar m_lambda= new DoublePar("lambda", 1.);

	/*--------------------
	 * Attributes
	 --------------------*/

	/*--------------------
	 * Constructor
	 --------------------*/
	public QuorumDModel() {
		super();
	}


	/*--------------------
	 * overrides & implementations
	 --------------------*/

	@Override
	public int ApplyLocalFunction(OneRegisterIntCell cell) {
		return transitionFunction(cell);
	}

	/** first local transition function */
	public final int transitionFunction(OneRegisterIntCell cell) {

		double lambda=m_lambda.GetVal();// as defined in the FMM paper
		// double lambdaR= lambdaG  / cell.GetNeighbourhoodSize();//rescaled
		// counting the number of each cell : SLOW!
		int nN = Count(cell, N); 
		int nD = Count(cell, D);
		int nA = Count(cell, A);// can be improved
		int q= cell.GetState();
		if (q == D) {
			return D; // invariant state
		} else if (nA == 0 && nD == 0) {
			return N; // only N cells
		} else if (nN == 0) {
			return A; // only alert & defect in neighb.
		} else {
			double wA = Math.exp(lambda*(nA+nD)), wN= Math.exp(lambda*nN);
			double pA= wA/(wA+wN);
			//Macro.fDebug("wA:%.3f wN:%.3f pA:%.3f",wA,wN,pA);
			return RandomEventDouble(pA) ? A : N; // w.p. pA : A, otherwise N
		} 
	}
	/** testing another Local transition function */
	public final int testTransitionFunction(OneRegisterIntCell cell) {

		// counting the number of each cell : SLOW!
		int nN = Count(cell, N); 
		int nD = Count(cell, D);
		int nA = Count(cell, A);// can be improved
		Macro.fPrint("%d %d %d", nN, nD, nA);
		int q= cell.GetState();
		if (q == D) {
			return D; // invariant state
		} 
		if (nN<nD+nA) {
			return A;
		} else if (nD+nA<nD) {
			return N;
		} else {
			return RandomEventDouble(1/2) ? A : N; // w.p. pA : A, otherwise N
		} 
	}


	
	/** be careful : should the cell count itself ??? ***/
	private int Count(OneRegisterIntCell cell, int qTarget) {
		int count=0;
		//int qCell= cell.GetState();
		int NbSz= cell.GetNeighbourhoodSize();
		for (int pos=0; pos < NbSz; pos++){
			int qNeighb= cell.ReadNeighbourState(pos);
			if (qNeighb == qTarget){
				count++;
			}
		}
		/*if (qCell==qTarget){
			count--;
		}*/
		return count;
	}

	@Override
	public SuperInitializer GetDefaultInitializer() {
		return new QuorumDInit();
	}

	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TV5;
	}


	/*--------------------
	 *  Get & Set methods
	 --------------------*/
	
	
	public int GetStatesNum() {
		return 3;
	}


	public double getLambda() {
		return m_lambda.GetVal();
	}


	public void setLambda(double lambda) {
		this.m_lambda.SetVal(lambda);
	}


	public FLPanel GetSpecificPanel(){
		FLPanel p= FLPanel.NewPanel();
		p.Add(m_lambda);
		p.Add( this.GetRandomizer().GetSeedControl() );
		return p;
	}

	/*--------------------
	 *  Other methods
	 --------------------*/
}
