package models.CAmodels.decentralisedDiagnosis;

import components.allCells.OneRegisterIntCell;
import components.types.ProbaPar;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.elements.FLPanel;
import initializers.SuperInitializer;
import initializers.CAinit.BinaryInitializer;
import models.CAmodels.ClassicalModel;
import topology.basics.TopologyCoder;

/**
 * @author Nicolas Gauville
 * Larger Than Life Model
 */
public class NGLTLifeModel extends ClassicalModel {
	final static int DEADCELL = 0, LIVINGCELL = 1, FAILURE = 2;
	public static final String NAME = "LT-Life";

	private ProbaPar m_pFailure;
	private ProbaPar m_pRepair;

	// 6-37 49-81
	// 56-69 20-76
	// 11-25_28-44
	// 6-63_3-9
	// 72-111_23-118
	// 48-71_11-61

	// Paquets
	private int _bornMin = 34;
	private int _bornMax = 45;
	private int _surviveMin = 34;
	private int _surviveMax = 58;

	// MooreR3 :
	// - 15-28 7-26 vagues / clignotant avec défaillance
	// - 18-37 4-29 idem, avec objets grandissant au début (a comparer ...)

	// MooreR7
	// 6-37 49-81 fluides clignotants
	// 127-188_77-193

	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TMR5;
	}

	public NGLTLifeModel() {
		super();

		m_pFailure = new ProbaPar("pFailure", 0.0);
		m_pRepair = new ProbaPar("pRepair", 0.0);
	}

	public void setRules(int bornMin, int bornMax, int surviveMin, int surviveMax) {
		_bornMin = bornMin;
		_bornMax = bornMax;
		_surviveMin = surviveMin;
		_surviveMax = surviveMax;
	}

	public final int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
		int nAlive = in_Cell.CountOccurenceOf(LIVINGCELL) + in_Cell.CountOccurenceOf(FAILURE);
		int predstate = in_Cell.GetState();

		if (predstate != FAILURE && RandomEventDouble(m_pFailure)) {
			return FAILURE;
		} else if (predstate == FAILURE) {
			if (RandomEventDouble(m_pRepair)) {
				return RandomEventDouble(0.5) ? DEADCELL : LIVINGCELL;
			}
			return FAILURE;
		}

		if (predstate == DEADCELL && (nAlive >= _bornMin && nAlive <= _bornMax)) {
			return LIVINGCELL;
		} else if (predstate == LIVINGCELL && !(nAlive >= _surviveMin && nAlive <= _surviveMax)) {
			return DEADCELL;
		}

		return predstate;
	}

	public SuperInitializer GetDefaultInitializer() {
		return new BinaryInitializer();
	}

	@Override
	public PaintToolKit GetPalette() {
		PaintToolKit palette = PaintToolKit.EmptyPalette();
		palette.AddColor(FLColor.c_white);
		palette.AddColor(FLColor.c_blue);
		palette.AddColor(FLColor.c_black);
		return palette;
	}

	public FLPanel GetSpecificPanel() {
		FLPanel r = FLPanel.NewPanel(m_pFailure.GetControl(), m_pRepair.GetControl());
		r.setOpaque(false);
		return r;
	}
}
