package models.CAmodels.decentralisedDiagnosis;

import grafix.gfxTypes.DoubleField;
import grafix.gfxTypes.elements.FLPanel;
import initializers.CAinit.OneRegisterInitializer;
import models.CAmodels.nState.IsingStab;

/**
 * @author Nicolas Gauville
 */
public class NGIsingStabInitializer extends OneRegisterInitializer {
    DoubleField mF_KoNum = new DoubleField("% ko cells:", 3, 0.0);

    public NGIsingStabInitializer() {
        super();
    }

    public NGIsingStabInitializer(double ko) {
        super();
        mF_KoNum.SetDouble(ko);
    }

    public void SubInit() {
        for (int i = 0; i < GetLsize(); i++) {
            InitState(i, IsingStab.NORMAL);
        }

        InitStateKRandom((int) (mF_KoNum.GetValue() * (double) GetSize()), IsingStab.FAIL);
    }

    public void setProportions(double prctAlive, double prctFail) {
        mF_KoNum.SetDouble(prctFail);
    }

    public FLPanel GetSpecificPanel() {
        FLPanel p = FLPanel.NewPanelVertical(mF_KoNum);
        FLPanel Control = FLPanel.NewPanel(p, GetRandomizer());
        p.setOpaque(false);
        Control.setOpaque(false);
        return Control;
    }
}
