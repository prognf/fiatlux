package models.CAmodels.decentralisedDiagnosis;

import components.allCells.Cell;
import components.allCells.ClassicalCACell;
import components.allCells.OneRegisterIntCell;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.elements.FLPanel;
import initializers.SuperInitializer;
import main.tables.PlotterSelectControl;
import models.CAmodels.ClassicalModel;
import topology.basics.TopologyCoder;

/**
 * @author Nicolas Gauville
 * Modèle de vagues qui s'accumulent inspiré de Greenberg-Hastings
 */
public class NGRDCModel extends ClassicalModel {
    final public static String NAME = "AccumulatingWaves";
    final public static int NB_EXCITED = 5;

    final public static int S_FAILING = 2 * NB_EXCITED + 2;
    final public static int S_ALERT = 2 * NB_EXCITED + 1;
    final public static int INCREASE_EXCITATION = 5;

    public SuperInitializer GetDefaultInitializer() {
        return new NGRDCInitializer(this);
    }

    @Override
    public Cell GetNewCell() {
        ClassicalModel mod = this;
        return new ClassicalCACell(mod);
    }

    @Override
    public String GetDefaultAssociatedTopology() {
        return TopologyCoder.s_TM8;
    }

    private int m_MAXSTATE = 2 * NB_EXCITED + 2;

    public NGRDCModel() {
        super();
    }

    public final int ApplyLocalFunction(OneRegisterIntCell cell) {
        int state = cell.GetState();

        if (state == S_ALERT || state == S_FAILING) {
            // Si la cellule est défaillante ou en état d'alerte, on ne change plus son état
            return state;
        } else {
            int alertCount = cell.CountOccurenceOf(S_ALERT);

            if (alertCount > 0) {
                // L'état d'alerte se propage sur la grille
                return S_ALERT;
            } else {
                if (isExcitedStatus(state)) {
                    int moreExcitedCount = cell.CountOccurenceOf(state + 1);

                    if (moreExcitedCount > 0) {
                        if (!isExcitedStatus(state + 1)) {
                            // Excitation maximale -> passage en état d'alerte
                            return S_ALERT;
                        } else {
                            return state + 1;
                        }
                    } else {
                        int countAsExcited = cell.CountOccurenceOf(state);
                        int countRefractory = cell.CountOccurenceOf(state - NB_EXCITED);
                        if (countAsExcited > INCREASE_EXCITATION - 1 && countRefractory > 2) { // ????
                            if (!isExcitedStatus(state + 1)) {
                                // Excitation maximale -> passage en état d'alerte
                                return S_ALERT;
                            } else {
                                return state + 1;
                            }
                        }
                        // Passage en mode réfractaire
                        return getRefractoryStatus(state);
                    }
                } else if (isRefractoryStatus(state)) {
                    // Cellule refractaire, redeviens active si excitation plus forte dans le voisinage
                    // Sinon retour état normal en une seule itération
                    int moreExcitedCount = cell.CountOccurenceOf(state + NB_EXCITED + 1);

                    if (moreExcitedCount > 0) {
                        if (!isExcitedStatus(state + NB_EXCITED + 1)) {
                            // Excitation maximale -> passage en état d'alerte
                            return S_ALERT;
                        } else {
                            return state + NB_EXCITED + 1;
                        }
                    }
                } else {
                    // Cellule normale, on detecte la présence de cellule défaillante ou excitée dans le voisinage
                    // Détection et transmission des vagues excitatrices
                    for (int s = 2 * NB_EXCITED; s > NB_EXCITED; s--) {
                        int count = cell.CountOccurenceOf(s);
                        if (count > INCREASE_EXCITATION) {
                            return s + 1;
                        } else if (count > 0) {
                            return s;
                        }
                    }

                    int failingCount = cell.CountOccurenceOf(S_FAILING);
                    if (failingCount > 0) {
                        return NB_EXCITED + 1; // Premier état d'excitation
                    }
                }
            }
        }

        return 0;
    }

    @Override
    public String [] GetPlotterSelection2D() {
        return PlotterSelectControl.REACDIFFSELECT;
    }

    @Override
    final public int GetMaxStateValue() {
        return m_MAXSTATE;
    }

    @Override
    public PaintToolKit GetPalette() {
        PaintToolKit m_palette = PaintToolKit.EmptyPalette();
        m_palette.AddColor(FLColor.c_white);

        for (int i = 0; i < NB_EXCITED; i++) {
            m_palette.AddColorRGB((int) (100 + 155 * ((double) (i + 1) / NB_EXCITED)), (int) (50 + 100 * ((double) (i + 1) / NB_EXCITED)), 0);
        }

        for (int i = 0; i < NB_EXCITED; i++) {
            m_palette.AddColorRGB((int) (100 + 155 * ((double) (i + 1) / NB_EXCITED)), 0, 0);
        }

        m_palette.AddColor(FLColor.c_purple);
        m_palette.AddColor(FLColor.c_black);

        m_palette.Print();
        return m_palette;
    }

    public FLPanel GetSpecificPanel() { return FLPanel.NewPanel(); }

    private int getRefractoryStatus(int status) {
        return status - NB_EXCITED;
    }
    private boolean isRefractoryStatus(int status) {
        return status > 0 && status < NB_EXCITED + 1;
    }
    private boolean isExcitedStatus(int status) {
        return status > NB_EXCITED && status < 2 * NB_EXCITED + 1;
    }
}
