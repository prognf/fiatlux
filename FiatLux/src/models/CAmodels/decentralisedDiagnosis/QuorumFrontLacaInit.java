package models.CAmodels.decentralisedDiagnosis;

import components.types.IntPar;
import components.types.ProbaPar;
import grafix.gfxTypes.elements.FLPanel;
import initializers.CAinit.OneRegisterInitializer;
import main.MathMacro;

public class QuorumFrontLacaInit extends OneRegisterInitializer {


	private static final int INTIDEF = 0;
	private static final int LDEF = 20;
	IntPar m_initType = new IntPar("init style", INTIDEF);
	IntPar m_L = new IntPar("Lsquare", LDEF);
	ProbaPar m_dini= new ProbaPar("Kappa", 0.02);


	public FLPanel GetSpecificPanel(){

		FLPanel p = FLPanel.NewPanel();
		p.AddGridBagY(m_initType.GetControl(), m_dini.GetControl(), m_L.GetControl()); 
		//p.Add( this.GetRandomizer().GetSeedControl() );
		return p;
	}
	@Override
	protected void SubInit() {
		switch(m_initType.GetVal()){
		case -1:
			uniformD();
			return;
		case 0:
			oneD();
			return;
		case 1:
			TriangleB();
			return;
		case 2:			
			Triangle();
			return;
		case 3: 
			squareAlert();
			return;
		case 4 : 
			cylinder();
			return;
		case 5:
			half();
			return;
		case 6:
			fourth();
			return;
		default:
			Diamond();
			return;

		}
	}

	private void Diamond() {
		int Xc= 2*GetXsize()/3, Yc= GetYsize()/3;
		int L= m_L.GetVal();
		for (int j=0; j<L; j++) {
			for (int i=0; i<L; i++ ){
				InitStateXY(Xc+i-j, Yc+j, 1);
			}
		}

	}

	private void half() {
		int L= m_L.GetVal();
		for (int j=0; j<L; j++) {
			for (int i=0; i<GetXsize(); i++ ){
				InitStateXY(i, j+GetYsize()/2, 1);
			}
		}
	}

	private void fourth() {
		int L= m_L.GetVal();
		int X= GetXsize(), Y= GetYsize();
		for (int j=0; j<L; j++) {
			for (int i=0; i<L; i++ ){
				int x= X/2+ i-j, y = Y/2+i+j;
				if (x<Y && y<Y) {
					InitStateXY(x,y, 1);
					InitStateXY(x,y-1, 1);
				}
			}
		}
	}

	private void cylinder() {

		int L= m_L.GetVal();
		int width= L/2, Xc= GetXsize() - 1 - L -width, Yc=  GetYsize() - width - 1;
		/*for (int j=0; j<width; j++)
		for (int i=0; i<L-width+j; i++ ){
			 {
				int x= Xc - i + j, y= Yc + i;
				InitStateXY(x, y, 1);
			}
		}*/
		for (int j=0; j<width; j++)
			for (int i=0; i<L; i++ ){
				{
					int x= Xc + i + (j+1)/2, y= Yc - i + j/2;
					InitStateXY(x, y, 1);
				}
			}
	}
	private void Triangle() {
		int Xc= GetXsize()/3, Yc= GetYsize()/3;
		int L= m_L.GetVal();
		for (int i=0; i<L; i++ ){
			for (int j=L-i; j<L; j++) {
				InitStateXY(Xc+i, Yc+j, 1);
			}
		}
	}

	private void TriangleB() {
		int L= m_L.GetVal();
		int Xc= GetXsize()-L-1, Yc= GetYsize() -1 ;
		for (int j=0; j<L; j++) {
			for (int i=0; i<j; i++ ){
				InitStateXY(Xc+i, Yc-j, 1);
			}
		}
	}

	private void Bernoulli() {
		int L= GetLsize();
		for (int i=0; i<L; i++ ){
			int st=QuorumDModel.N;//RandomInt(100)<46?QuorumDModel.A:QuorumDModel.N;
			if (RandomEventDouble(m_dini)){
				st=QuorumDModel.D;
			}
			InitState(i, st);
		}
	}


	private void oneSeed() {
		int X= GetXsize();
		int Y= GetYsize();
		int S= m_L.GetVal();
		for (int y=Y/2-S; y<Y/2+S; y++ ){
			for (int x=X/2-S;x<X/2+S;x++){
				InitStateXY(x, y,QuorumDModel.A);
			}
		}
		InitStateXY(X/2, Y/2,QuorumDModel.D);
	}

	private void squareFrontierD() {
		int X= GetXsize(), Xc= X/2;
		int Y= GetYsize(), Yc= Y/2;
		int L= m_L.GetVal();
		for (int i=0; i<L; i++ ){
			InitStateXY(Xc+i, Yc, QuorumDModel.D);
			InitStateXY(Xc, Yc+i, QuorumDModel.D);
			InitStateXY(Xc+L, Yc+i, QuorumDModel.D);
			InitStateXY(Xc+i+1, Yc+L, QuorumDModel.D);
		}
	}

	private void squareAlert() {
		int X= GetXsize(), Xc= X/2;
		int Y= GetYsize(), Yc= Y/2;
		int L= m_L.GetVal();
		for (int y=0; y<L; y++ ){
			for (int x=0; x<L; x++ ){
				InitStateXY(Xc+x, Yc+y, QuorumDModel.A);
			}
		}
	}

	private void oneD() {
		int X= GetXsize(), Y= GetYsize();
		InitStateXY(X-2, Y-2, QuorumDModel.D);
	}
	
	private void uniformD() {
		int X= GetXsize();
		int Y= GetYsize();
		for (int y=0; y<Y; y++ ){
			for (int x=0; x<X; x++ ){
				int q= m_dini.Bernoulli(this.GetRandomizer())?QuorumDModel.D:QuorumDModel.N;
				InitStateXY(x, y, q);
			}
		}
	}

	public void setInitVal(int initval) {
		m_initType.SetVal(initval);
	}

	/** default density in percent **/
	public void setKappa(int kappaPercent) {
		m_dini.SetVal(kappaPercent/MathMacro.HUNDRED);
	}
	
}
