package models.CAmodels.decentralisedDiagnosis;

import components.types.IntPar;
import components.types.ProbaPar;
import grafix.gfxTypes.elements.FLPanel;
import initializers.CAinit.OneRegisterInitializer;

public class QuorumDInit extends OneRegisterInitializer {


	IntPar m_initType = new IntPar("init style", 0);
	IntPar m_L = new IntPar("Lalert-sq", 0);
	ProbaPar m_dini= new ProbaPar("dFailure", 0.1);


	public FLPanel GetSpecificPanel(){

		FLPanel p = FLPanel.NewPanel();
		p.AddGridBagY(m_initType.GetControl(), m_dini.GetControl(), m_L.GetControl()); 
		//Add(m_L);
		//p.Add(m_initType);
		//p.Add( this.GetRandomizer().GetSeedControl() );
		return p;
	}
	@Override
	protected void SubInit() {
		switch(m_initType.GetVal()){
		case 1:
			oneSeed();
			return;
		case 2:			
			squareFrontierD();
			return;
		case 3: 
			squareAlert();
			return;
		default:
			Bernoulli();
			return;
		}
	}

	private void Bernoulli() {
		int L= GetLsize();
		for (int i=0; i<L; i++ ){
			int st=QuorumDModel.N;//RandomInt(100)<46?QuorumDModel.A:QuorumDModel.N;
			if (RandomEventDouble(m_dini)){
				st=QuorumDModel.D;
			}
			InitState(i, st);
		}
	}


	private void oneSeed() {
		int X= GetXsize(), Y= GetYsize();
		int CX= 3*X/4, CY=3*Y/4;
		int SQL= m_L.GetVal();
		InitStateXY(CX, CY, QuorumDModel.D);
		// alert square
		for (int y=-SQL; y<+SQL; y++ ){
			for (int x=-SQL;x<+SQL;x++){
				InitStateXY(CX+x, CY+y, QuorumDModel.A);
			}
		}
		
	}

	private void squareFrontierD() {
		int X= GetXsize(), Xc= X/2;
		int Y= GetYsize(), Yc= Y/2;
		int L= m_L.GetVal();
		for (int i=0; i<L; i++ ){
			/*InitStateXY(Xc+i, Yc, QuorumDModel.D);
			InitStateXY(Xc, Yc+i, QuorumDModel.D);
			InitStateXY(Xc+L, Yc+i, QuorumDModel.D);
			InitStateXY(Xc+i+1, Yc+L, QuorumDModel.D);*/
			InitStateXY(Xc+i, Yc+i, QuorumDModel.D);
		}
		InitStateXY(Xc+L-1, Yc, QuorumDModel.D);
		InitStateXY(Xc, Yc+L-1, QuorumDModel.D);
	}

	private void squareAlert() {
		int X= GetXsize(), Xc= X/2;
		int Y= GetYsize(), Yc= Y/2;
		int L= m_L.GetVal();
		for (int y=0; y<L; y++ ){
			for (int x=0; x<L; x++ ){
				InitStateXY(Xc+x, Yc+y, QuorumDModel.A);
			}
		}
		InitStateXY(Xc,Yc,QuorumDModel.D);
		InitStateXY(Xc+L-1,Yc,QuorumDModel.D);
		InitStateXY(Xc,Yc+L-1,QuorumDModel.D);
		InitStateXY(Xc+L-1,Yc+L-1,QuorumDModel.D);
	}

	
	public double getDfailure() {
		return m_dini.GetVal();
	}


	public void setDfailure(double dFailure) {
		m_dini.SetVal(dFailure);
	}
	
	/** selects the type of init **/
	public void setInitType(int initval) {
		m_initType.SetVal(initval);
	}
	
}
