package models.CAmodels.decentralisedDiagnosis;

import grafix.gfxTypes.DoubleField;
import grafix.gfxTypes.elements.FLPanel;
import initializers.CAinit.OneRegisterInitializer;
import models.CAmodels.typing.IMaxStateAwareModel;

/**
 * @author Nicolas Gauville
 * Initializer for NGAverage2DModel
 */
public class NGAverage2DInitializer extends OneRegisterInitializer {
    private static final double DEF_KO_CELLS = 0.1;

    private DoubleField mF_KoNum = new DoubleField("% ko cells:", 3, DEF_KO_CELLS);
    private IMaxStateAwareModel m_model;

    public NGAverage2DInitializer(IMaxStateAwareModel in_model) {
        m_model = in_model;
    }

    public void SubInit() {
        int maxstate = m_model.GetMaxStateValue();

        InitStateKRandom((int) (mF_KoNum.GetValue() * (double) GetLsize()), maxstate);
    }

    public FLPanel GetSpecificPanel() {
        FLPanel p = FLPanel.NewPanel(mF_KoNum);
        FLPanel Control = FLPanel.NewPanel(p, GetRandomizer());
        p.setOpaque(false);
        Control.setOpaque(false);
        return Control;
    }
}
