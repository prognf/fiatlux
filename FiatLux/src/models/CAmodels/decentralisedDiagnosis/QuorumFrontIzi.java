package models.CAmodels.decentralisedDiagnosis;

import components.allCells.OneRegisterIntCell;
import components.arrays.RegularDynArray;
import components.types.IntC;
import components.types.ProbaPar;
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.AutomatonViewer;
import initializers.SuperInitializer;
import models.CAmodels.binary.BinaryModel;
import topology.basics.PlanarTopology;
import topology.basics.SuperTopology;
import topology.basics.TopologyCoder;

/*--------------------
 * Ising Model...
 * 
 *--------------------*/
public class QuorumFrontIzi extends BinaryModel  {

	public final static String NAME = "QuorumFrontIzi";

	final static int ZERO=0 /* N : neutral */, ONE= 1 /* A : alert*/;

	private static final String S_LABEL = "p (01,10->1)";

	ProbaPar m_probaP = new ProbaPar(S_LABEL, 0.2);

	public QuorumFrontIzi() {
		super();
	}

	
	/*
	 **********************************************************/
	public final int ApplyLocalFunction(OneRegisterIntCell cell) {
		return QuorumFrontIzi(cell);
	}

	private int QuorumFrontIzi(OneRegisterIntCell cell) {
		int 
		x= cell.ReadNeighbourState(0), 
		y= cell.ReadNeighbourState(1),
		z= cell.ReadNeighbourState(2);
		
		// D special case  : transformation to ONE 
		if (y==QuorumDModel.D)
			return QuorumDModel.D;
		if (x==QuorumDModel.D)
			x=ONE;
		if (z==QuorumDModel.D)
			z=ONE;
		
		// rule
		if (x==z) return z;
		return RandomEventDouble(m_probaP)?1:0;		
	}

	public void setP(double p) {
		m_probaP.SetVal(p);
	}
	
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TNCE;
	}

	public SuperInitializer GetDefaultInitializer() {
		return new QuorumFrontLacaInit();
	}

	@Override
	/** specific viewer */
	public AutomatonViewer GetPlaneAutomatonViewer(
			IntC cellPixSize, 
			SuperTopology topo, RegularDynArray automaton) {	
		PlanarTopology topo2D = (PlanarTopology) topo;
		AutomatonViewer av= new QuorumDdiagViewer(cellPixSize, topo2D.GetXYsize(), automaton);
		av.SetPalette(GetPalette());
		return av;
	}

	public FLPanel GetSpecificPanel(){
		FLPanel p= FLPanel.NewPanel();
		p.Add( m_probaP );
		return p;
	}

	/** override if needed */
//	public PaintToolKit GetPalette(){
//		PaintToolKit p = PaintToolKit.GetRandomPaletteDark(QuorumDdiagViewer.NCOLS); 
//		p.SetColor(0, FLColor.c_white);
//		return p;
//	}



}
