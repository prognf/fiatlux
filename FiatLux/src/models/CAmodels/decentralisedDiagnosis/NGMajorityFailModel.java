package models.CAmodels.decentralisedDiagnosis;

import components.allCells.Cell;
import components.allCells.ClassicalCACell;
import components.allCells.OneRegisterIntCell;
import grafix.gfxTypes.PaintToolKit;
import initializers.SuperInitializer;
import models.CAmodels.ClassicalModel;
import topology.basics.TopologyCoder;

public class NGMajorityFailModel extends ClassicalModel {
	public static final String NAME = "Majority-Fail";
	public final static int ZERO = 0, ONE = 1, FAIL = 2;

	@Override
	public SuperInitializer GetDefaultInitializer() {
		return new NGMajorityFailInitializer();
	}

	public NGMajorityFailModel() {
		super();
	}

	@Override
	public PaintToolKit GetPalette() {
		PaintToolKit palette = PaintToolKit.EmptyPalette();
		palette.AddColorRGB(255, 255, 255);
		palette.AddColorRGB(0, 0, 200);
		palette.AddColorRGB(0, 0, 0);
		return palette;
	}

	@Override
	public Cell GetNewCell() {
		ClassicalModel mod = this;
		return new ClassicalCACell(mod);
	}

	@Override
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TNCE;
	}

	public final int ApplyLocalFunction(OneRegisterIntCell inCell){
		return MajorityRule(inCell);
	}
	
	public static int MajorityRule(OneRegisterIntCell inCell) {
		if (inCell.GetState() == FAIL) {
			return FAIL;
		}

		int nAlive = inCell.CountOccurenceOf(ONE);
		int nDead = inCell.CountOccurenceOf(ZERO);

		if (nAlive > nDead) {
			return ONE;
		} else if(nDead > nAlive) {
			return ZERO;
		} else {
			return inCell.GetState();
		}
	}
}

