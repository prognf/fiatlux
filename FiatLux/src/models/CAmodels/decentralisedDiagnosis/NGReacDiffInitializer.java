package models.CAmodels.decentralisedDiagnosis;

import components.types.ProbaPar;
import grafix.gfxTypes.elements.FLPanel;
import initializers.CAinit.OneRegisterInitializer;

public class NGReacDiffInitializer extends OneRegisterInitializer {
	final static String CLASSNAME= Thread.currentThread().getStackTrace()[1].getClassName();

	ProbaPar pEx = new ProbaPar("pEx:", 0.33);
	ProbaPar pRef = new ProbaPar("pRef:", 0.5);
	ProbaPar pDef = new ProbaPar("pDef:", 0);
	NGReacDiffModel m_model;

	public NGReacDiffInitializer(NGReacDiffModel in_model) {
		m_model= in_model;
	}

	public void setValues(double pExVal, double pRefVal, double pDefVal) {
		pEx.SetVal(pExVal);
		pRef.SetVal(pRefVal);
		pDef.SetVal(pDefVal);
	}

	public void SubInit() {
		int excited = NGReacDiffModel.EXCITED;
		int refractory = NGReacDiffModel.REFRACTORY;
		int normal = NGReacDiffModel.NORMAL;

		for (int cell = 0; cell < GetLsize(); cell++) {
			if (RandomEventDouble(pEx)) {
				InitState(cell, excited);
			} else if (RandomEventDouble(pRef)) {
				InitState(cell, refractory);
			} else {
				InitState(cell, normal);
			}
		}

		int nbDef = (int) (pDef.GetVal() * GetLsize());
		InitStateKRandom(nbDef, NGReacDiffModel.FAILURE);
	}

	public FLPanel GetSpecificPanel() {
		FLPanel p = FLPanel.NewPanelVertical(pDef.GetControl(), pEx.GetControl(), pRef.GetControl());
		FLPanel control = FLPanel.NewPanel(p, GetRandomizer());
		p.setOpaque(false);
		control.setOpaque(false);
		return control;
	}
}
