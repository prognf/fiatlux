package models.CAmodels.decentralisedDiagnosis;

import java.util.ArrayList;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import components.allCells.Cell;
import components.allCells.ClassicalCACell;
import components.allCells.OneRegisterIntCell;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.elements.FLList;
import grafix.gfxTypes.elements.FLPanel;
import initializers.SuperInitializer;
import models.CAmodels.ClassicalModel;
import topology.basics.TopologyCoder;

public class NGMajorityRotatingToomModel extends ClassicalModel {
	public static final String NAME = "Majority-Rotation";
	public final static int ZERO = 0, ONE = 1, FAIL = 2;
	public final static double k = 1;
	private FLList list;

	@Override
	public SuperInitializer GetDefaultInitializer() {
		return new NGMajorityFailInitializer();
	}

	public NGMajorityRotatingToomModel() {
		super();
	}

	@Override
	public PaintToolKit GetPalette() {
		PaintToolKit palette = PaintToolKit.EmptyPalette();
		palette.AddColorRGB(255, 255, 255);
		palette.AddColorRGB(0, 0, 200);
		palette.AddColorRGB(0, 0, 0);
		return palette;
	}

	@Override
	public Cell GetNewCell() {
		ClassicalModel mod = this;
		return new ClassicalCACell(mod);
	}

	@Override
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TM9;
	}

	public int countStateRotating(int search, OneRegisterIntCell inCell) {
		int n1 = -1, n2 = -1, n3 = -1;

		ArrayList<OneRegisterIntCell> ngb = new ArrayList<>();

		int mode = list.GetSelectedItemRank();
		if (mode == 1) {
			ngb.add(0, inCell.GetNeighbour(1));
			ngb.add(1, inCell.GetNeighbour(0));
			ngb.add(2, inCell.GetNeighbour(3));
		}

		ngb.add((int) ((ngb.size()) * Math.random()), inCell.GetNeighbour(5));
		ngb.add((int) ((ngb.size()) * Math.random()), inCell.GetNeighbour(7));
		ngb.add((int) ((ngb.size()) * Math.random()), inCell.GetNeighbour(2));
		ngb.add((int) ((ngb.size()) * Math.random()), inCell.GetNeighbour(4));
		ngb.add((int) ((ngb.size()) * Math.random()), inCell.GetNeighbour(6));
		ngb.add((int) ((ngb.size()) * Math.random()), inCell.GetNeighbour(8));

		if (mode == 0) {
			ngb.add(0, inCell.GetNeighbour(1));
			ngb.add(1, inCell.GetNeighbour(0));
			ngb.add(2, inCell.GetNeighbour(3));
		}

		for (OneRegisterIntCell n : ngb) {
			int status = n.GetState();

			if (status != FAIL) {
				if (n1 == -1) {
					n1 = status;
				} else if (n2 == -1) {
					n2 = status;
				} else if (n3 == -1) {
					n3 = status;
					break;
				}
			}
		}

		return (n1 == search ? 1 : 0) + (n2 == search ? 1 : 0) + (n3 == search ? 1 : 0);
	}

	public final int ApplyLocalFunction(OneRegisterIntCell inCell){
		if (inCell.GetState() == FAIL) {
			return FAIL;
		}

		double n0 = countStateRotating(ZERO, inCell);
		double n1 = countStateRotating(ONE, inCell);

		if (n1 > n0) {
			return ONE;
		}

		return ZERO;
	}

	public FLPanel GetSpecificPanel() {
		FLPanel p = new FLPanel();
		String[] names = {"Priorité Toom", "Aléatoire"};
		list = new FLList(names, new Listener());
		p.Add(list);

		return	p;
	}

	class Listener implements ListSelectionListener {
		Listener() {
		}

		@Override
		public void valueChanged(ListSelectionEvent e) {
		}
	}
}

