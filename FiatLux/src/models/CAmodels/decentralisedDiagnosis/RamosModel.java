package models.CAmodels.decentralisedDiagnosis;

import components.allCells.OneRegisterIntCell;
import components.types.ProbaPar;
import grafix.gfxTypes.elements.FLPanel;
import models.CAmodels.binary.BinaryModel;
import topology.basics.TopologyCoder;

/*--------------------
 * choose one neighbour at random and copy its state
 *--------------------*/

public class RamosModel extends BinaryModel {

	public static final String NAME = "Ramos";

	private static final double DEF_ALPHA = 0.5, DEF_BETA=0.5;

	private ProbaPar m_alpha= new ProbaPar("alpha", DEF_ALPHA);
	private ProbaPar m_beta= new ProbaPar("beta", DEF_BETA);

	/*--------------------
	 * Attributes
	 --------------------*/

	/*--------------------
	 * Constructor
	 --------------------*/
	public RamosModel() {
		super();
	}


	/*--------------------
	 * overrides & implementations
	 --------------------*/

	@Override
	public int ApplyLocalFunction(OneRegisterIntCell cell) {
		int ql=cell.ReadNeighbourState(0);
		int qr=cell.ReadNeighbourState(1);
		if (ql==qr) {
			return ql;
		} else {
			boolean toone= (ql==1)?RandomEventDouble(m_alpha):RandomEventDouble(m_beta);
			return toone?1:0;
		}
		//sreturn Macro.ERRORint;
	}

	//public final int ApplyLocalFunction(OneRegisterIntCell in_Cell) {

	
	/*public SuperInitializer GetDefaultInitializer() {
		return new QuorumDInit();
	}*/

	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TLR1;
	}


	/*--------------------
	 *  Get & Set methods
	 --------------------*/



	public FLPanel GetSpecificPanel(){
		FLPanel p= FLPanel.NewPanel();
		//p.Add(m_proba1, m_proba2 );
		p.Add(m_alpha,m_beta);
		p.Add( this.GetRandomizer().GetSeedControl() );
		return p;
	}



	/*--------------------
	 *  Other methods
	 --------------------*/
}
