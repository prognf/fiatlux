package models.CAmodels.decentralisedDiagnosis;

import components.allCells.OneRegisterIntCell;
import components.types.DoublePar;
import grafix.gfxTypes.elements.FLPanel;
import initializers.SuperInitializer;
import models.CAmodels.binary.BinaryModel;
import topology.basics.TopologyCoder;

/*--------------------
 * Model: Majority Model Description: classical majority without noise
 * 
*--------------------*/
public class BiasedQuiesentModelToom extends BinaryModel {

	public static final String NAME = "BQ-Toom";
	final static int ZERO=0, ONE=1;
	public static final int D = QuorumDModel.D, A= QuorumDModel.A, N=QuorumDModel.N;
	DoublePar m_bias= new DoublePar("p:", 0.1);


	public BiasedQuiesentModelToom() {
		super();
	}

	public final int ApplyLocalFunction(OneRegisterIntCell inCell){
		return localRule(inCell);
	}

	@Override
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TNCE;
	}

	public int localRule(OneRegisterIntCell inCell) {
		int q= inCell.GetState();
		if (q==D) // error state
			return D;
		// 1,D -> 1star
		int qn= inCell.ReadNeighbourState(N), qe=inCell.ReadNeighbourState(D);
		if (qn==D) {
			qn=A; // transformation into an A
		}
		if (qe==D) {
			qe=A; // transformation into an A
		}
		if (qn==qe){// "outer-quiescence"
			return qn;
		} else {
			boolean one= RandomEventDouble(m_bias.GetVal());
			return one ? ONE : ZERO;
		}
	}
	
	@Override
	public FLPanel GetSpecificPanel(){
		return m_bias.GetControl();
	}
	
	@Override
	public SuperInitializer GetDefaultInitializer() {
		return new QuorumDInit();
	}
	
	/* external setting **/
	public void setBiasPval(double pval) {
		m_bias.SetVal(pval);
	}
	
}

