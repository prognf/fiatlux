package models.CAmodels.densClassif;

import components.allCells.OneRegisterIntCell;
import grafix.gfxTypes.controllers.DoubleController;
import grafix.gfxTypes.elements.FLPanel;
import models.CAmodels.binary.BinaryModel;
import topology.basics.TopologyCoder;

/*--------------------
 * ongoing study
 *  * @author Nazim Fates 
 *****--------------------*/
public class GenSchueleModel extends BinaryModel {
	
	
	public static final String NAME = "GenSchuele";
	public static final String TXT = "eps (0:1)";
	final private static int OCTOPUS=8;

	/*--------------------
	 * Attributes
	 --------------------*/

	double m_eps = 0.66;
	double [] m_Transition = new double[OCTOPUS];	
	
	/*--------------------
	 * overrides & implementations
	 --------------------*/

	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TV5;
	}

	/* overloading this method */
	public FLPanel GetSpecificPanel() {
		return new p_ShiftControl();
	}
	/*--------------------
	 * Constructor
	 --------------------*/
	
	public GenSchueleModel() {
		super();
	}

	/*--------------------
	 * main
	 --------------------*/
	
	


	/* MAIN FUNCTION  */
	/* Fates Model */ 
	final public int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
		int N1 = in_Cell.CountOccurenceOf(1), N= in_Cell.GetNeighbourhoodSize();
		boolean majRule = RandomEventDouble(m_eps);
		if (majRule){
			if (2* N1 > N){
				return 1;
			} else if (2 * N1 < N){
				return 0;
			} else {
				return in_Cell.GetState();
			}
		} else {
			return N1 % 2;
		}
	}
	
	public void SetEps(double val){
		m_eps= val;
	}
	
	/*--------------------
	 * GFX
	 --------------------*/

	class p_ShiftControl extends DoubleController{

		public p_ShiftControl() {
			super(TXT);
		}

		@Override
		protected double ReadDouble() {
			return m_eps;
		}

		@Override
		protected void SetDouble(double val) {
			SetEps(val);	
		}
		
	}
	
	}
