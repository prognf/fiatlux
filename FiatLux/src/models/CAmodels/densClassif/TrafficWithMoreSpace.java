package models.CAmodels.densClassif;

import components.allCells.Cell;
import components.allCells.ClassicalCACell;
import components.allCells.OneRegisterIntCell;
import grafix.gfxTypes.controllers.DoubleController;
import grafix.gfxTypes.elements.FLPanel;
import models.CAmodels.binary.BinaryModel;
import topology.basics.TopologyCoder;

public class TrafficWithMoreSpace extends BinaryModel {

	private static final String NOISE_TXT = "eta";
	public static final String NAME = "TraMoreSpace";

	/*--------------------
	 * overrides & implementations
	 --------------------*/

	@Override
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TLR2;
	}


	public Cell GetNewCell() {
		return new ClassicalCACell(this);
	}

	@Override
	public FLPanel GetSpecificPanel(){
		return new NoiseController();
	}

	/*--------------------
	 * Attributes
	 --------------------*/

	double m_noise=0.0;//1F;

	/*--------------------
	 * Constructor
	 --------------------*/
	public TrafficWithMoreSpace() {
		super();
	}

	/*--------------------
	 * Get & Set methods
	 --------------------*/

	/** Neighb= [qA,qB,q,qC,qD] **/
	public final int ApplyLocalFunction(OneRegisterIntCell cell) {
		int q = cell.GetState();
		int 
		qa= cell.ReadNeighbourState(0),
		qb= cell.ReadNeighbourState(1),
		qc= cell.ReadNeighbourState(3),
		qd= cell.ReadNeighbourState(4);
		if (q==0) {
			if (qb==0)
				return 0;
			else //qB=1
				// exception eta
				if( (qc==0) && (qd==0) && RandomEventDouble(m_noise)) {
					return 0;
				}
				// exception eta //qB=1
				else if ((qc==1) && RandomEventDouble(m_noise)) {
					return 1; //qC
				}
			return 1-qc; 
		} else { //q=X
			if ((qc==0) & (qd==0)) {
				return 0;
			} else
				return 1;
		}
	}

	/*--------------------
	 * Noise controller
	 --------------------*/

	class NoiseController extends DoubleController {


		public NoiseController() {
			super(NOISE_TXT);
		}

		@Override
		protected double ReadDouble() {
			return m_noise;
		}

		@Override
		protected void SetDouble(double val) {
			m_noise= val;
		}

	}

}
