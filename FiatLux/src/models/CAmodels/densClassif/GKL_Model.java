package models.CAmodels.densClassif;

import components.allCells.Cell;
import components.allCells.ClassicalCACell;
import components.allCells.OneRegisterIntCell;
import grafix.gfxTypes.controllers.DoubleController;
import grafix.gfxTypes.elements.FLPanel;
import models.CAmodels.binary.BinaryModel;
import topology.basics.TopologyCoder;

public class GKL_Model extends BinaryModel {

	private static final String NOISE_TXT = "Noise";
	public static final String NAME = "GKL";

	/*--------------------
	 * overrides & implementations
	 --------------------*/

	@Override
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_GKL;
	}
	
	
	public Cell GetNewCell() {
		return new ClassicalCACell(this);
	}

	@Override
	public FLPanel GetSpecificPanel(){
		return new NoiseController();
	}

	/*--------------------
	 * Attributes
	 --------------------*/

	// TODO Beware of this !!!
	double m_noise=0.0;//1F;
	

	
	/*--------------------
	 * Constructor
	 --------------------*/
	public GKL_Model() {
		super();
	}

	/*--------------------
	 * Get & Set methods
	 --------------------*/

	public final int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
		int state = in_Cell.GetState();
		int s;
		if (state==0){
			 s = state + in_Cell.ReadNeighbourState(0) + in_Cell.ReadNeighbourState(1);
		} else {
			 s = state + in_Cell.ReadNeighbourState(2) + in_Cell.ReadNeighbourState(3);
		}
		int result= s>1 ? 1 : 0;
		if (RandomEventDouble(m_noise)){
			result= 1 - result;
		}
		return result;
	}
	
	/*--------------------
	 * Noise controller
	 --------------------*/

	class NoiseController extends DoubleController {

		
		public NoiseController() {
			super(NOISE_TXT);
		}

		@Override
		protected double ReadDouble() {
			return m_noise;
		}

		@Override
		protected void SetDouble(double val) {
			m_noise= val;
		}
		
	}
		
}
