package models.CAmodels.densClassif;

import components.allCells.OneRegisterIntCell;
import main.Macro;
import models.CAmodels.binary.BinaryModel;
import topology.basics.SuperTopology;
import topology.basics.TopologyCoder;
import topology.zoo.NESW_TM;

/*--------------------
 * equivalent of the Traffic Model
 *****--------------------*/
public class ReynagaModel extends BinaryModel {


	final static int ZERO = 0, ONE = 1;
	public static final String NAME = "Reynaga";

	enum TopoType { N4, N8 }

    private TopoType m_topoType;
	int m_NeighbSize; // the "middle" of the 


	/*--------------------
	 * construction
	 --------------------*/

	public ReynagaModel() {
		super();
	}

	@Override
	protected void SubInitialiseTopologyInfo(SuperTopology topologyManager) {
		m_NeighbSize= topologyManager.GetActualNeighbMaxSize();
		Macro.print(3, "Reynaga's rule, topo size : " + m_NeighbSize);
		switch (m_NeighbSize){
		case 4:
			m_topoType= TopoType.N4;
			break;
		case 8:
			m_topoType= TopoType.N8;
			break;
		default:
			Macro.SystemWarning(" non supported topology ! ");
		}
	}
	
	

	/*--------------------
	 * overrides & implementations
	 --------------------*/

	@Override
	public String GetName(){
		return GetKey() + "-" + m_topoType;
	}
	
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TV4;
	}

	@Override
	final public int ApplyLocalFunction(OneRegisterIntCell cell) {
		switch(m_topoType){
		case N4:
			return LocalFunction4(cell);
		case N8:
			return LocalFunction8(cell);
		default:
			return -1;
		}
	}

	static final int LocalFunction4(OneRegisterIntCell c) {
		int st = c.GetState();
		if (st == ONE){
			st += NST(c,NESW_TM.NORTH) + NST(c,NESW_TM.EAST); 
		} else {
			st += NST(c,NESW_TM.SOUTH) + NST(c,NESW_TM.WEST);		
		}
		return (st > 1)?ONE:ZERO;
	}

	static final int LocalFunction8(OneRegisterIntCell c) {
		int st = c.GetState();
		int dec=0;
		if (st == ZERO){
			dec=4; 
		} 
		for (int i=0; i< 4;i++){
			st += NST(c,i+dec);
		}
		return (st > 2)?ONE:ZERO;
	}

	static final private int NST(OneRegisterIntCell c, int neighbPos) {
		return c.ReadNeighbourState(neighbPos);
	}




	//@Override
	/*public AbstractInitializer GetNewInitializer(){
		return new LifeInitializer();
	}*/


}
