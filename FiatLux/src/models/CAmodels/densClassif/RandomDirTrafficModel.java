package models.CAmodels.densClassif;

import components.allCells.OneRegisterIntCell;
import grafix.gfxTypes.controllers.DoubleController;
import grafix.gfxTypes.elements.FLPanel;
import models.CAmodels.binary.BinaryModel;
import models.CAmodels.binary.MajorityModel;
import models.CAmodels.binary.Traffic2DModel;
import topology.basics.TopologyCoder;

/*--------------------
 * ongoing study
 *  * @author Nazim Fates 
 *****--------------------*/
public class RandomDirTrafficModel extends BinaryModel {
	
	
	public static final String NAME = "RandomDirTraffic";
	public static final String TXT = "eps (0:1)";

	/*--------------------
	 * Attributes
	 --------------------*/

	double m_eps = 0;
	
	/*--------------------
	 * overrides & implementations
	 --------------------*/

	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TV4;
	}

	/** overloading this method */
	public FLPanel GetSpecificPanel() {
		return FLPanel.NewPanel(new p_ShiftControl(), GetRandomizer());
	}
	
	/*--------------------
	 * Constructor
	 --------------------*/
	
	public RandomDirTrafficModel() {
		super();
	}

	/*--------------------
	 * main
	 --------------------*/

	/** MAIN FUNCTION  */
	final public int ApplyLocalFunction(OneRegisterIntCell inCell) {
		boolean majRule = RandomEventDouble(m_eps);
		if (majRule){
			return MajorityModel.MajorityRule(inCell);
		//	return MajorityCNESW(inCell);
		} else{
			return Traffic2DModel.TrafficDiag(inCell);
		}
	}
	
	private int MajorityCNESW(OneRegisterIntCell c) {
		int s= c.GetState() + NST(c,0)  + NST(c,2) + NST(c,4) + NST(c,6);
		return (s>2)?1:0;
	}

	public void SetEps(double val){
		m_eps= val;
	}
	
	static final private int NST(OneRegisterIntCell c, int neighbPos) {
		return c.ReadNeighbourState(neighbPos);
	}
	/*--------------------
	 * GFX
	 --------------------*/

	class p_ShiftControl extends DoubleController{

		public p_ShiftControl() {
			super(TXT);
		}

		@Override
		protected double ReadDouble() {
			return m_eps;
		}

		@Override
		protected void SetDouble(double val) {
			SetEps(val);	
		}
		
	}
	
	}
