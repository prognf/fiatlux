package models.CAmodels.densClassif;

import components.allCells.OneRegisterIntCell;
import grafix.gfxTypes.controllers.DoubleController;
import grafix.gfxTypes.elements.FLPanel;
import models.CAmodels.binary.BinaryModel;
import topology.basics.TopologyCoder;

/*--------------------
 * ongoing study
 *--------------------*/
public class FuksDensityStudyModel extends BinaryModel {


	public static final String NAME = "Fuks-DensityStudy";
	public static final String TXT = "Shift proba";
	private static final double DEF_PVAL = 0.25;
	/*--------------------
	 * Attributes
	 --------------------*/

	double m_pShift = DEF_PVAL;

	/*--------------------
	 * overrides & implementations
	 --------------------*/

	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TLR1;			
	}	

	/* MAIN FUNCTION  */
	/* FUKS Model */ 
	final public int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
		int 
		x = in_Cell.ReadNeighbourState(0),
		y = in_Cell.ReadNeighbourState(1),
		z = in_Cell.ReadNeighbourState(2);
		double p_ToOne= y * (1 - 2 * m_pShift) + m_pShift * (x + z);
		boolean one = RandomEventDouble(p_ToOne);
		return one ? 1 : 0;
	}




	/* overloading this method */
	public FLPanel GetSpecificPanel() {
		return new p_ShiftControl();
	}
	/*--------------------
	 * Constructor
	 --------------------*/

	public FuksDensityStudyModel() {
		super();
	}

	/*--------------------
	 * Get / set
	 --------------------*/

	public void SetpShift(double in_val){
		m_pShift= in_val;
	}


	/*--------------------
	 * GFX
	 --------------------*/

	class p_ShiftControl extends DoubleController{

		public p_ShiftControl() {
			super(TXT);
		}

		@Override
		protected double ReadDouble() {
			return m_pShift;
		}

		@Override
		protected void SetDouble(double val) {
			SetpShift(val);		
		}

	}

}
