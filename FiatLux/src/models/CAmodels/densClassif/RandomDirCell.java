package models.CAmodels.densClassif;

import components.allCells.OneRegisterIntCell;
import components.randomNumbers.FLRandomGenerator;
import topology.zoo.NESW_TM;

public class RandomDirCell extends OneRegisterIntCell {

	FLRandomGenerator m_randomSource;
	float m_diceRoll;
	
	protected RandomDirCell(FLRandomGenerator modelRandomSource){
		m_randomSource= modelRandomSource;
	}
	
	@Override
	public void sig_UpdateBuffer() {
		m_diceRoll = m_randomSource.RandomFloat();
		int st= GetState();
		if (st ==0){
			int W= this.ReadNeighbourState(NESW_TM.WEST);
			if (W==1){
		//		boolean tryWhorizontal = ((RandomDirCell)GetNeighbour(NESW_TM.WEST)).IsTryingH();
			}
		}
	}

	@Override
	public void sig_MakeTransition() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean IsStable() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void sig_Reset() {
		// TODO Auto-generated method stub

	}

}
