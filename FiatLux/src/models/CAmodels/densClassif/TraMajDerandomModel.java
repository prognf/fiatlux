package models.CAmodels.densClassif;

import components.allCells.OneRegisterIntCell;
import models.CAmodels.binary.BinaryModel;
import topology.basics.TopologyCoder;

/** LR3 : what is this ??? */
public class TraMajDerandomModel extends BinaryModel {

	public static final String NAME = "DerandomTraMaj";

	/*--------------------
	 * overrides & implementations
	 --------------------*/

	@Override
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TLR3;
	}


	/*public Cell GetNewCell() {
		return new ClassicalCACell(this);
	}*/



	/*--------------------
	 * Attributes
	 --------------------*/




	/*--------------------
	 * Constructor
	 --------------------*/
	public TraMajDerandomModel() {
	}

	/*--------------------
	 * Get & Set methods
	 --------------------*/

	public final int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
		int c = in_Cell.GetState();
		int 
		lll=in_Cell.ReadNeighbourState(0), 
		ll=	in_Cell.ReadNeighbourState(1), 
		l=	in_Cell.ReadNeighbourState(2),
		r=	in_Cell.ReadNeighbourState(4), 
		rr= in_Cell.ReadNeighbourState(5),
		rrr=in_Cell.ReadNeighbourState(6);
		int pos= 4*l + 2*c + r;
		int 	suml= lll + ll, 
				sumr= rr + rrr;
		boolean notTrafXOO= (sumr==0) && ( ((lll==1) && (ll==0)) || suml==0 ) ; 
		boolean notTrafXXO= (suml==2) && ( ((rrr==0) && (rr==1)) || sumr==2 );
		switch (pos){
		case 4:			
			return notTrafXOO?0:1;
		case 6:
			return notTrafXXO?1:0;
		default:
			return c==0?l:r;
		}


	}


}
