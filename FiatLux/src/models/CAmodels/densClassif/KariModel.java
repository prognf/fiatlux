package models.CAmodels.densClassif;

import components.allCells.Cell;
import components.allCells.ClassicalCACell;
import components.allCells.OneRegisterIntCell;
import models.CAmodels.binary.BinaryModel;
import topology.basics.TopologyCoder;

public class KariModel extends BinaryModel {

	public static final String NAME = "Kari";

	/*--------------------
	 * overrides & implementations
	 --------------------*/

	@Override
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TLR3;
	}
	
	
	public Cell GetNewCell() {
		return new ClassicalCACell(this);
	}

	/*--------------------
	 * Attributes
	 --------------------*/

	

	
	/*--------------------
	 * Constructor
	 --------------------*/
	public KariModel() {
		super();
	}

	/*--------------------
	 * Get & Set methods
	 --------------------*/

	public final int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
		int state = in_Cell.GetState();
		int copy= Traffic3(in_Cell,2,3,4);
		
		if ( (copy==1) &&
			(Traffic3(in_Cell,0,1,2)==0) && 
			(Traffic3(in_Cell,1,2,3)==0) && 
			(Traffic3(in_Cell,3,4,5)==0) ){	
			copy = 0;
		}  else if ( (copy==0) && 
				(Traffic3(in_Cell,1,2,3)==1) && 
				(Traffic3(in_Cell,3,4,5)==1) && 
				(Traffic3(in_Cell,4,5,6)==1) ){
			copy = 1;
		}
		return copy;
	}


	private int Traffic3(OneRegisterIntCell inCell, int left, int center, int right) {
		if (inCell.ReadNeighbourState(center)==0){
			return inCell.ReadNeighbourState(left);
		} else {
			return inCell.ReadNeighbourState(right);
		}				
	}
	
	
}
