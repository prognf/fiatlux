package models.CAmodels.densClassif;

import components.allCells.OneRegisterIntCell;
import grafix.gfxTypes.controllers.DoubleController;
import grafix.gfxTypes.elements.FLPanel;
import models.CAmodels.binary.BinaryModel;
import topology.basics.TopologyCoder;

/*--------------------
 * ongoing study
 *  * @author Nazim Fates 
 *****--------------------*/
public class TraMajDensityStudyModel extends BinaryModel {
	public static final String NAME = "TraMaj";
	public static final String DELTA_TXT = "eta (0:1)", NOISE_TXT= "Noise";
	final private static int OCTOPUS=8;

	/*--------------------
	 * Attributes
	 --------------------*/
	double m_noise = 0.00;
	double m_delta = 0.1;
	double[] m_Transition = new double[OCTOPUS];
	
	/*--------------------
	 * overrides & implementations
	 --------------------*/

	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TLR1;			
	}

	/* overloading this method */
	public FLPanel GetSpecificPanel() {
		FLPanel p = FLPanel.NewPanel(new p_ShiftControl(), new p_NoiseControl());
		FLPanel panel = FLPanel.NewPanel(p, this.GetRandomizer());
		p.setOpaque(false);
		panel.setOpaque(false);
		return panel;
	}
	/*--------------------
	 * Constructor
	 --------------------*/
	
	public TraMajDensityStudyModel() {
		super();
		InitTransitionTable();
	}

	/*--------------------
	 * main
	 --------------------*/
	
	private void UpdateTable(){
		m_Transition[0]= 0;
		m_Transition[1]= 0;
		m_Transition[2]= 0;//m_noise;
		m_Transition[3]= 1;
		
		m_Transition[4]= 1- m_delta;
		m_Transition[5]= 1;
		m_Transition[6]= m_delta;
		m_Transition[7]= 1;		
	}
	
	private void InitTransitionTable() {
		UpdateTable();
	}


	/* MAIN FUNCTION  */
	/* Fates Model */ 
	final public int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
		int NeighbourhoodByte = 
			in_Cell.ReadNeighbourState(2)
			| in_Cell.ReadNeighbourState(1) << 1
			| in_Cell.ReadNeighbourState(0) << 2;
		double ptrans= m_Transition[NeighbourhoodByte];
		int result;
		if (ptrans==0){
			result= 0;
		} else if (ptrans==1){
			result= 1;
		} else {
			boolean one = RandomEventDouble(ptrans);
			result= one ? 1 : 0;
		}
		if (RandomEventDouble(m_noise)){
			result= 1 - result;
		}
		return result;
	}
	
	public void SetEps(double val){
		m_delta= val;
		UpdateTable();
	}
	
	public void SetNoise(double val){
		m_noise= val;
		UpdateTable();
	}
	
	/*--------------------
	 * GFX
	 --------------------*/

	class p_ShiftControl extends DoubleController{
		public p_ShiftControl() {
			super(DELTA_TXT);
			minimize();
		}

		@Override
		protected double ReadDouble() {
			return m_delta;
		}

		@Override
		protected void SetDouble(double val) {
			SetEps(val);	
		}
		
	}
	
	class p_NoiseControl extends DoubleController {
		public p_NoiseControl() {
			super(NOISE_TXT);
			minimize();
		}

		@Override
		protected double ReadDouble() {
			return m_noise;
		}

		@Override
		protected void SetDouble(double val) {
			SetNoise(val);	
		}
	}
}
