package models.CAmodels.densClassif;

import components.allCells.OneRegisterIntCell;
import grafix.gfxTypes.controllers.DoubleController;
import grafix.gfxTypes.elements.FLPanel;
import models.CAmodels.binary.BinaryModel;
import topology.basics.TopologyCoder;

/*--------------------
 * ongoing study
 *  * @author Nazim Fates 
 *****--------------------*/
public class TraMajModelDouble2D extends BinaryModel {
	
	
	public static final String NAME = "DoubleTraMaj";
	public static final String TXT = "eps (0:1)";

	/*--------------------
	 * Attributes
	 --------------------*/

	double m_eps = 0;
	
	/*--------------------
	 * overrides & implementations
	 --------------------*/

	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TV4;
	}

	/* overloading this method */
	public FLPanel GetSpecificPanel() {
		return new p_ShiftControl();
	}
	/*--------------------
	 * Constructor
	 --------------------*/
	
	public TraMajModelDouble2D() {
		super();
	}

	/*--------------------
	 * main
	 --------------------*/

	/** MAIN FUNCTION  */
	final public int ApplyLocalFunction(OneRegisterIntCell c) {
		//boolean majority = RandomEventDouble(m_eps);
		
		int shift=1;
		int  x, y= c.GetState(),z;
		
		/*if (majority) {
			return MajorityModel.MajorityRule(c);
		}	
		else*/ if (RandomEventDouble(m_eps)){
			x= NST(c,2);
			z= NST(c,0);
		} else{
			x= NST(c,3);
			z= NST(c,1);
		}		 
		return Traffic(x,y,z);
		// return MajorityModel.MajorityRule(inCell);
	}
	
	private int Traffic(int x, int y, int z) {
		return (y==0)?x:z;
	}

	static final private int NST(OneRegisterIntCell c, int neighbPos) {
		return c.ReadNeighbourState(neighbPos);
	}
	
	public void SetEps(double val){
		m_eps= val;
	}
	
	/*--------------------
	 * GFX
	 --------------------*/

	class p_ShiftControl extends DoubleController{

		public p_ShiftControl() {
			super(TXT);
		}

		@Override
		protected double ReadDouble() {
			return m_eps;
		}

		@Override
		protected void SetDouble(double val) {
			SetEps(val);	
		}
		
	}
	
	}
