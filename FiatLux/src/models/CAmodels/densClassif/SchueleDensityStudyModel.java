package models.CAmodels.densClassif;

import components.allCells.OneRegisterIntCell;
import grafix.gfxTypes.controllers.DoubleController;
import grafix.gfxTypes.elements.FLPanel;
import models.CAmodels.binary.BinaryModel;
import topology.basics.TopologyCoder;

/*--------------------
 * ongoing study
 *  * @author Nazim Fates 
 *****--------------------*/
public class SchueleDensityStudyModel extends BinaryModel {
	
	
	public static final String NAME = "Schuele";
	public static final String TXT = "eps (0:1)";
	final private static int OCTOPUS=8;

	/*--------------------
	 * Attributes
	 --------------------*/

	double m_eps = 0.66;
	double [] m_Transition = new double[OCTOPUS];	
	
	/*--------------------
	 * overrides & implementations
	 --------------------*/

	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TLR1;
	}

	/* overloading this method */
	public FLPanel GetSpecificPanel() {
		return new p_ShiftControl();
	}
	/*--------------------
	 * Constructor
	 --------------------*/
	
	public SchueleDensityStudyModel() {
		super();
		InitTransitionTable();
	}

	/*--------------------
	 * main
	 --------------------*/
	
	private void UpdateTable(){
		m_Transition[1]= 1- m_eps;
		m_Transition[2]= 1- m_eps;
		m_Transition[3]= m_eps;
		m_Transition[4]= 1- m_eps;
		m_Transition[5]= m_eps;
		m_Transition[6]= m_eps;
	}
	
	private void InitTransitionTable() {
		// fixed values
		m_Transition[0]= 0;
		m_Transition[7]= 1;
		UpdateTable();
	}


	/* MAIN FUNCTION  */
	/* Fates Model */ 
	final public int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
		int NeighbourhoodByte = 
			in_Cell.ReadNeighbourState(2)
			| in_Cell.ReadNeighbourState(1) << 1
			| in_Cell.ReadNeighbourState(0) << 2;
		double ptrans= m_Transition[NeighbourhoodByte];
		if (ptrans==0){
			return 0;
		} else if (ptrans==1){
			return 1;
		} else {
			boolean one = RandomEventDouble(ptrans);
			return one ? 1 : 0;
		}
	}
	
	public void SetEps(double val){
		m_eps= val;
		UpdateTable();
	}
	
	/*--------------------
	 * GFX
	 --------------------*/

	class p_ShiftControl extends DoubleController{

		public p_ShiftControl() {
			super(TXT);
		}

		@Override
		protected double ReadDouble() {
			return m_eps;
		}

		@Override
		protected void SetDouble(double val) {
			SetEps(val);	
		}
		
	}
	
	}
