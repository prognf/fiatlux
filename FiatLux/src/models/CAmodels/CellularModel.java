package models.CAmodels;

import java.util.List;

import components.allCells.Cell;
import components.allCells.CellCastException;
import components.allCells.SuperCell;
import components.arrays.GenericCellArray;
import components.arrays.RegularDynArray;
import components.types.IntC;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.AutomatonViewer;
import grafix.viewers.HexagonalViewer;
import grafix.viewers.LineAutomatonViewer;
import grafix.viewers.LineAutomatonViewerDefault;
import grafix.viewers.OneRegisterAutomatonViewer;
import initializers.SuperInitializer;
import main.Macro;
import main.tables.PlotterSelectControl;
import models.SuperModel;
import topology.basics.LinearTopology;
import topology.basics.PlanarTopology;
import topology.basics.SuperTopology;
import topology.basics.TopologyCoder;
import topology.zoo.HexaTM;
import updatingScheme.AlphaScheme;
import updatingScheme.UpdatingScheme;

/*--------------------
 * contains information on a model : what cell to use , what
 * initializer, etc. it returns a default AutomatonViewer
 * creates cells that are ruled by the model
 * returns the associated Viewer, given a topology
 --------------------*/

public abstract class CellularModel extends SuperModel {
	
	
	/*--------------------
	 * Abstract methods definition
	 --------------------*/
	 
	/**
	 * association of a specific initializer to a model 
	 */
	abstract public SuperInitializer GetDefaultInitializer();
	
	/**
	 * Creates an empty cell
	 * @return the cell (without neighbourhood)
	 */
	abstract public SuperCell GetNewCell();
	

	
	
	/*--------------------
	 * attributes
	 --------------------*/

	
	/*--------------------
	 * constructor
	 --------------------*/
	public CellularModel() {
		Macro.print(3, "New cellular model: " + getClass().getSimpleName());
	}

	
	/*--------------------
	 * overrides
	 --------------------*/
	// creation of typed arrays
	final public GenericCellArray<SuperCell> GetNewCellArray(int Lsize){
		return new GenericCellArray<SuperCell>(Lsize);
	}


    /** default associated updating scheme
	 * Override if needed **/
	public UpdatingScheme GetDefaultUpdatingScheme() {
		return new AlphaScheme();
	}

	
	/**
	 * @return a default topology code
	 */
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TM9;
	}
	
	/** override
	 * we allow a model to return a specific panel 
	 */
	public FLPanel GetSpecificPanel() {
		return null;
	}
	
	/** override this ! : cellChange determines if the cells are inert or no */
	public Cell GetNewBlindCell(int state, boolean cellChange) /*throws Exception*/ {
		throw new CellCastException();
		//return null;
	} 
	
	
	
	/**
	 * defines the associated plotters for 1D simulations
	 * override if needed
	 */
	public String[] GetPlotterSelection1D() {
		return PlotterSelectControl.defaultSelect;
	}

	/**
	 * defines the associated plotters for 2D simulations
	 *  override if needed
	 */
	public String[] GetPlotterSelection2D() {
		return PlotterSelectControl.defaultSelect;
	}
	
	static public String GetModelSettingInfo(){
		return "NULL";
	}
	
	static public String GetModelDefaultSetting(){
		return GetModelSettingInfo();
	} 
	
	

	/** override if needed */
	public PaintToolKit GetPalette(){
		Macro.print(5,"Using default palette");
		return PaintToolKit.GetDefaultPalette(); 
	}
	
	/** override
	 * 1D Viewer : called by simulation sampler */
	public LineAutomatonViewer GetLineAutomatonViewer(IntC SquareDim, 
		LinearTopology topo, RegularDynArray automaton, int Tsize) {
		LineAutomatonViewer av = 
			new LineAutomatonViewerDefault(SquareDim, topo, automaton, Tsize);
		av.SetPalette( GetPalette() );
		return av;
	}

	/** override 
	 * 2D Viewer : called by simulation sampler  */
	public AutomatonViewer GetPlaneAutomatonViewer(
			IntC in_SquareDim, 
			SuperTopology in_TopologyManager, RegularDynArray in_Automaton) {
		PlanarTopology topo2D= (PlanarTopology)in_TopologyManager;
		AutomatonViewer av = null;
		if (in_TopologyManager.GetName() == HexaTM.NAME ){
			av = new HexagonalViewer(in_SquareDim, topo2D.GetXYsize(), in_Automaton);
		} else {
			// default selection
			av = new OneRegisterAutomatonViewer(in_SquareDim, topo2D.GetXYsize(), in_Automaton);
			//av= new EnhancedAutomatonViewer(in_SquareDim, topo2D.GetXYsize(), in_Automaton);
		}
		av.SetPalette( GetPalette() );
		return av;
	}
		
	/*--------------------
	 * name for information
	 --------------------*/
	
	/**
	 * @return a String declaring parameters used specifically by this model.
	 */
	public String getCurrentSettings() {
		return "not_implemented";
	}
	
	
	/*--------------------
	 * Topology
	 --------------------*/
	
	/** This intermediary step ensures that the topology graph has been initialized. **/
	final public void InitialiseTopologyInfo(SuperTopology topologyManager) {
		if (topologyManager.GetTopologyGraph() == null) {
			Macro.FatalError("Topology graph has not been initialized and " +
					"cannot be used to initialize the model.");
		}
		SubInitialiseTopologyInfo(topologyManager);
	}
	
	/** override if the behaviour of the model depends on the topology **/
	protected void SubInitialiseTopologyInfo(SuperTopology topologyManager) {
		// nothing by default
	}
	
	//OC
	public String GetDisableDimension(){
		return null;
	}

    public List<String> GetDisabledTopology(){
		return null;
	}
    //\OC
}
