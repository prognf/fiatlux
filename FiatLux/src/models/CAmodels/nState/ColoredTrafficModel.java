package models.CAmodels.nState;

import components.allCells.Cell;
import components.allCells.ClassicalCACell;
import components.allCells.OneRegisterIntCell;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.controllers.DoubleController;
import grafix.gfxTypes.controllers.FLCheckBox;
import grafix.gfxTypes.elements.FLPanel;
import initializers.SuperInitializer;
import models.CAmodels.ClassicalModel;
import topology.basics.TopologyCoder;
import updatingScheme.TriangularScheme;
import updatingScheme.UpdatingScheme;

/*--------------------
 * extended Traffic rule
*--------------------*/

public class ColoredTrafficModel extends ClassicalModel {

	
	/*--------------------
	 * overrides & implementations
	 --------------------*/

	/* overrides */
	public SuperInitializer GetDefaultInitializer() {
		return new ColoredTrafficInitializer();
	}

	/* overrides */
	public Cell GetNewCell() {
		return new ClassicalCACell(this);
	}
	
	@Override
	public String GetDefaultAssociatedTopology(){
		return TopologyCoder.s_TLR1;
	}
	
	@Override
	public UpdatingScheme GetDefaultUpdatingScheme(){
		return new TriangularScheme(N_STATES-1);
	}

	public FLPanel GetSpecificPanel() {
		FLPanel p = new FLPanel();
		p.Add(new ErrorController(), new ColorStyleControl());
		return p;
	}
	/*--------------------
	 * Attributes
	 --------------------*/
	private double m_Error=0.1;
	private boolean m_colorStyle;

	
	/*--------------------
	 * Constructor
	 --------------------*/
	public ColoredTrafficModel() {
	}

	/*--------------------
	 * // Get & Set methods
	 --------------------*/
	

	final static int N_STATES=8, BACKGROUND= N_STATES -1, NOISE = N_STATES - 2;
	public static final String NAME = "ColoredTraffic";
	
	/* overrides */
	public PaintToolKit GetPalette() {
		FLColor[] palette = new FLColor[N_STATES];
		palette[0] = FLColor.c_black;
		palette[1] = FLColor.c_blue;
		palette[2] = FLColor.c_yellow;
		palette[3] = FLColor.c_green;
		palette[4] = FLColor.c_red;
		palette[5] = FLColor.c_purple;
		palette[6] = FLColor.c_white;
		palette[7] = FLColor.c_grey1; // background triangle color
		return new PaintToolKit(palette);
	}

	public final int ApplyLocalFunction(OneRegisterIntCell in_Cell) {

		int x = in_Cell.ReadNeighbourState(0),
		y = in_Cell.ReadNeighbourState(1),
		z = in_Cell.ReadNeighbourState(2);
		int newstate;
		if ((y!=0) && (z!=0)){ // 011 & 111
			return	(m_colorStyle?y:z); // two cases for a 1
		} else 
			if ((y==0) && (x!=0)){ // two other cases for a 1 :100 & 101
			if (z==0){
				if (RandomEventDouble(m_Error)){ // error
					return 0;
				}
			}
			return x; // 
		} else {
			if ( (x!=0) && (y!=0) && (RandomEventDouble(m_Error))){
				return NOISE; // special case : "apparition" 
			}
			return 0;
		}
	}

	/*--------------------
	 *  Other methods
	 --------------------*/
	
	class ErrorController extends DoubleController{

		
		public ErrorController() {
			super("Err (%)", CONVERSIONRATE.PERCENT);			
		}

		@Override
		protected double ReadDouble() {
			return m_Error;
		}

		@Override
		protected void SetDouble(double val) {
			m_Error= val;			
		}
		
	}
	
	class ColorStyleControl extends  FLCheckBox {

		@Override
		protected void SelectionOff() {
			m_colorStyle= false;	
		}

		@Override
		protected void SelectionOn() {
			m_colorStyle= true;			
		}

	}
	
}
