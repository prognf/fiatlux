package models.CAmodels.nState;

import components.allCells.Cell;
import components.allCells.ClassicalCACell;
import components.allCells.OneRegisterIntCell;
import components.types.DoublePar;
import components.types.ProbaPar;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.elements.FLPanel;
import initializers.SuperInitializer;
import initializers.CAinit.BinaryInitializer;
import main.tables.PlotterSelectControl;
import models.CAmodels.ClassicalModel;
import topology.basics.TopologyCoder;

/*--------------------
 * random trees, random matches, trees burn
*--------------------*/
public class ForestFireModel extends ClassicalModel {

	final public static String NAME="ForestFire"; 
	final static int LIM=1; // neighb. required to transmit excitation
	private static final int EMPTY=0, TREE = 1, FIRE=2;

	/*--------------------
	 * Attributes
	 --------------------*/

	private PaintToolKit m_Color ; 
	private DoublePar 
		m_pSeed= new ProbaPar("pSeed",.01), 
		m_pMatch= new ProbaPar("pMatch",0.001),
		m_pForestPropagation= new ProbaPar("Forest Propagation",0.01);


	/*--------------------
	 * overrides & implementations
	 --------------------*/

	public SuperInitializer GetDefaultInitializer() {
		BinaryInitializer init = new BinaryInitializer();
		init.SetInitRate(0);
		return init;
	}

	@Override
	public Cell GetNewCell() {
		ClassicalModel mod = this;
		return new ClassicalCACell(mod);
	}

	@Override
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_FV4;
	}



	/*--------------------
	 * Constructor
	 --------------------*/

	public ForestFireModel() {
		super();
	}	

	/*--------------------
	 * main method
	 --------------------*/



	/* simple model */
	public final int ApplyLocalFunction(OneRegisterIntCell cell) {
		int newstate, oldstate = cell.GetState();
		newstate = oldstate;
		switch(oldstate){
		case EMPTY:
			int nTree=cell.CountOccurenceOf(TREE);
			double pPropagate=nTree*m_pForestPropagation.GetVal();
			boolean propagateTree=RandomEventDouble(pPropagate);
			if (propagateTree || RandomEventDouble(m_pSeed.GetVal()) /*|| (cell.CountOccurenceOf(TREE)>0)*/){ 
				newstate = TREE; // fresh cell burns
			}
			break;
		case TREE:
			int nfire= cell.CountOccurenceOf(FIRE);
			if (( nfire > 0) || RandomEventDouble(m_pMatch.GetVal()) ){
				newstate= FIRE;
			}
			break;
		case FIRE:
			newstate= EMPTY;
			break;
		}
		return newstate;
	}

	/*--------------------
	 * Get & Set methods
	 --------------------*/

	@Override
	public int GetMaxStateValue(){
		return FIRE;
	}
	
	@Override
	public String [] GetPlotterSelection2D() {
		return PlotterSelectControl.REACDIFFSELECT;
	}


	/*--------------------
	 * GFX
	 --------------------*/
	/* overrides */
	@Override
	public PaintToolKit GetPalette() {
		m_Color= new PaintToolKit(3);
		m_Color.SetColor(EMPTY, FLColor.c_grey1);
		m_Color.SetColor(TREE, FLColor.c_darkgreen);
		m_Color.SetColor(FIRE, FLColor.c_yellow);
		return m_Color;
	}
	

	public FLPanel GetSpecificPanel() {
		FLPanel panel= new FLPanel();
		panel.Add(m_pSeed.GetControl(), m_pMatch.GetControl(), m_pForestPropagation );
		return panel;
	}


	

}
