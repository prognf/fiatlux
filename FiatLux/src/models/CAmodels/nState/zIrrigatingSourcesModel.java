package models.CAmodels.nState;

import components.allCells.Cell;
import components.allCells.ClassicalCACell;
import components.allCells.OneRegisterIntCell;
import components.types.ProbaPar;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.elements.FLPanel;
import initializers.SuperInitializer;
import main.Macro;
import models.CAmodels.ClassicalModel;
import topology.basics.TopologyCoder;

public class zIrrigatingSourcesModel extends ClassicalModel {

		final public static String NAME="Irrigation"; 
		static final int EMPTY=0, SOURCE = 1, WATER=2;

		/*--------------------
		 * overrides & implementations
		 --------------------*/

		public SuperInitializer GetDefaultInitializer() {
			return new IrrigatingSourcesInitializer();
		}

		@Override
		public Cell GetNewCell() {
			ClassicalModel mod = this;
			return new ClassicalCACell(mod);
		}

		@Override
		public String GetDefaultAssociatedTopology() {
			return TopologyCoder.s_FV4;
		}

		/*--------------------
		 * Attributes
		 --------------------*/

		private PaintToolKit m_Color ; 
		private ProbaPar 
			m_pS= new ProbaPar("pS",0.1), 
			m_pD= new ProbaPar("pD",0.2);


		/*--------------------
		 * Constructor
		 --------------------*/

		public zIrrigatingSourcesModel() {
			super();
		}	

		/*--------------------
		 * main method
		 --------------------*/



		/* simple model */
		public final int ApplyLocalFunction(OneRegisterIntCell cell) {
			int state = cell.GetState();
			int nSource= cell.CountOccurenceOf(SOURCE);
			int nWater= cell.CountOccurenceOf(WATER);
			int nLive= nSource + nWater ;
			switch(state){
			case EMPTY:
				if ( (nLive==1) && RandomEventDouble(m_pS) ){ 
					return WATER;
				} else {
					return EMPTY;
				}
			case WATER:
				if (RandomEventDouble(m_pD) && (nLive==1)){
					return EMPTY;
				} else{
					return WATER;
				}
			case SOURCE:
				return SOURCE;
			}
			return Macro.ERRORint;
		}

		/*--------------------
		 * Get & Set methods
		 --------------------*/



		/*--------------------
		 * GFX
		 --------------------*/
		/* overrides */
		@Override
		public PaintToolKit GetPalette() {
			m_Color= new PaintToolKit(3);
			m_Color.SetColor(EMPTY, FLColor.c_white);
			m_Color.SetColor(SOURCE, FLColor.c_blue);
			m_Color.SetColor(WATER, FLColor.c_red);
			return m_Color;
		}
		

		public FLPanel GetSpecificPanel() {
			FLPanel panel= new FLPanel();
			panel.Add(m_pS.GetControl(), m_pD.GetControl() );
			return panel;
		}


		

}
