package models.CAmodels.nState;

import components.allCells.Cell;
import components.allCells.ClassicalCACell;
import components.allCells.OneRegisterIntCell;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import initializers.SuperInitializer;
import initializers.CAinit.BinaryInitializer;
import models.CAmodels.ClassicalModel;
import topology.basics.TopologyCoder;

/*--------------------
 * models excitable media
*--------------------*/
public class ModuloModel extends ClassicalModel {

	final public static String NAME="Modulo";

	private static final int MODULE = 4; 
		
	int m_module=MODULE;
	
	/*--------------------
	 * overrides & implementations
	 --------------------*/

	public SuperInitializer GetDefaultInitializer() {
		//return new ThreeStateInitializer();
		return new BinaryInitializer();
	}
	
	@Override
	public Cell GetNewCell() {
		ClassicalModel mod = this;
		return new ClassicalCACell(mod);
	}
	
	@Override
	public String GetDefaultAssociatedTopology() {
		return //TopologyCoder.s_TM8;
				TopologyCoder.s_TLR1;
	}
		
	/*--------------------
	 * Attributes
	 --------------------*/
	
	
	
	/*--------------------
	 * Constructor
	 --------------------*/
	
	public ModuloModel() {
		super();
	}	
	
	/*--------------------
	 * main method
	 --------------------*/

	public final int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
		int sum = in_Cell.GetSum(); 
		return (sum%m_module);
	}
	
	
	

	/*--------------------
	 * Get & Set methods
	 --------------------*/

	/* overrides */
	public PaintToolKit GetPalette() {
		FLColor[] palette = new FLColor[MODULE];
		palette[0] = FLColor.c_black;
		palette[1] = FLColor.c_blue;
		palette[2] = FLColor.c_green;
		palette[3] = FLColor.c_red;
		return new PaintToolKit(palette);
	}
	
	
	/*--------------------
	 * Other methods
	 --------------------*/

}
