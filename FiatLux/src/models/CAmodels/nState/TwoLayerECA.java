package models.CAmodels.nState;

import components.allCells.Cell;
import components.allCells.ClassicalCACell;
import components.allCells.OneRegisterIntCell;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import initializers.SuperInitializer;
import models.CAmodels.ClassicalModel;
import topology.basics.TopologyCoder;

/*--------------------
 * models excitable media
*--------------------*/
public class TwoLayerECA extends ClassicalModel {

	final public static String NAME="TwoLayerECA"; 
		
	
	/*--------------------
	 * overrides & implementations
	 --------------------*/

	public SuperInitializer GetDefaultInitializer() {
//		return new NStateInitializer();
		return new NStatePatternInitializer();
	}
	
	@Override
	public Cell GetNewCell() {
		ClassicalModel mod = this;
		return new ClassicalCACell(mod);
	}
	
	@Override
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TLR1;
	}
		
	/*--------------------
	 * Attributes
	 --------------------*/
	
	
	
	/*--------------------
	 * Constructor
	 --------------------*/
	
	public TwoLayerECA() {
		super();
	}	
	
	/*--------------------
	 * main method
	 --------------------*/

	public final int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
		int 
			x= in_Cell.ReadNeighbourState(0),
			y= in_Cell.ReadNeighbourState(1),		
			z= in_Cell.ReadNeighbourState(2);
		
		int 
		xr= right(x), yl= left(y), yr=right(y), zl= left(z);		
		
		int 
		newl= f30(xr,yl,yr),
		newr= f30(yl,yr,zl);
		int news= 2*newl+newr;
		/*Macro.fDebug(" (%d, %d ,%d)  (%d,%d,%d)-> %d (%d,%d,%d)-> %d [%d]", 
				x,y,z, xr,yl,yr, newl, 
				yl,yr,zl, newr, news);*/
		return news;
	}
	
	private int left(int v) {
		return v/2;
	}

	private int right(int v) {
		return v%2;
	}

	private int f30(int x, int y, int z) {
		return (x + Math.max(y, z) )%2;
	}


	/*--------------------
	 * Get & Set methods
	 --------------------*/

	/* overrides */
	public PaintToolKit GetPalette() {
		FLColor[] palette = new FLColor[4];
		palette[0] = FLColor.c_black;
		palette[1] = FLColor.c_yellow;
		palette[2] = FLColor.c_orange;
		palette[3] = FLColor.c_blue;
		return new PaintToolKit(palette);
	}
	
	
	/*--------------------
	 * Other methods
	 --------------------*/

}
