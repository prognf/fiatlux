package models.CAmodels.nState;

import javax.swing.JCheckBox;

import grafix.gfxTypes.IntField;
import grafix.gfxTypes.elements.FLPanel;
import initializers.CAinit.OneRegisterInitializer;


class DLA_TCAinit extends OneRegisterInitializer {


	final static int DEFPOPSIZE=0, DEFSQUARELEN=20;

	IntField mF_Density = new IntField("Density (%):", 3, 0);
	IntField mF_PopSize = new IntField("Population size:", 3, DEFPOPSIZE);
	IntField mF_SquareLen = new IntField("Square Len:", 3, DEFSQUARELEN);

	JCheckBox mF_Obstacle= new JCheckBox("Obstacle");

	/*--------------------
	 * abstract methods definition
	 --------------------*/
	public void SubInit() {
		ZeukodInit();
		//StaticInit();
	}

	public FLPanel GetSpecificPanel() {
		FLPanel p = new FLPanel();
		p.Add(mF_Density, mF_PopSize, mF_SquareLen);
		p.Add(mF_Obstacle);
		p.setOpaque(false);
		return p;
	}

	/*--------------------
	 * Other methods
	 --------------------*/

	private void ZeukodInit() {
		int size = GetLsize(), Xsize= GetXsize(), Ysize= GetYsize();

		// density init
		for (int cell = 0; cell < size; cell++) {
			if (RandomEventInt(mF_Density.GetValue())) {
				InitState(cell, DLA_TCAmodel.P2);
			} else {
				InitState(cell, DLA_TCAmodel.E2);
			}					
		}

		// pop init
		for (int i = 0; i < mF_PopSize.GetValue(); i++) {
			int pos = RandomPos();
			InitState(pos, DLA_TCAmodel.P2);
		}

		// borders with static cells
		for (int x=0; x<Xsize; x++){
			InitStateXY(x, 0, DLA_TCAmodel.ST);
			InitStateXY(x, Ysize-1, DLA_TCAmodel.ST);
		}
		for (int y=0; y<Ysize; y++){
			InitStateXY(0, y, DLA_TCAmodel.ST);
			InitStateXY(Xsize-1, y, DLA_TCAmodel.ST);
		}


		int L=mF_SquareLen.GetValue();
		int X= GetXsize()/2 - L/2, Y= GetYsize()/2 -  L/2 ; 
		for (int dx=0; dx < L ; dx++){
			for (int dy=0; dy < L ; dy++){
				InitStateXY(X+dx, Y+dy,DLA_TCAmodel.P2);
			}
		}

		if (mF_Obstacle.isSelected()){
			for (int dx=0; dx < L ; dx++){
				InitStateXY(X+dx, Y- 3,DLA_TCAmodel.OB);

			}
		}
	}

	

}// end class
