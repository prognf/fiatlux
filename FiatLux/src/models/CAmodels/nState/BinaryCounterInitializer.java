package models.CAmodels.nState;

import grafix.gfxTypes.IntField;
import grafix.gfxTypes.elements.FLPanel;
import initializers.CAinit.OneRegisterInitializer;

/*--------------------
 * Initializer of the model
*--------------------*/
class BinaryCounterInitializer extends OneRegisterInitializer {

	final static int DEFAULT_VAL = 4;

	IntField m_Field = new IntField("oualou", 3, DEFAULT_VAL);

	public FLPanel GetSpecificPanel() {
		return m_Field;
	}

	// n particles at the beginning
	protected void SubInit() {
		int size = GetLsize();
		InitState(size/2, BinaryCounterModel.STAR); 
	}

}// end class
