package models.CAmodels.nState;

import components.allCells.Cell;
import components.allCells.ClassicalCACell;
import components.allCells.OneRegisterIntCell;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import initializers.SuperInitializer;
import main.Macro;
import models.CAmodels.ClassicalModel;
import topology.basics.TopologyCoder;

/*--------------------
 * simple binary counter
*--------------------*/

public class BinaryCounterModel extends ClassicalModel {

	/*--------------------
	 * overrides & implementations
	 --------------------*/

	@Override
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TLR1;
	}
	
	/* overrides */
	public SuperInitializer GetDefaultInitializer() {
		return new BinaryCounterInitializer();
	}

	/* overrides */
	public Cell GetNewCell() {
		return new ClassicalCACell(this);
	}

	/*--------------------
	 * Attributes
	 --------------------*/

	/*--------------------
	 * Constructor
	 --------------------*/
	public BinaryCounterModel() {
	}

	/*--------------------
	 * // Get & Set methods
	 --------------------*/
	

	public final static int N_STATES=5;
	public static final String NAME = "BinaryCounter";
	public static final int ZERO = 0, ONE=1, STAR=2, ONESTAR=3;

	/* overrides */
	public PaintToolKit GetPalette() {
		FLColor[] palette = new FLColor[N_STATES];
		palette[ZERO] = FLColor.c_black;
		palette[ONE] = FLColor.c_blue;
		palette[STAR] = FLColor.c_yellow;
		palette[ONESTAR] = FLColor.c_green;
		return new PaintToolKit(palette);
	}

	public final int ApplyLocalFunction(OneRegisterIntCell in_Cell) {

		//int left = in_Cell.GetOccurenceOf(0);
		int state = in_Cell.GetState();
		int right = in_Cell.ReadNeighbourState(2);
		switch (state){
		case ZERO:
			return ((right == STAR) || (right == ONESTAR))? ONE: ZERO;
		case ONE:
			return ((right == STAR) || (right == ONESTAR))? ONESTAR : ONE; 
		case ONESTAR:
			return ZERO;
		case STAR:
			return STAR;
		default:
			Macro.FatalError(" unhandled state: " + state);
			return 0;
		}
	}
	/*--------------------
	 *  Other methods
	 --------------------*/
}
