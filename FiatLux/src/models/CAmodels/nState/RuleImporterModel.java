package models.CAmodels.nState;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JFileChooser;

import components.allCells.OneRegisterIntCell;
import components.types.FLfileReader;
import components.types.IntPar;
import experiment.samplers.CAsimulationSampler;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.IconManager;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.elements.FLActionButton;
import grafix.gfxTypes.elements.FLPanel;
import initializers.SuperInitializer;
import initializers.CAinit.BinaryInitializer;
import initializers.CAinit.StringInitializer;
import main.Macro;
import models.CAmodels.ClassicalModel;
import models.CAmodels.simplified.BoundConditions;
import models.CAmodels.simplified.Topologies;

public class RuleImporterModel  extends ClassicalModel {
    public static final String NAME = "RuleImporter";
    private ArrayList<Character> m_code;

    private int m_nbStates;
    private String m_path;
    private int[][][][][] m_transitionRule;

    private PaintToolKit m_Color;
    private IntPar m_StatesPar;
    private boolean m_firstInit;

    private CAsimulationSampler m_sampler;

    /*public RuleImporterModel(int nbStates, String path){
        m_nbStates = nbStates;
        m_path = path;
        m_transitionRule = new int[m_nbStates][m_nbStates][m_nbStates][m_nbStates][m_nbStates];
        FLfileReader file = FLfileReader.open(m_path);
        m_code = new ArrayList<>();
        for(char c :"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray()){
            m_code.add(c);
        }
        initTransitionRule();
        setTransitionRule(file);

    }*/
    public RuleImporterModel(){
        m_nbStates = 8;
        m_firstInit = true;
        m_path = "/models/CAmodels/nState/LangtonRule.txt";
        m_transitionRule = new int[m_nbStates+1][m_nbStates+1][m_nbStates+1][m_nbStates+1][m_nbStates+1];
        m_code = new ArrayList<>();
        for(char c :"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray()){
            m_code.add(c);
        }
        InputStream default_is = getClass().getResourceAsStream(m_path);
        String str = "";
        try (Scanner scan =  new Scanner(default_is)) {
            while (scan.hasNextLine()) {
                str +=  scan.nextLine();
            }
            System.out.println(str);
        }

        initTransitionRule();
        setTransitionRule(str);
        SetPalette();

    }
    public int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
        int[] neighbours = in_Cell.GetNeighbourhoodState();
        int newstate = m_transitionRule[neighbours[0]][neighbours[1]][neighbours[2]][neighbours[3]][neighbours[4]];
        if(newstate==-1){
            Macro.SystemWarning("Impossible cell state -1 for transition : "+neighbours[0]+", "+
                    neighbours[1]+", "+neighbours[2]+", "+neighbours[3]+", "+neighbours[4]);
        }
        return newstate;
    }

    @Override
    public SuperInitializer GetDefaultInitializer() {
        //System.out.println("W : "+m_sampler.GetViewer().getWidth() );
        if(m_sampler!=null){
            System.out.println("Sampler non null");
            if(m_sampler.GetViewer()!=null){
                System.out.println("Viever not null");
                if(m_sampler.GetViewer().getHeight()!=0){
                    System.out.println("Height not null");
                }
            }
        }

        //return new StringInitializer(m_path, m_sampler.GetViewer().getWidth(), m_sampler.GetViewer().getHeight(), m_nbStates);
        //return new models.CAmodels.nState.LangtonLoopInitializer();
        return new BinaryInitializer();
    }


    public void initTransitionRule(){
        for(int i1 = 0;i1<m_nbStates+1;i1++){
            for(int i2 = 0;i2<m_nbStates+1;i2++){
                for(int i3 = 0;i3<m_nbStates+1;i3++){
                    for(int i4 = 0;i4<m_nbStates+1;i4++){
                        for(int i5 = 0;i5<m_nbStates+1;i5++){
                            m_transitionRule[i1][i2][i3][i4][i5]=m_nbStates;
                        }
                    }
                }
            }
        }
    }

    public void setTransitionRule(FLfileReader file){
        int i1,i2,i3,i4,i5,i6;
        int max =  0;
        String line = file.readLn();
        Pattern p = Pattern.compile("(.)(.)(.)(.)(.)->(.)");
        Matcher m;
        //List<Integer> list;
        while(line !=null){
            m = p.matcher(line);
            if(m.find()){
                i1=charToInt(m.group(1));
                i2=charToInt(m.group(2));
                i3=charToInt(m.group(3));
                i4=charToInt(m.group(4));
                i5=charToInt(m.group(5));
                i6=charToInt(m.group(6));
                //list = Arrays.asList(i1, i2, i3, i4, i5,i6,max);
                //max = Collections.max(list);


                m_transitionRule[i1][i2][i3][i4][i5] = i6;
                m_transitionRule[i1][i3][i4][i5][i2] = i6;
                m_transitionRule[i1][i4][i5][i2][i3] = i6;
                m_transitionRule[i1][i5][i2][i3][i4] = i6;
            }
            line = file.readLn();

        }
        //m_nbStates = max;
        GetPalette();
    }
    public void setTransitionRule(String str){
        int i1,i2,i3,i4,i5,i6;
        Pattern p = Pattern.compile("(.)(.)(.)(.)(.)->(.)");
        Matcher m = p.matcher(str);
        while(m.find()){
            i1=charToInt(m.group(1));
            i2=charToInt(m.group(2));
            i3=charToInt(m.group(3));
            i4=charToInt(m.group(4));
            i5=charToInt(m.group(5));
            i6=charToInt(m.group(6));

            m_transitionRule[i1][i2][i3][i4][i5] = i6;
            m_transitionRule[i1][i3][i4][i5][i2] = i6;
            m_transitionRule[i1][i4][i5][i2][i3] = i6;
            m_transitionRule[i1][i5][i2][i3][i4] = i6;

        }
        GetPalette();
    }

    /** Return the int corresponding to the char ; m_nbStates if the int exceed number of states m_nbstates **/
    private int charToInt(String s){
        int result = m_code.indexOf(s.charAt(0));
        result = (result<m_nbStates)?result:m_nbStates;
        if (result==-1){
            Macro.SystemWarning("Mistake in file " + m_path + " ; State " + s.charAt(0) +
                    " exceed number of states " + m_nbStates);
        }
        return result;
    }

    public String GetDefaultAssociatedTopology() {
        return BoundConditions.Toric + Topologies.vonNeumann5;
    }
    @Override
    public PaintToolKit GetPalette() {


        return m_Color;
    }
    public void SetPalette(){
        m_Color= new PaintToolKit(m_nbStates+1);
        System.out.println("Number of states : "+m_nbStates);
        if(m_firstInit){
            m_Color.SetColor(0, FLColor.c_black);
            //System.out.println("black : "+FLColor.HSBtoRGB(0,0,0));
            m_Color.SetColor(1, FLColor.c_red);
            //m_Color.SetColor(1, new FLColor(FLColor.getHSBColor(0f,1f,1f)));
            m_Color.SetColor(2, FLColor.c_blue);
            m_Color.SetColor(3, FLColor.c_lightgreen);
            m_Color.SetColor(4, FLColor.c_yellow);
            m_Color.SetColor(5, FLColor.c_purple);
            m_Color.SetColor(6, FLColor.c_white);
            m_Color.SetColor(7, FLColor.c_lightblue);
            m_Color.SetColor(8, FLColor.c_orange);
            m_firstInit=false;
        }
        else{
            m_Color = PaintToolKit.GetCompleteRainbow(m_nbStates+1);
            System.out.println("Changed palette");
        }

    }
    public SuperInitializer setInitializer(){
        if(m_sampler!= null){
            return new StringInitializer("/home/ochaze/Documents/Telecom2A/fiatlux/FiatLux/src/models/CAmodels/nState/LangtonConfig.txt", m_sampler.GetViewer().getWidth(), m_sampler.GetViewer().getHeight(), m_nbStates);
        }
        return null;
    }

    @Override
    public FLPanel GetSpecificPanel() {
        m_StatesPar= new IntPar("number of states", 8);
        FLPanel res = FLPanel.NewPanel();
        res.Add(m_StatesPar);
        res.Add(new ButtonImportRule());
        res.Add(new ButtonApplyRule());
        res.setOpaque(false);
        return res;
    }

    class ButtonApplyRule extends FLActionButton {
        public ButtonApplyRule() {
            super("Apply rule");
        }
        @Override
        public void DoAction() {
            m_nbStates = m_StatesPar.GetVal();
            m_transitionRule = new int[m_nbStates+1][m_nbStates+1][m_nbStates+1][m_nbStates+1][m_nbStates+1];
            GetPalette();
            initTransitionRule();
            setTransitionRule(FLfileReader.open(m_path));
            SetPalette();
            m_sampler.GetViewer().SetPalette(m_Color);

        }
    }
    /** create a initializer from a text file */
    class ButtonImportRule extends FLActionButton {
        public ButtonImportRule() {
            super("Import rule");
            setIcon(IconManager.getUIIcon("openMini"));
            //AddActionListener(this);
        }

        @Override
        public void DoAction() {
            JFileChooser fc = new JFileChooser();
            int returnValue = fc.showOpenDialog(this);
            if(returnValue==fc.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                m_path = file.getPath();


            }

        }
    }

    public void SetGridSize(int width, int height){
        System.out.println("W : "+width+" H : "+height);
    }
    public void SetSampler(CAsimulationSampler sampler){
        m_sampler = sampler;
    }


}
