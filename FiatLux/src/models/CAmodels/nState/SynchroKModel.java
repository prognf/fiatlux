package models.CAmodels.nState;

import components.allCells.Cell;
import components.allCells.ClassicalCACell;
import components.allCells.OneRegisterIntCell;
import components.types.DoublePar;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.elements.FLPanel;
import initializers.SuperInitializer;
import main.Macro;
import models.CAmodels.ClassicalModel;
import topology.basics.TopologyCoder;

/*--------------------
 * random trees, dandom matches, trees burn
 *--------------------*/
public class SynchroKModel extends ClassicalModel {

	final public static String NAME="SynchroK"; 
	private static final int ZERO=0, ONE= 1, TWO=2, THREE=3;
	private static final int K=4, IN=K, EX=K+1, PS=K+2, GHARIB=K+3;

	OneRegisterIntCell m_cell;

	/*--------------------
	 * overrides & implementations
	 --------------------*/

	public SuperInitializer GetDefaultInitializer() {
		SynchroKInitializer init = new SynchroKInitializer();
		return init;
	}

	@Override
	public Cell GetNewCell() {
		ClassicalModel mod = this;
		return new ClassicalCACell(mod);
	}

	@Override
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TLR1;
	}

	/*--------------------
	 * Attributes
	 --------------------*/

	private PaintToolKit m_Color ; 
	private DoublePar 
	m_pSeed= new DoublePar("pSeed",.01), 
	m_pMatch= new DoublePar("pMatch",0.001);


	/*--------------------
	 * Constructor
	 --------------------*/

	public SynchroKModel() {
		super();
	}	

	/*--------------------
	 * main method
	 --------------------*/
	/** model assumes neighb. which includes cell itself */
	public final int ApplyLocalFunction(OneRegisterIntCell cell) {
		m_cell= cell;
	    //return QuasiPerfect();
		return Perfect();
	}

	
	private int Perfect(){
		int state = m_cell.GetState();
		int NE= m_cell.GetNeighbourhoodSize() - 1;
		if (state<K){
			if (homogeneousNeighb(state,NE)) return nextQ(state); // cyclicity
			if (countExt(IN)>0)
				return EX;
			if (countExt(EX)>0)
				return GHARIB;
			if (countExt(GHARIB)>0)
				return ZERO;
			return IN;
		}
		if (state==IN){
			return EX;
		}
		if (state==EX){
			return GHARIB;
		}
		if (state==GHARIB){
			return ZERO;
		}
		return -999;
	}
	
	private int QuasiPerfect(){
		int state = m_cell.GetState();
		int 	sI= countExt(IN),
				sE= countExt(EX);
		int NE= m_cell.GetNeighbourhoodSize() - 1;
		switch(state){
		case EX:
			return IN;
		case IN:
			if (coex(ZERO,ONE)||coex(ONE,TWO)||coex(TWO,ZERO)){
				String s= String.format("[%d %d %d]", countExt(ZERO), countExt(ONE), countExt(TWO));
				Macro.SystemWarning("Panic on I: " + s);
			}
			boolean flag=false;//for checking only
			int qtrans=-1;
			for (int q=0; q<K; q++){
				if (countExt(q)>0){
					qtrans=q;
					if (flag){
						Macro.SystemWarning("Panic2 on I");
					} else {
						flag=true;
					}
				}
			}
			if (flag) 
				return nextQ(qtrans); // "inside" transition to next 
			return TWO;  // zero-one-two for isolated 0
		case ZERO:
			if (homogeneousNeighb(ZERO,NE)) return nextQ(ZERO); // cyclicity
			if (sE>0) return EX; // propagation of E (NO exception for ZERO !!!)
			if (sI>0) return ONE; // extension
			if (countExt(PS)>0){
				return PS;
			}
			return IN;
		case ONE:
		case TWO:
		case THREE:
			if (homogeneousNeighb(state,NE)) return nextQ(state); // cyclicity
			if (countExt(ZERO)>0) return EX; // only first time ??
			if (sE>0) return EX; // propagation of E 
			if (sI>0) return nextQ(state); // extension
			return PS;
		case PS:
			if (sE>0) return EX;
			return PS;
		case GHARIB:
			return GHARIB;
		default:
			Macro.FatalError("unhandled switch case");
		}
		return -999;
	}

	private boolean homogeneousNeighb(int q, int NEsize) {
		return countExt(q)==NEsize;
	}

	private int nextQ(int q) {
		return (q+1) % K;
	}

	private boolean coex(int q1, int q2) {
		return (countExt(q1)>0) && (countExt(q2)>0);
	}

	private int countExt(int q) {
		int count= m_cell.CountOccurenceOf(q);
		int delta= ( q==m_cell.GetState() ) ?1:0;
		return count - delta;
	}

	/*--------------------
	 * Get & Set methods
	 --------------------*/


	/*--------------------
	 * GFX
	 --------------------*/
	/* overrides */
	@Override
	public PaintToolKit GetPalette() {
		m_Color= new PaintToolKit(8);
		//Macro.Debug("Size col:" + m_Color.GetSize());
		m_Color.SetColor(ZERO, FLColor.c_white);
		m_Color.SetColor(ONE, FLColor.c_yellow);
		m_Color.SetColor(TWO, FLColor.c_orange);
		m_Color.SetColor(THREE, FLColor.c_red);
		
		/*m_Color.SetColor(IN, FLColor.c_blue);
		m_Color.SetColor(EX, FLColor.c_black);
		m_Color.SetColor(EX, FLColor.c_purple);*/
		m_Color.SetColor(IN, FLColor.c_darkblue);
		m_Color.SetColor(EX, FLColor.c_blue);
		//m_Color.SetColor(EX, FLColor.);
		
		m_Color.SetColor(PS, FLColor.c_grey1);
		m_Color.SetColor(GHARIB, FLColor.c_purple);
		
		return m_Color;
	}


	public FLPanel GetSpecificPanel() {
		FLPanel panel= new FLPanel();
		panel.Add(m_pSeed.GetControl(), m_pMatch.GetControl() );
		return panel;
	}




}
