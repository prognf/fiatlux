package models.CAmodels.nState;

import components.types.IntegerList;
import grafix.gfxTypes.IntField;
import grafix.gfxTypes.elements.FLPanel;
import initializers.CAinit.OneRegisterInitializer;

/*--------------------
 * Initializer of the model
 *--------------------*/
class VoronoiInitializer extends OneRegisterInitializer {

	final static int DEFAULT_VAL = 4;
	
	IntField m_Field = new IntField("Number of origins");

	public FLPanel GetSpecificPanel() {
		m_Field.SetValue(DEFAULT_VAL);
		return m_Field;
	}

	// n particles at the beginning
	protected void SubInit() {
		int k= m_Field.GetValue();
		IntegerList posInit = RandomKpositions(k);
		for (int i=0; i < k; i++) {
			InitState(posInit.Get(i), i % (VoronoiModel.N_STATES-2) + 1); 
		}
	}

}// end class
