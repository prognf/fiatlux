package models.CAmodels.nState;

import components.types.IntPar;
import grafix.gfxTypes.elements.FLPanel;
import initializers.CAinit.OneRegisterInitializer;

/*--------------------
 * Initializer of the model
 *--------------------*/
class NStateInitializer extends OneRegisterInitializer {
	/*--------------------
	 * attributes
	 --------------------*/

	IntPar m_InitStyle = new IntPar("Init style",0);

	/*--------------------
	 * implementations
	 --------------------*/

	public FLPanel GetSpecificPanel() {
		return m_InitStyle.GetControl();
	}

	// n particles at the beginning
	protected void SubInit() {

		switch(m_InitStyle.GetVal()) {
		case 2:
			BlueStyle();
			return;
		case 1:
			PureBernoulli();
			return;
		default:
			OneOnly();
			return;
		}
	}

	private void OneOnly() {
		int size = GetLsize();
		InitState(size/2, 2); 
	}

	private void BlueStyle() {
		int size = GetLsize();
		for (int cell = 0; cell < size; cell++) {
			int state=0;
			if (RandomInt(2)==0) {
				state=1;
			}	
			InitState(cell, state); 
		}
	}

	private void PureBernoulli() {
		int size = GetLsize();
		for (int cell = 0; cell < size; cell++) {
			int state=RandomInt(4);
			InitState(cell, state); 
		}
	}
	/*--------------------
	 * get / set
	 --------------------*/

}// end class
