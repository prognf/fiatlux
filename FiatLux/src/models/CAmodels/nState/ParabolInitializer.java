package models.CAmodels.nState;

import initializers.CAinit.OneRegisterInitializer;

public class ParabolInitializer extends OneRegisterInitializer {
    @Override
    protected void SubInit() {
        int center = (int)(GetLsize()/2);
        InitState(center, 1);
        InitState(center-1, 2);
        InitState(center+1, 2);
    }
}
