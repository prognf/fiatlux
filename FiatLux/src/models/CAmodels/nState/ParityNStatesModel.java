package models.CAmodels.nState;

import components.allCells.OneRegisterIntCell;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import initializers.SuperInitializer;
import models.CAmodels.ClassicalModel;

public class ParityNStatesModel extends ClassicalModel {

    public final static String NAME = "ParityStates";
    final static int N_STATES=10;
    private PaintToolKit m_Color;


    @Override
    public int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
        int[] neighbours = in_Cell.GetNeighbourhoodState();
        int newstate=0;
        for(int state : neighbours){
            newstate += state;
        }
        newstate = newstate%N_STATES;
        return newstate;
    }

    @Override
    public SuperInitializer GetDefaultInitializer() {
        return new ParityNStatesInitializer(N_STATES);
    }

    /*--------------------
     * GFX
     --------------------*/
    @Override
    public PaintToolKit GetPalette() {
        m_Color= new PaintToolKit(10);
        m_Color.SetColor(0, FLColor.c_black);
        m_Color.SetColor(1, FLColor.c_red);
        m_Color.SetColor(2, FLColor.c_orange);
        m_Color.SetColor(3, FLColor.c_yellow);
        m_Color.SetColor(4, FLColor.c_lightgreen);
        m_Color.SetColor(5, FLColor.c_green);
        m_Color.SetColor(6, FLColor.c_blue);
        m_Color.SetColor(7, FLColor.c_lightblue);
        m_Color.SetColor(8, FLColor.c_lightpurple);
        m_Color.SetColor(9, FLColor.c_purple);



        return m_Color;
    }
}
