package models.CAmodels.nState;

import javax.swing.JCheckBox;

import grafix.gfxTypes.IntField;
import grafix.gfxTypes.elements.FLPanel;
import initializers.CAinit.OneRegisterInitializer;


class Traffic2DtransactionalModellInit extends OneRegisterInitializer {


	final static int DEFPOPSIZE=0, DEFSQUARELEN=20;

	IntField mF_Density = new IntField("Density (%):", 3, 0);
	IntField mF_PopSize = new IntField("Population size:", 3, DEFPOPSIZE);
	IntField mF_SquareLen = new IntField("Square Len:", 3, DEFSQUARELEN);

	JCheckBox mF_Obstacle= new JCheckBox("Obstacle");

	/*--------------------
	 * abstract methods definition
	 --------------------*/
	public void SubInit() {
		ZeukodInit();
		//StaticInit();
	}

	public FLPanel GetSpecificPanel() {
		FLPanel p = FLPanel.NewPanelVertical(mF_Density, mF_PopSize, mF_SquareLen, mF_Obstacle);
		p.setOpaque(false);
		return p;
	}

	/*--------------------
	 * Other methods
	 --------------------*/

	private void ZeukodInit() {
		int size = GetLsize(), Xsize= GetXsize(), Ysize= GetYsize();

		// density init
		for (int pos = 0; pos < size; pos++) {
			if (RandomEventInt(mF_Density.GetValue())) {
				InitState(pos, Traffic2DtransactionalModel.P2);
			} else {
				InitState(pos, Traffic2DtransactionalModel.E2);
			}					
		}

		// pop init
		for (int i = 0; i < mF_PopSize.GetValue(); i++) {
			int pos = RandomPos();
			InitState(pos, Traffic2DtransactionalModel.P2);
		}

/*		// borders with static cells
		for (int x=0; x<Xsize; x++){
			InitStateXY(x, 0, Traffic2DtransactionalModel.ST);
			InitStateXY(x, Ysize-1, Traffic2DtransactionalModel.ST);
		}
		for (int y=0; y<Ysize; y++){
			InitStateXY(0, y, Traffic2DtransactionalModel.ST);
			InitStateXY(Xsize-1, y, Traffic2DtransactionalModel.ST);
		}
*/

		int L=mF_SquareLen.GetValue();
		int X= GetXsize()/2 - L/2, Y= GetYsize()/2 -  L/2 ; 
		for (int dx=0; dx < L ; dx++){
			for (int dy=0; dy < L ; dy++){
				InitStateXY(X+dx, Y+dy,Traffic2DtransactionalModel.P2);
			}
		}

		if (mF_Obstacle.isSelected()){
			for (int dx=0; dx < L ; dx++){
				InitStateXY(X+dx, Y- 3,Traffic2DtransactionalModel.OB);

			}
		}
	}

	

}// end class
