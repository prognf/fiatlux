package models.CAmodels.nState;

import components.allCells.Cell;
import components.allCells.ClassicalCACell;
import components.allCells.OneRegisterIntCell;
import components.types.DoublePar;
import components.types.ProbaPar;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.elements.FLPanel;
import initializers.SuperInitializer;
import models.CAmodels.ClassicalModel;
import models.CAmodels.decentralisedDiagnosis.NGIsingStabInitializer;
import topology.basics.TopologyCoder;

public class IsingStab extends ClassicalModel {
	public static final String NAME = "Ising-Stab";
	public final static int ALERT = 2, NORMAL = 0, FAIL = 1;
	public static double LAMBDA = 10;
	private DoublePar m_lambda = new DoublePar("lambda", LAMBDA);
	private ProbaPar m_failure = new ProbaPar("failure", 0.0); // 0.00005);
	public final static double k = 1;

	@Override
	public SuperInitializer GetDefaultInitializer() {
		return new NGIsingStabInitializer();
	}

	public IsingStab() {
		super();
	}

	@Override
	public PaintToolKit GetPalette() {
		PaintToolKit palette = PaintToolKit.EmptyPalette();
		palette.AddColorRGB(255, 255, 255);
		palette.AddColorRGB(0, 0, 0);
		palette.AddColorRGB(255, 0, 0);
		return palette;
	}

	public void setFailureProbability(double probability) {
		m_failure.SetVal(probability);
	}

	@Override
	public Cell GetNewCell() {
		ClassicalModel mod = this;
		return new ClassicalCACell(mod);
	}

	public void setLambda(double lambda) {
		LAMBDA = lambda;
		m_lambda.SetVal(lambda);
	}

	@Override
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TM9;
	}

	public final int ApplyLocalFunction(OneRegisterIntCell inCell){
		return MajorityRule(inCell);
	}
	
	public int MajorityRule(OneRegisterIntCell inCell) {
		if (inCell.GetState() == FAIL) {
			return FAIL;
		}

		if (RandomEventDouble(m_failure)) {
			return FAIL;
		}

		double A = inCell.CountOccurenceOf(ALERT);
		double N = inCell.CountOccurenceOf(NORMAL);
		double D = inCell.CountOccurenceOf(FAIL);
		double S = A + N + D;

		double lambda = m_lambda.GetVal();

		double p1;
		boolean versionRapport = true;


		if (versionRapport) {
			p1 = Math.exp((N / S) * lambda) / (Math.exp((N / S) * lambda) + Math.exp(((A) / S) * lambda) + Math.exp(((D) / S) * lambda));
		} else {
			p1 = Math.exp( (N/S) * lambda ) / ( Math.exp( (N/S) * lambda) + Math.exp( ((A+D)/S) * lambda)  );
		}

		if (N == 0) {
			return ALERT;
		}

		if (!versionRapport && (A == 0) && (D == 0)) {
			p1 = 1;
		}

		if (RandomEventDouble(p1)) {
			return NORMAL;
		}

		return ALERT;
	}

	public FLPanel GetSpecificPanel() {
		FLPanel p = new FLPanel();
		p.Add(m_lambda, m_failure);

		return	p;
	}
}

