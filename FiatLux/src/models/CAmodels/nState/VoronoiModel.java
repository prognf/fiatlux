package models.CAmodels.nState;

import components.allCells.Cell;
import components.allCells.ClassicalCACell;
import components.allCells.OneRegisterIntCell;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import initializers.SuperInitializer;
import models.CAmodels.ClassicalModel;

/*--------------------
 * Voronoi diagrams with various colours
*--------------------*/

public class VoronoiModel extends ClassicalModel {

	/*--------------------
	 * overrides & implementations
	 --------------------*/

	/* overrides */
	public SuperInitializer GetDefaultInitializer() {
		return new VoronoiInitializer();
	}

	/* overrides */
	public Cell GetNewCell() {
		return new ClassicalCACell(this);
	}

	/*--------------------
	 * Attributes
	 --------------------*/

	/*--------------------
	 * Constructor
	 --------------------*/
	public VoronoiModel() {
	}

	/*--------------------
	 * // Get & Set methods
	 --------------------*/
	

	final static int N_STATES=5;
	public static final String NAME = "Voronoi";
	
	/* overrides */
	public PaintToolKit GetPalette() {
		FLColor[] palette = new FLColor[N_STATES];
		palette[0] = FLColor.c_black;
		palette[1] = FLColor.c_blue;
		palette[2] = FLColor.c_yellow;
		palette[3] = FLColor.c_green;
		palette[4] = FLColor.c_red;
		return new PaintToolKit(palette);
	}

	public final int ApplyLocalFunction(OneRegisterIntCell in_Cell) {

		int nA = in_Cell.CountOccurenceOf(1);
		int nB = in_Cell.CountOccurenceOf(2);
		int nC = in_Cell.CountOccurenceOf(3);
		int predstate = in_Cell.GetState();
		int newstate = predstate;
		if (predstate == 0) {
			if (nA + nB + nC == 0) {
				newstate = 0;
			} else if (nA + nB == 0) {
				newstate = 3;
			} else if (nA + nC == 0) {
				newstate = 2;
			} else if (nB + nC == 0) {
				newstate = 1;
			} else {
				newstate = 4;
			}
		}
		return newstate;
	}

	/*--------------------
	 *  Other methods
	 --------------------*/
}
