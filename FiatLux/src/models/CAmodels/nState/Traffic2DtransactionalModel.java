package models.CAmodels.nState;

import components.allCells.Cell;
import components.allCells.ClassicalCACell;
import components.allCells.OneRegisterIntCell;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import initializers.SuperInitializer;
import main.Macro;
import models.CAmodels.ClassicalModel;
import topology.basics.TopologyCoder;
import topology.zoo.vonNeumannTM;

/*--------------------
 * ???
*--------------------*/
public class Traffic2DtransactionalModel extends ClassicalModel {


	/*--------------------
	 * overrides & implementations
	 --------------------*/

	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TV5;
	}

	public SuperInitializer GetDefaultInitializer() {
		return new Traffic2DtransactionalModellInit();
	}

	public Cell GetNewCell() {
		ClassicalModel mod = this;
		return new ClassicalCACell(mod);
	}

	public final PaintToolKit GetPalette(){
		return new PaintToolKit(PALETTE);
	}

	/*--------------------
	 * Attributes
	 --------------------*/


	final public static String NAME="Traffic2DTCA"; 

	// used in RandomDirAmongEmptyCells to build the array of empty neighbors
	private int[] FreeDirectionBuffer= new int[direction.length];

	static int[] direction = {
		vonNeumannTM.NORTH,vonNeumannTM.EAST,
		vonNeumannTM.SOUTH,vonNeumannTM.WEST
	};


	final public static int
	E0  =  0,
	E1  =  1,
	E2  =  2,
	R1  =  3,
	P0N =  4, P0S =  5, P0E =  6, P0W =  7, P0I =  8,
	P1N =  9, P1S = 10, P1E = 11, P1W = 12,	P1I = 13,
	P2  = 14, C1  = 15, OB = 16; 
	//ST = 17; // fixed static agent

	static int[] ST_receptor = {
		P0S,P0W,P0N,P0E
	};
	static int[] ST_sender = {
		P0N,P0E,P0S,P0W   //,P0I
	};

	final static public FLColor COL1= FLColor.c_purple;
	final public static FLColor [] PALETTE = 
	{	FLColor.c_white, //E0
		FLColor.c_white, //E1
		FLColor.c_white, //E2
		FLColor.c_grey2, //R1
		FLColor.c_red,   //P0N
		FLColor.c_red,   //P0S
		FLColor.c_red,   //P0E
		FLColor.c_red,   //P0W
		FLColor.c_green, //P0I
		COL1,//P1N
		COL1,//P1S
		COL1,//P1E
		COL1,//P1W
		FLColor.c_green, //P1I
		FLColor.c_blue,//P2
		FLColor.c_black,  //C1
		FLColor.c_orange,  // obstacle
		FLColor.c_black  // aggregate
	};


	/*--------------------
	 * Constructor
	 --------------------*/
	public Traffic2DtransactionalModel() {
	}

	/*--------------------
	 * Get & Set methods
	 --------------------*/


	private boolean GoodReception(OneRegisterIntCell in_Cell, int i) {
		return in_Cell.ReadNeighbourState(direction[i]) == ST_receptor[i];
	}

	private int RandomDirAmongEmptyCells(OneRegisterIntCell in_Cell) {

		int count= 0;

		for(int dir= 0; dir< 2; dir++){
			int neighbor_state= in_Cell.ReadNeighbourState(direction[dir]);
			if (neighbor_state == E2){
				FreeDirectionBuffer[count]= dir;
				count++;
			}
		}

		if (count > 0) {
			int choosen_dir= FreeDirectionBuffer[RandomInt(count)];
			return (ST_sender[choosen_dir] );
		} else {
			return P0I;
		}
	}

	/* used with perfect cells */
	public final int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
		int oldstate = in_Cell.GetState();
		switch(oldstate) {
		case E0: {
			for(int i=0;i<ST_receptor.length;i++) { // all directions
				if (GoodReception(in_Cell,i)) { 
					for(int j=i+1;j<ST_receptor.length;j++)
						if (GoodReception(in_Cell,j)) return C1;
					return R1;
				}
			}
			return E1;
		}
		case E1: return E2; 
		case E2: return E0; 
		case R1: return P2; 
		case P0N: return P1N; 
		case P0S: return P1S; 
		case P0E: return P1E; 
		case P0W: return P1W; 
		case P0I: return P1I; 
		case P2:
				return RandomDirAmongEmptyCells(in_Cell);
		case P1N: if (in_Cell.ReadNeighbourState(vonNeumannTM.NORTH) == R1) return E2; else return P2;
		case P1E: if (in_Cell.ReadNeighbourState(vonNeumannTM.EAST) == R1) return E2; else return P2;
		case P1S: if (in_Cell.ReadNeighbourState(vonNeumannTM.SOUTH) == R1) return E2; else return P2;
		case P1W: if (in_Cell.ReadNeighbourState(vonNeumannTM.WEST) == R1) return E2; else return P2;
		case P1I: return P2;
		case C1: return E2;
		case OB: return OB;
		default: 
			Macro.FatalError(this,"GetTransitionResult","State doesn't exist "+oldstate);
		return -1;
		}


	}


	/*--------------------
	 * Other methods
	 --------------------*/

}
