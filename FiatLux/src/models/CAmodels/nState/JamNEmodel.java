package models.CAmodels.nState;

import components.allCells.OneRegisterIntCell;
import components.types.ProbaPar;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.elements.FLPanel;
import initializers.SuperInitializer;
import models.CAmodels.ClassicalModel;
import topology.basics.TopologyCoder;
import topology.zoo.vonNeumannTM;

/*--------------------
 * ???
*--------------------*/
public class JamNEmodel extends ClassicalModel {


	final static int EMPTY=0, VN=1, VE=2, VNW=3, VEW=4;

	private static final double DEF_radio = 0.001;

	/*--------------------
	 * attributes
	 --------------------*/

	ProbaPar m_radio = new ProbaPar("radio",DEF_radio);
	
	/*--------------------
	 * overrides & implementations
	 --------------------*/

	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TV5;
	}

	public SuperInitializer GetDefaultInitializer() {
		return new JamNEinitializer();
	}

	/*public Cell GetNewCell() {
		ClassicalModel mod = this;
		return new ClassicalCACell(mod);
	}*/

	public final PaintToolKit GetPalette(){
		return new PaintToolKit(PALETTE);
	}

	/*--------------------
	 * Attributes
	 --------------------*/


	final public static String NAME="JamNE"; 

	/*static int[] direction = {
		vonNeumannTM.NORTH,vonNeumannTM.EAST,
		vonNeumannTM.SOUTH,vonNeumannTM.WEST
	};*/

	
	public FLPanel GetSpecificPanel(){
		return FLPanel.NewPanel(m_radio.GetControl(), this.GetRandomizer());
	}

	final public static FLColor [] PALETTE = 
	{	FLColor.c_white, //
		FLColor.c_red,   //
		FLColor.c_green, //
		FLColor.c_brown, //
		FLColor.c_darkgreen // 
	};


	/*--------------------
	 * Constructor
	 --------------------*/
	
	/*--------------------
	 * Get & Set methods
	 --------------------*/



	/* used with perfect cells */
	public final int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
		int o= in_Cell.GetState();
		int n= in_Cell.ReadNeighbourState(vonNeumannTM.NORTH);
		int e= in_Cell.ReadNeighbourState(vonNeumannTM.EAST);
		int s= in_Cell.ReadNeighbourState(vonNeumannTM.SOUTH);
		int w= in_Cell.ReadNeighbourState(vonNeumannTM.WEST);

		if (o==EMPTY){
			if (s==VN){
				return VNW;
			} else if (w==VE){
				return VEW;
			} else 
				return EMPTY;
		} else if (o==VN){
			return (n==EMPTY)?EMPTY:VNW;
		} else if (o==VE){
			return (e==EMPTY)?EMPTY:VEW;
		} else if (o==VEW){
			return RandomEventDouble(m_radio)?VNW:VE;
		} else if (o==VNW){
			return RandomEventDouble(m_radio)?VEW:VN;
		}
		return -1;
	}


	/*--------------------
	 * Other methods
	 --------------------*/

}
