package models.CAmodels.nState;

import components.allCells.Cell;
import components.allCells.ClassicalCACell;
import components.allCells.OneRegisterIntCell;
import components.types.ProbaPar;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.controllers.IntController;
import grafix.gfxTypes.elements.FLPanel;
import initializers.SuperInitializer;
import main.MathMacro;
import main.tables.PlotterSelectControl;
import models.CAmodels.ClassicalModel;
import topology.basics.TopologyCoder;

/*--------------------
 * models excitable media
 *--------------------*/
public class ReacDiffModel extends ClassicalModel {

	final public static String NAME="Reac-Diff"; 
	final static int LIM=1; // neighb. required to transmit excitation

	/*--------------------
	 * overrides & implementations
	 --------------------*/

	public SuperInitializer GetDefaultInitializer() {
		return new ReacDiffInitializer(this);
		//return new ThreeStateInitializer();
	}

	@Override
	public Cell GetNewCell() {
		ClassicalModel mod = this;
		return new ClassicalCACell(mod);
	}

	@Override
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_FV4;
	}

	/*--------------------
	 * Attributes
	 --------------------*/


	private int m_MAXSTATE = 5;
	private ProbaPar m_pTr;

	// Dynamically processing of the palette when changing the num of states
	private PaintToolKit m_palette; 

	/*--------------------
	 * Constructor
	 --------------------*/

	public ReacDiffModel() {
		super();
		m_pTr= new ProbaPar("pTr", 1.);
	}	

	/*--------------------
	 * main method
	 --------------------*/



	/** Greenberg-Hastings model: simple reac-diff model */
	public final int ApplyLocalFunction(OneRegisterIntCell cell) {
		return GreenbergHastings(cell);
		//return Variant(cell);
	}

	/*--------------------
	 * Get & Set methods
	 --------------------*/

	private int Variant(OneRegisterIntCell cell) {
		int[] stNeighb= cell.GetNeighbourhoodState();
		int max = MathMacro.Max(stNeighb);
		int st= cell.GetState();
		if (st==m_MAXSTATE){
			return st;
		} else {
			int diff= max - st;
			if ((diff>0) && RandomEventDouble(m_pTr) ){
				return st + diff / 2;
			} else {
				int evap = (int) ((double)st/1.5); 
				return evap;
			}
		}
	}

	private int GreenbergHastings(OneRegisterIntCell cell) {
		int state = cell.GetState();
		if (state == 0) { // fresh cell
			int count= cell.CountOccurenceOf(m_MAXSTATE); 
			boolean burn =
					(count > LIM - 1)
					&& RandomEventDouble(m_pTr);
			return burn? m_MAXSTATE : 0; // fresh cell burns
		} else { 
			return state - 1;
		}
		
	}

	@Override
	final public int GetMaxStateValue() {
		return m_MAXSTATE;
	}


	@Override
	public String [] GetPlotterSelection2D() {
		//Macro.Debug("plotttter!");
		return PlotterSelectControl.REACDIFFSELECT;
	}


	public void SetMValue(int m) {
		m_MAXSTATE= m;		
	}

	/*--------------------
	 * GFX
	 --------------------*/
	/** overrides */
	@Override
	public PaintToolKit GetPalette() {
		m_palette= PaintToolKit.EmptyPalette();
		ReBuildPalette();
		return m_palette;
	}


	private void ReBuildPalette() {
		//Macro.Debug("(re)building palette of size:" + m_MAXSTATE);
		m_palette.SetColorsRedShift(m_MAXSTATE);
		m_palette.SetColor(m_MAXSTATE, FLColor.c_orange);
		//m_palette.Print();
	}

	public FLPanel GetSpecificPanel() {
		FLPanel panel= FLPanel.NewPanel(new MaxStateControl(), m_pTr.GetControl());
		return panel;
	}


	class MaxStateControl extends IntController {

		public MaxStateControl() {
			super("M value");
		}

		@Override
		protected int ReadInt() {
			return m_MAXSTATE;
		}

		@Override
		protected void SetInt(int val) {
			SetMValue(val);
			ReBuildPalette();			
		}

	}
}
