package models.CAmodels.nState;

import components.types.ProbaPar;
import grafix.gfxTypes.elements.FLPanel;
import initializers.CAinit.OneRegisterInitializer;

public class SynchroKInitializer extends OneRegisterInitializer {
	final static String CLASSNAME= Thread.currentThread().getStackTrace()[1].getClassName();

	ProbaPar mF_pZero= new ProbaPar("Pzero", 0.1);
	
	/*--------------------
	 * attributes
	 --------------------*/

	/*--------------------
	 * construction
	 --------------------*/

	/*--------------------
	 * implementations
	 --------------------*/

	public void SubInit() {
		for (int pos=0; pos< GetLsize(); pos++){
			int q;
			if (RandomEventDouble(mF_pZero)){
				q=0;
			} else {
				int deltaq=0;
				if (RandomEventDouble(mF_pZero)){
					deltaq=1;
				}
				q= 1+ deltaq;  //RandomInt(2);
			}
			InitState(pos, q);
		}
	}

	public FLPanel GetSpecificPanel() {
		FLPanel panel = FLPanel.NewPanel(mF_pZero, GetRandomizer());
		panel.setOpaque(false);
		return panel;
	}

	/*--------------------
	 * Other methods
	 --------------------*/
}