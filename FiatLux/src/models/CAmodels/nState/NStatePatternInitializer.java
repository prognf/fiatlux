package models.CAmodels.nState;

import grafix.gfxTypes.elements.FLPanel;
import grafix.gfxTypes.elements.FLTextField;
import initializers.CAinit.OneRegisterInitializer;

/*--------------------
 * Initializer of the model
 *--------------------*/
class NStatePatternInitializer extends OneRegisterInitializer {
	
	private static final int SZ_PATTERN_PARSER = 20;

	private static final String INI = "1";
	
	/*--------------------
	 * attributes
	 --------------------*/

	PatternParser m_parser= new PatternParser();
	String m_initStr= INI;

	/*--------------------
	 * implementations
	 --------------------*/

	public FLPanel GetSpecificPanel() {
		FLPanel p= FLPanel.NewPanel(m_parser);
		return p;
	}

	// n particles at the beginning
	protected void SubInit() {
		int L= GetLsize();
		for (int i=0; i<L; i++) {
			InitState(i, 0);
		}
		int W=m_initStr.length();
		for (int pos=0;pos<W;pos++) {
			char c= m_initStr.charAt(pos);
			int cellPos=L/2-W/2+pos;
			int state=c>'0'?c-'0':0;
			InitState(cellPos, state);
		}
	}

	/*--------------------
	 * get / set
	 --------------------*/
	
	/*--------------------
	 * inner class
	 --------------------*/
	
	class PatternParser extends FLTextField {

		protected PatternParser() {
			super(SZ_PATTERN_PARSER);
			this.setText("1");
		}

		@Override
		public void parseInput(String input) {
			if (input.length()>0){
				m_initStr= input;
			}
		}

		/*public void focusGained(FocusEvent e) {
			this.setText(m_InitPattern);
		}*/

	}	

}// end class
