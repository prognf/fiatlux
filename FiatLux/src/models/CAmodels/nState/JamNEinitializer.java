package models.CAmodels.nState;

import components.types.IntPar;
import components.types.IntegerList;
import components.types.ProbaPar;
import grafix.gfxTypes.elements.FLPanel;
import initializers.CAinit.OneRegisterInitializer;

/*--------------------
 * Initializer of the model
*--------------------*/
class JamNEinitializer extends OneRegisterInitializer {
	final static int DEF_NORIGIN = 0;
	final static double DEF_dens = .10;
	
	IntPar mp_norigin = new IntPar("Number of origins", DEF_NORIGIN);
	ProbaPar mp_dens = new ProbaPar("dens", DEF_dens);

	public FLPanel GetSpecificPanel() {
		FLPanel panel = FLPanel.NewPanelConstruct(mp_dens, mp_norigin);
		panel.setOpaque(false);
		return panel;
	}

	// n particles at the beginning
	protected void SubInit() {
		int k= mp_norigin.GetVal();
		IntegerList posInit = RandomKpositions(k);
		for (int i=0; i < k; i++) {
			int pos= posInit.Get(i);
			int st= 3*RandomInt(2)+1; // state 1 or 4
			InitState(pos, st); 
		}
		
		for (int pos=0; pos<GetLsize(); pos++){
			int st=(RandomEventDouble(mp_dens))?
					3*RandomInt(2)+1:0; // state 1 or 4
			InitState(pos,st);
		}
		
	}

}// end class
