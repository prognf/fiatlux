package models.CAmodels.nState;

import components.allCells.Cell;
import components.allCells.ClassicalCACell;
import components.allCells.OneRegisterIntCell;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import initializers.SuperInitializer;
import initializers.CAinit.UniformInit;
import models.CAmodels.ClassicalModel;
import topology.basics.TopologyCoder;

/*--------------------
 * solution by Ana Busic to density classification (from F. Benezit construction)
 * @author Nazim Fates 
*--------------------*/

public class MajorityVotingModel extends ClassicalModel {

	public final static String NAME = "MajorityVoting";
	final static int N_STATES=4;
	private static final int STATEZERO=0, STATEMINUS=1, STATEPLUS= 2,STATEONE = 3;

	/*--------------------
	 * overrides & implementations
	 --------------------*/

	public SuperInitializer GetDefaultInitializer() {
		UniformInit init= new UniformInit(); 
		init.SetStateRange(N_STATES);
		return init;
	}

	public Cell GetNewCell() {
		return new ClassicalCACell(this);
	}
	
	public String GetDefaultAssociatedTopology() {
			return TopologyCoder.s_TLR2;
		}

	/*--------------------
	 * Attributes
	 --------------------*/

	/*--------------------
	 * Constructor
	 --------------------*/
	public MajorityVotingModel() {
	}

	/*--------------------
	 * // Get & Set methods
	 --------------------*/
	


	/* overrides */
	public PaintToolKit GetPalette() {
		FLColor[] palette = new FLColor[N_STATES];
		palette[0] = FLColor.c_blue;
		palette[1] = FLColor.c_cyan;
		palette[2] = FLColor.c_yellow;
		palette[3] = FLColor.c_red;
		return new PaintToolKit(palette);
	}

	public final int ApplyLocalFunction(OneRegisterIntCell in_Cell) {

		int s= in_Cell.ReadNeighbourState(2);
		int newstate=s;
		int right= in_Cell.ReadNeighbourState(3);
		int left= in_Cell.ReadNeighbourState(1);
		switch(s){
		case STATEZERO:
			if( right == STATEONE){
				newstate= STATEPLUS;
			} else if (right == STATEPLUS){
				newstate= STATEMINUS;
			} else if (right== STATEMINUS){
				int rightmost= in_Cell.ReadNeighbourState(4);
				if (rightmost != STATEONE){
					newstate= STATEMINUS;
				}
			}
			break;
		case STATEMINUS:
			if (right == STATEONE){
				newstate= STATEONE;
			} else {
				if (left == STATEZERO){
					newstate= STATEZERO;
				}
			}
			break;
		case STATEPLUS:
			if (left == STATEZERO){
				newstate= STATEZERO;
			} else {
				if (right == STATEONE){
					newstate= STATEONE;
				}
			}

			break;
		case STATEONE:
			if( left == STATEZERO){
				newstate= STATEMINUS;
			} else if (left == STATEMINUS){
				newstate= STATEPLUS;
			} else if (left == STATEPLUS){
				int leftmost= in_Cell.ReadNeighbourState(0);
				if (leftmost != STATEZERO){
					newstate= STATEPLUS;
				}
			}
			break;
		}
		return newstate;
	}

	/*--------------------
	 *  Other methods
	 --------------------*/
}
