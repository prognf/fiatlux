package models.CAmodels.nState;

import components.allCells.Cell;
import components.allCells.ClassicalCACell;
import components.allCells.OneRegisterIntCell;
import grafix.gfxTypes.PaintToolKit;
import initializers.SuperInitializer;
import initializers.CAinit.CyclicInitializer;
import models.CAmodels.ClassicalModel;


/*--------------------
 * The Cyclic model is another toy model 
*--------------------*/
public class CyclicModel extends ClassicalModel {

	final static String NAME="Cyclic";
	
	/*--------------------
	 * overrides & implementations
	 --------------------*/

	public SuperInitializer GetDefaultInitializer() {
		return new CyclicInitializer();
	}

	public Cell GetNewCell() {
		ClassicalModel mod = this;
		return new ClassicalCACell(mod);
	}

	/*--------------------
	 * Attributes
	 --------------------*/


	public static final int NUMSTATE = 10;

	/*--------------------
	 * Constructor
	 --------------------*/
	public CyclicModel() {
	}

	/*--------------------
	 * Get & Set methods
	 --------------------*/

	/* used with perfect cells */
	public final int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
		int newstate, oldstate = in_Cell.GetState();
		newstate = oldstate;
		int targetstate = (oldstate + 1) % NUMSTATE;
		if (in_Cell.CountOccurenceOf(targetstate) > 0) {
			newstate = targetstate; // transition
		}
		return newstate;
	}

	/* overriding */
	public final PaintToolKit GetPalette() {
			return PaintToolKit.GetGradient(NUMSTATE + 1);
	}

	


	/*--------------------
	 * Other methods
	 --------------------*/

}
