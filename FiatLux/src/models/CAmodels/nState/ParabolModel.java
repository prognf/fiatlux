package models.CAmodels.nState;

import java.util.Arrays;
import java.util.List;

import components.allCells.OneRegisterIntCell;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import initializers.SuperInitializer;
import models.CAmodels.ClassicalModel;
import topology.basics.SuperTopology;
import topology.basics.TopologyCoder;

/*--------------------
 * A One-dimensional CA to draw a parabol
 * @author : Oceane Chaze
 *--------------------*/

public class ParabolModel extends ClassicalModel {

    public static final String NAME = "Parabol";
    final static int LIM=2; // neighb. required to transmit excitation
    private static final int NONE=0, CENTER = 1, BARRIER=2, LEFT = 3, RIGHT = 4;

    private PaintToolKit m_Color;

    @Override
    public int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
        int[] neighbours = in_Cell.GetNeighbourhoodState();
        int newstate, oldstate = in_Cell.GetState();
        newstate = oldstate;
        switch (oldstate){
            case CENTER:
                newstate = CENTER;//Inutile
                break;
            case BARRIER :
                if(neighbours[1]== CENTER){
                    newstate = RIGHT;
                }
                else if(neighbours[3]== CENTER){
                    newstate = LEFT;
                }
                else if(neighbours[1]==RIGHT){
                    newstate = NONE;
                }
                else if (neighbours[3]==LEFT){
                    newstate = NONE;
            }
                else{
                    newstate = BARRIER;//Inutile
                }
                break;
            case LEFT:
                if(neighbours[1]==CENTER || neighbours[1]==BARRIER){
                    newstate=RIGHT;
                }
                else{
                    newstate=NONE;
                }
                break;
            case RIGHT:
                if(neighbours[3]==CENTER || neighbours[3]==BARRIER){
                    newstate=LEFT;
                }
                else{
                    newstate=NONE;
                }
                break;
            case NONE:
                if(neighbours[1]==RIGHT){
                    newstate=RIGHT;
                }
                else if(neighbours[3]==LEFT){
                    newstate=LEFT;
                }
                else if((neighbours[0]==RIGHT  || neighbours[0] == CENTER) && neighbours[1]==BARRIER){
                    newstate=BARRIER;
                }
                else if(neighbours[3]==BARRIER && (neighbours[4]==LEFT || neighbours[4]==CENTER)){
                    newstate=BARRIER;
                }
                break;
        }
        return newstate;
    }

    public String GetDefaultAssociatedTopology() {
        return TopologyCoder.s_TLR2;
    }

    public List<String> GetDisabledTopology() {
        List<String> disabledTopology = Arrays.asList(TopologyCoder.LINER1,TopologyCoder.LINER3,TopologyCoder.LINEA4, TopologyCoder.OUTERLINER1, TopologyCoder.MOORE8, TopologyCoder.GKL, TopologyCoder.LVOORHEES,TopologyCoder.CUSTOM1D);
        return disabledTopology;
    }

    @Override
    protected void SubInitialiseTopologyInfo(SuperTopology topologyManager) {
        super.SubInitialiseTopologyInfo(topologyManager);
    }

    @Override
    public SuperInitializer GetDefaultInitializer() {
        return new ParabolInitializer();
    }
    //OC
    public String GetDisableDimension(){
        return "2D";
    }

    /* override
     * 1D Viewer */
    //public LineAutomatonViewer GetLineAutomatonViewer(IntC in_SquareDim,
    //                                                  LinearTopology in_TopologyManager, RegularDynArray in_automaton, int in_Tsize) {
        //LineAutomatonViewer av = new LineAutomatonViewerDifference(in_SquareDim);
        //av.SetPalette( GetPalette() );
        //return av;
    //}
    //\OC
    /*--------------------
     * GFX
     --------------------*/
    /* overrides */
    @Override
    public PaintToolKit GetPalette() {
        m_Color= new PaintToolKit(5);
        m_Color.SetColor(NONE, FLColor.c_white);
        m_Color.SetColor(CENTER, FLColor.c_black);
        m_Color.SetColor(BARRIER, FLColor.c_blue);
        m_Color.SetColor(LEFT, FLColor.c_lightcyan);
        m_Color.SetColor(RIGHT, FLColor.c_lightpurple);

        return m_Color;
    }
}
