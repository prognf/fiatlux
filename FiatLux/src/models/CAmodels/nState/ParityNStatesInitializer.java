package models.CAmodels.nState;

import initializers.CAinit.OneRegisterInitializer;

public class ParityNStatesInitializer extends OneRegisterInitializer {

    private int N_STATES;

    public ParityNStatesInitializer(int n_states){
        N_STATES = n_states;
    }
    @Override
    protected void SubInit() {
        for (int i = 0; i < GetLsize(); i++) {
            int st= RandomInt(N_STATES);
            InitState(i, st);
        }
    }
}
