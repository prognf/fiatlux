package models.CAmodels.nState;

import grafix.gfxTypes.controllers.DoubleController;
import grafix.gfxTypes.elements.FLPanel;
import initializers.CAinit.OneRegisterInitializer;

public class ColoredTrafficInitializer extends OneRegisterInitializer {
	private static final int NCOLORS = 5;
	private double m_rate = 0.45;

	@Override
	public FLPanel GetSpecificPanel() {
		return new RateController();
	}

	@Override
	protected void SubInit() {
		int size= GetLsize();
		int [] init= new int[size];
		for (int pos=0; pos < size; pos++){
			int state=0;
			if (RandomEventDouble(m_rate )){
				init[pos] = 1 ;
			} else {
				init[pos] = 0;
			}
		}
		int count = 0, color =1;
		for (int pos= size- 1 ; pos>0; pos--){

			if (init[pos]==0){
				count --;				 
			} else {
				if (count <0){ // new color
					count=0;
					color= (color ++) % NCOLORS + 1;
				}
				count++;
				InitState(pos, color);
			}
		}
	}

	class RateController extends DoubleController {

		public RateController() {
			super("p ini (in%)",CONVERSIONRATE.PERCENT);
		}

		@Override
		protected double ReadDouble() {
			return m_rate;
		}

		@Override
		protected void SetDouble(double val) {
			m_rate= val;			
		}

	}
}
