package models.CAmodels.nState;

import components.allCells.OneRegisterIntCell;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import initializers.SuperInitializer;
import initializers.CAinit.BinaryInitializer;
import models.CAmodels.ClassicalModel;
import models.CAmodels.simplified.BoundConditions;
import models.CAmodels.simplified.Topologies;

public class StarWarsModel extends ClassicalModel {

    public static final String NAME = "StarWars";
    final static int LIM=1; // neighb. required to transmit excitation
    private static final int ZERO=0, ONE = 1, TWO=2, THREE = 3;

    /*--------------------
	 * Attributes
	 --------------------*/

    private PaintToolKit m_Color;

    /*--------------------
	 * Constructor
	 --------------------*/

    public StarWarsModel() {
        super();
    }


    public String GetDefaultAssociatedTopology() {
        return BoundConditions.Toric + Topologies.Moore8;
    }

    @Override
    public SuperInitializer GetDefaultInitializer() {
        return new BinaryInitializer();
    }
    @Override
    public int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
        int newstate, oldstate = in_Cell.GetState();
        newstate = oldstate;
        int[] neighbours = in_Cell.GetNeighbourhoodState();
        switch (oldstate) {
            case ZERO:
                if (containsExact(neighbours, 1,2)) {
                    newstate = ONE;
                }
                break;
            case ONE:
                if (!(containsExact(neighbours, 1,3) || containsExact(neighbours, 1,4) || containsExact(neighbours, 1,5))){
                    newstate = TWO;
                }
                break;
            case TWO:
                newstate = THREE;
                break;
            case THREE :
                newstate = ZERO;
        }
        return newstate;
    }

    public boolean containsExact(int[] array, int key, int n) {
        int found =0;
        for(int i : array){
            if(i==key){
                found++;
            }
        }
        if(found==n){
            return true;
        }
        return false;
    }

    /*--------------------
     * GFX
     --------------------*/
    /* overrides */
    @Override
    public PaintToolKit GetPalette() {
        m_Color= new PaintToolKit(4);
        m_Color.SetColor(ZERO, FLColor.c_black);
        m_Color.SetColor(ONE, FLColor.c_red);
        m_Color.SetColor(TWO, FLColor.c_orange);
        m_Color.SetColor(THREE, FLColor.c_yellow);


        return m_Color;
    }

}
