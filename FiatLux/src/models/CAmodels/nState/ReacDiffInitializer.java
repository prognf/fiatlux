package models.CAmodels.nState;

import grafix.gfxTypes.IntField;
import grafix.gfxTypes.elements.FLPanel;
import initializers.CAinit.OneRegisterInitializer;
import main.Macro;

public class ReacDiffInitializer extends OneRegisterInitializer {

	final static String CLASSNAME= Thread.currentThread().getStackTrace()[1].getClassName();

	public enum INITSTYLE { POINTSLINE, SPARSE, CENTERPOINT }
	public INITSTYLE m_InitStyle = INITSTYLE.POINTSLINE; 
	private static final double DEF_EXCITATIONDENS = 0.01;
	private static final int DEF_DEPARTURES = 3;

	/*--------------------
	 * attributes
	 --------------------*/
	int m_dim = -1; // 1D or 2D ?

	IntField mF_FireNum = new IntField("departures:", 3, DEF_DEPARTURES);
	IntField mF_FireLin = new IntField("line len:", 3, 0);
	ReacDiffModel m_model;

	/*--------------------
	 * construction
	 --------------------*/

	public ReacDiffInitializer() {
	}

	public ReacDiffInitializer(ReacDiffModel in_model) {
		m_model= in_model;
	}

	/*--------------------
	 * implementations
	 --------------------*/


	public void SubInit() {
		Macro.print(6, this, "init style " + m_InitStyle);
		switch (m_InitStyle){
		case POINTSLINE:
			ForestFireInit();
			break;
		case SPARSE:
			SparseExcitationsInit(DEF_EXCITATIONDENS);
			break;
		case CENTERPOINT:
			int mValue = 1;
			if (m_model != null) {
				mValue = m_model.GetMaxStateValue();
			}
			CenterPointInit(mValue);
			break;
		}		
	}

	public FLPanel GetSpecificPanel() {
		FLPanel p = FLPanel.NewPanelVertical(mF_FireNum,mF_FireLin);
		FLPanel Control= FLPanel.NewPanel(p, GetRandomizer());
		p.setOpaque(false);
		Control.setOpaque(false);
		return Control;
	}

	/*--------------------
	 * Other methods
	 --------------------*/

	private void ForestFireInit() {
		int maxstate = 1;

		if (m_model != null) {
			maxstate = m_model.GetMaxStateValue();
		}

		InitStateKRandom(mF_FireNum.GetValue(), maxstate);

		// fire line
		int l = mF_FireLin.GetValue();
		int Xpos = (GetXsize() - l) / 2;
		int Ypos = GetYsize() / 2;
		for (int k = 0; k < l; k++) {
			InitStateXY(Xpos + k, Ypos, maxstate);
			InitStateXY(Xpos + k, Ypos - 1, maxstate - 1);
		}
		//InitStateXY(Xpos, Ypos, maxstate);
	}

	/* excitationDens : proba between 0 & 1 */
	private void SparseExcitationsInit(double excitationDens){
		int maxstate = 1;

		if (m_model != null) {
			maxstate = m_model.GetMaxStateValue();
		}

		for (int cell=0; cell < GetLsize(); cell++){
			if ( RandomEventDouble(excitationDens) ){
				InitState(cell,maxstate);
			} else {
				InitState(cell,0);
			}
		}
		Macro.print(7,"Sparse init done with M=" + maxstate);
	}
}// end class
