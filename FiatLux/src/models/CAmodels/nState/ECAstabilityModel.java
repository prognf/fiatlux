package models.CAmodels.nState;

import components.allCells.OneRegisterIntCell;
import components.types.RuleCode;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.elements.FLPanel;
import initializers.SuperInitializer;
import initializers.CAinit.BinaryInitializer;
import main.tables.PlotterSelectControl;
import models.CAmodels.ClassicalModel;
import models.CAmodels.tabled.ECAcontrol;
import models.CAmodels.tabled.ECAlookUpTable;
import topology.basics.TopologyCoder;

/*--------------------
 * models excitable media
*--------------------*/
public class ECAstabilityModel extends ClassicalModel {

	final public static String NAME="ECAstability"; 
		
	
	/*--------------------
	 * overrides & implementations
	 --------------------*/

	public SuperInitializer GetDefaultInitializer() {
		return new BinaryInitializer();
	}
	
	
	@Override
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TLR1;
	}
		
	/*--------------------
	 * Attributes
	 --------------------*/
	ECAlookUpTable m_LookUpTable;
	
	
	
	/*--------------------
	 * Constructor
	 --------------------*/
	
	public ECAstabilityModel(RuleCode Wcode) {
		super();
		m_LookUpTable=  new ECAlookUpTable(Wcode);
	}	
	
	public ECAstabilityModel() {
		this(RuleCode.FromInt(0));
	}


	/*--------------------
	 * main method
	 --------------------*/

	/* MAIN FUNCTION */
	public int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
		int  // parity of the neighbours
			stL= in_Cell.ReadNeighbourState(0) % 2,
			stC= in_Cell.ReadNeighbourState(1) % 2,
			stR= in_Cell.ReadNeighbourState(2) % 2;
		int NeighbByte = stR | stC << 1 | stL << 2 ; 
		int newState= m_LookUpTable.getTransitionResult(NeighbByte);
		int newNeighbByte = stR | newState << 1 | stL << 2 ;
		int nextState= m_LookUpTable.getTransitionResult(newNeighbByte);
		boolean isNewStateStable= (nextState==newState);
		return newState + 2 * (isNewStateStable?0:1); 
	}
	

	/*--------------------
	 * Get & Set methods
	 --------------------*/

	/* overrides */
	public PaintToolKit GetPalette() {
		FLColor[] palette = new FLColor[4];
		palette[0] = FLColor.c_white;
		palette[1] = FLColor.c_blue;
		palette[2] = FLColor.c_lightgrey;
		palette[3] = FLColor.c_darkblue;
		return new PaintToolKit(palette);
	}
	
	
	/*--------------------
	 * GFX
	 --------------------*/

	
	@Override
	public FLPanel GetSpecificPanel() {
		FLPanel p = new FLPanel();
		p.Add(new ECAcontrol(m_LookUpTable));
		return p;  
	}


	@Override
	public String [] GetPlotterSelection1D() {
		return PlotterSelectControl.ECAselect;
	}  
	
		
}
