package models.CAmodels.binary;

import components.allCells.OneRegisterIntCell;

/*--------------------***
 * In the Expand Model, a cell gets excited if it has at least one neighbour
 * excited but not all 
 * Date of creation: 25 Jan. 2002
*--------------------*/
public class ExpandModel extends BinaryModel {

	final static String NAME="Expand"; 
	
	public ExpandModel() {
		super();
	}

	public final int ApplyLocalFunction(OneRegisterIntCell in_Cell) {

		int nA = in_Cell.CountOccurenceOf(1);
		if (nA > 0) {
			if (nA == in_Cell.GetNeighbourhoodSize())
				return 0;
			else
				return 1;
		} else {
			return 0;
		}

	}

}
