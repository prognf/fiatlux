package models.CAmodels.binary;

import components.allCells.OneRegisterIntCell;

/*--------------------
 * Model: Majority Model Description: classical majority without noise
 * 
*--------------------*/
public class MajorityModel extends BinaryModel {

	public static final String NAME = "Majority";
	final static int ZERO=0, ONE=1;

	public MajorityModel() {
		super();
	}

	public final int ApplyLocalFunction(OneRegisterIntCell inCell){
		return MajorityRule(inCell);
	}
	
	
	public static int MajorityRule(OneRegisterIntCell inCell) {
		int nAlive = inCell.CountOccurenceOf(ONE);
		int nDead = inCell.CountOccurenceOf(ZERO);
		if (nAlive > nDead)
			return ONE;
		else {
			if (nAlive < nDead)
				return ZERO;
			else
				return inCell.GetState(); // same state
		}
	}
}

