package models.CAmodels.binary;

import components.allCells.OneRegisterIntCell;
import components.types.IntegerList;
import grafix.gfxTypes.controllers.IntController;
import grafix.gfxTypes.elements.FLPanel;

/*--------------------
 * Model: Majority Model Description: classical majority without noise
 * 
*--------------------*/
public class SampledMajorityModel extends BinaryModel {

	public static final String NAME = "SampledMajority";
	final static int ZERO=0, ONE=1;

	int m_k=4;
	
	
	public SampledMajorityModel() {
		super();
	}

	public final int ApplyLocalFunction(OneRegisterIntCell cell){
		int [] neighbst= cell.GetNeighbourhoodState();
		int M= cell.GetNeighbourhoodSize();
		IntegerList sample= IntegerList.getKamongN(m_k, M, GetRandomizer());
		
		int sumstate=0;
		for (int index : sample) {
			sumstate+= neighbst[index];
		}
		//Macro.fPrintNOCR(":%d:", sumstate);
		if (2*sumstate<m_k)
			return 0;
		if (2*sumstate>m_k)
			return 1;
		return cell.GetState();
	}
	
	
	public static int MajorityRule(OneRegisterIntCell inCell) {
		int nAlive = inCell.CountOccurenceOf(ONE);
		int nDead = inCell.CountOccurenceOf(ZERO);
		if (nAlive > nDead)
			return ONE;
		else {
			if (nAlive < nDead)
				return ZERO;
			else
				return inCell.GetState(); // same state
		}
	}

	@Override
	public FLPanel GetSpecificPanel() {
		Kcontrol kcontrol= new Kcontrol();
		FLPanel p= FLPanel.NewPanel(kcontrol, GetRandomizer());
		return p;
	}
	
	
	class Kcontrol extends IntController   {

		private static final String KSTR = "Kval";

		public Kcontrol() {
			super(KSTR);
		}

		@Override
		protected int ReadInt() {
			return m_k;
		}

		@Override
		protected void SetInt(int val) {
			m_k= val;
		}
		
	}

}

