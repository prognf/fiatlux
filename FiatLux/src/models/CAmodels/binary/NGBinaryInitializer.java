package models.CAmodels.binary;

import grafix.gfxTypes.DoubleField;
import grafix.gfxTypes.elements.FLPanel;
import initializers.CAinit.OneRegisterInitializer;

/**
 * @author Nicolas Gauville
 */
public class NGBinaryInitializer extends OneRegisterInitializer {
    private static final int DEF_KO_CELLS = 0;

    DoubleField mF_KoNum = new DoubleField("% ko cells:", 3, DEF_KO_CELLS);

    public void SubInit() {
        for (int i = 0; i < GetLsize(); i++) {
            InitState(i, RandomEventDouble(0.5) ? 0 : 1);
        }

        InitStateKRandom((int) (mF_KoNum.GetValue() * (double) GetSize()), 2);
    }

    public void setKoCells(double koCells) {
        mF_KoNum.SetDouble(koCells);
    }

    public FLPanel GetSpecificPanel() {
        FLPanel p = FLPanel.NewPanel(mF_KoNum);
        FLPanel Control = FLPanel.NewPanel(p, GetRandomizer());
        p.setOpaque(false);
        Control.setOpaque(false);
        return Control;
    }
}
