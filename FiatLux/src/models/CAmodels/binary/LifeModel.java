package models.CAmodels.binary;

import initializers.SuperInitializer;
import initializers.CAinit.BinaryInitializer;
import models.CAmodels.simplified.BinaryTotalisticSimplifiedModel;
import models.CAmodels.simplified.BoundConditions;
import models.CAmodels.simplified.Topologies;

/*--------------------
 * Conway's Life 
 *****--------------------*/
public class LifeModel extends BinaryTotalisticSimplifiedModel {
	public static final String NAME = "Life";

	@Override
	public SuperInitializer GetDefaultInitializer() { 
		//return new NpointsInitializer();
		//return new BinaryCodeInitializer(); 
		return new BinaryInitializer();
	}
	
	public String GetDefaultAssociatedTopology() {
		return BoundConditions.Toric + Topologies.Moore8;
	}

	@Override
	public int ApplyLocalFunction(int nbAlive, int nbDead, int previousState) {
		if ((previousState == DEAD) && (nbAlive == 3)) {
			return ALIVE;
		} else if ((previousState == ALIVE) && ((nbAlive < 2) || (nbAlive > 3))) {
			return DEAD;
		}

		return previousState;
	}
}
