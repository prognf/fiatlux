package models.CAmodels.binary;

import components.allCells.OneRegisterIntCell;
import components.types.ProbaPar;
import grafix.gfxTypes.elements.FLPanel;

/*--------------------
 * Model: Majority Model Description: classical majority without noise
 * 
*--------------------*/
public class MinorityNoiseModel extends BinaryModel {

	public static final String NAME = "MinorityNoise";
	final static int ZERO=0, ONE=1;
	
	private static final double DEFNOISE = 0.01;

	ProbaPar m_noise= new ProbaPar("noise", DEFNOISE);
	
	public MinorityNoiseModel() {
		super();
	}

	public final int ApplyLocalFunction(OneRegisterIntCell inCell){
		if (RandomEventDouble(m_noise)) {
			return 1 - inCell.GetState();
		}
		return MinorityRule(inCell);
	}
	
	
	public static int MinorityRule(OneRegisterIntCell inCell) {
		int nAlive = inCell.CountOccurenceOf(ONE);
		int nDead = inCell.CountOccurenceOf(ZERO);
		if (nAlive > nDead)
			return ZERO;
		else {
			if (nAlive < nDead)
				return ONE;
			else
				return inCell.GetState(); // same state
		}
	}
	
	@Override
	public FLPanel GetSpecificPanel() {
		FLPanel p = FLPanel.NewPanel(m_noise);
		p.Add(this.GetRandomizer().GetSeedControl());
		return  p;
	}

}

