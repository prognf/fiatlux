package models.CAmodels.binary; 
import initializers.SuperInitializer;
import initializers.CAinit.BinaryInitializer;
import main.tables.PlotterSelectControl;
import models.CAmodels.ClassicalModel;

/* 
 * models with two states 0 and 1 
 * */
abstract public class BinaryModel extends ClassicalModel {    
				
	/*--------------------
	 *	 Get / Set
	 *--------------------*/
	
	public SuperInitializer GetDefaultInitializer() { 
		//return new NpointsInitializer();
		//return new BinaryCodeInitializer(); 
		return new BinaryInitializer();
	}
	
	
	/** defines the associated plotters */
	@Override
	public String[] GetPlotterSelection1D() {
		return PlotterSelectControl.BinarySelect1D;
	}
	
	/** defines the associated plotters */
	@Override
	public String[] GetPlotterSelection2D() {
		return PlotterSelectControl.BinarySelect2D;
	}
	

} //end class



