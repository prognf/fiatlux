package models.CAmodels.binary;

import components.allCells.OneRegisterIntCell;
import components.types.FLStringList;
import main.Macro;
import models.CAmodels.simplified.BoundConditions;
import models.CAmodels.simplified.Topologies;

/*--------------------
 * loading BFO
*--------------------*/
public class RuleBasedBFObinaryModel extends BinaryModel {

	
	final static String [] ruleBFO= {
			"*11100***", "11100****",
			"*00100***", "00100****",
			"**010100*", "***0110**",
			"**0110***", "*010100**",
			"***11101*", "111010***",
			"1110111**", "**1110110"};
	
	public static final String NAME = "BFOrule";

	
	public static FLStringList m_subrules;
	
	public RuleBasedBFObinaryModel() {
		super();
		m_subrules= FLStringList.fromArray(ruleBFO);
		Macro.print("loaded BFO...");
	}

	public final int ApplyLocalFunction(OneRegisterIntCell cell) {
		int q= cell.GetState();
		int [] neighbSt= cell.GetNeighbourhoodState();
		boolean flip= false;
		for (String rule : m_subrules) {
			flip = flip || neighbMatches(neighbSt,rule);; 
		}
		return flip?1-q:q;
	}

	final static char [] ST = {'0','1'};
	
	/** says if rule matches current cell neighb '*' is a wildcard **/
	private boolean neighbMatches(int[] neighbSt, String rule) {
		assert(neighbSt.length ==9);
		assert(rule.length()==9);
		for (int i=0; i<neighbSt.length;i++) {
			char current= ST[neighbSt[i]];
			char ruleChar= rule.charAt(i);
			if ((ruleChar!='*') && (ruleChar!=current)) {
				return false; // mismatch if the chars differ on something else than a wildcard
			}
		}
		return true; // all chars match
	}
	
	public String GetDefaultAssociatedTopology() {
		return BoundConditions.Toric + Topologies.LineR4;
	}
	
}
// -4 -3 -2 -1 0 1 2 3 4