package models.CAmodels.binary;

import components.allCells.OneRegisterIntCell;
import topology.basics.TopologyCoder;

/*--------------------
 * Conway's Life 
 *****--------------------*/
public class KaleidoscopeLifeModel extends BinaryModel {
	
	
	final static int DEADCELL = 0, LIVINGCELL = 1;
	public static final String NAME = "KaleidoscopeLife";
	
	/*--------------------
	 * overrides & implementations
	 --------------------*/

	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TEM;
	}
	
	
	public KaleidoscopeLifeModel() {
		super();
	}

	public final int ApplyLocalFunction(OneRegisterIntCell in_Cell) {

		int nAlive = in_Cell.CountOccurenceOf(LIVINGCELL);
		//	int predstate = in_Cell.GetState();

			if (nAlive == 4)
				return LIVINGCELL;
			else
				return DEADCELL;
	}


	}
