package models.CAmodels.binary;

import components.allCells.OneRegisterIntCell;

public class NGLogicExpModel extends BinaryModel {

	public static final String NAME = "LogicExp";

	public NGLogicExpModel() {
		super();
	}

	public final int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
		return R2(in_Cell);
	}

	public final int R1(OneRegisterIntCell in_Cell) {
		if (in_Cell.GetNeighbourhoodSize() != 4) {
			throw new Error("Wrong neighbourhood size (size = " + in_Cell.GetNeighbourhoodSize() + ", expected 4)");
		}

		return (in_Cell.GetNeighbour(0).GetState() * in_Cell.GetNeighbour(1).GetState() >= 1 || in_Cell.GetNeighbour(2).GetState() * in_Cell.GetNeighbour(3).GetState() >= 1 ? 1 : 0);
	}

	public final int R2(OneRegisterIntCell in_Cell) {
		if (in_Cell.GetNeighbourhoodSize() != 3) {
			throw new Error("Wrong neighbourhood size (size = " + in_Cell.GetNeighbourhoodSize() + ", expected 3)");
		}

		return (in_Cell.GetNeighbour(0).GetState() * in_Cell.GetNeighbour(1).GetState() >= 1 || in_Cell.GetNeighbour(0).GetState() * in_Cell.GetNeighbour(2).GetState() >= 1 || in_Cell.GetNeighbour(1).GetState() * in_Cell.GetNeighbour(2).GetState() >= 1 ? 1 : 0);
	}
}
