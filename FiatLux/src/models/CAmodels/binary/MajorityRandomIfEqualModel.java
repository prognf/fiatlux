package models.CAmodels.binary;

import components.allCells.OneRegisterIntCell;
import topology.basics.TopologyCoder;

/*--------------------
 * Model: Majority Model Description: classical majority without noise
 *--------------------*/
public class MajorityRandomIfEqualModel extends BinaryModel {

	public static final String NAME = "MajRnd";
	final static int ZERO=0, ONE=1;

	public MajorityRandomIfEqualModel() {
		super();
	}

	public final int ApplyLocalFunction(OneRegisterIntCell inCell){
		int nAlive= inCell.CountOccurenceOf(ONE);
		int nDead= inCell.CountOccurenceOf(ZERO);
		if (nAlive > nDead)	
			return ONE;
		else if (nAlive < nDead) 
			return ZERO;
		else {
			int st= RandomInt(2); // random flip
			return st;
		}
	}
	
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TV4;
	}
}
