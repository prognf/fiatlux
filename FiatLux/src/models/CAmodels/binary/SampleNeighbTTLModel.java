package models.CAmodels.binary;

import components.allCells.OneRegisterIntCell;
import components.types.IntegerList;
import grafix.gfxTypes.controllers.IntController;
import grafix.gfxTypes.elements.FLPanel;
import models.CAmodels.tabled.BinaryTableControl;
import models.CAmodels.tabled.TabledModel;
import topology.basics.TopologyCoder;

/*--------------------
 * Model: Majority Model Description: classical majority without noise
 * 
*--------------------*/
public class SampleNeighbTTLModel extends TabledModel {

	public static final String NAME = "SampleFixedSizeTTL";
	final static int ZERO=0, ONE=1;
	private static final int TABLESIZE = 6; // arbitrary !

	int m_k=2;
	
	
	public SampleNeighbTTLModel() {
		super(TABLESIZE);
	}

	
	
	public final int ApplyLocalFunction(OneRegisterIntCell cell){
		int [] neighbst= cell.GetNeighbourhoodState();
		int M= cell.GetNeighbourhoodSize();
		IntegerList sample= IntegerList.getKamongN(m_k, M, GetRandomizer());
		
		int sumstate=0;
		for (int index : sample) {
			sumstate+= neighbst[index];
		}
		return GetBitTable(sumstate);
	}
	
	
	public String GetDefaultAssociatedTopology(){
		return "T"+TopologyCoder.COMP;
		//return TopologyCoder.s_TLR1;
	}
	
	public void setKval(int kval) {
		m_k= kval;
	}
	
	
	@Override
	public FLPanel GetSpecificPanel() {
		Kcontrol kcontrol= new Kcontrol();
		BinaryTableControl wset= new BinaryTableControl(this);
		FLPanel p= FLPanel.NewPanel(wset, kcontrol, GetRandomizer().GetSeedControl());
		return p;
	}
	
	
	class Kcontrol extends IntController   {

		private static final String KSTR = "Kval";

		public Kcontrol() {
			super(KSTR);
		}

		@Override
		protected int ReadInt() {
			return m_k;
		}

		@Override
		protected void SetInt(int val) {
			m_k= val;
		}
		
	}

}

