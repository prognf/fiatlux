package models.CAmodels.binary;

import components.allCells.OneRegisterIntCell;
import topology.basics.TopologyCoder;

/*--------------------
 * 2D equivalent of the Traffic Model
 *****--------------------*/
public class Traffic2DModel extends BinaryModel {


	final static int ZERO = 0, ONE = 1;
	public static final String NAME = "Traffic2D";

	//final static int L=0, NL= 1, NR=2, R=3, SR= 4, SL= 5;   //TH6 TOPO
	final static int E=2, NE=1, N=0, W=6, SW=5, S=4, NW=7, SE=3; //TM8 TOPO
	
	/*--------------------
	 * construction
	 --------------------*/

	public Traffic2DModel() {
		super();
	}

	/*--------------------
	 * overrides & implementations
	 --------------------*/

	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_THX;
	}

	@Override
	final public int ApplyLocalFunction(OneRegisterIntCell cell) {
		return Traffic2Drule(cell);
	}
	
	/** constants depend on topology ! **/
	public static final int Traffic2Drule(OneRegisterIntCell c) {
		int st = c.GetState();
		if (st == ZERO){
			return ( (NST(c,W) == ONE ) || 
					((NST(c,SW) == ONE) && (NST(c,S) == ONE)) ) ? ONE : ZERO;
		} else {
			return ( (NST(c,E) == ZERO ) || 
					((NST(c,NE) == ZERO) && (NST(c,N) == ZERO)) ) ? ZERO : ONE;		
		}

	}

	/** constants depend on topology ! **/
	public static final int TrafficDiag(OneRegisterIntCell c) {
		int st = c.GetState();
		if (st == ZERO){
			return NST(c,SW);
		} else {
			return NST(c,NE);	
		}

	}
	
	
	/** constants depend on topology ! **/
	public static final int VarTraffic2Drule(OneRegisterIntCell c) {
		int st = c.GetState();
		if (st == ZERO){
			return ( ((NST(c,W) == ONE ) && (NST(c,NW) == ZERO)) || 
					((NST(c,SW) == ONE) && (NST(c,S) == ONE)) ) ? ONE : ZERO;
		} else {
			return ( ((NST(c,E) == ZERO ) && (NST(c,SE) == ONE)) || 
					((NST(c,NE) == ZERO) && (NST(c,N) == ZERO)) ) ? ZERO : ONE;		
		}
	}
	
	/** constants depend on topology ! **/
	public static final int NewTraffic2Drule(OneRegisterIntCell c) {
		int st = c.GetState();
		if (st == ONE){
			return  (( NST(c,N) == ONE ) || 
					( NST(c,E) == ONE )) 
					? ONE : ZERO;
		} else {
			return  ( (NST(c,S)==ONE) && (NST(c,SE)==ZERO) )
			? ONE : ZERO;	
		}
	}
	
	
	
	static final private int NST(OneRegisterIntCell c, int neighbPos) {
		return c.ReadNeighbourState(neighbPos);
	}

	


	//@Override
	/*public AbstractInitializer GetNewInitializer(){
		return new LifeInitializer();
	}*/


}
