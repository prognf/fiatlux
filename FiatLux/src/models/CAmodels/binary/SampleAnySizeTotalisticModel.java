package models.CAmodels.binary;

import components.allCells.OneRegisterIntCell;
import components.types.IntegerList;
import grafix.gfxTypes.elements.FLPanel;
import models.CAmodels.tabled.BinaryTableControl;
import models.CAmodels.tabled.TabledModel;
import topology.basics.TopologyCoder;

/*--------------------
 * Model: Santiago de Chile
*--------------------*/
public class SampleAnySizeTotalisticModel extends TabledModel {

	public static final String NAME = "SampleAnySizeTTL";
	final static int ZERO=0, ONE=1;
	private static final int TABLESIZE = 5+1;

	final static int MINK=1;
	
	
	public SampleAnySizeTotalisticModel() {
		super(TABLESIZE);
	}

	public final int ApplyLocalFunction(OneRegisterIntCell cell){
		int [] neighbst= cell.GetNeighbourhoodState();
		int M= cell.GetNeighbourhoodSize();
		int k= MINK + RandomInt(M+1-MINK);
		IntegerList sample= IntegerList.getKamongN(k, M, GetRandomizer());
		
		int sumstate=0;
		for (int index : sample) {
			sumstate+= neighbst[index];
		}
		return GetBitTable(sumstate);
	}
	
	
	public static int MajorityRule(OneRegisterIntCell inCell) {
		int nAlive = inCell.CountOccurenceOf(ONE);
		int nDead = inCell.CountOccurenceOf(ZERO);
		if (nAlive > nDead)
			return ONE;
		else {
			if (nAlive < nDead)
				return ZERO;
			else
				return inCell.GetState(); // same state
		}
	}

	/*--------------------
	 * overrides & implementations
	 --------------------*/

	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TLR2;
	}
	
	@Override
	public FLPanel GetSpecificPanel() {
		BinaryTableControl wset= new BinaryTableControl(this);
		return FLPanel.NewPanel(wset, GetRandomizer().GetSeedControl());
	}
	
}
	