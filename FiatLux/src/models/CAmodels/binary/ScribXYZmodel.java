package models.CAmodels.binary;

import java.lang.reflect.InvocationTargetException;

import org.codehaus.janino.ExpressionEvaluator;

import components.allCells.OneRegisterIntCell;
import grafix.gfxTypes.controllers.SimpleReadController;
import grafix.gfxTypes.elements.FLPanel;
import main.Macro;
import topology.basics.TopologyCoder;

public class ScribXYZmodel extends BinaryModel {

	private static final int ERRORCODE = -54321;
	public static final String NAME = "ScribXYZ";
	private static final String TXT_READRULE = "Rule code:";
	private static final int LEN = 10;

	String m_rule="(x+z)%2";
	ExpressionEvaluator m_ee;

	RidRule m_readRule;

	public ScribXYZmodel() {
		m_readRule= new RidRule();
		ParseRule(m_rule);
	}

	@Override
	public int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
		int 
		x= in_Cell.ReadNeighbourState(0),
		y= in_Cell.ReadNeighbourState(1),
		z= in_Cell.ReadNeighbourState(2);
		// Evaluate it with varying parameter values; very fast.
		try {
			Integer res = (Integer) m_ee.evaluate(
					new Object[] {          // parameterValues
                            x,
                            y,
                            z,
					}
					);
			return res;
		} catch (InvocationTargetException e) {
			e.printStackTrace();
			return ERRORCODE;
		}

	}

	public void ParseRule(String input){
		// Compile the expression once; relatively slow.
		try {
			m_ee = new ExpressionEvaluator(
					input,                     					// expression
					int.class,                           		// expressionType
					new String[] { "x", "y", "z" },           	// parameterNames
					new Class[] { int.class, int.class, int.class } // parameterTypes
					);
		} catch (Exception e){
			Macro.SystemWarning(e, " compile exception ; the janino module may not be linked...");
		}
	}

	
	public String GetDefaultAssociatedTopology(){
		return TopologyCoder.s_TLR1;
	}
	
	public FLPanel GetSpecificPanel(){
		return m_readRule;
	}
	
	class RidRule extends SimpleReadController {
		public RidRule() {
			super(TXT_READRULE, LEN);
		}

		@Override
		public void ReadAndParseInput(String readInput) {
			ParseRule(readInput);
		}
	}
}
