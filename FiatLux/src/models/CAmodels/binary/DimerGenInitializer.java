package models.CAmodels.binary;

import initializers.CAinit.OneRegisterInitializer;

/**
 * for the Dimer / Domino model
 */
public class DimerGenInitializer extends OneRegisterInitializer {

	public void SubInit() {
		int X= GetXsize(), Y= GetYsize();
		for (int x=0; x<X; x++) {
			int sta=x/2%2;
			InitStateXY(x, 1, sta);
			InitStateXY(x, Y-2, sta);
		}
	}


	/*    public FLPanel GetSpecificPanel() {
    }*/
}
