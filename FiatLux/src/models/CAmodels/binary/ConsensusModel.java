package models.CAmodels.binary;

import components.allCells.OneRegisterIntCell;
import grafix.gfxTypes.controllers.IntController;
import grafix.gfxTypes.elements.FLPanel;


/*--------------------
 * majority-like with threshold
 *--------------------*/
public class ConsensusModel extends BinaryModel {

	final static int ZERO=0, ONE=1;
	public static final String NAME = "Consensus";

	int m_StabilityLimit = 3;

	public ConsensusModel() {
		super();
	}


	public final int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
		int state= in_Cell.GetState();
		int same = in_Cell.CountOccurenceOf(state);
		int diff = in_Cell.GetNeighbourhoodSize() - same;
		if ( diff >= m_StabilityLimit){
			return 1 - state;
		} else {
			return state;
		}
	}

	@Override
	public FLPanel GetSpecificPanel(){
		FLPanel p = new LimitController();
		return p;
	}

	/**
	 * GFX
	 */
	class LimitController extends IntController {

		private static final String TXT_LIMIT = "LIMIT";

		public LimitController() {
			super(TXT_LIMIT);
		}

		@Override
		protected int ReadInt() {
			return m_StabilityLimit;
		}

		@Override
		protected void SetInt(int val) {
			m_StabilityLimit= val;		
		}
	} 

}
