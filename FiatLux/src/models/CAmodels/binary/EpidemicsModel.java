package models.CAmodels.binary;

import components.allCells.OneRegisterIntCell;

/*--------------------
 * In the Epidemics cell gets excited if it has at least one neighbour excited
*--------------------*/
public class EpidemicsModel extends BinaryModel {

	final static String NAME="Epidemics";

	public EpidemicsModel() {
		super();
	}

	final public int ApplyLocalFunction(OneRegisterIntCell in_Cell) {

		int nA = in_Cell.CountOccurenceOf(1);
		if (nA > 0) {
			return 1;
		} else {
			return 0;
		}
	}



}
