package models.CAmodels.binary;

import components.allCells.OneRegisterIntCell;
import grafix.gfxTypes.controllers.IntController;
import grafix.gfxTypes.elements.FLPanel;
import topology.basics.TopologyCoder;

/*--------------------
 * Conway's Life with parameters's choice
 *****--------------------*/
public class LifeModelParameters extends BinaryModel {

	public static final String NAME = "LifeParam";
	
	final static int DEADCELL = 0, LIVINGCELL = 1;
	final static int DISPLAY_SIZE = 1 ;
	final static int ZERO = 0, EIGHT = 8 ;
	final static int PARAMARRAYSIZE = 4;
	final static String 
	BIRTH_LO="Birth Lo", BIRTH_HI="Birth Hi",
	SURV_LO="Survival Lo", SURV_HI="Survival Hi";

	/*--------------------
	 * attributes
	 --------------------*/

	/* default parameters*/
	private int 
	m_SurvivalInf,m_SurvivalSup,m_BirthInf,m_BirthSup;

	/* panel which contains the four parameters */
	public FLPanel m_flpan = new FLPanel() ;

	/* table settings */
	IntController[] paramArray = new IntController[PARAMARRAYSIZE] ;
	private String m_Name;

	/*--------------------
	 * overrides & implementations
	 --------------------*/

	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TM8;
	}

	public final int ApplyLocalFunction(OneRegisterIntCell in_Cell) {

		int nAlive = in_Cell.CountOccurenceOf(LIVINGCELL);
		int predstate = in_Cell.GetState();

		if (predstate == DEADCELL)
			if ((nAlive >= m_BirthInf) && (nAlive <= m_BirthSup)) 
				return LIVINGCELL;
			else
				return DEADCELL;
		else if ((nAlive >= m_SurvivalInf) && (nAlive <= m_SurvivalSup))
			return LIVINGCELL;
		else
			return DEADCELL;

	}

	
	/*--------------------
	 * constructors
	 --------------------*/
	/*creates a new LifeModel with parameters*/
	public LifeModelParameters(int C){
		//		filename		
		int BL= C / 1000;
		C= C % 1000;
		int BH = C / 100;
		C= C % 100;
		int SL= C / 10;
		C= C % 10;
		int SH= C;
		Construct(BL,BH,SL,SH);
	}
	/*creates a new LifeModel with parameters*/
	public LifeModelParameters(int in_BirthInf, int in_BirthSup,
			int in_SurvivalInf, int in_SurvivalSup ) {
		Construct(in_BirthInf, in_BirthSup, in_SurvivalInf, in_SurvivalSup);
	}

	/* this one is needed because of first constructor ! */
	private void Construct(int in_BirthInf, int in_BirthSup,
			int in_SurvivalInf, int in_SurvivalSup){
		m_BirthInf = in_BirthInf ;
		m_BirthSup = in_BirthSup ;
		m_SurvivalInf = in_SurvivalInf ;
		m_SurvivalSup = in_SurvivalSup ;
	}

	
	
	@Override
	public String GetName(){
		return GetKey() + 
				(m_BirthInf) + (m_BirthSup) + (m_SurvivalInf) + (m_SurvivalSup);
	}

	public LifeModelParameters() {
		this(3,3,2,3);
	}
	/*--------------------
	 * Get/Set methods
	 --------------------*/

	/*Set the lower limit for a dead cell
	 *Under this limit a cell will born
	 *instead of the dead cell 
	 */
	public void setBirthInf(int p) {
		m_BirthInf = p ; 
	}

	/*Set the higher limit for a dead cell
	 *above this limit a cell will born
	 *instead of the dead cell 
	 */
	public void setBirthSup(int p) {
		m_BirthSup = p ;
	}

	/*Set the lower limit for a living cell
	 *under this limit the cell will die
	 */
	public void setSurvivalInf(int p) {
		m_SurvivalInf = p ;
	}

	/*Set the higher limit for a living cell
	 *above this limit the cell will die
	 */
	public void setSurvivalSup(int p) {
		m_SurvivalSup = p ;
	}


	public FLPanel GetSpecificPanel() {

		int count=0;
		paramArray[count++] = new BInfControl();
		paramArray[count++] = new BSupControl();
		paramArray[count++] = new SInfControl();
		paramArray[count++] = new SSupControl();


		for (IntController aParamArray : paramArray) {
			// USEFUL ? paramArray[i].Update();
			aParamArray.SetValidityRange(ZERO, EIGHT);
		}

		m_flpan.Add(paramArray);
		return m_flpan;
	}

	class SInfControl extends IntController {

		protected SInfControl() {
			super(SURV_LO, DISPLAY_SIZE);
		}

		protected int ReadInt() {
			return m_SurvivalInf;
		}

		protected void SetInt(int val) {
			setSurvivalInf(val);
		}
	}
	class SSupControl extends IntController {

		protected SSupControl() {
			super(SURV_HI, DISPLAY_SIZE);
		}

		protected int ReadInt() {
			return m_SurvivalSup;
		}

		protected void SetInt(int val) {
			setSurvivalSup(val);
		}
	}

	class BInfControl extends IntController {

		protected BInfControl() {
			super(BIRTH_LO, DISPLAY_SIZE);
		}

		protected int ReadInt() {
			return m_BirthInf;
		}

		protected void SetInt(int val) {
			setBirthInf(val);
		}
	}

	class BSupControl extends IntController {

		protected BSupControl() {
			super(BIRTH_HI,DISPLAY_SIZE);
		}

		protected int ReadInt() {
			return m_BirthSup;
		}

		protected void SetInt(int val) {
			setBirthSup(val);	
		}
	}
}

