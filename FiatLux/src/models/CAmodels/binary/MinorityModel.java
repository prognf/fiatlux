package models.CAmodels.binary;

import components.allCells.OneRegisterIntCell;

/*--------------------
 * Does the inverse as the majority does
*--------------------*/
public class MinorityModel extends BinaryModel {

	public static final String NAME = "Minority";

	public MinorityModel() {
		super();
	}

	public final int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
		int nAlive = in_Cell.CountOccurenceOf(1);
		int nDead = in_Cell.CountOccurenceOf(0);
		if (nAlive > nDead)
			return 0;
		else {
			if (nAlive < nDead)
				return 1;
			else
				return in_Cell.GetState(); // same state
		}

	}
}
