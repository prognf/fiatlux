package models.CAmodels.binary;

import components.allCells.OneRegisterIntCell;

/*--------------------
 * Model: Fredkin Model Description: parity counter Date of creation: 25 Jan.
 * 2002
*--------------------*/

public class ParityModel extends BinaryModel {

	public static final String NAME = "Parity";

	public ParityModel() {
		super();
	}

	public final int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
		int nAlive = in_Cell.CountOccurenceOf(1);
		return ((nAlive++) % 2);
	}
}
