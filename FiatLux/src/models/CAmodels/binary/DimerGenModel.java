package models.CAmodels.binary;

import components.allCells.OneRegisterIntCell;
import initializers.SuperInitializer;
import models.CAmodels.simplified.BoundConditions;
import models.CAmodels.simplified.Topologies;

/*--------------------
 * creating dimers 
 *--------------------*/
public class DimerGenModel extends BinaryModel {

	public static final String NAME = "DimerGen";
	final static int ZERO=0, ONE=1;

	public DimerGenModel() {
		super();
	}

	public final int ApplyLocalFunction(OneRegisterIntCell cell){
		return dimerGen(cell);
	}

	/* neighbour in pos has no 1s */ 
	private static boolean neighbEmpty(OneRegisterIntCell cell, int pos) {
		return cell.GetState()==0 &&
				cell.GetNeighbour(pos).CountOccurenceOf(1)==0;
	}


	public int dimerGen(OneRegisterIntCell cell) {
		boolean desertClose= 
				neighbEmpty(cell,0) || neighbEmpty(cell, 2)
				|| neighbEmpty(cell, 4) || neighbEmpty(cell, 6) ;
		if (desertClose &&	RandomEventDouble(.01) ){
			return 1;
		}


		int 
		qN=cell.ReadNeighbourState(0), 
		qE=cell.ReadNeighbourState(2),
		qS=cell.ReadNeighbourState(4),
		qW=cell.ReadNeighbourState(6);
		int n1v4= qN+qE+qS+qW;
		int n1v8=cell.CountOccurenceOf(1);
		int n1vD= n1v8 - n1v4;
		int qc=cell.GetState();
		boolean err= ( (qc==0) && (n1v8==0) ) 
				|| ( (qc==1) && (n1v4!=1) )
				|| ( (qc==1) && (n1vD>0) ) ;
		return err?1-qc:qc;	
	}



	public String GetDefaultAssociatedTopology() {
		return BoundConditions.Toric + Topologies.Moore8;
	}

	@Override
	public SuperInitializer GetDefaultInitializer() {
		return new DimerGenInitializer();
	}


}

