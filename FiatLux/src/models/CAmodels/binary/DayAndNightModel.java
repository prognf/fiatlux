package models.CAmodels.binary;

import models.CAmodels.simplified.BinaryTotalisticSimplifiedModel;
import models.CAmodels.simplified.BoundConditions;
import models.CAmodels.simplified.Topologies;

/*--------------------
 * Life's alternative created by Nathan Thomson in 1997
 *****--------------------*/

public class DayAndNightModel extends BinaryTotalisticSimplifiedModel {

    public static final String NAME = "DayAndNight";

    public String GetDefaultAssociatedTopology() {
        return BoundConditions.Toric + Topologies.Moore8;
    }

    @Override
    public int ApplyLocalFunction(int nbAlive, int nbDead, int previousState) {
        if ((previousState == DEAD) && ((nbAlive == 3) || (nbAlive == 6) || (nbAlive == 7) || (nbAlive == 8))) {
            return ALIVE;
        } else if ((previousState == ALIVE) && !((nbAlive == 3) || (nbAlive == 4) || (nbAlive == 6) || (nbAlive == 7) || (nbAlive == 8))) {
            return DEAD;
        }

        return previousState;
    }

    //OC
    public String GetDisableDimension(){
        return "1D";
    }

}
