package models.CAmodels.tabled;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JComponent;

import components.types.RuleCode;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.controllers.LongController;
import grafix.gfxTypes.elements.FLPanel;

/* this works specifically with a "tabled model" */
abstract class BinaryTableControlAbstract extends FLPanel 
implements MouseListener {
	
	abstract public void RefreshDisplay();

	private static final int LTST = 20;
	private static final String S_CODE = "code";	
	final static int TEXT_WIDTH = 15, LEN_CODE=4;


	int m_size; // number of bits of the table

	protected InputCode m_inputcode; // reads the Wcode 
	protected ITabledModel m_model;
	
	ActiveRectangle[] m_activeRect;

	public BinaryTableControlAbstract(ITabledModel in_model) {
		m_model= in_model;
		m_size = GetLUT().GetSize();
		m_inputcode= new InputCode();
		m_inputcode.minimize();
		
		// squares that control each cell of the transition table
		m_activeRect = new ActiveRectangle[m_size];
		for (int bitPos=0 ; bitPos < m_size; bitPos++){
			m_activeRect[bitPos] = new ActiveRectangle(bitPos);
			m_activeRect[bitPos].addMouseListener(this);
		}
		
	}	
	
	final private LookUpTableBinary GetLUT() {
		return m_model.GetLookUpTable();
	}




	protected void UpdateAllCellTableControls() {
		for (ActiveRectangle bitControl : m_activeRect){
			bitControl.repaint();
		}
	}

	
	/** common part **/
	protected FLPanel GetFirstLineForControl() {
		/*FLPanel line1= new FLPanel();
		line1.Add(m_Input);
		return line1;*/
		return m_inputcode;
	}


	
	/** reads the Wcode **/
	class InputCode extends LongController {

		private InputCode(){
			super(S_CODE, LEN_CODE);
		}

		protected void DoProcessingTask(long val) {
			GetLUT().SetRule(new RuleCode(val));
			RefreshDisplay();
		}

		@Override
		protected long ReadLong() {
			return GetLUT().GetRuleCode().GetCodeAsLong();
		}

		@Override
		protected void SetLong(long val) {
			DoProcessingTask(val);			
		}

	}

	/** flips one bit table and updates **/
	protected void flipBit(int bitPos){
		int bitVal= GetLUT().getBitTable(bitPos);
		GetLUT().SetRuleBit(bitPos, 1 - bitVal);
		RefreshDisplay();
	}

	protected int GetBitVal(int bitPos) {
		return GetLUT().getBitTable(bitPos);
	}

	public void mousePressed(MouseEvent e) {
	}

	public void mouseReleased(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
		//eventOutput("Mouse entered", e);
	}

	public void mouseExited(MouseEvent e) {
		//eventOutput("Mouse exited", e);
	}

	public void mouseClicked(MouseEvent e) {
		if (e.getSource() instanceof ActiveRectangle ){
			int numClic= ((ActiveRectangle)e.getSource()).m_bitPos;
			flipBit(numClic);
			
		} else{
			// ???
		}
			
	}

 /* --------------------- GFX -----------------------------*/
	private static final Dimension DIM = new Dimension(LTST, LTST);

	class ActiveRectangle extends JComponent {

		
		public int m_bitPos;
		
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			int bitVal= GetBitVal(m_bitPos);
			Color col= (bitVal==1)?FLColor.c_blue:FLColor.c_white;
			g.setColor(col);
			g.fillRect(0,0,LTST-1,LTST-1);
		}

		ActiveRectangle(int bitPosControl){
			m_bitPos= bitPosControl;
			
			setPreferredSize(DIM);
			setVisible(true);
		}
	
	}


	
}
