package models.CAmodels.tabled;

import components.types.RuleCode;
import main.Macro;
import main.MathMacro;

public class LookUpTableNstate {


	/*--------------------
	 * Abstract method definition
	 --------------------*/


	/*--------------------
	 * Attributes
	 --------------------*/
	final int m_K; // basis of the writing
	protected int[] m_lutVal;
	private RuleCode m_Wcode;
	
	
	/*--------------------
	 * construction
	 --------------------*/

	public LookUpTableNstate(int K, int sz_Table) {
		m_K= K;
		m_lutVal= new int[sz_Table];
		m_Wcode= RuleCode.NULLRULE;
	}


	/*--------------------
	 * Rule Table operations
	 --------------------*/


	/** sets the look up table according to Wolfram's convention **/
	public void SetRule(RuleCode ruleNum) {
		m_Wcode= ruleNum;
		long Wcode= ruleNum.GetCodeAsLong();
		String str_lut= MathMacro.Long2NaryString(Wcode, m_K, m_lutVal.length);
		for (int i=0; i<m_lutVal.length; i++) {
			m_lutVal[i]= Character.getNumericValue( str_lut.charAt(i) );
		}
	}

	
	/*--------------------
	 * GET / SET
	 --------------------*/

	
	/** size = number of outputs */
	final public int GetSize(){
		return m_lutVal.length;
	}

	/** main method for searching in the look-up table) */ 
	final public int GetTransitionResult(int neighbourhoodByte) {
		return m_lutVal[neighbourhoodByte];
	}

	/** conversion from binary table to RuleCode */
	public RuleCode GetRuleCode() {
		return m_Wcode;
	}


	/** returns the i-th bit of the transition table */
	public int GetLutTable(int i) {
		return m_lutVal[i];
	}

	/** transforms a transition table into a string **/ 
	public String GetTableAsString(){
		StringBuilder s = new StringBuilder("[ ");
		for (int lutval :  m_lutVal)
			s.append(lutval).append(" ");
		s.append("]");
		return s.toString();
	}

	/** Scode = collection of outputs of the L.U.T. **/
	public String GetScode(){
		StringBuilder out = new StringBuilder();
		for (int lutval : m_lutVal){
			out.append(lutval);
		}
		return out.toString();
	}

	public void PrintTable() {
		Macro.print( GetTableAsString() );		
	}


	public void SetRuleByListOfTransitions(String listOfTransitions) {
		Long Wcode= Long.parseLong(listOfTransitions, m_K);
		SetRule(new RuleCode(Wcode));
	}
	
	public void setTransition(int lutpos, int tval) {
		m_lutVal[lutpos]= tval;
		recomputeWcode();
	}


	private void recomputeWcode() {
		long wcode=0;
		int mult=1;
		for (int i=0 ; i < m_lutVal.length; i++) {
			wcode += mult * m_lutVal[i];
			mult *= m_K;
		}
		m_Wcode= new RuleCode(wcode);
	}

}
