package models.CAmodels.tabled;

import components.allCells.OneRegisterIntCell;
import components.types.FLString;
import components.types.IntegerList;
import components.types.RuleCode;
import grafix.gfxTypes.elements.FLPanel;
import main.Macro;
import main.MathMacro;
import topology.basics.SuperTopology;
import topology.basics.TopologyCoder;


public class OuterTotalisticModel extends TabledModel {

	
	/*--------------------
	 * Constructor
	 --------------------*/

	protected OuterTotalisticModel(int szTable) {
		super(szTable);
	}

	private int m_tableSize;
	
	/** always use InitialiseTopologyInfo after construction **/
	public OuterTotalisticModel(SuperTopology topology, int tabSize) {
		super(tabSize);
		//this.SetRuleWcode(ruleCode);
	}
	
	public OuterTotalisticModel() {
		this(null,0);
	}

	/* always call before using the model !
	 * builds the lookup table with a proper size */
	public void SubInitialiseTopologyInfo(SuperTopology topologyManager) {
		RuleCode ruleNum= GetRuleCode();
		int NeighbSizeMax = topologyManager.GetActualNeighbMaxSize();
		// each central cell value is associated to 0 to N-1 possibilities !
		m_tableSize= 2 * (NeighbSizeMax + 1);
		ResizeLookUpTable(m_tableSize); 
		SetRuleWcode(ruleNum);
	}
	
	/*--------------------
	 * Abstract method definition
	 --------------------*/

	@Override
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TV4;
	}
	
	/* overloading this method */
	public FLPanel GetSpecificPanel() {
		OuterTotalisticControl OuterTotalisticVC = new OuterTotalisticControl(this); 
		return OuterTotalisticVC;
	}

	/** transition function 
	 * TODO Check history of this one : problem with the calculus of bitpos !! **/
	public final int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
		int mystate = in_Cell.GetState();
		if (mystate>=2) {
			return mystate;
		}
		int nAlive = in_Cell.CountOccurenceOf(1);
		try {
			int bitpos= 2 * nAlive + mystate;
			return GetBitTable(bitpos);
		} catch (Exception e) {
			e.printStackTrace();
			Macro.print("Nalive : " + nAlive);
			return 0;
		}
	}

	/*--------------------
	 * Attributes
	 --------------------*/

	final static int SZ_NEIGHB = (8 + 1) * 2; // for 8 neigbours out maxi
	public static final String NAME = "OuterTotalistic";

	
	/*--------------------
	 * Get & Set methods
	 --------------------*/

	static final public String GetCharacteristics(int W) {
		String s = "OuterTotalistic <" + FLString.Decimal2String(W) + ","
				+ FLString.Decimal2String(GetConjugateRule(W)) + ">" + " MR: "
				+ FLString.Decimal2String(GetMinimalRepresentative(W));
		return s;
	}

	static public long GetMinimalRepresentative(long in_RuleNum) {
		long x0, x1;
		x0 = in_RuleNum;
		x1 = GetConjugateRule(in_RuleNum);
		if (x1 < x0)
			x0 = x1;
		return x0;
	}

	/* code when applying 0/1 symmetry */
	static public long GetConjugateRule(long in_RuleNum) {
		int[] NewRuleBit = new int[SZ_NEIGHB];
		int[] RuleBit = MathMacro.Decimal2BinaryTab(in_RuleNum, SZ_NEIGHB);

		/* transfo */
		for (int i = 0; i < SZ_NEIGHB; i++) {
			NewRuleBit[i] = 1 - RuleBit[SZ_NEIGHB - 1 - i];
		}

		/* decoding */
		return MathMacro.BinaryTab2Decimal(NewRuleBit);
	}

	/* prints all Minimal representative rules */
	static public void OuterTotalisticMinimals() {

		IntegerList codes = new IntegerList();
		for (int W = 0; W < 1024; W++) {
			if (W == GetMinimalRepresentative(W)) {
				codes.addVal(W);
			}
		}

		String filename = "OuterTotalisticMinimals.dat";
		//check this one (modified)
		codes.WriteToFileCol(filename);

		Macro.print(1, "I have written file <" + filename + ".dat> in current dir.");
	}

	public static void DoTest() {
		Macro.BeginTest("OuterTotalisticModel");

		Macro.print(GetCharacteristics(0));
		Macro.print(GetCharacteristics(204));
		Macro.print(GetCharacteristics(31));
		Macro.print(GetCharacteristics(240));

		Macro.EndTest();
	}

}
