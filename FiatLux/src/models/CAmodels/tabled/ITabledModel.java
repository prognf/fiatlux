package models.CAmodels.tabled;

import components.types.RuleCode;

/**
 * An interface for Models using a lookup table
 */
public interface ITabledModel {
	
	/**
	 * @return the lookup table
	 */
    LookUpTableBinary GetLookUpTable();
	
	/**
	 * @param i the index of the bit
	 * @return the bit of the lookup table at position i
	 */
    int GetBitTable(int i);

	RuleCode GetRuleCode();
}
