package models.CAmodels.tabled;

import javax.swing.event.ChangeEvent;

import components.allCells.OneRegisterIntCell;
import components.types.IntegerList;
import components.types.RuleCode;
import grafix.gfxTypes.controllers.IntController;
import grafix.gfxTypes.controllers.ReadDisplaySliderControl;
import grafix.gfxTypes.elements.FLPanel;
import main.Macro;
import main.MathMacro;
import topology.basics.TopologyCoder;

// to test
public class BeehiveModel extends TabledModel {
	
	
	final static String CLASSNAME= Thread.currentThread().getStackTrace()[1].getClassName();
	final static int HEXA= 6, TABLE_LENGTH=13, NEIGHBS= 64;
	public static final String NAME = "Beehive";
	final static RuleCode DEF_RULENUM= new RuleCode(3);
	private static final int BCODE_INPUT= 13, BCODE_OUTPUT = 16;
	private static final String TXT_DCODE = "Decimal code";
	/*--------------------
	 * attributes
	 --------------------*/

	final IntegerList m_CodesList = new IntegerList();
	int [] m_CodeRuleTable64 = new int [NEIGHBS]; // synthetic rule table
	final boolean m_MonotonousEvolution;
	
	// GFX
	DecimalInputCode mc_DcodeVC;
	BinaryInputCode mc_BcodeVC;
	/*--------------------
	 * construction
	 --------------------*/
	/* monotonous evolution means 0=> 1 and 1 stays */
	public BeehiveModel(boolean MonotonousEvolution) {
		super(NEIGHBS);
		m_MonotonousEvolution= MonotonousEvolution;
		Construct(m_CodesList);
		SetRuleCode(DEF_RULENUM);
		//TestRules();
		//TestTable();
	}

	private void TestRules(){
		for (int i=0; i< NEIGHBS; i++){
			int [] codi= MathMacro.Decimal2BinaryTab(i, HEXA);
			Analyse(codi);
		}
	}

	private void TestTable(){
		for (int neighb = 0 ; neighb < NEIGHBS; neighb++){
			String tab= Array2String( MathMacro.Decimal2BinaryTab(neighb, HEXA) );
			Macro.print(neighb + " " + m_CodeRuleTable64[neighb] + " > " + tab);
		}

	}

	private void Construct(IntegerList codesList) {
		for (int i=0; i< CODCOD.length; i++){
			int [] codi= CODCOD[i];
			int rcode =  MathMacro.Tab2CodeInt(codi);
			m_CodesList.addVal(rcode);
		}	
		m_CodesList.Print();
	}
	/*--------------------
	 * actions
	 --------------------*/
	@Override
	public int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
		if (m_MonotonousEvolution && in_Cell.GetState()==1){
			return 1;
		} else {
			int transition_pos = 
				in_Cell.ReadNeighbourState(0)  <<  0 
				| in_Cell.ReadNeighbourState(1) << 1
				| in_Cell.ReadNeighbourState(2) << 2
				| in_Cell.ReadNeighbourState(3) << 3
				| in_Cell.ReadNeighbourState(4) << 4
				| in_Cell.ReadNeighbourState(5) << 5;			
			//Macro.Debug(" transition :" + transition_pos + " res : " + m_RuleBit[transition_pos]);
			return m_CodeRuleTable64[transition_pos];
		}
	}

	/*--------------------
	 * overrides
	 --------------------*/
	/*public AbstractInitializer GetNewInitializer() {
		return new StandardInitializer();
	}*/
	
	/* overloading this method */
	public FLPanel GetSpecificPanel() {
		mc_DcodeVC = new DecimalInputCode();
		mc_BcodeVC= new BinaryInputCode();
		FLPanel p = new FLPanel();
		p.Add( mc_DcodeVC, mc_BcodeVC);
		return p;
	}


	/* from m_RuleBit to m_CodeRuleTable */ 
	private void UpdateTransitionTable(){
		for (int neighb = 1 ; neighb < NEIGHBS -1; neighb++){
			int [] neighb_array = MathMacro.Decimal2BinaryTab(neighb, HEXA);
			int myrulecode_pos= CodeInList(neighb_array);
			int bitvalue= GetTransitionResult(myrulecode_pos);
			m_CodeRuleTable64[neighb]= bitvalue;
		}
		// special for first and last neighbs		m_CodeRuleTable64[0]= 0;
		m_CodeRuleTable64[NEIGHBS - 1]= 0;
		PrintTable();
	}

	//@Override TODO : CURIOUS
	public void SetRuleCode(RuleCode Wcode) {
		//SetRuleNum(in_RuleNum); 
		UpdateTransitionTable();
	}
	
	@Override
	public String GetName(){
		return GetKey() + GetRuleCode();
	}

	@Override
	public String GetDefaultAssociatedTopology(){
		return TopologyCoder.s_THX;
	} 

	/*--------------------
	 * others
	 --------------------*/

	private void MakeRefCodesList(){

		for (int[] codi : CODCOD) {
			Analyse(codi);
		}
	}



	private int CodeInList(int [] array){
		int trynum=0;
		int index = -1;
		while ((index == -1) && (trynum < 6)){
			int rcode= MathMacro.Tab2CodeInt(array);
			index= m_CodesList.IndexOf(rcode);
			Permutation6(array);
			trynum++;			
		}		
		return index;
	}

	private String Array2String(int [] array){
		String s= " <";
		for (int anArray : array) {
			s += "" + anArray;
		}
		return s +">";
	}

	private void Analyse(int [] array){
		String tab= Array2String(array);
		int rcode= MathMacro.Tab2CodeInt(array);
		Permutation6(array);
		int refcode= CodeInList(array);		
		String newtab="...";
		if (refcode!=-1){
			newtab= Array2String( CODCOD[refcode] );
		}
		Macro.print(tab +  "  r: " + rcode   + " rcode:" + refcode + " array " + newtab);

	}

	private void Permutation6(int [] array){
		int stored= array[0];
		for (int i=0; i< HEXA -1; i++){
			array[i]= array[i+1];
		}
		array[HEXA -1]= stored; 
	}


	private class DecimalInputCode extends IntController{
		public DecimalInputCode() {
			super(TXT_DCODE);
		}
		@Override
		protected int ReadInt() {
			return (int)GetRuleCode().GetCodeAsLong();
		}
		@Override
		protected void SetInt(int val) {
			SetRuleCode(new RuleCode(val));
			mc_BcodeVC.Update();
		}
	}
	
	private class BinaryInputCode extends ReadDisplaySliderControl {

		public BinaryInputCode() {
			super("Bcode", BCODE_INPUT, BCODE_OUTPUT);
		}

		@Override
		public String GetDisplayFromOwner() {
			return GetLookUpTable().GetTableAsString();
		}

		@Override
		public void Read(String s) {
			int len = s.length();
			char [] exp = s.toCharArray();
			for (int i=0; i< exp.length; i++){
				Macro.print(":" + exp[i]);
				if (exp[i] == '0'){
					SetRuleBit(i, 0);
				} else if (exp[i] == '1'){
					SetRuleBit(i, 1);
				} else {
					Macro.SystemWarning("Unrecognized char in binary expression");
				}
			}
			UpdateTransitionTable();
			mc_DcodeVC.Update();
		}

		@Override
		public void stateChanged(ChangeEvent e) {
			// Do nothing as long as there is no slider
		}

		@Override
		public int GetValueFromOwner() {
			// Do nothing as long as there is no slider
			return 0;
		}
	}


		/*--------------------
		 * transition table
		 *--------------------*/


		int [] code1  = { 1, 0, 0, 0, 0, 0};
		int [] code2a = { 1, 1, 0, 0, 0, 0};
		int [] code2b = { 1, 0, 1, 0, 0, 0};
		int [] code2c = { 1, 0, 0, 1, 0, 0};

		int [] code3a = { 1, 1, 1, 0, 0, 0};
		int [] code3b = { 1, 1, 0, 1, 0, 0};
		int [] code3c = { 1, 1, 0, 0, 1, 0};
		int [] code3d = { 1, 0, 1, 1, 0, 0};
		int [] code3e = { 1, 0, 1, 0, 1, 0};


		int [] code4a = { 1, 1, 1, 1, 0, 0};
		int [] code4b = { 1, 1, 1, 0, 1, 0};
		int [] code4c = { 1, 1, 0, 1, 1, 0};	 

		int [] code5  = { 1, 1, 1, 1, 1, 0};
		int [] code6  = { 1, 1, 1, 1, 1, 1};

		int [] [] CODCOD={ 
				code1, code2a, code2b, code2c, 
				code3a, code3b, code3c, code3d, code3e,
				code4a, code4b, code4c,
				code5, code6};


		/*--------------------
		 * test
		 *--------------------*/
		public static void DoTest(){
			Macro.BeginTest(CLASSNAME);
			Macro.EndTest();
		}


	}
