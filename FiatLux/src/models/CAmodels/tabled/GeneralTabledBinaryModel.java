package models.CAmodels.tabled;

import java.util.ArrayList;

import components.allCells.OneRegisterIntCell;
import components.types.RuleCode;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.elements.FLActionButton;
import grafix.gfxTypes.elements.FLPanel;
import initializers.SuperInitializer;
import initializers.CAinit.BinaryInitializer;
import main.Macro;
import main.tables.PlotterSelectControl;
import models.CAmodels.CellularModel;
import topology.basics.SuperTopology;
import topology.basics.TopologyCoder;


/*--------------------
 * a binary model with a look-up table
*--------------------*/
public class GeneralTabledBinaryModel extends TabledModel implements ITabledModel {

	public final static String NAME="GeneralBinary";
		
	/*--------------------
	 * Constructor
	 --------------------*/
	
	public GeneralTabledBinaryModel(int TableSize, RuleCode ruleNum) {
		super(TableSize);
		SetRuleWcode(ruleNum);
	}
	
	
	/** SPECIAL : use only with reflexivity !!! 
	 * to get static information **/
	public GeneralTabledBinaryModel(){
		super(0);
	}
	
	public static TabledModel EmptyRule(int nbits) {
		return new GeneralTabledBinaryModel(nbits, new RuleCode(0));
	}
	
	/*--------------------
	 * overrides
	 --------------------*/
	
	/** always call before using the model !
	 * makes a lookup table with a proper size */
	public void SubInitialiseTopologyInfo(SuperTopology topologyManager) {
		int NeighbSizeMax = topologyManager.GetActualNeighbMaxSize();
		RuleCode ruleNum= GetRuleCode();
		int tableSize= 1 << NeighbSizeMax; // 2^n
		ResizeLookUpTable(tableSize); // from 0 to N possibilities !
		SetRuleWcode(ruleNum);
	}
	
	public SuperInitializer GetDefaultInitializer() { 
		//return new NpointsInitializer();
		//return new BinaryCodeInitializer(); 
		return new BinaryInitializer();
	}
	
	/*--------------------
	 * Abstract method definition
	 --------------------*/
	
	/** MAIN FUNCTION */
	public int ApplyLocalFunction(OneRegisterIntCell cell) {
		int neighbIndex = 0;
		int sz= cell.GetNeighbourhoodSize();
		
		// left neighb. (index 0) has the most significant bit
		for (int i=0; i< sz; i++){
			neighbIndex= neighbIndex | 
				cell.ReadNeighbourState(sz - i - 1) << i; 
		}
		return GetTransitionResult(neighbIndex);
	}

	/*--------------------
	 * Get / Set
	 --------------------*/
	@Override
	public FLPanel GetSpecificPanel() {
		FLPanel container, table;
		ButtonInfo btn;

		container = FLPanel.NewPanel(
				table = new BinaryTableControl(this), // call back
				btn = new ButtonInfo());

		table.setOpaque(false);
		container.setOpaque(false);
		btn.defineAsPanelButtonStylized(FLColor.c_lightgrey);

		return container;
	}			
	

	@Override
	public String [] GetPlotterSelection1D() {
		return PlotterSelectControl.ECAselect;
	}  
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TLR2;
	}
	
	private static final String LBLRULEINFO = "infoRule";

	
	private class ButtonInfo extends FLActionButton {

		public ButtonInfo() {
			super(LBLRULEINFO);
		}
		
		@Override
		public void DoAction() {
			RuleCode W = GetLookUpTable().GetRuleCode();
			int K= GetNeighbSize();
			ArrayList<RuleCode> listSym = RuleCode.GetSymmetrics(W,K);
			String txt = 
				String.format(" (f,R,C,RC)=(%s,%s,%s,%s)", 
					listSym.get(0), listSym.get(1),	listSym.get(2),	listSym.get(3));
			
			String transitions= GetLookUpTable().GetScode();
			txt+= "::" + transitions;
			Macro.print(txt);
			Macro.UserMessage(txt);
		}
	}

	/*--------------------
	 * testing
	 --------------------*/
	
	public static void DoTest() {		
		
		try {
			int x= 1 << 5;
			Macro.print(" exp " + x);
			Object hihi =  Class.forName("CAmodels.Binary.GeneralBinaryModel" ).newInstance ();
			CellularModel haha= (GeneralTabledBinaryModel) hihi;
			String info = haha.GetDefaultAssociatedTopology();
			//Macro.Debug("Name :" + info);
		} catch (InstantiationException | ClassNotFoundException | IllegalAccessException e) {
				e.printStackTrace();
		}

    }

	

}
