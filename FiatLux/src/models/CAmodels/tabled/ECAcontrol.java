package models.CAmodels.tabled;

import components.types.FLString;
import components.types.RuleCode;
import grafix.gfxTypes.elements.FLPanel;
import grafix.gfxTypes.elements.FLTextArea;
import grafix.gfxTypes.elements.FLTextField;
import main.Macro;


/*--------------------
 * controls both ECAmodel and FuzzyECAxxx 
 * acts on an ECA table
*--------------------*/
public class ECAcontrol extends FLPanel {

	final static String DEF_LABEL = "ECA:";
	final static int LENWCODE=3, LENTCODE = 6;

	// main attribute
	ECAlookUpTable m_ecaTable;

	// GFX
	ParseECAcode m_InputField = new ParseECAcode(); // inner class
	FLTextArea m_WcodeDisplay = new FLTextArea(LENWCODE);
	FLTextArea m_TcodeDisplay = new FLTextArea(LENTCODE); 

	/*--------------------
	 * Constructor
	 --------------------*/

	public ECAcontrol(ECAlookUpTable lookUpTable) {
		this(lookUpTable, DEF_LABEL);
		setOpaque(false);
	}

	public ECAcontrol(ECAlookUpTable lookUpTable, String label) {
		m_ecaTable = lookUpTable;
		AddLabel(label);
		Add(m_InputField, m_WcodeDisplay, m_TcodeDisplay);
		Update();
	}


	/*--------------------
	 * MAIN FUNCTION
	 --------------------*/

	/** sets the rule */
	public void SetModelCode(RuleCode Wcode) {
		if (Wcode.IsInBounds(256)) {
			m_ecaTable.SetRule(Wcode);
			Update();
		} else {
			Macro.print("error : Wcode out of bounds");
		}
	}

	protected void Update(){
		RuleCode Wcode = m_ecaTable.GetRuleCode();
		String Tcode = ECAlookUpTable.GetTransitionCodePadded(Wcode); 
		m_WcodeDisplay.SetText(""+Wcode);
		m_TcodeDisplay.SetText(Tcode);
	}

	/** parsing ECA code */
	class ParseECAcode extends FLTextField {

		ParseECAcode() {
			super(LENTCODE);
		}

		public void parseInput(String input) {
			RuleCode Wcode;
			// entry is Tcode
			if ( ECAlookUpTable.IsRegularTcode(input)) {
				Wcode = ECAlookUpTable.TcodeToWcode(input);
				SetModelCode(Wcode);
			} else {
			// entry is Wcode
				try{
					int code = Integer.parseInt(input);
					SetModelCode(new RuleCode(code));
				} catch (NumberFormatException e){
					FLString.ParsingError(DEF_LABEL, input);
				}
			}
		}
	}

}
