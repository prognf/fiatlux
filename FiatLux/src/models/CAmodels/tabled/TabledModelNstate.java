package models.CAmodels.tabled;

import components.types.RuleCode;
import models.CAmodels.ClassicalModel;


abstract public class TabledModelNstate extends ClassicalModel {

	private LookUpTableNstate m_LookUpTable;  

	/*--------------------
	 * construction
	 --------------------*/

	protected TabledModelNstate(int Kbasis, int szTable){
		m_LookUpTable = new LookUpTableNstate(Kbasis, szTable);
	}

	/*--------------------
	 * Get / Set
	 --------------------*/
	final protected int GetTransitionResult(int neighbourhoodNum) {
		return m_LookUpTable.GetTransitionResult(neighbourhoodNum);
	}

	final public int GetBitTable(int lutpos) {
		return m_LookUpTable.GetLutTable(lutpos);
	}

	final public LookUpTableNstate GetLookUpTable() {
		return m_LookUpTable;
	}
	
	public RuleCode GetRuleCode() {
		return m_LookUpTable.GetRuleCode();
	}

	/** sets a rule by its Wcode **/
	public void SetRuleByWcode(RuleCode Wcode){
		m_LookUpTable.SetRule(Wcode);
	}

	protected void PrintTable() {
		m_LookUpTable.PrintTable();
	}


	
}
