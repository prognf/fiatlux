package models.CAmodels.tabled;

import grafix.gfxTypes.elements.FLLabel;
import grafix.gfxTypes.elements.FLPanel;

/** the user input is an integer which is translated into 
 * a binary table
 */
public class BinaryTableControl extends BinaryTableControlAbstract  {

	// simple print of the transition table
//	private FLTextArea m_Line1 = new FLTextArea(TEXT_WIDTH); 
//	private FLTextArea m_Line2 = new FLTextArea(TEXT_WIDTH);


	public BinaryTableControl(ITabledModel in_model) {
		super(in_model);
		// SetBoxLayoutY();
		// setAlignmentY(Component.LEFT_ALIGNMENT);
		Add(GetFirstLineForControl());
	
		FLPanel lineTabcontrol= new FLPanel();
		lineTabcontrol.SetGridBagLayout();
		
		for (int bitPos=0 ; bitPos < m_size; bitPos++){
			FLPanel squareAndLabel= new FLPanel();
			squareAndLabel.SetGridBagLayout();
			
			
			FLLabel bitNum= new FLLabel(""+bitPos);
			squareAndLabel.AddGridBag(bitNum,0,0);
			squareAndLabel.AddGridBag(m_activeRect[bitPos],0,1);
			// adding the component + the mouse listener
			lineTabcontrol.AddGridBag(  squareAndLabel, bitPos, 0 );
			
		}
		Add(lineTabcontrol);
		
		RefreshDisplay();
	}


	// refresh display
	public void RefreshDisplay() {
	//	String s1 = "", s2 = "";
	//	for (int i = 0; i < m_size; i++) {
	//		s1 += "|" + i + "|";
	//		s2 += "|" + GetBitVal(i) + "|";
	//	}
	//	
	//	m_Line1.setText(s1);
	//	m_Line2.setText(s2);
		m_inputcode.Update();
		UpdateAllCellTableControls();
	}
}
