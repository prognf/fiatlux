package models.CAmodels.tabled;

import components.types.RuleCode;
import main.Macro;
import main.MathMacro;
import topology.basics.TopologyCoder;

public class LookUpTableBinary {


	/*--------------------
	 * Abstract method definition
	 --------------------*/

	/* to be called by GFX controls */
	//abstract public void SetRuleNum(int in_RuleNum);

	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TV5;
	}

	/*--------------------
	 * Attributes
	 --------------------*/
	final protected int[] m_RuleBit;

	/*--------------------
	 * construction
	 --------------------*/

	// TODO : prevent the construction of empty tables
	public LookUpTableBinary(int sz_Table) {
		//Macro.Debug("Building table of size :" + sz_Table);
		/*
		 * if (!(sz_Table>0)){
			Macro.SystemWarning("A table of size 0 has been built !");
		}*/
		m_RuleBit= new int[sz_Table];
	}


	/*--------------------
	 * Rule Table operations
	 --------------------*/


	/** sets the look up table according to Wolfram's convention **/
	public void SetRule(RuleCode ruleNum) {
		//Macro.Debug(" ³³³³ setting look up table, code:" + ruleNum +" current table size:" + GetSize());
		long Wcode= ruleNum.GetCodeAsLong();
		int[] newtable= MathMacro.Decimal2BinaryTab(Wcode, m_RuleBit.length);
		for (int i=0; i < GetSize(); i++) {
			m_RuleBit[i]= newtable[i];
		}
		//m_RuleBit= MathMacro.Decimal2BinaryTab(Wcode, m_RuleBit.length);
		this.PrintTable();
	}

	/** for setting one bit of the lookup table only **/
	public void SetRuleBit(int i, int bitvalue) {
		m_RuleBit[i] = bitvalue;		
	}
	/** size = number of outputs */
	final public int GetSize(){
		return m_RuleBit.length;
	}

	/** main method for searching in the look-up table) */ 
	final public int getTransitionResult(int neighbourhoodByte) {
		return m_RuleBit[neighbourhoodByte];
	}

	/** returns the i-th bit of the transition table 
	 * same as getTransitionResut */
	public int getBitTable(int i) {
		return m_RuleBit[i];
	}

	/** conversion from binary table to RuleCode */
	public RuleCode GetRuleCode() {
		return MathMacro.BinaryTab2RuleCode(m_RuleBit);
	}

	/** transforms a transition table into a string **/ 
	public String GetTableAsString(){
		StringBuilder s = new StringBuilder("[ ");
		for (int bit :  m_RuleBit)
			s.append(bit).append(" ");
		s.append("]");
		return s.toString();
	}

	/** Scode = collection of outputs of the L.U.T. **/
	public String GetScode(){
		StringBuilder out = new StringBuilder();
		for (int bit : m_RuleBit){
			out.append(bit);
		}
		return out.toString();
	}

	public void PrintTable() {
		Macro.print( GetTableAsString() );		
	}
}
