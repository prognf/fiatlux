package models.CAmodels.tabled;

import components.allCells.OneRegisterIntCell;
import components.types.RuleCode;
import grafix.gfxTypes.elements.FLPanel;
import main.Macro;
import topology.basics.SuperTopology;
import topology.basics.TopologyCoder;

public class TotalisticModel extends TabledModel implements ITabledModel{

	public static final String NAME = "Totalistic";
	
	/*--------------------
	 * Constructor
	 --------------------*/
	
	/** used by the graphical interface **/	
	public TotalisticModel(){
		super(0); //empty table
	}
	
	/** always use InitialiseTopologyInfo after construction **/
	private TotalisticModel(int szTable) {
		super(szTable);
		Macro.Debug("œœœ NEW TOTALISTIC MODEL");
	}

	
	/** IMPORTANT tablesize should be neighbsize + 1 */
	static public TotalisticModel createNewRule(RuleCode Wcode, int tablesize) {
		TotalisticModel model= new TotalisticModel(tablesize);
		model.SetRuleCode(Wcode);
		return model;
	}
	
	/** always call before using the model !
	 * builds the lookup table with a proper size */
	// it would be good to remove this mechanism
	public void SubInitialiseTopologyInfo(SuperTopology topologyManager) {
		int NeighbSizeMax = topologyManager.GetActualNeighbMaxSize();
		int tableSize= (NeighbSizeMax + 1); // from 0 to N possibilities !
		//Macro.Debug(" **£££££££££ creating table of size :" + tableSize);
		ResizeLookUpTable(tableSize);
	}
	
	/*--------------------
	 * Abstract method definition
	 --------------------*/

	/** controller **/
	public FLPanel GetSpecificPanel() {
		return new BinaryTableControl(this); // callback intialized 
	}

	public final int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
		int nAlive = in_Cell.CountOccurenceOf(1);
		try {
			return GetBitTable(nAlive);
		} catch (Exception e) {
			e.printStackTrace();
			Macro.print("Nalive : " + nAlive);
			return 0;
		}
	}

	@Override
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TV5;
	}
	
	//PDDL @Override
	/*public RegularAutomatonViewer GetPlaneAutomatonViewer(
		IntC in_CellSize, 
		SuperTopology in_TopologyManager, RegularDynArray in_Automaton){
		IntC GridSize= ((PlanarTopology)in_TopologyManager).GetXYsize();
		RegularAutomatonViewer view= 
				new PddlViewer(in_CellSize, GridSize, in_Automaton);
		//view.SetPalette( GetPalette() );
		return view;
	}*/
	
	/*--------------------
	 * Attributes
	 --------------------*/

	/*--------------------
	 * Get & Set methods
	 --------------------*/


	/** changing rule */
	public void SetRuleCode(RuleCode Wcode) {
		super.SetRuleWcode(Wcode);
	}

	/*--------------------
	 * overrides & implementations
	 --------------------*/

	@Override
	public String GetName(){
		return GetKey() + "-" + GetRuleCode();
	}
	
}
