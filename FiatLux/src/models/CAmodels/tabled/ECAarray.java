package models.CAmodels.tabled;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTable;

import main.Macro;



/* array used to choose a rule 
 * with binary numbers for ECA models
 */
public class ECAarray extends JTable implements ActionListener{
	
	public ECAarray(int li, int col) {
		
		super(li,col) ;
		setBinaryValues() ;
	}
	
	// initialize the first line of the array
	public void setBinaryValues() {
		
		setValueAt("000",0,0) ;
		setValueAt("001",0,1) ;
		setValueAt("010",0,2) ;
		setValueAt("011",0,3) ;
		setValueAt("100",0,4) ;
		setValueAt("101",0,5) ;
		setValueAt("110",0,6) ;
		setValueAt("111",0,7) ;
		
	}
	
	/* converts the binary value at the specific column
	 * to a decimal number
	 */
	/*public void ConvertBinaryToDecimal(int value,int column) {
		switch (column) {
		case 0 :
			
		}
	}*/
	
	public void actionPerformed(ActionEvent e) {
		
		try {
			
		}
		catch (Exception ex){
			Macro.print("") ;
		}
	}
}
