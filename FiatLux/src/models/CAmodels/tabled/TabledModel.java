package models.CAmodels.tabled;

import components.types.RuleCode;
import initializers.SuperInitializer;
import initializers.CAinit.BinaryInitializer;
import main.Macro;
import models.CAmodels.ClassicalModel;


abstract public class TabledModel extends ClassicalModel implements ITabledModel {

	private LookUpTableBinary m_LookUpTable;  

	/*--------------------
	 * construction
	 --------------------*/

	protected TabledModel(int szTable){
		m_LookUpTable = new LookUpTableBinary(szTable);
	}

	protected void ResizeLookUpTable(int tableSize) {
		m_LookUpTable = new LookUpTableBinary(tableSize);
	}
	
	/*--------------------
	 * Get / Set
	 --------------------*/
	
	
	@Override
	public SuperInitializer GetDefaultInitializer() {
		return new BinaryInitializer();
	}

	/*--------------------
	 * Get / Set
	 --------------------*/
	final protected int GetTransitionResult(int neighbourhoodNum) {
		return m_LookUpTable.getTransitionResult(neighbourhoodNum);
	}

	final public int GetBitTable(int bitpos) {
		return m_LookUpTable.getBitTable(bitpos);
	}

	protected void SetRuleBit(int bitpos, int bitValue) {
		m_LookUpTable.SetRuleBit(bitpos, bitValue);
	}

	final public LookUpTableBinary GetLookUpTable() {
		return m_LookUpTable;
	}
	

	public RuleCode GetRuleCode() {
		return m_LookUpTable.GetRuleCode();
	}

	public void SetRuleWcode(RuleCode Wcode){
		m_LookUpTable.SetRule(Wcode);
	}

	protected void PrintTable() {
		m_LookUpTable.PrintTable();
	}

	/** use with care **/
	public int GetNeighbSize(){
		int sz= m_LookUpTable.GetSize();
		int neighbsize= (int)(Math.log(sz) / Math.log(2.)) ;
		Macro.print( "Non-verified log conversion, found neighb. size: " + neighbsize);
		return neighbsize;
	}
	
}
