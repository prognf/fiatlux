package models.CAmodels.tabled;

import grafix.gfxTypes.elements.FLLabel;
import grafix.gfxTypes.elements.FLPanel;


public class OuterTotalisticControl extends BinaryTableControlAbstract {
	private static final String S_CODE = "Table Code";

	OuterTotalisticControl(TabledModel in_model) {
		super(in_model);
		
		// SetBoxLayoutY();
		add(GetFirstLineForControl());

		FLPanel lineTabcontrol= new FLPanel();
		lineTabcontrol.SetGridBagLayout();
		
		// alterning bits for O-cells and 1-cells
		for (int bitPos2=0 ; bitPos2 < m_size/2; bitPos2++){
			FLPanel squareAndLabel= new FLPanel();
			squareAndLabel.SetGridBagLayout();
			
			
			FLLabel bitNum= new FLLabel(""+bitPos2);
			squareAndLabel.AddGridBag(bitNum,0,0);
			squareAndLabel.AddGridBag(m_activeRect[2*bitPos2],0,1);
			squareAndLabel.AddGridBag(m_activeRect[2*bitPos2+1],0,2);
			// adding the component + the mouse listener
			lineTabcontrol.AddGridBag(  squareAndLabel, bitPos2, 0 );
			
		}
		Add(lineTabcontrol);

		RefreshDisplay();
	}

	// refresh display
	public void RefreshDisplay() {
		m_inputcode.Update();
		UpdateAllCellTableControls();
	}
}
