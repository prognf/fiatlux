package models.CAmodels.tabled;

import components.types.FLString;
import components.types.FLStringList;
import components.types.IntegerList;
import components.types.RuleCode;
import main.Macro;
import main.MathMacro;

public class ECAlookUpTable extends LookUpTableBinary {


	public final static int OCTOPUS=8;
	final static String CONVENTIONID="I";
	private final static String[] LETTER = { "A", "B", "C", "D", "E", "F", "G", "H"};

	public ECAlookUpTable(RuleCode Wcode) {
		super(OCTOPUS);
		SetRule(Wcode);	
	}

	/*--------------------
	 * Get & Set methods
	 --------------------*/

	/** is transition i passive ?*/
	public boolean PA(int i) {
		int resP= (i/2) % 2;
		return getBitTable(i) == resP;
	}
	/** is transition i active ?*/
	public boolean AC(int i) {
		return !PA(i);
	}


	public static ECAlookUpTable EmptyTable(){
		return new ECAlookUpTable(new RuleCode(0));
	}


	/** * returns the transition code with empty string for Identity */
	static public String GetTransitionCodeRaw(RuleCode Wcode) {
		int[] RuleBit = MathMacro.Decimal2BinaryTab(Wcode.GetCodeAsLong(), OCTOPUS);
		int[] letter2WolframPos = { 0, 1, 4, 5, 2, 3, 6, 7 };
		int[] bitForAdding = { 1, 1, 1, 1, 0, 0, 0, 0 };
		StringBuilder code = new StringBuilder();
		for (int i = 0; i < OCTOPUS; i++) {
			int Wpos = letter2WolframPos[i];
			if (RuleBit[Wpos] == bitForAdding[i])
				code.append(LETTER[i]);
		}
		return code.toString();
	}
	
	/** returns "unpadded" transition code I for Identity and fixed width of String */
	static public String GetTransitionCodeUnPadded(RuleCode Wcode) {
		String bCode= GetTransitionCodeRaw(Wcode);
		// convention for identity
		if (bCode.length() == 0) {
			bCode= CONVENTIONID;
		}
		return bCode;
	}


	/** returns "pretty" transition code 
	 * I for Identity and fixed width of String */
	static public String GetTransitionCodePadded(RuleCode Wcode) {
		String bCode=GetTransitionCodeUnPadded(Wcode);
		// complementation in order to get a fixed string length
		String s = "        ";
		String s2 = s.substring(0, OCTOPUS - bCode.length());
		bCode= bCode + s2;
		return bCode.toString();
	}
	
	/* returns (2**pow) if s1 contains s2  (0 otherwise)*/
	static int Contains(String in_s1, String in_s2, int pow){
		return ((!in_s1.contains(in_s2))?0:MathMacro.Power(2,pow));
	}

	final static String REGEXP="A?B?C?D?E?F?G?H?I?";//"[A-H]+";
	/* returns if a string is a regular Tcode */
	static public boolean IsRegularTcode(String in_code){
		/* if (in_Tcode.matches(REGEXP)){
		Macro.print("Regular");
	} else{
		Macro.print("XXX");
	} */
		return	(in_code.length() > 0) &&  in_code.matches(REGEXP);
	}

	/** conversion from Tcode To Wcode convention : I is code for identity*/
	static public RuleCode TcodeToWcode(String in_Tcode){
		int Wcode = -1;
		if (in_Tcode.matches(REGEXP)){
			Wcode = 204 +
					Contains(in_Tcode,"A",0) * 1 +
					Contains(in_Tcode,"B",1) * 1 +
					Contains(in_Tcode,"C",4) * 1 +
					Contains(in_Tcode,"D",5) * 1 -
					Contains(in_Tcode,"E",2) * 1 -
					Contains(in_Tcode,"F",3) * 1 -
					Contains(in_Tcode,"G",6) * 1 -
					Contains(in_Tcode,"H",7) * 1 ;
		}	
		return RuleCode.FromInt(Wcode);
	}



	/** prints a string with various information on codes * */
	static public String GetCharacteristics(RuleCode Wcode) {
		long W= Wcode.GetCodeAsLong();
		String s = "ECA < " + FLString.Decimal2String(W) + " , "
				+ FLString.Decimal2String(GetReflexiveRule(W)) + " , "
				+ FLString.Decimal2String(GetConjugateRule(W)) + " , "
				+ FLString.Decimal2String(GetReflexiveConjugateRule(W)) + " >  "
				+ GetClassType(W) + " MR: "
				+ FLString.Decimal2String(GetMinimalRepresentative(W)) + " OP#: "
				+ GetTransitionCodePadded(Wcode);
		// GetTable(W);
		return s;
	}

	/** prints a string with various information on codes * */
	static public String GetSymmetries(long W) {
		long 	refl= GetReflexiveRule(W), 
				conj= GetConjugateRule(W), 
				reflconj= GetReflexiveConjugateRule(W);
		String s= "(" + W;
		if ( refl != W )
		{ s += "," + refl ; }
		if (( conj != W ) && (conj != refl) )
		{ s += "," + conj ; }
		if (( reflconj != W ) && (reflconj != refl) && (reflconj != conj) )
		{ s += "," + reflconj; }
		s+=")";
		return s;
	}


	/* type of symmetry */
	static public String GetClassType(long in_RuleNum) {
		String type = "AS";
		long R = in_RuleNum;
		long Rc = GetConjugateRule(in_RuleNum);
		long Rr = GetReflexiveRule(in_RuleNum);
		long Rcr = GetReflexiveConjugateRule(in_RuleNum);
		if (R == Rc) {
			if (R == Rr) {
				type = "S*";
			} else {
				type = "C+";
			}
		} else {
			if (R == Rcr) {
				type = "RC";
			}
			if (R == Rr) {
				type = "R+";
			}
		}
		return type;
	}

	static public FLStringList GetForbiddenBlocks(int in_RuleNum) {

		int[] RuleBit = MathMacro.Decimal2BinaryTab(in_RuleNum, OCTOPUS);
		FLStringList l = new FLStringList();
		for (int i = 0; i < OCTOPUS; i++) {
			int centralbit = (i / 2) % 2;
			// Macro.print( " i : " + i + " central bit " + centralbit );
			if (RuleBit[i] != centralbit) {
				l.Add(MathMacro.Int2BinaryString(i, 3));
			}
		}
		return l;
	}

	static public long GetMinimalRepresentative(long in_RuleNum) {
		long x0, x1, x2, x3;
		x0 = in_RuleNum;
		x1 = GetReflexiveConjugateRule(in_RuleNum);
		x2 = GetReflexiveRule(in_RuleNum);
		x3 = GetConjugateRule(in_RuleNum);
		if (x1 < x0)
			x0 = x1;
		if (x2 < x0)
			x0 = x2;
		if (x3 < x0)
			x0 = x3;
		return x0;
	}

	/* 0/1 and L/R symmetry */
	static public long GetReflexiveConjugateRule(long in_RuleNum) {
		return GetReflexiveRule(GetConjugateRule(in_RuleNum));
	}

	/* L/R symmetry */
	static public long GetReflexiveRule(long in_RuleNum) {
		int[] NewRuleBit = new int[OCTOPUS];
		int[] permut = { 0, 4, 2, 6, 1, 5, 3, 7 };
		int[] RuleBit = MathMacro.Decimal2BinaryTab(in_RuleNum, OCTOPUS);
		/* transfo */
		for (int i = 0; i < OCTOPUS; i++) {
			NewRuleBit[i] = RuleBit[permut[i]];
		}
		/* decoding */
		return MathMacro.BinaryTab2Decimal(NewRuleBit);

	}

	/* O/1 symmetry */
	static public long GetConjugateRule(long in_RuleNum) {
		int[] NewRuleBit = new int[OCTOPUS];
		int[] RuleBit = MathMacro.Decimal2BinaryTab(in_RuleNum, OCTOPUS);
		/* transfo */
		for (int i = 0; i < OCTOPUS; i++) {
			NewRuleBit[i] = 1 - RuleBit[OCTOPUS - i - 1];
		}
		/* decoding */
		return MathMacro.BinaryTab2Decimal(NewRuleBit);
	}

	final static String OUTPUT="ECAmacros.txt";
	
	/** generates text for macros (Latex, HTML, ...) */
	static public void GenerateEcaMacros() {
		// 256 rules table generation
		FLStringList codes = new FLStringList();
		for (int W = 0; W < 256; W++) {
			if (true/*W == GetMinimalRepresentative(W)*/) {
				/*OLD if (count==0){
					codes.Add("_BEGLINE_");
				}
				codes.Add( "_RESULT_ECA_(" + W + "," + GetTransitionCode(RuleCode.FromInt(W) ) + ")");
				if (count==3){
					codes.Add("_ENDLINE_");
				}
				count= (count +1)%4;*/
				String trW=GetTransitionCodePadded(RuleCode.FromInt(W)).trim();
				String macrotxt= 
						String.format("\\ifstrequal{#1}{%d}{%s}{}",W,trW);
				codes.Add(macrotxt);		
			}
		}
		codes.WriteToFile(OUTPUT);
	}


	/** generates text for macros (Latex, HTML, ...) */
	static public void GenerateEcaMinRepList() {
		// 256 rules table generation
		FLStringList codes = new FLStringList();
		for (int W = 0; W < 256; W++) {
			if (W == (int)GetMinimalRepresentative((long)W)) {
				String trW=GetTransitionCodePadded(RuleCode.FromInt(W)).trim();
				String macrotxt= String.format("./runExp %d %s",W,trW);
				codes.Add(macrotxt);		
			}
		}
		codes.WriteToFile(OUTPUT);
	}

	
	/** generates 88 minimal rules out of 256 rules **/
	static public IntegerList GetEcaMinimalsList(){	     
		IntegerList codes= new IntegerList();
		for (int W=0; W<256; W++){
			if (W == models.CAmodels.tabled.ECAlookUpTable.GetMinimalRepresentative(W) ){
				codes.addVal(W);				
			}
		}
		return codes;	
	}  

	/** generates rules invariant by reflection and conjugacy **/
	static public IntegerList GetRCinvariants(){	     
		IntegerList codes= new IntegerList();
		for (int W=0; W<256; W++){
			if (W == GetConjugateRule(W) && (W==GetReflexiveRule(W)) ){
				codes.addVal(W);				
			}
		}
		return codes;	
	}  
	
	/** generates rules invariant by reflection and conjugacy **/
	static public IntegerList GetCinvariants(){	     
		IntegerList codes= new IntegerList();
		for (int W=0; W<256; W++){
			if ( W == GetConjugateRule(W) ){
				codes.addVal(W);				
			}
		}
		return codes;	
	}
	
	/** 0-quiescent rules, minimals with regard to reflection symmetry **/
	static public IntegerList GetQuiescentMinimalsReflectionSymmetry(){	     
		IntegerList codes= new IntegerList();
		for (int W=2; W<256; W+=2){
			if (W <= GetReflexiveRule(W)){
				codes.addVal(W);				
			}
		}
		return codes;	
	}

	/*--------------------
	 * for coalescence studies
	 --------------------*/

	final static String [] OABC = { "O", "A", "B", "C"};

	static private String BinCoupleToLetter(int bit1, int bit2){
		return OABC[2*bit1+bit2];
	} 

	static String TwoIntToLetterTriple(int a, int b){
		String result="";
		// binary decomposition
		int a0= a %2 ; a /= 2;
		int a1= a %2 ; a /= 2;
		int a2= a %2 ;
		int b0= b %2 ; b /= 2;
		int b1= b %2 ; b /= 2;
		int b2= b %2 ;
		result= BinCoupleToLetter(a2,b2) + BinCoupleToLetter(a1,b1) + BinCoupleToLetter(a0,b0);
		return result;
	}


	String LetterTransition(int a, int b){
		int ra= getBitTable(a);
		int rb= getBitTable(b);
		return BinCoupleToLetter(ra,rb);
	}

	/* lists the rules which depend only on 2 neighbours */
	public static void IdentifyECARule(int index){
		int a=index%2; index>>=1;
		int b=index%2; index>>=1;
		int c=index%2; index>>=1;
		int d=index%2; index>>=1;
		
		ECAlookUpTable 
		tLM= ECAlookUpTable.EmptyTable(),
		tLR= ECAlookUpTable.EmptyTable();
		ProcessT(tLM,tLR,0,0,a);
		ProcessT(tLM,tLR,0,1,b);
		ProcessT(tLM,tLR,1,0,c);
		ProcessT(tLM,tLR,1,1,d);
		RuleCode wLM= tLM.GetRuleCode(), wLR= tLR.GetRuleCode();
		String out= wLM.toString() + ":" + ECAlookUpTable.GetCharacteristics(wLM) + "  -- ";
		out+= wLR.toString() + ":" + ECAlookUpTable.GetCharacteristics(wLR);
		Macro.fPrint("ZOZOPOP %d %d %d %d >>> %s ZOZOPOP", a, b, c, d, out);
	}

	private static void ProcessT(ECAlookUpTable tLM, ECAlookUpTable tLR, 
			int b1, int b2, int val) {
		SetTable(tLM,b1,b2,0,val);
		SetTable(tLM,b1,b2,1,val);
		SetTable(tLR,b1,0,b2,val);
		SetTable(tLR,b1,1,b2,val);
	}

	private static void SetTable(ECAlookUpTable table, int b1, int b2, int b3,
			int bitval) {
		int bitpos= 4*b1+2*b2+b3;
		table.SetRuleBit(bitpos, bitval);
	}

}
