package models.CAmodels.tabled;

import components.allCells.MultiRegisterCell;
import components.allCells.OneRegisterIntCell;
import components.types.RuleCode;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.elements.FLActionButton;
import grafix.gfxTypes.elements.FLPanel;
import main.Macro;
import main.tables.PlotterSelectControl;
import models.CAmodels.binary.BinaryModel;
import topology.basics.TopologyCoder;

/*--------------------
 * ECA model
 *--------------------*/
public class ECAmodel extends BinaryModel implements ITabledModel {
	public final static String NAME="ECA";
	final static boolean filter= false; // prints only the active the transitions 
	final static int OCTOPUS= 8;
	private static final String LBLRULEINFO = "rule info";

	public final static int[] ECA_minimals = {
		0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 
		18, 19, 22, 23, 24, 25, 26, 27, 28, 29, 30, 32, 33, 34,
		35, 36, 37, 38, 40, 41, 42, 43, 44, 45, 46, 50, 51, 54,
		56, 57, 58, 60, 62, 72, 73, 74, 76, 77, 78, 90, 94, 104,
		105, 106, 108, 110, 122, 126, 128, 130, 132, 134, 136,
		138, 140, 142, 146, 150, 152, 154, 156, 160, 162, 164,
		168, 170, 172, 178, 184, 200, 204, 232	
	};

	ECAlookUpTable m_LookUpTable;
	ECAcontrol m_control;

	/*--------------------
	 * Constructor
	 --------------------*/
	public ECAmodel(RuleCode Wcode) {
		// init
		m_LookUpTable=  new ECAlookUpTable(Wcode);
	}


	public ECAmodel() {
		this(RuleCode.DEFAULTECACODE);
	}

	public static ECAmodel NewRule(int Wcode) {
		return new ECAmodel(new RuleCode(Wcode));
	}

	public static ECAmodel NewRuleDefault() {
		return new ECAmodel();
	}
	/*--------------------
	 * Abstract method definition
	 --------------------*/

	/* MAIN FUNCTION */
	public int ApplyLocalFunction(OneRegisterIntCell in_Cell) {
		int NeighbourhoodByte = 
				in_Cell.ReadNeighbourState(2)
				| in_Cell.ReadNeighbourState(1) << 1
				| in_Cell.ReadNeighbourState(0) << 2;
		return m_LookUpTable.getTransitionResult(NeighbourhoodByte);
	}


	/** Special for coalescence **/
	public int ApplyLocalFunctionForCoalescence(MultiRegisterCell in_Cell, int layer) {
		int 
		left= in_Cell.GetNeighbourStateI(layer, 0),
		middle= in_Cell.GetNeighbourStateI(layer, 1),
		right= in_Cell.GetNeighbourStateI(layer, 2);
		int NeighbourhoodByte = 
				right| middle  << 1	| left << 2;
		return m_LookUpTable.getTransitionResult(NeighbourhoodByte);
	}

	/*--------------------
	 * Get / Set
	 --------------------*/

	public RuleCode GetRuleCode() {
		return m_LookUpTable.GetRuleCode();
	}


	public void SetRule(RuleCode Wcode) {
		m_LookUpTable.SetRule(Wcode);
		if (m_control!=null){
			m_control.Update();
		}
	}

	public ECAlookUpTable GetECAlookUpTable(){
		return m_LookUpTable;
	}

	@Override
	public FLPanel GetSpecificPanel() {
		m_control = new ECAcontrol(m_LookUpTable);
		FLPanel res = FLPanel.NewPanel(m_control, new ButtonInfo());
		res.setOpaque(false);
		return res;
	}


	@Override
	public String [] GetPlotterSelection1D() {
		return PlotterSelectControl.ECAselect;
	}  



	/*--------------------
	 * GFX
	 --------------------*/

	public void repaint(){
		m_control.repaint();
		//super.repaint();
	}

	/** palette **/
	@Override
	public PaintToolKit GetPalette(){
		PaintToolKit paint= PaintToolKit.GetDefaultPalette();
		//paint.SetColor(2, FLColor.c_grey2);
		return paint;
	}

	class ButtonInfo extends FLActionButton {
		public ButtonInfo() {
			super(LBLRULEINFO);
		}
		@Override
		public void DoAction() {
			RuleCode W = m_LookUpTable.GetRuleCode();
			String txt = ECAlookUpTable.GetCharacteristics(W);
			String transitions= m_LookUpTable.GetScode();
			txt+= "::" + transitions;
			Macro.print(txt);
			Macro.UserMessage(txt);
		}
	}


	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TLR1;
	}

	@Override
	public LookUpTableBinary GetLookUpTable() {
		return m_LookUpTable;
	}

	@Override
	public int GetBitTable(int i) {
		return m_LookUpTable.getBitTable(i);
	}


	/* special override **/
	/* -> Model Name (e.g. "ECA232" ) **/
	@Override
	public String GetName() {
		//FIXME this is recalculated al the time !!!
		return NAME + m_LookUpTable.GetRuleCode();
	}

	/*--------------------
	 * testing
	 --------------------*/
	public static void DoTest() {
		/*	Macro.BeginTest(m_ClassName);

		Macro.print("CoalTest:" + BinCoupleToLetter(1,0));
		Macro.print("CoalTest:" + TwoIntToLetterTriple(5,7));

		ECAmodel myRule= new ECAmodel(46);
		Macro.print("CoalTest:" + myRule.LetterTransition(1,1));
		Macro.print("CoalTest:" );
		myRule.PrintTable();

		Macro.print(GetCharacteristics(240));

		Macro.EndTest();*/
	}

	/** command line call */
	public static void main(String[] argv) {
		//ECAlookUpTable.GenerateEcaMacros();
		ECAlookUpTable.GenerateEcaMinRepList();
	}
}
