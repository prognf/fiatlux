package models.CAmodelsvar.receptors;

import components.allCells.ReceptorCell;
import main.Macro;


class DoubleBranchCell extends ReceptorCell {

	private static final int LEFT = 0, RIGHT=2;

	private DoubleBranchProcessModel m_model;

	/*--------------------
	 * constructor
	 --------------------*/

	public DoubleBranchCell(DoubleBranchProcessModel in_model) {
		m_model= in_model;
	}


	/*--------------------
	 * rule calculus
	 --------------------*/
	
	/* next state calculus */
	final public void sig_UpdateBuffer() {
		if ((m_state==0)){
			// nothing to do
		} else { /* we are not empty */
			switch( m_model.GetBehaviour()){
			case 0: // do nothing
				IncrementDelta(this);
				break;
			case 1: // "go left"
				IncrementDelta(GetNeighbour(LEFT));
				break;
			case 2: // go right
				IncrementDelta(GetNeighbour(RIGHT));
				break;
			case 3: // "branch"
				IncrementDelta(GetNeighbour(LEFT));
				IncrementDelta(this);
				IncrementDelta(GetNeighbour(RIGHT));
				break;
			default:
				Macro.FatalError("bad state returned !");
			}
		}
	}
	
	/* transition operates */
	final public void sig_MakeTransition() {
		m_state= m_buffer % 2;
		m_buffer=0;
	}


	
}
