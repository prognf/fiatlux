package models.CAmodelsvar.receptors;

import components.allCells.Cell;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.controllers.DoubleController;
import grafix.gfxTypes.elements.FLPanel;
import initializers.SuperInitializer;
import initializers.CAinit.BinaryInitializer;
import models.CAmodels.CellularModel;
import topology.basics.TopologyCoder;

/*--------------------
 *see associated cell for rules description
 *****--------------------*/
public class DoubleBranchProcessModel extends CellularModel {

	public final static double DEF_Ppar= 0.1 ; 
 	
	private double m_ParP= DEF_Ppar; // proba, par of the model
	
	final static String S_PARP="Par p:";
	
	final public static String NAME="DoubleBranch"; 
	
	/*--------------------
	 * overrides  
	 *--------------------*/
	@Override
	public SuperInitializer GetDefaultInitializer() {
		return new BinaryInitializer();
	}

	@Override
	public Cell GetNewCell() {
		return new DoubleBranchCell(this); 
	}

	@Override
	public FLPanel GetSpecificPanel() {
		return new PparField();
	}
	
	/* overrides */
	@Override
	public PaintToolKit GetPalette(){
		FLColor [] palette = new FLColor [3];
		palette[0] = FLColor.c_black;
		palette[1] = FLColor.c_yellow;
		palette[2] = FLColor.c_green; 
		return new PaintToolKit(palette); 
	}
	
	@Override
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TLR1;
	}
	/*--------------------
	 * construction  
	 *--------------------*/
	
	public DoubleBranchProcessModel() {
		super();
	}

	/*--------------------
	 * get / set  
	 *--------------------*/
	
	
	
	
	/*--------------------
	 * parP reading / setting
	 *--------------------*/
	
	/** not optimised **/
	public int GetBehaviour() {
		double proba= 3 * m_ParP;
		if (RandomEventDouble(proba) ){
			return 1  + RandomInt(3);
		} else {
			return 0;
		}		
	}
	
	final public void SetParP(double in_par){
		m_ParP= in_par;
	}
	
	final public double GetParP(){
		return m_ParP;
	}
	
	
	/*--------------------
	 * control
	 *--------------------*/
	
	class PparField extends DoubleController {

		public PparField() {
			super(S_PARP,2);
		}

		
		@Override
		public double ReadDouble() {
				return GetParP();
		}

		@Override
		public void SetDouble(double val) {
			SetParP(val);			
		}
	}

}
