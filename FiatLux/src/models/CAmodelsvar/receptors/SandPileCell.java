package models.CAmodelsvar.receptors;

import components.allCells.OneRegisterIntCell;
import components.allCells.ReceptorCell;
import models.CAmodels.CellularModel;

class SandPileCell extends ReceptorCell {


	private CellularModel m_model;
	private int m_Npointer; // index that designates one neighbour

	public SandPileCell(SandPileModel randomWalkModel) {
		m_model= randomWalkModel;
	}

	/*--------------------
	 * Pointer Management
	 --------------------*/

	final public void RandomizePointer() {
		int size = GetNeighbourhoodSize();
		if (size > 0)
			m_Npointer = m_model.RandomInt(size);
	/*	else 
			Macro.SystemWarning("found empty neighb");*/
	}

	final public OneRegisterIntCell GetPointedNeighbour() {
		return GetNeighbour(m_Npointer);
	}

	final public void AdvancePointer() {
		m_Npointer = ++m_Npointer % GetNeighbourhoodSize();
	}


	final public void sig_UpdateBuffer() {
		//		 state transition
		int state = GetState();
		if (state > 0) { // we are not empty
			// we select a neighbour randomly
			if (m_model.RandomEventInt(RandomWalkModel.AGITATION_RATE)){
				this.RandomizePointer();
				OneRegisterIntCell targetNeighb = GetPointedNeighbour();
				GiveOneParticle(this,targetNeighb);
			}
		}
	}

	/** updates state and delta */
	final public void sig_MakeTransition() {		
		m_state += m_buffer;
		m_buffer= 0;
	}

}
