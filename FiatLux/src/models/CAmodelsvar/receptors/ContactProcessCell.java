package models.CAmodelsvar.receptors;

import components.allCells.OneRegisterIntCell;
import components.allCells.ReceptorCell;
import components.types.IntegerList;
import main.Macro;


class ContactProcessCell extends ReceptorCell {

	final static int RECOVER=-1;
	
	final static String CLASSNAME="ContactProcessCell";

	private ContactProcessModel m_model;
	
	/*--------------------
	 * constructor
	 --------------------*/

	public ContactProcessCell(ContactProcessModel in_model) {
		m_model= in_model;
	}


	/*--------------------
	 * rule calculus
	 --------------------*/
	final public void sig_UpdateBuffer() {
		//		 state transition
		int state = GetState();
		if (state > 0) { // we are not empty
			// we select a neighbour randomly
			if (m_model.IsInfecting()){  // infection ?
				OneRegisterIntCell targetNeighb = GetFreeNeighbour();
				if (targetNeighb != null){
					OneRegisterIntCell.IncrementDelta(targetNeighb); // zero to one signal
				}
			}
			if (m_model.IsRecovered()){
				this.m_buffer= RECOVER;
			}
		}
	}
	
	/* selects one free neighbour at random */
	final private OneRegisterIntCell GetFreeNeighbour() {
		OneRegisterIntCell SelectedNeighbour= null;
		int size= GetNeighbourhoodSize();
		
		// which are the empty neighbours?
		IntegerList indices= new IntegerList();
		for (int i=0 ; i < size ; i++){
			if (GetNeighbour(i).GetState()==0){
				indices.addVal(i);
			}
		}
		if (indices.size()>0){
			SelectedNeighbour= GetNeighbour(indices.ChooseOneAtRandom(m_model.GetRandomizer()));
		}
		return SelectedNeighbour;
	}

	
	final public void sig_MakeTransition() {
		if ((m_state==0)){
			if (m_buffer>0){
				m_buffer= 0;
				m_state= 1;
			} else if (m_buffer<0){
				Macro.FatalError(this, "transition : problem1 in updating " );
			}
		} else { /* m_State == 1 */
			if (m_buffer==-1){
				m_buffer= 0;
				m_state= 0;
			} else if (m_buffer>0){
				Macro.FatalError(this, "transition : problem2 in updating " );
			}
		}
		
	}


	

}
