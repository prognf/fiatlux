package models.CAmodelsvar.receptors;

import components.allCells.OneRegisterIntCell;
import components.allCells.ReceptorCell;

class TasepCell extends ReceptorCell {


	private TasepModel m_model;

	public TasepCell(TasepModel model) {
		m_model= model;
	}


	/*--------------------
	 * signals
	 --------------------*/

	/** state transition **/
	final public void sig_UpdateBuffer() {
		OneRegisterIntCell koko= GetNeighbour(2);
		int state1 = GetState(), state2= koko.GetState();
		boolean jump = (state1 > 0) 
				&& (state2==0) // jump possible
				&& m_model.Jump();
		if ( jump ){
			GiveOneParticle(this, koko);
		}
	}
	
	/** updates state and delta */
	final public void sig_MakeTransition() {		
		m_state += m_buffer;
		m_buffer= 0;
	}


}
