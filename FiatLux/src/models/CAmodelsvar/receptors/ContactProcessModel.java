package models.CAmodelsvar.receptors;

import components.allCells.Cell;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.controllers.DoubleController;
import grafix.gfxTypes.elements.FLPanel;
import initializers.SuperInitializer;
import initializers.CAinit.BinaryInitializer;
import models.CAmodels.CellularModel;

/*--------------------
 *see associated cell for rules description
 *****--------------------*/
public class ContactProcessModel extends CellularModel {

	public final static double DEF_Ppar= 0.50 ; 
 	
	private double m_ParP= DEF_Ppar; // proba, par of the model
	
	final static String S_PARP="Par p:";
	
	final public static String NAME="ContactProcess"; 
	
	/*--------------------
	 * overrides  
	 *--------------------*/
	/*
	 * override 
	 */
	public SuperInitializer GetDefaultInitializer() {
		return new BinaryInitializer();
	}

	/* overrides
	 */
	public FLPanel GetSpecificPanel() {
		return new PparField();
	}
	
	public Cell GetNewCell() {
		return new ContactProcessCell(this); 
	}

	/* overrides */
	@Override
	public PaintToolKit GetPalette(){
		FLColor [] palette = new FLColor [3];
		palette[0] = FLColor.c_black;
		palette[1] = FLColor.c_yellow;
		palette[2] = FLColor.c_green; 
		return new PaintToolKit(palette); 
	}
	
	
	/*--------------------
	 * parP reading / setting
	 *--------------------*/
	
	final public void SetParP(double in_par){
		m_ParP= in_par;
	}
	
	final public double GetParP(){
		return m_ParP;
	}
	
	/* transiton from 1 to 0 */
	final public boolean IsRecovered(){
		return RandomEventDouble(1 - m_ParP);
	}

	/* transiton from 0 to 1 */
	final public boolean IsInfecting() {
		//Macro.Debug("p:" + m_ParP);
		return RandomEventDouble(m_ParP);
	}
	
	
	/*--------------------
	 * control
	 *--------------------*/
	
	class PparField extends DoubleController {

		public PparField() {
			super(S_PARP,2);
		}

		
		@Override
		public double ReadDouble() {
				return GetParP();
		}

		@Override
		public void SetDouble(double val) {
			SetParP(val);			
		}
	}

}
