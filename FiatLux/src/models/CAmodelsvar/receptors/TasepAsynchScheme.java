package models.CAmodelsvar.receptors;

import grafix.gfxTypes.elements.FLPanel;
import updatingScheme.SimpleArrayScheme;


public class TasepAsynchScheme extends SimpleArrayScheme {

	
	public final static String NAME="TasepScheme";
	/*--------------------
	 * attributes
	 --------------------*/

	
	/*--------------------
	 * construction
	 --------------------*/

	
	
	/* use with care */
	public TasepAsynchScheme() {
		super();
	}
	
	
	
	/*--------------------
	 * Signals
	 --------------------*/
	public void sig_NextStep() {
		for (int i = 0; i < GetSize(); i++) {
			UpdateOneCell();
		}
		//UpdateOneCell();
	}

	private void UpdateOneCell() {
		int cell = RandomInt(GetSize());
		PrepareTransitionCell(cell);
		MakeTransitionCell(cell);
		int next= (cell + 1)% GetSize();
		MakeTransitionCell(next);		
	}



	public FLPanel GetSpecificPanel() {
		return null;
	}

	public void UpdateOneCell(int pos){
		PrepareTransitionCell(pos);
		MakeTransitionCell(pos);
	}
	
	/*--------------------
	 * Get/Set : dynamics method
	 --------------------*/
	
	@Override
	public String GetName() {
		return NAME;
	}


	
	

	
	

}
