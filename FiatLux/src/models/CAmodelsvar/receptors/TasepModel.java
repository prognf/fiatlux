package models.CAmodelsvar.receptors;

import components.allCells.Cell;
import components.types.ProbaPar;
import grafix.gfxTypes.elements.FLPanel;
import initializers.SuperInitializer;
import initializers.CAinit.BinaryInitializer;
import models.CAmodels.CellularModel;
import topology.basics.TopologyCoder;
import updatingScheme.UpdatingScheme;

/*--------------------
 * TASEP
 *****--------------------*/
public class TasepModel extends CellularModel {

	public static final String NAME = "Tasep";
	private static final double DEF_P = 0.5;
	private static final String LABEL = "jump prb.";
	ProbaPar m_jumpP = new ProbaPar(LABEL, DEF_P);
	
	
	public TasepModel() {

	}

	/*public PaintToolKit GetPalette(){
		/*PaintToolKit palette= PaintToolKit.GetRainbow(2);
		palette.SetColor(0, FLColor.c_white);
		palette.SetColor(1, FLColor.c_blue);
		return palette;
	}*/
	
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TLR1;
	}

	
	 public SuperInitializer GetDefaultInitializer() { 
		 return new BinaryInitializer();
		 //return new ReceptorInitDevice(); 
	}
	
	 public UpdatingScheme GetDefaultUpdatingScheme(){
		 return new TasepAsynchScheme();
	 }

	
	public Cell GetNewCell() {
		return new TasepCell(this); // HERE IS THE CODE !
	}
	
	public FLPanel GetSpecificPanel(){
		return m_jumpP.GetControl();
	}
	
	public boolean Jump() {
		return RandomEventDouble(m_jumpP.GetVal());
	}
	
	public ProbaPar GetJumpProba(){
		return m_jumpP;
	}
	
}
