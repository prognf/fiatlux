package models.CAmodelsvar.receptors;

import components.allCells.OneRegisterIntCell;
import components.allCells.ReceptorCell;
import models.CAmodels.CellularModel;

class RandomWalkCell extends ReceptorCell {


	private CellularModel m_model;
	private int m_Npointer; // index that designates one neighbour

	public RandomWalkCell(RandomWalkModel randomWalkModel) {
		m_model= randomWalkModel;
	}

	
	/*--------------------
	 * signals
	 --------------------*/

	/**		 state transition **/
	final public void sig_UpdateBuffer() {
		int state = GetState();
		if (state > 0) { // we are not empty
			// we select a neighbour randomly
			if (m_model.RandomEventInt(RandomWalkModel.AGITATION_RATE)){
				this.RandomizePointer();
				OneRegisterIntCell targetNeighb = GetPointedNeighbour();
				GiveOneParticle(this,targetNeighb);
			}
		}
	}
	
	/* updates state and delta */
	final public void sig_MakeTransition() {		
		m_state += m_buffer;
		m_buffer= 0;
	}
	
	/*--------------------
	 * Pointer Management
	 --------------------*/

	final public void RandomizePointer() {
		int size = GetNeighbourhoodSize();
		if (size > 0)
			m_Npointer = m_model.RandomInt(size);
	/*	else 
			Macro.SystemWarning("found empty neighb");*/
	}

	final public OneRegisterIntCell GetPointedNeighbour() {
		return GetNeighbour(m_Npointer);
	}

	final public void AdvancePointer() {
		m_Npointer = ++m_Npointer % GetNeighbourhoodSize();
	}



	

}
