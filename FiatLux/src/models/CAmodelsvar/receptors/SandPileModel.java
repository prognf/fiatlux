package models.CAmodelsvar.receptors;

import components.allCells.Cell;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import initializers.SuperInitializer;
import initializers.CAinit.ReceptorInitializer;
import models.CAmodels.CellularModel;
import topology.basics.TopologyCoder;

/*--------------------
 * Abelian Sand Pile model
 *****--------------------*/
public class SandPileModel extends CellularModel {

	public static final String NAME = "SandPile";
	private static final int NCOL = 20;// max. number of colors
	
	public SandPileModel() {

	}

	public PaintToolKit GetPalette(){
		//PaintToolKit palette= PaintToolKit.GetRandomPalette(NCOL);
		PaintToolKit palette= PaintToolKit.GetRainbow(NCOL);
		palette.SetColor(0, FLColor.c_white);
		palette.SetColor(1, FLColor.c_blue);
		return palette;
	}
	
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TV5;
	}

	
	public final static int AGITATION_RATE = 100; // (in %)
	

	
	 public SuperInitializer GetDefaultInitializer() { 
		 return new ReceptorInitializer(); }
	

	
	public Cell GetNewCell() {
		return new SandPileCell(this); // HERE IS THE CODE !
	}
	
	
}
