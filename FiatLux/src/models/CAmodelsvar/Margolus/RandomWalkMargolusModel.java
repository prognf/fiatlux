package models.CAmodelsvar.Margolus;

public class RandomWalkMargolusModel extends MargolusModel {

	public static final String NAME = "Margolus-RW";

	void UpdateBuffer(MargolusTessella inout_Tessella) {
		inout_Tessella.Rotation( RandomInt(2) );
	}

}
