package models.CAmodelsvar.Margolus;

public class WalkerTexasRangerMargolusModel extends MargolusModel {

	public static final String NAME = "Margolus-rangers";
	
	
	void UpdateBuffer(MargolusTessella inout_Tessella) {
		int nb_particle = inout_Tessella.GetTotalPopulation();
		
		switch (nb_particle) {
		case 1:
			inout_Tessella.Rotation( RandomInt(2) );
			break;
		case 2:
			if (inout_Tessella.GetCellState(0)==inout_Tessella.GetCellState(2)){
				// Particles on one diagonal
				if(RandomInt(2)==0){
					inout_Tessella.Identity();
				} else {
					inout_Tessella.Inversion();
				}
			} else {
				// Particles on the same side
				inout_Tessella.Inversion();
			}
			break;
		case 0:
		case 3:
		case 4:
			inout_Tessella.Identity();
			break;
		}
		
	}

}
