package models.CAmodelsvar.Margolus;

import components.allCells.Tessella;


public class MargolusTessella extends Tessella {
	
	final static int N_MARGO=4;
	
	/*--------------------
	 * Attributes
	 --------------------*/
	MargolusModel  m_MargolusModel;
	MargolusCell[] m_MargolusCell;
	int            m_s0, m_s1, m_s2, m_s3;
	
	/*--------------------
	 * Construction
	 --------------------*/

	public MargolusTessella(MargolusModel in_MargolusModel, MargolusCell[] in_CellArray){
		super(in_CellArray);
		m_MargolusModel = in_MargolusModel;
		m_MargolusCell= in_CellArray;
	}

	
	/*--------------------
	 * Get/Set
	 --------------------*/	
	/* returns the number of cells in state 1*/
	final protected int GetTotalPopulation(){
		return 
		GetCellState(0)+
		GetCellState(1)+
		GetCellState(2)+
		GetCellState(3);
	}
	
	final protected int GetCellState(int cell){
		return m_MargolusCell[cell].GetState();
	}
	
	
	
	public boolean IsCheckerboard() {
		int s= GetCellState(0);
		return (s==GetCellState(2)) && (GetCell(1)==GetCell(3)) && (s!=GetCellState(1));
	}
	
	/*--------------------
	 * Margolus basic operations
	 * *************************************************************************/

	final void Identity(){
		m_s0 = GetCellState(0);
		m_s1 = GetCellState(1);
		m_s2 = GetCellState(2);
		m_s3 = GetCellState(3);
	}
	
	final void RightRotation(){
		m_s0 = GetCellState(3);
		m_s1 = GetCellState(0);
		m_s2 = GetCellState(1);
		m_s3 = GetCellState(2);
	}

	final void LeftRotation(){
		m_s0 = GetCellState(1);
		m_s1 = GetCellState(2);
		m_s2 = GetCellState(3);
		m_s3 = GetCellState(0);
	}

	final void CentralSymmetry(){
		m_s0 = GetCellState(2);
		m_s1 = GetCellState(3);
		m_s2 = GetCellState(0);
		m_s3 = GetCellState(1);
	}

	final void Inversion(){
		m_s0 = 1-GetCellState(0);
		m_s1 = 1-GetCellState(1);
		m_s2 = 1-GetCellState(2);
		m_s3 = 1-GetCellState(3);
	}

	
	/** sets a configuration into a checkerboard 
	 * s is 0 or 1 **/
	final void CheckerboardTransform(int s){
		m_s0 = s;
		m_s1 = 1-s;
		m_s2 = s;
		m_s3 = 1-s;		
	}
	
	final void Rotation(int direction){
		if(direction==0){
			RightRotation();
		} else {
			LeftRotation();
		}
	}
	
	/*--------------------
	 * Signals
	 --------------------*/

	@Override
	public void sig_UpdateBuffer() {	
		m_MargolusModel.UpdateBuffer(this);		
	}

	@Override
	public void sig_MakeTransition(){
			m_MargolusCell[0].SetState(m_s0);
			m_MargolusCell[1].SetState(m_s1);
			m_MargolusCell[2].SetState(m_s2);
			m_MargolusCell[3].SetState(m_s3);
	}
	
	
	public void PrintState() {
		// TODO Auto-genera		
	}



	
}
