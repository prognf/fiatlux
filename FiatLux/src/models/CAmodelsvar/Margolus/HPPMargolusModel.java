package models.CAmodelsvar.Margolus;

public class HPPMargolusModel extends MargolusModel {

	final static String NAME = "BallisticMargolus";
		
	void UpdateBuffer(MargolusTessella inout_Tessella) {
		int nb_particle = inout_Tessella.GetTotalPopulation();
		if (nb_particle==2){
			if (inout_Tessella.IsCheckerboard()){
				inout_Tessella.Identity();	
			} else{
				if (RandomEventInt(100)){
					int r=RandomInt(2);	
					inout_Tessella.PrintState();
					inout_Tessella.CheckerboardTransform(r);
					inout_Tessella.PrintState();
				} else{
					inout_Tessella.Identity();
				}
			}
		} 
		else {
			inout_Tessella.Identity();
		}

	}
}