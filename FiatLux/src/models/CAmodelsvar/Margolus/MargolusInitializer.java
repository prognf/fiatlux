package models.CAmodelsvar.Margolus;

import grafix.gfxTypes.IntField;
import grafix.gfxTypes.elements.FLPanel;
import initializers.CAinit.OneRegisterInitializer;

public class MargolusInitializer extends OneRegisterInitializer {
	final static int DEFPOPSIZE=0, DEFSQUARELEN=0;

	/*--------------------
	 * abstract methods definition
	 --------------------*/
	public void SubInit() {
		MargolusInit();
		//StaticInit();
	}

	public FLPanel GetSpecificPanel() {
		int pop= GetLsize() / 2;
		mF_PopSize.SetValue(pop);
		FLPanel p = FLPanel.NewPanelVertical(mF_Density, mF_PopSize, mF_SquareLen);
		p.setOpaque(false);
		return p;
	}

	/*--------------------
	 * Other methods
	 --------------------*/
	IntField mF_Density = new IntField("Density (%):", 3, 0);
	IntField mF_PopSize = new IntField("Population size:", 3, DEFPOPSIZE);
	IntField mF_SquareLen = new IntField("Square Len:", 3, DEFSQUARELEN);

	private void MargolusInit() {
		int size = GetLsize();

		for (int cell = 0; cell < size; cell++) {
			int state= RandomEventInt(mF_Density.GetValue())?
					MargolusModel.PARTICLE:MargolusModel.EMPTY;
			InitState(cell, state);
		}

		for (int i = 0; i < mF_PopSize.GetValue(); i++) {
			int pos = RandomPos();
			InitState(pos, MargolusModel.PARTICLE);
		}

		int L=mF_SquareLen.GetValue();
		int X= super.GetXsize()/2 - L/2, 
				Y= super.GetYsize()/2 -  L/2 ; 
		for (int dx=0; dx < L ; dx++){
			for (int dy=0; dy < L ; dy++){
				super.InitStateXY(X+dx, Y+dy,MargolusModel.PARTICLE);
			}
		}
		/*
		for (int dx=0; dx < L ; dx++){
			SetAutomatonStateXY(X+dx, Y- 3,);
		}
		 */
	}



	/*private void StaticInit(){
		int X= GetXsize(), Y= GetYsize();
		for (int dx=0; dx < X ; dx++){
			for (int dy=0; dy < Y ; dy++){
				if ((dx%2 == 0)&&(dy%2 == 0)){
					SetAutomatonStateXY(dx, dy,MargolusModel.PARTICLE);
				} else {
					SetAutomatonStateXY(dx, dy,MargolusModel.EMPTY);
				}
			}
		}

	}*/

}// end class
