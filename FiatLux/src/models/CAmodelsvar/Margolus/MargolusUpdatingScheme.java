package models.CAmodelsvar.Margolus;

import components.allCells.Tessella;
import components.arrays.RegularDynArray;
import grafix.gfxTypes.elements.FLPanel;
import topology.basics.SuperTopology;
import topology.zoo.MargolusTM;
import updatingScheme.UpdatingScheme;
public class MargolusUpdatingScheme extends UpdatingScheme {

	final static String NAME="Margolus";
	/*--------------------
	 * Attributes
	 --------------------*/
	boolean    m_ParityCounter, DEFAULT = true;
	Tessella[] m_TessellaEven, m_TessellaOdd, m_CurrentTessella;
	MargolusModel m_Model; // each updating scheme is linked to a MargolusModel !
	
	/*--------------------
	 * Construction
	 --------------------*/
	
	/* the partition of Tessella is obtained with the Topology */
	@SuppressWarnings("unused")
	public MargolusUpdatingScheme(MargolusModel in_model){
		super();
		m_Model= in_model;		
	}
	
	 
	@Override
	public void LinkTo(RegularDynArray in_automaton, SuperTopology in_topo) {
		MargolusTM margolusTopo= (MargolusTM) in_topo;
		
		margolusTopo.setEvenOddTessela(in_automaton, m_Model, this);// for call back
		String msg= String.format("size even / odd  %d %d ", m_TessellaEven.length, m_TessellaOdd.length);
		//Macro.Debug(msg);
		
		m_ParityCounter   = DEFAULT;
		m_CurrentTessella = m_TessellaEven;
		
	}
	
		
	public void SetTessellaEvenOdd(Tessella[] in_Even, Tessella[] in_Odd) {
		m_TessellaEven    = in_Even;
		m_TessellaOdd     = in_Odd;	
	}
	

	/*--------------------
	 * Signals
	 --------------------*/
	
	/* calculates the new states with synchronous dynamics */
	public void sig_NextStep() {
		// *** synchronous updating ****
		for (int tess = 0; tess < GetSize(); tess++) {
			GetTessella(tess).sig_UpdateBuffer();
		}
		for (int tess = 0; tess < GetSize(); tess++) {
			GetTessella(tess).sig_MakeTransition();
		}
		
		if (m_ParityCounter){
			m_CurrentTessella = m_TessellaOdd;			
		} else {
			m_CurrentTessella = m_TessellaEven;						
		}
		m_ParityCounter   = !m_ParityCounter;
	}


	public void sig_Init() {
		m_ParityCounter= DEFAULT;
		m_CurrentTessella = m_TessellaEven;
		for (int tess=0; tess < GetSize(); tess++){
			GetTessella(tess).sig_Reset();
		}
	}


	public void sig_SynchroniseBufferStateWithMemory() {
		for (int tess=0; tess < GetSize(); tess++){
			Tessella t= GetTessella(tess);
			t.sig_UpdateBuffer();
		}		
	}


	public FLPanel GetSpecificPanel() {
		return null;
	}
	
	/*--------------------
	 * Get/Set
	 --------------------*/
	
	final protected Tessella GetTessella(int cell) {
		return m_CurrentTessella[cell];
	}
	
	protected int GetSize(){
		return m_CurrentTessella.length;
	}
	
	@Override
	public String GetName() {
		return NAME;
	}


	@Override
	public void sig_Update() {
		// TODO Auto-generated method stub
		
	}
}
