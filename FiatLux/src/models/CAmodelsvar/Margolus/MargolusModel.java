package models.CAmodelsvar.Margolus;

import components.allCells.SuperCell;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import initializers.SuperInitializer;
import models.CAmodels.CellularModel;
import topology.basics.TopologyCoder;
import updatingScheme.UpdatingScheme;

/*--------------------
 for the study of pattern creation 
*--------------------*/
abstract public class MargolusModel extends CellularModel {

	// the updating acts on tessela, not on cells !
	abstract void UpdateBuffer(MargolusTessella inout_Tessella);

	public final static int PARTICLE= 1, EMPTY= 0;
	
	/*--------------------
	 * overrides & implementations
	 --------------------*/

	@Override
	public UpdatingScheme GetDefaultUpdatingScheme(){
		return new MargolusUpdatingScheme(this);
	}
	
	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_Margolus;
	}
	
	public SuperInitializer GetDefaultInitializer() {
		return new MargolusInitializer();
	}

	public SuperCell GetNewCell() {
		return new MargolusCell();
	}

		
	/*--------------------
	 * Attributes
	 --------------------*/
	static private FLColor[] m_Color;
	
	/*--------------------
	 * Constructor
	 --------------------*/
	public MargolusModel() {
		
		m_Color = new FLColor[3];
		m_Color[0] = FLColor.c_black;
		m_Color[1] = FLColor.c_purple;
		m_Color[2] = FLColor.c_orange;
	}

	/*--------------------
	 * Get & Set methods
	 --------------------*/
	/* painting procedure */
	final public PaintToolKit GetPalette() {
		return new PaintToolKit(m_Color);
	}
	
	
}
