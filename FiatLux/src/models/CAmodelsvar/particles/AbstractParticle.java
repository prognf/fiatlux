package models.CAmodelsvar.particles;

public abstract class AbstractParticle {

	protected int m_id;
	
	public AbstractParticle(int id) {
		m_id = id;
	}
	
	public int getID() {
		return m_id;
	}

}
