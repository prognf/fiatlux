package models.CAmodelsvar.zzzLGCA;

import components.allCells.AlphaUpdatable;

public interface zzzLGcell extends AlphaUpdatable {

	void sig_Collide(boolean testConservation);
	
	void sig_Propagate();
	
	int getNChannels();
	
}
