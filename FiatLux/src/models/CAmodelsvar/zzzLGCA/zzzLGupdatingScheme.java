package models.CAmodelsvar.zzzLGCA;

import java.util.ArrayList;

import components.arrays.RegularDynArray;
import grafix.gfxTypes.elements.FLPanel;
import topology.basics.SuperTopology;
import updatingScheme.UpdatingScheme;

public class zzzLGupdatingScheme extends UpdatingScheme {
	
	private ArrayList<zzzLGcell> m_UpdatableCell;
	
	/*--------------------------------------------------------------------
	 * CONSTRUCTOR
	 * ------------------------------------------------------------------*/

	public zzzLGupdatingScheme() {
		super();
	}
	
	@Override
	public void LinkTo(RegularDynArray in_automaton, SuperTopology in_topo) {
		super.SetSize(in_automaton);
		m_UpdatableCell = in_automaton.GetCastedArray();
	}
	
	/*-----------------------------------------------------------------------
	 * SIGNALS
	 * --------------------------------------------------------------------*/

	public void MakeTransitions() {
		for (zzzLGcell cell : m_UpdatableCell) {
			cell.sig_MakeTransition();
		}
	}
	
	@Override
	public void sig_Init() {
		for (zzzLGcell cell : m_UpdatableCell) {
			cell.sig_MakeTransition();
		}
	}

	public void sig_NextStep() {
		for (zzzLGcell cell : m_UpdatableCell) {
			 cell.sig_Collide(false);
		}
		MakeTransitions();
		
		for (zzzLGcell cell : m_UpdatableCell) {
			 cell.sig_Propagate();
		}
		MakeTransitions();
	}
	
	/*-----------------------------------------------------------------------
	 * UI
	 * --------------------------------------------------------------------*/
	
	@Override
	public FLPanel GetSpecificPanel() {
		return null;
	}

	static final String NAME = "LGP-UpdatingScheme";
	@Override
	public String GetName() {
		return NAME;
	}

	@Override
	public void sig_Update() {
		
	}

}
