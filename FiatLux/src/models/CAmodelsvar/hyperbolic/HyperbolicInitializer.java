package models.CAmodelsvar.hyperbolic;

import components.arrays.SuperSystem;
import grafix.gfxTypes.IntField;
import grafix.gfxTypes.elements.FLPanel;
import initializers.CAinit.OneRegisterInitializer;
import topology.basics.SuperTopology;

public class HyperbolicInitializer extends OneRegisterInitializer {
	private static final int DEFTYPE=6;

	IntField 
	m_InitStyle = new IntField("Init Type:", 3, DEFTYPE), 
	m_PosOne= new IntField("PosOne", 3, 4);

	private HyperbolicTopology m_topo;

	@Override
	public FLPanel GetSpecificPanel() {
		FLPanel p = FLPanel.NewPanelVertical(m_InitStyle, m_PosOne);
		p.setOpaque(false);
		return p;
	}

	@Override
	protected void SubInit() {
		int pos1= m_PosOne.GetValue();
		if (m_InitStyle.GetValue() == 6){
			
			InitState(pos1, 2);
			int pos2= m_topo.GetPosOfRightToDaughter(pos1);			
			InitState(pos2, 1);
			int pos3= m_topo.GetPosOfKthDaughter(pos1, 2);
			InitState(pos3, 3);
		} else {
			for (int pos=0 ; pos < GetLsize(); pos++){
				int state;

				switch(m_InitStyle.GetValue()){
				case 1:
					state= m_topo.GetSectorNumber(pos);
					break;
				case 2:
					state= m_topo.GetLevel(pos);
					break;
				case 3:
					state= m_topo.GetColor(pos);
					break;
				case 4:
					state= (pos == pos1) ? 1 :0;
					break;
				case 5:
					state= (pos == pos1) ? 2 :0;
					break;
				default:
					state = pos % 20;
				}

				InitState(pos, state);
			}
		}
	}

	/** overriding **/
	@Override
	public void LinkTo(SuperSystem system, SuperTopology topo) {
		super.LinkTo(system, topo);
		m_topo = (HyperbolicTopology) topo;
	}

}
