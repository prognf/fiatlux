package models.CAmodelsvar.hyperbolic;

import java.util.ArrayList;

import components.types.IntegerList;
import grafix.gfxTypes.XYpoint;
import main.Macro;
import main.MathMacro;
import models.CAmodelsvar.hyperbolic.Heptagon.Color;
import topology.basics.SuperTopology;

public class oldHyperbolicTopology extends SuperTopology {


	public final static String NAME= "Hyperbolic Topology";

	public final static int P=7;
	final static double alpha = Math.PI / P;

	static final int POINTA= 0;

	static final int POINTB =6;

	// GFX attributes
	private XYpoint m_Xpos, m_Ypos;
	Heptagon m_centralVertex = new Heptagon(/* PIPO */Color.Black); // central cell
	ArrayList<Heptagon> m_hlist = new ArrayList<Heptagon>();

	//TODO
	public int GetSize() {
		return 1+7;
	}


	public oldHyperbolicTopology(){
	
		MakeCentralCell();		
		AnalyseOmega();
		//AnalyseSymmetric();
		MakeLevel1Cells();
		MakeLevel2Cells();
		MakeLevel3Cells();
		PrintCell(m_centralVertex);
		PrintLeVel1();
	}

	private void MakeCentralCell() {
		//double omega= 1.0 / 
		double val= sqrt( 2. * cos( 2. * alpha) - 1. );
		double radius=  ( cos( alpha) - sqrt(3) * sin(alpha) ) /  val;
		Macro.print("radius:" + radius);
		double expecOmega= (radius * radius + 1.) / (2 * radius* cos(alpha));
		Macro.print("Expect omega"+ expecOmega);
		for (int i=0; i < P ;i++){
			XYpoint point=  MathMacro.PointModuleAngle(radius, i, P);
			m_centralVertex.SetV(i,point);
		}	
		//m_hlist.add(m_centralVertex);
	}
	
	private void MakeLevel1Cells() {
		for (int i=0; i< P; i++){
			XYpoint pA= m_centralVertex.GetV(i);
			XYpoint pB= m_centralVertex.GetV( (i+1) % P);
			Heptagon hepta= MakeSymmetricHeptagon(m_centralVertex, pA, pB, i+2);
			m_hlist.add( hepta );
		}
		
	}
	
	private void MakeLevel2Cells() {
		for (int i=0; i<P; i++){
			Heptagon Hepta= m_hlist.get(i);
			AddThreeOffspring(Hepta);				
		}
	}
	
	int NTOADD=3*P+1;
	private void MakeLevel3Cells() {
	
		int startindex= m_hlist.size() - 1;
		for (int k=0; k<NTOADD; k++){
			Heptagon Hepta= m_hlist.get(startindex - k);
			AddThreeOffspring(Hepta);				
		}
	}
	

	/** three offspring from one cell **/
	private void AddThreeOffspring(Heptagon cell) {
		for (int i=0; i<3; i++){
			int vertexA= i+2, vertexB= (vertexA+1) % P;
			XYpoint pA= cell.GetV(vertexA);
			XYpoint pB= cell.GetV(vertexB);
			Heptagon  hept = MakeSymmetricHeptagon(cell, pA, pB, 4+i);
			m_hlist.add( hept );
		}	
	}


	private void PrintLeVel1() {
		for (Heptagon cell: m_hlist){
			PrintCell(cell);
		}		
	}
	
	/** prints points after symmetry operation **/
	private void AnalyseSymmetric() {
		XYpoint A= m_centralVertex.GetV(POINTA);
		XYpoint OMEGA= CalculateCircle(m_centralVertex.GetV(POINTA), m_centralVertex.GetV(POINTB));
		XYpoint sym = GetSymmetric(A, OMEGA);
		XYpoint vOMEGAA = A.Vector(OMEGA);
		XYpoint vOMEGAsym= sym.Vector(OMEGA);
		Macro.print("O" + OMEGA+ " A" + A + " Sym:" + sym +  " v1 : " + vOMEGAA + " v2 : " + vOMEGAsym);
		Macro.print(" R:" + GetRadiusOfLine(vOMEGAA) + " Exp:" + vOMEGAA.Norm());
	}

	private void AnalyseOmega() {
		double omega= 1. / sqrt(2.* cos( 2. * alpha) - 1);

		Macro.print(" Omega :" + omega);
	}

	private void PrintCell(Heptagon centralVertex){
		for (int i=0; i < P ;i++){
			Macro.print( "i :" + i + " -> "+ centralVertex.GetV(i));
		}	
		Macro.CR();
	}

	

	public XYpoint GetPointA(int posA, int posB) {
		Heptagon hepta= m_hlist.get(0);
		XYpoint 
			pA= hepta.GetV(posA), 
			pB= hepta.GetV(posB);
		return pA;
	}

	
	public XYpoint GetCircleAB(int posA, int posB) {
		Heptagon hepta= m_hlist.get(0);
		XYpoint 
			pA= hepta.GetV(posA), 
			pB= hepta.GetV(posB);
		return CalculateCircle(pA, pB);
	}


	/* pA, pB : point indices in central cell */
	private Heptagon MakeSymmetricHeptagon(Heptagon origin, XYpoint pA, XYpoint pB, int order){
		Heptagon vertex = new Heptagon(/* PIPO */Color.Black);
		XYpoint line= CalculateCircle(pA, pB);
		for (int i=0; i< P; i++){
			int newi= (P - 1 - i + order + P) % P; // keep the same direction of rotation
			XYpoint point= GetSymmetric(origin.GetV(newi),line);
			int symI= i ; // keep the same direction of rotation
			vertex.SetV(i,point); 
		}
		return vertex;
	}

	private XYpoint GetSymmetric(XYpoint point, XYpoint line) {
		double x= point.m_x, y = point.m_y, a = line.m_x, b= line.m_y;
		double dividor= (x - a)*(x - a) + (y - b)*(y - b);
		double factor= (a * a + b * b - 1.)/ dividor;
		double x2= a + factor * (x - a); 
		double y2= b + factor * (y - b); 
		return new XYpoint(x2, y2);
	}

	/* radius associated to aline */
	private double GetRadiusOfLine(XYpoint p){

		double r= SQ( p.m_x )  + SQ( p.m_y) - 1.;
		//Macro.Debug( "in : " + p  + "out " + r);
		return r;
	}


	/* a circle that passes through pA and pB */
	private XYpoint CalculateCircle(XYpoint pA, XYpoint pB) {
		double xa = pA.m_x, ya= pA.m_y, xb= pB.m_x, yb= pB.m_y;
		double divide= 2. * ( xa * yb - ya * xb);
		double num_a= yb * ( xa * xa + ya * ya + 1.) - ya * ( xb * xb + yb * yb + 1.);
		double num_b= xb * ( xa * xa + ya * ya + 1.) - xa * ( xb * xb + yb * yb + 1.);
		XYpoint ab = new XYpoint(num_a / divide, - num_b / divide);
		return ab;
	}

	@Override
	public int GetDimension() {
		return 3;
	}

	@Override
	public String GetName() {
		return NAME;
	}

	@Override
	protected IntegerList GetNeighbourhoodTable(int cell) {
		IntegerList neighb = new IntegerList();
		return neighb;
	}

	@Override
	protected boolean IsOnBorder(int index) {
		// TODO Auto-generated method stub
		return false;
	}

	private double cos(double theta) {
		return Math.cos(theta);
	}

	private double sin(double theta) {
		return Math.sin(theta);
	}

	private double sqrt(double theta) {
		return Math.sqrt(theta);
	}

	private double SQ(double y) {
		return y*y;
	}

	/*public static void main(String [] arg){
		HyperbolicTopology topo = new HyperbolicTopology();
	}*/


	

	

	
}
