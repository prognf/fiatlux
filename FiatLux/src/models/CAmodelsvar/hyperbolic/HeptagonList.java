package models.CAmodelsvar.hyperbolic;

import java.util.ArrayList;

public class HeptagonList extends ArrayList<Heptagon> {

	public void Add(Heptagon hept0, Heptagon hept1, Heptagon hept2) {
		add(hept0); 
		add(hept1);
		add(hept2);		
	}

	public void Add(Heptagon hept0, Heptagon hept1) {
		add(hept0); 
		add(hept1);
	}

}
