package models.CAmodelsvar.hyperbolic;

import grafix.gfxTypes.XYpoint;

class Heptagon {

	/** 
	 * attributes 
	 ***/

	public enum Color {White, Black}

    final public Color m_color ;

	private int P= HyperbolicTopology.P;

	public int m_order; // "order" in the sequence of pentagons 

	XYpoint [] m_vertex = new XYpoint[P];

	// temporary information attributes, used only during construction
	private int m_sector ; // Sector number in [0,7], 7 is central cell
	private int m_level ; // Sector number in [0,7], 7 is central cell
	public HeptagonList m_daughters; //TODO make this private
	private Heptagon m_parent, m_left, m_right;


	public int m_position; // position in global list (not optimal structure ?)	
	private HeptagonList m_neighbourhood ; 

	/**
	 * construction
	 */

	public Heptagon(Color in_color) {
		m_color= in_color;
		m_sector = P;
		m_neighbourhood = new HeptagonList();
		m_daughters= new HeptagonList();
	}

	/**
	 * methods
	 */

	public XYpoint GetV(int i) {
		return m_vertex[i];
	}

	public void SetV(int i, XYpoint point) {
		m_vertex[i]= point;			
	}

	public boolean IsBlack() {
		return m_color == Color.Black;
	}

	public int GetSector() {
		return m_sector;
	}

	public void SetSector(int sector) {
		m_sector = sector;
	}

	public int GetLevel() {
		return m_level;
	}

	public void SetLevel(int level) {
		m_level = level;
	}

	public void SetParent(Heptagon parent){
		m_parent= parent;
		m_neighbourhood.add(parent);
	}

	public Heptagon GetParent() {
		return m_parent;
	}

	public void SetDaughters(Heptagon hept0, Heptagon hept1) {
		m_daughters.Add(hept0, hept1);
		m_neighbourhood.Add(hept0, hept1);	
	}

	public void SetDaughters(Heptagon hept0, Heptagon hept1, Heptagon hept2) {
		m_daughters.Add(hept0, hept1, hept2);
		m_neighbourhood.Add(hept0, hept1, hept2);
	}

	public void AddNeighb(Heptagon Neighb) {
		m_neighbourhood.add(Neighb);
	}

	public HeptagonList GetNeighb() {
		return m_neighbourhood;
	}

	public void SetRightNeighb(Heptagon rightNeighb) {
		m_right= rightNeighb;
		AddNeighb(rightNeighb);
	}

	public Heptagon getRight() {
		return m_right;
	}

	public void SetLeftNeighb(Heptagon leftNeighb) {
		m_left= leftNeighb;		
		AddNeighb(leftNeighb);
	}

	public Heptagon getLeft() {
		return m_left;
	}

	public Heptagon GetRightToDaughters() {
		if (m_daughters.size()==0){
			return null;
		} else {
			int lst = m_daughters.size() - 1;
			return m_daughters.get(lst).getRight();
		}
	}




}
