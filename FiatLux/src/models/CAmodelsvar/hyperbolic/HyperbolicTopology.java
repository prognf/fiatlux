package models.CAmodelsvar.hyperbolic;

import components.types.IntegerList;
import grafix.gfxTypes.XYpoint;
import main.Macro;
import main.MathMacro;
import models.CAmodelsvar.hyperbolic.Heptagon.Color;
import topology.basics.SuperTopology;

public class HyperbolicTopology extends SuperTopology {


	public final static String NAME= "Hyperbolic Topology";

	public final static int P=7;
	final static double alpha = Math.PI / P;

	static final int POINTA= 0;

	static final int POINTB =6;

	private static final int LEVMAX = 5;

	public static final int SPECIALDIMHYPERBOLIC = 999;

	Heptagon m_centralVertex; // central cell
	HeptagonList m_hlist = new HeptagonList();
	CellPosMap m_cellPosMap = new CellPosMap();



	class CellPosMap {
		HeptagonList [] [] m_CoordList= new HeptagonList [P+1] [LEVMAX+1];

		public void Add(int sector, int level, HeptagonList listLevel) {
			m_CoordList[sector][level]= listLevel;
			for (Heptagon cell : listLevel){
				cell.SetLevel(level);
				cell.SetSector(sector);
			}
		}


		public HeptagonList GetListSectorLevel(int sector, int level){
			return m_CoordList[sector][level];
		}


		public Heptagon GetSectorLevelListPos(int sector, int level, int ListPos) {
			Heptagon cell= GetListSectorLevel(sector,level).get(ListPos);
			return cell;
		}


		public Heptagon GetLeftOfLevelListPos(int sector, int level) {
			int newSector = (P + sector - 1) % P; 
			HeptagonList lst=GetListSectorLevel(newSector, level); 
			return lst.get( lst.size() - 1);
		}


		public Heptagon GetRightOfLevelListPos(int sector, int level) {
			int newSector = (sector + 1) % P; 
			HeptagonList lst=GetListSectorLevel(newSector, level); 
			return lst.get(0);
		}


		public Heptagon GetSectorRoot(int sector) {
			return m_CoordList [sector][1].get(0);
		}



	}

	// GFX attributes
	private XYpoint m_Xpos, m_Ypos;


	/*
	 * construction
	 */


	public HyperbolicTopology(){

		MakeCentralCell();		
		//AnalyseOmega();
		//AnalyseSymmetric();	

		for (int sector=0; sector < P; sector++){
			MakeOneSector(sector);	
		}
		PutAllCellsInList();

	}


	/* for debug only */
	int GetPosOfRightToDaughter(int pos){
		Heptagon cell= GetCellPos(pos);
		return pos= cell.GetRightToDaughters().m_position;
	}

	/* for debug only */
	int GetPosOfKthDaughter(int pos, int k){
		Heptagon cell= GetCellPos(pos);
		return pos= cell.m_daughters.get(k).m_position;
	}

	/*
	 * topology building
	 */

	/** maps all the structure into a 1D global list **/
	private void PutAllCellsInList() {
		PutInGlobalList(m_centralVertex);

		// putting all cells in list
		for (int sector=0; sector<P; sector++){
			for (int level=1; level<=LEVMAX; level++){
				HeptagonList listLevel= m_cellPosMap.GetListSectorLevel(sector,level);
				for (Heptagon cell : listLevel){
					PutInGlobalList(cell);
				}
			}
		}
		// building the neighbourhood
		for (int sector=0; sector<P; sector++){
			Heptagon root= m_cellPosMap.GetSectorRoot(sector);
			m_centralVertex.AddNeighb(root);
			for (int level=1; level<=LEVMAX; level++){
				HeptagonList listLevel= m_cellPosMap.GetListSectorLevel(sector,level);
				int szList= listLevel.size();
				for (int posInLevel=0; posInLevel < szList; posInLevel++){
					Heptagon RightNeighb, LeftNeighb;
					if (posInLevel==szList-1){
						RightNeighb= m_cellPosMap.GetRightOfLevelListPos(sector,level);
					} else {
						RightNeighb= m_cellPosMap.GetSectorLevelListPos(sector,level, posInLevel + 1);
					}
					if (posInLevel==0){
						LeftNeighb= m_cellPosMap.GetLeftOfLevelListPos(sector,level);
					} else {
						LeftNeighb= m_cellPosMap.GetSectorLevelListPos(sector,level, posInLevel - 1);
					}
					Heptagon targetCell = listLevel.get(posInLevel);
					targetCell.SetRightNeighb(RightNeighb);
					targetCell.SetLeftNeighb(LeftNeighb);
				}
			}
		}

		// filling "gaps"
		for (int sector=0; sector<P; sector++){
			for (int level=1; level<=LEVMAX; level++){
				HeptagonList listLevel= m_cellPosMap.GetListSectorLevel(sector,level);
				for (Heptagon targetCell : listLevel){
					if (targetCell.IsBlack()){
						Heptagon parentNextTo= targetCell.GetParent();
						targetCell.AddNeighb(parentNextTo.getLeft());
					} 
					Heptagon rightToDaughters= targetCell.GetRightToDaughters();
					if (rightToDaughters != null ){ // pb with last level !!
						targetCell.AddNeighb(rightToDaughters);
					}

				}
			}
		}
	}




	/** adds in global list and remembers its position in the global list **/
	private void PutInGlobalList(Heptagon cell) {
		m_hlist.add(cell);
		int pos= m_hlist.size() - 1;
		cell.m_position= pos;
	}


	/** constructs one sector */
	private void MakeOneSector(int sectorNum) {
		// "root" of the the sector
		HeptagonList listLevel= InitOneBranch(sectorNum);
		m_cellPosMap.Add(sectorNum,1,listLevel);

		// "branches"
		for (int level=2; level <= LEVMAX; level++){
			HeptagonList newLevel= MakeOneLevelCells(sectorNum, listLevel);
			m_cellPosMap.Add(sectorNum,level,newLevel);			
			listLevel= newLevel;
		}
	}

	private HeptagonList MakeOneLevelCells(int sectorNum, HeptagonList listToProcess) {

		HeptagonList list = new HeptagonList();
		for (Heptagon hepta : listToProcess){
			if (hepta.IsBlack()){
				TwoOffspring(list,hepta);
			} else {
				ThreeOffspring(list,hepta);
			}		
		}
		return list;		
	}

	/** central cell **/
	private void MakeCentralCell() {

		m_centralVertex = new Heptagon(Color.White);
		//double omega= 1.0 / 
		double val= sqrt( 2. * cos( 2. * alpha) - 1. );
		double radius=  ( cos( alpha) - sqrt(3) * sin(alpha) ) /  val;
		Macro.print("radius:" + radius);
		double expecOmega= (radius * radius + 1.) / (2 * radius* cos(alpha));
		Macro.print("Expect omega"+ expecOmega);
		for (int i=0; i < P ;i++){
			XYpoint point=  MathMacro.PointModuleAngle(radius, i, P);
			m_centralVertex.SetV(i,point);
		}	
	}


	/** returns one cell that is the root of a sector **/
	private HeptagonList InitOneBranch(int sector){
		HeptagonList list= new HeptagonList();
		XYpoint pA= m_centralVertex.GetV(sector);
		XYpoint pB= m_centralVertex.GetV( (sector+1) % P);
		Heptagon hepta= MakeSymmetricHeptagon(m_centralVertex, pA, pB, sector+2, Color.White);
		hepta.SetSector(sector);
		list.add( hepta );
		hepta.SetParent(m_centralVertex);
		return list;
	}




	/** three offspring from one cell 
	 * @param list **/
	private void ThreeOffspring(HeptagonList list, Heptagon cell) {
		Heptagon 	hept0= GetOffSpring(cell, 0, Color.Black),
		hept1= GetOffSpring(cell, 1, Color.White),
		hept2= GetOffSpring(cell, 2, Color.White);
		cell.SetDaughters(hept0, hept1, hept2);
		list.Add(hept0, hept1, hept2);
	}

	/** two offspring from one cell **/
	private void TwoOffspring(HeptagonList list, Heptagon cell) {
		Heptagon 	hept0= GetOffSpring(cell, 1, Color.Black), 
		hept1= GetOffSpring(cell, 2, Color.White);
		cell.SetDaughters(hept0, hept1);
		list.Add(hept0, hept1);	
	}


	/** generates an offspring with a given color **/
	private Heptagon GetOffSpring(Heptagon parentCell, int i, Color offspringColor) {
		int vertexA= i+2, vertexB= (vertexA+1) % P;
		XYpoint pA= parentCell.GetV(vertexA);
		XYpoint pB= parentCell.GetV(vertexB);
		Heptagon  offspringCell = MakeSymmetricHeptagon(parentCell, pA, pB, 4+i, offspringColor);
		offspringCell.SetSector( parentCell.GetSector() ); //same sector as parent
		offspringCell.SetParent(parentCell);
		return offspringCell;
	}


	/*
	 * printing PROCEDURES
	 */



	private void PrintAll() {
		for (Heptagon cell: m_hlist){
			PrintCell(cell);
		}		
	}


	private void PrintCell(Heptagon centralVertex){
		for (int i=0; i < P ;i++){
			Macro.print( "i :" + i + " -> "+ centralVertex.GetV(i));
		}	
		Macro.CR();
	}

	/*
	 * topology 
	 */

	/** number of cells **/
	public int GetSize() {
		return m_hlist.size();
	}


	private Heptagon GetCellPos(int pos) {
		return m_hlist.get(pos);
	}

	/** returns the sector of a given cell **/
	public int GetSectorNumber(int pos) {
		int sector= GetCellPos(pos).GetSector();
		return sector;
	}	

	/** returns the level of a given cell **/
	public int GetLevel(int pos) {
		int level= GetCellPos(pos).GetLevel();
		return level;
	}

	/** 0 = black , 1 = white **/
	public int GetColor(int pos) {
		return GetCellPos(pos).IsBlack()?1:0;
	}

	/*
	 * implementations
	 */



	@Override
	public int GetDimension() {
		return SPECIALDIMHYPERBOLIC;
	}

	@Override
	public String GetName() {
		return NAME;
	}

	@Override
	/** building the topology 
	 * from cells to indices **/
	protected IntegerList GetNeighbourhoodTable(int pos) {
		HeptagonList neighb= GetCellPos(pos).GetNeighb();
		IntegerList posList = new IntegerList();
		for (Heptagon cell: neighb){
			posList.addVal(cell.m_position);
		}
		return posList;
	}



	@Override
	protected boolean IsOnBorder(int index) {
		return false;
	}



	/*
	 * MATHEMATICS PROCEDURES
	 */

	/** prints points after symmetry operation **/
	private void AnalyseSymmetric() {
		XYpoint A= m_centralVertex.GetV(POINTA);
		XYpoint OMEGA= CalculateCircle(m_centralVertex.GetV(POINTA), m_centralVertex.GetV(POINTB));
		XYpoint sym = GetSymmetric(A, OMEGA);
		XYpoint vOMEGAA = A.Vector(OMEGA);
		XYpoint vOMEGAsym= sym.Vector(OMEGA);
		Macro.print("O" + OMEGA+ " A" + A + " Sym:" + sym +  " v1 : " + vOMEGAA + " v2 : " + vOMEGAsym);
		Macro.print(" R:" + GetRadiusOfLine(vOMEGAA) + " Exp:" + vOMEGAA.Norm());
	}

	private void AnalyseOmega() {
		double omega= 1. / sqrt(2.* cos( 2. * alpha) - 1);

		Macro.print(" Omega :" + omega);
	}

	public XYpoint GetPointA(int posA, int posB) {
		Heptagon hepta= m_hlist.get(0);
		XYpoint 
		pA= hepta.GetV(posA), 
		pB= hepta.GetV(posB);
		return pA;
	}


	public XYpoint GetCircleAB(int posA, int posB) {
		Heptagon hepta= m_hlist.get(0);
		XYpoint 
		pA= hepta.GetV(posA), 
		pB= hepta.GetV(posB);
		return CalculateCircle(pA, pB);
	}

	/* pA, pB : point indices in central cell */
	private Heptagon MakeSymmetricHeptagon(Heptagon origin, XYpoint pA, XYpoint pB, int order, Heptagon.Color color){
		Heptagon vertex = new Heptagon(color);
		XYpoint line= CalculateCircle(pA, pB);
		for (int i=0; i< P; i++){
			int newi= (P - 1 - i + order + P) % P; // keep the same direction of rotation
			XYpoint point= GetSymmetric(origin.GetV(newi),line);
			int symI= i ; // keep the same direction of rotation
			vertex.SetV(i,point); 
		}
		return vertex;
	}

	private XYpoint GetSymmetric(XYpoint point, XYpoint line) {
		double x= point.m_x, y = point.m_y, a = line.m_x, b= line.m_y;
		double dividor= (x - a)*(x - a) + (y - b)*(y - b);
		double factor= (a * a + b * b - 1.)/ dividor;
		double x2= a + factor * (x - a); 
		double y2= b + factor * (y - b); 
		return new XYpoint(x2, y2);
	}

	/* radius associated to aline */
	private double GetRadiusOfLine(XYpoint p){

		double r= SQ( p.m_x )  + SQ( p.m_y) - 1.;
		//Macro.Debug( "in : " + p  + "out " + r);
		return r;
	}


	/* a circle that passes through pA and pB */
	private XYpoint CalculateCircle(XYpoint pA, XYpoint pB) {
		double xa = pA.m_x, ya= pA.m_y, xb= pB.m_x, yb= pB.m_y;
		double divide= 2. * ( xa * yb - ya * xb);
		double num_a= yb * ( xa * xa + ya * ya + 1.) - ya * ( xb * xb + yb * yb + 1.);
		double num_b= xb * ( xa * xa + ya * ya + 1.) - xa * ( xb * xb + yb * yb + 1.);
		XYpoint ab = new XYpoint(num_a / divide, - num_b / divide);
		return ab;
	}	


	private double cos(double theta) {
		return Math.cos(theta);
	}

	private double sin(double theta) {
		return Math.sin(theta);
	}

	private double sqrt(double theta) {
		return Math.sqrt(theta);
	}

	private double SQ(double y) {
		return y*y;
	}


	public int GetNeighbSize(int pos) {
		return GetCellPos(pos).GetNeighb().size();
	}
}
