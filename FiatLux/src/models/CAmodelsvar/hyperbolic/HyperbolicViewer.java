package models.CAmodelsvar.hyperbolic;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.util.ArrayList;

import components.allCells.OneRegisterIntCell;
import components.arrays.GenericCellArray;
import components.arrays.RegularDynArray;
import components.types.IntC;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.XYpoint;
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.AutomatonViewer;
import main.Macro;
import main.StatsMacro;



public class HyperbolicViewer extends AutomatonViewer {

	final static int P=7;
	final static int DEFSIZE = 800;
	final static double SCALE= 300.0;
	final static int DX=450, DY=350;
	private static final double dtheta = 0.01;
	private static final boolean TRIANGLEDIR = false;	


	private HyperbolicTopology m_Htopo;

	private ArrayList<Polygon>  m_polygon = new ArrayList<Polygon>();
	private GenericCellArray<OneRegisterIntCell> m_Array;  

	public FLPanel GetSpecificPanel(){return null;}

	public HyperbolicViewer(HyperbolicTopology topology, RegularDynArray in_Automaton) {
		super();	
		UpdateWindowSize();
		m_Array= new GenericCellArray<OneRegisterIntCell>(in_Automaton);
		m_Htopo= topology;


		// adding cells
		for (Heptagon cell : m_Htopo.m_hlist){
			m_polygon.add( GetPolygon(cell) );
		}

		// palette
		int size = m_polygon.size();
		this.SetPalette(PaintToolKit.GetRandomPalette(size));

	}

	/* main component */
	public void paintComponent(Graphics g) {
		//Macro.Debug(" Hyper paint");
		//g.setColor(FLColor.c_white);
		//g.fillRect(0, 0, 200, 200);

		int Ndraw= m_Array.size();//
		Macro.print(7, "Polygons to draw : " + Ndraw);
		for (int pos=0; pos< Ndraw; pos++){
			//int state = GetCellColorNumXY(x, y); 
			int state= m_Array.GetCell(pos).GetState();
			FLColor col= GetColor(state);
			g.setColor(col);
			DrawHepta(g, m_polygon.get(pos));	
			g.setColor(FLColor.c_black);
			DrawHeptaBorders(g, pos);	

		}	

		g.setColor(FLColor.c_blue);
		DrawCircle(g, 0.,0.,1.);
		//g.setColor(FLColor.c_black);
		//DrawCircleAB(g);
		//DebugInfo(g);
	}





	private void DebugInfo(Graphics g) {
		/*	Heptagon target= m_Htopo.m_hlist.get(2);
		HeptagonList lst= target.GetNeighb();
		for (int k=0; k< lst.size(); k++){
			String info = k + ":" + lst.size(); 
			WriteInfo(g, lst.get(k).m_position, info);
		}
		int pos = target.GetRightToDaughters().m_position;
		WriteInfo(g, pos, "ToADD?");*/
		for (int pos=0; pos< m_Array.size(); pos++){

			HeptagonList lst= m_Htopo.m_hlist.get(pos).GetNeighb();
			String info = "" + lst.size(); 
			WriteInfo(g, pos, info);
		}
	}

	IntC GetXY(XYpoint vertex){
		return new IntC( GetX(vertex.m_x), GetY(vertex.m_y));
	}

	int GetX(double x){
		return (int) (SCALE * x) + DX;
	}

	int GetY(double y){
		return (int) (SCALE * y) + DY;
	}


	private Polygon GetPolygon(Heptagon vertex) {
		int [] Vx = new int [P], Vy = new int [P]; 		
		for (int i=0; i < P ;i++){
			IntC xy= GetXY(vertex.GetV(i));
			Vx[i] = xy.X(); 	Vy[i] = xy.Y();
		}
		return new Polygon(Vx,Vy,P);
	}

	/* three circles for symmetry */
	private void DrawCircleAB(Graphics g) {


		Graphics2D g2D= (Graphics2D) g;
		for (int i=0 ; i < 3; i++){
			int posA= i+2, posB= posA+1;
			XYpoint ab= m_Htopo.GetCircleAB(posA, posB);
			DrawCircle(g, ab);
			XYpoint pA= m_Htopo.GetPointA(posA,posB);
			XYpoint pB= m_Htopo.GetPointA(posB,posA);
			//DrawPoint(g, pA);
			//DrawPoint(g, pB);
		}

	}


	private void DrawPoint(Graphics g, XYpoint pA) {
		DrawCircle(g, pA.m_x, pA.m_y, 0.1);
		DrawCircle(g, pA.m_x, pA.m_y, 0.09);
	}

	private void DrawCircle(Graphics g, double x, double y, double r) {
		int R= (int)(SCALE * r);
		super.drawCircle(g, GetX(x), GetY(y), R);
	}

	private void DrawCircle(Graphics g, XYpoint ab) {

		//int xp = GetX(0), yp = GetY(0);
		//int R= (int)(SCALE * r);

		/*Ellipse2D circle= new Ellipse2D.Double(xp, yp, R, R);
		g2D.draw(circle);*/
		double r= ab.CircleRadius();
		DrawCircle(g, ab.m_x, ab.m_y, r);

	}

	private void DrawHepta(Graphics g, Polygon poly) {
		//g.drawPolygon(poly);
		g.fillPolygon(poly);
		//Macro.Debug(" poly : " + new IntegerList( m_Htopo.m_Vx ) + " | " + new IntegerList( m_Htopo.m_Vy) );
		/* white line for one edge */

	}

	private void DrawHeptaBorders(Graphics g, int pos) {
		Polygon poly=m_polygon.get(pos);
		g.drawPolygon(poly);

		//	MoreInfo(g,pos);
	}



	/** use only for debug !! **/
	private void MoreInfo(Graphics g, int pos) {
		Polygon poly=m_polygon.get(pos);

		if ( TRIANGLEDIR) { // "directions"
			int 
			x0 = poly.xpoints[0], y0= poly.ypoints[0],
			x1 = poly.xpoints[1], y1= poly.ypoints[1],
			x2 = poly.xpoints[2], y2= poly.ypoints[2];
			int xm = (int) StatsMacro.Mean( poly.xpoints ), 
			ym=  (int) StatsMacro.Mean( poly.ypoints );

			g.setColor(FLColor.c_white);
			g.drawLine(x0, y0, xm, ym);
			g.setColor(FLColor.c_yellow);
			g.drawLine(x1, y1, xm, ym);
		}
	}

	private void  WriteInfo(Graphics g, int pos, String info){
		Polygon poly=m_polygon.get(pos);
		int 
		x0 = poly.xpoints[0], y0= poly.ypoints[0],
		x1 = poly.xpoints[1], y1= poly.ypoints[1],
		x2 = poly.xpoints[2], y2= poly.ypoints[2];
		int xm = (int) StatsMacro.Mean( poly.xpoints ), 
		ym=  (int) StatsMacro.Mean( poly.ypoints );
		g.drawString(info, xm, ym);
	}

	@Override
	protected IntC GetXYWindowSize() {
		return new IntC(DEFSIZE, DEFSIZE);
	}





}
