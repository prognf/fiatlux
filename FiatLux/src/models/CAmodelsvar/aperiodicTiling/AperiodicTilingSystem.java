package models.CAmodelsvar.aperiodicTiling;

import java.util.ArrayList;

import grafix.gfxTypes.elements.FLFrame;
import grafix.gfxTypes.elements.FLPanel;
import main.Macro;

public class AperiodicTilingSystem {

	ArrayList<MetaTileAbstract> m_tileL;
	TopologyTiling m_topo;
	
	public static void main(String [] argv){
		DoTest();
	}

	public AperiodicTilingSystem() {
		//test3();
		m_tileL = new ArrayList<MetaTileAbstract>();
		m_topo= new TopologyTiling();
	}

	private void test3() {
		Macro.Debug("test3");
		//MetaTileCompound pointer= MetaTileCompound.getPointer();
		//includeTiles(pointer);
		//Hat h= new Hat();
		//includeTiles(h);
		MetaTileAbstract c= TileGenerator.fromtileCode("n 0 0 0 0");
		addTile(c);
	}

	final static String CLASSNAME= Thread.currentThread().getStackTrace()[1].getClassName();

	public static void DoTest(){
		Macro.BeginTest(CLASSNAME);
		AperiodicTilingSystem sys= new AperiodicTilingSystem();
		AperiodicTilingViewer av= new AperiodicTilingViewer(sys);
		FLPanel viewerControl = av.GetController();
		FLFrame frame= new FLFrame("test aperiodic tiling one-tile");

		viewerControl.setFocusable(false);
		NoriaKeyHandler keyHandler= new NoriaKeyHandler(sys, av);
		FLPanel p=FLPanel.NewPanelVertical(viewerControl, av);
		
		frame.getContentPane().add(p);
		frame.packAndShow();
		frame.setFocusable(true);
		frame.addKeyListener(keyHandler);
		Macro.EndTest();
	}

	


//public void removeFocusFromAllObjects(Container container) {
//    container.setFocusable(false);
//    for (Component child : container.getComponents()) {
//        if (child instanceof Container) {
//            removeFocusFromAllObjects((Container) child);
//        } else {
//            child.setFocusable(false);
//        }
//    }
//}


	public void addTile(MetaTileAbstract tile) {
		m_tileL.add(tile);
		addRecToTopology(tile);
	}

	private void addRecToTopology(MetaTileAbstract tile) {
		if (tile instanceof Hat) {
			m_topo.addHat((Hat)tile);
		} else {
			for (MetaTileAbstract tile2 : tile.m_compoundL) {
				addRecToTopology(tile2);
			}
		}
	
}

	public void printConfiguration() {
		String s="";
		for (MetaTileAbstract t : m_tileL) {
			s+= String.format("%s :: %s (%d) ; ",t.m_name,t.getCenterABC(),t.m_angle);
		}
		Macro.print(s);
	}


}
