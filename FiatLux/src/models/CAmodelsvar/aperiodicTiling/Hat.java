package models.CAmodelsvar.aperiodicTiling;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import components.types.FLString;
import grafix.gfxTypes.FLColor;

public class Hat extends MetaTileAbstract {

	// a hat is composed of 12 tesserae
	List<Tessera> m_desc= new ArrayList<Tessera>() ;
	
	
	final static String DESC8 = 
			"-1 0 0 4 ; -1 0 0 5 ; -1 0 0 0 ; -1 0 0 1 ;" +
					"0,1,0,3 ; 0,1,0,4 ; 0,0,-1,1 ; 0,0,-1,2" ;


	Hat(){
		super.m_name="hat";
		setCenterABC(0, 0, 0);
		String [] desc= DESC8.split(";");
		for (String tess : desc) {			
			m_desc.add( Tessera.newTess( tess ) );
		}
	}

	/* UVW coord **/
	Hat(int x, int y, int z, int a) {
		this();
		translateABC(x,y,z);
		rotate(a);
	}

	/** centered hat **/
	static public Hat newHat(String scoord){
		int [] coordABC=FLString.parse_k_numbers(4, scoord);
		return new Hat(coordABC[0], coordABC[1], coordABC[2], coordABC[3]);
	}




	/* special case for H, UVW coord **/
	public static Hat newHatReflected() {
		Hat newhat = new Hat();
		newhat.reflect();
		newhat.rotate(5);
		return newhat;
	}

	/* for GFX */
	public  List<Tessera> getTesserae() {
		return m_desc;
	}

	/*------------- OPERATIONS -----------------*/

	public void translateABC(int dx, int dy, int dz) {
		super.translateCenterABC(dx,dy,dz);
		for (Tessera tess : m_desc) {
			tess.translateABC(dx, dy, dz);
		}
	}

	/* rotation wrt to (x,y,z) */
	public void rotateLeftABC(int x, int y, int z) {
		m_angle=(m_angle+1)%6;
		for (Tessera tess : m_desc) {
			tess.rotationLeftABC(x,y,z);
		}			
	}

	@Override
	public void rotate(int x, int y, int z, int da) {
		if (da < 0) {
			da+=6;
		}
		for (int i=0; i<da; i++)
			rotateLeftABC(x,y,z);
	}

	public void reflect() {
		for (Tessera tess : m_desc) {
			tess.reflexionABC( getCenterABC() );
		}
	}

	/** six contact points for topology **/
	public List<ATpoint> getContactPoints() {
		Tessera tess1= getTesserae().get(0);
		ATpoint p0= tess1.m_coordABC;
		ATpoint p1= pointAndDirectionalTranslation(p0,tess1.m_a);
		Tessera tess2= getTesserae().get(1);
		ATpoint p2= pointAndDirectionalTranslation(p0,tess2.m_a);
		Tessera tess3= getTesserae().get(3);
		ATpoint p3= pointAndDirectionalTranslation(p0,tess3.m_a);
		Tessera tess4= getTesserae().get(5);
		ATpoint p4= tess4.m_coordABC;
		Tessera tess5= getTesserae().get(6);
		ATpoint p5= tess5.m_coordABC;
		ATpoint p6= pointAndDirectionalTranslation(p5,tess5.m_a);
		ArrayList<ATpoint> list = new ArrayList<ATpoint>();
		Collections.addAll(list, p0, p1, p2, p3, p4, p5, p6);
		return list;
	}

	public void drawContactPoints(List<ATpoint> l, AperiodicTilingViewer v) {
		fillCirc(l,v,0, FLColor.c_black);
		fillCirc(l,v,1, FLColor.c_green);
		fillCirc(l,v,2, FLColor.c_brown);
		fillCirc(l,v,3, FLColor.c_blue);
		fillCirc(l,v,4, FLColor.c_yellow);
		fillCirc(l,v,5, FLColor.c_orange);
		fillCirc(l,v,6, FLColor.c_darkblue);
	}

	private void fillCirc(List<ATpoint> l, AperiodicTilingViewer v, int index, FLColor col) {
		v.filledCircleABC(l.get(index), col);
	}

	/* start from p and move one triangular len in direction a */
	private ATpoint pointAndDirectionalTranslation(ATpoint p, int a) {
		int dx=0,dy=0,dz=0;
		switch(a) {
		case 0: dx=1; break;
		case 1: dy=1; break;
		case 2: dz=1; break;
		case 3: dx=-1; break;
		case 4: dy=-1; break;
		case 5: dz=-1; break;
		}
		ATpoint pn= new ATpoint(p); 
		pn.translate(dx, dy, dz);
		return pn;
	}

	public String toString(){
		return String.format("hat> c:%s", getCenterABC());
	}
	
	
	
}

