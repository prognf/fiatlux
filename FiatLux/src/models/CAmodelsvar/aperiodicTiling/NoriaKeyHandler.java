package models.CAmodelsvar.aperiodicTiling;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import main.Macro;

public class NoriaKeyHandler  
implements KeyListener  {

	private static final int LEFTKEY = 37,UPKEY=38,RIGHTKEY=39,DOWNKEY=40;


	final AperiodicTilingSystem m_system;
	final AperiodicTilingViewer m_viewer;

	NoriaKeyHandler(AperiodicTilingSystem system, AperiodicTilingViewer viewer){
		m_system= system;
		m_viewer= viewer;
		//addKeyListener(this);
	}

	final static String Z="0, 0, 0, 0";


	final static String KEYS="itpfhTPFHcvbnCVBNjklm";

	
	private MetaTileAbstract keyToTile(char c) {
		int pos= KEYS.indexOf(c);
		if (pos==0)
			return Hat.newHat(Z);
		pos--;
		int level=1+pos/4;
		char type= TileGenerator.TYPE[pos%4];
		MetaTileAbstract tile= TileGenerator.getTileInZ(level, type);
		if (tile==null) {
			Macro.SystemWarning("problem with key typed");
		}
		return tile;
	}


	/** Handle the key pressed event from the text field. */
	public void keyPressed(KeyEvent e) {
		//Macro.print(" ** keyPressed :: " + e.getKeyCode() + " chr:" + e.getKeyChar() );
		char c=e.getKeyChar();
		if (KEYS.contains(""+c)) {
			MetaTileAbstract t= keyToTile(c); 
			m_system.addTile(t);
		}
		// ZOOM
		if (e.getKeyCode()==KeyEvent.VK_PAGE_UP) {
			m_viewer.multRadius(1.1);
			m_viewer.repaint();
		}
		if (e.getKeyCode()==KeyEvent.VK_PAGE_DOWN) {
			m_viewer.multRadius(1./1.1);
			m_viewer.repaint();
		}

		// no operation if there is no tile
		if (m_system.m_tileL.isEmpty()){
			m_viewer.repaint();
			return;
		}
		int dx=0, dy=0, dz=0, da=0;
		if (e.getKeyCode()==LEFTKEY)  {
			dx=-1;
		}
		if (e.getKeyCode()==RIGHTKEY)  {
			dx=1;
		}
		if (e.getKeyCode()==DOWNKEY) {
			dy=-1;
		}
		if (e.getKeyCode()==UPKEY)  {
			dy=1;
		}
		if (c=='q') {
			dz=1;
		}
		if (c=='w') {
			dz=-1;
		}
		if (c=='a') {
			da=1;
		}
		if (c=='z') {
			da=-1;
		}
		//String st= String.format(" (%d,%d,%d) ", dx, dy, dz);
		int last=m_system.m_tileL.size()-1;
		MetaTileAbstract tile = m_system.m_tileL.get(last);
		// MetaTileAbstract tile2= tile.m_metaL.get(tile.m_metaL.size() - 1);
		tile.translateABC(dx, dy, dz);
		tile.rotate(da);
		if (e.getKeyCode()==KeyEvent.VK_BACK_SPACE) {
			m_system.m_tileL.clear();
		}
		if (e.getKeyCode()==127) {
			m_system.m_tileL.remove(last);
		}
		if (c=='i') {
			m_system.printConfiguration();
		}
		if (c=='s') {
			m_system.addTile(TileGenerator.getPointer());
		}
		if (c==' ') {
			m_system.m_topo.update();
		}
		m_viewer.repaint();
	}

	/** Handle the key released event */
	public void keyReleased(KeyEvent e) {
	}

	/** Handle the key typed event */
	public void keyTyped(KeyEvent e) {
	}

}
