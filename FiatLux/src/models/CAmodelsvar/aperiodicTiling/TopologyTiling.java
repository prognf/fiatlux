package models.CAmodelsvar.aperiodicTiling;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import components.types.IntC;
import main.Macro;

public class TopologyTiling {

	
	AperiodicTilingSystem m_system;
	List<HatCell> m_cellL;
	
	// association point -> hat for detecting proximity
	HashMap<IntC,ArrayList<HatCell>> m_hatsInPoint;
	
	public TopologyTiling() {
		m_hatsInPoint= new HashMap<IntC,ArrayList<HatCell>>();
		m_cellL= new ArrayList<HatCell>();
	}

	/** adds a new hat cell to the topology **/
	void addHat(Hat hat){
		List<ATpoint> listp= hat.getContactPoints();
		// new cell 
		HatCell cell= new HatCell(hat);
		m_cellL.add(cell);
		// adds all the points of the hat in the "topology" 
		for (ATpoint p : listp) {
			addPointHat(p, cell);
		}
	}
	
	/** adds point to the map and checks topology connections **/
	void addPointHat(ATpoint p, HatCell hatc) {
		
		// transform from a,b,c to a',b',0
		int a2=p.x-p.z,
			b2= p.y+p.z;
		IntC key= new IntC(a2, b2);
		if (!m_hatsInPoint.containsKey(key)) {// new point
			ArrayList<HatCell> l= new ArrayList<HatCell>();
			l.add(hatc);
			m_hatsInPoint.put(key, l);
			//Macro.fPrint("hat:%s new point:%s", hat, key);
		} else {
			ArrayList<HatCell> hatlist = m_hatsInPoint.get(key);
			for (HatCell hatConnex : hatlist) {
				Macro.fPrint("hatc:%s connex with added hatc:%s", hatConnex, hatc);
				hatc.addNeighb(hatConnex);
				hatConnex.addNeighb(hatc);
			}
			hatlist.add(hatc);
		}
		
	}

	/** one global time step **/
	void update() {
		for (HatCell cell : m_cellL) {
			cell.calculateNextState();
		}
		for (HatCell cell : m_cellL) {
			cell.updateToNextState();
		}
	}
	
}
