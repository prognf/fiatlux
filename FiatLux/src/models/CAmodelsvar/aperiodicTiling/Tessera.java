package models.CAmodelsvar.aperiodicTiling;

import java.awt.Polygon;

import components.types.FLString;

/** what is called kite in the original paper 
 * 3 coord of the "center" + orientation **/
class Tessera {
	
	ATpoint m_coordABC; // preparing the transition
	int m_a; //index
	
	/* constr. */
	public Tessera(int i, int j, int k, int a) {
		m_coordABC= new ATpoint(i, j, k);
		m_a=a;
	}

	/* constructor with string */
	public static Tessera newTess(String strpos) {
		int [] v = FLString.parse_k_numbers(4, strpos);
		return new Tessera(v[0], v[1], v[2], v[3]);
	}


	/* GFX */
	Polygon getPolygon(AperiodicTilingViewer viewer) {
		return viewer.getTesseraPolygon(m_coordABC, m_a);
	}
	
	
	public void translateABC(int dx, int dy, int dz) {
		m_coordABC.translate(dx, dy, dz);
	}
	
	
	/** rotation with respect to center (cx,cy,cz) **/
	void rotationLeftABC(int cx, int cy, int cz) {
		int i2=-(m_coordABC.z-cz);
		int j2=(m_coordABC.x-cx);
		int k2=(m_coordABC.y-cy);
		
		int index2=(m_a+1)%6;
		m_coordABC.x=cx+i2; 
		m_coordABC.y=cy+j2; 
		m_coordABC.z=cz+k2; m_a= index2;
	}
	
	
 	/** ABC rotation **/
	void reflexionABC(int cx, int cy, int cz) {
		int x2=-(m_coordABC.x-cx);
		int y2=m_coordABC.z-cz;
		int z2=m_coordABC.y-cy;
		int index2=(9-m_a)%6;
		m_coordABC.x=cx+x2; m_coordABC.y=cy+y2; m_coordABC.z=cz+z2; m_a= index2;
	}
	
	void reflexionABC(ATpoint center) {
		reflexionABC(center.x, center.y, center.z);
	}

	

}
