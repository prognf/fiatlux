package models.CAmodelsvar.aperiodicTiling;

import java.util.ArrayList;

import components.types.FLString;
import components.types.FLStringList;

public class Envelope extends ArrayList<ATpoint>{


	//// conversion UVW -> ABC
	//Macro.print("reading file: "+ relpath + "... ");
	//FLStringList read= FLStringList.OpenFileInsideSysFile(pathtoressource, relpath);
	
		/* relative path given for envelope file **/
		public void createEnvelopeFromFile(FLStringList data) {
			String ln;
			int linesBegin= data.findFirstPosContains("NENVLP=");
			int NENVLP= FLString.IParseInString(data.Get(linesBegin), "NENVLP=");
			for (int j=0; j< NENVLP; j++) {
				ln= data.get(linesBegin+1+j);
				int [] point=FLString.parse_k_numbers(3,ln);
				//Macro.fPrint("Nenv C:%d > %d %d %d ",NENVLP, point[0],point[1],point[2]);
				ATpoint p= new ATpoint(point[0],point[1],point[2]);
				if (ln.contains("+")) { //special mult
					p. mult(3);
				}
				this.add(p);
			}
		}

	
	public void translateUVW(int dx, int dy, int dz) {
		// from UVW to ABC
		int da= dx-dz, db=dx+dy, dc=dy+dz;
		translateABC(da, db, dc);
	}
	

	public void translateABC(int dx, int dy, int dz) {
		for (ATpoint p : this) {
			p.translate(dx, dy, dz);
		}
	}

	/* envelope do not neceassarily turn on their centers */
	public void rotateABC(int x, int y, int z, int da) {
		for (ATpoint p : this) {
			p.envelopePointRotateABC(x, y, z, da);
		}
	}
	
	public String getInfo() {
		ATpoint sum= new ATpoint(0, 0, 0);
		for (ATpoint p : this) {
			sum.translate(p.x, p.y, p.z);
		}
		return "grav:"+sum;
	}

}
