package models.CAmodelsvar.aperiodicTiling;

import java.awt.BasicStroke;
import java.awt.Component;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.util.ArrayList;
import java.util.List;

import components.randomNumbers.FLRandomGenerator;
import components.types.DoubleCouple;
import components.types.IntC;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.XYpoint;
import grafix.gfxTypes.controllers.FLCheckBox;
import grafix.gfxTypes.controllers.PolyChoicePanel;
import grafix.gfxTypes.controllers.Radio2Control;
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.AutomatonViewer;

public class AperiodicTilingViewer extends AutomatonViewer 
{

	final static int DEFSIZE = 1000;
	private static final double DEF_RADIUS = 15.;
	final static int DX=DEFSIZE/2, DY=DEFSIZE/2-50;//centering of the grid
	final static double SCALE= 300.0;
	private static final int HEX = 6;

	private static final int L = 18; // hex grid : width in numer of cells
	private static final int NCOLOR = 256;


	private static final int DEFVAL_COLORATIONLEVEL = 0; // for level of coloration

	final private AperiodicTilingSystem m_system;
	
	//GFX
	int m_hatCol; // for drawing
	private Graphics m_g; //replaces g
	private double m_Radius = DEF_RADIUS;
	// hex grid
	private ArrayList<Polygon>  m_hexL = new ArrayList<Polygon>();

	FLRandomGenerator m_rnd = FLRandomGenerator.GetNewRandomizer();
	
	// options
	boolean m_drawEnvelopes=true;
	//	boolean m_ONECOLPERHAT = true; //option :one color for each hat
	FLdrawInternalLines m_drawInternalLines= new FLdrawInternalLines();
	int nhat=0;
	private int m_levelColor=1;
	/* **********************************
	 * main function
	 * **********************************/

	public void paintComponent(Graphics g) {

		m_g= g;
		m_hatCol=0;
		drawHexGrid();
		// for each tile
		/*for (MetaTileAbstract tile : m_system.m_tileL) {
			drawTile(tile);
		}*/
		for (HatCell cell : m_system.m_topo.m_cellL) {
			drawHatCell(cell);
		}
		drawGridCenter();
		//drawCenters();
		if (m_drawEnvelopes)
			drawEnvelopes();
	}


	

	/*********** constr ************************/


	public FLPanel GetSpecificPanel(){return null;}

	public AperiodicTilingViewer(AperiodicTilingSystem system) {
		m_system = system;
		UpdateWindowSize();

		makeHexagonalGridCells();
		//test2();

		// palette RANDOMCOLORS
		m_rnd.setSeedFixedAndReset(1984);
		PaintToolKit pal= PaintToolKit.GetRandomPalette(m_rnd,NCOLOR);
		pal.SetColor(0, FLColor.c_blue);
		this.SetPalette(pal);

	}

	/*********** MAIN FUNCTIONS ************************/

	private void zzzdrawTile(MetaTileAbstract tile) {
		//Macro.Debug("drawing tiles ; N:"+tile.getHatList().size());
		if (tile.getLevel()==m_levelColor) {
			m_hatCol = (m_hatCol +1)% NCOLOR;	
		}
		if (!( tile instanceof Hat)) { // recursive draw
			for (MetaTileAbstract tile2 : tile.m_compoundL) {
				zzzdrawTile(tile2);
			}
		} else {// hat case			
			Hat hat=(Hat)tile;
			// drawing the 8 tesserae of the hat
			for (Tessera tess : hat.getTesserae()) {
				Polygon p = tess.getPolygon(this); //call-back
				m_g.setColor( GetColor(m_hatCol%NCOLOR) );
				m_g.fillPolygon(p);	
				// lines of the tessera
				if (m_drawInternalLines.isSelected()){
					m_g.setColor(FLColor.c_black);
					m_g.drawPolygon(p);
				}
			}
			List<ATpoint> contact= hat.getContactPoints();
			hat.drawContactPoints(contact,this);
		}
	}
	
	private void drawHatCell(HatCell cell) {
		Hat hat=cell.m_hat;
		FLColor hatCol= GetColor(cell.m_state%NCOLOR) ;
		// drawing the 8 tesserae of the hat
		for (Tessera tess : hat.getTesserae()) {
			Polygon p = tess.getPolygon(this); //call-back
			m_g.setColor( hatCol );
			m_g.fillPolygon(p);	
			// lines of the tessera
			if (m_drawInternalLines.isSelected()){
				m_g.setColor(FLColor.c_black);
				m_g.drawPolygon(p);
			}
		}
	}
	
	private void drawGridCenter() {
		ATpoint pcenter= new ATpoint(0,0,0);
		filledCircleABC(pcenter, FLColor.c_red);
	}

	private void zzzdrawCenters() {
		for (MetaTileAbstract tile : m_system.m_tileL) {
			this.filledCircleABC(tile.getCenterABC(),FLColor.c_cyan);
			for (MetaTileAbstract tile2 : tile.m_compoundL) {
				this.filledCircleABC(tile2.getCenterABC(),FLColor.c_brown);
			}
		}
	}
	FLColor [] ENVCOL = {FLColor.c_black, FLColor.c_black, FLColor.c_purple, FLColor.c_yellow, FLColor.c_blue}; 

	private void drawEnvelopes() {
		for (MetaTileAbstract tile : m_system.m_tileL) {
			for (MetaTileAbstract tile2 : tile.m_compoundL) {
				for (MetaTileAbstract tile3 : tile2.m_compoundL) {
					drawEnvelope(tile3, ENVCOL[ tile3.getLevel()], 2);
				}
				drawEnvelope(tile2, ENVCOL[ tile2.getLevel()], 3);
			}
			drawEnvelope(tile, ENVCOL[ tile.getLevel()], 4);
		}
	}

	private void drawEnvelope(MetaTileAbstract tile, FLColor col, int width) {
		m_g.setColor(col);
		Polygon poly = 
				getEnvelopePolygonABC(tile.m_envelope);
		setPenWidth(m_g,width);
		m_g.drawPolygon(poly);
		setPenWidth(m_g,1);
	}

	
	/*********** other FUNCTIONS ************************/

	private void setPenWidth(Graphics g, int width) {
		((Graphics2D)g).setStroke(new BasicStroke(width));
	}

	IntC GetXY(XYpoint vertex){
		return new IntC( GetX(vertex.m_x), GetY(vertex.m_y));
	}

	int GetX(double x){
		return (int) (SCALE * x) + DX;
	}

	int GetY(double y){
		return (int) (SCALE * y) + DY;
	}

	final static double K= Math.sqrt(3)/2.;

	/** coordinates from the (a,b,c) system triangular grid */
	DoubleCouple pointABCtoCoordinates(int i, int j, int k) {
		double x=m_Radius*(i+(j-k)*Math.cos(Math.PI/3));
		double y=m_Radius*(j+k)*Math.sin(Math.PI/3);
		return new DoubleCouple(x, y);
	}

	/** coordinates from the (a,b,c) system triangular grid -> XY */
	DoubleCouple pointABCtoCoordinates(ATpoint p) {
		return pointABCtoCoordinates(p.x, p.y, p.z);
	}

	double xcos(int i, int a) {
		return 2*i*K*m_Radius*Math.cos(a*Math.PI/3 + Math.PI/6);
	}

	double ysin(int i, int a) {
		return 2*i*K*m_Radius*Math.sin(a*Math.PI/3 + Math.PI/6);
	}

	/** red axes : UVW -> XY **/
	DoubleCouple pointUVWtoCoordinates(int x, int y, int z) {
		double xr= xcos(x,0), yr= ysin(x,0);
		xr += xcos(y,1);
		yr += ysin(y,1);
		xr += xcos(z,2);
		yr += ysin(z,2);
		return new DoubleCouple(xr, yr);
	}

	/** grey axes : UVW **/
	DoubleCouple pointHex(int x, int y, int z, int a) {
		DoubleCouple delta= pointUVWtoCoordinates(x, y, z);
		double xr= m_Radius*Math.cos(Math.PI/3*a);; 	
		double yr= m_Radius*Math.sin(Math.PI/3*a);
		return new DoubleCouple(delta.X()+ xr, delta.Y() + yr);
	}

	/** "internal" hexagon points :  UVW **/
	DoubleCouple pointInt(int x, int y, int z, int a) {
		DoubleCouple delta= pointUVWtoCoordinates(x, y, z);
		double xr= K*m_Radius*Math.cos(Math.PI/3*a + Math.PI/6);; 	
		double yr= K*m_Radius*Math.sin(Math.PI/3*a + Math.PI/6);
		return new DoubleCouple (delta.X()+ xr , delta.Y() + yr);
	}

	private Polygon getHexagon(int x, int y, int z) {
		DoubleCouple [] hex = new DoubleCouple[6];
		for (int i=0; i<HEX;i++) {
			hex[i]= pointHex(x, y, z, i);
		}
		int [] Vx = new int [HEX], Vy = new int [HEX]; 		
		for (int i=0; i < HEX ;i++){
			Vx[i] = DX + (int) (hex[i].X() - m_Radius); 	
			Vy[i] = DY - (int) hex[i].Y();
		}
		return new Polygon(Vx,Vy,HEX);
	}

	/* for the representation of the grid **/
	private void makeHexagonalGridCells() {
		m_hexL.clear();
		m_hexL.add(getHexagon(0, 0, 0));
		for (int i=-L; i<=L; i++){
			for (int j=-L; j<=L; j++){
				for (int k=-L; k<=L; k++){
					m_hexL.add(getHexagon(i, j, k));
				}	
			}	
		}
	}


	/** returns the tessera (kite) with coord (x,y,z) and angle a **/
	public Polygon getTesseraPolygon(ATpoint p, int a) {
		DoubleCouple delta= pointABCtoCoordinates(p); 
		DoubleCouple [] tesspointL = new DoubleCouple[4];
		tesspointL[0]= new DoubleCouple(0, 0);
		tesspointL[1]= pointInt(0,0,0,a); 
		tesspointL[2]= pointHex(0,0,0,a);
		int index2= (a + 5)%6; 
		tesspointL[3]= pointInt(0,0,0,index2);

		int [] Vx = new int [4], Vy = new int [4]; 		
		for (int i=0; i < 4 ;i++){

			Vx[i] = DX + (int) ( delta.X() + tesspointL[i].X() ); 	
			Vy[i] = DY - (int) ( delta.Y() + tesspointL[i].Y() );
		}
		return new Polygon(Vx,Vy,4);
	}

	/** ABC style **/
	public Polygon getEnvelopePolygonABC(Envelope env) {
		int N= env.size();
		int [] Vx = new int [N], Vy = new int [N]; 	
		for (int i=0; i< N; i++) {
			ATpoint elem = env.get(i);
			//Macro.Debug(" point ABC read:"+elem);
			DoubleCouple c= 
					pointABCtoCoordinates(elem.x, elem.y, elem.z);
			Vx[i] = DX + (int) c.X(); 	
			Vy[i] = DY - (int) c.Y();
			//Macro.fDebug(" Vx,Vy: %d,%d",Vx[i],Vy[i]);
		}
		return new Polygon(Vx,Vy,N);
	}

	@Override
	protected IntC GetXYWindowSize() {
		return new IntC(DEFSIZE, DEFSIZE);
	}

	/** multiplies grid radius by r (zoom/unzoom) **/
	public void multRadius(double r) {
		m_Radius*=r;
		makeHexagonalGridCells();
		repaint();
	}

	/**---------- D R A W I N G ----------------- */

	void drawHexGrid() {
		m_g.setColor(FLColor.c_grey1);
		for (Polygon hex : m_hexL) {
			m_g.drawPolygon(hex);
		}
	}

	private void filledCircleABC(int x, int y, int z, FLColor col) {
		m_g.setColor(col);
		filledCircleABC(x, y, z);
	}

	void filledCircleABC(ATpoint p, FLColor col) {
		filledCircleABC(p.x, p.y, p.z, col);
	}

	void filledCircleABC(int x, int y, int z) {
		filledCircleABC(x, y, z, 7.);
	}

	/* a circle with a radis that is a fraction of the gridhex ratio **/
	void filledCircleABC(int x, int y, int z, double divratio) {
		DoubleCouple xy= pointABCtoCoordinates(x,y,z);
		int xd= DX+ (int)xy.X();
		int yd= DY- (int)xy.Y(); 
		int r= (int)(m_Radius/divratio);
		super.fillCircle(m_g, xd, yd, r);
		m_g.setColor(FLColor.black);
		super.drawCircle(m_g, xd, yd, r);
	}


	public FLPanel GetController() {
		FLPanel p= new FLPanel();
		Radio2Control c1= new EnvelopeDraw();
		FlselectColorLevel c2= new FlselectColorLevel();
		this.disableFocusComponent(c2);
		p.Add(m_drawInternalLines, c1, c2);
		return p;
	}

	class EnvelopeDraw extends Radio2Control{

		public EnvelopeDraw() {
			super("envelopes:", "on", "off"); // not focusable
			super.setPartsNonFocusable();
		}

		protected void ActionSelection1() {
			m_drawEnvelopes= true; redraw();
		}

		protected void ActionSelection2() {
			m_drawEnvelopes= false; redraw();
		}
	}

	public void redraw() {
		repaint();
	}
	
	class FLdrawInternalLines extends FLCheckBox {

		FLdrawInternalLines(){
			super("lines",true);
			this.setFocusable(false);
		}
		
		@Override
		protected void SelectionOn() {
			AperiodicTilingViewer.this.repaint();
		}

		@Override
		protected void SelectionOff() {
			AperiodicTilingViewer.this.repaint();
		}
		
	}


	final static String [] TAB = {"0","1","2","3"};
	class FlselectColorLevel extends PolyChoicePanel {
		
		FlselectColorLevel(){
			super("coloration:", TAB, DEFVAL_COLORATIONLEVEL);
			this.setFocusable(false);
			this.getButton().setText("+");
		}

		@Override
		protected void Update() {
			m_levelColor= m_indexChoice;
			AperiodicTilingViewer.this.repaint();
		}

	}
	
	/** should recursively remove the focusable property from every component */
	static public void disableFocusComponent(Container c)
	{
	    Component[] cmps = c.getComponents();
	    for(Component cmp : cmps)
	    {
	        if(cmp.isFocusable()) // or even you could specify the JComponent you want to make it not-focusable, for example (cmp instanceof JButton)
	        {
	            cmp.setFocusable(false);	            
	        }
	        if (cmp instanceof Container) {
	        	disableFocusComponent((Container)cmp);
	        }
	    }
	}

}
