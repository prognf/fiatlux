package models.CAmodelsvar.aperiodicTiling;

import java.util.ArrayList;

abstract public class MetaTileAbstract {

	
	public abstract void 	translateABC(int dx, int dy, int dz);
	protected abstract void rotate(int x, int y, int z, int a);
	
	protected String m_name="ANONYMOUS"; // type
	ArrayList<MetaTileAbstract>  m_compoundL = new ArrayList<MetaTileAbstract>(); // compounds
	private ATpoint m_centerABC;	// position of the center
	int m_angle;            // keep track of the rotations
	Envelope m_envelope= new Envelope();  // envelope
	
	/** add tiles of level 1, level 2,... **/
	void addAll(MetaTileAbstract... level1coll) {
		for (MetaTileAbstract tilelevel1 : level1coll) {
			m_compoundL.add(tilelevel1);
		}
	}

	/** add envelope points **/
	void addEnvelope(ATpoint... pointL) {
		for (ATpoint p : pointL) {
			m_envelope.add(p);
		}
	}
	
	/** recursive function, used by the viewer **/
	public ArrayList<Hat> zzzgetHatList() {
		if (this instanceof Hat) { // Hat case
			ArrayList<Hat> hlist= new ArrayList<>();
			hlist.add((Hat)this);
			return hlist;
		}
		// general case 
		ArrayList<Hat> hlist= new ArrayList<>();
		for (MetaTileAbstract tile : m_compoundL) {
			hlist.addAll(tile.zzzgetHatList());
		}
		return hlist;
	}
	
	
	/*** -----------------  operators ------------- ***/
	
	protected void translateCenterABC(int dx, int dy, int dz) {
		m_centerABC.translate(dx, dy, dz);
	}
	
	final protected void rotate(int da) {
		ATpoint cnt= getCenterABC();
		rotate(cnt.x,cnt.y,cnt.z,da);
	}

	/*** -----------------  get / set ------------- ***/
	
	protected void setCenterABC(int x, int y, int z) {
		m_centerABC= new ATpoint(x, y, z);
	}
	
	protected ATpoint getCenterABC() {
		return m_centerABC;
	}

	/** hack for envelopes color **/
	public int getLevel() {
		if (m_name.startsWith("super"))
			return 1;
		if (m_name.startsWith("mega"))
			return 2;
		if (m_name.startsWith("giga"))
			return 3;
		if (m_name.startsWith("tera"))
			return 4;
		return 0;
	}
	
}
