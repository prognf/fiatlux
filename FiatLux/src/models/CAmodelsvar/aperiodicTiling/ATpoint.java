package models.CAmodelsvar.aperiodicTiling;

public class ATpoint {

	int x,y,z; // three coordinates
	
	/** point ABC coord **/
	public ATpoint(int xn, int yn, int zn) {
		x=xn; y=yn; z=zn; 
	}
	
	/** copy **/
	public ATpoint(ATpoint p) {
		x=p.x; y=p.y; z=p.z; 
	}
	/* translation */
	public void translate(int dx, int dy, int dz) {
		x+= dx; y+= dy; z+= dz;
		//Macro.fDebug("trnsl to: %d %d %d, delta: %d %d %d",x,y,z,dx,dy,dz);
	}
	
	/* translation (dx,dy,dz) in UVW coord. */
	public void translateABCfromUVWdelta(int dx, int dy, int dz) {
		x+= dx-dz; //var(a)=var(u)-var(w) 
		y+= dx+dy; //var(b)=...
		z+= dy+dz;
	}
	
	/* rotation of +1 (to the left) wrt (cx,cy,cz) **/
	public void envelopePointRotateLeftABC(int cx, int cy, int cz) {
		int i2=-(z-cz);
		int j2=(x-cx);
		int k2=(y-cy);
		x=cx+i2; y=cy+j2; z=cz+k2; 
	}
	
	/* special rotation of +da (to the left) wrt (cx,cy,cz) **/
	public void envelopePointRotateABC(int cx, int cy, int cz, int da) {
		da=(da+6)%6; // for negative values
		for (int i=0; i<da; i++) {
			envelopePointRotateLeftABC(cx, cy, cz);
		}
	}
	
	public void envelopePointReflect(int cx, int cy, int cz) {
		int x2=z-cz;
		int y2=y-cy;
		int z2=x-cx;
		x=cx+x2; y=cy+y2; z=cz+z2;
	}
	
	
	/** from UVW $centered$ to ABC **/
	static public ATpoint convertUVWtoABC(int x, int y, int z) {
		int i= (x-z), j=(x+y), k=y+z;
		//Macro.fDebug("TRenv: %d %d %d -> %d %d %d",p.x,p.y,p.z,i,j,k);
		return new ATpoint(i, j, k);
	}
	
	static public ATpoint convertUVWtoABC(ATpoint p) {
		return convertUVWtoABC(p.x, p.y, p.z);
	}

	@Override
	public String toString() {
		return String.format("(%d,%d,%d)",x,y,z);
	}

	public void mult(int m) {
		x*=m; y*=m; z*=m;
	}
}
