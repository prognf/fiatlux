package models.CAmodelsvar.aperiodicTiling;

import components.types.FLString;
import components.types.FLStringList;

public class MetaTileCompound extends MetaTileAbstract {

	/** constructor with empty content **/
	public MetaTileCompound(String name) {
		super();
		setCenterABC(0,0,0);
		m_name= name;
	}

	/* constructor with compounds **/
	public MetaTileCompound(String name, FLStringList data) {
		this(name);
		addComponentsfromFile(data);
		m_envelope.createEnvelopeFromFile(data);

	}

	private void addComponentsfromFile(FLStringList data) {
		String ln;
		int linesBegin= data.findFirstPosContains("NCOMP=");
		int NCOMP= FLString.IParseInString(data.Get(linesBegin), "NCOMP=");
		for (int j=0; j< NCOMP; j++) {
			ln= data.get(linesBegin+1+j);
			MetaTileAbstract tile = TileGenerator.fromtileCode(ln);
			this.addAll(tile);
		}
	}

	/************************** 
	 * operations 
	 * ************************/

	/** usual translation, applies to all the components **/
	public void translateABC(int dx, int dy, int dz) {
		translateCenterABC(dx, dy, dz);
		m_envelope.translateABC(dx, dy, dz);
		for (MetaTileAbstract mtile : m_compoundL) {
			mtile.translateABC(dx, dy, dz);
		}
	}

	/** rotates all the components by a wrt (x,y,z) **/
	protected void rotate(int x, int y, int z, int da) {
		for (MetaTileAbstract mtile : m_compoundL) {
			mtile.rotate(x,y,z,da);
		}
		m_envelope.rotateABC(x,y,z,da);
		m_angle=(m_angle+da)%6;
	}

}
