package models.CAmodelsvar.aperiodicTiling;

import java.util.HashSet;
import java.util.Set;

public class HatCell {

	final Hat m_hat;
	// CA state
	int m_state;
	int m_buffer;
	static int m_hatCounter;
	// topology 
	Set<HatCell> m_neighb= new HashSet<HatCell>();


	HatCell(Hat hat){
		m_hat=hat;
		m_state= m_hatCounter==0?1:0;
		m_hatCounter++;
	}

	public String toString(){
		return String.format("hat> c:%s st:%d", m_hat.getCenterABC(), m_state);
	}

	public void addNeighb(HatCell hat) {
		m_neighb.add(hat);
	}

	public void calculateNextState() {
		//ftransitionCrown();
		ftransitionFredkin();
	}
	public void ftransitionCrown() {
		if (m_state>0) {
			m_buffer=m_state;
		} else {
			int sup=0;
			for (HatCell neighb : m_neighb) {
				if (neighb.m_state>0) {
					sup=neighb.m_state;
				}
			}
			m_buffer= sup==0?0:(sup+1);
		}
	}

	public void ftransitionFredkin() {
		int sum=0;
		for (HatCell neighb : m_neighb) {
			sum+=neighb.m_state;
		}
		m_buffer= sum%2;
	}


	/*
	 * 	int sum=0;
			for (HatCell neighb : m_neighb) {
				sum+= neighb.m_state;
			}
			m_buffer=sum>0?1:0;
	 * 
	 */
	public void updateToNextState() {
		m_state=m_buffer;
	}
}
