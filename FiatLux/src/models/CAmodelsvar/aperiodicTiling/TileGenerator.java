package models.CAmodelsvar.aperiodicTiling;

import java.util.HashMap;

import components.types.FLString;
import components.types.FLStringList;
import main.Macro;

public class TileGenerator {

	
	static String KEYS_BIS= "tpfhTPFHcvbnCVBN";
	
	
	// association tilename -> description
	static HashMap <String,FLStringList> m_dataTile= new HashMap<>();
	
	public final static char [] TYPE = {'T','P','F', 'H'};
	
	/** from code to tile ex. "t 0 0 1 2" : t=lettertype*/
	static public MetaTileAbstract fromtileCode(String code) {
		char start= code.charAt(0);
		// hat case (level 0)
		if (start== 'm')
			return Hat.newHat(code);
		if (start== 'r')
			return Hat.newHatReflected();
		// gen case
		int index=KEYS_BIS.indexOf(start);
		// Macro.fDebug(">>> tile creation form code:%s index:%d", code,index);
		if (index>=0)  {
			int level= 1+index/4; // level 1 = super !
			return getTile(level,TYPE[index%4],code);
		} else {
			Macro.SystemWarning("Tile code not understood: " + code);
			return null;
		}
	}

	/**-------------------- main ---------------------*/

	final static String [] STAGE= {"ZZZLEVEL0","super","mega","giga","tera","peta"};
	
	/* level 1=super, 2= mega... ; char 'T','P','F','H' ; scode = pos & angle */
	static MetaTileAbstract getTile(int level, char type, String scode) {
		String stype=STAGE[level]+type; // e.g. "megaH"
		//Macro.fDebug("creating : %s in coord:%s",stype, scode);
		return create(stype,scode);
	}
	
	/* level 1=super, 2= mega... ; char 'T','P','F','H' */
	static MetaTileAbstract getTileInZ(int level, char type) {
		return getTile(level, type, "0 0 0 0");
	}

	private static MetaTileAbstract create(String name, String scoord) {
		int [] coordABC=FLString.parse_k_numbers(4, scoord);
		int x=coordABC[0], y=coordABC[1], z=coordABC[2], a=coordABC[3]; 
		if (scoord.contains("+")) { //special code for mult coord.
			Macro.fPrint("x3 coord. for:%s", scoord);
			x*=3;y*=3;z*=3;
		}
		MetaTileCompound newtile = new MetaTileCompound(name, getDescription(name));
		newtile.rotate(a);
		newtile.translateABC(x, y, z);
		return newtile;
	}
	/**-------------------- special ---------------------*/

	private static FLStringList getDescription(String name) {
		if (! m_dataTile.containsKey(name)) { // not seen before
			String pathtoressource="models/CAmodelsvar/aperiodicTiling/dataTile/";
			String relpath="Xenv-"+name+".txt";
			Macro.print("reading file : "+ relpath + "... ");
			FLStringList data= FLStringList.OpenFileInsideSysFile(pathtoressource, relpath);
			m_dataTile.put(name, data);
		}
		return m_dataTile.get(name);
	}

	/** to identify points **/
	static MetaTileAbstract getPointer() {
		MetaTileAbstract pointer= new MetaTileCompound("pointer");
		ATpoint 
		p1= new ATpoint(0, 0, 0),
		p2= new ATpoint(0, 0, 1);
		pointer.addEnvelope(p1,p2);
		return pointer;
	}
	
	/**-------------  I/O -------------------------------------*/

}
