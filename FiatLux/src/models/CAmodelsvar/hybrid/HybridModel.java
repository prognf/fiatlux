package models.CAmodelsvar.hybrid;

import java.util.ArrayList;

import components.allCells.Cell;
import components.allCells.OneRegisterBlindCell;
import components.allCells.SuperCell;
import components.types.IntegerList;
import components.types.RuleCode;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.controllers.UpdatableTextDisplay;
import grafix.gfxTypes.elements.FLPanel;
import initializers.SuperInitializer;
import main.Macro;
import models.CAmodels.CellularModel;
import models.CAmodels.ClassicalModel;
import models.CAmodels.tabled.ITabledModel;

/*  hybrid= non spatially homogeneneous */
abstract public class HybridModel extends CellularModel {

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();
	private static final int TXTLEN = 10;

	abstract ClassicalModel GetModelFromCode(RuleCode Wcode);

	/*----------------------------------------------------------------------------
		  - attributes
		  --------------------------------------------------------------------------*/

	private ArrayList<ClassicalModel> m_ruleTable;
	private UpdatableTextDisplay m_display; 

	/*---------------------------------------------------------------------------
		  - construction
		  --------------------------------------------------------------------------*/

	/** exogeneous setting of the rule list **/
	public void ConstructRules(IntegerList list){
		m_ruleTable= new ArrayList<ClassicalModel>();
		for (int r : list){
			ClassicalModel model= GetModelFromCode(new RuleCode(r));
			m_ruleTable.add(model);
		}
		sig_UpdateView(); // 
	}


	/*---------------------------------------------------------------------------
		  - actions
		  --------------------------------------------------------------------------*/

	/** MAIN FUNCTION */
	public int ApplyLocalFunction(HybridCell hybridECAcell, int ruleNum) {
		return GetTable(ruleNum).ApplyLocalFunction(hybridECAcell);
	}


	/*---------------------------------------------------------------------------
		  - implementations & overrides
		  --------------------------------------------------------------------------*/
	@Override
	public SuperCell GetNewCell() {
		return new HybridCell(this);
	}

	@Override
	public SuperInitializer GetDefaultInitializer() {
		return new HybridCAInitializer(this);
	}

	/** cellChange determines if the cells are inert or no */
	@Override
	public Cell GetNewBlindCell(int state, boolean cellChange){ 
		return OneRegisterBlindCell.GetNewBlindCell(state,cellChange,GetRandomizer()); 
	} 


	/*---------------------------------------------------------------------------
		  - get & set
		  --------------------------------------------------------------------------*/

	public int GetRuleNum(){
		return m_ruleTable.size();
	}


	/*---------------------------------------------------------------------------
	  - GFX
	  --------------------------------------------------------------------------*/

	public void sig_UpdateView(){
		if (m_display != null){
			m_display.NotifyModification(GetRuleListForDisplay());
		}
	}

	// TODO : Cast here : tabled model assumed
	// easy solution is to remember the codes list (see constructor)
	public String GetRuleListForDisplay() {
		StringBuilder s= new StringBuilder("[");
		for (ClassicalModel model : m_ruleTable){
			ITabledModel mod2=(ITabledModel) model; 
			RuleCode Wcode = mod2.GetRuleCode();
			s.append(Wcode).append(":");
		}
		s.deleteCharAt(s.length()-1); // remove the last ":"
		s.append("]");
		return s.toString();
	}

	/** palette */
	@Override
	public PaintToolKit GetPalette(){
		PaintToolKit paint= PaintToolKit.GetDefaultPalette();
		paint.SetColor(2, FLColor.c_grey2);
		return paint;
	}

	public ClassicalModel GetTable(int ruleNum){
		return m_ruleTable.get(ruleNum);
	}


	@Override
	public FLPanel GetSpecificPanel() {
		m_display = new UpdatableTextDisplay(TXTLEN, GetRuleListForDisplay());
		FLPanel p = new FLPanel();
		p.Add(m_display);
		return p; 
	}

	/*---------------------------------------------------------------------------
		  - test
		  --------------------------------------------------------------------------*/



	public static void DoTest(){
		Macro.BeginTest(CLASSNAME);
		Macro.EndTest();
	}

}
