package models.CAmodelsvar.hybrid;

import components.allCells.OneRegisterIntCell;

/*  hybrid= non spatially homogeneneous */
public class HybridCell extends OneRegisterIntCell  {
	
	int m_ruleNum=-1;
	final HybridModel m_model;
	
	public HybridCell(HybridModel hybridModel) {
		m_model= hybridModel;
	}
	
	@Override
	public boolean IsStable() {
		sig_UpdateBuffer();
		return (m_buffer==m_state);
	}
	
	@Override
	public void sig_Reset() {
			m_state=0;
			m_buffer=0;
			m_ruleNum=-1;
	}
	
	@Override
	public void sig_UpdateBuffer() {
		m_buffer= m_model.ApplyLocalFunction(this, m_ruleNum);		
	}
	
	@Override
	public void sig_MakeTransition() {
		m_state= m_buffer;		
	}

	public void SetRuleNum(int ruleNum) {
		m_ruleNum= ruleNum;		
	}
	
		
}
