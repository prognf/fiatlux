package models.CAmodelsvar.hybrid;

import components.types.FLString;
import components.types.IntegerList;
import grafix.gfxTypes.controllers.SimpleReadController;
import grafix.gfxTypes.elements.FLPanel;
import initializers.CAinit.ArrayInitializer;

/*-------------------------------------------------------------------------------
- 
- @author Nazim Fates
------------------------------------------------------------------------------*/

public class HybridCAInitializer extends ArrayInitializer {


	enum INITMODE { PATTERN, OPENBOUND, PERBOUND }

    private static final String TXT_HYBRID = "CA codes seq:";

	/*----------------------------------------------------------------------------
	  - attributes
	  --------------------------------------------------------------------------*/

	final HybridModel m_model; 
	INITMODE m_InitMode= INITMODE.PATTERN;

	/*---------------------------------------------------------------------------
	  - construction
	  --------------------------------------------------------------------------*/

	public HybridCAInitializer(HybridModel hybridModel) {
		super();
		m_model= hybridModel;
	}


	/*---------------------------------------------------------------------------
	  - actions
	  --------------------------------------------------------------------------*/



	@Override
	protected void SubInit() {
		// "rules" init
		switch(m_InitMode){
		case PATTERN:
			RulePatternInit();
			break;
		case OPENBOUND:
			QuasiUniformInit();
			break;
		case PERBOUND:
			PerBoundInit();
			break;

		}
		// now we apply the "regular" init
		// FIXME super.SubInit();		
	}

	private void PerBoundInit() {
		IntegerList ruleList= IntegerList.GetNewListWithInit(1,3,11);
		m_model.ConstructRules(ruleList);
		int size= GetSize();
		for (int pos = 3; pos < size  - 1 ; pos++) {
			SetCellRuleNum(pos,2);
		}		
		SetCellRuleNum(0, 0);
		SetCellRuleNum(1, 1);
		SetCellRuleNum(2, 1);
		SetCellRuleNum(size-1, 0);		
	}

	private void QuasiUniformInit() {
		IntegerList ruleList= IntegerList.GetNewListWithInit(1,67,65);
		m_model.ConstructRules(ruleList);
		int size= GetSize();
		for (int pos = 3; pos < size  - 2 ; pos++) {
			SetCellRuleNum(pos,2);
		}		
		SetCellRuleNum(1, 0);
		SetCellRuleNum(2, 1);
		SetCellRuleNum(size-2, 0);		
	}

	private void RulePatternInit() {
		int NRules= m_model.GetRuleNum();
		//Macro.Debug("nrules" + NRules);
		for (int pos = 0; pos < GetSize(); pos++) {
			int ruleNum= pos % NRules;
			SetCellRuleNum(pos,ruleNum);
		}		
	}

	private void SetCellRuleNum(int pos, int ruleNum) {
		HybridCell cell= (HybridCell)GetCell(pos);
		cell.SetRuleNum(ruleNum);		
	}

	/*---------------------------------------------------------------------------
	  - implementations & overrides
	  --------------------------------------------------------------------------*/


	@Override
	public FLPanel GetSpecificPanel() {
		FLPanel p = new FLPanel();
		p.SetBoxLayoutY();
		HybridCAcontrol control = new HybridCAcontrol();
		p.Add(control, super.GetSpecificPanel());
		p.setOpaque(false);
		return p;
	}

	/*---------------------------------------------------------------------------
	  - get & set
	  --------------------------------------------------------------------------*/



	class HybridCAcontrol extends SimpleReadController {

		private static final int LEN = 10;

		public HybridCAcontrol() {
			super(TXT_HYBRID, LEN);
		}

		@Override
		public void ReadAndParseInput(String s) {
			if (!s.isEmpty()){
				String [] sruleTab = s.split("[^0-9]+"); //  not numeric
				IntegerList list= new IntegerList();
				for (String sDataRule : sruleTab){
					//Macro.Debug("read:" + sDataRule);
					int ruleCode= FLString.ParseInt(sDataRule);
					list.addVal(ruleCode);
				}
				m_model.ConstructRules(list);
			}
		}
	}

	/*---------------------------------------------------------------------------
	  - test
	  --------------------------------------------------------------------------*/

	public static void DoTest(){
		//Macro.BeginTest(CLASSNAME);
		//Macro.EndTest();
	}


}
