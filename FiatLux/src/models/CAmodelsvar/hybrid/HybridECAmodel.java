package models.CAmodelsvar.hybrid;

import components.types.RuleCode;
import main.tables.PlotterSelectControl;
import models.CAmodels.ClassicalModel;
import models.CAmodels.tabled.ECAmodel;
import topology.basics.TopologyCoder;


/*--------------------
 * hybrid= non spatially homogeneneous
 * ECA model : this part extends the abstract part 
*--------------------*/
public class HybridECAmodel extends HybridModel  {

	public final static String NAME="hybridECA";
	int [] DEFRULE = { 51, 204, 255 };


	/*public HybridECAmodel() {
		super();
		ConstructRules(new IntegerList(DEFRULE));
	}*/

	
	@Override
	ClassicalModel GetModelFromCode(RuleCode Wcode) {
		return new ECAmodel(Wcode);
	}

	public String GetDefaultAssociatedTopology() {
		return TopologyCoder.s_TLR1;
	}

	
	@Override
	public String [] GetPlotterSelection1D() {
		return PlotterSelectControl.ECAselect;
	}  

	
}
