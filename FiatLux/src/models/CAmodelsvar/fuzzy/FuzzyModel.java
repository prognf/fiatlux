package models.CAmodelsvar.fuzzy;

import components.allCells.Cell;
import components.arrays.RegularDynArray;
import components.types.IntC;
import grafix.gfxTypes.PaintToolKit;
import grafix.viewers.LineAutomatonViewer;
import initializers.SuperInitializer;
import models.CAmodels.CellularModel;
import topology.basics.LinearTopology;

public abstract class FuzzyModel extends CellularModel {

	/*--------------------
	 * Constructor
	 --------------------*/
	
	public FuzzyModel() {
		super();
	}

	/*--------------------
	 * overrides & implementations
	 --------------------*/

	public String GetDefaultAssociatedTopology() {
		return "TL1";
	}	
	
	abstract public float GetTransitionResult(FuzzyCell in_Cell);
	
	public Cell GetNewCell() {
		Cell newcell = new FuzzyCell(this);
		return newcell;
	}
	
	/*
	 * we allow the possibility for a model to return a specific initializer
	 * override this method for use
	 */
	public SuperInitializer GetDefaultInitializer() {
		return new FuzzyInitializer();
	}
	
	/* overrides */
	@Override
	public PaintToolKit GetPalette(){
		//return FLColor.GetNiceRainbow();
		return PaintToolKit.GetBWpalette();
	}
	
			
	/* override
	 * 1D Viewer */
	public LineAutomatonViewer GetLineAutomatonViewer(IntC in_SquareDim, 
		LinearTopology in_TopologyManager, RegularDynArray in_automaton, int in_Tsize) {
		LineAutomatonViewer av = new FuzzyLineAutomatonViewer(in_SquareDim, in_TopologyManager, in_automaton,in_Tsize);
		av.SetPalette( GetPalette() );
		return av;
	}	
	
	@Override
	public String [] GetPlotterSelection1D() {
		return null;
	}

}
