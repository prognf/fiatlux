package models.CAmodelsvar.fuzzy;

import grafix.gfxTypes.DoubleField;
import grafix.gfxTypes.controllers.DoubleController;
import grafix.gfxTypes.controllers.IntController;
import grafix.gfxTypes.elements.FLPanel;

public class FuzzyInitializer extends FuzzyInitDevice  {
	final static double DEF_RATE= 0.5;
	final static String TXT_INIT= "Non-Zero Cells Rate (%)";
	
	/*--------------------
	 * attributes
	 --------------------*/
	
	private DoubleField m_InitRate = 
		new DoubleField(TXT_INIT, DEF_RATE, DoubleController.CONVERSIONRATE.PERCENT);
	public final static int INIT_BOOLEAN=0, INIT_FUZZY=1, INIT_CENTRAL=2;
	private int m_InitType = INIT_BOOLEAN;
	final static float MAX= (float)1;
	
	/*--------------------
	 * MAIN FUNCTION
	 --------------------*/
	protected void SubInit() {
		switch (m_InitType) {
		case INIT_BOOLEAN:
			RandomArrayBoolean(m_InitRate.GetValue());
			break;
		case INIT_FUZZY:
			RandomArrayUniform(m_InitRate.GetValue());
			break;
		case INIT_CENTRAL:
			CentralFuzzy();
			break;
		default:
			break;
		}
					
	}

	/*
	 * random init with state values in {0, StateRange - 1} FillRate determines
	 * how many cells are not in state 0
	 */
	protected void RandomArrayBoolean(double in_FillRate) {
				for (int i = 0; i < GetSize(); i++) {
				if (RandomEventDouble(in_FillRate)) {
					SetAutomatonState(i, MAX);
				} else {
					SetAutomatonState(i, 0);
				}
				// central cell set to 1/2
				float ONEHALF = 0.5F;
				SetAutomatonState(GetSize()/2, ONEHALF);
			}
	}
	
	/*
	 * random init with state values in {0, StateRange - 1} FillRate determines
	 * how many cells are not in state 0
	 */
	protected void RandomArrayUniform(double in_FillRate) {
				for (int i = 0; i < GetSize(); i++) {
				if (RandomEventDouble(in_FillRate)) {
					float val = RandomFloat(MAX);
					SetAutomatonState(i, val);
				} else {
					SetAutomatonState(i, 0);
				}
			}
	}
	
	
	final static float HALF= (float)0.5;
	
	protected void CentralFuzzy(){
		SetAutomatonState( GetSize() / 3, HALF);
	}
	
		
	/*--------------------
	 * Get / Set
	 --------------------*/
	/* overrides */
	public FLPanel GetSpecificPanel() {
		FLPanel
			p0 = m_InitRate,
			p1 = GetRandomizer().GetSeedControl(),
			p2 = new InitTypeControl(),
			f = FLPanel.NewPanelVertical(p0, p1, p2);
		f.setOpaque(false);
		return f;
	}
	
	class InitTypeControl extends IntController{
		public InitTypeControl() {
			super("InitType");
		}

		@Override
		protected int ReadInt() {
				return m_InitType;
		}

		@Override
		protected void SetInt(int val) {
			m_InitType= val;
		}
		
	}
}
