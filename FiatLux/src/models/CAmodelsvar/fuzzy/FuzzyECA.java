package models.CAmodelsvar.fuzzy;

import components.types.RuleCode;
import models.CAmodels.tabled.ECAcontrol;
import models.CAmodels.tabled.ECAlookUpTable;

/*  */
abstract public class FuzzyECA extends FuzzyModel  
{
	
	
	protected ECAcontrol m_ECAcontrol ;
	protected ECAlookUpTable m_LookUpTable;

	public FuzzyECA(RuleCode Wcode) {
		m_LookUpTable= new ECAlookUpTable(Wcode);
	}
	
	final protected float bit(int i) {
		return m_LookUpTable.getBitTable(i);
	}

	
}
