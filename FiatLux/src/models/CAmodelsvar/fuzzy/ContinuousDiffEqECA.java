package models.CAmodelsvar.fuzzy;

import javax.swing.event.ChangeEvent;

import components.types.RuleCode;
import grafix.gfxTypes.controllers.FloatController;
import grafix.gfxTypes.elements.FLPanel;
import models.CAmodels.tabled.ECAcontrol;
/*--------------------
 * relationship between CA and DE
 * @author Nazim Fates
*--------------------*/
public class ContinuousDiffEqECA extends FuzzyECA 
{
	public final static String NAME="ContinuousDE-ECA";
	final static String TXT_DT="DT";
	final static float ZERO_F= 0.F, ONE_F=1.F;
	final static float DEF_DT= 1.F;
		
	
	/*--------------------
	 * Attributes
	 --------------------*/
	float m_dt = DEF_DT;
	FloatController m_DTcontrol;
	
	/*--------------------
	 * Constructor
	 --------------------*/

	public ContinuousDiffEqECA(RuleCode Wcode) {
		super(Wcode);
	}
	
	public ContinuousDiffEqECA() {
		this(RuleCode.NULLRULE);
	}

	
	/*--------------------
	 * overrides & implementations
	 --------------------*/

	
	/*--------------------
	 * MAIN FUNCTION (merci Nicolas Rougier)
	 --------------------*/

	final private float Saturation(float val){
		if (val>ONE_F)
			return ONE_F;
		else if (val<ZERO_F)
			return ZERO_F;
		else
			return val;
	}

	//final static int CX=1, CY=-2, CZ=1; // MAJ
	//final static int CX=1, CY=-1, CZ=0;   // R-SHIFT
	final static int CX=-2, CY=1, CZ=1;   // W142 
	//final static int CX=-1, CY=0, CZ=1;   // W142
	
	public float linGetTransitionResult(FuzzyCell in_Cell) {
		float 
			x = in_Cell.GetNeighbourState(0),
			y=  in_Cell.GetNeighbourState(1),
			z=  in_Cell.GetNeighbourState(2);
		
		//return Saturation(y +  m_dt * (2 *z - ONE_F ));
		//return (ONE_F - dt) * y +  dt * z ;
		//return Saturation( y  + (CX * x + CY * y +  CZ * z) * dt ) ;
		//return Saturation(y +  dt * ( x + z - ONE_F ));
		//return Saturation(y + (1- 2*y) * (x + - 2 * x * z) * dt ); // XOR ???
		
		float diff= 
			bit(0) * (1-x) * (1- y) * (1-z) +
			bit(1) * (1-x) * (1- y) * z +
			bit(4) *    x  * (1- y) * (1-z) +
			bit(5) *    x  * (1- y) * z -
			(1 - bit(2)) * (1-x) * y  * (1-z) -
			(1 - bit(3)) * (1-x) * y  * z -
			(1 - bit(6)) *    x  * y  * (1-z) -
			(1 - bit(7)) *    x  * y  * z ;
		float new_y = y + m_dt * diff;
		return new_y;
	}


	public float satGetTransitionResult(FuzzyCell in_Cell) {
		float 
			/*x = in_Cell.GetNeighbourState(0),*/
			y=  in_Cell.GetNeighbourState(1),
			z=  in_Cell.GetNeighbourState(2);
		
	 return Saturation(y +  m_dt * (2 *z - ONE_F ));
	}
	
	public float GetTransitionResult(FuzzyCell in_Cell) {
		return linGetTransitionResult(in_Cell);
	}
	
	/*--------------------
	 * Get / Set
	 --------------------*/

	public void SetDt(float val){
		m_dt= val;
	}

	
	/*--------------------
	 * GFX
	 --------------------*/

	
	/* overrides */
	public FLPanel GetSpecificPanel() {
		FLPanel panel= new FLPanel();
		m_ECAcontrol = new ECAcontrol(m_LookUpTable); // call back initialised
		m_DTcontrol = new DTControl(); 
		panel.Add(m_DTcontrol, m_ECAcontrol);
		panel.setOpaque(false);
		return panel;
	}

	class DTControl extends FloatController{

		public DTControl() {
			super(TXT_DT);
		}

		@Override
		protected float ReadFloat() {
			return m_dt;
		}

		@Override
		protected void SetFloat(float val) {
			m_dt= val;
		}

		@Override
		public void stateChanged(ChangeEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public int GetValueFromOwner() {
			// TODO Auto-generated method stub
			return 0;
		}
		
	}

}
