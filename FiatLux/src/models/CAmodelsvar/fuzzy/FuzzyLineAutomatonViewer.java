package models.CAmodelsvar.fuzzy;

import components.allCells.OneRegisterRealCell;
import components.arrays.GenericCellArray;
import components.arrays.RegularDynArray;
import components.types.IntC;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.controllers.Radio3Control;
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.LineAutomatonViewer;
import topology.basics.LinearTopology;

/*--------------------
 * enables the display of a 1D automaton the model passed
 * in the constructor is used to interpret the colors
 * it implements OenRegisterReadable by saying what are the current colors
 * @author Nazim Fates
*--------------------*/
public class FuzzyLineAutomatonViewer extends LineAutomatonViewer {

	private final static String CLASSNAME="FuzzyLineAutomatonViewer";

	private static final int DEF_SELECT = 2;
	
	GenericCellArray<OneRegisterRealCell> m_array ;
	Radio3Control m_PaletteControl; 
	RegularDynArray m_automaton;
	
	public FuzzyLineAutomatonViewer(IntC in_CellSize, 
			LinearTopology in_topo, RegularDynArray in_automaton, int in_Tsize) {
		super(in_CellSize, in_topo, in_automaton, in_Tsize);
		m_automaton= in_automaton;
		m_array = new GenericCellArray<OneRegisterRealCell>(in_automaton);
		
		m_PaletteControl = new PaletteControl(); // inner class
	}

	@Override
	public void UpdateBuffer(int[] timeBuffer) {
		for (int pos=0 ; pos < timeBuffer.length ; pos++){
			float state= m_array.GetCell(pos).GetState();
			timeBuffer[pos]= StateToColorCode(state);
		}
	}
	

	@Override
	protected void FlipCellState(int pos) {
		OneRegisterRealCell cell = m_array.GetCell(pos);
		float state = 1.F - cell.GetState();
		cell.SetState(state);	
	}

	/** overrides  */
	public FLPanel GetSpecificPanel(){
		FLPanel p = new FLPanel();
		p.Add(super.GetSpecificPanel(), m_PaletteControl);
		return	p;	
	}
	
	final int StateToColorCode(float in_state) {
		return (int)(in_state * FLColor.MAX) ;
	}
	
	final static PaintToolKit 
		one= PaintToolKit.GetNiceRainbow(), 
		two= PaintToolKit.GetBWpalette(), 
		three= PaintToolKit.GetRandomPalette(FLColor.MAX);

	class PaletteControl extends Radio3Control {

		PaletteControl(){
			super("palette:","rainbow","black'n'white","random");
			SetSelection(DEF_SELECT);
		}
		
		@Override
		protected void Action1() {SetUpPalette(one);}
		
		@Override
		protected void Action2() {SetUpPalette(two);}

		@Override
		protected void Action3() {SetUpPalette(three);}

		private void SetUpPalette(PaintToolKit paint) {
			SetPalette( paint);
			UpdateView();
		}
		
	}


}
