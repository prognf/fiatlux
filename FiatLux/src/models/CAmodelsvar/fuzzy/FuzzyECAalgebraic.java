package models.CAmodelsvar.fuzzy;

import components.types.RuleCode;
import grafix.gfxTypes.DoubleField;
import grafix.gfxTypes.controllers.DoubleController.CONVERSIONRATE;
import grafix.gfxTypes.elements.FLPanel;
import models.CAmodels.tabled.ECAcontrol;

public class FuzzyECAalgebraic extends FuzzyECA{
	
	public final static String NAME="FuzzyECAalgebraic"; 

	private double DEF_ALPHAQ=0.;
	
	/*--------------------
	 * Attributes
	 --------------------*/


	DoubleField m_alphaQ= new DoubleField("inertiaQ", DEF_ALPHAQ, CONVERSIONRATE.ONE);
	
	
	/*--------------------
	 * overrides & implementations
	 --------------------*/


	
	/*--------------------
	 * Constructor
	 --------------------*/
	
	
	public FuzzyECAalgebraic(RuleCode Wcode) {
		super(Wcode);
	}

	public FuzzyECAalgebraic() {
		this(RuleCode.NULLRULE);
	}

	/*--------------------
	 * MAIN FUNCTION 
	 --------------------*/
	
	public float GetTransitionResult(FuzzyCell in_Cell) {
		float x = in_Cell.GetNeighbourState(0),
			  y=  in_Cell.GetNeighbourState(1),
			  z=  in_Cell.GetNeighbourState(2);
		
		float p= 
				bit(0) * (1-x) * (1- y) * (1-z) +
				bit(1) * (1-x) * (1- y) * z     +
				bit(2) * (1-x) *     y  * (1-z) +
				bit(3) * (1-x) *     y  * z     +
				bit(4) *    x  * (1- y) * (1-z) +
				bit(5) *    x  * (1- y) * z     +
				bit(6) *    x  *     y  * (1-z) +
				bit(7) *    x  *     y  * z ;
		float alpha = (float)m_alphaQ.GetValue();
		return (1 - alpha) * p + alpha * y;
		//return (1 - x * (1 - y)) * z + x * (1 - y) * (1 - z);
	}
	
	/*--------------------
	 * Get / Set
	 --------------------*/

	/*--------------------
	 * GFX
	 --------------------*/
	
	/* overrides */
	public FLPanel GetSpecificPanel() {		
		m_ECAcontrol = new ECAcontrol(m_LookUpTable); 
		FLPanel p = new FLPanel();
		p.Add(m_ECAcontrol, m_alphaQ);
		p.setOpaque(false);
		return p;
	}
	
	
	
}
