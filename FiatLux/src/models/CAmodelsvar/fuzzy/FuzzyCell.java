package models.CAmodelsvar.fuzzy;

import components.allCells.OneRegisterRealCell;
import components.allCells.SuperCell;
import components.arrays.GenericCellArray;


/* import java.awt.*;
 import javax.swing.*;
 */

/*--------------------
 * classical CA cell : it can only read neighbours state
 * @author Nazim Fates
*--------------------*/
public class FuzzyCell extends OneRegisterRealCell {

	final static int ZERO = 0;

	FuzzyModel m_Model;

	public FuzzyCell(FuzzyModel in_Model) {
		m_Model = in_Model;
	}

	public void sig_Reset() {
		SetState(ZERO);
	}

	public void sig_UpdateBuffer() {
		SetBufferState(m_Model.GetTransitionResult(this));
	}

	/* current state <- buffer state */
	final public void sig_MakeTransition() {
		SetState(GetBufferState());
	}

	/* return the state of the i-th neighbour (useful for ECA for example) */
	final public float GetNeighbourState(int in_NeighbourPosition) {
		return GetNeighbour(in_NeighbourPosition).GetState();
	}

	/*
	 * returns the number of occurences of state in_state in the neighbourhood
	 * of the cell this means it has to ask all the neighbours about their state
	 * (slow)
	 */
	final public int GetOccurenceOf(int in_State) {
		int count = 0;
		for (int i = 0; i < GetNeighbourhoodSize(); i++) {
			if (GetNeighbour(i).GetState() == in_State)
				count++;
		}
		return count;
	}

	/*--------------------
	 * NEIGHBOURHOOD methods
	 --------------------*/
	private GenericCellArray<FuzzyCell> m_Neighbourhood;

	/* returns the i-th neighbour  (no control) */
	final public FuzzyCell GetNeighbour(int in_NeighbourPosition) {
		return m_Neighbourhood.get(in_NeighbourPosition);
	}

	/*  a SimpleCell can be only surrounded by similar cells
	 * we use a cast here ! */
	public void SetNeighbourhood(GenericCellArray<SuperCell> in_NeighbArray) {
		m_Neighbourhood= new GenericCellArray<FuzzyCell>(in_NeighbArray);
	}
	
	/* returns the size of the neighbourhood of the cell */
	final public int GetNeighbourhoodSize() {
		return m_Neighbourhood.size();
	}
}
