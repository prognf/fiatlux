package models.CAmodelsvar.fuzzy;

import components.allCells.OneRegisterRealCell;
import components.arrays.GenericCellArray;
import components.arrays.RegularDynArray;
import components.arrays.SuperSystem;
import initializers.CAinit.ArrayInitializer;
import main.Macro;
import topology.basics.SuperTopology;

/*--------------------
 * A SimpleInitDevice controls OneRegister cells 
  * @author : Nazim Fates
*--------------------*/

public abstract class FuzzyInitDevice extends ArrayInitializer {

	
	/*--------------------
	 * Attributes
	 --------------------*/
	GenericCellArray<OneRegisterRealCell> m_Array; 
		
	/*--------------------
	 * 
	 --------------------*/
	
	@Override
	public void LinkTo(SuperSystem system, SuperTopology topo){
		super.LinkTo(system, topo);
		if (system == null){
			Macro.FatalError("Automaton given not initialized");
		}
		RegularDynArray system2= (RegularDynArray)system;
		m_Array = new GenericCellArray<OneRegisterRealCell>( system2 );
	}
	
	/*--------------------
	 * protected methods
	 --------------------*/

	/* direct setting of a cell state */
	protected void SetAutomatonState(int pos, float in_state) {
		m_Array.GetCell(pos).SetState(in_state);
	}

	/*--------------------
	 * Other methods
	 --------------------*/


		

}// end class

