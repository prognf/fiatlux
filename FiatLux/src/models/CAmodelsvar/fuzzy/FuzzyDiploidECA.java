package models.CAmodelsvar.fuzzy;

import components.types.ProbaPar;
import components.types.RuleCode;
import grafix.gfxTypes.elements.FLPanel;
import models.CAmodels.tabled.ECAcontrol;
import models.CAmodels.tabled.ECAlookUpTable;

public class FuzzyDiploidECA extends FuzzyModel{
	
	public final static String NAME="FuzzyDiploidECA"; 

	private double DEF_w=0.5;
	
	/*--------------------
	 * Attributes
	 --------------------*/


	ProbaPar m_w= new ProbaPar("w2", DEF_w);

	protected ECAcontrol m_ECAcontrolA, m_ECAcontrolB ;
	protected ECAlookUpTable m_LookUpTableA, m_LookUpTableB;
	
	
	/*--------------------
	 * overrides & implementations
	 --------------------*/


	
	/*--------------------
	 * Constructor
	 --------------------*/
	
	
	public FuzzyDiploidECA(RuleCode WcodeA, RuleCode WcodeB) {
		m_LookUpTableA= new ECAlookUpTable(WcodeA);
		m_LookUpTableB= new ECAlookUpTable(WcodeB);
	}

	public FuzzyDiploidECA() {
		this(new RuleCode (184), RuleCode.NULLRULE);
	}

	/*--------------------
	 * MAIN FUNCTION 
	 --------------------*/
	
	public float GetTransitionResult(FuzzyCell in_Cell) {
		float x = in_Cell.GetNeighbourState(0),
			  y=  in_Cell.GetNeighbourState(1),
			  z=  in_Cell.GetNeighbourState(2);
		float 
			p1= localECAf(m_LookUpTableA,x,y,z),
			p2= localECAf(m_LookUpTableB,x,y,z);	
	/*	float p= 
				bit(0) * (1-x) * (1- y) * (1-z) +
				bit(1) * (1-x) * (1- y) * z     +
				bit(2) * (1-x) *     y  * (1-z) +
				bit(3) * (1-x) *     y  * z     +
				bit(4) *    x  * (1- y) * (1-z) +
				bit(5) *    x  * (1- y) * z     +
				bit(6) *    x  *     y  * (1-z) +
				bit(7) *    x  *     y  * z ;*/
		float alpha = (float)m_w.GetVal();
		return (1 - alpha) * p1 + alpha * p2;
	}
	
	/*--------------------
	 * Get / Set
	 --------------------*/

	/*--------------------
	 * GFX
	 --------------------*/
	
	private float localECAf(ECAlookUpTable lookUpTable, float x, float y, float z) {
			float p= 
		lookUpTable.getBitTable(0) * (1-x) * (1- y) * (1-z) +
		lookUpTable.getBitTable(1) * (1-x) * (1- y) * z     +
		lookUpTable.getBitTable(2) * (1-x) *     y  * (1-z) +
		lookUpTable.getBitTable(3) * (1-x) *     y  * z     +
		lookUpTable.getBitTable(4) *    x  * (1- y) * (1-z) +
		lookUpTable.getBitTable(5) *    x  * (1- y) * z     +
		lookUpTable.getBitTable(6) *    x  *     y  * (1-z) +
		lookUpTable.getBitTable(7) *    x  *     y  * z ;
			return p;
	}

	/* overrides */
	public FLPanel GetSpecificPanel() {	
		FLPanel panel= new FLPanel();
		m_ECAcontrolA = new ECAcontrol(m_LookUpTableA); // call back initialised
		m_ECAcontrolB = new ECAcontrol(m_LookUpTableB); // call back initialised
		panel.Add(m_ECAcontrolA, m_ECAcontrolB, m_w);
		return panel;
	}
	
	
	
}
