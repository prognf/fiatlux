package models.IPSmodels;

import components.types.IntegerList;



public class PhaseSynchIPSmodel extends AbstractDensClassifIPSmodel {

	private static final int HALFNEIGHB = 2; // for majority function 

	final static int K=3;

	public static String NAME="PhaseSynch";

	public PhaseSynchIPSmodel() {
		super();
	}

	
	/** MAIN RULE DEF **/
	private boolean RuleOne(int n1, int n2) {
		if (Case(n1,n2,1,2)) return true;
		
		// break case
		if ( Case(n1,n2,0,3) || Case(n1,n2,0,2)  ) 
				return RandomEventDouble(m_breakRate.GetVal());
		return (n1 + n2 > K) ;
	}

	/** main function **/
	@Override
	final public boolean IsChangeApplied(int pos1, int pos2) {
			
		int alpha1= GetCellState(pos1), alpha2= GetCellState(pos2);  
		if (alpha1 == alpha2){
			return false;
		} else {
			int nI1= GetCellNeighbOccurrence(pos1, alpha1),
			    nI2= GetCellNeighbOccurrence(pos2, alpha2);
			//int sigma= nI1 + nI2;
			return RuleOne(nI1,nI2);			
		}
	}

	
	private boolean Case(int nI1, int nI2, int val1, int val2) {
		return ((nI1==val1) && (nI2==val2)) ||
			   ((nI1==val2) && (nI2==val1));
	}

	private boolean IsStable(int pos, int alpha) {
		IntegerList neighb= GetNeighbGraph(pos);
		boolean condA=( 	
				(alpha == GetNeighb(neighb,0)) && 
				(alpha == GetNeighb(neighb,2)) && 
				(alpha != GetNeighb(neighb,1)) && 
				(alpha != GetNeighb(neighb,3)) );
		boolean condB=( 	
				(alpha == GetNeighb(neighb,1)) && 
				(alpha == GetNeighb(neighb,3)) && 
				(alpha != GetNeighb(neighb,0)) && 
				(alpha != GetNeighb(neighb,2)) );

		return condA || condB;		
	}

	private int GetNeighb(IntegerList neighb, int index) {
		return GetCellState(neighb.Get(index));
	}

	/** for majority rule application **/
	public boolean IsStateChanged(int pos) {
		int st= GetCellState(pos);
		int nId= GetCellNeighbOccurrence(pos, st);
		return ( (nId < HALFNEIGHB) && IsNoiseApplied() );
	}
}
