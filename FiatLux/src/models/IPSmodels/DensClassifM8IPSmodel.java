package models.IPSmodels;

import components.types.IntegerList;


public class DensClassifM8IPSmodel extends AbstractDensClassifIPSmodel {


	private static final int HALFNEIGHB = 4; // for majority function 

	// to checkerboard
	private static final int AA = 0, BB=2, CC=4, DD=6;

	//	to lines
	//	private static final int AA = 1, BB=3, CC=5, DD=7;



	public static String NAME="DensClassifM8-IPS";


	public DensClassifM8IPSmodel() {
		super();
	}


	/** non conservation of particles **/
	public boolean IsModelNonConservative() {
		return true;
	}


	/** for majority rule application **/
	public boolean IsStateChanged(int pos) {
		//return false;
		int st= GetCellState(pos);
		int nId= GetCellNeighbOccurrence(pos, st);
		return ( (nId < HALFNEIGHB ) 
				&& IsNoiseApplied() );
	}


	int SIGMA=2;
	
	/** main function **/
	@Override
	final public boolean IsChangeApplied(int pos1, int pos2) {


		int alpha1= GetCellState(pos1), alpha2= GetCellState(pos2);  
		if (alpha1 == alpha2){
			return false;
		} else {
			IntegerList neighb1= GetNeighbGraph(pos1);
			IntegerList neighb2= GetNeighbGraph(pos2);

			int 
			N1= GetCellState(neighb1.Get(AA)),
			E1= GetCellState(neighb1.Get(BB)),
			S1= GetCellState(neighb1.Get(CC)),
			W1= GetCellState(neighb1.Get(DD));
			int 
			N2= GetCellState(neighb2.Get(AA)),
			E2= GetCellState(neighb2.Get(BB)),
			S2= GetCellState(neighb2.Get(CC)),
			W2= GetCellState(neighb2.Get(DD));

			int 
			nI14= SumID(alpha1, N1, E1, S1, W1),
			nI24= SumID(alpha2, N2, E2, S2, W2);
			int s= nI14 + nI24;
			//boolean noise= RandomEventDouble(m_NoiseRate.GetVal());
			return 	(s > SIGMA) || smallNoiseSymBreak(s) /*|| (nI14==4) || (nI24==4)*/;

		}
	}

	private boolean smallNoiseSymBreak(int s) {
		return (s == SIGMA ) && RandomEventDouble(m_breakRate.GetVal());
	}


	private int SumID(int alpha, int N, int E, int S, int W) {
		int s=0;
		if (N==alpha) s++;
		if (E==alpha) s++;
		if (S==alpha) s++;
		if (W==alpha) s++;
		return s;
	}


	private boolean AllNeighbSimilar(int n, int e, int s, int w) {
		return (n==e) && (e==s) && (s==w);
	}




}
//((W1 == alpha1) && (W2==alpha2)); /*||
//(false && AllNeighbSimilar(N1,E1,S1,W1) && 
//AllNeighbSimilar(N2,E2,S2,W2));/*
//( (N1==alpha1) && (E1==alpha1) && (S1==alpha1) && (W1==alpha1) ) ||
//( (N2==alpha2) && (E2==alpha2) && (S2==alpha2) && (W2==alpha2) );*/ 
/*	(nI1 == 4) || (nI2==4); /*||*/
/*	((E1 == alpha1) && (E2==alpha2)) ||
	;*/ 

