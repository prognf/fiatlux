package models.IPSmodels;

import architecture.interactingParticleSystems.IPSViewerRegular;
import components.types.DoublePar;
import components.types.IntC;
import grafix.gfxTypes.elements.FLPanel;
import initializers.IPSinit.IPSinitRegularGrid.CODE_IPS_INIT;
import main.MathMacro;

// TODO : pre-calculus of the exponentials
public class KamaisModel extends IPSmodel {
	private static final double DVAL = 2.;
	private static final int HALFNEIGHB = 2;

	DoublePar m_lambda;
	protected final DoublePar m_MajorityProba= new DoublePar("Maj. prob.", 0.);
	
	public static String NAME = "Kamais";

	public KamaisModel() {
		super();
		m_lambda = new DoublePar("lambda", DVAL);
	}
	
	/** main function **/
	@Override
	final public boolean IsChangeApplied(int pos1, int pos2) {
		int alpha1 = GetCellState(pos1), alpha2 = GetCellState(pos2);
		if (alpha1 == alpha2) {
			return false;
		} else {
			// binary state is assumed
			int d1= GetCellNeighbOccurrence(pos1, alpha2),// note the inversion
			    d2= GetCellNeighbOccurrence(pos2, alpha1),
			    s= d1 + d2 - 2, comps= 6 - s;
			double lambda = m_lambda.GetVal();
			
			double probaChg= 
					MathMacro.Power(lambda, comps)  / 
					(MathMacro.Power(lambda, s) + MathMacro.Power(lambda, comps));
			return RandomEventDouble(probaChg);
		}
	}
	
	/** for majority rule application **/
	public boolean IsStateChanged(int pos) {
		int st= GetCellState(pos);
		int nId= GetCellNeighbOccurrence(pos, st);
		return ( (nId < HALFNEIGHB) && IsMajorityApplied() );
	}
	
	
	boolean IsMajorityApplied() {
		return RandomEventDouble(m_MajorityProba.GetVal());
	}

	@Override
	public FLPanel GetViewControl() {
		FLPanel p = new FLPanel();
		p.Add(m_MajorityProba.GetControl(), m_lambda.GetControl());
		p.AddRand(GetRandomizer());
		return p;
	}
	
	
	/** do we allow particles to disappear ? */
	public boolean IsModelNonConservative() {
		return true;
	}

	@Override
	public CODE_IPS_INIT GetInitializerCode() {
		return CODE_IPS_INIT.Bernoulli;
	}

	@Override
	public IPSViewerRegular GetViewer(IntC cellPixSize, IntC XYsize) {
		return IPSmodel.GetDefaultViewer(cellPixSize, XYsize);
	}
}
