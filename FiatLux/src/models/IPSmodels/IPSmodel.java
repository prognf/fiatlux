package models.IPSmodels;

import architecture.interactingParticleSystems.IPSViewerRegular;
import architecture.interactingParticleSystems.InteractingParticleSystemPlanarAbstract;
import architecture.interactingParticleSystems.InteractingParticleSystemRegular;
import components.types.IntC;
import components.types.IntegerList;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.elements.FLPanel;
import initializers.IPSinit.IPSinitRegularGrid.CODE_IPS_INIT;
import main.Macro;
import models.SuperModel;
import topology.basics.PlanarTopology;


abstract public class IPSmodel extends SuperModel {

	final static String CLASSNAME2= Thread.currentThread().getStackTrace()[1].getClassName();

	abstract public CODE_IPS_INIT GetInitializerCode();

	abstract public IPSViewerRegular GetViewer(IntC cellPixSize, IntC xysize);

	abstract public FLPanel GetViewControl();

	public abstract boolean IsChangeApplied(int pos1, int pos2);

	/*----------------------------------------------------------------------------
	  - attributes
	  --------------------------------------------------------------------------*/
	private InteractingParticleSystemPlanarAbstract m_system;

	/*---------------------------------------------------------------------------
	  - construction
	  --------------------------------------------------------------------------*/

	public IPSmodel() {
		super();
	}

	public void LinkTo(InteractingParticleSystemPlanarAbstract in_system){
		m_system= in_system;
	}

	/*---------------------------------------------------------------------------
	  - actions
	  --------------------------------------------------------------------------*/

	/** override if needed **/
	public void ProcessClickOnCell(int cellPos){
		int state = m_system.GetCellState(cellPos);
		m_system.SetCellState(cellPos, 1 - state);
	}

	/** do we allow particles to disappear ? 
	 * override if needed **/
	public boolean IsModelNonConservative() {
		return false;
	}

	/** use only with non-conservative models 
	 * override **/
	public boolean IsStateChanged(int pos) {
		Macro.FatalError(" use only with non-conservative models ");
		return false;
	}
	/*---------------------------------------------------------------------------
	  - get & set
	  --------------------------------------------------------------------------*/
	/** CAST : use only with "regular" IPS **/
	protected IntegerList GetNeighbGraph(int cellPos){
		InteractingParticleSystemRegular sys = (InteractingParticleSystemRegular) m_system;
		return sys.GetNeighbGraph(cellPos);
	}
	/** default associated viewer 
	 * use link to afterwards !   **/
	static public IPSViewerRegular GetDefaultViewer(IntC cellPixSize, IntC XYsize) {
		IPSViewerRegular view= new IPSViewerRegular(cellPixSize, XYsize);
		view.SetPalette(PaintToolKit.GetDefaultPalette());
		return view;
	}


	protected final int GetCellState(int pos) {
		return m_system.GetCellState(pos);
	}

	protected int GetCellNeighbOccurrence(int pos, int state){
		IntegerList neighbList= GetNeighbGraph(pos);
		int count=0;
		for (int neighb : neighbList){
			if (GetCellState(neighb) == state){
				count++;
			}
		}
		return count;
	}

	/*---------------------------------------------------------------------------
	  - codes
	  --------------------------------------------------------------------------*/

	// special case for HighWay : TASEP
	public enum CODE_IPS_MODEL { stochosP, highwayTASEP, Kamais, RandomWalk, AdhesionWalk, CellSorting, 
		PhaseSynch, DensClassif4, DensClassif8, SultraWeaving}
	// stochosP is treated as a special case in IPSdesignerPanel
	
    /** in the construction,
	 * the model has the possibility to use the perception topology (neighb. of cells) **/
	static public IPSmodel CodeToModel(CODE_IPS_MODEL code, PlanarTopology topoP){
		switch(code){
		case Kamais:
			return new KamaisModel();
		case RandomWalk:
			return new SimpleRandomWalkIPSmodel();
		case AdhesionWalk:
			return new AdhesionWalkIPSmodel();
		case CellSorting:
			return new CellSortingIPSmodel(topoP);
		case PhaseSynch:
			return new PhaseSynchIPSmodel();
		case DensClassif4:
			return new DensClassifV4IPSmodel();
		case DensClassif8:
			return new DensClassifM8IPSmodel();
		case SultraWeaving:
			return new SultraWeavingIPSmodel(topoP);
		default:
			return null;
		}
	}

	/*---------------------------------------------------------------------------
	  - test
	  --------------------------------------------------------------------------*/
	public static void DoTest(){
		Macro.BeginTest(CLASSNAME2);
		Macro.EndTest();
	}
}
