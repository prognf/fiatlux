package models.IPSmodels;

import architecture.interactingParticleSystems.IPSViewerRegular;
import components.types.DoublePar;
import components.types.IntC;
import components.types.IntegerList;
import grafix.gfxTypes.elements.FLPanel;
import initializers.IPSinit.IPSinitRegularGrid.CODE_IPS_INIT;


public class AdhesionWalkIPSmodel extends IPSmodel {


	private DoublePar m_NoiseRate= new DoublePar("Noise prob.", 0.);


	public static String NAME="AdhesionRW-IPS";


	public AdhesionWalkIPSmodel() {
		super();
	}


	@Override
	public CODE_IPS_INIT GetInitializerCode() {
		return CODE_IPS_INIT.Bernoulli;
		//return CODE_IPS_INIT.Particles;
	}

	@Override
	public FLPanel GetViewControl() {
		FLPanel p= m_NoiseRate.GetControl();
		return p;
	}

	/** main function **/
	@Override
	final public boolean IsChangeApplied(int pos1, int pos2) {
		int alpha1= GetCellState(pos1), alpha2= GetCellState(pos2);  
		if (alpha1 == alpha2){
			return false;
		} else {
			IntegerList 
			neighb1= GetNeighbGraph(pos1), 
			neighb2= GetNeighbGraph(pos2);
			int s1= GetNeighbSum(neighb1),
			s2= GetNeighbSum(neighb2);
			int deltaE= (s1 - s2)*(alpha1-alpha2);
			boolean noise= RandomEventDouble(m_NoiseRate.GetVal());
			return (deltaE <0) || noise ;
		}
	}

	/** input: list of neighbour indices ; output: sum of states in these positions **/  
	private int GetNeighbSum(IntegerList neighb1) {
		int sum=0;
		for (int pos : neighb1){
			sum += GetCellState(pos);
		}
		return sum;
	}

	@Override
	public IPSViewerRegular GetViewer(IntC cellPixSize, IntC XYsize) {
		IPSViewerRegular viewer= IPSmodel.GetDefaultViewer(cellPixSize, XYsize);
		//FLColor [] cols= {FLColor.c_brown, FLColor.c_darkgreen, FLColor.c_yellow}; 
		//PaintToolKit palette = new PaintToolKit(cols);
		//viewer.SetPalette(palette);
		return viewer;
	}


	

	
	/*--------------------
	 * GFX
	 --------------------*/

}
