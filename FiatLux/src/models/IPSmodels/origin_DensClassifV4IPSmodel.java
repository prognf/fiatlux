package models.IPSmodels;

import components.types.IntegerList;



public class origin_DensClassifV4IPSmodel extends AbstractDensClassifIPSmodel {

	private static final int HALFNEIGHB = 2; // for majority function 


	public static String NAME="DensClassif-IPS4";

	public origin_DensClassifV4IPSmodel() {
		super();
	}

	/** for majority rule application **/
	public boolean IsStateChanged(int pos) {
		int st= GetCellState(pos);
		int nId= GetCellNeighbOccurrence(pos, st);
		return ( (nId < HALFNEIGHB) && IsNoiseApplied() );
	}



	/** main function **/
	@Override
	final public boolean IsChangeApplied(int pos1, int pos2) {
		return IsChangeAppliedToCheckerboard(pos1, pos2);
	}
	
	final static int K=3;

	final public boolean IsChangeAppliedToCheckerboard(int pos1, int pos2) {
		
		int alpha1= GetCellState(pos1), alpha2= GetCellState(pos2);  
		if (alpha1 == alpha2){
			return false;
		} else {
			int nI1= GetCellNeighbOccurrence(pos1, alpha1),
			    nI2= GetCellNeighbOccurrence(pos2, alpha2);
			int sigma= nI1 + nI2;
			/*boolean blq= ((nI1==0)||(nI2==0)) && ( (nI1 + nI2) <4 ) ;
			return !blq;*/
			if (Case(nI1,nI2,0,4)) return true; // deblocking crosses
			
			//if ((nI1==0) || (nI2==0)) return false; // blocking isolated
			
			// deblocking Ferrero
			if  (Case(nI1,nI2,1,2))
				return true; //RandomEventDouble(m_BreakRate);
			
			//boolean noise= RandomEventDouble(m_NoiseRate.GetVal());
			return (nI1 + nI2 > K) ;		
		}
	}

	private boolean Case(int nI1, int nI2, int val1, int val2) {
		return ((nI1==val1) && (nI2==val2)) ||
			   ((nI1==val2) && (nI2==val1));
	}

	

	final public boolean IsChangeAppliedToLines(int pos1, int pos2) {		
		int alpha1= GetCellState(pos1), alpha2= GetCellState(pos2);  
		return (!IsStable(pos1,alpha1)) || (!IsStable(pos2,alpha2));
	}

	private boolean IsStable(int pos, int alpha) {
		IntegerList neighb= GetNeighbGraph(pos);
		boolean condA=( 	
				(alpha == GetNeighb(neighb,0)) && 
				(alpha == GetNeighb(neighb,2)) && 
				(alpha != GetNeighb(neighb,1)) && 
				(alpha != GetNeighb(neighb,3)) );
		boolean condB=( 	
				(alpha == GetNeighb(neighb,1)) && 
				(alpha == GetNeighb(neighb,3)) && 
				(alpha != GetNeighb(neighb,0)) && 
				(alpha != GetNeighb(neighb,2)) );

		return condA || condB;		
	}

	private int GetNeighb(IntegerList neighb, int index) {
		return GetCellState(neighb.Get(index));
	}

	
}
