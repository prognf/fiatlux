package models.IPSmodels;

import java.util.ArrayList;

import components.types.FLStringList;
import main.Macro;

public class RateDataTable {



	private static final boolean PRINTTABLE = false;
	private final int NEIGHBSIZE;	
	// maximum indices in the transition table
	final private int MAXN1, MAXNB, MAXPOS;
	
	/** rates **/
	final private double[] m_Rate, m_Exponent;
	
	/** possible transitions */
	final private ArrayList<Triplet> m_ValidTransitions;


	public RateDataTable(int neighbSize) {
		NEIGHBSIZE= neighbSize;
		MAXN1= (NEIGHBSIZE+1);
		MAXNB= (NEIGHBSIZE+1);
		MAXPOS = CellSortingIPSmodel.NSTATES * MAXN1 * MAXNB;
		m_Rate= new double [MAXPOS];
		m_Exponent= new double [MAXPOS];
		m_ValidTransitions = new ArrayList<Triplet>();
		for (int state=1; state <= 2; state++){
			for (int n1=0 ; n1 < MAXN1; n1++){
				for (int nB=0; nB + n1 < MAXNB ; nB++){
					Triplet t = new Triplet(state,n1,nB);
					m_ValidTransitions.add(t);
				}				
			}
		}
	}
	
	
	
	/** main function **/
	final double GetRateTable(int st, int n1, int nB) {
		// coding operation : from triplet to pos		
		int pos=  TripletToPos(st,n1,nB);
		return m_Rate[pos];
	}

	final private double GetExponent(int st, int n1, int nB) {
		// coding operation : from triplet to pos		
		int pos=  TripletToPos(st,n1,nB);
		return m_Exponent[pos];
	}


	/** fills rate table without rescaling factor **/
	private void computeRateState(double [][] Beta){
		for (Triplet t : m_ValidTransitions){
			double exp= ComputeExp(Beta, t);
			int pos= t.ToPos();
			m_Exponent[pos]= exp; 
			double val= Exp( - exp );
			m_Rate[pos]= val; 
			if ((val > 1) || (val  + Macro.EPSILON < 0. )){
				Macro.FatalError("Problem with the computation of rates : " + val + " Exp: " + exp);
			} 
		}
	}
	
	void ComputeRateTable(double [][] Beta){
		// fill rate table without rescaling factor
		computeRateState(Beta);

		// find rescaling factor
		double max_val = -1;			// fill rate table without rescaling factor
		for (Triplet t : m_ValidTransitions){
			double val = GetRateTable(t);
			if (val > max_val){
				max_val= val;
			}
		}			
		//Macro.Debug(" found max val :" + max_val);
		// rescaling of the values in order not to overpass 1
		for (int pos=0 ; pos <  MAXPOS; pos++){
			m_Rate[pos] /= max_val;  
		}
		if(PRINTTABLE)
			RateTableToString().Print();
	}

	private double GetRateTable(Triplet t) {
		return m_Rate[t.ToPos()];
	}

	final private int TripletToPos(int st, int n1, int nB) {
		return nB + n1 * MAXNB + st * MAXNB * MAXN1;
	}

	// from pos to states
	class Triplet {

		final private int m_st, m_n1, m_nB;
		final private int m_n2; // additional info
		
		public Triplet(int in_st, int n1, int nB) {
			m_st= in_st;
			m_n1= n1;
			m_nB= nB;
			m_n2= NEIGHBSIZE - m_n1- m_nB; 
		}
		public int ToPos() {
			return m_nB + m_n1 * MAXNB + m_st * MAXNB * MAXN1;
		}

		public String toString(){
			return String.format(" state:%d  n1:%d  n2:%d nB:%d", m_st, m_n1, m_n2, m_nB);
		}

	}
	
	
	/** effectively computes the exchange rates **/
	final private double Exp(double val) {
		return Math.exp(val);
	}

	/** state , number of ones , number of boundary cells **/
	final private double ComputeExp(double [][] Beta, Triplet t) {
		// formula computation
		double expo= 
			t.m_n1 * Beta[t.m_st][1] + 
			t.m_n2 * Beta[t.m_st][2] + 
			t.m_nB * Beta[t.m_st][0];  
		return expo;
	}

	private FLStringList RateTableToString(){
		FLStringList out = new FLStringList();
		for (Triplet t : m_ValidTransitions ){
			String line= t + 
			String.format("> rate:%f exp %.2f ", 
					GetRateTable(t.m_st, t.m_n1, t.m_nB), GetExponent(t.m_st, t.m_n1, t.m_nB));
			out.Add(line);		
		}
		return out;		
	}

	
	
}
