package models.IPSmodels;

import architecture.interactingParticleSystems.IPSViewerRegular;
import components.types.IntC;
import grafix.gfxTypes.elements.FLPanel;
import initializers.IPSinit.IPSinitRegularGrid.CODE_IPS_INIT;
import main.Macro;

/*-------------------------------------------------------------------------------
- 
- @author Nazim Fates
------------------------------------------------------------------------------*/
final public class SimpleRandomWalkIPSmodel extends IPSmodel {

	final static String CLASSNAME= Thread.currentThread().getStackTrace()[1].getClassName();
	public static String NAME="SimpleRW";

	/*----------------------------------------------------------------------------
	  - attributes
	  --------------------------------------------------------------------------*/

	/*---------------------------------------------------------------------------
	  - construction
	  --------------------------------------------------------------------------*/

	public SimpleRandomWalkIPSmodel() {
		super();
	}


	/*---------------------------------------------------------------------------
	  - actions
	  --------------------------------------------------------------------------*/

	/** main function **/
	@Override
	final public boolean IsChangeApplied(int pos1, int pos2) {

		// we check exchange only if the cells are different 
		int state1= GetCellState(pos1),state2= GetCellState(pos2);  
		if (state1 == state2){
			return false;
		} else {
			return true;//RandomEventDouble( rate );
		}
	}

	/*---------------------------------------------------------------------------
	  - implementations & overrides
	  --------------------------------------------------------------------------*/

	@Override
	public CODE_IPS_INIT GetInitializerCode() {
		return CODE_IPS_INIT.Particles;
	}

	@Override
	public FLPanel GetViewControl() {
		FLPanel p = new FLPanel();
		return p;
	}
	/*---------------------------------------------------------------------------
	  - get & set
	  --------------------------------------------------------------------------*/

	/** default associated viewer **/
	public IPSViewerRegular GetViewer(IntC cellPixSize, IntC XYsize) {
		IPSViewerRegular viewer= IPSmodel.GetDefaultViewer(cellPixSize, XYsize);
		return viewer;
	}

	/*---------------------------------------------------------------------------
	  - test
	  --------------------------------------------------------------------------*/

	public static void DoTest(){
		Macro.BeginTest(CLASSNAME);
		Macro.EndTest();
	}

}
