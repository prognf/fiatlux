package models.IPSmodels;

import architecture.interactingParticleSystems.IPSViewerRegular;
import components.types.DoublePar;
import components.types.IntC;
import components.types.IntegerList;
import grafix.gfxTypes.elements.FLPanel;
import initializers.IPSinit.IPSinitRegularGrid.CODE_IPS_INIT;
import main.Macro;
import topology.basics.PlanarTopology;


final public class SultraWeavingIPSmodel extends IPSmodel {

	final static String CLASSNAME= Thread.currentThread().getStackTrace()[1].getClassName();
	public static String NAME="SultraWeaving";
	static final int NSTATES = 675; 	
	final static double DEF_RATE=0.01;

	/*--------------------
	 * attributes
	 --------------------*/

	private DoublePar m_exchRate= new DoublePar("Exchg. Rate", DEF_RATE);

	/*--------------------
	 * construction
	 --------------------*/

	/** IPS model should be given the perception topology **/
	public SultraWeavingIPSmodel(PlanarTopology topoP) {
		super();		
	}

	@Override
	public void ProcessClickOnCell(int cellPos){
	//	double localRate= GetCellExchangeRate(cellPos);
//		String msg = String.format(" pos: %d rate:%f ", cellPos, localRate) +
		String msg= GetCellInfo(cellPos);
		Macro.print(msg);
	}

	/*--------------------
	 * settings 
	 --------------------*/

	

	/*--------------------
	 * get & set
	 --------------------*/


	@Override
	public CODE_IPS_INIT GetInitializerCode() {
		return CODE_IPS_INIT.Sultra;
	}

	@Override
	public FLPanel GetViewControl() {
		return FLPanel.NewPanel(m_exchRate.GetControl(), GetRandomizer());
	}

	

	/*--------------------
	 * actions
	 --------------------*/

	
	/** main function 
	 * **/
	@Override
	final public boolean IsChangeApplied(int pos1, int pos2) {

		// we check exchange only if the cells are different 
		int state1= GetCellState(pos1),state2= GetCellState(pos2);  
		if (state1 == state2){
			return false;
		} else {
			try {
				return RandomEventDouble( m_exchRate.GetVal() );
			} catch (Exception e){
				Macro.SystemWarning(e, "value of the exchg. rate : "  + m_exchRate.GetVal());
				return false;
			}
		}
	}

	public String GetCellInfo(int pos){
		int state= GetCellState(pos);
		IntegerList neighb= GetNeighbGraph(pos); 
		int [] count = new int[NSTATES];
		for (int posN : neighb){
			count[ GetCellState(posN) ]++; 
		}
		String msg= 
			String.format(" st:%d n1:%d nB:%d", state, count[1], count[0]);
		msg += " neighb> " + neighb ;
		return msg;
	}

	


	/*--------------------
	 * Get Set
	 --------------------*/


	/*--------------------
	 * GFX
	 --------------------*/

	
	/** default associated viewer **/
	public IPSViewerRegular GetViewer(IntC cellPixSize, IntC XYsize) {
		return new SultraViewer(cellPixSize, XYsize);
	}


	

	/*--------------------
	 * test
	 --------------------*/
	public static void DoTest(){
		Macro.BeginTest(CLASSNAME);
		Macro.EndTest();
	}
}
