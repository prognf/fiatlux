package models.IPSmodels;

import architecture.interactingParticleSystems.IPSViewerRegular;
import components.types.IntC;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.elements.FLActionButton;
import grafix.gfxTypes.elements.FLPanel;

public class SultraViewer extends IPSViewerRegular {

	private static final int NCOLORS = SultraWeavingIPSmodel.NSTATES;
	private static final PaintToolKit RNDPALETTE = 
			PaintToolKit.GetRandomPalette(NCOLORS);	
	private static PaintToolKit CHKBPALETTE ; 
			
	boolean m_HiHa;
	
	public SultraViewer(IntC cellPixSize, IntC xYsize) {
		super(cellPixSize, xYsize);
		SetPalette(RNDPALETTE);
		// special palette for analysis
		CHKBPALETTE= new PaintToolKit(NCOLORS);
		for (int col=0; col < NCOLORS; col++){
			int bitcol = (col / 25 ) * 5 + col % 5;
			if (bitcol%2==0){
				CHKBPALETTE.SetColor(col, FLColor.c_white);
			} else {
				CHKBPALETTE.SetColor(col, FLColor.c_blue);
			}
		}
		
	}
	
	
	//@override
	public FLPanel GetSpecificPanel(){
		FLPanel p= new FLPanel();
		p.Add(new ChangeView());
		return p;
	}
	
	class ChangeView extends FLActionButton {

		public ChangeView() {
			super("hi ha !");
		}

		@Override
		// HACK HERE
		public void DoAction() {			 
			m_HiHa = ! m_HiHa;
			SetPalette(m_HiHa?CHKBPALETTE:RNDPALETTE);
			UpdateView();
		}
		
	}
	
}
