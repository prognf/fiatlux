package models.IPSmodels;

import architecture.interactingParticleSystems.IPSViewerRegular;
import components.types.FLString;
import components.types.IntC;
import components.types.IntegerList;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.controllers.MultiReadOneDisplayControl;
import grafix.gfxTypes.elements.FLPanel;
import initializers.IPSinit.IPSinitRegularGrid.CODE_IPS_INIT;
import main.Macro;
import topology.basics.PlanarTopology;


final public class CellSortingIPSmodel extends IPSmodel {

	final static String CLASSNAME= Thread.currentThread().getStackTrace()[1].getClassName();
	public static String NAME="CellSorting";
	static final int NSTATES = 3; 	
	private static final double MULT = 100;
	private static final double BETA_11 = 3, BETA_12_21= 1, BETA_22= 2, BETA_1B=5, BETA_2B=3;


	/*--------------------
	 * attributes
	 --------------------*/


	private double[][] m_Beta; // adhesion matrix

	private RateDataTable m_RateTable;

	/*--------------------
	 * construction
	 --------------------*/

	/** IPS model should be given the perception topology **/
	public CellSortingIPSmodel(PlanarTopology topoP) {
		super();
		m_Beta= new double[NSTATES][NSTATES];

		int NEIGHBSIZE= topoP.GetActualNeighbMaxSize();
		//Macro.Debug(" neighbSize:" + NEIGHBSIZE);
		m_RateTable= new RateDataTable(NEIGHBSIZE);

		SetBetaFromDefaultValues(1.);
	}

	public void SetPerceptionTopology(){

	}

	@Override
	public void ProcessClickOnCell(int cellPos){
		double localRate= GetCellExchangeRate(cellPos);
		String msg = String.format(" pos: %d rate:%f ", cellPos, localRate) +
		GetCellInfo(cellPos);
		Macro.print(msg);
	}

	/*--------------------
	 * settings 
	 --------------------*/

	public void SetBetaBounds(double beta1b, double beta2b) {
		SetBeta(1,0, beta1b);
		SetBeta(2,0, beta2b);
		UpdateRateTable();
	}

	private void SetBetaFromDefaultValues(double alpha) {
		SetBeta(1,1, alpha * BETA_11);
		SetBeta(1,2, alpha * BETA_12_21); 
		SetBeta(2,1, m_Beta[1][2]);
		SetBeta(2,2, alpha * BETA_22);

		SetBeta(1,0, alpha * BETA_1B);
		SetBeta(2,0, alpha * BETA_2B);	
		UpdateRateTable();
	}



	final private void SetBeta(int i, int j, double val) {
		m_Beta[i][j]= val;
	}

	final private double GetBeta(int i, int j) {
		return m_Beta[i][j];
	}



	/*--------------------
	 * get & set
	 --------------------*/


	@Override
	public CODE_IPS_INIT GetInitializerCode() {
		return CODE_IPS_INIT.CellSorting;
	}

	@Override
	public FLPanel GetViewControl() {
		BetaValControl viewControl= new BetaValControl();
		viewControl.Update();
		FLPanel p = new FLPanel();
		p.SetGridBagLayout();
		p.AddGridBagY(viewControl, GetRandomizer().GetSeedControl());
		return p;
	}


	/*--------------------
	 * actions
	 --------------------*/

	private void UpdateRateTable() {
		m_RateTable.ComputeRateTable(m_Beta);			
	}

	/** main function 
	 * **/
	@Override
	final public boolean IsChangeApplied(int pos1, int pos2) {

		// we check exchange only if the cells are different 
		int state1= GetCellState(pos1),state2= GetCellState(pos2);  
		if (state1 == state2){
			return false;
		} else {
			double rate1= ComputeLocalRate(state1, pos1);
			double rate2= ComputeLocalRate(state2, pos2);
			double rate= rate1 * rate2;
			//Macro.DebugNOCR(" | " + rate);
			return RandomEventDouble( rate );
		}
	}

	/** input: state of central cell and position ; 
	 *  output : transition rate 
	 *  the use of param. state is to "save" calls **/
	private double ComputeLocalRate(int state, int cellPos) {
		// building neighbourhood state
		IntegerList neighb= GetNeighbGraph(cellPos); 
		int [] count = new int[NSTATES];
		for (int posN : neighb){
			count[ GetCellState(posN) ]++; 
		}
		try{
			// do we realise the exchange ?
			double exchRate= m_RateTable.GetRateTable(state, count[1], count[0]);
			return exchRate;
		} catch (Exception e) {
			Macro.FatalError(e, " problem when evaluating cell pos: " + cellPos + GetCellInfo(cellPos) );
			return 0;
		}

	}

	public String GetCellInfo(int pos){
		int state= GetCellState(pos);
		IntegerList neighb= GetNeighbGraph(pos); 
		int [] count = new int[NSTATES];
		for (int posN : neighb){
			count[ GetCellState(posN) ]++; 
		}
		String msg= 
			String.format(" st:%d n1:%d nB:%d", state, count[1], count[0]);
		msg += " neighb> " + neighb ;
		return msg;
	}

	/** for external call only (to check computation) **/
	public double GetCellExchangeRate(int pos){
		int state= GetCellState(pos);
		double rate= ComputeLocalRate(state, pos);
		return rate;
	}


	/*--------------------
	 * Get Set
	 --------------------*/


	/*--------------------
	 * GFX
	 --------------------*/

	/** default associated viewer **/
	public IPSViewerRegular GetViewer(IntC cellPixSize, IntC XYsize) {
		IPSViewerRegular viewer= IPSmodel.GetDefaultViewer(cellPixSize, XYsize);
		FLColor [] cols= {FLColor.c_brown, FLColor.c_darkgreen, FLColor.c_yellow}; 
		PaintToolKit palette = new PaintToolKit(cols);
		viewer.SetPalette(palette);
		return viewer;
	}

	final private String [] S_BETA= { "B11", "B12", "B22", "B1b", "B2b" };

	class BetaValControl extends MultiReadOneDisplayControl {

		public BetaValControl() {
			super(S_BETA, 3, 5);
			Display();
		}

		@Override
		public String Display() {
			return "Inner [" + GetBeta(1,1) + "," + GetBeta(1,2) + "," + GetBeta(2,2) + "]  Bound [" + GetBeta(1, 0) + "," +  GetBeta(2,0) + "]" ;
		}

		@Override
		public void ReadInput(int argpos, String s_input) {
			int i,j;
			double val= FLString.ParseDouble(s_input);
			switch (argpos){
			case 0: 
				SetBeta(1,1, val); 
				break;
			case 1:
				SetBeta(1,2, val);
				SetBeta(2,1, val);
				break;
			case 2:
				SetBeta(2,2, val);
				break;
			case 3:
				SetBeta(1,0, val);
				break;
			case 4:
				SetBeta(2,0, val);
				break;

			default:
				Macro.FatalError(" bad arg");
			}
			UpdateRateTable();

		}

	}


	/* sets alpha as a multiplicating factor for default values */
	public void SetBetaValWithAlpha(double alpha) {
		SetBetaFromDefaultValues(alpha);
	}



	/*--------------------
	 * test
	 --------------------*/
	public static void DoTest(){
		Macro.BeginTest(CLASSNAME);
		Macro.EndTest();
	}
}
