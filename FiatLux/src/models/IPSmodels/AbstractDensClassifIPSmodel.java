package models.IPSmodels;

import architecture.interactingParticleSystems.IPSViewerRegular;
import components.types.DoublePar;
import components.types.IntC;
import grafix.gfxTypes.elements.FLPanel;
import initializers.IPSinit.IPSinitRegularGrid.CODE_IPS_INIT;

abstract public class AbstractDensClassifIPSmodel extends IPSmodel {

	protected final DoublePar m_NoiseRate= new DoublePar("Noise prob.", 0.);
	protected final DoublePar m_breakRate= new DoublePar("break rate", 0.);

	@Override
	public CODE_IPS_INIT GetInitializerCode() {
		return CODE_IPS_INIT.Bernoulli;
		//return CODE_IPS_INIT.Particles;
	}

	@Override
	public FLPanel GetViewControl() {
		FLPanel p= new FLPanel();
		p.Add(m_NoiseRate.GetControl(), m_breakRate.GetControl());
		p.AddRand(GetRandomizer());
		return p;
	}


	public void SetNoiseRate(double noiseRate){
		m_NoiseRate.SetVal(noiseRate);
	}


	boolean IsNoiseApplied() {
		return RandomEventDouble(m_NoiseRate.GetVal());
	}

	/** non conservation of particles **/
	public boolean IsModelNonConservative() {
		return true;
	}


	/*--------------------
	 * GFX
	 --------------------*/
	@Override
	public IPSViewerRegular GetViewer(IntC cellPixSize, IntC XYsize) {
		IPSViewerRegular viewer= IPSmodel.GetDefaultViewer(cellPixSize, XYsize);
		//FLColor [] cols= {FLColor.c_brown, FLColor.c_darkgreen, FLColor.c_yellow}; 
		//PaintToolKit palette = new PaintToolKit(cols);
		//viewer.SetPalette(palette);
		return viewer;
	}

	


}
