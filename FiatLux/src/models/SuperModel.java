package models;

import java.lang.reflect.Field;

import components.randomNumbers.FLRandomGenerator;
import components.randomNumbers.StochasticSource;
import components.types.FLsignal;
import components.types.Monitor;

/**a model needs to receive signals
for instance for resetting the randomizer**/

abstract public class SuperModel extends StochasticSource
implements Monitor 
{
	/**
	 * the key is a static identifier proper to each model
	 * it is found in the MANDATORY field "NAME"
	 *  */
	public String GetKey(){
		Class<? extends SuperModel> oneClass= this.getClass();
		try{
			Field f =  oneClass.getField("NAME");
			String nameRead= (String) f.get(String.class);
			//Macro.Debug("user readable key name : " + nameRead);
			return nameRead;
		} catch (Exception e) {
			return null;
		}

	}

	/** override if more info (such as ruleNum */
	public String GetName() {
		return GetKey();
	}

	protected SuperModel(){
		super();
	}

	/*---------------------------------------------------------------------------
	  - signals
	  --------------------------------------------------------------------------*/
	
	@Override
	public void ReceiveSignal(FLsignal sig){
		switch(sig){
		case init:
			sig_Init();
			break;
		case update:
			sig_Update();
			break;
		case nextStep:
			sig_NextStep();
			break;
		}
	}
	
	final public void sig_Init() {
		sig_RandomizerInit();
	}
	
	/** override if needed */
	public void sig_NextStep() {}

	/** override if needed */
	public void sig_Update() {}

	
	/** called by sig_Init **/	
	protected void sig_RandomizerInit() {
		//Macro.Debug("sig. rnd. init");
		FLRandomGenerator rand = GetRandomizer();
		if (rand  !=  null){
			rand.sig_ResetRandomizer();
		}		
	}


}
