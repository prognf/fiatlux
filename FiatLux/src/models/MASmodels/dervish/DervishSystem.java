package models.MASmodels.dervish; 
import components.arrays.GridSystem;
import components.types.FLsignal;
import components.types.IntC;
import components.types.Monitor;
import grafix.viewers.GridViewer;
import main.Macro;
/*******************************************************
 * simple multi-state turmite
 ********************************************************/ 
 public class DervishSystem extends GridSystem implements Monitor {  
		
	 final static String MODELNAME="DervishSystem";

	private static final int NSTATE = 3;
	
	/*--------------------
	 * Attributes
	 --------------------*/
	
	 protected int[][] m_array; // cells
	 protected int[][] m_deltaState; // changes of the environment
	 protected DervishAgent m_agent;
	/*--------------------
	 * construction
	--------------------*/
	
	public DervishSystem(IntC in_size){
		super(in_size);
		initArray();
		m_agent= new DervishAgent(this);
	}
	
	
	/** inits & clears the field */
	public void initArray() {
		Macro.PrintInitSignal(4, this);
		int Xsize= GetXsize(), Ysize= GetYsize();
		m_array = new int[Xsize][Ysize];
		m_deltaState = new int[Xsize][Ysize];
		for (int x = 0; x < Xsize; x++) {
			for (int y = 0; y < Ysize; y++) {
				m_array[x][y] = 0;
				m_deltaState[x][y] = 0;
			}
		}
	}

	/*--------------------
	 * main
	--------------------*/
			
	/*--------------------
	 * signals (dynamics)
	 --------------------*/
	
	@Override
	public void ReceiveSignal(FLsignal sig){
		//Macro.print("DervishSystem signal received :"+sig);
		switch(sig){
		case init:
			sig_Init();
			break;
		case update:
			sig_Update();
			break;
		case nextStep:
			sig_NextStep();
			break;
		}
	}
	
	/** taking influences into account to change the state of the cells 
	 * change to non-final if needed... */
	final protected void UpdateEnvironment() {
		int X= GetXsize(), Y= GetYsize();
		for (int y=0; y< Y; y++){
			for (int x=0; x< X ; x++){
				m_array[x][y]+=m_deltaState[x][y];
				m_deltaState[x][y]=0;
			}
		}
	}
	
	/** initialisation */
	public void sig_Init(){
		super.sig_Init(); // resets the time
		ResetEnvironment();
	}

	/** Main Method  **/
	public void sig_NextStep(){
		TimeClick();
		m_agent.calculateNextStep();
		m_agent.sig_InternalUpdate();
		// Environment Updating
		UpdateEnvironment();
	}

	
	
	/*--------------------
	 * override
	--------------------*/
	
	
	/*--------------------
	 * Get / Set
	--------------------*/
	
	private void ResetEnvironment() {
		//Macro.Debug("Reset Env");
		for (int x=0; x<GetXsize(); x++){
			for (int y=0; y<GetYsize(); y++){
				m_array[x][y]=0;
				m_deltaState[x][y]=0;
			}
		}		
	}
	
	public GridViewer GetSystemViewer(IntC CellSizeForViewer) {
		return new DervishViewer(this, CellSizeForViewer);
	}
	
	/* TODO  : hack */
	protected DervishAgent GetDervishAgent(int agentnum){
		return m_agent;
	}	

	public IntC GetXYsize() {
		return m_XYsize;
	}

	public int GetXsize() {
		return m_XYsize.X();
	}

	public int GetYsize() {
		return m_XYsize.Y();
	}


	public int GetCellState(int x, int y) {
		return Math.floorMod(m_array[x][y], NSTATE);
	}


	public void SetCellState(int x, int y, int state) {
		m_array[x][y]= state;
	}


	public DervishInitializer GetNewInitializer() {
		return new DervishInitializer(this);
	}


	public String GetModelName() {
		return MODELNAME;
	}


	public void setCellChange(int x, int y, int delta) {
		m_deltaState[x][y]= delta;
	}

}
