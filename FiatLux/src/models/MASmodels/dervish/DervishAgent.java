package models.MASmodels.dervish;

import components.types.IntC;

public class DervishAgent  {

	/*--------------------
	 * attributes
	 --------------------*/

	DervishSystem m_system;

	int m_x,m_y;
	protected int m_dir; // internal state : orientation (int, no modulo)
	protected int m_deltaDir = 0; // this is for truning
	

	/*--------------------
	 * construction
	--------------------*/

	public DervishAgent(DervishSystem system) {
		m_system=system;
	}


	/*--------------------
	 * reset
	--------------------*/

	public void sig_AgentReset() {
		m_dir=0;
		m_deltaDir= 0;
	}

	/*--------------------
	 * influences
	--------------------*/

	/* desired orientation at next step */
	public void calculateNextStep(){		
		int state= m_system.GetCellState(m_x, m_y);
		switch(state) {
			case 0:
				m_deltaDir=1;
				break;
			case 1:
				m_deltaDir=3;
				break;
			case 2:
				m_deltaDir=0;
				break;
		}
		m_system.setCellChange(m_x,m_y,1);
	}
	/*--------------------
	 * update
	--------------------*/

	final static int [] DX = {0,1,0,-1};
	final static int [] DY = {1,0,-1,0};
	
	/* empty */
	public void sig_InternalUpdate() {
		m_dir += m_deltaDir;
		int dirint= Math.floorMod(m_dir + m_deltaDir, 4); // 
		m_x += DX[dirint];
		m_y += DY[dirint];
		m_x=(m_x + m_system.GetXsize() ) % m_system.GetXsize();
		m_y=(m_y + m_system.GetYsize() ) % m_system.GetYsize();
	}

	public int GetOrientation() {
		return  Math.floorMod(m_dir, 4); 
	}
	
	public void SetOrientation(int dir) {
		m_dir= dir;
	}


	public int GetPositionX() {
		return m_x;
	}

	public int GetPositionY() {
		return m_y;
	}


	public void setPosition(IntC pos) {
		m_x= pos.X();
		m_y=pos.Y();
	}
}
