package models.MASmodels.dervish;

import components.types.IntC;
import components.types.Monitor;
import experiment.samplers.SuperSampler;
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.AutomatonViewer;
import grafix.viewers.GridViewer;
import initializers.CAinit.InitPanel;
import main.Macro;

public class DervishSampler extends SuperSampler {
	
	private DervishSystem m_system ;
	GridViewer m_viewer;
	DervishInitializer m_initializer;

	public DervishSampler(DervishSystem in_system, IntC CellSizeForViewer) {
		m_system = in_system;
		m_viewer= m_system.GetSystemViewer(CellSizeForViewer);
		this.addMonitor(m_viewer);
		m_initializer= m_system.GetNewInitializer();
		if (m_initializer != null){
			Macro.print(4, "Initializer added");
		} else {
			Macro.print(4, "No Initializer was found !!!");
		}
		addMonitor(m_initializer);
	}
	
		
	/*--------------------
	 * signals
	 --------------------*/
		
	@Override
	public FLPanel GetSimulationWidgets() {
		InitPanel ip = new InitPanel(this, m_initializer.GetSpecificPanel());
		ip.adaptSize();
		return FLPanel.NewPanel(ip);
	}

	@Override
	public FLPanel GetSimulationRuleControls() {
		return null;//m_system.GetViewControl();
	}

	@Override
	public FLPanel GetSimulationView() {
		return FLPanel.NewPanel(m_viewer);
	}

	@Override
	public AutomatonViewer GetViewer() {
		return m_viewer;
	}

	/*--------------------
	 * implementations
	 --------------------*/
	
	@Override
	public String GetSimulationInfo() {
		return "";//m_system.GetSimulationInfo();
	}

	@Override
	public int GetTime() {
		return m_system.GetTime();
	}

	@Override
	public String [] GetPlotterSelection() {
		return null;
	}

	/*--------------------
	 * get / set
	 --------------------*/
	
	@Override
	protected String GetModelName() {
		return m_system.GetModelName();
	}


	@Override
	protected Monitor GetLinkedSystemAsMonitor() {
		return m_system;
	}


	@Override
	public String GetTopologyInfo() {
		return "2D:"+m_system.GetXsize()+","+m_system.GetYsize();//TODO
	}

	
}
