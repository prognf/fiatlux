package models.MASmodels.dervish;

import components.arrays.SuperSystem;
import components.types.IntC;
import grafix.gfxTypes.IntField;
import grafix.gfxTypes.elements.FLPanel;
import initializers.SuperInitializer;
import topology.basics.SuperTopology;

public class DervishInitializer extends SuperInitializer {


	final static String TXT_NTURMITES= "N particules";
	final static int DEF_NTURMITES= 1;


	final static String S_RANDOM="random";
	final static int RANDOM=0, SQUARE=1, DEF_SELECT=0;

	/*--------------------
	 * attributes
	 *************************************************************************/

	DervishSystem m_system;
	IntField mF_Nturmites= new IntField(TXT_NTURMITES, 3, DEF_NTURMITES);

	/*--------------------
	 * construction
	 --------------------*/
	public DervishInitializer(DervishSystem in_system) {
		m_system= in_system;
	}


	/*--------------------
	 * main
	 --------------------*/

	public void SubInit() {
		m_system.sig_Init();
		init();
	}




	/* nothing to do (see constructor ) */
	private void init() {
		int nagent= 1;//mF_Nturmites.GetValue();
		for (int agentnum=0; agentnum< nagent; agentnum++){
			//IntC pos = GetRandomizer().RandomIntCouple(m_system.GetXsize(),m_system.GetYsize() );
			IntC pos = new IntC(m_system.GetXsize()/3,m_system.GetYsize()/2 );
			m_system.GetDervishAgent(agentnum).setPosition(pos);
		}

	}


	/*--------------------
	 * get / set
	 --------------------*/
	public FLPanel GetSpecificPanel() {
		FLPanel panel = FLPanel.NewPanelVertical(mF_Nturmites);
		panel.setOpaque(false);
		return panel;
	}


	@Override
	public void LinkTo(SuperSystem system, SuperTopology topologyManager) {
		// TODO Auto-generated method stub
		
	}


	@Override
	protected void PreInit() {
		
	}


}
