package models.MASmodels.dervish;

import java.awt.event.MouseEvent;

import architecture.multiAgent.tools.DIR;
import components.types.IntC;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.viewers.GridViewer;
import grafix.viewers.MouseInteractive;
import main.Macro;

public class DervishViewer extends GridViewer 
implements MouseInteractive
{

	final static int NCOLAGENT=4;
	final static protected FLColor[] // color of cells
	                  PaletteCell = {FLColor.c_white, FLColor.c_blue, FLColor.c_lightpurple};
	final static protected FLColor[] // colors of the agents
	                  PaletteAgent = { FLColor.c_brown, FLColor.c_superRed, FLColor.c_darkgreen, FLColor.c_red };

    DervishSystem m_system;

	public DervishViewer(DervishSystem system, IntC pixCellSize) {
		super(pixCellSize, system.GetXYsize()); //inversion ?
		// CAST
		try{
			m_system= system;
			Macro.print(5," Viewer linked to system");
		} catch (Exception e){
			Macro.FatalError("Turmite Viewer can only be used with TurmiteSystem");
		}

		SetPalette(new PaintToolKit(PaletteCell));
		// mouse becomes active
		ActivateMouseFollower(this);
		
	}

	@Override
	protected void DrawSystemState() {
		// ENVIRONMENT
		for (int x = 0; x < getXsize(); x++){
			for (int y = 0; y < getYsize(); y++) {
				int state = m_system.GetCellState(x,y); 
				DrawSquareColor(x, y,state);
			}
		}
		// AGENTS
		int n = 1;// m_system.GetAgentNumber();
		
		for (int agent = 0; agent < n; agent++) {
			DIR orientation = 
				DIR.FromInt( m_system.GetDervishAgent(agent).GetOrientation() );

			int x = m_system.GetDervishAgent(agent).GetPositionX();
			int y = m_system.GetDervishAgent(agent).GetPositionY();
			
			int icol= agent % NCOLAGENT;
			SetColor(PaletteAgent[icol]);
			DrawCellTriangle(x, y, orientation);
		}

	}

	@Override
	public void ProcessMouseEventXY(int x, int y, MouseEvent event) {
		//Macro.Debug( "hi ha clic ");
		int st= m_system.GetCellState(x, y) % 2;
		m_system.SetCellState(x, y, st+1);		
		this.UpdateView();
	}

	
}

