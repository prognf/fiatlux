package models.MASmodels.IRMASmodels;

import architecture.basisIRMAS.IRMAsystem;
import architecture.basisIRMAS.IRagent;
import components.types.IntC;

public class ErraticAgent extends IRagent {

	private static final int RANDOMMOVE = 1;
	
	public ErraticAgent(IRMAsystem system, int index) {
		super(system, index);
	}

	/** side effect : the environment should set m_target */
	public void sig_PutInfluence() {
		if (/*(!m_free) &&*/ m_rand.RandomEventPercent(RANDOMMOVE)){
			SelectRandomDir();
		}
		// the agent puts an influence on the cell where it is located
		m_system.PutLocalInfluence(this);

		// the agent puts an influence where it wants to go
		// callback : m_target is modified
		IntC dxy= m_dir.Offset();
		m_system.PutMoveInfluenceWithOffset(this, dxy);
	}

	public void sig_MakeReaction() {
		//Macro.Debug("agent " + m_index + " LOCAL " + 
		//m_inflLocal.IsValid() + ":" + m_inflNeighb.IsValid());
		
		 // conflict with influences ?
		boolean free= m_inflLocal.IsValid() && m_inflMove.IsValid();
		if (free){	//move accepted
			
			//m_xypos.SetVal(m_xytarget);	

			// semi-hack exchange of target position and actual position
			//Macro.Debug(" before exchg: " + m_xypos.ToString() + " >>>] " + m_xytarget.toString());
			
			IntC temp= m_xypos;
			m_xypos= m_xytarget;
			m_xytarget= temp.DuplicateM();
			
			//Macro.Debug(" after exchg: " + m_xypos.ToString() + " >>>] " + m_xytarget.toString());
			//m_xytarget.SetVal(99,999);
			//Macro.Debug(" after noise: " + m_xypos.ToString() + " >>>] " + m_xytarget.toString());
			
		} 

	}

	
	
}
