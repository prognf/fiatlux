package models.MASmodels.IRMASmodels;

import java.util.ArrayList;
import java.util.Map.Entry;

import architecture.basisIRMAS.IRMAsystem;
import architecture.basisIRMAS.IRagent;
import architecture.basisIRMAS.Influence;
import architecture.basisIRMAS.Influence.REACTION;
import components.types.FLString;
import components.types.IntC;
import grafix.viewers.GridViewer;
import main.Macro;

public class ErraticJammingSystem extends IRMAsystem {

	private static final boolean PRN = false; // for debug

	public ErraticJammingSystem(IntC XYsize, int Nagent) {
		super(XYsize, Nagent);
	}
	
	public void CalculateInfluences(){
		ResetInfluenceArray();
		for (IRagent agent : GetAgentList()){
			agent.sig_PutInfluence();
		}
	}

	
	public void MakeReaction(){
		// Macro.Debug("sig Reac !!");
		// setting responses in each influence
		for (Entry<Integer, ArrayList<Influence>> val : m_inflRegister.entrySet()){
			ArrayList<Influence> listofInfl = val.getValue();
			if (PRN){
				Macro.print(
					" cell : " + val.getKey() 
				+ " ->>>" + FLString.ToStringNoSep(listofInfl) );
			}
			
			Influence infl= listofInfl.get(0);
			
			int numberOcc=listofInfl.size();
			if (numberOcc==1){ // no conflict
				infl.SetStatus(REACTION.valid);
				// ValidateAgentInfluence(agentIndex);
			} else { // conflict
				for (Influence inflNonValid : listofInfl){
					inflNonValid.SetStatus(REACTION.nonvalid);
				}
			}
		}
		// transmitting reaction signal
		for (IRagent agent : super.GetAgentList()){
			agent.sig_MakeReaction();		
		}
	}

	@Override
	protected IRagent GetNewAgent(IRMAsystem mAsystem, int index) {
		return new ErraticAgent(this,index);
	}

	@Override
	public GridViewer GetViewer(IntC cellSizeForViewer) {
		return new ErraticJammingViewer(this, cellSizeForViewer);
	}
	
}
