package models.MASmodels.IRMASmodels;

import architecture.basisIRMAS.IRMAsviewer;
import architecture.basisIRMAS.IRMAsystem;
import architecture.basisIRMAS.IRagent;
import components.types.IntC;
import grafix.gfxTypes.FLColor;

public class NqueensViewer extends IRMAsviewer {

	final static int NCOLAGENT=5;
	static protected FLColor[] 
			PaletteAgent = 
		{ FLColor.c_white, FLColor.c_green, FLColor.c_orange, FLColor.c_red, FLColor.c_brown, FLColor.c_blue };

	public NqueensViewer(IRMAsystem in_system, IntC in_PixSize) {
		super(in_system, in_PixSize);
		SetPalette(PaletteAgent);
	}

	
	protected void DrawEnvironmentAgents() {
		FLColor col;
		col = GetColor(0);
		super.DrawUniformCellularBackGround(col);
		
		// AGENTS
		int n = m_IRsystem.GetAgentNumber();
		for (int agentIndex = 0; agentIndex < n; agentIndex++) {
			IRagent agent= m_IRsystem.GetAgent(agentIndex);
			int icol= ((NqueensAgent) agent).GetCaptures() + 1 ;
			if (icol>=NCOLAGENT){
				icol= NCOLAGENT - 1;
			}
			SetColorIndex(icol);
			int 	x= agent.GetPos().X(), 
					y= agent.GetPos().Y() ;
			DrawCellCircle(x, y);
		}
	}
	
}
