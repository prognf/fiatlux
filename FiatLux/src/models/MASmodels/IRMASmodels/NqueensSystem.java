package models.MASmodels.IRMASmodels;

import java.util.ArrayList;
import java.util.Map.Entry;

import architecture.basisIRMAS.IRMAsystem;
import architecture.basisIRMAS.IRagent;
import architecture.basisIRMAS.Influence;
import architecture.basisIRMAS.Influence.REACTION;
import components.types.FLString;
import components.types.IntC;
import grafix.gfxTypes.elements.FLActionButton;
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.GridViewer;
import main.Macro;

public class NqueensSystem extends IRMAsystem {

	private static final boolean PRN = false; // for debug

	public NqueensSystem(int Nagent) {
		super(new IntC(Nagent, Nagent), Nagent);
	}

	//@override
	public void sig_Init(){
		super.sig_Init();
		CalculateCaptures();
	}
	
	public void CalculateInfluences(){
		ResetInfluenceArray();
		//specific : global calculus of captures
		//CalculateCaptures();
		for (IRagent agent : GetAgentList()){
			agent.sig_PutInfluence();
		}
	}

	
	/** tells each agent if it is in capture with another one 
	 * TODO : suppress CAST**/
	public void CalculateCaptures() {
		
		for (IRagent agent : GetAgentList()){
			((NqueensAgent)agent).ResetCaptures();
		}
		for (int iagent=0; iagent < m_Nagent; iagent++){
			for (int jagent=iagent+1 ; jagent < m_Nagent; jagent++){
				NqueensAgent 
					agenti= (NqueensAgent)GetAgent(iagent),
					agentj= (NqueensAgent)GetAgent(jagent);
				if (InCapture(agenti,agentj)){
					agenti.IncrementCaptures();
					agentj.IncrementCaptures();
				}
			}
		}
	}

	private boolean InCapture(NqueensAgent agenti, NqueensAgent agentj) {
		IntC 
			ixy= agenti.GetPos(),
			jxy= agentj.GetPos();
		boolean capLC= IntC.SameLineOrCol(ixy,jxy);
		boolean capDIAG= IntC.SameDiag(ixy,jxy);
		//Macro.FormatDebug(" i:> %s j:> %s ", ixy, jxy );
		return capLC || capDIAG;
	}


	public void MakeReaction(){
		// Macro.Debug("sig Reac !!");
		// setting responses in each influence
		for (Entry<Integer, ArrayList<Influence>> val : m_inflRegister.entrySet()){
			ArrayList<Influence> listofInfl = val.getValue();
			if (PRN){
				Macro.print(
					" cell : " + val.getKey() 
				+ " ->>>" + FLString.ToStringNoSep(listofInfl) );
			}
			
			Influence infl= listofInfl.get(0);
			
			int numberOcc=listofInfl.size();
			if (numberOcc==1){ // no conflict
				infl.SetStatus(REACTION.valid);
				// ValidateAgentInfluence(agentIndex);
			} else { // conflict
				for (Influence inflNonValid : listofInfl){
					inflNonValid.SetStatus(REACTION.nonvalid);
				}
			}
		}
		// transmitting reaction signal
		for (IRagent agent : super.GetAgentList()){
				agent.sig_MakeReaction();		
		}
		// GLOBAL STEP ! 
		CalculateCaptures();
	}

	@Override
	protected IRagent GetNewAgent(IRMAsystem mAsystem, int index) {
		return new NqueensAgent(this,index);
	}


	@Override
	public GridViewer GetViewer(IntC cellSizeForViewer) {
		return new NqueensViewer(this, cellSizeForViewer);
	}

	@Override
	public
	FLPanel GetSimulationPanel(){
		FLPanel p= new FLPanel();
		p.Add( new Zorklux() );
		return p;
	}
	
	class Zorklux extends FLActionButton {

		public Zorklux() {
			super("sys. info");
		}

		@Override
		public void DoAction() {
			Macro.SeparationLine();
			Macro.print(" situation at time " + GetTime() );
			for (IRagent agent : GetAgentList()){
				NqueensAgent agentNQ= (NqueensAgent)agent;
				Macro.fPrint("%s  captures: %d ", agentNQ, agentNQ.GetCaptures() );
			}
			
			for (int iagent=0; iagent < m_Nagent; iagent++){
				for (int jagent=iagent+1 ; jagent < m_Nagent; jagent++){
					NqueensAgent 
						agenti= (NqueensAgent)GetAgent(iagent),
						agentj= (NqueensAgent)GetAgent(jagent);
					if (InCapture(agenti,agentj)){
						Macro.fPrint(" in capture: %s - %s ",agenti,agentj );
					}
				}
			}
			Macro.SeparationLine();
		}
		
	}
	
}
