package models.MASmodels.IRMASmodels;

import architecture.basisIRMAS.IRMAsviewer;
import architecture.basisIRMAS.IRMAsystem;
import architecture.basisIRMAS.IRagent;
import architecture.multiAgent.tools.DIR;
import components.types.IntC;
import grafix.gfxTypes.FLColor;

public class ErraticJammingViewer extends IRMAsviewer {

	final static int NCOLAGENT=4;
	final static protected FLColor[] 
			PaletteAgent = 
		{ FLColor.c_cyan, FLColor.c_superRed, FLColor.c_green, FLColor.c_red };
	private static final FLColor 
		BACKGROUNDCOL= FLColor.c_white, GRIDCOL = FLColor.c_grey1;
	
	public ErraticJammingViewer(IRMAsystem in_system, IntC in_PixSize) {
		super(in_system, in_PixSize);
		SetPalette(PaletteAgent);
	}

	public void DrawEnvironmentAgents() {
		DrawGrid(GRIDCOL, BACKGROUNDCOL);
		// AGENTS
		int n = m_IRsystem.GetAgentNumber();
		for (int agentIndex = 0; agentIndex < n; agentIndex++) {
			IRagent agent= m_IRsystem.GetAgent(agentIndex);
			int 	x= agent.GetPos().X(), 
					y= agent.GetPos().Y() ;
			int icol= agentIndex % NCOLAGENT;
			SetColorIndex(icol);
			DIR dir= agent.GetDir();
			DrawCellTriangle(x, y, dir);
		}
	}
	
}
