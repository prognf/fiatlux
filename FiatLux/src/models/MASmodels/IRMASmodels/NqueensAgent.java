package models.MASmodels.IRMASmodels;

import architecture.basisIRMAS.IRMAsystem;
import architecture.basisIRMAS.IRagent;
import components.types.IntC;
import components.types.IntCm;

public class NqueensAgent extends IRagent {

	int m_captures; // how many queens can capture this queen

	public NqueensAgent(IRMAsystem system, int index) {
		super(system, index);
	}

	/** side effect : the environment should set m_target */
	public void sig_PutInfluence() {
		
		boolean moving= 
				   ((m_captures==1) && m_rand.RandomEventPercent(1) )
				|| ((m_captures==2) && m_rand.RandomEventPercent(10) )
				|| ((m_captures==3) && m_rand.RandomEventPercent(60) )
				|| (m_captures>3) ;
				
		//Macro.Debug(" agent " + m_index + " capt: " + m_captures);
		if (moving) {
			SelectRandomDir();
			IntC dxy= m_dir.Offset();
			m_system.PutLocalInfluence(this);
			m_system.PutMoveInfluenceClosedBC(this, dxy);
		} else {
			m_system.PutLocalInfluence(this);
			m_system.PutMoveInfluence(this);
		}
	}

	public void sig_MakeReaction() {
		// conflict with influences ?
		boolean free= m_inflLocal.IsValid() && m_inflMove.IsValid();
		if (free){	//move accepted
			IntCm temp= m_xypos;
			m_xypos= m_xytarget;
			m_xytarget= temp; 
			//m_xypos.SetVal(m_xytarget);
		} 

	}

	public void ResetCaptures() {
		m_captures= 0;
	}

	public void IncrementCaptures() {
		m_captures++;
	}

	public int GetCaptures() {
		return m_captures;
	}

	public String toString(){
		return "A"+ m_index + ":" + m_xypos;
	}


}
