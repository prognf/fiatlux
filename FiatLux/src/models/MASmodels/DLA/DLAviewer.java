package models.MASmodels.DLA;

import architecture.multiAgent.tools.EnvCell;
import components.types.IntC;
import grafix.gfxTypes.FLColor;
import grafix.viewers.GridViewer;

/*--------------------
 * A TurmitesViewer enables the display of a Turmites automaton
 * 
 * @author Nazim Fates
*--------------------*/
public class DLAviewer extends GridViewer {

	/*--------------------
	 * // Attributes
	 --------------------*/

	DLAsystem m_system; // related system

	final static int NCOLORS=3, POS_BORDERCOL= 2, POS_AgentCol=1;

	/*--------------------
	 * // Constructor
	 --------------------*/
	public DLAviewer(DLAsystem in_system, IntC in_PixSize) {
		super(in_PixSize, in_system);
		m_system = in_system;
		// colors
		CreatePalette( 
				FLColor.c_white, FLColor.c_blue, FLColor.c_black);
	}

	/*--------------------
	 * Other methods
	 --------------------*/

	/* main component 
	 * X-axis: from left to right
	 * Y-axis :from TOP to BOTTOM !
	 * */
	@Override
	protected void DrawSystemState() {
		// cells
		DrawBackgroundCol0();
		SetColorIndex(POS_BORDERCOL);
		for (int x = 0; x < getXsize(); x++)
			for (int y = 0; y < getYsize(); y++) {
				int state = m_system.GetCellState(x, y); 
				if (state != 0) {
					DrawSquare(x, y);	
				}
			}

		// agents
		int n = m_system.GetAgentNumber();
		SetColorIndex(POS_AgentCol);
		for (int agentnum = 0; agentnum < n; agentnum++) {
			int state= m_system.GetInternalState(agentnum);
			if (state==1){ /* mobile particle */
				EnvCell cell = m_system.GetAgentPos(agentnum);
				int x = cell.GetX();
				// we DO NOT reverse  NORTH and SOUTH ! 
				int y = cell.GetY();
				DrawSquare(x, y);
			}
		}

	}

}
