package models.MASmodels.DLA;

import architecture.multiAgent.tools.EnvCell;
import architecture.multiAgent.tools.MultiAgentSystem;
import components.types.IntC;
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.GridViewer;
import initializers.MASinit.MultiAgentInitializer;

public class DLAsystem extends MultiAgentSystem {

	
	public final static int ST_STICK=1;

	
	//int m_nMobileAgents; // counter of mobile agents
	
	
	private static final String NAME = "DLA";
	
	public DLAsystem(IntC in_size) {
		super(in_size);
		//Macro.Debug("DLA creation");
	}


	@Override
	public String GetModelName() {
		return NAME;
	}

	@Override
	public EnvCell GetNewEnvCell(int x, int y) {
		return new DLAcell(x,y);
	}

	@Override
	public MultiAgentInitializer GetNewInitializer() {
		return new DLAinitializer(this);
	}

	@Override
	public FLPanel GetViewControl() {
		return null;
	}

	@Override
	public DLAparticle GetNewAgent(int agentnum) {
		return new DLAparticle(this);
	}


	@Override
	public GridViewer GetSystemViewer(IntC CellSizeForViewer) {
		return new DLAviewer(this, CellSizeForViewer);
	}
	
	/* creating agents */
	/*public void sig_Init(){
		m_nMobileAgents= n_agents;
	}
	
	public void sig_AgentStuck(){
		m_nMobileAgents--;
	}
	
	public int GetMobileAgentsNumber(){
		return m_nMobileAgents;
	}*/
	
	/** 
	 * called by the viewer 
	 * TODO : how not to use a cast ???
	 * */
	final public int GetInternalState(int agentnum){
		DLAparticle part= (DLAparticle) super.GetAgent(agentnum);
		return part.GetInternalState();
	}


	
	

}
