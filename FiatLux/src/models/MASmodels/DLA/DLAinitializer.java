package models.MASmodels.DLA;

import architecture.multiAgent.tools.MultiAgentSystem;
import components.types.IntC;
import components.types.IntegerList;
import grafix.gfxTypes.IntField;
import grafix.gfxTypes.elements.FLList;
import grafix.gfxTypes.elements.FLPanel;
import initializers.MASinit.MultiAgentInitializer;
import main.Macro;

public class DLAinitializer extends MultiAgentInitializer {
	final static String TXT_NTURMITES= "N particules", S_SQUARELEN= "square len";
	final static int DEF_NTURMITES= 2; // space
	
	
	final static String S_RANDOM="random", S_SQUARE="square", S_RNDSQ="rnd. sq.", S_DEBUG="dbg";
	final static int RANDOM=0, SQUARE=1, RND_SQ=2,  DEF_SELECT=1;
	final static String[] stratego= { S_RANDOM, S_SQUARE, S_RNDSQ};
	
	FLList strategy= new FLList(stratego);

	/*--------------------
	 * attributes
	*************************************************************************/

	MultiAgentSystem m_system;
	IntField mF_Nturmites= new IntField(TXT_NTURMITES, 3, DEF_NTURMITES);
	IntField mF_SquareLen= new IntField(S_SQUARELEN, 3);
	
	/*--------------------
	 * construction
	 --------------------*/
	public DLAinitializer(MultiAgentSystem in_system) {
		super(in_system);
		m_system= in_system;
		strategy.setSelectedIndex(DEF_SELECT);
		IntC dim= m_system.GetXYsize();
		mF_SquareLen.SetValue(dim.Min() / 4);
	}

	/*--------------------
	 * main
	 --------------------*/

	public void SubInit() {
		//DebugInit();
		int select= strategy.GetSelectedItemRank();
		m_system.InitArray();
		switch (select) {
		case RANDOM:
			RandomInit();
			break;
		case SQUARE:
			SquareInit(true);
			break;
		case RND_SQ:
			SquareInit(false);
			break;
		default:
			Macro.FatalError("case not taken into account");
		}
		super.MakeBorders(DLAsystem.ST_STICK);
	}


	private void DebugInit() {
		super.CreateAgents(1);
		IntC pos= new IntC(1,1);
		m_system.SetAgentPos(0, pos);
	}

	/* nothing to do (see constructor ) */
	private void RandomInit() {
		int nagent= mF_Nturmites.GetValue();
		super.CreateAgents(nagent);
		for (int agentnum=0; agentnum< nagent; agentnum++){
			IntC pos = GetRandomizer().RandomIntCouple(GetXsize(),GetYsize() );
			m_system.SetAgentPos(agentnum, pos);
		}
	
	}
	
	private void SquareInit(boolean ordered){
		int len= mF_SquareLen.GetValue();
		int nagent= len * len ;
		IntegerList order; // order for positioning the agents
		if (ordered){
			order = IntegerList.NintegersFromZero(nagent);
		} else {
			order = IntegerList.getKamongN(nagent, nagent, GetRandomizer());
		}
		//Macro.Debug("debug list");
		order.Print();
		super.CreateAgents(nagent);
		int x0 = (GetXsize() - len ) / 2,
			y0 = (GetYsize() - len ) / 2;

        int step=0;
		for (int y= 0; y < len; y++) {
			for (int x= 0; x < len; x++) {
				int xpos = (x0 + x) % GetXsize();
				int ypos = (y0 + y) % GetYsize();
				int agentnum= order.Get(step);
				m_system.SetAgentPos(agentnum, new IntC(xpos, ypos));
				step++;
			}			
		}
	} 

	/*--------------------
	 * get / set
	 --------------------*/
	public FLPanel GetSpecificPanel() {
		FLPanel panel = FLPanel.NewPanelVertical(strategy, mF_Nturmites, mF_SquareLen);
		panel.setOpaque(false);
		return panel;
	}


}
