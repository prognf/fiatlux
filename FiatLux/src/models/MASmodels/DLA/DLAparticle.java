package models.MASmodels.DLA;

import java.util.ArrayList;

import architecture.multiAgent.tools.DIR;
import architecture.multiAgent.tools.EnvCell;
import architecture.multiAgent.tools.INFLDIR;
import architecture.multiAgent.tools.SimpleMovingAgent;
import main.Macro;


public class DLAparticle extends SimpleMovingAgent {

	/*--------------------
	 * attributes
	 --------------------*/
	final private DLAsystem m_system; // for sending sig_agentStuckSystem
	
	protected INFLDIR m_infl = null; // DIR
	boolean m_int_free = true;  // free or stick state ?
	boolean m_infl_sticks = false;  // do we stick now ?

	/*--------------------
	 * construction
	--------------------*/

	public DLAparticle(DLAsystem in_system) {
		super(in_system);
		m_system= in_system;
	}

	/*--------------------
	 * reset
	--------------------*/

	/*--------------------
	 * decision
	--------------------*/


	private final boolean isThereNoStickStateAroundMe() {
		for (DIR dir: DIR.values()){
			EnvCell look= m_system.GetAgentNeighbourCellAt(this,  dir);
			int state= look.GetState();
			//Macro.DebugNOCR(":" + state);
			if (state == DLAsystem.ST_STICK){
				return false;
			}
		}
		return true;
	}


	public void sig_AgentPerceivePutInfluence(){		
		if (m_int_free){
			if (isThereNoStickStateAroundMe()){
				// perception
				ArrayList<DIR> availableNeighbs= super.GetFreeAvailableNeighbs(m_system);

				// decision
				int size = availableNeighbs.size();
				if ((size>0)){
					int randomElement= GetSystem().RandomInt(size);
					m_infl= availableNeighbs.get(randomElement).ToInflDir();
				} else { // size == 0
					m_infl= INFLDIR.HERE;
				}
				// Macro.Debug(":"+m_infl);
				GetSystem().PutMoveInfl(this, m_infl);}
			else{
				//Macro.Debug("sticks !");
				m_infl_sticks= true; // particle STICKS
				GetSystem().PutMoveInfl(this, INFLDIR.HERE);
				PutStickInfl();
			}
		} else {
			// particle STICKED
			GetSystem().PutMoveInfl(this, INFLDIR.HERE);
		}

	}


	private void PutStickInfl() {
		// TODO : cast here : how to remove it ?
		DLAcell currentpos= (DLAcell)GetPosition();
		currentpos.PutStickInfl();		
	}

	/*--------------------
	 * update
	--------------------*/

	@Override
	public void sig_InternalUpdate() {
		if (m_infl_sticks){
			m_int_free= false;
			// decrease population
			//m_system.sig_AgentStuck();
			m_infl_sticks= false;
		}
		
	}


	@Override
	public void sig_PositionObservableUpdate() {
		// 
		if (m_int_free){
			switch (m_UpdateMode) {
			case CONFLICT:	
				break;			
			case NOCONFLICT:
				DoMove();
				break;
			default:
				Macro.FatalError("updatemode not expected:" + m_UpdateMode);
			break;
			}
		} else {
			// DO NOTHING
		}
		m_UpdateMode= null; // security

	}

	/*--------------------
	 * get / set
	--------------------*/

	@Override
	public void SetOrientation(DIR dir) {
		// DO NOTHING
		Macro.FatalError("Orientation set for Random Move agent");
	}

	final public int GetInternalState() {
		return m_int_free?1:2;
	}




}
