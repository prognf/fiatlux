package models.MASmodels.DLA;

import architecture.multiAgent.tools.EnvCell;

public class DLAcell extends EnvCell {
	
	
	final static int EMPTY=0, STICK=DLAsystem.ST_STICK;
	private boolean m_INFL_signal = false;
	
	public DLAcell(int in_x, int in_y) {
		super(in_x, in_y);
	}
	
	@Override
	public void sig_Reset() {
		m_INFL_signal= false;
		m_state= EMPTY;
		m_countInflMove= 0;
	}

	public void PutStickInfl(){
		m_INFL_signal= true;
	}
	
	@Override
	public void sig_Update() {
		if (m_INFL_signal){
			m_state= STICK;
		}
		m_countInflMove= 0; // TODO should be done automatically
	}

}
