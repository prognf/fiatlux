package models.MASmodels.turmite;

import architecture.multiAgent.tools.DIR;
import architecture.multiAgent.tools.SituatedAgent;
import components.types.IntC;
import main.Macro;

abstract public class TurmiteAgent extends SituatedAgent {

	public final static int ST_ZERO=0, ST_ONE=1, ST_TWO=2, ST_BORDER= 333;
	final static int NOCONFLICT=99, CONFLICT=199;

	abstract public void sig_AgentPerceivePutInfluence();
	abstract public TurmiteSystem GetSystem();
	abstract public DIR GetOrientation(); // 
	abstract public void SetOrientation(DIR dir); // used in init
	/*--------------------
	 * Attributes
	 --------------------*/

	
	// for storing the result of the conflict resolution phase
	protected UPDATEMODE m_UpdateMode ; 
	/*--------------------
	 * construction
	--------------------*/

	public TurmiteAgent(TurmiteSystem in_system, String label){
		super(in_system, label);
	}

	/*--------------------
	 * signals
	--------------------*/

	
	
	/*--------------------
	 * update
	--------------------*/

	@Override
	final public void sig_ConflictResolution(UPDATEMODE mode) {
		m_UpdateMode= mode;
	}

	
	/*--------------------
	 * influences
	--------------------*/

	
	protected void FlipCurrentCell(){
		// TODO : cast here : how to remove it ?
		TurmiteEnvCell currentpos= (TurmiteEnvCell)GetPosition();
		GetSystem().AddFlip(currentpos);
	}

	
	/*--------------------
	 * get / set
	--------------------*/

		
	 
	/*--------------------
	 * Test
	--------------------*/
	
	final public static void DoTest(){
		Macro.BeginTest(TurmiteAgent.class.getSimpleName());
		IntC size= new IntC(10,10);
		TurmiteSystem system = new TurmiteSystemModel1(size);
		TurmiteAgentModel1 agent1= new TurmiteAgentModel1(system,"TestLabel");
		agent1.SetOrientation(DIR.NORTH);
		Macro.print("dir1: " + agent1.GetOrientation());
		DIR newdir = agent1.GetOrientation().TurnLeft();
		Macro.print("dir2: " + newdir );
		Macro.print("dir3: " + agent1.GetOrientation() );
		Macro.EndTest();
	}
	
}
