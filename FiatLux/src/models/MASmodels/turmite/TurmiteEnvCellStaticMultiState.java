package models.MASmodels.turmite;

import architecture.multiAgent.tools.EnvCell;

public class TurmiteEnvCellStaticMultiState extends EnvCell {

	final static int Q=3;
	int m_relative_shift=0;
	
	public TurmiteEnvCellStaticMultiState(int x, int y) {
		super(x, y);
	}

	@Override
	public void sig_Reset() {
		m_relative_shift=0;
	}

	@Override
	public void sig_Update() {
		m_state = (m_state + m_relative_shift + Q)%Q;
	}

	public void setRelativeShift(int stateShift) {
		m_relative_shift= stateShift;
	}
	
}
