package models.MASmodels.turmite;

import architecture.multiAgent.tools.EnvCell;

abstract public class TurmiteEnvCell extends EnvCell {

	enum FLIPCELLPOLICY { FUSION, ANNIHILATION}
	public final static FLIPCELLPOLICY flipCellPolicy = FLIPCELLPOLICY.FUSION;
	
	abstract public void sig_Update();
	abstract public void SetStateToOne();
	
	/*--------------------
	 * attributes
	 --------------------*/

	protected int m_countflip; // influences
	
	/*--------------------
	 * constructor
	 --------------------*/

	public TurmiteEnvCell(int in_x, int in_y) {
		super(in_x, in_y);
		m_state= -1;
	}


	/*--------------------
	 * signals
	--------------------*/

	/* global reset before running a simulation ! */
	public void sig_Reset() {
		m_countflip=0;
		SetState(0); //m_state= 0;
		m_countInflMove=0;
	} 


	
	public void Update_StaticOnes(){
		boolean flip = false;
		switch (flipCellPolicy){
		case FUSION:
			flip = (m_countflip>0); 
			break;
		case ANNIHILATION:
			flip = (m_countflip % 2 == 1);
			break;
		}
		if (flip){
			switch(m_state){
			case 0:
				m_state= 1 ; break;
			case 1:
				m_state= 2 ; break;
			case 2:
				m_state= 1 ; break;
			}
			//m_state= 1 - m_state; // flip
		}		
		ResetCounters();
	}

	protected void ResetCounters() {
		/// reset counters
		m_countflip= 0;	
		m_countInflMove=0;				
	}


	/*--------------------
	 * influences
	--------------------*/


	public void AddFlip() {
		m_countflip++;
	}

	

}
