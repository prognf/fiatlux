package models.MASmodels.turmite;

import grafix.gfxTypes.IntField;
import grafix.gfxTypes.controllers.IntController;
import grafix.gfxTypes.elements.FLPanel;

/*--------------------
 * panel for the custom init of Turmites
 * @author Nazim Fates
*--------------------*/
public class TurmitesInitManual extends FLPanel {
	/*--------------------
	 * attributes
	 --------------------*/
	
	private static final int DEF_AGENTS = 2;
	private int m_Nagents = DEF_AGENTS;
	IntField [] m_xpos, m_ypos, m_dir;
	NControl m_Nturmites;
	FLPanel m_inputarray;

	/*--------------------
	 * construction
	--------------------*/

	
	public TurmitesInitManual() {
		m_inputarray= new FLPanel();
		m_Nturmites = new NControl();
		Add(m_Nturmites, m_inputarray);
		OpenInputWindow();			
	}

	/*--------------------
	 * main
	--------------------*/
	private void OpenInputWindow() {
		
		m_xpos = new IntField[m_Nagents];
		m_ypos = new IntField[m_Nagents];
		m_dir = new IntField[m_Nagents];
		
		m_inputarray.removeAll();
		m_inputarray.SetBoxLayoutY();
	//	m_inputarray.add(m_Nturmites);		
		
		for (int agent= 0; agent < m_Nagents; agent++){
			FLPanel line3 = new FLPanel();
			m_xpos[agent]= new IntField("X" + agent,3,agent);
			m_ypos[agent]= new IntField("Y" + agent,3);
			m_dir[agent]= new IntField("dir" + agent,3);
			line3.Add(m_xpos[agent], m_ypos[agent], m_dir[agent]);
			m_inputarray.add(line3);
		}
		m_inputarray.validate();
	}
	

	class NControl extends IntController/*IntReadControl*/ {
		public NControl() {
			super("Nturmites",3);
		}

		@Override
		protected int ReadInt() {
			return m_Nagents;
		}

		@Override
		protected void SetInt(int val) {
			m_Nagents= val;
			OpenInputWindow();					
		}
		
	}

	/*--------------------
	 * get set
	--------------------*/
	final int GetXAgent(int agent){
		return m_xpos[agent].GetValue();	
	}
	final int GetYAgent(int agent){
		return m_ypos[agent].GetValue();	
	}
	final int GetDAgent(int agent){
		return m_dir[agent].GetValue();	
	}

	public int GetNagents() {
				return m_Nagents;
	}
	
}
