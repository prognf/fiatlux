package models.MASmodels.turmite;

import architecture.multiAgent.tools.SituatedAgent;
import components.types.IntC;

public class TurmiteSystemModel1 extends TurmiteSystem {

	public TurmiteSystemModel1(IntC in_size) {
		super(in_size);
	}

	public SituatedAgent GetNewAgent(int agentnum) {
		return new TurmiteAgentModel1(this,"Agent" + agentnum);
	}

	public String GetModelName() {
		return getClass().getSimpleName();
	}
	

}
