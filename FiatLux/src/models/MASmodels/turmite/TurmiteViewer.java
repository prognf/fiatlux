package models.MASmodels.turmite;

import java.awt.event.MouseEvent;

import architecture.multiAgent.tools.DIR;
import architecture.multiAgent.tools.EnvCell;
import architecture.multiAgent.tools.MultiAgentSystem;
import architecture.multiAgent.tools.MultiAgentViewer;
import components.types.IntC;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.viewers.MouseInteractive;
import main.Macro;

public class TurmiteViewer extends MultiAgentViewer 
implements MouseInteractive
{

	final static int NCOLAGENT=4;
	final static protected FLColor[] 
	                  PaletteCell = {FLColor.c_white, FLColor.c_blue, FLColor.c_lightgrey},
	                  PaletteAgent = { FLColor.c_brown, FLColor.c_superRed, FLColor.c_darkgreen, FLColor.c_red };

    TurmiteSystem m_Tsystem;

	public TurmiteViewer(MultiAgentSystem in_system, IntC in_PixSize) {
		super(in_system, in_PixSize);
		// CAST
		try{
			m_Tsystem= (TurmiteSystem) in_system;
			Macro.print(5," Viewer linked to system");
		} catch (Exception e){
			Macro.FatalError("Turmite Viewer can only be used with TurmiteSystem");
		}

		SetPalette(new PaintToolKit(PaletteCell));
		// mouse becomes active
		ActivateMouseFollower(this);
		
	}

	@Override
	protected void DrawEnvironmentAgents() {
		// ENVIRONMENT
		for (int x = 0; x < getXsize(); x++){
			for (int y = 0; y < getYsize(); y++) {
				int state = m_system.GetCellState(x,y); 
				DrawSquareColor(x, y,state);
			}
		}
		// AGENTS
		int n = m_system.GetAgentNumber();
		
		for (int agent = 0; agent < n; agent++) {
			DIR orientation = 
				m_Tsystem.GetTurmiteAgent(agent).GetOrientation();

			EnvCell cell = m_system.GetAgentPos(agent);
			int x = cell.GetX();
			int y = cell.GetY();
			int icol= agent % NCOLAGENT;
			SetColor(PaletteAgent[icol]);
			DrawCellTriangle(x, y, orientation);
		}

	}

	@Override
	public void ProcessMouseEventXY(int x, int y, MouseEvent event) {
		//Macro.Debug( "hi ha clic ");
		int st= m_system.GetCellState(x, y) % 2;
		m_system.SetCellState(x, y, 1 - st);		
		this.UpdateView();
	}

	
}

