package models.MASmodels.turmite;

import architecture.multiAgent.tools.DIR;
import architecture.multiAgent.tools.INFLDIR;
import main.Macro;
/*
 * HERE THE INFLUENCE IS A NEW direction
 * and is thus of type DIR
 *  
 * 
 */
public class TurmiteAgentModel1 extends TurmiteAgent {

	final static DIR DEF_DIR= DIR.NORTH;

	private static final boolean PRINTTRACE = false;
	
	/*--------------------
	 * Attributes
	 --------------------*/
	
	// we keep a pointer to this system 
	// because we need to "speak" its language (e.g., AddFlip)
	private TurmiteSystem m_system= null;
	
	private INFLDIR m_buffer_dir;
	
	private DIR m_int; // orientation
	// private int m_obs= NOVAL;// empty
	
	/*--------------------
	 * construction
	--------------------*/

	public TurmiteAgentModel1(TurmiteSystem in_system, String label) {
		super(in_system, label);
		m_system= in_system;
	}

	/*--------------------
	 * reset
	--------------------*/

	public void sig_AgentReset() {
		super.sig_AgentReset();
	//	m_obs= NOVAL;
		m_int= DEF_DIR;
	}
	
	
	/*--------------------
	 * decision
	--------------------*/

	public void sig_AgentPerceivePutInfluence() {
		// PERCEPTION
		int cellstate= GetCurrentStateCell();
		// INFLUENCES
		switch(cellstate){
		case ST_ZERO:
		case ST_TWO:
			ToRightDirMoveInfl();	
			FlipCurrentCell(); 
			break;
		case ST_ONE:
			ToLeftDirMoveInfl();
			//StraightDirMoveInfl();
			FlipCurrentCell();
			break;
		case ST_BORDER:
			StayMoveInfl();
			break;
		default:
			Macro.FatalError("Unexpected state found:" + cellstate);
		}

	}

	/*--------------------
	 * update
	--------------------*/

	/* assumption : m_buffer_dir should have been updated before 
	 * the internal state is updated freely (direction) */
	public void sig_InternalUpdate() {
		// internal state gets updated
		// if the influence is to turn direction then we do it
		if (m_buffer_dir != INFLDIR.HERE){
			m_int= m_buffer_dir.ToDir();
		}
		if (PRINTTRACE)
			Macro.printNOCR(m_int + ":");
	}
	
	@Override
	public void sig_PositionObservableUpdate() {
		/*if (m_UpdateMode==null){
			Macro.Debug("what ??? says " + GetLabel());		
		} else {
			Macro.Debug("right, says " + GetLabel());
		}*/
		switch (m_UpdateMode) {
		case CONFLICT:	
			break;			
		case NOCONFLICT:
			DoMove();
			break;
		default:
			Macro.FatalError("updatemode not expected:" + m_UpdateMode);
		break;
		}
		m_UpdateMode= null; // security
	}
	
	/*--------------------
	 * influences
	--------------------*/
	
	private void StayMoveInfl(){
		PutMoveInfluence( INFLDIR.HERE  );		
	}
	
	/* production of influence 
	  taking into account the exclusion principle ==>
	counting the move influence on dest cell */
	private void ToRightDirMoveInfl(){
		INFLDIR newdir= m_int.TurnRight().ToInflDir();
		PutMoveInfluence( newdir  );		
	}

	/* production of influence */
	private void ToLeftDirMoveInfl(){
		INFLDIR newdir= m_int.TurnLeft().ToInflDir();
		PutMoveInfluence( newdir  );
	}
	
	/* we keep the same direction */
	private void StraightDirMoveInfl(){
		INFLDIR newdir= m_int.ToInflDir();
		PutMoveInfluence( newdir  );
	}

	/* we memorize in the envt AND in the agent the target cell 
	 * this memorisation is useful for the system
	 * when conflicts are solved
	 * */
	private void PutMoveInfluence(INFLDIR dir){
		m_buffer_dir = dir; // will be used in sig_InternalUpdate()
		m_system.PutMoveInfl(this, dir);
	}

	/*--------------------
	 * implmentations
	--------------------*/
	
	public TurmiteSystem GetSystem(){
		return m_system;
	}
	
	/* used at initialisation */
	public void SetOrientation(DIR dir) {
		m_int= dir;
	}

	@Override
	public DIR GetOrientation() {
		return m_int;
	}
	
	
}
