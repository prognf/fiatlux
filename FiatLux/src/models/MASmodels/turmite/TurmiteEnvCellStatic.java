package models.MASmodels.turmite;

public class TurmiteEnvCellStatic extends TurmiteEnvCell {

	public TurmiteEnvCellStatic(int inX, int inY) {
		super(inX, inY);
	}

	@Override
	public void sig_Update() {
		Update_StaticOnes();
	}

	@Override
	public void SetStateToOne() {
		m_state= 1;		
	}

}
