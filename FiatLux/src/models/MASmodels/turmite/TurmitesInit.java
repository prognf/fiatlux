package models.MASmodels.turmite;

import architecture.multiAgent.tools.DIR;
import components.types.IntC;
import initializers.MASinit.MultiAgentInitializer;

abstract public class TurmitesInit extends MultiAgentInitializer {
	private TurmiteSystem m_system;

	private int m_Xoffset, m_Yoffset;


	public TurmitesInit(TurmiteSystem in_system) {
		super(in_system);
		m_system= in_system;		
	}


	/*--------------------
	 * figure inits
	 --------------------*/
	

	/* absolute coord */
	protected void AbsPutAgent (int index, int x, int y, DIR dir){
		m_system.SetAgentPos(index,new IntC(x,y));
		m_system.SetAgentOrientation(index, dir);
	}

	/* relative coord */
	protected void PutAgentOffset(int index, int x, int y, DIR dir){
		int newx= (x + m_Xoffset) % GetXsize();
		int newy= (y + m_Yoffset) % GetYsize();
		AbsPutAgent(index, newx, newy, dir);
	}

	protected void PutAgentNoOffset(int index, int x, int y, DIR dir){		
		AbsPutAgent(index, x, y, dir);
	}

	/** relative coordinate setting **/
	public void SetOffset(int dx, int dy){
		m_Xoffset= dx;
		m_Yoffset= dy;
	}
	
	/** relative coordinate setting **/
	public void ShiftOffset(int dx, int dy){
		m_Xoffset += dx;
		m_Yoffset += dy;
	}

	/** relative coordinate setting **/
	public void SetOffsetInTheMiddle(){
		SetOffset(GetXsize() / 2, GetYsize() / 2);
	}
}
