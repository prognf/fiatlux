package models.MASmodels.turmite;


public class TurmiteEnvCellDynamic extends TurmiteEnvCell {

	private int m_MAXCELLONE; // maximum duration of a cell 1
	private int m_levelCellOne;  // current level of cell 1

	
	public TurmiteEnvCellDynamic(int inX, int inY, int MaxDurationCellOne) {
		super(inX, inY);
		m_MAXCELLONE= MaxDurationCellOne;
		m_levelCellOne=-99;
	}

	@Override
	public void sig_Update() {
		Update_DynamicOnes();
	}

	//@Override
	/* takes flips into account 
	 * and resets infl counters
	 */ 
	public void Update_DynamicOnes(){
		boolean flip = false;
		switch (flipCellPolicy){
		case FUSION:
			flip = (m_countflip>0); 
			break;
		case ANNIHILATION:
			flip = (m_countflip % 2 == 1);
			break;
		}
		if (flip){
			switch(m_state){
			case 0:
			case 2:
				SetStateToOne();
				break;
			case 1:
				m_state= 2 ; 
				break;
			} 
		}
		else { // noflip
			if (m_state ==1){
				if (m_levelCellOne> 0){
					m_levelCellOne--;
				}
				if (m_levelCellOne == 0){
					m_state = 2;
				}
			}
		}
		//m_state= 1 - m_state; // flip		
		ResetCounters();
	}
	
	public void SetStateToOne() {
		m_state= 1;
		m_levelCellOne= m_MAXCELLONE;		
	}
	
}
