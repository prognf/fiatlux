

package models.MASmodels.turmite;

import architecture.multiAgent.tools.DIR;
import main.Macro;

public class TurmiteAgentModel2 extends TurmiteAgent {


	final static int RST=-1;
	enum TURN_INFLUENCE { LEFT, RIGHT}

    final public static int FWD = 100;
	
	/*--------------------
	 * Attributes
	 --------------------*/
	// we keep a pointer to this system 
	// because we need to "speak" its language (e.g., AddFlip)
	private TurmiteSystemModel2 m_system= null;

	private TURN_INFLUENCE m_infl_dir;
	
	DIR m_dir; // orientation
	private DIR m_buffer_dir;
	//int m_int= RST; 
	
	
	/*--------------------
	 * construction
	--------------------*/

	public TurmiteAgentModel2(TurmiteSystemModel2 in_system, String label) {
		super(in_system, label);
		m_system= in_system;
	}

	/*--------------------
	 * signals
	--------------------*/

	public void sig_AgentPerceivePutInfluence() {
		// PERCEPTION
		int cellstate= GetCurrentStateCell();
		// INFLUENCES
		switch(cellstate){
		case ST_ZERO:
		case ST_TWO:
			PutTurnRightInfl();
			PutForwardInfl();
			FlipCurrentCell(); 
			break;
		case ST_ONE:
			PutTurnLeftInfl();
			PutForwardInfl();
			FlipCurrentCell();
			break;
		case ST_BORDER:
			// do nothing ?
			break;
		default:
			Macro.FatalError("Unexpected state found:" + cellstate);
		}

	}

	
	public void sig_InternalUpdate() {
		// Nothing to do
	}
	
	/* FOR THIS MODEL
	 * we decompose into two parts
	 */
	public void sig_PositionObservableUpdate() {
		PositionUpdate();		
		OrientationUpdate();
		m_UpdateMode= null; // security
	}
	
	
	private void PositionUpdate() {
		
		switch (m_UpdateMode) {
		case CONFLICT:	
			break;			
		case NOCONFLICT:
			DoMove();
			break;
		default:
			Macro.FatalError("updatemode not expected:" + m_UpdateMode);
		break;
		}
	}

    private void OrientationUpdate(){
		// 
		switch (m_UpdateMode) {
		case CONFLICT:	
			break;			
		case NOCONFLICT:
			m_dir= m_buffer_dir;
			break;
		default:
			Macro.FatalError("updatemode not expected:" + m_UpdateMode);
		break;
		}

	}

	/*--------------------
	 * influences
	--------------------*/
	/*private void StayMoveInfl(){
		PutMoveInfluence( DIR.HERE  );		
	}*/
	
	private void PutTurnLeftInfl() {
		m_infl_dir= TURN_INFLUENCE.LEFT;
	}
	private void PutTurnRightInfl() {
		m_infl_dir= TURN_INFLUENCE.RIGHT;
		
	}

	private void PutForwardInfl() {
		m_system.PutForwardInfl(this,FWD);		
	}
	
	/* *
	 * Called by system
	 */ 
	public TURN_INFLUENCE GetInflDir() {
		return m_infl_dir;
	}
	
	/*--------------------
	 * implementations
	--------------------*/
	
	public TurmiteSystem GetSystem(){
		return m_system;
	}

	public void SetOrientation(DIR dir) {
		m_dir=dir;		
	}

	public DIR GetOrientation() { 
		return m_dir;
	}

	
	/*--------------------
	 * get / set
	--------------------*/
	
	/* called by system */
	public void SetTurnTarget(DIR target_dir) {
		m_buffer_dir= target_dir;		
	}

	

	

		

}



