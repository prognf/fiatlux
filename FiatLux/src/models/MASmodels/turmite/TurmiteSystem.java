package models.MASmodels.turmite; 
import architecture.multiAgent.tools.DIR;
import architecture.multiAgent.tools.EnvCell;
import architecture.multiAgent.tools.MultiAgentSystem;
import components.types.IVector;
import components.types.IntC;
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.GridViewer;
import initializers.MASinit.MultiAgentInitializer;
import main.Macro;
/*******************************************************
 * implements an SMA model
 * @author Nazim Fates
 ********************************************************/ 
public abstract class TurmiteSystem extends MultiAgentSystem {  

	enum Turmitetype {TYPE1, TYPE2 }

	/*--------------------
	 * Attributes
	 --------------------*/

	private int m_CellType=2; // type of cell ; 0/1 : do the 1's disappear ?
	private int m_MaxCellOne = 10;

	/*--------------------
	 * construction
	--------------------*/

	public TurmiteSystem(IntC in_size){
		super(in_size);
		//sig_Init();	
		m_CellType = 0;
		InitArray();
	}

	/*--------------------
	 * main
	--------------------*/


	/*--------------------
	 * cells influences
	--------------------*/

	public void AddFlip(TurmiteEnvCell in_pos) {
		in_pos.AddFlip();		
	}

	/*--------------------
	 * implementations
	--------------------*/

	/* view & control */
	public FLPanel GetViewControl(){
		FLPanel panel = new FLPanel();
		return	panel; 
	}

	@Override
	public EnvCell GetNewEnvCell(int x, int y) {
		switch (m_CellType){
		case 0:
			return new TurmiteEnvCellDynamic(x,y, m_MaxCellOne );
		case 1:
			return new TurmiteEnvCellStatic(x,y);
		case 2:
			return new TurmiteEnvCellStaticMultiState(x, y);
		default:
			Macro.FatalError("non-existing choice");return null;
		}
	}

	@Override
	public MultiAgentInitializer GetNewInitializer() {
		return new TurmitesGFXInitializer(this);
	}


	/*--------------------
	 * Get / Set
	--------------------*/
	@Override
	public GridViewer GetSystemViewer(IntC CellSizeForViewer) {
		return new TurmiteViewer(this, CellSizeForViewer);
	}


	public DIR GetOrientationAgent(int a) {
		return GetTurmiteAgent(a).GetOrientation();
	}

	public EnvCell GetPositionAgent(int a) {
		return GetAgent(a).GetPosition();
	}


	protected TurmiteAgent GetTurmiteAgent(int agentnum){
		return (TurmiteAgent) GetAgent(agentnum);
	}


	public void SetAgentOrientation(int agentnum, DIR dir) {
		TurmiteAgent agent= GetTurmiteAgent(agentnum);
		agent.SetOrientation(dir);
	}


	/*--------------------
	 * OTHER
	--------------------*/

	public int CountNonZeroCells(){
		int count=0;
		for (int x=0; x < GetXsize() ; x++){
			for (int y=0; y < GetYsize() ; y++){
				if (GetCellState(x,y) != 0){
					count++;
				}
			}
		}
		return count;
	}

	public IVector CountStateCells(){
		IVector count=new IVector(3);
		for (int x=0; x < GetXsize() ; x++){
			for (int y=0; y < GetYsize() ; y++){
				int st =GetCellState(x,y);
				count.IncrementPos(st);
			}
		}

		return count;
	}

}//class
