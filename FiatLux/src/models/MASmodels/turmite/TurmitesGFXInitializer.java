package models.MASmodels.turmite;

import architecture.multiAgent.tools.DIR;
import components.types.IVector;
import grafix.gfxTypes.IntField;
import grafix.gfxTypes.controllers.FLCheckBox;
import grafix.gfxTypes.controllers.IntVectorControl;
import grafix.gfxTypes.controllers.Radio2Control;
import grafix.gfxTypes.elements.FLPanel;
import grafix.gfxTypes.elements.FLTabbedPane;
import main.Macro;

/*--------------------
 * intializer for Turmites
 * @author Nazim Fates
*--------------------*/
public class TurmitesGFXInitializer extends TurmitesInit {
	final static boolean DEF_BORDER= false ;
	final static int LEN=2;
	final static String 	TXT_NTURMITES="Nagents";
	final static String [] LineLbl= { "DX", "DO", "Oini"};
	final static int
		DEF_NTURMITES=1, DEF_INCREMENT=1, DEF_INCANGLE=0, DEF_INITORIENT=0; //space 

	
	final static int SQLEN=0, SQDIR=1, SQDELTA=2;
	final static String [] SquareLbl= {"Sq. Len", "Sq. Init. dir", "Sq Delta dir."};
	final static int [] SquareVal = { 4, 2, -1};
	IVector m_SquarePar = new IVector(SquareVal);

	
	final static int  LINE=0, SQUARE=1, RANDOM=2, ONECELLONE=3,  GLIDER=4, CUSTOM=5, FACE=6;
	final static int DEF_SELECT=0;

		
	/*--------------------
	 * attributes
	 --------------------*/

	public boolean m_AbsorbingBorders= false;
	
	int m_InitStrategy = LINE;  // should be controlled

	IntField mF_Nturmites;
	IVector m_LinePar= new IVector(new int [] {DEF_INCREMENT, DEF_INCANGLE, DEF_INITORIENT});
	
	IntField m_FaceLen= new IntField("Face len", LEN, 4);
	
	Radio2Control m_RadioFace= new OrderSelect();
	FLTabbedPane m_select;
	TurmitesInitManual m_Manual;
	int m_FaceInitType ;  // for face init
	TurmiteSystem m_turmiteSystem;

	/*--------------------
	 * construction
	--------------------*/
	public TurmitesGFXInitializer(TurmiteSystem in_system) {
		super(in_system);
		mF_Nturmites = new IntField(TXT_NTURMITES, LEN, DEF_NTURMITES);
		mF_Nturmites.AddPlusMinusButtons();
		
		m_turmiteSystem= in_system;
		SetOffsetInTheMiddle();
	}	

	/*--------------------
	 * main
	--------------------*/

	
	/* we assume environment initialised */
	public void SubInit(){	
		// select method
		int select;
		if (m_select != null){  // if the graphical component was not called
			select= m_select.GetSelectedTabIndex();
		} else {
			select= m_InitStrategy;
		}
		// init method
		switch(select){
		case RANDOM:
			RandomLocations();
			break;
		case LINE:
			PutAgentsInLine();
			break;
		case ONECELLONE:
			OneCellOneInit();
			break;
		case SQUARE:
			SquareInit();
			break;
		case GLIDER:
			//GliderInit();
			GliderBInit();
			break;
		case CUSTOM:
			CustomInit();
			break;
		case FACE:
			FaceInit();
			break;
		default:
			Macro.FatalError("case not taken into account");
		}

		// borders
		if (m_AbsorbingBorders){
			//Macro.Debug("borders");
			MakeAbsorbingBorders();
		}
	
	}

	private void MakeAbsorbingBorders() {
		super.MakeBorders(TurmiteAgent.ST_BORDER);

	}

	
	/*--------------------
	 * inits
	 --------------------*/

	private void FaceInit(){
	
		int squarelen= m_FaceLen.GetValue();
		CreateAgents(NAGENTS);
		int [] DX= {0, 1, 1, 0};
		int [] DY= {0, 0, 1, 1};
		DIR [] INITDIR= {DIR.NORTH, DIR.NORTH, DIR.SOUTH, DIR.SOUTH};
		int [] permA = {0, 1, 2, 3}, permB={0, 2, 1, 3}, perm;
		if (m_FaceInitType==0){
			perm= permA;
		} else{
			perm= permB;
		}
		for (int index=0; index < NAGENTS ; index++){
			int newindex= perm[index];
			int x = DX[newindex] * squarelen;
			int y = DY[newindex] * squarelen;
			DIR dir = INITDIR[newindex];
			PutAgentOffset(index, x, y, dir);
		}
		
	}
	
	private void CustomInit() {
		int nagents = m_Manual.GetNagents();
		CreateAgents(nagents);
		for (int index=0; index< nagents; index++){
			int x= m_Manual.GetXAgent(index); 
			int y= m_Manual.GetYAgent(index);
			DIR dir = DIR.FromIntModulo(m_Manual.GetDAgent(index));
			PutAgentOffset(index, x, y, dir);
		}		
	}

	private void RandomLocations(){
		int nagents = mF_Nturmites.GetValue();
		CreateAgents(nagents);
		for (int index=0; index< nagents; index++){
			int x= RandomInt( GetXsize() ); 
			int y= RandomInt( GetYsize() );
			DIR dir = DIR.RandomDir4(GetRandomizer());
			PutAgentNoOffset(index, x, y, dir);
		}
	}

	private void PutAgentsInLine() {
		int nagents = mF_Nturmites.GetValue();
		CreateAgents(nagents);
		
		int increment = m_LinePar.GetVal(0);
		int incrementOrientation =  m_LinePar.GetVal(1);
		int orientationIndex=  m_LinePar.GetVal(2);

		for (int i=0; i< nagents; i++){
			int x= (increment* i) % GetXsize(); 
			int y= 0 ;
			PutAgentOffset(i, x ,y, DIR.FromIntModulo(orientationIndex));			
			orientationIndex+= incrementOrientation;
		}		
	}

    private static final int AGENTONE = 0;
	/* to show sensitivity to initial condition */
	private void OneCellOneInit() {
		CreateAgents(1); // one single agent
		PutAgentOffset(AGENTONE, 0, 0, DIR.NORTH);
		SetCellToOneWithOffset(0, 1);	
	}



	private void GliderInit() {
		CreateAgents(2);
		PutAgentOffset(0, 1, 1, DIR.NORTH);
		PutAgentOffset(1, 3, 1, DIR.EAST);
		SetCellToOneWithOffset(0, 0);
		SetCellToOneWithOffset(1, 0);
		SetCellToOneWithOffset(0, 1);
		SetCellToOneWithOffset(1, 1);
		SetCellToOneWithOffset(2, 1);
	}
	
	private void GliderBInit(){
		CreateAgents(4);
		PutAgentOffset(0, 0, 0, DIR.NORTH);
		PutAgentOffset(1, 0, 1, DIR.NORTH);
		PutAgentOffset(2, 1, 1, DIR.NORTH);
		PutAgentOffset(3, 1, 0, DIR.NORTH);
		
	}
	

	final static int NAGENTS=4;
	private void SquareInit() {
		
		CreateAgents(NAGENTS);
		int [] DX= {0, 1, 1, 0};
		int [] DY= {0, 0, 1, 1};
		int orientation= m_SquarePar.GetVal(SQDIR);
		int squarelen = m_SquarePar.GetVal(SQLEN);
		int deltaorientation= m_SquarePar.GetVal(SQDELTA);
		for (int index=0; index < NAGENTS ; index++){
			int x = DX[index] * squarelen;
			int y = DY[index] * squarelen;
			DIR dir = DIR.FromIntModulo(orientation);
			orientation+= deltaorientation;
			PutAgentOffset(index, x, y, dir);
		}

	}

	/*--------------------
	 * get / set
	--------------------*/

	public void SetCellToOneWithOffset(int x, int y){
		int dx= GetXsize() / 2, dy = GetYsize() / 2;
		TurmiteEnvCell cell = (TurmiteEnvCell)m_turmiteSystem.GetCellXY(x +  dx,y + dy);
		cell.SetStateToOne();
	}
	
	public void SelectFaceInit(int dist, int order){
		m_InitStrategy= FACE;
		m_FaceInitType= order;
		m_FaceLen.SetValue(dist);
	}
	
	public void SelectSquareInit(int incDist, int initorientation, int incAngle){
		m_InitStrategy= SQUARE;
		int [] pars= {incDist, initorientation, incAngle};
		m_SquarePar.SetValues(pars);		
	}

	public void SelectGliderInit(){
		m_InitStrategy= GLIDER;
	}

	/* special for scripts */ 
	public void SelectLineInit(int nagents, int incDist, int incAngle, int initorientation){
		m_InitStrategy = LINE;
		mF_Nturmites.SetValue(nagents);
		m_LinePar.SetVal(0,incDist);
		m_LinePar.SetVal(1,incAngle);
		m_LinePar.SetVal(2,initorientation);
	}

	public void SelectOneCellOneInit(){
		m_InitStrategy= ONECELLONE;
	}


	public FLPanel GetSpecificPanel() {
		FLPanel panelA = new IntVectorControl(m_LinePar, LineLbl);
		FLPanel panelB = new IntVectorControl(m_SquarePar, SquareLbl);
		FLPanel panelC = new FLPanel();
		FLPanel panelD = new FLPanel();
		FLPanel panelE = new FLPanel();
		m_Manual = new TurmitesInitManual();
		FLPanel panelF = m_Manual;
		FLPanel panelG = new FLPanel();
		panelG.Add(m_FaceLen, m_RadioFace);
		
		m_select = new FLTabbedPane();
		m_select.AddTab("Line", panelA);
		m_select.AddTab("Square", panelB);		
		m_select.AddTab("Random", panelC);
		m_select.AddTab("OneCellOne", panelD);
		m_select.AddTab("Glider", panelE);
		m_select.AddTab("Custom", panelF);
		m_select.AddTab("Face", panelG);
		
		BorderControl border = new BorderControl();
		FLPanel TopPanel = FLPanel.NewPanelVertical(mF_Nturmites,border);
		TopPanel.setOpaque(false);

		FLPanel mainPanel = new FLPanel();
		mainPanel.SetBoxLayoutY();
		mainPanel.Add(TopPanel, m_select);
		mainPanel.setOpaque(false);
		return mainPanel;
	}

	/*--------------------
	 * GFX
	 --------------------*/

	class OrderSelect extends Radio2Control{

		public OrderSelect() {
			super("initial order", "A", "B");
		}
		
		@Override
		protected void ActionSelection1() {
			m_FaceInitType=0;	
		}
		
		@Override
		protected void ActionSelection2() {
			m_FaceInitType=1;	
		}
		
		
	}//Radio2Control
	
	class BorderControl extends FLCheckBox{
		
		private static final String TXTBORDER = "border";

		private BorderControl(){
			super(TXTBORDER, DEF_BORDER);
		}
		@Override
		protected void SelectionOff() {
			m_AbsorbingBorders= false;
		}

		@Override
		protected void SelectionOn() {
			m_AbsorbingBorders= true;			
		}
				
	}
	
	

	
	
}
