package models.MASmodels.turmite;

import architecture.multiAgent.tools.DIR;
import architecture.multiAgent.tools.SituatedAgent;
import components.types.IntC;
import main.Macro;
import models.MASmodels.turmite.TurmiteAgentModel2.TURN_INFLUENCE;

public class TurmiteSystemModel2 extends TurmiteSystem {

	
	/*--------------------
	 * attributes
	 --------------------*/
	
	

	/*--------------------
	 * construction
	--------------------*/
	
	public TurmiteSystemModel2(IntC in_size) {
		super(in_size);
	}
	
	public SituatedAgent GetNewAgent(int agentnum) {
		return new TurmiteAgentModel2(this,"Agent" + agentnum);
	}

	public void PutForwardInfl(TurmiteAgentModel2 in_agent, int infl) {
		switch (infl) {
		case TurmiteAgentModel2.FWD:
			
			DIR current_dir = in_agent.GetOrientation();
			if (current_dir == null){
				Macro.FatalError("empty dir");
			}
			DIR target_dir = null;
			
			TURN_INFLUENCE relative_dir = in_agent.GetInflDir();
			switch (relative_dir) {
			case LEFT:
				target_dir =  current_dir.TurnLeft();
				break;
			case RIGHT:
				target_dir =  current_dir.TurnRight();
				break;
			default:
				Macro.FatalError("relatived dir not expected "+ relative_dir);
				break;
			}
			 // we put the influence in the system
			PutMoveInfl(in_agent, target_dir);
			in_agent.SetTurnTarget(target_dir);
		
			break;

		default:
			Macro.FatalError("Influence value not expected "+infl);
			break;
		} 
		
	}
	
	/*--------------------
	 * signals
	--------------------*/


	public String GetModelName() {
		return getClass().getSimpleName();
	}

}
