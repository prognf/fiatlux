package models.MASmodels;

import architecture.multiAgent.tools.EnvCell;

public class EnvCellSimple extends EnvCell {
	
	
	/*--------------------
	 * attributes
	 --------------------*/


	/*--------------------
	 * constructor
	 --------------------*/
	
	public EnvCellSimple(int in_x, int in_y) {
		super(in_x, in_y);
	}
	
	
	/*--------------------
	 * signals
	--------------------*/
	
	/* global reset before running a simulation ! */
	public void sig_Reset() {
		m_state= 0;
		m_countInflMove=0;
	} 
	

	@Override
	/* takes flips into account 
	 * and resets infl counters
	 */ 
	public void sig_Update(){
		m_countInflMove=0;		
	}
	

}
