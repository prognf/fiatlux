package models.MASmodels.rotor;

import architecture.multiAgent.tools.EnvCell;
import architecture.multiAgent.tools.MultiAgentSystem;
import architecture.multiAgent.tools.MultiAgentViewer;
import components.types.IntC;
import grafix.gfxTypes.FLColor;


public class RotorMasViewer extends MultiAgentViewer {

	final static int NCOLORS=4;
	final static int AGENTCOL=3;
	static FLColor [] COL= { FLColor.c_white, FLColor.c_blue, FLColor.c_darkgreen, 
		FLColor.c_brown
	};
	
	public RotorMasViewer(MultiAgentSystem system, IntC in_PixSize) {
		super(system, in_PixSize);
		// colors
		SetPalette(COL);
	}

	@Override
	protected void DrawEnvironmentAgents() {
		int Ymax= getYsize() - 1;
		for (int x = 0; x < getXsize(); x++)
			for (int y = 0; y < getYsize(); y++) {
				// we reverse  NORTH and SOUTH ! 
				int state = m_system.GetCellState(x,Ymax - y); 
				DrawSquareColor(x, y, state);
			}
		int n = m_system.GetAgentNumber();
		SetColor(GetColor(AGENTCOL));
		for (int agent = 0; agent < n; agent++) {
			EnvCell cell = m_system.GetAgentPos(agent);
			int x = cell.GetX();
			// we reverse  NORTH and SOUTH ! 
			int y = Ymax - cell.GetY();
			DrawSquare(x, y);
		}

	}


}

