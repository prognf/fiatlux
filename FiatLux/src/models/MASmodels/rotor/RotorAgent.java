package models.MASmodels.rotor;

import architecture.multiAgent.tools.DIR;
import architecture.multiAgent.tools.INFLDIR;
import architecture.multiAgent.tools.SimpleMovingAgent;
import main.Macro;
import models.MASmodels.EnvCellSimple;


public class RotorAgent extends SimpleMovingAgent {

	/*--------------------
	 * attributes
	 --------------------*/
	protected INFLDIR m_infl = null;

	/*--------------------
	 * construction
	--------------------*/

	public RotorAgent(RotorSystem in_system) {
		super(in_system);
	}


	/*--------------------
	 * reset
	--------------------*/

	public void sig_AgentReset() {
		m_infl= null;
	}



	/*--------------------
	 * influences
	--------------------*/

	public void sig_AgentPerceivePutInfluence(){		
		EnvCellSimple cell = (EnvCellSimple)GetPosition();
		int dirSt= cell.GetState();
		m_infl = INFLDIR.FromInt(dirSt);
		//m_infl= INFLDIR.HERE;
		//Macro.Debug(":"+m_infl);
		GetSystem().PutMoveInfl(this, m_infl);
	}
	/*--------------------
	 * update
	--------------------*/

	/* empty */
	public void sig_InternalUpdate() {

	}

	@Override
	public void sig_PositionObservableUpdate() {
		EnvCellSimple cell = (EnvCellSimple)GetPosition();
		cell.SetState(1);
		switch (m_UpdateMode) {
		case CONFLICT:	
			break;			
		case NOCONFLICT:
			DoMove();
			break;
		default:
			Macro.FatalError("updatemode not expected:" + m_UpdateMode);
		break;
		}
		m_UpdateMode= null; // security
	}

    @Override
	public void SetOrientation(DIR dir) {
		// DO NOTHING
		Macro.FatalError("Orientation set for Random Move agent");
	}

}
