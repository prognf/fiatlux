package models.MASmodels.amoebae;

import components.types.IntC;
import components.types.Monitor;
import experiment.samplers.SuperSampler;
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.AutomatonViewer;

public class AmoebaeSampler extends SuperSampler {
	AmoebaeSystem 			m_system;	// main system
	AmoebaeInfotaxisViewer 	m_viewer; 	// monitor

	public AmoebaeSampler(AmoebaeSystem system, IntC pixSize) {
		m_system= system;
		addMonitor(m_viewer);
	}
	
	@Override
	public String GetSimulationInfo() {
		return "t:"+m_system.GetTime();
	}

	@Override
	public String GetTopologyInfo() {
		return "sz:"+m_system.GetXYsize();
	}

	@Override
	public int GetTime() {
		return m_system.GetTime();
	}

	@Override
	public FLPanel GetSimulationWidgets() {
		return FLPanel.NewPanel();
	}

	@Override
	public FLPanel GetSimulationRuleControls() {
		return m_system.GetSpecicPanel();
	}

	@Override
	public FLPanel GetSimulationView() {
		return FLPanel.NewPanel(m_viewer);
	}

	@Override
	public String[] GetPlotterSelection() {
		return null;
	}

	@Override
	protected Monitor GetLinkedSystemAsMonitor() {
		return m_system;
	}

	@Override
	protected String GetModelName() {
		return m_system.GetName();
	}

	public AutomatonViewer GetViewer() {
		return m_viewer;
	}

}
