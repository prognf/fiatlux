package models.MASmodels.amoebae;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.util.ArrayList;

import components.arrays.GridSystem;
import components.types.DoublePar;
import components.types.FLsignal;
import components.types.IntC;
import components.types.IntPar;
import components.types.IntegerList;
import components.types.Monitor;
import components.types.ProbaPar;
import grafix.gfxTypes.controllers.IntController;
import grafix.gfxTypes.elements.FLActionButton;
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.GridViewer;
import main.Macro;
import main.MathMacro;
import main.tables.PlotterSelectControl;
import models.MASmodels.amoebae.AmoebaAgent.AMOEBASTATE;

/*-------------------------------------------------------------------------------
- 
------------------------------------------------------------------------------*/

public class AmoebaeInfotaxisSystem extends GridSystem 
implements Monitor {

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();

	// neighb
	private static final int NDIR = 4;
	// N, E, S, W
	private static final IntC 
	dN= new IntC(0,1),  dE= new IntC(1,0), 
	dS= new IntC(0,-1), dW= new IntC(-1,0);
	private static final IntC [] DXY = {dN , dE, dS, dW};

	private static final String NAMOEBAE_TXT= "N amoebae";
	private static final int NAMOEBAE_DEF= 20;

	private static final String NAME= "AmoebaeInfotaxis";

	private static final String 
	LBL_K= "K",
	LBL_REEMISSIONPROBA= "reemission p.",
	LBL_AGITATIONPROBA= "agitation p.",
	LBL_BUTTONCOPYINFO= "copy configuration to clipboard",
	LBL_BUTTONPASTEINFO= "paste configuration from clipboard";
	private static final double
	DEF_K= .5,
	DEF_REEMISSIONPROBA= 0.9,
	DEF_AGITATIONPROBA=0.2;
	
	public final int UNIFORM = 0;
	public final int HALFDISTANCE = 1;
	public final int CORNER = 2;
	public final int GROUPED = 3;

	//--------------------------------------------------------------------------
	//- attributes
	//--------------------------------------------------------------------------

	char m_expType;
	private boolean m_gridDynamics = true; // set to false to disable reaction-diffusion

	//	boolean m_environmentActive= true;
	IntPar m_Namoebae;

	AmoebaeGridCell [][] m_cell;   				
	ArrayList<AmoebaAgent> m_amoebaList; // position of the agents

	IntC m_emittingCenter= new IntC(Xsize()/4,Ysize()/4);        // target
	//ProbaPar m_pActivation= new ProbaPar(LBL_ACTIVATIONPROBA, DEF_ACTIVATIONPROBA);
	final ProbaPar m_pAgitation= new ProbaPar(LBL_AGITATIONPROBA, DEF_AGITATIONPROBA);
	final ProbaPar m_pReemission= new ProbaPar(LBL_REEMISSIONPROBA, DEF_REEMISSIONPROBA);

	final DoublePar m_Kdetec= new DoublePar(LBL_K, DEF_K); // attenuation of the probability
	
	int m_configInit;
	
	FLPanel m_panel;

	boolean m_detected= false; 		// has anybody met the emitting center?

	int m_detectionTime;

	//--------------------------------------------------------------------------
	//- construction
	//--------------------------------------------------------------------------

	public AmoebaeInfotaxisSystem(char expType, IntC gridSize, int N, double K, double pA, double pR, int initConfig) {
		this(gridSize);

		m_Kdetec.SetVal(K);
		m_pAgitation.SetVal(pA);
		m_pReemission.SetVal(pR	);
		m_Namoebae.SetVal(N);
		m_configInit = initConfig;

		m_expType= expType;
		switch (m_expType){
		case 'A':
			m_gridDynamics= true;
			break;
		case 'B':
			m_gridDynamics= false;
			break;
		}
	}



	public AmoebaeInfotaxisSystem(IntC gridSize) {
		super(gridSize);
		m_Namoebae= new IntPar(NAMOEBAE_TXT, NAMOEBAE_DEF);
		// construction of the grid
		m_cell= new AmoebaeGridCell[Xsize()][Ysize()];
		for (int y=0; y< Ysize(); y++){
			for (int x=0; x< Xsize(); x++){
				m_cell[x][y]= new AmoebaeGridCell();
			}
		}
		for (int y=0; y< Ysize(); y++){
			for (int x=0; x< Xsize(); x++){
				for (int neighbIndex=0; neighbIndex < NDIR; neighbIndex++){
					AddNeighbWithControl(new IntC(x,y),neighbIndex);
				}
			}
		}
	}

	private void AddNeighbWithControl(IntC xy, int neighbIndex) {
		IntC txy= IntC.AddNew(xy, DXY[neighbIndex]);
		if (IsInsideGrid(txy)){
			GetCell(xy).AddNeighb(GetCell(txy));
		}
	}

	/**
	 * calculates the detection probability as 
	 * a function of the SQUARE of the distance to the emitting center
	 */
	private double DetectionProba(int d2) {
		//double r=Math.sqrt(d2);
		if (d2!=0){
			return m_Kdetec.GetVal()/(double)(d2);
		} else {
			return 1;
		}
	}

	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------

	private void NextStep() {
		if (!m_detected){
			OneNextStep();
		} else {
			// do nothing
		}
	}

	private void OneNextStep() {
		//Macro.Debug("kloup");
		// grid new state
		ClearInfluenceTable();
		Influence();
		// reaction
		Reaction();
		// time 
		TimeClick();
	}

	//--------------------------------------------------------------------------
	//- INFLUENCE
	//--------------------------------------------------------------------------

	private void Influence() {
		//DEBUG
		//GetCell(m_emittingCenter).InfluenceActivateByHit();
		// agents part
		InfluenceAgentsOnGrid();
		// CA part (influence)
		if (m_gridDynamics){
			InfluencePropagationOfExcitations();
		}
	}

	private void InfluenceAgentsOnGrid() {
		for (AmoebaAgent agent: m_amoebaList){
			agent.MakeInfluenceStep();
		}
	}

	private void InfluencePropagationOfExcitations() {
		for (int y=0; y< Ysize(); y++){
			for (int x=0; x< Xsize(); x++){
				AmoebaeGridCell cell= GetCell(x,y);
				if (cell.HasAnExcitedNeighbour()){
					cell.PropagateByNeighb();
				}
			}
		}
	}



	/** chooses one excited dir if it exists, otherwise returns -1 **/
	/*	private int FindExcitedNeighb(IntC pos) {
		ArrayList<AmoebaeGridCell> neighb= GetCell(pos).m_neighb;
	}*/

	private void ClearInfluenceTable() {
		for (int y=0; y< Ysize(); y++){
			for (int x=0; x< Xsize(); x++){
				AmoebaeGridCell cell = m_cell[x][y];
				cell.ClearActivation();
			}
		}
	}

	/** realises a detection test in cell pos */
	boolean DetectionEvent(IntC pos){
		double pDetection= m_cell[pos.X()][pos.Y()].m_detectProba;
		return RandomEventDouble(pDetection);
	}

	/** returns movement to an excited cell if such a cell exists */
	public IntC GetMovementToExcitedNeighb(IntC pos) {
		IntegerList listDirExcited= new IntegerList();
		for (int indexDXY=0; indexDXY < NDIR ; indexDXY++){
			IntC txy= IntC.AddNew(pos, DXY[indexDXY]);
			if (IsInsideGrid(txy) && IsExcited(txy)){
				listDirExcited.addVal(indexDXY);

			}
		}
		if (listDirExcited.size()==0){
			return null;
		} else {
			int dirChosen= listDirExcited.ChooseOneAtRandom(this.GetRandomizer());
			return DXY[dirChosen];
		}
	}

	private boolean IsExcited(IntC xy) {
		return GetCell(xy).IsStateExcited();
	}



	private boolean IsInsideGrid(IntC xy) {
		return m_XYsize.isInsideGrid(xy);
	}



	//--------------------------------------------------------------------------
	//- REACTION
	//--------------------------------------------------------------------------

	private void Reaction() {
		ReactionAgents();
		if (m_gridDynamics){
			ReactionGridCells();
		}

	}

	private void ReactionAgents() {
		for (AmoebaAgent am: m_amoebaList){
			am.ReactionStep();
		}
	}


	/** transforming influences into changes of states **/
	private void ReactionGridCells() {
		for (int y=0; y< Ysize(); y++){
			for (int x=0; x< Xsize(); x++){
				GetCell(x,y).MakeReaction();
			}
		}

	}

	private void MakeDetection() {
		for (AmoebaAgent am: m_amoebaList){
			IntC pos= am.GetPos();
			if (pos.IsEqualTo(m_emittingCenter)&&!m_detected){
				m_detected= true;
				Macro.print("Kloupa! t:"+GetTime());
			}
		}
	}

	/** reset system **/
	private void InitArray(){
		m_detected= false;

		for (int y=0; y < Ysize(); y++){
			for (int x=0; x < Xsize(); x++){
				ResetCellState(x,y,0);// cleaning
				// proba of detection
				int d2=MathMacro.EuclideanDistSquare(x, y, m_emittingCenter);
				m_cell[x][y].m_detectProba= DetectionProba(d2);

			}
		}
		// creates new list of amoebae
		int N= m_Namoebae.GetVal();
		m_amoebaList= new ArrayList<AmoebaAgent>();
		for (int i=0; i< N; i++){
			//IntC pos= GetXYsize().RandomPos(this);

			//size.Mult(0.5);
			int x = -1 ,y = -1; //Necessary to prevent eclipse from complaining about x and y that may not have been initialized
			
			if (m_configInit == CORNER){
				x= RandomInt(Xsize()/3)+2*Xsize()/3;
				y= RandomInt(Xsize()/3)+2*Ysize()/3;
			} else if (m_configInit == UNIFORM){
				x= RandomInt(Xsize());
				y= RandomInt(Ysize());
			} else if (m_configInit == HALFDISTANCE){
				x = RandomInt(Xsize());
				y = RandomInt(Ysize());
				
				// We only accept points that are at least one half of the grid to the source
				while(MathMacro.EuclideanDistSquare(x, y, this.GetCenterPos()) < MathMacro.EuclideanDistSquare(0, 0, new IntC(Xsize()/2, Ysize()/2))){
					x = RandomInt(Xsize());
					y = RandomInt(Ysize());
				}
			} else if (m_configInit == GROUPED){ 
				x= RandomInt(Xsize()/6)+3*Xsize()/6;
				y= RandomInt(Ysize()/6)+3*Ysize()/6;
			}
			else {
				Macro.FatalError("Bad initial situation type");
			}

			IntC pos= new IntC(x, y);

			AmoebaAgent agent= new AmoebaAgent(this, pos);
			m_amoebaList.add(agent);
		}
	}



	//--------------------------------------------------------------------------
	//- signals
	//--------------------------------------------------------------------------

	@Override
	public void ReceiveSignal(FLsignal sig) {
		switch(sig){
		case init:
			super.sig_Init();
			InitArray();
			break;
		case nextStep:
			NextStep();
			break;
		case update:
			break;
		}
		// detection step
		MakeDetection();
	}

	//--------------------------------------------------------------------------
	//- get / set
	//--------------------------------------------------------------------------
	
	public double GetBoudingBoxRatio(){
		double res = 0;
		
		int X = this.GetXYsize().X();
		int Y = this.GetXYsize().Y();
		int xmax = -1;
		int xmin = X + 1;
		int ymax = -1;
		int ymin = Y + 1;

		
		for(int i = 0; i < this.GetAgentNumber(); i++){
			int x_agent = this.GetAgent(i).GetPos().X();
			int y_agent = this.GetAgent(i).GetPos().Y();
			
			if(x_agent > xmax){
				xmax = x_agent;
			}
			if(x_agent < xmin){
				xmin = x_agent;
			}
			
			if(y_agent > ymax){
				ymax = y_agent;
			}
			if(y_agent < ymin){
				ymin = y_agent;
			}
		}
		
		res = ( (double) (xmax-xmin) * (ymax-ymin) ) / ((double)(X * Y));
		
		return res;
	}

	public int GetAgentNumber() {
		return m_Namoebae.GetVal();
	}

	public AmoebaAgent GetAgent(int agentIndex) {
		return m_amoebaList.get(agentIndex);
	}

	public IntC GetAgentPos(int agentIndex) {
		return m_amoebaList.get(agentIndex).GetPos();
	}

	public String GetName() {
		return NAME;
	}

	public IntC GetCenterPos() {
		return m_emittingCenter;
	}

	AmoebaeGridCell GetCell(IntC pos) {
		return GetCell(pos.X(),pos.Y());
	}

	private final AmoebaeGridCell GetCell(int x, int y) {
		return m_cell[x][y];
	}

	final private int GetCellState(IntC pos) {
		return GetCellState(pos.X(), pos.Y());
	}

	final public int GetCellState(int x, int y) {
		return m_cell[x][y].GetState();
	}

	/*private void SetCellState(int x, int y, int state) {
		m_cell[x][y].Set= state;
	}*/

	private int GetCellStateAgent(AmoebaAgent agent) {
		IntC pos= agent.GetPos();
		return GetCellState(pos);
	}

	private void ResetCellState(int x, int y, int i) {
		m_cell[x][y].Reset();
	}


	public GridViewer GetSystemViewer(IntC CellSizeForViewer) {
		return new AmoebaeInfotaxisViewer(this, CellSizeForViewer);
	}
	
	public String[] GetPlotterSelection() {
		return PlotterSelectControl.INFOTAXIS_SELECT;
	}

	/**
	 * Get the global probability of a cell becoming excited 
	 * and so emitting a signal.
	 * @return a probability as a double
	 */
	public double getEmissionProbability() {
		double sumInverseProbabilities = 1;
		
		for(AmoebaAgent a : m_amoebaList){
			sumInverseProbabilities = sumInverseProbabilities * (1 - this.getEmissionProbabiltyFor(a));
		}
		
		return 1 - sumInverseProbabilities;
	}

	/**
	 * get the probability that the n-th amoebae sets the cell
	 * on which she is to excited and thus emits a signal.
	 * @param a the amoebae
	 * @return a probability as a double
	 */
	public double getEmissionProbabiltyFor(AmoebaAgent a){
		IntC pos = a.GetPos();
		AMOEBASTATE state = a.GetState();
		double result = 0;
		
		if(state == AMOEBASTATE.receiver){ //not excited amoebae
			if(this.GetCell(pos).IsNeutralState()){ //the amoebae is on a neutral cell
				result = (1 - m_pAgitation.GetVal()) * this.GetCell(pos).m_detectProba;
			}
		}
		else { //excited amoebae
			if(this.GetCell(pos).IsNeutralState()){ //the amoebae is on a neutral cell
				result = (1 - m_pAgitation.GetVal()) * m_pReemission.GetVal();
			}
		}
		
		return result;
	}
	
	public boolean HasDetectionOccured() {
		return m_detected;
	}


	//--------------------------------------------------------------------------
	//- GFX
	//--------------------------------------------------------------------------

	FLPanel GetSpecicPanel(){
		FLPanel p1 = FLPanel.NewPanelConstruct(
				m_Namoebae,
				m_pAgitation,
				m_pReemission,
				m_Kdetec,
				new InitConfController());

		m_Namoebae.GetControl().minimize();
		m_pAgitation.GetControl().minimize();
		m_pReemission.GetControl().minimize();
		m_Kdetec.GetControl().minimize();
		
		FLPanel p2 = FLPanel.NewPanelConstruct(
				new CopyInfo(),
				new PasteInfo(),
				this.GetRandomizer());

		p1.setOpaque(false);
		p2.setOpaque(false);
		m_panel = FLPanel.NewPanelVertical(p1, p2);
		m_panel.setOpaque(false);
		
		return m_panel;
	}
	
	private class CopyInfo extends FLActionButton {
		public CopyInfo(){
			super(LBL_BUTTONCOPYINFO);
		}
		
		public void DoAction(){
			AmoebaeInfotaxisParamParser p = new AmoebaeInfotaxisParamParser(m_Namoebae.GetVal(), m_XYsize.X(), m_configInit, m_pAgitation.m_val, m_pReemission.m_val, m_Kdetec.m_val, GetSeed());
			
			Clipboard c = Toolkit.getDefaultToolkit().getSystemClipboard();
			StringSelection s = new StringSelection(p.toString());
			c.setContents(s, s);
		}
	}
	
	private class PasteInfo extends FLActionButton {
		public PasteInfo(){
			super(LBL_BUTTONPASTEINFO);
		}
		
		public void DoAction() {
			AmoebaeInfotaxisParamParser p = new AmoebaeInfotaxisParamParser();
			
			Clipboard c = Toolkit.getDefaultToolkit().getSystemClipboard();
			
			Transferable t = c.getContents( null );
			
			try{
				if( t.isDataFlavorSupported( DataFlavor.stringFlavor ) ){
					Object o = t.getTransferData( DataFlavor.stringFlavor );
					String data = (String)t.getTransferData( DataFlavor.stringFlavor );
					
					p.parseParam(data);
					
					SetSeed(p.getSeed());
					
					m_pAgitation.SetVal(p.getpA());
					m_pReemission.SetVal(p.getpR());
					m_Kdetec.SetVal(p.getK());
					
					m_Namoebae.SetVal(p.getN());
					m_configInit = p.getI();
					
					
					
				}
			}
			catch(Exception e){
				//Do nothing ...
			}
		}
	}

	private class InitConfController extends IntController {

		public InitConfController() {
			super("Init Configuration");
		}

		@Override
		protected int ReadInt() {
			return m_configInit;
		}

		@Override
		protected void SetInt(int val) {
			m_configInit = val;
		}
		
	}

	public boolean ReemissionEvent() {
		return RandomEventDouble(m_pReemission);
	}



	public boolean RandomMoveEvent() {
		return RandomEventDouble(m_pAgitation);
	}



	public IntC RandomDir() {
		return DXY[ RandomInt(NDIR)];
	}

}
