package models.MASmodels.amoebae;

import java.util.ArrayList;

public class AmoebaeGridCell {

	static final int EXCITED = 2;
	static final int RECOVERING = 1;
	static final int NEUTRAL = 0;


	enum ACINFL { none, activation, propagation }

    private int m_state; 					// reaction-diffusion layer
	private ACINFL m_infl;			// buffer for "influences"
	public double m_detectProba;	//detection probability
	ArrayList<AmoebaeGridCell> m_neighb;	//neighbourhood

	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------

	public AmoebaeGridCell() {
		m_neighb= new ArrayList<AmoebaeGridCell>();
	}

	public void AddNeighb(AmoebaeGridCell neighb){
		m_neighb.add(neighb);
	}

	//--------------------------------------------------------------------------
	//- influences
	//--------------------------------------------------------------------------

	public void Reset() {
		m_state=0;
		m_infl= ACINFL.none;
	}

	public void InfluenceActivateByHit() {
		m_infl=ACINFL.activation;
	}

	public void ClearActivation() {
		m_infl= ACINFL.none;
	}

	public void PropagateByNeighb() {
		if (m_infl!=ACINFL.activation){
			m_infl= ACINFL.propagation;
		}
	}

	
	public boolean HasAnExcitedNeighbour() {
		for (AmoebaeGridCell cell : m_neighb){
			if (cell.m_state == EXCITED){
				return true;
			}
		}
		return false;
	}

	//--------------------------------------------------------------------------
	//- reactions
	//--------------------------------------------------------------------------
	public boolean IsInflExcited() {
		return (m_infl==ACINFL.activation) || (m_infl==ACINFL.propagation);
	}

	public void MakeReaction() {
		int st= m_state;
		int nextSt= st;
		switch (st){
		case NEUTRAL:
			if (IsInflExcited()){
				nextSt= EXCITED;
			}
			break;
		case RECOVERING:
			nextSt= NEUTRAL;
			break;
		case EXCITED:
			nextSt= RECOVERING;
			break;
		}
		m_state= nextSt;

	}

	public int GetState() {
		return m_state;
	}

	public boolean IsStateExcited() {
		return m_state==EXCITED;
	}

	public boolean IsNeutralState() {
		return m_state==NEUTRAL;
	}



}
