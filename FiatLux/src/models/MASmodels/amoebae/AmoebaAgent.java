package models.MASmodels.amoebae;

import components.types.IntC;
import components.types.IntCm;

public class AmoebaAgent {

	enum AMOEBASTATE { emitter, receiver }

	private static final IntC NOMOVE = new IntC(0, 0);

	//--------------------------------------------------------------------------
	//- attributes
	//--------------------------------------------------------------------------

	private final AmoebaeInfotaxisSystem m_sys;
	private final IntCm m_pos;
	private IntC m_inflDxy;
	private AMOEBASTATE m_state;

	//--------------------------------------------------------------------------
	//- construction
	//--------------------------------------------------------------------------

	public AmoebaAgent(AmoebaeInfotaxisSystem system, IntC pos) {
		m_sys= system;
		m_pos= pos.DuplicateM();
		m_inflDxy= NOMOVE;
		m_state= AMOEBASTATE.receiver;
	}

	//--------------------------------------------------------------------------
	//- influences
	//--------------------------------------------------------------------------
	public void MakeInfluenceStep() {

		if (m_sys.RandomMoveEvent()){ // RANDOM MOVE
			m_inflDxy= m_sys.RandomDir();	
		} else if (m_sys.DetectionEvent(m_pos) || // "hit"
				(m_state==AMOEBASTATE.emitter)){  //  emitter state
			// hit
			m_sys.GetCell(m_pos).InfluenceActivateByHit(); // influence on the cell
			m_state=AMOEBASTATE.emitter; // we immediately update state to emitter
		} else {
			// no hit
			if (m_sys.GetCell(m_pos).IsNeutralState()){
				IntC dxy= m_sys.GetMovementToExcitedNeighb(m_pos);
				if (dxy != null){
					m_inflDxy= dxy;
				} else {
					m_inflDxy= NOMOVE;
				}
			}
		}
	}

	//--------------------------------------------------------------------------
	//- reactions
	//--------------------------------------------------------------------------

	public void ReactionStep() {
		ReactionUpdateState();
		ReactionMovement();
	}

	/** reaction part **/
	public void ReactionUpdateState(){
		if (m_state==AMOEBASTATE.emitter){
			boolean reemit= m_sys.ReemissionEvent();
			m_state= reemit?AMOEBASTATE.emitter:AMOEBASTATE.receiver;
		} // implicit: receiver state remains receiver 
	}

	/** reaction part **/
	public void ReactionMovement(){
		m_pos.AddBlockingBorders(m_inflDxy, m_sys.GetXYsize());
		m_inflDxy= NOMOVE;
	}


	//--------------------------------------------------------------------------
	//- get / set
	//--------------------------------------------------------------------------


	public IntC GetPos() {
		return m_pos;
	}

	public AMOEBASTATE GetState(){
		return m_state;
	}

	public boolean IsReceiver() {
		return m_state==AMOEBASTATE.receiver;
	}





}
