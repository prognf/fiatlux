package models.MASmodels.amoebae;

import javax.swing.JComponent;

import components.types.IntC;
import components.types.Monitor;
import experiment.samplers.SuperSampler;
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.AutomatonViewer;

public class AmoebaeInfotaxisSampler extends SuperSampler {
	AmoebaeInfotaxisSystem 	m_system;	// main system
	AmoebaeInfotaxisViewer 	m_viewer; 	// monitor

	public AmoebaeInfotaxisSampler(AmoebaeInfotaxisSystem system, IntC pixSize) {
		m_system= system;
		m_viewer= new AmoebaeInfotaxisViewer(m_system, pixSize);
		addMonitor(m_viewer);
	}
	
	@Override
	public String GetSimulationInfo() {
		return "t:" + m_system.GetTime();
	}

	@Override
	public String GetTopologyInfo() {
		return "sz:" + m_system.GetXYsize();
	}

	@Override
	public int GetTime() {
		return m_system.GetTime();
	}

	@Override
	public FLPanel GetSimulationWidgets() {
		return FLPanel.NewPanel();
	}

	@Override
	public FLPanel GetSimulationRuleControls() {
		return m_system.GetSpecicPanel();
	}

	@Override
	public JComponent GetSimulationView() {
		return m_viewer;
	}

	@Override
	public String[] GetPlotterSelection() {
		return m_system.GetPlotterSelection();
	}

	@Override
	protected Monitor GetLinkedSystemAsMonitor() {
		return m_system;
	}

	@Override
	protected String GetModelName() {
		return m_system.GetName();
	}

	public AutomatonViewer GetViewer() {
		return m_viewer;
	}

	public double getEmissionProbability() {
		return m_system.getEmissionProbability();
	}

}
