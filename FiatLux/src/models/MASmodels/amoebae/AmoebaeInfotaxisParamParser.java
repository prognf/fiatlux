package models.MASmodels.amoebae;

import java.util.Locale;

import main.Macro;
import main.commands.CommandParser;

public class AmoebaeInfotaxisParamParser {
	String CLASSNAME = "AmoebaeInfotaxisParamParser";
	
	int N;
	int L;
	int I;
	
	double pA;
	double pR;
	double K;
	
	int seed;
	
	public AmoebaeInfotaxisParamParser(int p_N, int p_L, int p_I, double p_pA, double p_pR, double p_K, int p_seed){
		this.N = p_N;
		this.L = p_L;
		this.I = p_I;
		
		this.pA = p_pA;
		this.pR = p_pR;
		this.K = p_K;
		
		this.seed = p_seed;
	}
	
	public AmoebaeInfotaxisParamParser(){
		//Does nothing
	}
	
	public String toString(){
		return String.format(Locale.US, "N=%d L=%d I=%d pA=%.2f pR=%.2f K=%.2f S=%d", N, L, I, pA, pR, K, seed);
	}
	
	public int getN(){ return this.N; }
	
	public int getL(){ return this.L; }
	
	public int getI(){ return this.I; }
	
	public double getpA(){ return this.pA; }
	
	public double getpR(){ return this.pR; }
	
	public double getK(){ return this.K; }
	
	public int getSeed(){ return this.seed; }
	
	public void parseParam(String param){
		CommandParser p = new CommandParser(CLASSNAME, param.split(" "));
		this.N = p.IParse("N=");
		this.L = p.IParse("L=");
		this.I = p.IParse("I=");
		
		this.pA = p.DParse("pA=");
		this.pR = p.DParse("pR=");
		this.K = p.DParse("K=");
		
		this.seed = p.IParse("S=");
	}
	
	public static void main(String[] argv){
		Macro.print("Starting AmoebaeInfotaxisParamParser testing");
		AmoebaeInfotaxisParamParser parser = new AmoebaeInfotaxisParamParser();
		parser.parseParam("N=20 L=50 I=2 pA=0.2 pR=0.8 K=0.5 S=788635");
		
		assert(parser.getN() == 20);
		assert(parser.getL() == 50);
		assert(parser.getI() == 2);
		assert(parser.getpA() == 0.2);
		assert(parser.getpR() == 0.8);
		assert(parser.getK() == 0.5);
		assert(parser.getSeed() == 788635);
		
		AmoebaeInfotaxisParamParser parser2 = new AmoebaeInfotaxisParamParser(30, 40, 3, 0.1, 0.7, 0.3, 8993);
		assert(parser2.toString() == "N=30 L=40 I=3 pA=0.10 pR=0.70 K=0.30 S=8993");
		Macro.print("Testing done");
	}
}
