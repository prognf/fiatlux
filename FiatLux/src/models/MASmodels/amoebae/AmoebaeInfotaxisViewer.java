package models.MASmodels.amoebae;

import components.types.IntC;
import grafix.gfxTypes.FLColor;
import grafix.viewers.GridViewer;

/*--------------------
 *--------------------*/
public class AmoebaeInfotaxisViewer extends GridViewer {

	/*--------------------
	 * attributes
	 --------------------*/

	final static FLColor 
	CENTERCOL= FLColor.c_blue, 
	AGENTCOL_R= FLColor.c_black,
	AGENTCOL_E= FLColor.c_purple;


	AmoebaeInfotaxisSystem m_system; // related system

	/*--------------------
	 * constructor
	 --------------------*/
	public AmoebaeInfotaxisViewer(AmoebaeInfotaxisSystem in_system, IntC in_PixSize) {
		super(in_PixSize, in_system);
		m_system = in_system;

		// colors
		CreatePalette(FLColor.c_white, FLColor.c_orange, FLColor.c_red);
		SetMargins(2, 2);
	}

	/*--------------------
	 * Other methods
	 --------------------*/


	/* main component 
	 * X-axis: from left to right
	 * Y-axis :from TOP to BOTTOM !
	 * */
	@Override
	protected void DrawSystemState() {
		// cells
		DrawBackground();
		SetColorIndex(0);
		for (int x = 0; x < getXsize(); x++){
			for (int y = 0; y < getYsize(); y++) {
				int state= m_system.GetCellState(x, y);
				DrawSquareColor(x, y, state);	
				//DrawSquare(x, y);
			}
		}

		// agents
		int n = m_system.GetAgentNumber();
		for (int agentIndex = 0; agentIndex < n; agentIndex++) {
			AmoebaAgent ag = m_system.GetAgent(agentIndex);
			//IntC cell= m_system.GetAgentPos(agentIndex);
			IntC cell= ag.GetPos();
			FLColor colorA= ag.IsReceiver()?AGENTCOL_R:AGENTCOL_E; 
			SetColor(colorA);
			DrawCellCircle(cell);
		}
		// center
		SetColor(CENTERCOL);
		IntC posC= m_system.GetCenterPos();
		DrawDiamond(posC);
	}


}
