package models.MASmodels.randomMove;

import components.types.IntC;
import grafix.gfxTypes.IntField;
import grafix.gfxTypes.elements.FLList;
import grafix.gfxTypes.elements.FLPanel;
import initializers.MASinit.MultiAgentInitializer;
import main.Macro;

public class RandomMoveInitializer extends MultiAgentInitializer {

	
	final static String TXT_NTURMITES= "N particules", S_SQUARELEN= "square len";
	final static int DEF_NTURMITES= 2, DEF_SQUARELEN= 20; // space
	
	
	final static String S_RANDOM="random", S_SQUARE="square";
	final static int RANDOM=0, SQUARE=1, DEF_SELECT=1;
	final static String[] stratego= { S_RANDOM, S_SQUARE};
	
	FLList strategy= new FLList(stratego);

	/*--------------------
	 * attributes
	*************************************************************************/

	RandomMoveSystem m_system;
	IntField mF_Nturmites= new IntField(TXT_NTURMITES, 3, DEF_NTURMITES);
	IntField mF_SquareLen= new IntField(S_SQUARELEN, 3, DEF_SQUARELEN);
	
	/*--------------------
	 * construction
	 --------------------*/
	public RandomMoveInitializer(RandomMoveSystem in_system) {
		super(in_system);
		m_system= in_system;
		strategy.setSelectedIndex(DEF_SELECT);
	}

	
	/*--------------------
	 * main
	 --------------------*/

	public void SubInit() {
		int select= strategy.GetSelectedItemRank();
		m_system.InitArray();
		switch (select) {
		case RANDOM:
			RandomInit();
			break;
		case SQUARE:
			SquareInit();
			break;
		default:
			Macro.FatalError("case not taken into account");
		}

	}

	/* nothing to do (see constructor ) */
	private void RandomInit() {
		int nagent= mF_Nturmites.GetValue();
		super.CreateAgents(nagent);
		for (int agentnum=0; agentnum< nagent; agentnum++){
			IntC pos = GetRandomizer().RandomIntCouple(GetXsize(),GetYsize() );
			m_system.SetAgentPos(agentnum, pos);
		}
	
	}
	
	private void SquareInit(){
		int len= mF_SquareLen.GetValue();
		int nagent= len * len ;
		super.CreateAgents(nagent);
		int x0 = (GetXsize() - len ) / 2,
			y0 = (GetYsize() - len ) / 2;

        int agentnum=0;
		for (int y= 0; y < len; y++) {
			for (int x= 0; x < len; x++) {
				int xpos = (x0 + x) % GetXsize();
				int ypos = (y0 + y) % GetYsize();
				m_system.SetAgentPos(agentnum, new IntC(xpos, ypos));
				agentnum++;
			}			
		}
	} 

	/*--------------------
	 * get / set
	 --------------------*/
	public FLPanel GetSpecificPanel() {
		FLPanel panel = FLPanel.NewPanelVertical(strategy, mF_Nturmites, mF_SquareLen);
		panel.setOpaque(false);
		return panel;
	}
	

}
