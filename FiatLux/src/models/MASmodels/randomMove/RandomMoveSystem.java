package models.MASmodels.randomMove; 
import architecture.multiAgent.tools.DIR;
import architecture.multiAgent.tools.EnvCell;
import architecture.multiAgent.tools.MultiAgentSystem;
import architecture.multiAgent.tools.SimpleMovingAgent;
import architecture.multiAgent.tools.SituatedAgent;
import components.types.IntC;
import grafix.gfxTypes.elements.FLPanel;
import initializers.MASinit.MultiAgentInitializer;
import models.MASmodels.EnvCellSimple;
import models.MASmodels.turmite.TurmiteAgent;
/*******************************************************
 * implements an SMA model
 * @author Nazim Fates
 ********************************************************/ 
 public class RandomMoveSystem extends MultiAgentSystem {  
		
	 final static String MODELNAME="Simple Moving Agents System";
	

	/*--------------------
	 * Attributes
	 --------------------*/
	
	
	/*--------------------
	 * construction
	--------------------*/
	
	public RandomMoveSystem(IntC in_size){
		super(in_size);
	}
	
	/*--------------------
	 * main
	--------------------*/

	/*--------------------
	 * cells influences
	--------------------*/
	
	/* what is the target cell (influence) of the agent ? */
	final public EnvCell GetAgentTarget(SituatedAgent situatedAgent){
		SimpleMovingAgent agentRM= (SimpleMovingAgent) situatedAgent;
		return agentRM.GetTarget();
	}
	
	/*--------------------
	 * override
	--------------------*/
	@Override
	/** View/Control **/
	public FLPanel GetViewControl(){
		FLPanel panel = new FLPanel();
		return	panel; 
	}
	
	@Override
	public String GetModelName() {
		return MODELNAME;
	}
	
	/*--------------------
	 * get / set
	--------------------*/

	protected TurmiteAgent GetTurmiteAgent(int agentnum){
		return (TurmiteAgent) GetAgent(agentnum);
	}	

	/* used by initializer */
	public void SetAgentOrientation(int agentnum, DIR dir) {
		SimpleMovingAgent agent= (SimpleMovingAgent) GetAgent(agentnum);
		agent.SetOrientation(dir);
	}

	@Override
	public EnvCell GetNewEnvCell(int x, int y) {
		return new EnvCellSimple(x,y);
	}

	@Override
	public SituatedAgent GetNewAgent(int agentnum) {
		return new RandomMoveAgent(this);
	}

	@Override
	public MultiAgentInitializer GetNewInitializer() {
		return new RandomMoveInitializer(this);
	}


}
