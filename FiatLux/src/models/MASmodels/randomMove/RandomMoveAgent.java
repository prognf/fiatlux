package models.MASmodels.randomMove;

import java.util.ArrayList;

import architecture.multiAgent.tools.DIR;
import architecture.multiAgent.tools.INFLDIR;
import architecture.multiAgent.tools.SimpleMovingAgent;
import main.Macro;


public class RandomMoveAgent extends SimpleMovingAgent {

	/*--------------------
	 * attributes
	 --------------------*/


	protected INFLDIR m_infl = null;


	/*--------------------
	 * construction
	--------------------*/

	public RandomMoveAgent(RandomMoveSystem in_system) {
		super(in_system);
	}


	/*--------------------
	 * reset
	--------------------*/

	public void sig_AgentReset() {
		m_infl= null;
	}



	/*--------------------
	 * influences
	--------------------*/

	public void sig_AgentPerceivePutInfluence(){		

		// perception
		ArrayList<DIR> availableNeighbs= GetFreeAvailableNeighbs(GetSystem());

		// decision
		int size = availableNeighbs.size();
		if ((size>0)){
			int randomElement= GetSystem().GetRandomizer().RandomInt(size);
			m_infl= availableNeighbs.get(randomElement).ToInflDir();
		} else { // size == 0
			m_infl= INFLDIR.HERE;
		}
		//Macro.Debug(":"+m_infl);
		GetSystem().PutMoveInfl(this, m_infl);

	}
	/*--------------------
	 * update
	--------------------*/

	/* empty */
	public void sig_InternalUpdate() {

	}

	@Override
	public void sig_PositionObservableUpdate() {

		switch (m_UpdateMode) {
		case CONFLICT:	
			break;			
		case NOCONFLICT:
			DoMove();
			break;
		default:
			Macro.FatalError("updatemode not expected:" + m_UpdateMode);
		break;
		}
		m_UpdateMode= null; // security
	}

    @Override
	public void SetOrientation(DIR dir) {
		// DO NOTHING
		Macro.FatalError("Orientation set for Random Move agent");
	}

}
