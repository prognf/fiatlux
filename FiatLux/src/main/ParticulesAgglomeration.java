package main;

public interface ParticulesAgglomeration {

	
	int GetSize();
	
	int GetState(int pos);
}
