package main;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/************************************
 * The class  FLAbout is an About Dialog Box
 * @author Nazim Fates
 *******************************************/ 


//v 0.1.0 : initial release
//v 0.1.1 : the software is divided into 3 packages : Resources, CellularAutomata & CAModels
//v 0.1.2 : the Identity Model is added + "about" box + Main Package
//v 0.1.3 : Data analysis is integrated + model is changed to one layer to
//avoid ambiguous neighbourhood references
//v 0.1.4 : the code is cleaned
//v 0.2.1 : the models includes CFGs
//v 0.2.2 : before new distribution
//v 0.3.1 : the model is cleaner with a Sampler class
//v 0.4.1 : working on the classification objects
//v 0.4.2 : classification of objects is stabilized
//v 1.1 : before presentation to MIM students
//v 1.2 : after Alexis' modifications
//v 2.1 : Loria improvements
//v 2.2 : Multi-agents
//v 2.3 : films allowed
//v 2.4 : improved presentation (non clickable elements) + reading of Tcodes
//v 2.5 : cells are clickable + cleaning of Init and History (reals)
//v 2.6 : grand menage de printemps !
//V 2.7 : generation des simu Dicty + debut stage Romain Cornu
//v 2.8 : improvement of interface (no enter key, local rule applicable by click)
//drop function in 1D + Fuzzy CAs (with Antoine Spicher), 
//v 2.9 : coding of random walk with Antoine Spicher
//        possibility to visualize the unstable cells
//v 3.0 : interface with an association model / topology
//v 3.1 : new interface ; internship Alexandre Bryszkowski
//v 3.2 : double simulation window available
//v 3.3 : SMA Turmite simulation (single & double)
//v 3.4 : Refactoring operation (arrays & co.)
//v 3.5 : new user interface (more horizontal) 
//v 3.6 : adding Interacting Particle Systems and LGCA
//v 3.7 : "monitor system", more things on IPS and LGCA
//v 4.0 : distribution with the Cecill licence
//v 4.1 : améliorations code + mise à jour sequentielle
//v 5.1 : distribution après AUTOMATA 2015
//v 6.0 : après les développements de Nicolas Gauville
//v 6.1 : après les développements d'Océane Chazé

//v 7.1 : reprise de l'interface, stochosLine, etc.

public class FLAbout extends JOptionPane{

	static public void Show(){
		JFrame frame= new JFrame();
		String s= main.FiatLux.FIATLUX_VERSION ; 
		s += "\nAuthor:  Nazim Fates,  Inria researcher, contact: Nazim.Fates@inria.fr \n ";
		s += "FiatLux is a free software distributed under the Cecill licence ";
				s += "visit : \nhttp://https://project.inria.fr/fiatlux/";
		s += "\nEnjoy & please send feedback to the author if you use this simulator.";
		showMessageDialog(frame, s);
	}
}

