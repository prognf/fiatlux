package main;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static main.tables.ModelSelectorAllCA.CATYPE_list.valueOf;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.AccessControlException;
import java.util.Properties;

import components.types.FLString;
import main.tables.ModelSelectorAllCA;
import main.tables.ModelSelectorAllCA.CATYPE_list;


/**
 * This Properties class determines the configuration for FiatLux
 * BEWARE : new constants have to be defined BOTH in default.properties AND config.properties
 * @author Olivier Boure
 */
public class FiatLuxProperties extends Properties {
	public static String m_filepath;
	public final static String m_defaultpath = "main/default.properties";
	public final static String m_userpath = "user.properties";
	public final static String m_userpathPrevious = "user-previous.properties";

	private static FiatLuxProperties instance;
	public static FiatLuxProperties getInstance() {
		if (instance == null) {
			instance = new FiatLuxProperties(CONFIG_FILEPATH);
		}

		return instance;
	}

	/* DEFAULTS ----------------------------------------*/

	public static int VERBOSE = 4;
	//public static boolean ENABLE_DEBUG = true;
	public static boolean ENABLE_WARNING = true;

	/**
	 * The system type on start
	 */
	private static int m_SYSTYPE = 0;
	public enum SYSTYPE_list {CA, LGCA, MAS, IPS}

	public enum SIDEBAR_TABS {INITIALIZER, VIEWER, SYNCHRONISM, MEASURES, RECORD}

	/**
	 * The cellular automata type on start
	 */
	public static CATYPE_list CATYPE ;

	public static int LOOK_AND_FEEL = 0;

	/* default selection in tabs */
	public static int GeneralCA_TAB_DEF_SELECTION = 0;
	public static int Particle_TAB_DEF_SELECTION = 0;
	public static int Linear_TAB_DEF_SELECTION = 0;
	public static int Variants_TAB_DEF_SELECTION = 0;
	public static int Stochastic_TAB_DEF_SELECTION = 0;
	public static int AsynchInfo_TAB_DEF_SELECTION = 0;
	public static int Tests_TAB_DEF_SELECTION = 0;
	public static int Diagnostic_TAB_DEF_SELECTION = 0;
	public static int IpsModel_TAB_DEF_SELECTION = 1;

	/* Simulation panel tabs */
	public static boolean SHOW_INITIALIZER_PANEL = true;
	public static boolean SHOW_VIEWER_PANEL = true;
	public static boolean SHOW_SYNCHRONISM_PANEL = true;
	public static boolean SHOW_MEASURES_PANEL = false;
	public static boolean SHOW_RECORD_PANEL = false;
	public static boolean MINIMIZED_MODE = false;

	/* default size type selection */
	// preset topo size selection (small , med, large, etc.)
	public enum sizeType { small, medium, large}

	public static sizeType TOPOLOGY_select = sizeType.medium;

	public static int DEF_LWINDOW;


	// default selection setting
	// small / medium / large
	public final static int [] 
			Nsize1D = {10, 50, 400},
			Tsize1D = {15, 30, 300},
			border1D = {0, 0, 0};


	/* default grid sizes */
	public static int sL2D = 4, mL2D = 24, bL2D = 100;
	public static int border= 0;

	// for simulation panel (see ChronosWindow)
	public static int DEF_STROBVALUE = 1;
	
	// for default synchrony rate
	public static int DEF_SYNCHRATE = 100; // in %
	
	public final static String CONFIG_FILEPATH = "config.properties";

	// tell if the preferences are stored in the "user.properties" files (in the jar directory)
	public static boolean SAVE_PREFERENCES = false;

	public FiatLuxProperties(String filepath) {
		// Load default config
		try {
			ClassLoader loader = ClassLoader.getSystemClassLoader();
			InputStream default_is = loader.getResourceAsStream(m_defaultpath);

			File f = new File(m_userpath);
			if (f.exists()) {
				default_is = new FileInputStream(f);
				SAVE_PREFERENCES = true;
			}


			load(default_is);
			loadProperties();
		} catch (IOException ioe) {
			Macro.FatalError("Default properties not loaded !");
		} catch (AccessControlException ace) {
			Macro.print("Running in APPLET mode: ClassLoader disabled");
		}

		// Load user config
		m_filepath = filepath;
		try {
			load(new FileInputStream(filepath));
			Macro.print("Config file loaded");
			loadProperties();
		} catch (IOException ioe) {
			Macro.print(5, "File <" + filepath + "> not found");
		} catch (AccessControlException ace) {
			Macro.print("Running in APPLET mode: FileInputStream disabled");
		}

	}

	public static void enableSave() {
		try {
			ClassLoader loader = ClassLoader.getSystemClassLoader();
			InputStream default_is = loader.getResourceAsStream(m_defaultpath);
			File f = new File(m_userpath);
			Files.copy(default_is, Paths.get(f.getPath()), REPLACE_EXISTING);
			SAVE_PREFERENCES = true;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void disableSave() {
		File f = new File(m_userpath);
		File g = new File(m_userpathPrevious);
		if (f.exists()) {
			f.renameTo(g);
		} else {
			System.out.println(f.getPath() + " " + f.getName() + " does not exist !");
		}

		SAVE_PREFERENCES = false;
	}

	/* ACCESS ----------------------------------------*/

	public static int GetVerbose() {
		return VERBOSE;
	}

	public static void SetVerboseLevel(int level) {
		VERBOSE = level;
	}

	/** used by SimulationWindowCreator only **/
	public static int GetSystemType() {
		return m_SYSTYPE;
	}

	public static void SetSystemType(int value) {
		m_SYSTYPE = value;
		getInstance().Write("systype", SYSTYPE_list.values()[value].toString());
	}

	public static ModelSelectorAllCA.CATYPE_list GetDefCA() {
		return CATYPE;
	}

	public static void SetDefCA(ModelSelectorAllCA.CATYPE_list input) {
		CATYPE = input;
		getInstance().Write("catype", input.toString());
	}

	public void setListSelection(AAAMAIN_MODEL_TABLE.MT list, int value) {
		switch (list) {
			case GENERAL: FiatLuxProperties.GeneralCA_TAB_DEF_SELECTION = value; break;
			case PARTICLE: FiatLuxProperties.Particle_TAB_DEF_SELECTION = value; break;
			case LINEAR: FiatLuxProperties.Linear_TAB_DEF_SELECTION = value; break;
			case VARIANTS: FiatLuxProperties.Variants_TAB_DEF_SELECTION = value; break;
			case STOCHASTIC: FiatLuxProperties.Stochastic_TAB_DEF_SELECTION = value; break;
			case TESTS: FiatLuxProperties.Tests_TAB_DEF_SELECTION = value; break;
			case DIAGNOSTIC: FiatLuxProperties.Diagnostic_TAB_DEF_SELECTION = value; break;
		}

		saveListSelection();
	}

	public void saveListSelection() {
		Write("select-general", "" + FiatLuxProperties.GeneralCA_TAB_DEF_SELECTION);
		Write("select-particle", "" + FiatLuxProperties.Particle_TAB_DEF_SELECTION);
		Write("select-linear", "" + FiatLuxProperties.Linear_TAB_DEF_SELECTION);
		Write("select-variants", "" + FiatLuxProperties.Variants_TAB_DEF_SELECTION);
		Write("select-stochastic", "" + FiatLuxProperties.Stochastic_TAB_DEF_SELECTION);
		Write("select-asynchInfo", "" + FiatLuxProperties.AsynchInfo_TAB_DEF_SELECTION);
		Write("select-ips", "" + FiatLuxProperties.IpsModel_TAB_DEF_SELECTION);
		Write("select-tests", "" + FiatLuxProperties.Tests_TAB_DEF_SELECTION);
		Write("select-diagnostic", "" + FiatLuxProperties.Diagnostic_TAB_DEF_SELECTION);
	}

	public boolean getDefaultSidebarTabStatus(SIDEBAR_TABS tab) {
		switch (tab) {
			case INITIALIZER: return SHOW_INITIALIZER_PANEL;
			case VIEWER: return SHOW_VIEWER_PANEL;
			case SYNCHRONISM: return SHOW_SYNCHRONISM_PANEL;
			case MEASURES: return SHOW_MEASURES_PANEL;
			case RECORD: return SHOW_RECORD_PANEL;
		}

		return false;
	}

	public void setDefaultSidebarTabStatus(SIDEBAR_TABS tab, boolean value) {
		switch (tab) {
			case INITIALIZER: SHOW_INITIALIZER_PANEL = value; break;
			case VIEWER: SHOW_VIEWER_PANEL = value; break;
			case SYNCHRONISM: SHOW_SYNCHRONISM_PANEL = value; break;
			case MEASURES: SHOW_MEASURES_PANEL = value; break;
			case RECORD: SHOW_RECORD_PANEL = value; break;
		}

		saveDefaultSidebarTabsStatus();
	}

	public void setMinimizedMode(boolean minimized) {
		MINIMIZED_MODE = minimized;
		Write("minimized", (FiatLuxProperties.MINIMIZED_MODE ? "1" : "0"));
	}

	public void saveDefaultSidebarTabsStatus() {
		Write("tab-initializer", (FiatLuxProperties.SHOW_INITIALIZER_PANEL ? "1" : "0"));
		Write("tab-viewer", (FiatLuxProperties.SHOW_VIEWER_PANEL ? "1" : "0"));
		Write("tab-synchronism", (FiatLuxProperties.SHOW_SYNCHRONISM_PANEL ? "1" : "0"));
		Write("tab-measures", (FiatLuxProperties.SHOW_MEASURES_PANEL ? "1" : "0"));
		Write("tab-record", (FiatLuxProperties.SHOW_RECORD_PANEL ? "1" : "0"));
	}

	/* INTERNAL --------------------------------------*/
	/* MAIN FUNCTION --------------------------------------*/

	private void loadProperties() {
		// Verbose
		VERBOSE = Integer.parseInt(getProperty("verbose"));
		Macro.print(2,"Verbose level:" + VERBOSE);

		// System type tab
		String prop1 = getProperty("systype");
		try {
			m_SYSTYPE = SYSTYPE_list.valueOf(prop1).ordinal();
		} catch (IllegalArgumentException e){
			String lstLegal= FLString.ToString(SYSTYPE_list.values());
			Macro.FatalError("argument: "+prop1+ " not found in list:" + lstLegal);
		}

		// System type tab
		String prop2= getProperty("catype");
		try {
			CATYPE = valueOf(prop2);
		} catch (IllegalArgumentException e){
			String lstLegal= FLString.ToString(CATYPE_list.values());
			Macro.FatalError("argument: "+prop2+ " not found in list:" + lstLegal);
		}

		// System Panel
		GeneralCA_TAB_DEF_SELECTION = ReadI("select-general");
		Particle_TAB_DEF_SELECTION = ReadI("select-particle");
		Linear_TAB_DEF_SELECTION= ReadI("select-linear");
		Variants_TAB_DEF_SELECTION= ReadI("select-variants");
		Stochastic_TAB_DEF_SELECTION= ReadI("select-stochastic");
		AsynchInfo_TAB_DEF_SELECTION = ReadI("select-asynchInfo");
		IpsModel_TAB_DEF_SELECTION = ReadI("select-ips");
		Tests_TAB_DEF_SELECTION = ReadI("select-tests");
		Diagnostic_TAB_DEF_SELECTION = ReadI("select-diagnostic");
		LOOK_AND_FEEL = ReadFromKey("look-and-feel", 0);

		// Simulation sidebar
		SHOW_INITIALIZER_PANEL = (ReadDefValI("tab-initializer", 1) == 1);
		SHOW_VIEWER_PANEL = (ReadDefValI("tab-viewer", 1) == 1);
		SHOW_SYNCHRONISM_PANEL = (ReadDefValI("tab-synchronism", 1) == 1);
		SHOW_MEASURES_PANEL = (ReadDefValI("tab-measures", 0) == 1);
		SHOW_RECORD_PANEL = (ReadDefValI("tab-record", 0) == 1);
		MINIMIZED_MODE = (ReadDefValI("minimized", 0) == 1);

		// Window
		DEF_LWINDOW= ReadDefValI("L-window",400);

		// CA size defaults
		TOPOLOGY_select= sizeType.valueOf(getProperty("def-CA-size"));

		Nsize1D[1] = ReadFromKey("def-size1D", Nsize1D[1]);
		Tsize1D[1] = ReadFromKey("def-buff1D", Tsize1D[1]);

		mL2D=ReadFromKey("def-size2D", mL2D);
		border=	ReadFromKey("def-border", border);

		DEF_STROBVALUE = ReadFromKey("def-stroboscope", DEF_STROBVALUE);
	
		DEF_SYNCHRATE = ReadFromKey("synch-rate", DEF_SYNCHRATE);
	}

	private void Write(String key, String value) {
		setProperty(key, value);

		if (SAVE_PREFERENCES) {
			try {
				FileOutputStream fo = new FileOutputStream(new File(m_userpath));
				this.store(fo, "");
				fo.close();
			} catch (IOException e) {
				Macro.print("User properties not found");
			}
		}
	}

	/** reads from config file and leaves value to failval if reading fails */
	private int ReadFromKey(String key, int failval) {
		String readstr= getProperty(key);
		return (readstr!=null)?Integer.parseInt(readstr):failval;
	}

	private int ReadDefValI(String key, int def) {
		String read = getProperty(key);
		if (read == null){
			Macro.print(" key not found in init file : " + key);
			return def;
		} else {
			int ival = Integer.parseInt(read);
			return ival;
		}
	}

	private int ReadI(String prop) {
		return Integer.parseInt(getProperty(prop));
	}
	/* --------------------------------------*/


	public static final int INI_SMA_SELECT_MODEL = 1;

	public static boolean withScrollPane = false; // to scroll or not to scroll ?


	//LGCA defaults
	public static final int LGCA_DEFAULT_MODEL = 0;
	public static final int LGCA_DEFAULT_TOPO = 0;

}
