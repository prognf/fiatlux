package main;

import components.types.FLString;
import components.types.IntegerList;

public class Convert {

	/** converts binary string to decimal **/
	public static int BinaryStringtoDecimal(String binaryCoding) {
		return Integer.parseInt(binaryCoding, 2);
	}

	/** converts binary string to decimal **/
	public static String DecimaltoBinaryString(Integer decimal, int len) {
		return MathMacro.Int2BinaryString(decimal, len);
	}

	public static String Decimal2StringBaseN(int decimalCode, int nbits, int N) {
		StringBuilder conv= new StringBuilder(Integer.toString(decimalCode, N));
		while (conv.length()<nbits){
			conv.insert(0, "0");
		}
		return conv.toString();
	}

	public static Integer DigitAt(String str, int index) {
		String c = "" + str.charAt(index);
		Integer val= FLString.String2Int(c); 
		return val;
	}

	public static String IntArrayToString(int[] state) {
		return MathMacro.ArrayToString2(state);
	}

	final static int ASCICODEFORZERO= 48;

	public static IntegerList StringToDigitList(String rep3) {
		Character c0 = '0';
		int cc= c0;
		IntegerList list= new IntegerList();
		for (char c : rep3.toCharArray()){
			int val= c - ASCICODEFORZERO;
			list.addVal(val);
		}
		return list;
	}


	final static char [] Tcode = {'a', 'b', 'e', 'f', 'c', 'd', 'g', 'h' };

	/** conversion from binary to T-code */
	final static public String BinaryToTcode(String state) {
		char [] letter= state.toCharArray();
		int len=letter.length;
		StringBuilder out= new StringBuilder();
		for (int i=0; i<len; i++){
			int il= (i-1+len)%len;
			int ir= (i+1+len)%len;
			int tindex= 
					4* CharToBinVal(letter[il]) + 
					2* CharToBinVal(letter[i]) + 
					CharToBinVal(letter[ir]);
			out.append(Tcode[tindex]);
		}
		return out.toString();
	}

	/** char to binary */
	final static public int CharToBinVal(char c) {
		if (c=='0') return 0;
		if (c=='1') return 1;
		Macro.SystemWarning(" char should be 0 or 1");
		return Macro.ERRORint;
	}
	
	/** '0' <-> 1 */
	final static public char charFlip(char c) {
		if (c=='0') return '1';
		if (c=='1') return '0';
		Macro.SystemWarning(" char should be 0 or 1");
		return Macro.ERRORchar;
	}

	/** input 0 or 1 ; output '0 'or '1' **/
	public static char BinvalToChar(int b) {
		if (b==0) return '0';
		if (b==1) return '1';
		Macro.SystemWarning(" input should be 0 or 1");
		return Macro.ERRORchar;
	}

	public static String CharArrayToString(char[] tab) {
		return new String(tab);
	}

}
