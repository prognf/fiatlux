package main;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedHashMap;

import components.types.FLStringList;
import models.SuperModel;
import models.CAmodels.CellularModel;
import models.CAmodels.autotile.AutoTileKModel;
import models.CAmodels.autotile.BuildEvilKModel;
import models.CAmodels.autotile.EvilKModel;
import models.CAmodels.binary.DayAndNightModel;
import models.CAmodels.binary.KaleidoscopeLifeModel;
import models.CAmodels.binary.LifeModel;
import models.CAmodels.binary.LifeModelParameters;
import models.CAmodels.binary.MajorityModel;
import models.CAmodels.binary.MajorityRandomIfEqualModel;
import models.CAmodels.binary.MinorityModel;
import models.CAmodels.binary.MinorityNoiseModel;
import models.CAmodels.binary.ParityModel;
import models.CAmodels.binary.SampleNeighbTTLModel;
import models.CAmodels.binary.SampledMajorityModel;
import models.CAmodels.binary.ScribXYZmodel;
import models.CAmodels.chemical.DictyoModel;
import models.CAmodels.decentralisedDiagnosis.NGAverage2DModel;
import models.CAmodels.decentralisedDiagnosis.NGChaosMajorityModel;
import models.CAmodels.decentralisedDiagnosis.NGLTLifeModel;
import models.CAmodels.decentralisedDiagnosis.NGMajorityFailModel;
import models.CAmodels.decentralisedDiagnosis.NGMajorityRotatingToomModel;
import models.CAmodels.decentralisedDiagnosis.NGRDCModel;
import models.CAmodels.decentralisedDiagnosis.NGReacDiffModel;
import models.CAmodels.decentralisedDiagnosis.NGRgbDiffModel;
import models.CAmodels.decentralisedDiagnosis.NGRgbModel;
import models.CAmodels.decentralisedDiagnosis.Q2Rmodel;
import models.CAmodels.decentralisedDiagnosis.QuorumDModel;
import models.CAmodels.decentralisedDiagnosis.QuorumFrontIzi;
import models.CAmodels.decentralisedDiagnosis.QuorumFrontLacaModel;
import models.CAmodels.decentralisedDiagnosis.QuorumOneDLacaModel;
import models.CAmodels.densClassif.FuksDensityStudyModel;
import models.CAmodels.densClassif.GKL_Model;
import models.CAmodels.densClassif.SchueleDensityStudyModel;
import models.CAmodels.densClassif.TraMajDensityStudyModel;
import models.CAmodels.multiRegister.CoalescenceModel;
import models.CAmodels.multiRegister.NGMichaosDiagModel;
import models.CAmodels.multiRegister.NGReactDiffClassifModel;
import models.CAmodels.multiRegister.NGReactDiffDiagModel;
import models.CAmodels.multiRegister.NGRgbDiagModel;
import models.CAmodels.nState.DLA_TCAmodel;
import models.CAmodels.nState.ECAstabilityModel;
import models.CAmodels.nState.ForestFireModel;
import models.CAmodels.nState.IsingStab;
import models.CAmodels.nState.ParabolModel;
import models.CAmodels.nState.ParityNStatesModel;
import models.CAmodels.nState.ReacDiffModel;
import models.CAmodels.nState.RuleImporterModel;
import models.CAmodels.nState.StarWarsModel;
import models.CAmodels.nState.VoronoiModel;
import models.CAmodels.nState.zIrrigatingSourcesModel;
import models.CAmodels.stochastic.DiploidECAmodel;
import models.CAmodels.stochastic.DomanyKinzel;
import models.CAmodels.stochastic.HardCoreNoisyModel;
import models.CAmodels.stochastic.ProbabilisticLineR1Model;
import models.CAmodels.stochastic.ProbabilisticLineR2Model;
import models.CAmodels.stochastic.RandomCopyModel;
import models.CAmodels.stochastic.VoterModel;
import models.CAmodels.tabled.ECAmodel;
import models.CAmodels.tabled.GeneralTabledBinaryModel;
import models.CAmodels.tabled.OuterTotalisticModel;
import models.CAmodels.tabled.TotalisticModel;
import models.CAmodels.ternary.Life2SpeciesModel;
import models.CAmodels.ternary.TernaryTableModel;
import models.CAmodels.tests.BootstrapModel;
import models.CAmodelsvar.fuzzy.ContinuousDiffEqECA;
import models.CAmodelsvar.fuzzy.FuzzyDiploidECA;
import models.CAmodelsvar.fuzzy.FuzzyECAalgebraic;
import models.CAmodelsvar.receptors.RandomWalkModel;
import models.CAmodelsvar.receptors.TasepModel;

/*
 * "static class"
 * 
 * DO NOT FORGET TO CALL 
 * MODEL_TABLE.FillTable();
 * before using
 */
public class AAAMAIN_MODEL_TABLE {
	
	private static MT m_modelType; // family of models
	// this parameter will change during insertion of models in FillTable()
	
	static int lastSeparatorId = 0;

	
	/** this inner class is a couple which associates a class with its "type" **/
	static class modelInfo {

		Class<? extends SuperModel> m_classRef;
		MT m_classType;

		public modelInfo(Class<? extends SuperModel> classRef, MT classType) {
			m_classRef = classRef;
			m_classType = classType;
		}

		public modelInfo(MT classType) {
			m_classType = classType;
		}
	}

	// associates a name (Key of type String) to a class model 
	private static LinkedHashMap<String, modelInfo> m_map= new LinkedHashMap<String, modelInfo>();

	//model type
	public enum MT {GENERAL, LINEAR, STOCHASTIC, PARTICLE, BETA, VARIANTS, DIAGNOSTIC, TESTS}

	static public void FillTable() {
		
		
		/*----------------------
		 * **********************/
		m_modelType = MT.TESTS;
		/* ***********************
		 * ----------------------*/
		AddList(SampleNeighbTTLModel.class);
		AddList(SampledMajorityModel.class);
		AddSeparator();
		AddList(BootstrapModel.class);
		AddSeparator();
		AddList(AutoTileKModel.class);
		AddSeparator();
		AddList(QuorumDModel.class);		
		AddList(QuorumFrontIzi.class);
		AddList(QuorumFrontLacaModel.class);
		AddList(QuorumOneDLacaModel.class);
		AddSeparator();
		AddList(EvilKModel.class);
		AddList(BuildEvilKModel.class);

		
		
		m_modelType = MT.GENERAL;
		AddList(LifeModel.class);
		//AddSeparator();

		AddList(MajorityModel.class);
		AddList(MinorityModel.class);
		AddList(ParityModel.class);
		AddList(ParityNStatesModel.class);
	
		AddList(TotalisticModel.class);
		AddList(OuterTotalisticModel.class);
		AddSeparator();

		AddList(ReacDiffModel.class);
		AddList(ForestFireModel.class);
		AddList(VoronoiModel.class);
		AddList(StarWarsModel.class);
		AddList(RuleImporterModel.class);
		AddSeparator();

		AddList(GeneralTabledBinaryModel.class);
		AddList(TernaryTableModel.class);
		//AddList(MajorityVotingModel.class);

		m_modelType = MT.LINEAR;
		AddList(ECAmodel.class);
		AddList(FuzzyECAalgebraic.class);
		AddList(ContinuousDiffEqECA.class);
		AddSeparator();
		AddList(ECAstabilityModel.class);
		AddList(FuzzyDiploidECA.class);
		AddSeparator();
		AddList(ParabolModel.class);

		m_modelType = MT.PARTICLE;
		AddList(DictyoModel.class);
		AddSeparator();
		AddList(zIrrigatingSourcesModel.class);
		AddList(TasepModel.class);
		AddSeparator();
		// AddList(ContactProcessModel.class);
		AddList(RandomWalkModel.class);
		AddList(DLA_TCAmodel.class);


		// WHat is this ???
		// AddList(Traffic2DtransactionalModel.class);

		// AddList(WalkerTexasRangerMargolusModel.class);
		// AddList(RandomWalkMargolusModel.class);
		// AddList(BallisticMargolusModel.class);

		m_modelType = MT.STOCHASTIC;
		AddList(DiploidECAmodel.class);
		AddList(MajorityRandomIfEqualModel.class);
		AddList(MinorityNoiseModel.class);
		AddList(ProbabilisticLineR1Model.class);
		AddList(ProbabilisticLineR2Model.class);
		AddSeparator();
		AddList(HardCoreNoisyModel.class);
		AddList(DomanyKinzel.class);
		AddSeparator();
		//AddList(RetinaModel.class);
		AddList(RandomCopyModel.class);
		AddList(VoterModel.class);
		AddSeparator();
		AddList(FuksDensityStudyModel.class);
		AddList(SchueleDensityStudyModel.class);
		AddList(TraMajDensityStudyModel.class);
		AddList(GKL_Model.class);
		AddList(Q2Rmodel.class);

		//--------------------------------------
		m_modelType = MT.VARIANTS;
		AddList(MajorityRandomIfEqualModel.class);
		AddList(LifeModelParameters.class);
		AddList(Life2SpeciesModel.class);
		AddList(NGLTLifeModel.class);
		AddSeparator();
		AddList(DayAndNightModel.class);

		AddList(KaleidoscopeLifeModel.class);
		AddSeparator();

		//AddList(DictyoTCAmodel.class);
		//AddList(KariModel.class);
		//AddList(BeehiveModel.class);
		//AddList(HybridECAmodel.class);
		AddList(CoalescenceModel.class);
	
		m_modelType = MT.DIAGNOSTIC;
		AddList(NGReacDiffModel.class);
		AddList(NGReactDiffDiagModel.class);
		AddList(NGReactDiffClassifModel.class);

		AddSeparator();

		AddList(NGRgbModel.class);
		AddList(NGRgbDiffModel.class);
		AddList(NGRgbDiagModel.class);

		AddSeparator();

		AddList(NGChaosMajorityModel.class);
		AddList(NGMichaosDiagModel.class);

		AddSeparator();

		AddList(NGMajorityFailModel.class);
		AddList(NGMajorityRotatingToomModel.class);
		AddList(IsingStab.class);

		AddSeparator();

		AddList(NGAverage2DModel.class);
		AddList(NGRDCModel.class);

		
	}



	private static void AddList(Class<? extends SuperModel> modelClass) {
		modelInfo localClassInfo= new AAAMAIN_MODEL_TABLE.modelInfo(modelClass, m_modelType);
		AssociateName(localClassInfo);
	}

	private static void AddSeparator() {
		modelInfo separator = new AAAMAIN_MODEL_TABLE.modelInfo(m_modelType);
		AssociateName(separator);
	}


	static public FLStringList FindModelsOfType(MT modelType){
		FLStringList list= new FLStringList();
		for (String modelName : m_map.keySet()){
			if (m_map.get(modelName).m_classType == modelType){
				list.Add( modelName );
			}
		}
		return list;
	}

	/**
	 * REFLEXIVITY here 
	 */
	static private void AssociateName(modelInfo item) {
		Class<? extends SuperModel> oneClass= item.m_classRef;

		if (oneClass != null) {
			String className = oneClass.getCanonicalName();
			try {
				//Class.forName( className );
				try {
					Field f = oneClass.getField("NAME");
					String nameRead = (String) f.get(String.class);
					//Macro.Formatprint("class: %s key name:%s ",oneClass.getCanonicalName(), nameRead);
					m_map.put(nameRead, item);
				} catch (Exception e) {
					Macro.SystemWarning(e,
							"Field NAME not found in class:" + oneClass.getCanonicalName());
				}
			} /*catch( ClassNotFoundException e ) {
				Macro.print("Class not found: " + className);
			} */ catch (NoClassDefFoundError e) {
				String s = "Path problem with class: " + className;
				if (oneClass.equals(ScribXYZmodel.class)) {
					s += "\n Check link to external libraires (janino)";
				}
				Macro.SystemWarning(s);
			}
		} else {
			m_map.put("separator" + (lastSeparatorId++), item);
		}
	}

	/**
	 * 
	 */
	static public void PrintAllNames(){

		for (String item : m_map.keySet()){
			Macro.print(" base contains model : " + item);
		}
	}

	/**
	 * REFLEXIVITY here : construction of the object from the name
	 */
	public static CellularModel FindFromKey(String modelName) {
		Class<? extends SuperModel> foundModel=null;
		try{
			foundModel= m_map.get(modelName).m_classRef; //class associated to the name
		} catch (Exception e){
			Macro.SystemWarning(e, "Association failed with : " + modelName );
		}
		Constructor<?> ctor;
		Object object = null;
		CellularModel model=null;
		try {
			ctor = foundModel.getConstructor();
			object = ctor.newInstance();
			model= (CellularModel) object;
		} catch (NoSuchMethodException e) {
			Macro.SystemWarning("class " + foundModel.getCanonicalName() 
					+ " could not find empty constructor");
			e.printStackTrace();
		}/* catch (Exception e){
			Macro.SystemWarning("Un-identified exception (security ?) ; key " + modelName); 
		}*/ catch (IllegalArgumentException | InvocationTargetException | IllegalAccessException | InstantiationException e) {
			e.printStackTrace();
		}

		//Macro.Debug(model.GetName());
		return model;

	}

	
/*	static public void main(String [] argv ){
		//TestOne();
		FillTable();
		PrintAllNames();
	}
*/
	
}

