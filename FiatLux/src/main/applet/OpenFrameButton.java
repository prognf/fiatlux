package main.applet;

import grafix.gfxTypes.elements.FLActionButton;
import grafix.gfxTypes.elements.FLFrame;
import grafix.gfxTypes.elements.FLPanel;

public class OpenFrameButton extends FLActionButton {
	
	/**The panel to be displayed in a new Frame*/
	private final FLPanel m_panel;
	private final String m_title;

	/**
	 * Opens a panel in a new frame
	 * @param buttonText
	 * @param panel
	 */
	public OpenFrameButton(String buttonText, String title, FLPanel panel) {
		super(buttonText);
		m_title = title;
		m_panel = panel;
	}

	@Override
	public void DoAction() {
		FLFrame frame = new FLFrame(m_title);
		frame.addPanel(m_panel);
		frame.packAndShow();
	}
}
