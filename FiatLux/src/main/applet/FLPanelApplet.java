package main.applet;

import java.awt.Container;
import java.awt.FlowLayout;

import javax.swing.JApplet;

import grafix.gfxTypes.elements.FLFrame;
import grafix.gfxTypes.elements.FLPanel;

public abstract class FLPanelApplet extends JApplet {
	
	private FLFrame m_frame;
	private OpenFrameButton m_SimButton;
	
	public abstract FLPanel 	getPanel();
	public abstract String 	getAppletTitle();
	
	public void init() {
		// create button
		m_SimButton = new OpenFrameButton("Open", getAppletTitle(), getPanel());
		
		// add and display button.
		Container cp = getContentPane();
		cp.setLayout(new FlowLayout());
		cp.add(m_SimButton);
	}
}
