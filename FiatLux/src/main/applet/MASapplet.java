package main.applet;

import grafix.gfxTypes.elements.FLPanel;
import main.designPanels.MASdesignerPanel;

public class MASapplet extends FLPanelApplet {
	
	public final String NAME = "MAS Designer Panel";

	@Override
	public FLPanel getPanel() {
		return new MASdesignerPanel();
	}

	@Override
	public String getAppletTitle() {
		return NAME;
	}

}
