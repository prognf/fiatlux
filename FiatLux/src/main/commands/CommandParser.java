package main.commands;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Hashtable;

import components.types.FLString;
import components.types.FLStringList;
import main.Macro;


public class CommandParser {

	private static final String NOusage = "No usage given";
	String [] m_arg; // argument given in the command line
	String [] m_argf; // argument given in the command line
	String m_CallingClassName, m_usage;
	Hashtable<String, Object> m_configtable = new Hashtable<String, Object>();
	
	int m_pos; // parser position (incremental)
	private Date m_begTime, m_endTime; // how long did the experiment last ?

	
	
	/** central constructor  **/
	public CommandParser(String expName, String usage, String [] argv){
		m_begTime= Macro.GetTime();
		m_pos=0;
		m_CallingClassName= expName;
		m_usage= usage;
		m_arg= argv;
	}
	
	
	/** minimal init style **/
	public CommandParser(String [] argv){
		this(Thread.currentThread().getStackTrace()[2].getClassName(), NOusage, argv);
	}
	
	/** minimal init style (no usage, no check on number of args **/
	public CommandParser(String expName, String [] argv){
		this(expName, NOusage, argv);
	}
	
	/** start message + check number of args **/
	public CommandParser(String expName, String usage, String [] argv, int num_arg){
		this(expName, usage, argv);
		checkArgNum(num_arg);
	}
	

	/** checks if there is the right number of args */ 
	public void checkArgNum(int in_Narg){
		if (m_arg.length != in_Narg) {
			Macro.print(m_arg.length + " arguments given, requested:" + in_Narg);
			Usage();
		}
	}
	
	public boolean IsArgNum(int num) {
		return (m_arg.length == num);
	}
	
	
	/* works if CLASSNAME and USAGE are defined in the subclasses */
	final protected void Usage(){
		String args="";
        for (String aM_argv : m_arg) {
            args += aM_argv + " ";
        }
		String head="java " + m_CallingClassName;
		Macro.print("Your command was: " + head + " " + args);
		Macro.print("           Usage: "+ head  + " " +  m_usage );
		Macro.systemExit(-1);	
	}

	/* 
	 * parsing methods 
	 * */
		
	public String SParse(String pattern) {
		try {
		return FLString.SParseWithControl(m_arg[m_pos++], pattern);
		} catch (Exception e) {
			Macro.FatalError("error when parsing, expecting key:"+pattern);
			return null;
		}
	}
	
	public char CParse(String pattern) {
		return FLString.CParseWithControl(m_arg[m_pos++], pattern);
	}
	
	public int IParse(String pattern) {
		return FLString.IParseWithControl(m_arg[m_pos++], pattern);
	}
	
	public long LParse(String pattern) {
		return FLString.LParseWithControl(m_arg[m_pos++], pattern);
	}
	
	public double DParse(String pattern) {
		return FLString.DParseWithControl(m_arg[m_pos++], pattern);
	}
	
	public BigDecimal BIParse(String pattern) {
		return FLString.BIParseWithControl(m_arg[m_pos++], pattern);
	}

	public void Bye() {
		m_endTime = Macro.GetTime();
		Macro.print("Bye ! Experiment done in " + FLString.TimeFormat(m_endTime, m_begTime));
		Exit();
	}

	
	/** quit message **/
	public void Exit(){
		Macro.systemExit(0);
	}
	
	public void Usage(String usageMsg) {
		Macro.print(" USAGE: " + usageMsg);
		Macro.systemExit(-1);
	}

	public boolean ArgEquals(int argnum, String string) {
		return m_arg[argnum].equals(string);
	}


	public void badArg() {
		Macro.print(m_CallingClassName + ":: bad argument !");
		Macro.print("!!arg.received:" + FLString.ToString(m_arg));
	}


	public boolean firstArgEquals(String argtest) {
		return m_arg[0].equals(argtest);
	}


	/** first argument : argpos=0 **/
	public String oldGetArg(int argpos) {
		return m_arg[argpos];
	}
	
	/** first argument : argpos=1 **/
	public String getArg(int argpos) {
		return m_arg[argpos-1];
	}


	public int nArg() {
		return m_arg.length;
	}


	/** first argument has index 1 **/
	public FLStringList getListArgFrom(int argstart) {
		FLStringList arglist= new FLStringList();
		for (int i=argstart-1; i< m_arg.length;i++) {
			arglist.Add(m_arg[i]);
		}
		return arglist;
	}



}
