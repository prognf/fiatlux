package main.commands;

import main.Macro;

/**
 * OLD USE CommandParser instead 
 * redefine Static attribute USAGE */
abstract public class CommandInterpreter {

	private static final int BLANKLINES = 3; /* we skip lines before we start printing */
	static String USAGE2= "No usage";
	
	/** start message **/
	static protected void LaunchMessage(String in_ExpName, String in_Usage){
		Macro.SkipLines(BLANKLINES);
		Macro.print("*** Starting " + in_ExpName + "... ***");
		USAGE2= in_Usage;
	}
	
	/** start message **/
	static protected void LaunchMessage(String in_ExpName){
		LaunchMessage(in_ExpName, "??? [no usage was given]");
	}
	
	/** quit message **/
	protected static void ByeMessage(){
		Macro.systemExit(0);
	}
	
	/** checks if there is the right number of args */ 
	static protected void checkArgNum(String [] argv, int in_Narg){
		if (argv.length != in_Narg) {
			Macro.print(argv.length + " arguments given, requested:" + in_Narg);
			Usage(argv);
		}
	}
	
	
	/* works if CLASSNAME and USAGE are defined in the subclasses */
	final static protected void Usage(String [] argv){
		String args="";
        for (String anArgv : argv) {
            args += anArgv + " ";
        }
		Macro.print("Your command was:" + args);
		Macro.print("           Usage:" +  USAGE2 );
		Macro.systemExit(-1);	
	}
	
}
