package main.commands;

import experiment.toolbox.FormatGPdat;
import main.Macro;


public class ToGPdat {

	final static int NARG1=1, NARG2=2;

	/*--------------------
	 * Usage
	 --------------------*/

	public static void Usage() {
		String s = "usage : Commands.ToGPdat [transpose] <filename>";
		Macro.print(s);
		Macro.systemExit(-1);
	}
	
	/*--------------------
	 * Main
	 --------------------*/
	public static void main(String[] args) {
		if (args.length == NARG1) {
			String filename = args[0];
			Macro.print("***  converting " + filename
					+ " to gnuplot-readable format with average and std deviation");
			FormatGPdat datafile = new FormatGPdat(filename);
			datafile.WriteFileMuSigma();
		} else if (args.length == NARG2){
			String filename = args[1];
			Macro.print("***  converting " + filename
					+ " to gnuplot-readable format");
			FormatGPdat datafile = new FormatGPdat(filename);
			datafile.WriteFile();
		} else {
			Usage();
		}

	}

}
