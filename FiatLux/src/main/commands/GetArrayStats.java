package main.commands;

import experiment.toolbox.FormatGPdat;
import main.Macro;


/*--------------------
 * This class allows command line for getting stats on ExperimentArray1D objects
 * @author Nazim Fates
--------------------*/

public class GetArrayStats extends CommandInterpreter {
	
	/** Usage **/
	final static String 
	CLASSNAME="GetArrayStats", 	
	USAGE= "filenameMS.gpdat";
	final static int N_ARG=1;
	
	/* we take information from the last line of the array */
	public static void Process(String filename){
		FormatGPdat datafile= new FormatGPdat(filename);
		Macro.print("" + filename + ">>" + datafile.GetArrayStats());
	}
	
	public static void main(String[] argv) {
	//	LaunchMessage(CLASSNAME,USAGE);
		checkArgNum(argv,N_ARG);
		String filename= argv[0];
		Process(filename);
	//	ByeMessage();
	}
}
