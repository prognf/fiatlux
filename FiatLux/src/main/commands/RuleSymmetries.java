package main.commands;

import components.types.RuleCode;
import main.Macro;


public class RuleSymmetries {

	final static int NARG1=1, NARG2=2;

	String USAGE = "usage : RuleSymmetries K=neighb size W=rulecode";
	
	/*--------------------
	 *
	 --------------------*/

	/*--------------------
	 * Main
	 --------------------*/
	public static void main(String[] argv) {
		
		CommandParser exp= new CommandParser("kerdos tests", argv);
		int K= exp.IParse("K=");
		long W= exp.LParse("W=");
		
		Macro.fPrint("K=%d W=%d sym(I,R,C,RC):%s", K, W, RuleCode.GetStrBigIntSymmetrics(new RuleCode(W), K));
	}

}
