package main;



/*--------------------
 * contains some statistics functions
 * @author Nazim Fates
*--------------------*/
public class StatsMacro {



	static public void TestGetPeriod() {

		double pi = 3.14159;

		double[] x = new double[500];
		for (int k = 1; k < 10; k++) {
			for (int i = 0; i < 500; i++) {
				x[i] = (1.0 + Math
						.sin(0.5 + (double) i * 2.0 * pi / (double) k)) / 4.0;
			}
			Macro.print(" Spectrum :" + StatsMacro.GetSpectrum(x, 100));
		}
	}

	static public void TestQuadraticAverage() {

		double[] x = new double[500];
		for (int i = 0; i < 500; i++) {
			x[i] = (i % 2);
		}
		Macro.print(" Quadratic Average :" + StatsMacro.QuadraticAverage(x));
	}

	/**
	 * d-spectrum as a "period" of a time series 
	 * USES a histogram with in_BinNumber + 1 bins
	 */
	static public int GetSpectrum(double[] in_Array, int in_BinNumber) {

		// normalisation to [0,1]
		double maxval = MathMacro.Max(in_Array);
		double minval = MathMacro.Min(in_Array);
		double range = maxval - minval;
		if (range == 0.0) {
			return 1;
		} else {
			int[] frequency = new int[in_BinNumber + 1];
			// we count the frequencies for cluster of 1% width
			try {
                for (double anIn_Array : in_Array) {
                    int bin = (int) Math.round(in_BinNumber
                            * (anIn_Array - minval) / range);
                    frequency[bin]++;
                }

			} catch (Exception eround) {
				eround.printStackTrace();
				Macro.SystemWarning("Array given does not respect specifications ");
			}

			// we count the number of non-zero values
			int period = 0;
			for (int i = 0; i < 101; i++) {
				if (frequency[i] > MathMacro.NOISELIMIT)
					period++;
			}

			return period;
		}

	}

	/** returns the average calculated on the indices [0..k[ of the array * */
	static public double PartialAverageFromEnd(double[] in_Array, int in_rank) {
		double s = 0.;
		int max_index = in_Array.length - 1;
		if (in_rank >= max_index) {
			Macro.FatalError("PartialAverageFromEnd was called with an index superior to the length of the array !");
		}
		for (int i = 0; i < in_rank; i++) {
			s += in_Array[max_index - i];
		}
		return s / (double) in_rank;
	}

	/** INT ARRAY **/
	static public double Mean(int[] in_Array) {
		long sum = 0;
        for (int anIn_Array : in_Array) {
            sum += anIn_Array;
        }
		return sum / (double) in_Array.length;
	}

	/** INT ARRAY computing using the previously computed mean **/
	static public double StdDev(int[] in_Array, double mean) {
		long s = 0;
        for (int anIn_Array : in_Array) {
            s += anIn_Array * anIn_Array;
        }
		double var = s / (double) in_Array.length - mean * mean;
		return Math.sqrt(var);
	}

	/** one day I should test this method **/
	static public double Mean(double[] in_Array) {
		double s = 0.;
        for (double anIn_Array : in_Array) {
            s += anIn_Array;
        }
		return s / (double) in_Array.length;
	}

	/**
	 * Calculates the standard deviation of an array of numbers. see Knuth's The
	 * Art Of Computer Programming Volume II: Seminumerical Algorithms This
	 * algorithm is slower, but more resistant to error propagation.
	 * 
	 * @param data
	 *            Numbers to compute the standard deviation of. Array must
	 *            contain two or more numbers.
	 * @return standard deviation estimate of population ( to get estimate of
	 *         sample, use n instead of n-1 in last line )
	 */
	public static double sdKnuth(double[] data) {
		final int n = data.length;
		if (n < 2) {
			return Double.NaN;
		}
		double avg = data[0];
		double sum = 0;
		for (int i = 1; i < data.length; i++) {
			double newavg = avg + (data[i] - avg) / (i + 1);
			sum += (data[i] - avg) * (data[i] - newavg);
			avg = newavg;
		}
		return Math.sqrt(sum / (n - 1));
	}

	/** not the best algo : mean is computed apart !**/
	static public double StdDeviation(double[] in_Array) {
		return Math.sqrt(StatsMacro.Variance(in_Array, Mean(in_Array)));
	}

	/** not the best algo : mean is computed apart !**/
	static public double Variance(double[] in_Array) {
		return StatsMacro.Variance(in_Array, Mean(in_Array));
	}

	/** computing using the previously computed mean * */
	static public double Variance(double[] in_Array, double mean) {
		return StatsMacro.QuadraticAverage(in_Array) - mean * mean;
	}

	/** returns the quadratic average (not tested) */
	static public double QuadraticAverage(double[] in_Array) {
		double sum = 0;
		for (double val : in_Array) {
			sum += val * val;
		}
		int len = in_Array.length;
		return Math.sqrt(sum / (double) len);
	}

	/** warning : input array must be as follows : array[i] = number of time one has i value
	 * (e.g. number of i-sized agglomerates of particles)**/
	public static double RootMeanSquareFromHistogram(int[] array) {
		double result;
		double somme1 = 0;
		int nb_agglo1 = 0;
		for (int i = 0; i < array.length; i++){
			somme1 += array[i]*i*i;
			nb_agglo1 += array[i];
		}
		
		if (nb_agglo1==0){
			result = 0;
		} else {
			result = Math.sqrt( somme1 / nb_agglo1);		
		}
		return result;
	}


	
}// end of class
