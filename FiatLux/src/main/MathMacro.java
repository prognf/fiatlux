package main;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Stream;

import components.randomNumbers.FLRandomGenerator;
import components.randomNumbers.StochasticSource;
import components.types.FLString;
import components.types.FLStringList;
import components.types.IntC;
import components.types.IntegerList;
import components.types.RuleCode;
import grafix.gfxTypes.XYpoint;
import models.CAmodels.tabled.ECAlookUpTable;

/*--------------------
 * The class MathMacro contains some mathematics
 * @author Nazim Fates
 *--------------------*/

public class MathMacro {


	public final static double HUNDRED= 100.;
	public final static float HUNDREDFLOAT = 100.F;
	
	public final static double THOUSAND= 1000.;
	
	
	/** sign of b - a (1:positive ; 0 equal ; -1 negative */
	final static public int zzzSign(int a, int b) {
		int d = b - a;
		if (d > 0)
			return 1;
		else if (d < 0)
			return -1;
		else
			return 0;
	}



	/** from decimal to binary code , 
	 * len is the length of the binary Table */
	final static public int[] Decimal2BinaryTab(long intcode, int len) {
		int[] out_Tab = new int[len];
		// array decoding
		long code = intcode;
		for (int i = 0; i < len; i++) {
			out_Tab[i] = (int) code & 1;
			code >>= 1;
		}
		return out_Tab;
	}

	/** SPECIAL : from decimal to binary code with "inverted" table 
	 * len is the length of the binary Table */
	final static public int[] Decimal2BinaryTabInverted(long intcode, int len) {
		int[] out_Tab = new int[len];
		// array decoding
		long code = intcode;
		for (int i = 0; i < len; i++) {
			out_Tab[len - 1 - i] = (int) code & 1;
			code >>= 1;
		}
		return out_Tab;
	}


	/** from binary code to decimal **/
	final static public int Tab2CodeInt(int[] bintab) {
		long out_RuleNum= BinaryTab2Decimal(bintab);
		if ((out_RuleNum < Integer.MIN_VALUE)
				|| (out_RuleNum > Integer.MAX_VALUE)) {
			Macro.FatalError(" code in table exceeds int capacity ; size of the incoming table: " + bintab.length);
		}
		return (int) out_RuleNum;
	}

	/** from binary code to decimal : Least Sign. Bit has index 0
	 * FIXME : working or not ??? **/
	final static public long BinaryTab2Decimal(int[] tab) {

		Long rulenum = 0L;
		int len = tab.length;
		for (int i = 0; i < len; i++) {
			rulenum =rulenum *2L;
			rulenum =rulenum+ (long)tab[len - i - 1]; // the bit of high weight must
			// be taken into account first
			//Macro.DebugNOCR(":i"+i+" r"+rulenum+":");
		}
		//Macro.fDebug("long:%d from:%s", rulenum, MathMacro.ArrayToString2(tab));
		//Exception e = new Exception();
		//Macro.SystemWarning(e, "Binary2tab ??!");
		return rulenum;
		
	}

	
	/** from binary code to decimal : Least Sign. Bit has index 0**/
	final static public BigInteger BinaryTab2BigInt(int[] tab) {

		BigInteger TWO = BigInteger.valueOf(2);
		BigInteger rulenum = BigInteger.valueOf(0);
		int len = tab.length;
		for (int i = 0; i < len; i++) {
			rulenum= rulenum.multiply(TWO);
			int bit=tab[len - i - 1];
			rulenum= rulenum.add( BigInteger.valueOf( bit ) ); // the bit of high weight must
		}
		//		Macro.fDebug("ruleBI:%s from:%s", rulenum, MathMacro.ArrayToString2(tab));
		return rulenum;
		
	}
	
	/** from binary code to decimal : Most Sign. Bit has index 0**/
	final static public long InvBinaryTab2Decimal(int[] in_Tab) {
		long out_RuleNum = 0L;
		int len = in_Tab.length;
        for (int anIn_Tab : in_Tab) {
            out_RuleNum <<= 1;
            out_RuleNum += anIn_Tab;
        }
		return out_RuleNum;
	}

	/** from binary code to decimal **/
	final static public RuleCode BinaryTab2RuleCode(int[] in_Tab) {
		long W= BinaryTab2Decimal(in_Tab);
		return new RuleCode(W);
	}

	/**
	 * Sum.
	 * @param in_Tab
	 * @return the sum of elements of the array
	 */
	final static public int TabSum(int[] in_Tab) {
		int sum = 0;
		for (int val : in_Tab) {
			sum+= val;
		}
		return sum;
	}

	/** binary conversion with fixed complementation with 0s **/
	static public String Int2BinaryString(int number, int size) {
		String s = Integer.toBinaryString(number);
		int addlen = size - s.length();
		String zeros = FLString.NcopiesOfChar(addlen,'0');
		s = zeros + s;
		return s;
	}
	
	/** n-ary conversion with fixed complementation with 0s **/
	static public String Long2NaryString(long number, int base, int size) {
		String s = Long.toString(number,base);
		int addlen = size - s.length();
		String zeros = FLString.NcopiesOfChar(addlen,'0');
		s = zeros + s;
		return s;
	}
	
	// ALTERNATIVE CONSTRUCTION
	//String seq= String.format("%" + N + "s", Integer.toBinaryString(i)).replace(' ','0');

	/** generates the series of words in {0,1}^n 
	 * that are not equivalent up to a circular permutation
	 * (0^n and 1^n are excluded)
	 */
	static public FLStringList GenerateNonEquivalentBinWords(int n){
		FLStringList results = new FLStringList();
		int counter=1;
		long lsize= TwoToPowerLongResult(n) ;
		int size=-1;
		if (lsize<Integer.MAX_VALUE){
			size= (int)lsize;
		} else{
			Macro.FatalError("n is out of range:"  + n);
		}
		boolean [] equiv = new boolean[size];
		while (counter< size -1){
			if (!equiv[counter]){
				//Macro.Debug(" c: " + counter );			
				int [] bitArray= Decimal2BinaryTab(counter, n);
				//Macro.Debug(" tab: " + BitArrayToString(bit) );
				String s = MathMacro.ArrayToStringNoFormatting(bitArray);
				results.Add(s);
				// after n permutations the array of bits is left unchanged 
				for (int permut=0; permut < n ; permut++){
					LeftShift(bitArray);
					int newInt= Tab2CodeInt(bitArray);
					//Macro.Debug(" new: " + newInt );
					equiv[newInt]= true;
				}
			}
			counter++;
		}
		return results;
	}

	
	public static int [] IntArrayFromString(String line) {
		String [] parts= line.split(" ");
		int [] idline= Stream.of(parts).mapToInt(Integer::parseInt).toArray();
		//Macro.fDebug(" convert from : %s to %s",line, MathMacro.ArrayToStringWithFormatting( idline ) );
		return idline;
	}
	
	public static Long [] LongArrayFromString(String line) {
		String [] parts= line.split(" ");
		Long [] idline= Stream.of(parts).mapToLong(Long::parseLong).boxed().toArray(Long[]::new);
		return idline;
	}
	
	public static Integer [] IntArrayFromString2(String line) {
		String [] parts= line.split(" ");
		Integer [] idline= Stream.of(parts).mapToInt(Integer::parseInt).boxed().toArray(Integer[]::new);
		return idline;
	}
	/*-----------------------------------------------------------------------
	 * VECTORIAL STUFF
	 *---------------------------------------------------------------------*/

	private static void LeftShift(int[] bit) {
		int bit0= bit[bit.length - 1];
		for (int pos=bit.length - 2; pos>=0 ; pos--){
			bit[pos+1]= bit[pos];
		}
		bit[0]= bit0;

	}

	/**
	 * Scalar Product. Sizes must be identical.
	 * @param tab1 an integer table
	 * @param tab2 an integer table
	 * @return the scalar products of the 2 arrays
	 */
	final static public double zzzScalarProduct(double[] tab1, double[] tab2) {
		if (tab1.length != tab2.length) {
			Macro.FatalError("Scalar product: matrices have different size\n"
					+ tab1.toString() + "\n" + tab2.toString());
		}

		double sum = 0;
		for (int i = 0; i < tab1.length; i++) {
			sum += tab1[i]*tab2[i];
		}
		return sum;
	}

	/** used by LGCA machinery (devt by Olivier Bouré) **/
	final static public int ScalarProduct(int[] tab1, int[] tab2) {
		if (tab1.length != tab2.length) {
			Macro.FatalError("Scalar product: matrices have different size\n"
					+ tab1.toString() + "\n" + tab2.toString());
		}

		int sum = 0;
		for (int i = 0; i < tab1.length; i++) {
			sum += tab1[i]*tab2[i];
		}
		return sum;
	}

	/** average value of an array of double **/
	static public double mean(double[] array) {
		double sum = 0.;
		for (double elem : array) {
			sum += elem;
		}
		return sum/(double)array.length;
	}
	
	
	/** MIN value of an array */
	static public double Min(double[] in_Array) {
		double minval = in_Array[0];
		for (int i = 1; i < in_Array.length; i++)
			if (in_Array[i] < minval)
				minval = in_Array[i];
		return minval;
	}
	
	/** min of two values **/
	public final static int Min(int valA, int valB) {
		return (valA<valB)?valA:valB;
	}

	/** MAX value of an array */
	static public double Max(double[] in_Array) {
		double maxval = in_Array[0];
		for (int i = 1; i < in_Array.length; i++)
			if (in_Array[i] > maxval)
				maxval = in_Array[i];
		return maxval;
	}

	static public int Max(int a, int b){
		return (a>b)? a : b;
	}

	/** MIN value of an array */
	static public int Min(int[] in_Array) {
		int minval = in_Array[0];
		for (int i = 1; i < in_Array.length; i++)
			if (in_Array[i] < minval)
				minval = in_Array[i];
		return minval;
	}

	/** MAX value of an array */
	static public int Max(int[] in_Array) {
		int maxval = in_Array[0];
		for (int i = 1; i < in_Array.length; i++)
			if (in_Array[i] > maxval)
				maxval = in_Array[i];
		return maxval;
	}

	/** modulo with special treatment for negative a **/
	static public int Mod(int a, int b) {
		if (a<0) return (a+b)%b;
		else if (a<b) return a;
		else return a%b;
	}

	static public int Factorial(int val) {
		int res = 1;
		for (int i = 2; i < val; i++) {
			res *= val;
		}
		return res;
	}

	final static int NOISELIMIT = 0; // minimum value of frequency to count a
	private static final int DUMMYINT = -99999999;


	/** power of two in "long" type **/
	public static long TwoToPowerLongResult(int n) {
		return 1L<<n;
	}

	/** power of two in "int" type **/
	public static int TwoToPowerIntResult(int n) {
		return 1<<n;
	}


	public static int Power(int x, int n) {
		return /* n < 0 ? 1 / Power(x,-n) // Negative power */
				n == 0 ? 1 // Base
						: n == 1 ? x // cases
								: n % 2 == 0 ? (x = Power(x, n / 2)) * x // Even
										// power
										: x * Power(x, n - 1); // Odd power
	}

	public static double Power(double x, int n) {
		return /* n < 0 ? 1 / Power(x,-n) // Negative power */
				n == 0 ? 1 // Base
						: n == 1 ? x // cases
								: n % 2 == 0 ? (x = Power(x, n / 2)) * x // Even
										// power
										: x * Power(x, n - 1); // Odd power
	}

	/** simple encapsulation **/
	final public static double PowerD(double x, double a){
		return Math.pow(x, a);
	}

	static public void TestPower() {
		FLStringList l = new FLStringList();
		for (int i = 0; i <= 10; i++) {
			l.Add("" + Power(2, i));
		}
		l.PrintOnOneLine();
	}

	static public void TestCeiling() {
		int t = 100;
		double dt = 0.75;

		Macro.print(" 100/dt  :" + (100 / dt));
		Macro.print(" 100./dt  :" + ((double) 100 / dt));
		Macro.print(" ceil 100/dt  :" + Math.ceil(t / dt));
		Macro.print(" ceil 100./dt  :" + Math.ceil((double) t / dt));
		int c = 0;
		for (double u = 0; u < t + Macro.EPSILON; u += dt) {
			c++;
		}
		Macro.print(" nb of increments :" + c);
	}

	static public IntegerList ComputeExponentialSampling(int maxtime,
			double expstep) {
		IntegerList points = new IntegerList();
		double comp_ini = 1.;
		int comp = (int) comp_ini;
		for (int t = 0; t < maxtime; t++) {
			comp--;
			if (comp == 0) {
				points.addVal(t);
				// Macro.print(" pos, t " + pos + "," + t );
				comp_ini = comp_ini * expstep; // exponential increase
				comp = (int) comp_ini;
			}
		}
		return points;
	}

	//static int count = 0;


	/* 2D to 1D mapping with torical boundaries */
	public static int Map2Dto1D(int x, int y, int Xsize, int Ysize) {
		int xp = (x + Xsize) % Xsize, yp = (y + Ysize) % Ysize;
		return xp + yp * Xsize;
	}

	/*--------------------
	 * primes
	 --------------------*/

	/** (copied from the web) **/ 
	public static boolean isPrime(long n) {
		if(n < 2) return false;
		if(n == 2 || n == 3) return true;
		if(n%2 == 0 || n%3 == 0) return false;
		long sqrtN = (long)Math.sqrt(n)+1;
		for(long i = 6L; i <= sqrtN; i += 6) {
			if(n%(i-1) == 0 || n%(i+1) == 0) return false;
		}
		return true;
	}


	/**
	 * prime decomposition of numbers
	 */
	public static String PrimeDecompose(int x) {
		int currentFactor = 2;
		currentFactor = ExtractDiv(x, currentFactor);
		x /= currentFactor;
		StringBuilder decomp = new StringBuilder("" + currentFactor);
		while (x > 1) {
			currentFactor = ExtractDiv(x, currentFactor);
			x /= currentFactor;
			decomp.append("*").append(currentFactor);
		}
		return decomp.toString();
	}

	private static int ExtractDiv(int x, int currentFactor) {
		int div = currentFactor;
		boolean found = false;
		while (div <= x && (x % div != 0)) {
			div++;
		}
		return div;
	}

	/*--------------------
	 * trigonometry
	 --------------------*/

	/*
	 * i-th point over npoints convention point 0 on x-axis
	 */
	public static XYpoint PointModuleAngle(double radius, int i, int npoints) {

		double x = radius * Math.cos(2 * i * Math.PI / (double) npoints), y = radius
				* Math.sin(2 * i * Math.PI / (double) npoints);
		return new XYpoint(x, y);
	}

	/* wraps in a toroidal way integers that can be negative */
	final public static int Wrap(int x, int size) {
		int x2 = (x % size);
		return (x2 < 0) ? (x2 + size) : x2;
	}



	// calculate the combination
	// the value would be very large, so store it in the type of double
	public static double combination(int n, int k) {
		double ret = 1;
		while (k > 0) {
			ret = ret * ((double) n / (double) k);
			k--;
			n--;
		}
		return ret;
	}

	
	/** formatting from double array to String ; separator : e.g."|" **/ 
	public static String arraydoubleToString(double[] array, String sep) {
		StringBuilder s= new StringBuilder("[");
		for (double data : array){
			s.append(data).append(sep);
		}
		s.append("]");
		return s.toString();
	}

	
	/** formatting from general array to String ; separator : | **/ 
	public static String ArrayToStringWithFormatting(Object[] array) {
		StringBuilder s= new StringBuilder("|");
		for (Object data : array){
			s.append(data).append("|");
		}
		return s.toString();
	}

	/** formatting from int array to String ; separator : | **/ 
	public static String ArrayToStringWithFormatting(int[] bitArray) {
		StringBuilder s= new StringBuilder("|");
		for (int data : bitArray){
			s.append(data).append("|");
		}
		return s.toString();
	}

	/** inversion of the direction of concatenation **/
	public static String ArrayToString2(int[] bitArray) {
		StringBuilder s=new StringBuilder("{");
		for (int data : bitArray){
			s.append(data).append(",");
		}
		s.setCharAt(s.length()-1, '}');
		return s.toString();
	}
	
	/**integer list to str **/
	public static String ArrayToString(List<Integer> intlist) {
		StringBuilder s=new StringBuilder("{");
		for (int data : intlist){
			s.append(data).append(",");
		}
		s.setCharAt(s.length()-1, '}');
		return s.toString();
	}
	
	public static String ArrayToStringGen(List list) {
		StringBuilder s=new StringBuilder("{");
		for (Object data : list){
			s.append(data).append(",");
		}
		if (!list.isEmpty()) {
			s.deleteCharAt(s.length()-1);
		}
		s.append('}');
		return s.toString();
	}

	public static String ArrayToStringGen2(HashSet list) {
		StringBuilder s=new StringBuilder("{");
		for (Object data : list){
			s.append(data).append(",");
		}
		if (!list.isEmpty()) {
			s.deleteCharAt(s.length()-1);
		}
		s.append('}');
		return s.toString();
	}
	
	public static String ArrayToStringGenRaw(List cycleL) {
		if (cycleL.isEmpty()) {
			return "";
		}
		StringBuilder s=new StringBuilder("");
		for (Object data : cycleL){
			s.append(data).append(" ");
		}
		s.deleteCharAt(s.length()-1);
		return s.toString();
	}
	
	/** formatting from bit array to string without any formatting **/
	public static String ArrayToStringNoFormatting(int[] bitArray) {
		StringBuilder s= new StringBuilder();
		for (int data : bitArray){
			s.append(data);
		}
		return s.toString();
	}



	/** 
	 * generates a sequence of sequence of arrays that represent all the 
	 * Cartesian products where the size of set i is given by setSize[i]
	 **/
	public static ArrayList<int []> CartesianProduct(int [] setSize){
		ArrayList<int []> list= new ArrayList<int[]>();
		int n= setSize.length;
		int [] pos = new int[n];
		int cursor=0;
		boolean goright=true, goon=true; 
		while (cursor <n){
			Macro.print( ArrayToStringWithFormatting(pos));
			cursor=0;
			list.add(pos.clone());
			goon= true;
			do { 
				pos[cursor]++; 
				if (pos[cursor] == setSize[cursor]){
					pos[cursor]=0;				
					cursor++;			
				} else{
					goon = false;
				}
			} while ( goon && (cursor<n) );
		}
		return list;
	}	
	/*
	 * cursor=0;
			Macro.print( BitArrayToString(pos));
			while ((cursor<n) && (pos[cursor] == T[cursor] - 1) ) {
					pos[cursor]=0;				
					cursor++;			
			}
			if (cursor < n){
				pos[cursor]++;	
			}
	 */


	/*--------------------
	 * for TESTING
	 --------------------*/

	private static void TestCircPermut(){
		for (int k=3; k<=7; k++){
			FLStringList res= GenerateNonEquivalentBinWords(k);
			Macro.print( k + " sz " + (res.size() + 2));
			for (String config : res){
				//Macro.print(config);
			}
		}
	}


	static String [] A= { "D", "E"}, B1= {"B", "C", "E"}, B2= { "D", "F", "G"}, C= { "B", "C", "F", "G"}; 
	static int [] setSizes = { A.length, B1.length, B2.length, C.length };

	private static void TestCartesianProduct() {
		ArrayList<int []> cartesian= CartesianProduct(setSizes);
		Macro.print(" and now...");
		Set<Integer> NFPrules = new TreeSet<Integer>();
		for (int [] product : cartesian){
			//Macro.print( BitArrayToString(product));
			Set<String> rule = new TreeSet<String>();
			rule.add(A[product[0]]);
			rule.add(B1[product[1]]);
			rule.add(B2[product[2]]);
			rule.add(C[product[3]]);
			StringBuilder srule = new StringBuilder("A");
			for (String letter : rule){
				srule.append(letter);
			}
			srule.append("H");
			long Wcode = ECAlookUpTable.TcodeToWcode(srule.toString()).GetCodeAsLong();
			int minW= (int)ECAlookUpTable.GetMinimalRepresentative(Wcode);
			Macro.print(Wcode + " " + srule) ;
			NFPrules.add(minW);
		}
		Macro.print(" NFP " + NFPrules.toString());
		Macro.print(" NFP len " + NFPrules.size());
	}


	static int [] REVPROBARULES = { 27, 33, 35, 38, 41, 43, 46, 51, 54, 57, 60, 62, 90, 105, 110, 126, 134, 142, 150, 156, 204};

	static void showRevProbaRules(){
		for (int Wcode : REVPROBARULES){
			String Tcode = ECAlookUpTable.GetTransitionCodePadded(RuleCode.FromInt(Wcode));
			Macro.print(Wcode + " : " + Tcode);
		}
	}

	static void ShowNPFrules(){
		IntegerList minimals = ECAlookUpTable.GetEcaMinimalsList();
		for (int Wcode : minimals){
			String Tcode= ECAlookUpTable.GetTransitionCodePadded(RuleCode.FromInt(Wcode));
			boolean 
			A= Tcode.contains("A"),
			B= Tcode.contains("B"),
			C= Tcode.contains("C"),
			D= Tcode.contains("D"),
			E= Tcode.contains("E"),
			F= Tcode.contains("F"),
			G= Tcode.contains("G"),
			H= Tcode.contains("H");
			boolean 
			C0 = A & H, 
			C1= D | E, 
			C2= B | E | C, C3= D | F | G, 
			C4= B | C | F | G ; 
			boolean NPF = C0 & C1 & C2 & C3 & C4;

			//if (NPF) {
			String info= String.format(" Wcode : %s Tcode : %s", Wcode, Tcode);
			Macro.print(info);
			//}
		}
	}


	/** probaTab is NOT supposed to be normalised (weights)
	 * selects one index with a proba proportional to each entry
	 * randSample is the number drawn between 0 & 1
	 */
	public static int RandomSelectByWeight(ArrayList<Double> probaTab, double randSample){

		double totalW= 0;
		for (double val : probaTab){
			totalW += val;
		}
		//Macro.Debug(" sum :"  + totalW );
		double sum=0;
		int index=-1;
		try{
			while(sum<=randSample) {
				index++;
				sum += probaTab.get(index) / totalW; 			
			} 
		} catch (Exception e) {
			Macro.print("PROBA TAB:" + probaTab);
			String msg = 
					String.format("Random select randSample: %.15f index %d sum %f",
							randSample, index, sum);
			Macro.SystemWarning(msg);

			return -MathMacro.DUMMYINT;
		}
		//if (probaTab.size()==6){
		//Macro.FormatDebug("tab : %s rnd: %f sel: %d ",probaTab, randSample, index);
		//}
		return index;
	}

	/** probaTab is supposed to be normalised (sum of entries is one)
	 * selects one index with a proba equal to each entry
	 * randSample is the number drawn between 0 & 1
	 */
	public static int RandomSelect(double[] probaTab, double randSample) {
		int index=-1;
		double sum=0;
		try{
			while(sum<=randSample) {
				index++;
				sum += probaTab[index]; 			
			} 
		} catch (Exception e) {
			String msg = 
					String.format("Random select randSample: %f index %d sum %f",
							randSample, index, sum);
			Macro.SystemWarning(msg);

			return -MathMacro.DUMMYINT;
		}
		return index;
	}

	public static void TestRandomSelect(){
		FLRandomGenerator f = FLRandomGenerator.GetGlobalRandomizer();
		double [] tst= {0, 0.01, .99 }; // sum should be equal to 1
		int [] stat = new int [3];
		for (int sample=0; sample < 1000000; sample++){
			double r = f.RandomDouble();
			int i= RandomSelect(tst, r);
			stat[i]++;
		}
		Macro.fPrint(" %d   %d   %d ", stat[0], stat[1], stat[2]);
	}


	public static double Exp(double d) {
		return Math.exp(d);
	}



	public static int Abs(int val) {
		return Math.abs(val);
	}

	public static int[] OneAggloSizeSample_newmethod(ParticulesAgglomeration MD){
		StringBuilder s = new StringBuilder();
		for (int i = 0; i < MD.GetSize(); i++){
			s.append(MD.GetState(i));
		}
		//Macro.Debug(s);
		String[] splited = s.toString().split("0");
		if (s.charAt(0)=='1' && s.charAt(s.length()-1)=='1'&&splited.length != 1){
			splited[0] += splited[splited.length-1];
			splited[splited.length-1]="";
		}
		int[] result = new int[MD.GetSize()+1];
        for (String aSplited : splited) {
            if (aSplited != "") {
                result[aSplited.length()]++;
            }
        }
		result[0]=0;
		return result;
	}



	public static void DoTest() {
		Macro.print("*** Testing... ***");
		//IntegerList.TestGetPermutations();
		TestRandomSelect();
	}


	public static void main(String[] argv) {
		DoTest();
		Macro.systemExit();
	}

	public static Comparable<RuleCode> FindMin(ArrayList<RuleCode> list) {
		return Collections.min(list);
	}

	/** square of Euclidean distance to a point **/
	public static int EuclideanDistSquare(int x, int y, IntC center) {
		int dx= x - center.X(), dy= y - center.Y();
		return dx*dx+dy*dy;
	}


	/** input a,b ; output : percent that a/b represents (in [0,100]) **/
	public static double Ratio100(int quantity, int total) {
		return HUNDRED*quantity/total;
	}

	/** input: a real between 0 and 1, two digits precision ; out : int between 0 and 100 **/ 
	public static int percentFromRatio(double val) {
		return (int)(100*val);
	}



	public static <Z> Z SelectOneItemAtRandom(ArrayList<Z> listG, StochasticSource src) {
		int indexR=  src.RandomInt( listG.size());
		return listG.get(indexR);
	}



	



	



	

}
