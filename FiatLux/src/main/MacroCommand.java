package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/** useful as a single class ? **/
public class MacroCommand {
	
	/*--------------------------------------------------------------------
	 * INPUT
	 * ------------------------------------------------------------------*/
	
	public final static String prompt(String val, String description) {
		System.out.println(description);
		System.out.print(val +"= ");
	    String answer = null;
		
		//  open up standard input
	    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

	      try {
	    	  answer = br.readLine();
	      } catch (IOException ioe) {
	         System.out.println("IO error trying to read entry!");
	         System.exit(1);
	      }
	      return answer;
	}
	
	public final static int promptI(String val, String description) {
		String answer = prompt(val, description);
		return Integer.valueOf(answer);
	}
	
	public final static double promptD(String val, String description) {
		String answer = prompt(val, description);
		return Double.valueOf(answer);
	}
}
