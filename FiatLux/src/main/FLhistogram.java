package main;

import components.types.FLStringList;
import components.types.IntegerList;
/** basic implementation of histograms **/
public class FLhistogram {
		int [] m_frequency;
		int m_binWidth;
		int m_Nbins;
		
		public FLhistogram(IntegerList data, int binWidth, int nbins){
			m_binWidth= binWidth;
			m_Nbins= nbins;
			m_frequency = new int [m_Nbins + 1];
			for (Integer val : data){
				int binIndex= val / m_binWidth;
				if (binIndex < m_Nbins){
					m_frequency[binIndex]++;
				} else {
					m_frequency[m_Nbins]++; // special for out of range !
				}
			}
		}
		
		public FLStringList GetFrequencies(){
			FLStringList dat= new FLStringList();
			for (int i=0; i < m_frequency.length - 1; i++){
				int min = i * m_binWidth, max= i* m_binWidth -1;
				String str= String.format(" bin %d [%d-%d]: %d", i, min, max, m_frequency[i]);
				dat.Add(str);
			}
			int i = m_Nbins;
			String str= String.format(" bin %d [out]: %d", i, m_frequency[i]);
			dat.Add(str);
			return dat;
		}
}
