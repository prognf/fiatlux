package main.designPanels;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.util.Arrays;
import java.util.Collection;

import javax.swing.JCheckBox;
import javax.swing.JTextPane;

import components.randomNumbers.FLRandomGenerator;
import components.randomNumbers.RandomizerSeedControl;
import experiment.samplers.CAsimulationSampler;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.IconManager;
import grafix.gfxTypes.MacroGFX;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.elements.FLActionButton;
import grafix.gfxTypes.elements.FLBlockPanel;
import grafix.gfxTypes.elements.FLButton;
import grafix.gfxTypes.elements.FLPanel;
import grafix.html.DescriptionHelper;
import grafix.html.TextInterpreter;
import grafix.viewers.AutomatonViewer;
import grafix.viewers.LineAutomatonViewerDefault;
import grafix.viewers.OneRegisterAutomatonViewer;
import grafix.windows.CodeEditorWindow;
import grafix.windows.ConfigurationWindow;
import grafix.windows.MultiButtonStylizedView;
import grafix.windows.NewCAChoicesWindow;
import grafix.windows.SimulationWindowDoble;
import grafix.windows.SimulationWindowSingle;
import main.Macro;
import main.tables.ModelSelectorAllCA;
import models.SuperModel;
import models.CAmodels.CellularModel;
import models.CAmodels.nState.RuleImporterModel;
import topology.basics.SamplerInfo;
import topology.gfx.TopologySelectPanel;
import updatingScheme.gfx.UpdatingSchemeSelector;

public class CAdesignerPanel extends MultiButtonStylizedView {
	// Double ExP. option is suppressed 
	final static String[] LABELS = {"Open", "Double", " ", "New", "Edit", " ", "Options", "About", "Exit"}; //whitespace is for separation
	final static String[] ICONS = {"single", "double", "separator", "newModel", "editModel", "separator", "config", "about", "close"};

	// automatic check of enhanced viewer 
	private static final boolean DEF_SELECTION_ENHANCED = false;
	private static final int DESCRIPTION_SIZEX = 490, DESCRIPTION_SIZEY=100;
	
	FLPanel m_constructionPanel = new FLPanel(); // main panel
	ModelSelectorAllCA m_modelSelectionPanel;
	FLPanel m_topoContainer = new FLPanel();
	TopologySelectPanel m_TopoChoice= new TopologySelectPanel();
	FLActionButton m_ChangeSeedButton= new ChangeGlobalSeedButton();
	//OC
	/*FLActionButton m_importRuleButton = new FLActionButton("Import rule") {
		@Override
		public void DoAction() {
			System.out.println("DoAction");
			importRule();
		}
	};*/
	//\OC

	private JCheckBox m_Enhanced = new JCheckBox("Enhanced color viewer");
	// private JCheckBox m_advanced = new JCheckBox("Advanced");
	
	// description of models
	private JTextPane m_description = new JTextPane();
	private FLButton m_descriptionTitle;

	private Collection m_currentlyDisabledButtons;

	UpdatingSchemeSelector m_SelectUS = new UpdatingSchemeSelector();

	//RuleImporterWindowController m_ruleImporterWindow = new RuleImporterWindowController();
	
	public CAdesignerPanel() {
		super(LABELS, ICONS);

		// description text configuration
		DescriptionHelper.installDescriptionComponent(m_description);
		m_description.setSize(new Dimension(DESCRIPTION_SIZEX, DESCRIPTION_SIZEY));
		m_modelSelectionPanel = new ModelSelectorAllCA(this); // call back HACK
			
		// horizontal zone 1
		FLPanel buttonpanel = GetButtonPanel();

		//SetTopologyPanelVisible(DEF_SELECTTOPO); // dim 1 by default

		//m_description.setSize(new Dimension(490, 190));
		FLBlockPanel descriptionContainer = new FLBlockPanel();
		m_descriptionTitle = new FLButton("Description");
		m_descriptionTitle.getMargin().set(0, 0, 0, 0);
		m_description.getMargin().set(0, 0, 0, 0);
		descriptionContainer.defineAsWindowBlockPanel(FLColor.c_lightyellow, m_descriptionTitle, 170, false);
		descriptionContainer.AddGridBagButton(m_descriptionTitle);
		descriptionContainer.AddGridBagY(m_description);
		// descriptionTitle.setVisible(false);

		// only one is visible
		// m_topoContainer.Add(m_description);
		// m_topoContainer.Add(FLPanel.NewPanel(new Neighborhood(new vonNeumannTM()), new Neighborhood(new Moore8TM()), new Neighborhood(new MooreR5()) ));

		m_topoContainer.Add(descriptionContainer);
		m_topoContainer.Add(m_TopoChoice);
		m_topoContainer.SetBoxLayoutY();
		m_topoContainer.setAlignmentY(TOP_ALIGNMENT);
		m_modelSelectionPanel.setAlignmentY(TOP_ALIGNMENT);

		// m_constructionContainer.Add(m_constructionPanel);

		// option panel
		FLPanel optionsPanel= new FLPanel();
		optionsPanel.Add(m_ChangeSeedButton, m_Enhanced);
		m_Enhanced.setSelected(DEF_SELECTION_ENHANCED);
		// main panel
		SetBoxLayoutY();
		// FLPanel downline= FLPanel.NewPanel(m_SelectUS, optionsPanel);
		Add(m_constructionPanel);

		FLPanel modelSelectionAndSynchronism = FLPanel.NewPanelVertical(m_modelSelectionPanel, m_SelectUS);

		m_topoContainer.Add(buttonpanel);
		m_constructionPanel.SetGridBagLayout();
		m_constructionPanel.SetAnchor(GridBagConstraints.PAGE_START);
		m_constructionPanel.AddGridBag(modelSelectionAndSynchronism, 0, 0);
		m_constructionPanel.AddGridBag(m_topoContainer, 1, 0);
		//m_constructionPanel.Add(m_importRuleButton);
		m_topoContainer.getInsets().set(2, 2, 2, 2);
		modelSelectionAndSynchronism.getInsets().set(2, 2, 2, 2);
		m_modelSelectionPanel.getInsets().set(0, 0, 2, 0);
		m_currentlyDisabledButtons = Arrays.asList();

		// updating of the situation according to the selected model
		m_modelSelectionPanel.sig_Update();
	}
		
	/** assembles the components */
	private CAsimulationSampler GetSimulationSampler() {
		
		CellularModel myModel = m_modelSelectionPanel.GetCurrentlySelectedModel();
		
		// simu sampler creation
		SamplerInfo sampInfo = m_TopoChoice.GetSimulationSamplerInfo(myModel);
		sampInfo.updatingSchemeName = m_SelectUS.GetSelectedUSname();
		
		CAsimulationSampler simuSampler= sampInfo.GetSimulationSampler();
		//OC
		if (simuSampler.GetCellularModel() instanceof RuleImporterModel){
			RuleImporterModel model = (RuleImporterModel) simuSampler.GetCellularModel();
			model.SetSampler(simuSampler);
			System.out.println("Sampler is set");
			//simuSampler.ChangeInit();
		}
		//\OC

		// special enhanced viewing
		if (m_Enhanced.isSelected()){
			Macro.print(4,"Trying to add enchanced viewer...");
			AutomatonViewer v = simuSampler.GetAutomatonViewer();
			int dim= simuSampler.GetTopologyDimension();
			SetEnhanced(v, dim);
		}
		return simuSampler;
	}
	
	private void SetEnhanced(AutomatonViewer v, int dim) {
		if (dim == 1){
			if (v instanceof LineAutomatonViewerDefault){
				LineAutomatonViewerDefault v1D= (LineAutomatonViewerDefault)v;
				v1D.SetEnhanced(PaintToolKit.GetOctopusColors());
			} else {
				Macro.print(4, "enhanced 1D viewer not added...");
			}
		} else { // 2D case
			if (v instanceof OneRegisterAutomatonViewer){
				OneRegisterAutomatonViewer v2D= (OneRegisterAutomatonViewer)v;
				PaintToolKit palette256= PaintToolKit.GetRandomPalette(256);
				palette256.SetColor(0, FLColor.c_white);
				v2D.SetEnhanced(palette256);
			} else {
				Macro.print(4, "enhanced 2D viewer not added...");
			}
		}
		
	}

	/**
	 * creates a new window and displays it, 
	 */
	protected void DoAction(int action) {
		switch(action) {
			case 0: // Open
				OneWindow();
				break;
			case 1: // Double
				TwoWindows();
				break;
			// 2 : Separator
			case 3: // New model
				NewModel();
				break;
			case 4: // Edit model
				EditModel();
				break;
			// 5 : Separator
			case 6: // Configuration
				Configuration();
				break;
			case 7: // About
				main.FLAbout.Show();
				break;
			case 8: // Exit
				Macro.systemExit();
				break;
			default:
				break;
		}
	}
	
	private void OneWindow(){
		SimulationWindowSingle exp;
		try{
			CAsimulationSampler mysamp = GetSimulationSampler();
			exp = new SimulationWindowSingle(MacroGFX.FLSIMULATION,mysamp);
			int dim = mysamp.GetTopologyDimension();
			if (dim ==1 ){	// special setting for dimension one 
				int Tdim = mysamp.GetTsize();
				exp.SetIncrementStep(Tdim - 1);
			}
			//
			exp.BuildAndDisplay();
			exp.sig_Init();       // init signal sent automatically
			GetSimulationSampler();
		} catch (Exception e) {
			Macro.UserWarning(e, "Could not create Simulation Sampler !");
		}		
	}


	private void TwoWindows() {
		SimulationWindowDoble exp;
		try {
			CAsimulationSampler mysampA = GetSimulationSampler();
			CAsimulationSampler mysampB = GetSimulationSampler();
			exp = new SimulationWindowDoble(MacroGFX.FLSIMULATION,mysampA,mysampB);
			int dim = mysampA.GetTopologyDimension();
			if (dim == 1) {	// special setting for dimension one
				int Tdim= mysampA.GetTsize();
				exp.SetIncrementStep(Tdim - 1);
			}
			//
			exp.BuildAndDisplay();
			exp.sig_Init();       // init signal sent automatically
		} catch (Exception e) {
			Macro.UserWarning(e, "Could not create Simulation Sampler !");
		}		
	}

	private void NewModel() {
		(new NewCAChoicesWindow()).packAndShow();
	}

	/** NG : edition N. Gauville **/
	private void EditModel() {
		String modelName = m_modelSelectionPanel.GetCurrentlySelectedModel().GetName();
		if (CodeEditorWindow.isModelEditable(modelName)) {
			(new CodeEditorWindow(modelName)).packAndShow();
		}
	}

	private void Configuration() {
		(new ConfigurationWindow()).packAndShow();
	}

	/** change notification */
	public void sig_Update(){
		SuperModel model = m_modelSelectionPanel.GetCurrentlySelectedModel();
		String description = TextInterpreter.getInstance().getModelDescription(model.GetName());
		m_description.setText("<div class='block'>" + description + "</div>");
		m_descriptionTitle.setText(model.GetName());
		m_descriptionTitle.setIcon(IconManager.getCAIcon(model.GetName()));
		Collection currentModelDisabledTopo = m_modelSelectionPanel.GetCurrentlySelectedModelDisabledTopology();
		Collection<String> disable = currentModelDisabledTopo;
		Collection<String> enable = m_currentlyDisabledButtons;
		if(m_currentlyDisabledButtons!=null && currentModelDisabledTopo != null){
			disable.removeAll(m_currentlyDisabledButtons);
			enable.removeAll(currentModelDisabledTopo);
		}

		m_TopoChoice.SetEnabledTopology(disable, false );
		m_TopoChoice.SetEnabledTopology(enable, true);
		m_currentlyDisabledButtons = currentModelDisabledTopo;

		String associatedtopo = m_modelSelectionPanel.GetCurrentlySelectedModelAssociatedTopology();
		m_TopoChoice.SelectTopology(associatedtopo);



		// Update the Edit button to disable it if the selected model isnt editable
		getButtonById(4).setIcon(IconManager.getUIIcon("editModel" + (CodeEditorWindow.isModelEditable(model.GetName()) ? "" : "Disabled")));

		// update GFX
		validate();
	}
	
	class ChangeGlobalSeedButton extends FLActionButton {
		public ChangeGlobalSeedButton() {
			super(RandomizerSeedControl.S_SEED);
		}
		@Override
		public void DoAction() {
			int newseed= MacroGFX.ReadIntegerInWindow();
			FLRandomGenerator.SetDefaultSeed(newseed);			
		}
	}

	//OC
	/*@FXML
	public void importRule() {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/grafix/windows/RuleImporterWindow.fxml"));
		System.out.println("ImportRule");

		loader.setControllerFactory(iC->{
			if(iC.equals(grafix.windows.RuleImporterWindowController.class)){
				return m_ruleImporterWindow;
			}
			else{
				System.out.println("Au revoir");
				return null;
			}
		});
		//Parent root = loader.load();
		//stage.setScene(new Scene(root, 1500, 800));
		//abonnementsController.charger();

	}*/
	//\OC
}
