package main.designPanels;

import java.awt.Dimension;

import javax.swing.JTextPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import architecture.basisIRMAS.IRMASsampler;
import architecture.basisIRMAS.IRMAsystem;
import architecture.multiAgent.tools.MultiAgentSampler;
import architecture.multiAgent.tools.MultiAgentSystem;
import architecture.multiAgent.tools.MultiAgentSystem.EXCLUSIONMODE;
import architecture.multiAgent.tools.MultiAgentSystem.UPDATEPOLICY;
import architecture.rigidLatticeGas.BinLGsystem;
import architecture.rigidLatticeGas.RigidLGsampler;
import components.types.IntC;
import experiment.samplers.SuperSampler;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.IconManager;
import grafix.gfxTypes.controllers.Radio2Control;
import grafix.gfxTypes.elements.FLBlockPanel;
import grafix.gfxTypes.elements.FLButton;
import grafix.gfxTypes.elements.FLList;
import grafix.gfxTypes.elements.FLPanel;
import grafix.gfxTypes.elements.FLTabbedPane;
import grafix.html.DescriptionHelper;
import grafix.html.TextInterpreter;
import grafix.windows.ConfigurationWindow;
import grafix.windows.MultiButtonStylizedView;
import grafix.windows.SimulationWindowSingle;
import main.FiatLux;
import main.Macro;
import main.tables.ModelListCellRenderer;
import models.MASmodels.DLA.DLAsystem;
import models.MASmodels.IRMASmodels.ErraticJammingSystem;
import models.MASmodels.IRMASmodels.NqueensSystem;
import models.MASmodels.amoebae.AmoebaeInfotaxisSampler;
import models.MASmodels.amoebae.AmoebaeInfotaxisSystem;
import models.MASmodels.dervish.DervishSampler;
import models.MASmodels.dervish.DervishSystem;
import models.MASmodels.rotor.RotorSystem;
import models.MASmodels.turmite.TurmiteSystemModel1;
import models.MASmodels.turmite.TurmiteSystemModel2;
import topology.gfx.SizeParManagerTwoD;

/*--------------------
 * for the creation of experiments
 * 
 * @author Nazim Fates
 *--------------------*/

public class MASdesignerPanel extends MultiButtonStylizedView {

	final static String TITLE = "FiatLux : SMA simulator";
	final static String[] LABELS = {"Open", " ", "Options", "About", "Exit"};
	final static String[] ICONS = {"single", "separator", "config", "about", "close"};
	final static String[] EXPLIST = {"M1-Allow/Exclu", "M2-Allow/Exclu", "M1M2-Allow", "M1M2-Exclu"};

	private JTextPane m_description = new JTextPane();
	private FLButton m_descriptionTitle;

	/** ADD MODELS HERE **/
	enum MasModelChoice 
	{ Dervishes, Turmites, AmoebaeTaxis, rotor, NQueens, ErraticJams, DLA, rigidLG}

    //see Resource.GFX for initial choice
	final static int DEF_SELECT=0;

	final static String[]   UPDATELIST= 
		{"Synchronous","Sequential-n","Sequential-1"};
	final static UPDATEPOLICY [] POLICY =
		{ UPDATEPOLICY.SYNCHRONOUS, UPDATEPOLICY.SEQUENTIAL, UPDATEPOLICY.SEQUENTIAL_STEP};

	/*--------------------
	 * 3 buttons
	 --------------------*/
	protected void DoAction(int action) {
		switch (action) {
			case 0: // Open
				DoAction1();
				break;
			// 1: separator
			case 2: // Config
				Configuration();
				break;
			case 3: // About
				main.FLAbout.Show();
				break;
			case 4:
				Macro.systemExit();
				break;
		}
	}

	private void Configuration() {
		(new ConfigurationWindow()).packAndShow();
	}

	/* one or two windows */
	protected void DoAction1(){
		if (m_LeftRightPanel.GetSelectedTabIndex()==0){
			DoExperimentOneWindow();
		} 
	}	


	/*--------------------
	 * Attributes
	 --------------------*/

	// for choosing the model
	ModelChoiceControl mF_ModelChoice = new ModelChoiceControl();

	// Topology Management
	SizeParManagerTwoD m_SizeParManager = new SizeParManagerTwoD();

	// old ?
	FLList mF_DoubleExpType = new FLList(EXPLIST);

	FLPanel m_LPanel; // left panel
	FLTabbedPane m_LeftRightPanel;

	// update policy
	UpdateControl mF_Update = new UpdateControl();
	Radio2Control 
	mF_AllowExlu = new Radio2Control("Policy :","Allow","Exclu"),
	mF_M1M2 = new Radio2Control("Model :","1","2");

	/*--------------------
	 * Constructor
	 --------------------*/

	public MASdesignerPanel() {
		super(LABELS, ICONS);

		// adding the window panels
		FLPanel simulationpanel= GetSpecificPanel(); 
		this.Add(simulationpanel);
		mF_DoubleExpType.SelectItem(DEF_SELECT);

		mF_ModelChoice.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				updateSelection();
			}
		});

		updateSelection();
	}

	/*--------------------
	 * main methods
	 --------------------*/

	/** ADD MODELS HERE
	 * SINGLE window choice */
	private void DoExperimentOneWindow() {
		MasModelChoice choice= mF_ModelChoice.GetSelectedModel();
		IntC dim= m_SizeParManager.GetXYValue();
		MultiAgentSystem system= null;
		switch (choice){

		case rotor:
			system= new RotorSystem(dim);
			break;
		case DLA:
			system= new DLAsystem(dim);
			break;
		case ErraticJams:
			LaunchErraticJammingExp();
			break;
		case NQueens:
			LaunchNqueensExp();
			break;
		}
		if (system!=null){
			ShowExpSingle(system);
		} 
		// non-standard cases
		switch (choice){
		case Turmites:
			LaunchTurmitesExp();
			break;
		case Dervishes:
			LaunchDervishesExp();
			break;
		case AmoebaeTaxis:
			LaunchAmoebaeInfotaxisExp();
			break;
		case rigidLG:
			LaunchrigidLG();
			break;
		}


	}



	private MultiAgentSampler GetSampler(MultiAgentSystem system) {
		IntC PixCellSize= m_SizeParManager.GetCellPixSize();
		return new MultiAgentSampler(system, PixCellSize);
	}

	void ShowExpSingle(MultiAgentSystem system){
		Macro.print(" showing exp with model:" + system.GetModelName());
		SuperSampler sampler = GetSampler(system);
		ShowExpSingleFromSampler(sampler);
	}

	void ShowExpSingleFromSampler(SuperSampler sampler){
		SimulationWindowSingle exp=
				new SimulationWindowSingle("single simulation",sampler);
		exp.sig_Init(); 
		exp.BuildAndDisplay();
	}

	//--------------------------------------------------------------------------
	//- LAUNCH : ADD MODELS HERE
	//--------------------------------------------------------------------------

	private static final int NAGENT = 40;
	private static final String TURMITEPANELLEGEND = "Turmite experiment type";

	private void LaunchAmoebaeInfotaxisExp() {
		IntC gridSize= m_SizeParManager.GetXYValue();
		IntC pixCellSize= m_SizeParManager.GetCellPixSize();
		AmoebaeInfotaxisSystem system= new AmoebaeInfotaxisSystem(gridSize);
		AmoebaeInfotaxisSampler sampler= new AmoebaeInfotaxisSampler(system, pixCellSize);
		ShowExpSingleFromSampler(sampler);
	}

	private void LaunchErraticJammingExp() {
		IntC dim= m_SizeParManager.GetXYValue();
		IntC pixCellSize= m_SizeParManager.GetCellPixSize();
		IRMAsystem system = new ErraticJammingSystem(dim,NAGENT);
		IRMASsampler sampler= new IRMASsampler(system, pixCellSize);
		ShowExpSingleFromSampler(sampler);
	}

	private void LaunchNqueensExp() {
		IntC dim= m_SizeParManager.GetXYValue();
		IntC pixCellSize= m_SizeParManager.GetCellPixSize();
		
		IRMAsystem system = new NqueensSystem(dim.X());
		// hack for forcing border
		IRMASsampler sampler= new IRMASsampler(system, new IntC(pixCellSize.X(), 1));
		ShowExpSingleFromSampler(sampler);
	}


	void LaunchDervishesExp(){
		IntC dim= m_SizeParManager.GetXYValue();
		IntC pixCellSize= m_SizeParManager.GetCellPixSize();
		DervishSystem system= new DervishSystem(dim);
		SuperSampler sampler = new DervishSampler(system, pixCellSize);
		ShowExpSingleFromSampler(sampler);
	}
	
	void LaunchTurmitesExp(){
		int modeltype = 1;
		if (!mF_M1M2.IsFirstItemSelected()){
			modeltype= 2;
		}
		MultiAgentSystem.UPDATEPOLICY 	upd= mF_Update.GetSelectedUpdatePolicy();
		MultiAgentSystem.EXCLUSIONMODE 	exc= MultiAgentSystem.EXCLUSIONMODE.ALLOW_POLICY;
		if (!mF_AllowExlu.IsFirstItemSelected()){
			exc= EXCLUSIONMODE.EXCLUDE_POLICY;
		} 

		IntC dim= m_SizeParManager.GetXYValue();
		MultiAgentSystem system= null;
		switch (modeltype){
		case 1:
			system= new TurmiteSystemModel1(dim);
			break;
		case 2:
			system= new TurmiteSystemModel2(dim);
			break;
		}
		system.SetUpdateMode( upd );
		system.SetExclusionMode( exc );
		ShowExpSingle(system);
	}


	private void LaunchrigidLG() {
		int Lsize= 	m_SizeParManager.GetXYValue().X();
		int Tsize= 	m_SizeParManager.GetXYValue().Y();
		BinLGsystem system= new BinLGsystem(Lsize);
		IntC pix= m_SizeParManager.GetCellPixSize();
		SuperSampler sampler = new RigidLGsampler(system, pix, Tsize);
		ShowExpSingleFromSampler(sampler);
	}


	/*--------------------
	 * GFX
	 --------------------*/

	public FLPanel GetPanelForExternalInclusion(){
		return GetSpecificPanel();
	}

	protected FLPanel GetSpecificPanel() {
		// layout management

		FLPanel buttonpanel = GetButtonPanel();
		FLPanel ConstructionPanel = new FLPanel();

		FLBlockPanel modelChoiceContainer = new FLBlockPanel();
		FLButton modelChoiceTitle = new FLButton("Models");
		modelChoiceContainer.defineAsWindowBlockPanel(FLColor.c_grey0, modelChoiceTitle, 570, false);
		modelChoiceContainer.setDefaultWidth(155);
		modelChoiceContainer.AddGridBagButton(modelChoiceTitle);
		modelChoiceContainer.AddGridBagY(mF_ModelChoice);
		mF_ModelChoice.setCellRenderer(new ModelListCellRenderer());
		mF_ModelChoice.setPreferredSize(new Dimension(130, 500));

		ConstructionPanel.add(modelChoiceContainer);

		// L panel
		m_LPanel= FLPanel.NewPanelVertical(mF_AllowExlu, mF_M1M2, mF_Update);

		// L / R  panels
		m_LeftRightPanel = new FLTabbedPane();
		m_LeftRightPanel.AddTab("Single", m_LPanel);
		m_LeftRightPanel.AddTab("Double", mF_DoubleExpType);

		FLBlockPanel turmites = new FLBlockPanel();
		FLButton turmitesTitle = new FLButton(TURMITEPANELLEGEND);
		turmites.defineAsWindowBlockPanel(FLColor.c_lightgreen, turmitesTitle, 400, false);
		turmites.AddGridBagButton(turmitesTitle);
		turmites.AddGridBagY(m_LeftRightPanel);
		turmites.setDefaultWidth(380);
		m_LeftRightPanel.setPreferredSize(new Dimension(300, 250));

		DescriptionHelper.installDescriptionComponent(m_description);
		FLBlockPanel descriptionContainer = new FLBlockPanel();
		m_descriptionTitle = new FLButton("Description");
		descriptionContainer.defineAsWindowBlockPanel(FLColor.c_lightyellow, m_descriptionTitle, 170, false);
		descriptionContainer.AddGridBagButton(m_descriptionTitle);
		descriptionContainer.AddGridBagY(m_description);
		// descriptionContainer.setDefaultWidth(715);
		ConstructionPanel.add(FLPanel.NewPanelVertical(descriptionContainer, FLPanel.NewPanel(turmites, m_SizeParManager)));
		// ConstructionPanel.add(m_SizeParManager);
		m_descriptionTitle.getMargin().set(0, 0, 0, 0);
		m_description.getMargin().set(0, 0, 0, 0);
		m_description.setPreferredSize(new Dimension(710, 150));


		FLPanel mainpanel= new FLPanel();
		mainpanel.SetBoxLayoutY();
		mainpanel.add(ConstructionPanel);

		//mainpanel.add(turmites);
		// mainpanel.add(new FLLabel(TURMITEPANELLEGEND));
		// mainpanel.add(m_LeftRightPanel);

		mainpanel.add(buttonpanel);
		return mainpanel;

	}

	public void updateSelection() {
		String modelName = mF_ModelChoice.GetSelectedItemName();
		String description = TextInterpreter.getInstance().getModelDescription(modelName);
		m_description.setText("<div class='block'>" + description + "</div>");
		m_descriptionTitle.setText(modelName);
		m_descriptionTitle.setIcon(IconManager.getCAIcon(modelName));
		m_description.setSize(710, 150);
	}

	/*--------------------
	 * Get & Set 
	 --------------------*/

	public void SetDimInitalValue(IntC XY){
		m_SizeParManager.SetXY(XY);
	}

	/*--------------------
	 * GFX
	 --------------------*/


	class ModelChoiceControl extends FLList{

		public ModelChoiceControl() {
			super(MasModelChoice.values(),FiatLux.INI_SMA_SELECT_MODEL);
		}

		public MasModelChoice GetSelectedModel() {
			int rank = GetSelectedItemRank();
			return MasModelChoice.values()[rank];		
		}

	}

	class UpdateControl extends FLList{

		public UpdateControl() {
			super(UPDATELIST);
		}

		public UPDATEPOLICY GetSelectedUpdatePolicy() {
			int rank = GetSelectedItemRank();
			return POLICY[rank];		
		}

	}

}
