package main.designPanels;

import java.awt.Dimension;
import java.awt.event.ActionEvent;

import javax.swing.JTextPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import architecture.dtasep.TasepSampler;
import architecture.interactingParticleSystems.IPSsampler;
import architecture.interactingParticleSystems.IPSsamplerAbstract;
import architecture.interactingParticleSystems.IPSsamplerBimodal;
import architecture.interactingParticleSystems.InteractingParticleSystemOneDSampler;
import components.types.IntC;
import experiment.samplers.SuperSampler;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.IconManager;
import grafix.gfxTypes.elements.FLBlockPanel;
import grafix.gfxTypes.elements.FLButton;
import grafix.gfxTypes.elements.FLLabel;
import grafix.gfxTypes.elements.FLList;
import grafix.gfxTypes.elements.FLPanel;
import grafix.gfxTypes.elements.NeighborhoodButton;
import grafix.html.DescriptionHelper;
import grafix.html.TextInterpreter;
import grafix.windows.ConfigurationWindow;
import grafix.windows.MultiButtonStylizedView;
import grafix.windows.SimulationWindowSingle;
import main.FiatLuxProperties;
import main.Macro;
import main.tables.ModelListCellRenderer;
import models.IPSmodels.IPSmodel.CODE_IPS_MODEL;
import topology.basics.TopologyCoder;
import topology.gfx.SizeParManagerTwoD;

/*--------------------
 * allows the creation of experiments with interacting particle systems
*--------------------*/

public class IPSdesignerPanel extends MultiButtonStylizedView {
	final static String 	TITLE = "FiatLux : IPS simulator";
	final static String[] 	LABELS = {"Open", " ", "Options", "About", "Exit"};
	final static String[] 	ICONS = {"single", "separator", "config", "about", "close"};
	final static String 	TOPOLOGY = "Topology choice";
	private JTextPane m_description = new JTextPane();
	private FLButton m_descriptionTitle;

	final static int DEF_SELECT = FiatLuxProperties.IpsModel_TAB_DEF_SELECTION;

	/*--------------------
	 * Attributes
	 --------------------*/

	// for chosing the model
	IPSModelChoice mF_ModelChoice = new IPSModelChoice();

	// Topology Management
	SizeParManagerTwoD m_SizeParManager = new SizeParManagerTwoD();

	// choosing the 2 topologies
	//StringTextField mc_topoP= new StringTextField(3, DEF_TP);
	//StringTextField mc_topoE= new StringTextField(3, DEF_TE);
	final String [] TOPOTYPE = { "TV4", "TX4", "TM8", "FSLTR" };
	FLList mc_topoP= new FLList(TOPOTYPE);
	FLList mc_topoE= new FLList(TOPOTYPE);

	private NeighborhoodButton p1, p2, p3, p4, e1, e2, e3, e4;

	/*--------------------
	 * Constructor
	 --------------------*/
	public IPSdesignerPanel() {
		super(LABELS, ICONS);
		// adding the window panels
		FLPanel simulationpanel= GetSpecificPanel(); 
		this.Add(simulationpanel);

		mF_ModelChoice.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				updateSelection();
			}
		});

		updateSelection();
		selectTopoButton(p1);
		selectTopoButton(e1);
	}

	/*--------------------
	 * 3 buttons
	 --------------------*/
	protected void DoAction(int action) {
		switch(action) {
			case 0: // Open
				DoAction1();
				break;
			// 1: separator
			case 2: // Config
				Configuration();
				break;
			case 3: // About
				main.FLAbout.Show();
				break;
			case 4: // Exit
				Macro.systemExit();
				break;
		}
	}

	private void Configuration() {
		(new ConfigurationWindow()).packAndShow();
	}

	/* one or two windows */
	protected void DoAction1(){
		LaunchIPSExperiment();
	}	

	/*--------------------
	 * main methods
	 --------------------*/

	void ShowExpSingle(SuperSampler simulation){
		SimulationWindowSingle exp=
				new SimulationWindowSingle("single simulation",simulation);
		exp.sig_Init(); 
		exp.BuildAndDisplay();
	}


	void LaunchIPSExperiment() {
		IntC XYsize= m_SizeParManager.GetXYValue();
		IntC CellSize= m_SizeParManager.GetCellPixSize();

		
		// topology & model identification
		String 
		codeP = mc_topoP.GetSelectedItemName(),
		codeE = mc_topoE.GetSelectedItemName();
		/*Macro.Debug(" P " + codeP + "  E " + codeE);*/		

		CODE_IPS_MODEL codeModel= mF_ModelChoice.GetSelectedModel();	
		// HACK : TODO remove & clean
		if (codeModel==CODE_IPS_MODEL.stochosP) {
			InteractingParticleSystemOneDSampler sampler= 
					new InteractingParticleSystemOneDSampler(XYsize.X(),XYsize.Y(),CellSize);
			ShowExpSingle(sampler);
		} else if (codeModel== CODE_IPS_MODEL.PhaseSynch){
			IPSsamplerAbstract sampler= new IPSsamplerBimodal(XYsize,CellSize);
			ShowExpSingle(sampler);
		} else if (codeModel == CODE_IPS_MODEL.highwayTASEP){ // special case  
			int size= XYsize.X();
			TasepSampler sampler= new TasepSampler(size);
			int Tbuffer= XYsize.Y();
			sampler.AddViewer(CellSize, Tbuffer);
			ShowExpSingle(sampler);
		} else { // general  case : 2D
			Macro.print("general case IPS selection");
			IPSsampler sampler= new IPSsampler(codeP, codeE, XYsize, codeModel);
			sampler.AddViewer(CellSize);
			ShowExpSingle(sampler);
		}
	}

	public void updateSelection() {
		String modelName = mF_ModelChoice.GetSelectedItemName();
		String description = TextInterpreter.getInstance().getModelDescription(modelName);
		m_descriptionTitle.setText(modelName);
		m_description.setText("<div class='block'>" + description + "</div>");
		m_descriptionTitle.setIcon(IconManager.getCAIcon(modelName));
		m_description.setSize(710, 150);
	}

	/*--------------------
	 * GFX
	 --------------------*/
	private void SpecialHighwayDoubleTasep(int size) {
	}

	public FLPanel GetPanelForExternalInclusion(){
		return GetSpecificPanel();
	}

	protected FLPanel GetSpecificPanel() {
		// layout management
		FLPanel buttonpanel= GetButtonPanel();

		FLPanel ConstructionPanel= new FLPanel();
		// ConstructionPanel.setLayout(new BorderLayout());

		FLBlockPanel modelChoiceContainer = new FLBlockPanel();
		FLButton modelChoiceTitle = new FLButton("Models");
		modelChoiceContainer.defineAsWindowBlockPanel(FLColor.c_grey0, modelChoiceTitle, 570, false);
		modelChoiceContainer.setDefaultWidth(155);
		modelChoiceContainer.AddGridBagButton(modelChoiceTitle);
		modelChoiceContainer.AddGridBagY(mF_ModelChoice);
		mF_ModelChoice.setCellRenderer(new ModelListCellRenderer());
		mF_ModelChoice.setPreferredSize(new Dimension(130, 500));


		FLBlockPanel TopologyChoice = new FLBlockPanel();
		FLButton topologyTitle = new FLButton(TOPOLOGY);
		TopologyChoice.defineAsWindowBlockPanel(FLColor.c_lightgreen, topologyTitle, 400, false);
		TopologyChoice.AddGridBagButton(topologyTitle);

		p1 = new NeighborhoodButton(TopologyCoder.GetTopologyFromCode("TV4"));
		p2 = new NeighborhoodButton(TopologyCoder.GetTopologyFromCode("TX4"));
		p3 = new NeighborhoodButton(TopologyCoder.GetTopologyFromCode("TM8"));
		p4 = new NeighborhoodButton(TopologyCoder.GetTopologyFromCode("FSLTR"));
		p1.addActionListener(this);
		p2.addActionListener(this);
		p3.addActionListener(this);
		p4.addActionListener(this);
		FLPanel pr1 = FLPanel.NewPanel(p1, p2), pr2 = FLPanel.NewPanel(p3, p4);
		FLPanel perceptionButtons = FLPanel.NewPanelVertical(pr1, pr2);
		FLPanel perceptionList = FLPanel.NewPanelVertical(new FLLabel("Perception"), perceptionButtons);
		// JoinLabelAndList("Perception", mc_topoP);

		e1 = new NeighborhoodButton(TopologyCoder.GetTopologyFromCode("TV4"));
		e2 = new NeighborhoodButton(TopologyCoder.GetTopologyFromCode("TX4"));
		e3 = new NeighborhoodButton(TopologyCoder.GetTopologyFromCode("TM8"));
		e4 = new NeighborhoodButton(TopologyCoder.GetTopologyFromCode("FSLTR"));
		e1.addActionListener(this);
		e2.addActionListener(this);
		e3.addActionListener(this);
		e4.addActionListener(this);
		FLPanel er1 = FLPanel.NewPanel(e1, e2), er2 = FLPanel.NewPanel(e3, e4);
		FLPanel exchangeButtons = FLPanel.NewPanelVertical(er1, er2);
		FLPanel exchangeList = FLPanel.NewPanelVertical(new FLLabel("Exchange"), exchangeButtons);
		// JoinLabelAndList("Exchange", mc_topoE);

		pr1.setOpaque(false);
		pr2.setOpaque(false);
		er1.setOpaque(false);
		er2.setOpaque(false);
		perceptionButtons.setOpaque(false);
		perceptionList.setOpaque(false);
		exchangeButtons.setOpaque(false);
		exchangeList.setOpaque(false);
		FLPanel lists = FLPanel.NewPanelVertical(perceptionList, exchangeList);
		lists.setOpaque(false);
		TopologyChoice.AddGridBagY(lists);
		TopologyChoice.setDefaultWidth(380);

		DescriptionHelper.installDescriptionComponent(m_description);
		FLBlockPanel descriptionContainer = new FLBlockPanel();
		m_descriptionTitle = new FLButton("Description");
		descriptionContainer.defineAsWindowBlockPanel(FLColor.c_lightyellow, m_descriptionTitle, 170, false);
		descriptionContainer.AddGridBagButton(m_descriptionTitle);
		descriptionContainer.AddGridBagY(m_description);
		// descriptionContainer.setDefaultWidth(715);
		m_descriptionTitle.getMargin().set(0, 0, 0, 0);
		m_description.getMargin().set(0, 0, 0, 0);
		m_description.setPreferredSize(new Dimension(710, 150));

		ConstructionPanel.Add(modelChoiceContainer, FLPanel.NewPanelVertical(descriptionContainer, FLPanel.NewPanel(TopologyChoice, m_SizeParManager)));

		FLPanel mainpanel= new FLPanel();
		mainpanel.SetBoxLayoutY();
		mainpanel.add(ConstructionPanel);
		mainpanel.add(buttonpanel);
		//mainpanel.add(TopologyChoice);
		return mainpanel;
	}

	@Override
	public void actionPerformed(ActionEvent e){
		super.actionPerformed(e);

		if (e.getSource() == e1) {
			mc_topoE.SelectItem(0);
		} else if (e.getSource() == e2) {
			mc_topoE.SelectItem(1);
		} else if (e.getSource() == e3) {
			mc_topoE.SelectItem(2);
		} else if (e.getSource() == e4) {
			mc_topoE.SelectItem(3);
		} else if (e.getSource() == p1) {
			mc_topoP.SelectItem(0);
		} else if (e.getSource() == p2) {
			mc_topoP.SelectItem(1);
		} else if (e.getSource() == p3) {
			mc_topoP.SelectItem(2);
		} else if (e.getSource() == p4) {
			mc_topoP.SelectItem(3);
		} else {
			return;
		}

		if (e.getSource() instanceof NeighborhoodButton) {
			selectTopoButton((NeighborhoodButton) e.getSource());
		}
	}

	private void selectTopoButton(NeighborhoodButton b) {
		if (b == p1 || b == p2 || b == p3 || b == p4) {
			p1.unselectButton();
			p2.unselectButton();
			p3.unselectButton();
			p4.unselectButton();
		} else if (b == e1 || b == e2 || b == e3 || b == e4) {
			e1.unselectButton();
			e2.unselectButton();
			e3.unselectButton();
			e4.unselectButton();
		}

		b.selectButton();
	}

	/*--------------------
	 * Get & Set 
	 --------------------*/
	public void SetDimInitalValue(IntC XY){
		m_SizeParManager.SetXY(XY);
	}

	/*--------------------
	 * GFX
	 --------------------*/
	class IPSModelChoice extends FLList {
		public IPSModelChoice() {
			super(CODE_IPS_MODEL.values(),DEF_SELECT);
		}

		public CODE_IPS_MODEL GetSelectedModel() {
			int rank = GetSelectedItemRank();
			return CODE_IPS_MODEL.values()[rank];		
		}
	}
}
