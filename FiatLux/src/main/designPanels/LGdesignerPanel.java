package main.designPanels;

import java.awt.Dimension;

import javax.swing.JComponent;
import javax.swing.JTextPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import architecture.latticeKinesis.LatticeKinesisSampler;
import architecture.latticeKinesis.LatticeKinesisSystem;
import architecture.latticeKinesis.LatticeKrandomCollisions;
import architecture.latticeKinesis.SwarmingLGCAsys;
import components.types.BooleanPar;
import components.types.IntC;
import experiment.samplers.SuperSampler;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.IconManager;
import grafix.gfxTypes.elements.FLBlockPanel;
import grafix.gfxTypes.elements.FLButton;
import grafix.gfxTypes.elements.FLList;
import grafix.gfxTypes.elements.FLPanel;
import grafix.html.DescriptionHelper;
import grafix.html.TextInterpreter;
import grafix.windows.ConfigurationWindow;
import grafix.windows.MultiButtonStylizedView;
import grafix.windows.SimulationWindowSingle;
import main.FiatLux;
import main.Macro;
import main.tables.ModelListCellRenderer;
import topology.gfx.SizeParManagerTwoD;

/*--------------------
 * The Experiment Manager class allows the creation of experiments
 * @author Nazim Fates
*--------------------*/

public class LGdesignerPanel extends MultiButtonStylizedView {
	final static String 	TITLE= "FiatLux : LGCA simulator";
	final static String[] 	LABELS= { "Open", " ", "Options", "About", "Exit" };
	final static String[] ICONS = {"single", "separator", "config", "about", "close"};
	private JTextPane m_description = new JTextPane();
	private FLButton m_descriptionTitle;

	enum ModelChoice {Flocking, Collision}

    enum TopoChoice {NESW, Hex, Moore8, Moore9}

    /*--------------------
	 * 3 buttons
	 *--------------------*/
	protected void DoAction(int action) {
		switch(action) {
			case 0: // Open
				DoAction1();
				break;
			// 1 : separator
			case 2: // Config
				Configuration();
				break;
			case 3: // About
				main.FLAbout.Show();
				break;
			case 4: // Exit
				Macro.systemExit();
				break;
		}
	}

	private void Configuration() {
		(new ConfigurationWindow()).packAndShow();
	}

	public void updateSelection() {
		String modelName = mF_ModelChoice.GetSelectedItemName();
		String description = TextInterpreter.getInstance().getModelDescription(modelName);
		m_description.setText("<div class='block'>" + description + "</div>");
		m_descriptionTitle.setText(modelName);
		m_descriptionTitle.setIcon(IconManager.getCAIcon(modelName));
		m_description.setSize(710, 150);
	}

	/** MAIN : creates the sampler & launch exp */
	protected void DoAction1(){
		ModelChoice modelChoice= mF_ModelChoice.GetSelectedModel();
		IntC XYsize = m_SizeParManager.GetXYValue();
		IntC pixCellSize = m_SizeParManager.GetCellPixSize();
		LatticeKinesisSystem system=null;
		if (modelChoice == ModelChoice.Flocking){ // swarming == flocking
			
			boolean reflectingB= m_ReflectingBorder.GetVal();
			system = new SwarmingLGCAsys(XYsize,reflectingB);
			
		} else if (modelChoice == ModelChoice.Collision) {
			system= new LatticeKrandomCollisions(XYsize);
		}
		SuperSampler sampler = new LatticeKinesisSampler(system, pixCellSize);
		SimulationWindowSingle exp= new SimulationWindowSingle(main.FiatLux.FIATLUX_VERSION, sampler);
		exp.BuildAndDisplay();
	
	}

/*	private String ChoiceToCode(TopoChoice topoChoice) {
		switch (topoChoice){
		case NESW:
			return TopologyCoder.NESW;
		case Hex:
			return TopologyCoder.HEXAGONAL;
		case Moore8:
			return TopologyCoder.MOORE8;
		case Moore9:
			return TopologyCoder.MOORE9;
		default:
			Macro.FatalError(" topo not found : " + topoChoice);
			return null;
		}
	}
*/
	/*--------------------
	 * Attributes
	 *--------------------*/

	// for chosing the model
	ModelChoiceControl mF_ModelChoice = new ModelChoiceControl();

	BooleanPar m_ReflectingBorder = new BooleanPar("reflecting borders", false);
	
	// for chosing the topology
	// TopoChoiceControl mF_TopoChoiceInteraction= new TopoChoiceControl();
	// TopoChoiceControl mF_TopoChoicePerception= new TopoChoiceControl();

	// Topology Management
	SizeParManagerTwoD m_SizeParManager = new SizeParManagerTwoD();


	/*--------------------
	 * Constructor
	 *--------------------*/
	public LGdesignerPanel() {
		super(LABELS, ICONS);

		// adding the window panels
		Add(GetSpecificPanel());

		mF_ModelChoice.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				updateSelection();
			}
		});

		updateSelection();
	}

	/*--------------------
	 * main methods
	 *--------------------*/
	void ShowExpSingle(SuperSampler simulation){
		SimulationWindowSingle exp = new SimulationWindowSingle("single simulation",simulation);
		exp.sig_Init(); 
		exp.BuildAndDisplay();
	}

	/*--------------------
	 * GFX
	 *--------------------*/

	public FLPanel GetPanelForExternalInclusion(){
		return GetSpecificPanel();
	}

	protected FLPanel GetSpecificPanel() {
		// layout management

		FLPanel buttonpanel = GetButtonPanel();

		FLPanel ConstructionPanel = new FLPanel();
		// ConstructionPanel.setLayout(new BorderLayout());


		FLBlockPanel modelChoiceContainer = new FLBlockPanel();
		FLButton modelsTitle = new FLButton("Models");
		modelChoiceContainer.defineAsWindowBlockPanel(FLColor.c_grey0, modelsTitle, 570, false);
		modelChoiceContainer.setDefaultWidth(155);
		modelChoiceContainer.AddGridBagButton(modelsTitle);
		modelChoiceContainer.AddGridBagY(mF_ModelChoice);
		mF_ModelChoice.setCellRenderer(new ModelListCellRenderer());
		mF_ModelChoice.setPreferredSize(new Dimension(130, 500));

		DescriptionHelper.installDescriptionComponent(m_description);
		FLBlockPanel descriptionContainer = new FLBlockPanel();
		m_descriptionTitle = new FLButton("Description");
		descriptionContainer.defineAsWindowBlockPanel(FLColor.c_lightyellow, m_descriptionTitle, 170, false);
		descriptionContainer.AddGridBagButton(m_descriptionTitle);
		descriptionContainer.AddGridBagY(m_description);
		descriptionContainer.setDefaultWidth(715);
		m_descriptionTitle.getMargin().set(0, 0, 0, 0);
		m_description.getMargin().set(0, 0, 0, 0);
		m_description.setPreferredSize(new Dimension(710, 150));

		// ConstructionPanel.add(mF_TopoChoiceInteraction);
		// ConstructionPanel.add(mF_TopoChoicePerception);
		JComponent control = m_ReflectingBorder.GetControl();
		control.setOpaque(false);
		m_SizeParManager.AddElement(control);

		ConstructionPanel.Add(modelChoiceContainer, FLPanel.NewPanelVertical(descriptionContainer, m_SizeParManager));

		FLPanel mainpanel = new FLPanel();
		mainpanel.SetBoxLayoutY();
		mainpanel.add(ConstructionPanel);
		mainpanel.add(buttonpanel);
		return mainpanel;
	}

	/*--------------------
	 * Get & Set 
	 *--------------------*/
	public void SetDimInitalValue(IntC XY){
		m_SizeParManager.SetXY(XY);
	}

	/*--------------------
	 * GFX
	 *--------------------*/
	class ModelChoiceControl extends FLList {
		public ModelChoiceControl() {
			super(ModelChoice.values(),FiatLux.LGCA_DEFAULT_MODEL);
		}

		public ModelChoice GetSelectedModel() {
			int rank = GetSelectedItemRank();
			return ModelChoice.values()[rank];		
		}
	}

	/*class TopoChoiceControl extends FLList{
		public TopoChoiceControl() {
			super(TopoChoice.values(),FiatLux.LGCA_DEFAULT_TOPO);
		}

		public TopoChoice GetSelectedTopo() {
			int rank = GetSelectedItemRank();
			return TopoChoice.values()[rank];		
		}
	}*/
}


// "T" stands for torical BC
//TopoChoice topoChoiceInteraction= mF_TopoChoiceInteraction.GetSelectedTopo();
//String topoInteraction = "T" + ChoiceToCode(topoChoiceInteraction);

//TopoChoice topoChoicePerception = mF_TopoChoicePerception.GetSelectedTopo();
//String topoPerception = "T" + ChoiceToCode(topoChoicePerception);

//Macro.Debug(" Percep " + topoPerception + " Interact " + topoInteraction);

/* OLD OB if (model instanceof LGCAmodel) {
		LaunchLGCAExperiment(model, topoInteraction);
	} else if (model instanceof LGmodel) {
		LaunchLGExperiment((LGmodel) model, topoInteraction, topoPerception);
	} */