package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.TimeZone;

import javax.swing.JComponent;
import javax.swing.JOptionPane;

import components.types.FLString;



/*--------------------
 * The class Macro contains some macros
 * 
 * @author Nazim Fates
 *--------------------*/
public class Macro {

	// we use EPSILON in rounding problems when dividing
	public final static double EPSILON = 1E-10;

	public final static long start = System.currentTimeMillis();

	// for printing messages when signals are received (tracing of signals)
	public static final boolean TRACESIGNAL = false;

	// these are tags added to the message in print
	// prefix before showing messages at different importance levels	
	private final static String[] m_INFO = { 
			"*0>", "*1>", "*2:", "3] ", "4] ",
			"5) ", "6) ", "7) ", "8) ", "9) " };

	public static final int NOTFOUND = -1111;
	// formats
	public final static String PGM= ".pgm";


	/*-------------------------------
	 * 	SYSTEM
	 *--------------------------*/

	static public void systemExit() {
		systemExit(0);
	}

	static public void systemExit(int status) {
		Macro.print("*** Bye *** (status: "+ status +")");
		System.exit(status);
	}

	public static void systemExit(String message) {
		Macro.print("*** EXIT SYSTEM : "+ message );
		systemExit(-1);
	}
	
	/** [ Warning with the production of an Exception ] */
	static public void SystemWarning(String in_Message) {
		Exception e = new Exception();
		SystemWarning(e,in_Message);
	}

	static public void SystemWarning( Exception e, String in_Message) {
		e.printStackTrace();
		print("!!! Warning  !!!");
		Macro.print(in_Message);
	}

	/** [ Warning with the production of an Exception ] */
	static public void UserWarning(String in_Message) {
		Exception e = new Exception();
		UserWarning(e,in_Message);
	}

	static public void UserWarning( Exception e, String in_Message) {
		e.printStackTrace();
		print("!!! Warning  !!!");
		Macro.print(in_Message);
		UserMessage(in_Message);
	}


	/*
	 * ******************** MEMORY ************** */

	private static final long MEGABYTE = 1024L * 1024L;
	private static long bytesToMegabytes(long bytes) {return bytes / MEGABYTE;}
	public static String MemoryCheck () {
		Runtime runtime = Runtime.getRuntime();
		runtime.gc();
		long memory = runtime.totalMemory() - runtime.freeMemory();
		return "[" + bytesToMegabytes(memory) +"]";
	}

	/*
	 * ******************** Debug ************** */

	/** debug message with carriage return (use only temporarily) **/
	final static public void Debug(String in_Message) {
		Macro.print(" <<< dbg: " + in_Message);
	}

	/** debug message without carriage return (use only temporarily) **/
	final static public void DebugNOCR(String in_Message) {
		Macro.printNOCR("::" + in_Message);
	}


	/** debug with format **/
	public static void fDebug(String sFormat, Object ... args) {
		Macro.Debug(String.format(sFormat, args));		
	}

	/*
	 * ******************** user messages ************** */

	/** message in a pop-up window */
	public static void UserMessage(String msg) {
		JOptionPane.showMessageDialog(null, msg);
	}



	/** message in a pop-up window */
	public static void UserMessage(JComponent parentComponent,String msg) {
		JOptionPane.showMessageDialog(parentComponent, msg);
	}


	/*
	 * ******************** print Info ************** */

	/** console output */
	final static public void print(String msg) {
		//
		if (msg.contains(":::")) {
			Exception e = new Exception();
			Macro.SystemWarning(e, "good happens...");
		}
		System.out.println(msg);
	}	

	/** console output */
	final static public void printNOCR(String msg) {
		System.out.print(msg);
	}


	/** console output */
	final static public void print(int in_VerboseLevel, String msg) {
		if (in_VerboseLevel <= FiatLuxProperties.GetVerbose()) {
			print(m_INFO[in_VerboseLevel] + " " + msg);
		}
	}

	public static void print(int in_VerboseLevel, Object callingObject, String msg) {
		String className= callingObject.getClass().getSimpleName();
		print(in_VerboseLevel, className + "|" + msg);		
	}


	/** for using formatted output **/
	public static void fPrint(String sFormat, Object ... args) {
		Macro.print(String.format(sFormat, args));		
	}

	
	/** for using formatted output **/
	public static void fPrintNOCR(String sFormat, Object ... args) {
		Macro.printNOCR(String.format(sFormat, args));		
	}

	/** for using formatted output prints only if boolean arg is true **/
	public static void fPrint(boolean prnMsg, String sFormat, Object ... args) {
		if (prnMsg)
			Macro.print(String.format(sFormat, args));		
	}

	/** for using format **/
	public static void fPrintV(int verboseLevel, String sFormat, Object ... args) {
		Macro.print(verboseLevel, String.format(sFormat, args));		
	}

	/*
	 * ******************** specials (print info) ************** */

	public static void PrintInitSignal(int verboseLevel, Object callingobject) {
		print(verboseLevel, callingobject, "<init signal>");
	}

	final static public void printDate(int verboseLevel, String msg, boolean showDate) {
		String date ="";

		if (showDate) {
			long time = (System.currentTimeMillis()-start)/1000;
			long h= time/3600;
			long m= (time%3600)/60;
			long s= (time%3600)%60;
			date = "["+h+":"+m+":"+s+"] ";
		}

		print(verboseLevel, date + msg);
	}

	/** skips n lines */
	public static final void SkipLines(int nLines){ 
		for (int i=0; i < nLines; i++){
			Macro.print(".");
		}
	}

	/** skips line */
	final static public void CR() {
		System.out.println();
	}


	/** prints: ---------- */
	public static void SeparationLines(int numlines) {
		for (int i=0; i<numlines;i++) {
			Macro.print("---------------------");
		}
	}

	/** prints: ---------- */
	public static void SeparationLine() {
		SeparationLines(1);
	}

	/** uses separation lines before & after **/
	public static void PrintEmphasis(String msg) {
		SeparationLine();
		print(" **** " + msg+ " **** ");
		SeparationLine();
	}


	/** for "Usage" information printing */
	final static public void printUsage(String in_Classname, String in_Parameters) {
		print("Usage: java Commands." + in_Classname+ " "  + in_Parameters);
	}

	static java.util.Date t0, t1;

	static public void BeginTestC(Object caller){
		BeginTest(caller.getClass().getSimpleName());
	}


	static public void BeginTest(String in_className) {
		CR();
		print("----------------- Testing : " + in_className
				+ " ----------------");
		t0 = Macro.GetTime();				
	}


	static public void EndTest() {
		print("----------------- End Testing  ----------------");
		t1 = Macro.GetTime();
		Macro.print("Test done in " + FLString.TimeFormat(t1, t0));
		CR();
	}

	static public void Done() {
		print("Done !");
	}

	/*-------------------------------
	 * 	 *  FATAL ERROR
	 *--------------------------*/

	/** stops the system */
	static public void FatalError(Exception e, String in_Message) {
		e.printStackTrace();
		Macro.print("!!! Fatal Error  !!!");
		Macro.print(in_Message);
		systemExit(-1);
	}

	/** [ Error with the production of an Exception ] */
	static public void FatalError(String in_Message) {
		Exception e = new Exception();
		FatalError(e, in_Message);
	}

	/** [ Error with the production of an Exception ] */
	static public void FatalError(Object callingObject, String in_MethodName,
			String in_Message) {
		String className= callingObject.getClass().getSimpleName();
		FatalError(className + "." + in_MethodName + ">>" + in_Message);
	}

	/** [ Error with the production of an Exception ] */
	static public void FatalError(Object callingObject, String in_Message) {
		String className= callingObject.getClass().getSimpleName();
		FatalError(className + ">>" + in_Message);
	}


	/*--------------------
	 * I/O methods
	 --------------------*/

	/** should be valid for files and dirs */
	final public static boolean FileExists(String filename) {
		return (new java.io.File(filename).exists());
	}

	public static String GetCurrentDir() {
		File dir = new File (".");
		String d="";
		try {
			d = dir.getCanonicalPath();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return d;
	}


	/*--------------------
	 * I/O methods
	 --------------------*/

	public static String GetFileName(String modelname, int time, String Suffix){
		String stime = FLString.FormatIntWithZeros(time, 5);
		String fileName= modelname ;
		if ( (Suffix!=null) && !(Suffix.isEmpty()) ){
			fileName += "-" + Suffix; 
		}
		fileName += "-t" + stime;

		return fileName;
	}


	/*
	 * TIME
	 --------------------*/

	/** returns current time in ms * */
	final static public java.util.Date GetTime() {
		java.util.Date date = new java.util.Date();
		return date;
	}


	public static String GetDateTime() {

		/*
		 * from http://www.rgagnon.com/javadetails/java-0106.html * on some JDK,
		 * the default TimeZone is wrong * we must set the TimeZone manually!!! *
		 * Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("EST"));
		 */
		Calendar cal = Calendar.getInstance(TimeZone.getDefault());

		String DATE_FORMAT = "dd-MM-yyyy HH:mm:ss";
		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(
				DATE_FORMAT);
		/*
		 * * on some JDK, the default TimeZone is wrong * we must set the
		 * TimeZone manually!!! * sdf.setTimeZone(TimeZone.getTimeZone("EST"));
		 */
		sdf.setTimeZone(TimeZone.getDefault());

		return sdf.format(cal.getTime());
	}

	/*
	 ***************************************************************************
	 * other system commands
	 --------------------*/


	public static class CurrentClassGetter extends SecurityManager {
		public String getClassName() {
			return getClassContext()[1].getName();
		}
	}


	/** a small graphical animation to show process of experiments */
	public static void ProgressBar(int sampleI, int Z) {
		if (10 * sampleI % Z == 0){
			Macro.printNOCR("X");
		} 
		if (sampleI==Z-1){
			Macro.CR();
		}
	}

	/** indication about percentage done*/
	public static void HistogramProgress(int i, int histsize) {
		String percent = String.format(" progress: %.1f %% ", (double) i / histsize * 100);
		Macro.print(percent);
	}


	/**  runs a command on the system 
	 * USE with care !**/
	public static void ExecuteSystemCommand(String command){
		try{
			String s=null;
			print(4,"executing command:" + command);
			Process p = Runtime.getRuntime().exec(command);

			BufferedReader stdInput = new BufferedReader(new 
					InputStreamReader(p.getInputStream()));

			BufferedReader stdError = new BufferedReader(new 
					InputStreamReader(p.getErrorStream()));

			// read the output from the command
			print(4,"output of the command:");
			while ((s = stdInput.readLine()) != null) {
				print(s);
			}

			// read any errors from the attempted command

			while ((s = stdError.readLine()) != null) {
				print(4, "error generated by the command:" + s);
			}

		} catch (IOException e){
			Macro.SystemWarning(e, "problem in the system execution of:" + command);
		}
	}

	final static long MEGA= 1024 * 1024;

	// for the procedures which demand an int...
	public static final int ERRORint = -99999999;
	// for the procedures which demand a double...
	public static final double ERRORdouble = -99999999.999;
	// for the procedures which demand a char...
	public static final char ERRORchar = 255;
	// for the procedures which demand a char...
	public static final boolean ERRORboolean = false;

	private static final String FILENAMECONFIG = "config.set";


	public static String GetMemoryState() {
		return 
				String.format(" Mem : %d / %d (Free / Total)", Runtime.getRuntime().freeMemory() / MEGA, Runtime.getRuntime().totalMemory() / MEGA ) ;
	}

	/** used to wait indefinitely **/
	public static void Break() {
		try { // curious ???
			Object l= new Object();
			l.wait();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}		
	}

	public static String GetClassName(Object object) {
		return object.getClass().getSimpleName();
	}

	/** PRESS ENTER */
	public static void waitEnterKeyPressed() {
		try {
			System.in.read();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void waitEnterKeyPressed(String string) {
		Macro.print("["+string+"]"+ "press ENTER...");
		waitEnterKeyPressed();
	}

	/** reads in config file 
	 * value of field is followed by '=' **/
	public static double readConfigDoubleField(String strDpar) {
		String input= FLString.ReadFileInOneLine(FILENAMECONFIG);
		double dpar= FLString.DParseInString(input,strDpar+"=");
		return dpar;
	}

	/** reads in config file 
	 * value of field is followed by '=' **/
	public static int readConfigIntField(String strIpar) {
		String input= FLString.ReadFileInOneLine(FILENAMECONFIG);
		int ipar= FLString.IParseInString(input,strIpar+"=");
		return ipar;
	}

}

