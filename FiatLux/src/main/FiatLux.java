package main;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import components.types.FLStringList;
import grafix.windows.SimulationWindowCreator;


/******************************************
 * Main class 
 * @author Nazim Fates
 * Date of creation: Nov. 2001
 *******************************************/
public class FiatLux {
	
	public final static String FIATLUX_VERSION = "FiatLux SimulatorNbTest - v7.1 - 2023, Jan.";

	// for controlling messages
	public final static FiatLuxProperties config = FiatLuxProperties.getInstance();

	public static void main (String[] args){
		//Macro.Debug("huh");
		try {
			Macro.print("*** Welcome to " + FiatLux.FIATLUX_VERSION + " ***");
			String s= 
				"(c) Nazim Fates 2001-2022, [Nazim.Fates@loria.fr]," +
				"\n INRIA Nancy Grand Est - LORIA (since 2006)" + 
				"\n University Nancy 1 - LORIA (2005-2006)" +
				"\n ENS Lyon - LIP (2002-2005)" +
				"\n University Paris 7 - LIAFA (2001)" +
				"\n major contributions by Nikolaos Vlassopoulos, Olivier Boure and Nicolas Gauville" +
				"\n This simulator is a free software distributed under the Cecill License (see the \"About\" button)";
			
			String littre= 
					"\"Fiat lux\", que la lumiere soit faite, expression biblique\n" +
					" et latine que l'on emploie quelquefois pour demander qu'on\n" +
					" s'explique, qu'on donne plus de clarte au langage, a la discussion. (Littre)";

			Macro.print(s);
			Macro.print(littre);
			final String dir = System.getProperty("user.dir");
			Macro.print("date:" +Macro.GetDateTime() + " dir:" + dir);
			if (args.length == 0){
				RunSimulator();
			} else {
				Macro.FatalError("FiatLux takes no agrument. (run main.FiatLux)");
			}
			
		}
		catch(Exception e){
			Macro.print(" Exception caught at the top: ");
			e.printStackTrace();
		}		
	}
	
	

	public static void RunSimulator() {
		/**
		 * Force Swing to use the default Look&Feel
		 * (to enhance the UI on macOS platforms)
		 */
		String lookAndFeelClassName = UIManager.getCrossPlatformLookAndFeelClassName();
		if (FiatLuxProperties.LOOK_AND_FEEL == 1) {
			lookAndFeelClassName = "javax.swing.plaf.metal.MetalLookAndFeel";
		} else if (FiatLuxProperties.LOOK_AND_FEEL == 2) {
			lookAndFeelClassName = "com.sun.java.swing.plaf.motif.MotifLookAndFeel";
		} else if (FiatLuxProperties.LOOK_AND_FEEL == 3) {
			lookAndFeelClassName = "com.sun.java.swing.plaf.gtk.GTKLookAndFeel";
		}

		try {
			UIManager.setLookAndFeel(lookAndFeelClassName);
		} catch (UnsupportedLookAndFeelException e) {
			System.err.println("UnsupportedLookAndFeelException " + e.getMessage());
		} catch (ClassNotFoundException e) {
			System.err.println("ClassNotFoundException " + e.getMessage());
		} catch (InstantiationException e) {
			System.err.println("InstantiationException " + e.getMessage());
		} catch (IllegalAccessException e) {
			System.err.println("IllegalAccessException " + e.getMessage());
		}
		SimulationWindowCreator myWindow = new SimulationWindowCreator();
		SwingUtilities.updateComponentTreeUI(myWindow);
		myWindow.packAndShow();
	}
	
	/**
	 * Loads the properties
	 * @param config the Properties Object
	 */

	
	/*--------------------
	 * default selections
	--------------------*/
	
	public static final int INI_SMA_SELECT_MODEL = 0;

	public static boolean withScrollPane = true; // to scroll or not to scroll ?
		
	//LGCA defaults
	public static final int LGCA_DEFAULT_MODEL = 0;
	public static final int LGCA_DEFAULT_TOPO = 0;

	public static final int DEFAULT_FORMAT_SELECT_INDEX = 1; // preselection of the record format 

	/***/
	
	static String PRESETFILENAME="yreset.fl";
	
	static void runPresetConfiguration() {
		FLStringList read= FLStringList.OpenFile(PRESETFILENAME);
	}
	
	
}

