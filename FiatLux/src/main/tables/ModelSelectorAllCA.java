package main.tables;

import java.util.Arrays;
import java.util.List;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import components.types.FLStringList;
import grafix.gfxTypes.elements.FLPanel;
import grafix.gfxTypes.elements.FLTabbedPane;
import main.FiatLuxProperties;
import main.AAAMAIN_MODEL_TABLE;
import main.Macro;
import main.AAAMAIN_MODEL_TABLE.MT;
import main.designPanels.CAdesignerPanel;
import models.CAmodels.CellularModel;

/** TABLE OF MODELS
 */
public class ModelSelectorAllCA extends FLPanel implements ChangeListener {// monitoring tab selection
	final static String CLASSNAME = Thread.currentThread().getStackTrace()[1].getClassName();

	private static final String ERRORCODE = "ERRORCODEMODEL";
	// This determines the display order of Models Tabs
	public enum CATYPE_list {GENERAL, LINEAR, PARTICLE, STOCHASTIC, ASYNCHINFO, VARIANTS, DIAGNOSTIC, TESTS}

    /*--------------------
     * attributes
     --------------------*/
	FLTabbedPane m_modelCollectionPane = new FLTabbedPane();
	CAdesignerPanel m_window;

	ModelSelectorOneCAfamily 
	m_SelectorGeneral, m_SelectorLinear, m_SelectorStochastic,
	m_SelectorParticle, m_SelectorVariants, m_SelectorTests, m_SelectorDiagnostic;


	/*--------------------
	 * construction
	 --------------------*/
	/* call back involved */
	public ModelSelectorAllCA(CAdesignerPanel in_window){

		//INITIATES THE MODEL_TABLE
		AAAMAIN_MODEL_TABLE.FillTable();

		m_window= in_window;

		m_SelectorGeneral = CreateTab(MT.GENERAL, FiatLuxProperties.GeneralCA_TAB_DEF_SELECTION);
		m_SelectorLinear = CreateTab(MT.LINEAR, FiatLuxProperties.Linear_TAB_DEF_SELECTION);
		m_SelectorParticle = CreateTab(MT.PARTICLE,  FiatLuxProperties.Particle_TAB_DEF_SELECTION);
		m_SelectorVariants = CreateTab(MT.VARIANTS,  FiatLuxProperties.Variants_TAB_DEF_SELECTION);
		m_SelectorStochastic = CreateTab(MT.STOCHASTIC, FiatLuxProperties.Stochastic_TAB_DEF_SELECTION);
		m_SelectorTests = CreateTab(MT.TESTS, FiatLuxProperties.Tests_TAB_DEF_SELECTION);
		m_SelectorDiagnostic = CreateTab(MT.DIAGNOSTIC, FiatLuxProperties.Diagnostic_TAB_DEF_SELECTION);

		m_modelCollectionPane = new FLTabbedPane();

		m_modelCollectionPane.AddTab("general", m_SelectorGeneral);
		m_modelCollectionPane.AddTab("linear", m_SelectorLinear);
		m_modelCollectionPane.AddTab("particles", m_SelectorParticle);
		m_modelCollectionPane.AddTab("stochastic", m_SelectorStochastic);
		m_modelCollectionPane.AddTab("variants", m_SelectorVariants);
		m_modelCollectionPane.AddTab("diagnostic", m_SelectorDiagnostic);
		m_modelCollectionPane.AddTab("tests", m_SelectorTests);

		// Auto save
		m_modelCollectionPane.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				FiatLuxProperties.getInstance().SetDefCA(AssociateIntToEnumKey(m_modelCollectionPane.getSelectedIndex()));
			}
		});

		// initial setting
		CATYPE_list initSelect = FiatLuxProperties.GetDefCA();
		ModelSelectorOneCAfamily component = AssociateComponentToEnumKey(initSelect);
		m_modelCollectionPane.setSelectedComponent(component); //TODO

		// this operation DOES NOT commute with the previous one
		m_modelCollectionPane.addChangeListener(this);
		FLPanel p= new FLPanel();
		p.Add(m_modelCollectionPane);
		this.Add(p);
	}

	private ModelSelectorOneCAfamily CreateTab(MT type, int defSelection) {
		try{
			FLStringList list = AAAMAIN_MODEL_TABLE.FindModelsOfType(type);
			ModelSelectorOneCAfamily m = new ModelSelectorOneCAfamily(this, list.ToArray(), defSelection, type);
			return m;
		} catch (Exception e){
			Macro.SystemWarning(e, 
					"check default index : failed to init list for type : "+ 
							type + " index:" + defSelection);
			return null;
		}		
	}

	private ModelSelectorOneCAfamily AssociateComponentToEnumKey(CATYPE_list initSelect) {
		switch (initSelect){
		case GENERAL:
			return m_SelectorGeneral;
		case LINEAR:
			return m_SelectorLinear;
		case PARTICLE:
			return m_SelectorParticle;
		case STOCHASTIC:
			return m_SelectorStochastic;
		case VARIANTS:
			return m_SelectorVariants;
		case TESTS:
			return m_SelectorTests;
		case DIAGNOSTIC:
			return m_SelectorDiagnostic;
		default:
			Macro.SystemWarning("Forgotten case :" + initSelect);
			return m_SelectorGeneral;
		}
	}

	private CATYPE_list AssociateIntToEnumKey(int initSelect) {
		switch (initSelect){
			case 0:
				return CATYPE_list.GENERAL;
			case 1:
				return CATYPE_list.LINEAR;
			case 2:
				return CATYPE_list.PARTICLE;
			case 3:
				return CATYPE_list.STOCHASTIC;
			case 4:
				return CATYPE_list.VARIANTS;
			case 5:
				return CATYPE_list.DIAGNOSTIC;
			case 6:
				return CATYPE_list.TESTS;
			default:
				Macro.SystemWarning("Forgotten case :" + initSelect);
				return null;
		}
	}

	/*--------------------
	 * actions
	 --------------------*/
	@Override
	/** allows to update the selection when new tab is selected **/
	public void stateChanged(ChangeEvent e) {
		//Macro.SystemWarning(" stateChange");
		/*Object source= m_modelCollectionPane.getSelectedComponent();
		m_currentSelection= (ModelSelectorOneCAfamily)source;*/	
		/*if ( source instanceof ModelSelectorOneCAfamily){

			//Macro.Debug(" good source:" + source);
		} else {
			//Macro.Debug(" unknown tab change source:" + source.getClass().getCanonicalName());
		}*/

		sig_Update();	
	}

	public void sig_Update() {
		m_window.sig_Update();
	}

	/*--------------------
	 * get & set
	 --------------------*/


	public CellularModel GetCurrentlySelectedModel() {
		Object source= m_modelCollectionPane.getSelectedComponent();
		ModelSelectorOneCAfamily currentSelect= (ModelSelectorOneCAfamily)source;
		String modelKey= currentSelect.GetKeySelectedModel();
		Macro.print(5,"selected-model-key:" + modelKey );
        CellularModel model = NameToModel(modelKey);
		return model;
	}

	public String GetCurrentlySelectedModelAssociatedTopology() {
		CellularModel model = GetCurrentlySelectedModel();
		if (model==null){
			Macro.SystemWarning("I could not find selected model");
			return ERRORCODE;
		} else {
			String code= model.GetDefaultAssociatedTopology();
			//Macro.Debug("model: "+ model.GetName() +" associated with topology :" + code);
			return code;
		}
	}
	//OC
	public List<String> GetCurrentlySelectedModelDisabledTopology(){
		CellularModel model = GetCurrentlySelectedModel();
		if (model==null){
			Macro.SystemWarning("I could not find selected model");
			return Arrays.asList(ERRORCODE);
		} else {
			List<String> code= model.GetDisabledTopology();
			//Macro.Debug("model: "+ model.GetName() +" associated with topology :" + code);
			return code;
		}
	}
	// \OC
	/*--------------------
	 * test
	 --------------------*/
	public static void DoTest(){
		Macro.BeginTest(CLASSNAME);
		Macro.EndTest();
	}

	/***********************************
	 * CODE TO MODEL 
	 */

	/* linked to DATA */
	static public CellularModel NameToModel(String modelName) {
		if (modelName==null){
			Macro.SystemWarning("null model selected");
		}
		CellularModel model = AAAMAIN_MODEL_TABLE.FindFromKey(modelName);
		return model;
	}
}
