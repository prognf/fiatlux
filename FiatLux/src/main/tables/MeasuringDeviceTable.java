package main.tables;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedHashMap;

import architecture.dtasep.DTasepAlignmentMD;
import architecture.dtasep.DynamicCellsDensityMD;
import architecture.dtasep.ParticleBalanceMD;
import architecture.latticeKinesis.measuringD.LatticeKalignmentMD;
import architecture.latticeKinesis.measuringD.LkinesisConcentrationMD;
import architecture.latticeKinesis.measuringD.LkinesisCountParticulesMD;
import architecture.latticeKinesis.measuringD.LkinesisTransportMD;
import components.types.FLStringList;
import experiment.measuring.MeasuringDevice;
import experiment.measuring.amoebae.EmissionProbabilityMD;
import experiment.measuring.general.ActivityMD;
import experiment.measuring.general.DensityMD;
import experiment.measuring.general.MaxStateDensityMD;
import experiment.measuring.linear.BlocMD;
import experiment.measuring.linear.KinksMD;
import experiment.measuring.linear.ZeroOneMD;
import experiment.measuring.planar.BoundingBoxMD;
import experiment.measuring.planar.Correlation4MD;
import experiment.measuring.planar.DensityIpsMD;
import experiment.measuring.planar.IPS2D_CorrelationMD;
import experiment.measuring.planar.IPS2D_EnergyPairsMD;
import experiment.measuring.planar.PairEnergyMD;
import experiment.measuring.planar.SquarePatternsMD;
import experiment.measuring.specific.AverageNeighborhoodMD;
import experiment.measuring.specific.BlueDensityMD;
import experiment.measuring.specific.GreenDensityMD;
import experiment.measuring.specific.RedDensityMD;
import experiment.toolbox.CycleDetector;
import main.Macro;
import models.CAmodels.binary.ScribXYZmodel;


/*
 * "static class"
 */
public class MeasuringDeviceTable {


	enum TYPE { bin, migratio, dictyo, lgcaOB, ips, others, amoebae}

	/** class and type **/
	static class mdInfo {

		Class<? extends MeasuringDevice> m_classRef;
		TYPE m_classType;

		public mdInfo(Class<? extends MeasuringDevice> classRef, TYPE classType) {
			m_classRef= classRef;
			m_classType= classType;
		}
	}

	// associates a name (Key of type String) to a class model 
	private static LinkedHashMap<String, mdInfo> 
	m_map= new LinkedHashMap<String, mdInfo>();
	private static TYPE m_modelType;


	/** call before using the class **/
	static public void FillTable() {

		m_modelType= TYPE.bin;
		AddList(DensityMD.class); 
		AddList(ActivityMD.class);
		AddList(ZeroOneMD.class);
		AddList(KinksMD.class);
		AddList(BlocMD.class);
		AddList(Correlation4MD.class);
		AddList(SquarePatternsMD.class);
		AddList(CycleDetector.class);

		m_modelType= TYPE.migratio;
		AddList(LkinesisTransportMD.class);
		AddList(LatticeKalignmentMD.class);
		AddList(LkinesisCountParticulesMD.class);
		AddList(LkinesisConcentrationMD.class);

		m_modelType= TYPE.dictyo;
		AddList(BoundingBoxMD.class);

		m_modelType= TYPE.ips;
		AddList(IPS2D_CorrelationMD.class);
		AddList(IPS2D_EnergyPairsMD.class);
		AddList(PairEnergyMD.class);
		AddList(DensityIpsMD.class);
		
		m_modelType= TYPE.others;
		AddList(MaxStateDensityMD.class);
		AddList(ParticleBalanceMD.class);
		AddList(DTasepAlignmentMD.class);
		AddList(DynamicCellsDensityMD.class);
		AddList(RedDensityMD.class);
		AddList(BlueDensityMD.class);
		AddList(GreenDensityMD.class);
		AddList(ActivityMD.class);
		AddList(AverageNeighborhoodMD.class);

		m_modelType= TYPE.amoebae;
		AddList(EmissionProbabilityMD.class);
		
	}


	private static void AddList(Class<? extends MeasuringDevice> modelClass) {
		mdInfo localClassInfo= new mdInfo(modelClass, m_modelType);
		AssociateName(localClassInfo);
	}


	static public FLStringList FindModelsOfType(TYPE type){
		FLStringList list= new FLStringList();
		for (String modelName : m_map.keySet()){
			if (m_map.get(modelName).m_classType == type){
				list.Add( modelName );
			}
		}
		return list;
	}

	/**
	 * REFLEXIVITY here 
	 */
	static private void AssociateName(mdInfo item) {
		Class<? extends MeasuringDevice> oneClass= item.m_classRef;
		String className= oneClass.getCanonicalName();
		try {
			//Class.forName( className );
			try {
				Field f = oneClass.getField("NAME");
				String nameRead= (String) f.get(String.class);
				//Macro.Formatprint("class: %s key name:%s ",oneClass.getCanonicalName(), nameRead);
				m_map.put(nameRead, item);
			} catch (Exception e) {
				Macro.SystemWarning(e, 
						"Field NAME not found in class:" + oneClass.getCanonicalName());
			}
		} /*catch( ClassNotFoundException e ) {
				Macro.print("Class not found: " + className);
			} */catch (NoClassDefFoundError e){
				String s= "Path problem with class: " + className;
				if (oneClass.equals(ScribXYZmodel.class)){
					s+= "\n Check link to external libraires (janino)";
				}				
				Macro.SystemWarning(s);				
			}
	}

	/**
	 * 
	 */
	static public void PrintAllNames(){
		for (String item : m_map.keySet()){
			Macro.print(" base contains model : " + item);
		}
	}

	/**
	 * REFLEXIVITY here : construction of the object from the name
	 */

	@SuppressWarnings("unchecked")
	public static <Z extends MeasuringDevice> MeasuringDevice FindFromKey(String name) {
		Class<Z> foundModel=null;
		try{			
			foundModel= (Class<Z>) m_map.get(name).m_classRef;
		} catch (Exception e){
			Macro.SystemWarning(e, "Association failed with : " + name );
		}
		Constructor<?> ctor;
		Object object=null;
		MeasuringDevice target=null;
		try {
			ctor = foundModel.getConstructor();
			object = ctor.newInstance();
			target= (MeasuringDevice) object;
		} catch (NoSuchMethodException e) {
			Macro.SystemWarning("class " + foundModel.getCanonicalName() 
					+ " could not find empty constructor");
			e.printStackTrace();
		} catch (IllegalArgumentException | InvocationTargetException | IllegalAccessException | InstantiationException e) {
			e.printStackTrace();
		}

        //Macro.Debug(model.GetName());
		return target;

	}

	static public void main(String [] argv ){
		FillTable();
		PrintAllNames();
	}


}


