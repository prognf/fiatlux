package main.tables;

import java.awt.Color;
import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.JSeparator;

import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.IconManager;

public class ModelListCellRenderer extends DefaultListCellRenderer {
    private JSeparator SEPARATOR = new JSeparator(JSeparator.HORIZONTAL);

    public ModelListCellRenderer() {
        super();
        SEPARATOR.setBorder(BorderFactory.createLineBorder(Color.gray, 2));
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        if (!(value instanceof String) || value.equals("") || ((String) value).contains("separator")) {
            return SEPARATOR;
        }

        setText(value.toString());

        setBackground(isSelected ? FLColor.c_lightgrey : FLColor.WHITE);
        setBorder(BorderFactory.createEmptyBorder(5, 3, 5, 3));
        setIcon(IconManager.getCAIcon(value.toString()));

        return this;
    }
}