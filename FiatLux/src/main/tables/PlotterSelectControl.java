package main.tables;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import architecture.latticeKinesis.measuringD.LatticeKalignmentMD;
import architecture.latticeKinesis.measuringD.LkinesisConcentrationMD;
import architecture.latticeKinesis.measuringD.LkinesisCountParticulesMD;
import architecture.latticeKinesis.measuringD.LkinesisTransportMD;
import components.types.Monitor;
import experiment.measuring.MeasuringDevice;
import experiment.measuring.amoebae.EmissionProbabilityMD;
import experiment.measuring.general.ActivityMD;
import experiment.measuring.general.DecimalParameterMeasurer;
import experiment.measuring.general.DensityMD;
import experiment.measuring.general.MaxStateDensityMD;
import experiment.measuring.linear.BlocMD;
import experiment.measuring.linear.KinksMD;
import experiment.measuring.linear.ZeroOneMD;
import experiment.measuring.planar.BoundingBoxMD;
import experiment.measuring.planar.Correlation4MD;
import experiment.measuring.planar.LgcaCoverageMD;
import experiment.measuring.planar.LgcaNumParticleMD;
import experiment.measuring.planar.oldEnergy2MD_amoebae;
import experiment.measuring.specific.AverageNeighborhoodMD;
import experiment.measuring.specific.BlueDensityMD;
import experiment.measuring.specific.GreenDensityMD;
import experiment.measuring.specific.NonDefectiveActivityMD;
import experiment.measuring.specific.RedDensityMD;
import experiment.toolbox.CycleDetector;
import grafix.gfxTypes.elements.FLList;
import grafix.gfxTypes.elements.FLPanel;
import grafix.plotters.TimePlotter;
import main.Macro;



public class PlotterSelectControl extends FLPanel 
implements ListSelectionListener
{

	final static String SELECTPLOT= "plot";

	/*-------------------
	 * Attributes
	-------------------------*/

	FLList mF_list;

	// last selected, used for instant measure
	MeasuringDevice m_lastSelectedMD= null;

	/*-------------------
	 * Constructor
	-------------------------*/

	/** works with an association key -> objects **/
	public PlotterSelectControl(String [] selected_plotters){
		mF_list = new FLList(selected_plotters, this);
		// this.AssociateLabelAndList(SELECTPLOT, mF_list);
		add(mF_list);
		int [] NOSELECT= {};
		mF_list.setSelectedIndices(NOSELECT); // NO SELECTION BY DEFAULT
		MeasuringDeviceTable.FillTable();
	}


	/*-------------------
	 * info of preselections
	-------------------------*/

	public final static String[] defaultSelect = { 
		DensityMD.NAME,
	};

	public final static String [] ECAselect = {   
		DensityMD.NAME,
		ZeroOneMD.NAME,
		ActivityMD.NAME,
		KinksMD.NAME,
		BlocMD.NAME,
		CycleDetector.NAME,
	};

	public final static String [] BinarySelect1D= { 
		DensityMD.NAME,
		ActivityMD.NAME,
		KinksMD.NAME,
		BlocMD.NAME,
		CycleDetector.NAME,
	}; 

	public final static String [] BinarySelect2D= { 
		DensityMD.NAME,
		ActivityMD.NAME,
		Correlation4MD.NAME,
		CycleDetector.NAME,
	}; 


	public static final String[] LGCAselect={
		LgcaNumParticleMD.NAME,
		LgcaCoverageMD.NAME,
	};

	public static final String[] REACDIFFSELECT = {   
		MaxStateDensityMD.NAME,
		DensityMD.NAME,
	};

	
	public static final String[] DICTYOSELECT = { 	
		BoundingBoxMD.NAME,
		//		ClusterMD.NAME8,
		//		ClusterMD.NAME4,
		oldEnergy2MD_amoebae.NAME,
		//		ClusterWavefrontMD.NAME8,
		//		PotentialEnergyMD.NAME
	};

	public static final String[] MIGRATION_SELECT = {
		LatticeKalignmentMD.NAME,
		LkinesisTransportMD.NAME,
		LkinesisCountParticulesMD.NAME,
		LkinesisConcentrationMD.NAME
	};

	public static final String[] INFOTAXIS_SELECT= {
		EmissionProbabilityMD.NAME
	};

	public static final String[] RGB_SELECT = {
		RedDensityMD.NAME,
		GreenDensityMD.NAME,
		BlueDensityMD.NAME,
		ActivityMD.NAME,
		NonDefectiveActivityMD.NAME,
		AverageNeighborhoodMD.NAME
	};

	/*-------------------
	 * Get & Set methods
	-------------------------*/

	/** main **/
	public Monitor OpenSelectedPlotter() {
		Macro.print("Last selected MD : " + m_lastSelectedMD.GetName());
		if (m_lastSelectedMD instanceof DecimalParameterMeasurer){
			DecimalParameterMeasurer dpm= (DecimalParameterMeasurer) m_lastSelectedMD; 
			return (dpm!=null)?new TimePlotter(dpm):null;
		} else {
			Macro.SystemWarning("could not link with decimal parameter ");
			return null;
		}
	}

	public MeasuringDevice GetSelectedMD() {
		return m_lastSelectedMD;
	}


	/** for instant measure **/
	public MeasuringDevice GetSelectedDPMforInstantMeasure(){
		return m_lastSelectedMD;
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		String selected= mF_list.GetSelectedItemName();
		if (	(e.getValueIsAdjusting() == false)
				&(selected != null)){
			Macro.print(5,"List selection:"+ mF_list.getSelectedIndex() + " key:" + selected );
			MeasuringDevice selectedMD= MeasuringDeviceTable.FindFromKey(selected);
			m_lastSelectedMD= selectedMD;
		}

	}


	/*-------------------
	 * Other methods
	-------------------------*/

}
