package main.tables;

import java.awt.BorderLayout;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import grafix.gfxTypes.FLColorListRender;
import grafix.gfxTypes.elements.FLList;
import grafix.gfxTypes.elements.FLPanel;
import main.FiatLuxProperties;
import main.AAAMAIN_MODEL_TABLE;

public class ModelSelectorOneCAfamily extends FLPanel implements ListSelectionListener {
	/*--------------------
	 * attributes
	 --------------------*/
	private AAAMAIN_MODEL_TABLE.MT m_type;
	private ModelSelectorAllCA m_callback; // for call back (sig_update)
	FLList m_list;

	/*--------------------
	 * construction
	 --------------------*/
	public ModelSelectorOneCAfamily(ModelSelectorAllCA in_callback, String [] in_labels, int defSelection, AAAMAIN_MODEL_TABLE.MT type) {
		m_callback = in_callback;
		m_type = type;
		m_list = new FLList(in_labels, defSelection);
		m_list.setCellRenderer(new ModelListCellRenderer());
		m_list.addListSelectionListener(this);
		// adding to the panel
		setLayout(new BorderLayout());
		add(m_list);
	}


	public void LimitColor(int lim1, int lim2){
		// cosmetic
		FLColorListRender colType = new FLColorListRender(lim1,lim2);
		colType.SetColorsBlueRedCyan();
		m_list.setCellRenderer(colType);

	}

	/*--------------------
	 * actions
	 --------------------*/
	public void valueChanged(ListSelectionEvent e) {
		// selection changed
		//Macro.Debug( "List selection:"+ m_list.getSelectedIndex() );
		// this is necessary not to call this method more than once
		if (!e.getValueIsAdjusting()) {
			//Macro.Debug("-----val chg;");
			m_callback.sig_Update(); // change notification
        }

        // save
		FiatLuxProperties.getInstance().setListSelection(m_type, m_list.getSelectedIndex());
	}

	/*--------------------
	 * get & set
	 --------------------*/

	/* objects depends on current selection in the list */
	public String GetKeySelectedModel(){
		String modelname = m_list.GetSelectedItemName();
		return modelname;
	}
}
