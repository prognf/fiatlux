package grafix.html;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Singleton to read all the models descriptions (in descriptions.txt)
 * @author Nicolas Gauville
 * TODO: move "descriptions.txt" inside src folder ?
 */
public class TextInterpreter {
    private static TextInterpreter instance = new TextInterpreter();
    public static TextInterpreter getInstance() { return instance; }

    private ArrayList<DescriptionHelper> list;
    private DescriptionHelper current;

    public TextInterpreter() {
        list = new ArrayList<>();

        String path="/models/descriptions.txt";
        InputStream instream = getClass().getResourceAsStream(path);
        try (Scanner scanner =  new Scanner(instream)) {
            while (scanner.hasNextLine()){
                processLine(scanner.nextLine());
            }
        }
    }

    /**
     * Get the model description
     * @param modelName
     * @return String
     */
    public String getModelDescription(String modelName) {
        for (DescriptionHelper dh : list) {
            if (dh.getName().equals(modelName)) {
                return dh.toString();
            }
        }

        return "No description available.";
    }

    protected void processLine(String line) {
        if (line != null && !line.trim().isEmpty()) {
            if (line.substring(0, 1).equals("#")) {
                DescriptionHelper ndh = new DescriptionHelper(line.substring(1));
                list.add(ndh);
                current = ndh;
            } else if (current != null) {
                try (Scanner scanner = new Scanner(line)) {
                    scanner.useDelimiter("=");
                    if (scanner.hasNext()) {
                        String name = scanner.next().trim();
                        String value = scanner.next().trim();
                        String secondValue = "";
                        if (scanner.hasNext()) {
                            secondValue = scanner.next().trim();
                        }

                        if (name.equals("description")) {
                            current.addDescription(value);
                        } else if (name.equals("author")) {
                            current.addAuthor(value);
                        } else if (name.equals("color")) {
                            current.addColor(value, secondValue);
                        } else if (name.equals("link")) {
                            current.addLink(value, secondValue);
                        }
                    }
                }
            }
        }
    }
}
