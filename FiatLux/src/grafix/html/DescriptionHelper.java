package grafix.html;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.swing.JTextPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;

/**
 * An helper class to adapt the models description style
 * Convert the descriptions.txt file format (see grafix.html.TextInterpreter) to HTML format
 * @author Nicolas Gauville
 */
public class DescriptionHelper {
    private static HTMLEditorKit kit;

    /**
     * Create the HTMLEditorKit with the needed css style
     * @return
     */
    public static HTMLEditorKit getKit() {
        if (kit == null) {
            kit = new HTMLEditorKit();
            StyleSheet styleSheet = kit.getStyleSheet();
            styleSheet.addRule("* {font-family: 'SansSerif'; font-size: 11px; margin: 0; line-height: 70%;}");
            styleSheet.addRule(".b {text-decoration: none; font-weight: bold;");
            styleSheet.addRule("ul {margin: 0; padding: 2px; padding-left: 10px;");
            styleSheet.addRule("strong {font-family: 'SansSerif'; font-size: 18px;;}");
            styleSheet.addRule("table {margin: 0; padding: 0;}");
            styleSheet.addRule("tr {margin-left: 0; padding-left: 0;}");
            styleSheet.addRule(".box {display: inline-block; width: 15px; height: 15px; border: 1px solid black; margin: 2px;}");
            styleSheet.addRule(".block {font-family: 'SansSerif'; font-size: 10px; display: block; width: 480px; height: 160px; border: 0; background: none; padding: 2px; padding-top: 0; margin; 0; text-align: left;}");
            styleSheet.addRule("a {color: #0099CC; border: 0; text-decoration: none; font-weight: bold;}");
            styleSheet.addRule("a:hover {color: #00AADD; border: 0; text-decoration: none; font-weight: bold;}");
        }

        return kit;
    }

    /**
     * Method used to install stylesheet and hyperlink listener
     * for the description JTextPane
     * @param textPane
     */
    public static void installDescriptionComponent(JTextPane textPane) {
        textPane.setContentType("text/html");
        textPane.setOpaque(false);
        textPane.setEditable(false);
        textPane.setEditorKit(getKit());

        textPane.addHyperlinkListener(new HyperlinkListener() {
            public void hyperlinkUpdate(HyperlinkEvent e) {
                if(e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                    if(Desktop.isDesktopSupported()) {
                        try {
                            Desktop.getDesktop().browse(e.getURL().toURI());
                        } catch (IOException | URISyntaxException e1) {
                            e1.printStackTrace();
                        }
                    }
                }
            }
        });
    }

    private String name = "unknown";
    private StringBuilder sb;
    private boolean colorList = false;
    private boolean colorListCompleted = false;

    public DescriptionHelper() {
        sb = new StringBuilder();
    }

    public DescriptionHelper(String name) {
        sb = new StringBuilder();
        this.name = name;
    }

    public String toString() {
        completeColorList();
        return sb.toString();
    }

    public String getName() {
        return name;
    }

    public void addDescription(String description) {
        sb.append("<p>").append(description).append("</p>");
    }

    public void addColor(String htmlColor, String name) {
        if (!colorList) {
            colorList = true;
            sb.append("<table><tr><td style='padding-left: 0;'><span class='b'>Colors :</span></td>");
        }

        sb.append("<td><div class='box' style='background:").append(htmlColor).append("; color:");
        sb.append(htmlColor).append(";'>&nbsp;</div></td><td>").append(name).append("</td> ");
    }

    public void addLink(String url) {
        addLink(url, url);
    }

    public void addLink(String url, String name) {
        completeColorList();
        sb.append("<span class='b'>See more :</span> <a href='").append(url).append("'>").append(name).append("</a>");
    }

    public void addAuthor(String name) {
        completeColorList();
        sb.append("<span class='b'>Author :</span> ").append(name);
    }

    private void completeColorList() {
        if (colorList && !colorListCompleted) {
            colorListCompleted = true;
            sb.append("</tr></table>");
        } else {
            sb.append("<br>");
        }
    }
}
