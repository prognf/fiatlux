package grafix.plotters;

import components.arrays.RegularDynArray;
import components.types.DoubleCouple;
import components.types.FLsignal;
import components.types.Monitor;
import experiment.measuring.MeasuringDevice;
import experiment.measuring.general.DecimalParameterMeasurer;
import experiment.samplers.SuperSampler;
import grafix.viewers.DataViewer;
import main.Macro;


/*--------------------
 * A DataPlotter is a type of View associated to an automaton
 * it knows how to do an extraction of data from this automaton
 * and knows how to reset itself
 *--------------------*/

public abstract class DataPlotter implements Monitor {

	final static int DEFAULT_T_RANGE = 1000;
	/*--------------------
	 * Attributes
	--------------------*/

	private static final DoubleCouple DEFRANGE = new DoubleCouple(0, 1);

	// every extractor is linked to an Automaton 
	protected RegularDynArray m_automaton; // data provider

	protected DecimalParameterMeasurer m_Xpar, m_Ypar; // measures for X and Y

	protected DataViewer m_Viewer; // 2D display device

	/*--------------------
	 * construction
	--------------------*/

	public DataPlotter(	DecimalParameterMeasurer MDx, 
			DecimalParameterMeasurer MDy){
		m_Xpar = MDx;
		m_Ypar = MDy;
		//Create Viewer
		//	Warning : the simple fact of creating the object makes it visible on the screen !
		m_Viewer= new DataViewer();
		
		SetRangeDefault(m_Xpar.GetMinMax(),m_Ypar.GetMinMax());
		// make title
		String title= m_Ypar.GetName() + " versus " + m_Xpar.GetName() + " " ;
		GetViewer().SetTitle(title);
	} 

	

	/*--------------------
	 * signals
	--------------------*/
	/** processing signals **/
	public void ReceiveSignal(FLsignal sig){
		switch(sig){
		case init:
			sig_Init();
			break;
		case update:
			sig_Update();
			break;
		case nextStep:
			sig_NextStep();
			break;
		}
	}

	/** reset signal */
	final public void sig_Init() {
		Macro.print(4, "Data Plotter : init" );
		GetViewer().Reset();
		sig_Update();
	}


	public void sig_NextStep(){
		sig_Update(); 
	}

    /** main method */
	public void sig_Update() {
		double xval= m_Xpar.GetMeasure(), yval= m_Ypar.GetMeasure();
		//Macro.Debug("Plotter update: " + xval + " ," + yval);
		GetViewer().AddPoint(xval, yval);
		GetViewer().repaint();
		GetViewer().AddPoint(xval, yval);
		GetViewer().repaint();
	}


	public void sig_Close() {
		m_Viewer.dispose();
	}



	protected void SetRangeDefault(DoubleCouple dc1, 
			DoubleCouple dc2){
		if (dc1==null){
			dc1= DEFRANGE;
		}
		if (dc2==null){
			dc2= DEFRANGE;
		}
		SetRange(dc1.X(), dc1.Y(), 
				dc2.X(), dc2.Y());
	}

	protected void SetRange(double xmin, double xmax, double ymin, double ymax){
		GetViewer().SetRange(xmin, xmax, ymin, ymax);
	}

	/* links an automaton to  the extractor  */
	public void LinkTo(SuperSampler supersampler) {
		try{
			// linking of the automaton implies the linking of the measuring device
			//			if (m_Xpar != null) // these controls are for EmptyDataPlotter
			((MeasuringDevice)m_Xpar).LinkTo(supersampler);
			//			if (m_Ypar != null) // these 
			((MeasuringDevice)m_Ypar).LinkTo(supersampler);
		} catch (Exception e){
			Macro.UserWarning(e, "A problem occured when linking the DataPlotter");
			m_Viewer.dispose();
		}
	}



	/*--------------------
	 * Get/Set methods
	--------------------*/

	final private DataViewer GetViewer() {
		return m_Viewer;
	}

	final private RegularDynArray GetAutomaton() {
		return m_automaton;
	}

	final protected int GetLsize() {
		return GetAutomaton().GetSize();
	}

	public DecimalParameterMeasurer GetXMeasureDevice() {
		return m_Xpar;
	}

	public DecimalParameterMeasurer GetYMeasureDevice() {
		return m_Ypar;
	}
}
