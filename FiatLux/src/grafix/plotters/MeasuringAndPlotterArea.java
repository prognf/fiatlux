package grafix.plotters;

import static java.awt.GridBagConstraints.NORTH;

import java.awt.GridBagConstraints;

import components.types.Monitor;
import experiment.measuring.MeasuringDevice;
import experiment.measuring.general.DecimalParameterMeasurer;
import experiment.samplers.SuperSampler;
import grafix.gfxTypes.IconManager;
import grafix.gfxTypes.elements.FLActionButton;
import grafix.gfxTypes.elements.FLBlockPanel;
import grafix.gfxTypes.elements.FLButton;
import grafix.gfxTypes.elements.FLPanel;
import grafix.windows.SimulationWindowAbstract;
import main.FiatLuxProperties;
import main.Macro;
import main.tables.PlotterSelectControl;

public class MeasuringAndPlotterArea extends FLBlockPanel {

	private static final String S_ZLOUPBUTTON = "Inst. meas." ;
	//private static final String S_LNKBUTTON = "Link" ;

	final FLButton measuresTitle = new FLButton("Measures");

	PlotterSelectControl m_plotterSelector = null; 	// 
	FLPanel m_PlotterSelectionPanel= new FLPanel(); // panel for selecting the plotter

	ZloupButton m_zloup= new ZloupButton();
	FLButton m_openPlotButton;

	private SuperSampler m_sampler;		

	public MeasuringAndPlotterArea(SuperSampler sampler){
		m_sampler = sampler;
		Construct();
	}

	public void Construct(){
		// adds the selection of data plotters
		SetPlotterSelector();
		if (m_plotterSelector != null ){ // add panel only if needed
			m_PlotterSelectionPanel.Add(m_plotterSelector);

			// objects creation
			FLPanel 	plotpanelbuttons= 	new FLPanel();
			m_openPlotButton = new OpenPlotButton();
			//			m_linkButton = new LinkDeviceButton();

			// buttons panel creation
			plotpanelbuttons.SetGridBagLayout();
			plotpanelbuttons.GetGridBagC().fill = GridBagConstraints.HORIZONTAL;
			plotpanelbuttons.GetGridBagC().weighty = 10;
			plotpanelbuttons.AddGridBag(m_openPlotButton, 0, 10);
			plotpanelbuttons.AddGridBag(m_zloup, 0, 20);
			//plotpanelbuttons.AddGridBag(m_linkButton, 0, 30);

			defineAsWindowBlockPanel(SimulationWindowAbstract.COL_PLOTAREA, measuresTitle, 135);
			installBlockPanelStyle(true, m_zloup,  m_openPlotButton);
			m_PlotterSelectionPanel.setOpaque(false);
			m_plotterSelector.setOpaque(false);
			measuresTitle.setIcon(IconManager.getUIIcon("measures"));

			AddGridBagButton(measuresTitle);

			FLPanel subGrid = new FLPanel();
			subGrid.SetGridBagLayout();
			subGrid.GetGridBagC().anchor = NORTH;
			subGrid.GetGridBagC().insets.set(10, 5, 5, 5);
			subGrid.GetGridBagC().weightx = 0.2;
			subGrid.AddGridBag(m_PlotterSelectionPanel, 0, 0);
			subGrid.AddGridBag(plotpanelbuttons, 1, 0);

			subGrid.setOpaque(false);

			AddGridBagY(subGrid);

			setTabId(FiatLuxProperties.SIDEBAR_TABS.MEASURES);
		} else {
			Macro.print(4, "No Data plotter found for simulation window");
		}

	}

	/** button for opening plots */
	class ZloupButton extends FLActionButton {
		DecimalParameterMeasurer m_dpm = null;

		public ZloupButton() {super(S_ZLOUPBUTTON);}

		@Override
		public void DoAction() {
			MeasuringDevice dev= m_plotterSelector.GetSelectedMD();
			if (dev==null){
				Macro.print("empty selection of measuring device [no action]");
			} else {
				//if (m_dpm==null){ // "lazy" linkage...
				m_dpm= (DecimalParameterMeasurer)dev;	
				// CAST to most generic type
				SuperSampler samp= m_sampler;
				dev.LinkTo(samp);
				//}				
				String str= m_dpm.GetName() + " " + m_dpm.GetMeasure();
				Macro.print(str);

			}
		}
	}

	/** button for opening plots */
	//	class LinkDeviceButton extends FLActionButton {
	//
	//		public LinkDeviceButton() {
	//			super(S_LNKBUTTON);
	//		}
	//		
	//		@Override
	//		public void DoAction() {
	//			
	//			MeasuringDevice md= m_plotterSelector.GetSelectedMD();
	//			Macro.print("linking m.d.:" + md.GetName() );
	//			SuperSampler samplerCasted= m_sampler;
	//			md.LinkTo(samplerCasted);
	//		}
	//	}

	/** button for opening plots */
	class OpenPlotButton extends FLActionButton{
		private static final String S_OPENBUTTON = "Open Plot" ;

		public OpenPlotButton() {super(S_OPENBUTTON);}
		@Override
		public void DoAction() {OpenPlot();}
	}


	/** opens a plot in a new window */
	protected void OpenPlot(){
		m_PlotterSelectionPanel.SetNewPanel(m_plotterSelector);
		// adding data plotter
		Monitor dataPlotter = m_plotterSelector.OpenSelectedPlotter();  //<--opens new window
		// Data Extractor
		if (dataPlotter != null) {
			// CAST HERE from interface Simulable to object Sampler 
			SuperSampler samplerCasted= m_sampler;
			samplerCasted.addMonitor(dataPlotter);
			if (dataPlotter instanceof DataPlotter){
				((DataPlotter)dataPlotter).LinkTo(samplerCasted);
			} else {
				Macro.SystemWarning("unhandeled case for dataplotter");
			}

		} else {
			Macro.print(4,"No data plotter was found");
		}	
	}



	/** returns a new dataPlotterSelector */
	protected void SetPlotterSelector(){
		String [] selection=  m_sampler.GetPlotterSelection();
		if (selection != null){
			m_plotterSelector= new PlotterSelectControl(selection);
		} else {
			// do nothing ! 
		}
	}
}