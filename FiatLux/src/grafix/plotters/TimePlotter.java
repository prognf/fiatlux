package grafix.plotters;

import experiment.measuring.general.DecimalParameterMeasurer;
import experiment.measuring.general.TimeMD;

/*--------------------
 * plotting param. vs. time
 *--------------------*/

public class TimePlotter extends DataPlotter {

	/** use LinkTo(...) after construction **/
	public TimePlotter(DecimalParameterMeasurer in_ypar) {
		super(new TimeMD(), in_ypar);
	}

	public String GetName() {
		return m_Ypar.GetName();
	}
}
