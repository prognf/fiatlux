package grafix.gfxTypes;

import java.awt.Graphics;

/*
 * objects that can be "painted" in a file
 * used by MacroGFX
 */
public interface FilePaintable {

	/* main method (default style) */
    void paintComponentForRecording(Graphics g, String message);
	
	/* object's width in pixels */
    int getWidthInPixels();

	/* object's length in pixels */
    int getHeightInPixels();
}
