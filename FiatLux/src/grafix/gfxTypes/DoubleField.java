package grafix.gfxTypes;

import grafix.gfxTypes.controllers.DoubleController;

/*--------------------
 * holds a double passively (no processing)
 * @author Nazim Fates
*--------------------*/

public class DoubleField extends DoubleController {

	
	final static double ZERO=0.;
	
	double m_doubleValue;

	/*--------------------
	 * construction
	--------------------*/
	
	
	/* constructor 1 */
	public DoubleField(String in_str, int in_textlen, double in_value) {
		super(in_str, in_textlen);
		m_doubleValue= in_value;
		Update();
	}

	public DoubleField(String in_str, double in_value, CONVERSIONRATE conversionfactor){
		super(in_str, conversionfactor);
		m_doubleValue= in_value;
		Update();
	} 

	/*--------------------
	 * implementations
	--------------------*/

	@Override
	protected double ReadDouble() {
		return m_doubleValue;
	}

	@Override
	public void SetDouble(double val) {
		m_doubleValue= val;	
	}
	
	/*--------------------
	 * get / set
	--------------------*/

	final public double GetValue(){
		return ReadDouble();
	}	
}
