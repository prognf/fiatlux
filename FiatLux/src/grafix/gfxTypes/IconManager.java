package grafix.gfxTypes;

import java.net.URL;

import javax.swing.ImageIcon;

public class IconManager {
    private static String BASE_PATH = "/";
    private static String BASE_URL = "grafix/icons/";
    private static String CA_URL = "grafix/icons/ca/";
    private static String BASE_EXT = ".png";
    private static String DEFAULTICON=BASE_PATH + CA_URL + "Blank" + BASE_EXT;

    public static ImageIcon getCAIcon(String name) {
        return getIcon(CA_URL + name + BASE_EXT);
    }

    public static ImageIcon getUIIcon(String name) {
        return getIcon(BASE_URL + name + BASE_EXT);
    }

    private static ImageIcon getIcon(String path) {
        //URL imgURL = IconManager.class.getClass().getResource(BASE_PATH + path);
    	
    	URL imgURL = IconManager.class.getResource(BASE_PATH + path);
    	//Macro.fPrint(" path: %s URL:%s ",BASE_PATH + path, imgURL);
    	
    	
        if (imgURL == null) {// URL not found
            // attempt of second address
        	imgURL = IconManager.class.getClass().getResource(DEFAULTICON);
            if (imgURL == null) {
                return new ImageIcon(); // white icon if default icon not found
            }
        }

        ImageIcon imageicon=new ImageIcon(imgURL); // creation of the icon from URL 
		return imageicon;
    }
}
