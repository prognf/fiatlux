package grafix.gfxTypes.controllers;

import javax.swing.event.ChangeListener;

import components.types.FLString;
import grafix.gfxTypes.elements.FLPanel;
import grafix.gfxTypes.elements.FLTextArea;
import grafix.gfxTypes.elements.FLTextField;

/**
 * This class associates at least one <tt>Label</tt>, 
 * one <tt>TextField</tt>(control) and one <tt>TextArea</tt>(display).
 *  A Slider can be added via the method <tt>AddSlider(int, int, int)</tt>
 * @author Nazim Fat&egraves
 * @author Olivier Bour&eacute
 */
/* this panel associates ONE TextField(control) and ONE TextArea(display) */

public abstract class ReadDisplayBasicControl extends FLPanel implements ChangeListener {
	/**
	 * How should the value be read
	 */
	abstract public void Read(String s);

	/**
	 * How should the value be displayed.
	 */
	abstract public String GetDisplayFromOwner(); 
	
	/*--------------------------------------------------------------------
	 * ATTRIBUTES
	 * ------------------------------------------------------------------*/
	
	private FLTextFieldRead m_inputField; // for reading
	private FLTextArea m_outputArea; // for displaying
	
	protected String m_FieldName; // name of the field
	
	/*--------------------------------------------------------------------
	 * CONSTRUCTOR
	 * ------------------------------------------------------------------*/
	
	/** creates a graphical model with a String,
	 * an Input field which size is in_readlen
	 * and a display area which size is in_displaylen
	 */
	public ReadDisplayBasicControl(String in_str, int in_readlen, int in_displaylen) {
		// label for the field
		m_inputField = new FLTextFieldRead(in_readlen);
		m_outputArea = new FLTextArea(in_displaylen);
		m_FieldName= in_str;
		// panel management
		AddLabel(in_str);
		Add(m_inputField, m_outputArea);
		setOpaque(false);
	}
	

	public void minimize() {
		m_outputArea.setVisible(false);
		m_inputField.setText(m_outputArea.getText());
	}

	/** this inner class defines the behaviour when a String is entered */
	class FLTextFieldRead extends FLTextField {
		public FLTextFieldRead(int in_len) {
			super(in_len);
		}
		
		/* reads and displays */
		public void parseInput(String input) {
			Read(input);
			Update();
		}		
	}
	
	
	/*--------------------------------------------------------------------
	 * ACTIONS
	 * ------------------------------------------------------------------*/
		
	/** Update both the display and the slider*/
	public void Update() {
		UpdateDisplay();
	}	
	
	/*--------------------------------------------------------------------
	 * UPDATE
	 * ------------------------------------------------------------------*/
	
	
	private void UpdateDisplay() {
		String newdisplay = GetDisplayFromOwner();
		SetDisplay(newdisplay);

		if (m_outputArea.isVisible()) {
			SetInputDisplay(FLString.EMPTYSTRING);
		} else {
			SetInputDisplay(newdisplay);
		}
	}

	
	
	/** Update the Display */
	protected void SetDisplay(String in_val) {
		m_outputArea.setText(in_val);	
	}
	
	/** Update The Input */
	protected void SetInputDisplay(String in_val) {
		m_inputField.setText(in_val);	
	}
	
	/** Clear the Input */
	protected void ClearReadzone(){
		m_inputField.Clear();
	}
}
