package grafix.gfxTypes.controllers;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JCheckBox;

import grafix.gfxTypes.elements.FLFrame;
import main.Macro;

abstract public class FLCheckBox extends JCheckBox implements ItemListener  {
	/** check action */
	protected abstract void SelectionOn();

	/** deselection action */
	protected abstract void SelectionOff();

	/*--------------------
	 * construction
	 --------------------*/
	public FLCheckBox() {
		super();
		setOpaque(false);
		addItemListener(this);
	}

	public FLCheckBox(String txt) {
		this(txt,false);
	}

	public FLCheckBox(String txt, boolean selected){
		super(txt, selected);
		setOpaque(false);
		addItemListener(this);
	}

	/*--------------------
	 * actions
	 --------------------*/
	public void itemStateChanged(ItemEvent e) {
		if (e.getStateChange() == ItemEvent.DESELECTED){
			SelectionOff();
		} else {
			SelectionOn();
		}
	}

	/*--------------------
	 * test
	 --------------------*/
	public static void DoTest(){
		class TextCheckBox extends FLCheckBox {

			public TextCheckBox(String txt) {
				super(txt);
			}
			@Override
			public void SelectionOff() {
				Macro.print("OFF");
			}


			@Override
			public void SelectionOn() {
				Macro.print("ON");			
			}	
		}

		Macro.BeginTest("FLcheckBox");
		FLFrame fl = new FLFrame("test");
		TextCheckBox testobject= new TextCheckBox("test of check box");
		fl.addPanel(testobject);
		fl.packAndShow();
		Macro.EndTest();
	}
}