package grafix.gfxTypes.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;

/************************************
 * The class Radio2PosControl is a control with two positions action1 and action2
 * an action can be associated to each button 
 * @author  Nazim Fates
 * @version     %I%, %G%   
 *******************************************/
import grafix.gfxTypes.elements.FLPanel;

public class Radio2Control extends FLPanel {

	/** 
	 * override this one if needed 
	 **/
	protected void ActionSelection1() {
		// do nothing by default
	}

	/** 
	 * override this one if needed 
	 **/	
	protected void ActionSelection2() {
		// do nothing
	}

	/* attributes */
	boolean m_Action1Flag = true;

	JRadioButton m_Action1Button, m_Action2Button;
	ButtonGroup m_group; 

	public Radio2Control(String label, String action1, String action2){
		m_Action1Button = new JRadioButton(action1);
		m_Action2Button = new JRadioButton(action2);
		
		m_group = new ButtonGroup();
		m_group.add(m_Action1Button);
		m_group.add(m_Action2Button);

		// default selection
		m_Action1Button.setSelected(true);

		m_Action1Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				m_Action1Flag = true;
				ActionSelection1();
			}
		});
		m_Action2Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				m_Action1Flag = false;
				ActionSelection2();
			}
		});

		// panel management
		AddLabel(label);
		this.add(m_Action1Button);
		this.add(m_Action2Button);
	}

	public boolean IsFirstItemSelected(){
		return m_Action1Button.isSelected();
	}

	/** special for disabling the focus **/
	public void setPartsNonFocusable() {
		m_Action1Button.setFocusable(false);
		m_Action2Button.setFocusable(false);
	}
	
	

}
