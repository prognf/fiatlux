package grafix.gfxTypes.controllers;

import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;

import components.types.FLString;
import main.Macro;
/************************************
 * this class is for controlling and displaying integers 
 * users can set a range of validity for the input
 * @author Nazim Fates
 *******************************************/
abstract public class FloatController extends ReadDisplaySliderControl {	
	
	/*--------------------------------------------------------------------
	 * ABSTRACT
	 * ------------------------------------------------------------------*/
	
	/** Get the float from Owner */
	abstract protected float ReadFloat();
	/** Set the float to Owner */
	abstract protected void SetFloat(float val);
	
	/*--------------------------------------------------------------------
	 * CONSTANTS
	 * ------------------------------------------------------------------*/

	private static final int DEF_TEXTLEN = 3;
	public enum CONVERSIONRATE { ONE, PERCENT }
	
	/*--------------------------------------------------------------------
	 * ATTRIBUTES
	 * ------------------------------------------------------------------*/

	boolean m_checkBounds= false;
	float m_LimitLo= Float.MIN_VALUE;
	float m_LimitHi= Float.MAX_VALUE;
	
	// The Double and Integer limits for the Slider
	float 	m_Fmin, m_Fmax;
	int 	m_Imin, m_Imax;
	
	/*--------------------------------------------------------------------
	 * CONSTRUCTOR
	 * ------------------------------------------------------------------*/

	public FloatController(String in_str, int in_textlen) {
		super(in_str, in_textlen, in_textlen);
		Update();
	}

	public FloatController(String in_label) {
		this(in_label, DEF_TEXTLEN);
	}
	
	public void AddSlider(float min, float max, int ticks) {
		m_Fmin = min; 	m_Fmax = max;
		m_Imin = 0;		m_Imax = ticks;
		ActivateSlider(m_Imin, m_Imax);
	}

	/*--------------------------------------------------------------------
	 * ACTIONS
	 * ------------------------------------------------------------------*/

	@Override
	public void Read(String s) {
		if (!s.isEmpty()){
			try{
				float val= FLString.ParseFloat(s);
				if (m_checkBounds && (val < m_LimitLo) || (val>m_LimitHi)){
					Macro.UserMessage("please enter a number between " + m_LimitLo + " and " + m_LimitHi);
					ClearReadzone();
				} else {
					SetFloat(val);
					Update();
				}
			} catch (NumberFormatException e){
				FLString.ParsingError(m_FieldName, s);
				ClearReadzone();
			}
		}
	}
	
	@Override
	public void stateChanged(ChangeEvent e) {
		// Get the slider 
		JSlider source = (JSlider) e.getSource();
						
		// Convert the value of the slider to Double
		float doubleval = ConvertIntToFloat(source.getValue());

		// Set whatever owns this controller
		SetFloat(doubleval);
		
		// Update the display
		Update();
	}
	
	/*--------------------------------------------------------------------
	 * GET & SET
	 * ------------------------------------------------------------------*/

	@Override
	public int GetValueFromOwner() {
		return ConvertFloatToInt(ReadFloat());
	}
	
	@Override
	public String GetDisplayFromOwner() {
		return FLString.EMPTYSTRING + ReadFloat() ;
	}

	/* lo and hi are inclusive (i.e., valid numbers) */
	public void SetValidityRange(float LimitLo, float LimitHi){
		m_checkBounds= true;
		m_LimitLo= LimitLo;
		m_LimitHi= LimitHi;
	}
	
	private int ConvertFloatToInt(float val) {
		float res = (m_Imax - m_Imin) * (val-m_Fmin) / (m_Fmax - m_Fmin) + m_Imin;
		return (int) res;
	}

	private float ConvertIntToFloat(int val) {
		float res = (m_Fmax - m_Fmin) * (val-m_Imin) / (float) (m_Imax - m_Imin) + m_Fmin;
		return res;
	}
}
