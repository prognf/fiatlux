package grafix.gfxTypes.controllers;

import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;

import components.types.FLString;
import main.Macro;
/************************************
 * This class is for controlling and displaying integers. <br>
 * Users can set a range of validity for the input, or add
 * a Slider Controller.
 * @author Nazim Fat&egraves
 * @author Olivier Bour&eacute
 *******************************************/
abstract public class IntController extends ReadDisplaySliderControl {
	
	/*--------------------------------------------------------------------
	 * ABSTRACT
	 * ------------------------------------------------------------------*/
	
	/** Get the integer from Owner */
	protected abstract int ReadInt();
	/** Set the integer to Owner */
	protected abstract void SetInt(int val);
	
	/*--------------------------------------------------------------------
	 * CONSTANTS
	 * ------------------------------------------------------------------*/
	
	private static final int DEFLEN = 3;
	
	/*--------------------------------------------------------------------
	 * ATTRIBUTES
	 * ------------------------------------------------------------------*/
	
	int m_LimitLo = Integer.MIN_VALUE;
	int m_LimitHi = Integer.MAX_VALUE;
	
	// The Integer limits for the Slider
	int 	m_Imin, m_Imax;
	
	/*--------------------------------------------------------------------
	 * CONSTRUCTOR
	 * ------------------------------------------------------------------*/
	
	public IntController(String in_str, int in_textlen) {
		super(in_str, in_textlen, in_textlen);
		PrepareandUpdateDisplay();
	}
	
	public IntController(String in_str) {
		this(in_str, DEFLEN);
	}
	
	/* called after the initialisation overwrite if needed */
	protected void PrepareandUpdateDisplay(){
		Update();
	}
	
	public void AddSlider(int min, int max) {
		m_Imin = min;		
		m_Imax = max;
		ActivateSlider(m_Imin, m_Imax);
	}

	/*--------------------------------------------------------------------
	 * ACTIONS
	 * ------------------------------------------------------------------*/

	@Override
	public void Read(String s) {
		if (!s.isEmpty()){
			try{
				int val= FLString.ParseInt(s);
				if ((val < m_LimitLo) || (val > m_LimitHi)){
					Macro.UserMessage("please enter a number between " + m_LimitLo + " and " + m_LimitHi);
					ClearReadzone();
				} else {
					SetInt(val);
					Update();
				}
			} catch (NumberFormatException e){
				FLString.ParsingError(m_FieldName, s);
				//e.printStackTrace();
				ClearReadzone();
			}
		}
	}
	
	public void stateChanged(ChangeEvent e) {
		// Get the slider 
		JSlider source = (JSlider) e.getSource();
		
		// Set the value
		SetInt(source.getValue());

		// Update the display
		Update();
	}

	/*--------------------------------------------------------------------
	 * GET & SET
	 * ------------------------------------------------------------------*/

	@Override
	public String GetDisplayFromOwner() {
		return FLString.EMPTYSTRING + ReadInt();
	}
	
	@Override
	public int GetValueFromOwner() {
		return ReadInt();
	}
	
	/* lo and hi are inclusive (i.e., valid numbers) */
	public void SetValidityRange(int LimitLo, int LimitHi){
		m_LimitLo= LimitLo;
		m_LimitHi= LimitHi;
	}
}
