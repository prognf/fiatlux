package grafix.gfxTypes.controllers;

import grafix.gfxTypes.elements.FLActionButton;
import grafix.gfxTypes.elements.FLLabel;
import grafix.gfxTypes.elements.FLPanel;
import grafix.gfxTypes.elements.FLTextArea;

abstract public class PolyChoicePanel extends FLPanel {
	
	final protected static String CHANGE="CHANGE";
	
	protected int m_indexChoice;
	FLTextArea m_text= new FLTextArea(4);
	FLActionButton m_change= new BChangeButton(this);
	String [] m_options;
	int m_size;
			
	abstract protected void Update(); 
	
	public PolyChoicePanel(String label, String [] options, int defIndexSelect) {
		m_indexChoice= defIndexSelect;
		this.Add(new FLLabel(label), m_change, m_text);
		m_options= options;
		UpdateHere();
	}

	public FLActionButton getButton() {
		return m_change;
	}

	public void ChangeFormat() {
		m_indexChoice = (m_indexChoice +1 ) % m_options.length;
		UpdateHere();
	}
	
	private void UpdateHere() {
		m_text.SetText(m_options[m_indexChoice]);
		Update();
		
	}

	class BChangeButton extends FLActionButton {
		
		PolyChoicePanel m_caller;
		
		public BChangeButton(PolyChoicePanel changeButton) {
			super(CHANGE);
			m_caller= changeButton;
		}

		@Override
		public void DoAction() {
			m_caller.ChangeFormat();
		}
	}
	
} 

