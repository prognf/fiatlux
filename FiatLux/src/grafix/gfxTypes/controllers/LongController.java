package grafix.gfxTypes.controllers;

import javax.swing.event.ChangeEvent;

import components.types.FLString;
import main.Macro;
/************************************
 * this class is for controlling and displaying integers 
 * users can set a range of validity for the input
 * @author Nazim Fates
 *******************************************/
abstract public class LongController extends ReadDisplayBasicControl {
	
	private static final int DEFLEN = 3;

	protected abstract long ReadLong();
	protected abstract void SetLong(long val);
	
	/*--------------------
	 * attributes
	 --------------------*/
	
	long m_LimitLo= Long.MIN_VALUE;
	long m_LimitHi= Long.MAX_VALUE;
	
	/*--------------------
	 * construction
	 --------------------*/
	
	public LongController(String in_str, int in_textlen) {
		super(in_str, in_textlen, in_textlen);
		PrepareandUpdateDisplay();
	}
	
	public LongController(String in_str) {
		this(in_str, DEFLEN);
	}
	
	/* called after the initialisation overwrite if needed */
	protected void PrepareandUpdateDisplay(){
		Update();
	}

	/*--------------------
	 * implementations
	 --------------------*/
	
	@Override
	final public void stateChanged(ChangeEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public String GetDisplayFromOwner() {
		return FLString.EMPTYSTRING + ReadLong();
	}

	@Override
	public void Read(String s) {
		if (!s.isEmpty()){
			try{
				long val= FLString.ParseLong(s);
				if ((val < m_LimitLo) || (val>m_LimitHi)){
					Macro.UserMessage("please enter a number between " + m_LimitLo + " and " + m_LimitHi);
					ClearReadzone();
				} else {
					SetLong(val);
					Update();
				}
			} catch (NumberFormatException e){
				FLString.ParsingError(m_FieldName, s);
				e.printStackTrace();
				ClearReadzone();
			}
		}
	}

	/*--------------------
	 * get / set
	 --------------------*/
	
	/* lo and hi are inclusive (i.e., valid numbers) */
	public void SetValidityRange(int LimitLo, int LimitHi){
		m_LimitLo= LimitLo;
		m_LimitHi= LimitHi;
	}

}
