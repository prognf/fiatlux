package grafix.gfxTypes.controllers;

/**
 * When a handle for updating window labels is needed.
 */
public interface ILabelUpdatable {
	void updateLabel(String s);
}
