package grafix.gfxTypes.controllers;

import java.util.ArrayList;

import grafix.gfxTypes.elements.FLPanel;
import grafix.gfxTypes.elements.FLTextArea;
import grafix.gfxTypes.elements.FLTextField;


/* this panel associates ONE TextField(control) and ONE TextArea(display) */
public abstract class MultiReadOneDisplayControl extends FLPanel {
	
	/* reading the input */
	abstract public void ReadInput(int pos, String s);
	/*  how the value should be displayed ? */
	abstract public String Display(); 
	
	/*--------------------
	 * attributes
	 --------------------*/
	private ArrayList<FLTextFieldRead> m_textfield; // for reading
	private FLTextArea m_newarea; // for displaying
	protected String m_FieldName; // name of the field
	
	/*--------------------
	 * construction
	 --------------------*/
	
	/* labels of the field
	 */
	public MultiReadOneDisplayControl(String [] in_labels, int in_readlen, int in_displaylen) {
		setOpaque(false);

		// panel management
		m_textfield= new ArrayList<FLTextFieldRead>();
		for(int pos=0; pos < in_labels.length; pos++){
			FLTextFieldRead newtextfield = new FLTextFieldRead(pos, in_readlen);
			m_textfield.add( newtextfield );
			String label= in_labels[pos];
			this.AddLabel(label);
			this.add(newtextfield);
		}
		m_newarea = new FLTextArea(in_displaylen);
		add(m_newarea);
	}
	
	/* this inner class defines the behaviour when a String is entered */
	class FLTextFieldRead extends FLTextField{
		final int m_pos;
		
		public FLTextFieldRead(int pos, int in_len) {
			super(in_len);
			m_pos = pos;
		}
		
		/* reads and displays */
		public void parseInput(String input) {
			ReadInput(m_pos, input);
			Update();
		}		
	}
	
	
	/*--------------------
	 * actions
	 --------------------*/
		
	/* update the display with the model value */
	public void Update(){
		String newdisplay= Display();
		//Macro.Debug("Update" + newdisplay);
		SetDisplay(newdisplay);
		//SetInputDisplay(Macro.EMPTYSTRING);
	}	
	
	
	/* display update */
	protected void SetDisplay(String in_val) {
		m_newarea.setText(in_val);	
	}
	
	/* display update */
	/*protected void SetInputDisplay(String in_val) {
		m_newtextfield.setText(in_val);	
	}
	
	protected void ClearReadzone(){
		m_newtextfield.Clear();
	}*/
	
}
