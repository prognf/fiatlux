package grafix.gfxTypes.controllers;

import components.types.IVector;
import grafix.gfxTypes.elements.FLFrame;
import grafix.gfxTypes.elements.FLPanel;
import main.Macro;

/*--------------------
 * encapsulates an array of ints of arbitrary size
 * @author Nazim Fates
*--------------------*/

public class IntVectorControl extends FLPanel {

	VIntController [] m_IntController; 
	IVector m_data;

	/*--------------------
	 * Constructor
	 --------------------*/

	public IntVectorControl(IVector data, String [] label){
		super();
		m_data = data;
		int size= data.GetSize();
		m_IntController= new VIntController [size];
		for (int i = 0; i < size; i++) {
			m_IntController[i]= new VIntController(i,label[i]);
			this.Add(m_IntController[i]);
		}
	}

	/*--------------------
	 * Get / Set
	 --------------------*/

	public int GetVal(int pos) {
		return m_data.GetVal(pos);
	}

	public int GetSize() {
		return m_data.GetSize();
	}

	/*--------------------
	 * GFX
	 --------------------*/

	class VIntController extends grafix.gfxTypes.controllers.IntController {

		private int m_rank=-1; // associated rank

		public VIntController(int rank, String label) {
			super(label);
			//Macro.Debug("entered rank = " + rank);
			m_rank= rank;
			//Macro.Debug("created controller with rank :" + m_rank);
			Update(); 
			// it necessary to do it here to ensure that m_rank has been updated 

		}

		@Override
		protected int ReadInt() {
			//Macro.Debug("read:" + m_rank);
			return m_data.GetVal(m_rank);
		}

		@Override
		protected void SetInt(int val) {
			m_data.SetVal(m_rank, val);			
		}	

	}




	/*--------------------
	 * test
	 --------------------*/

	final static String ClassName= Thread.currentThread().getStackTrace()[1].getClassName();
	

	public static void DoTest() {
		Macro.BeginTest(ClassName);
		
		String label [] = {"hing", "hong", "hang", "hung"};

		// testing from vector to string
		int val [] = { 11, 20, -30, 50};
		IVector v = new IVector(val);
		IntVectorControl testObject = new IntVectorControl(v, label);
		FLFrame fr = new FLFrame("test");
		fr.addPanel(testObject);
		fr.packAndShow();
		for (int i = 0; i < testObject.GetSize(); i++) {
			Macro.print( "rank" + i + " val: " + testObject.GetVal(i) );
		}
		/* for continuous printing while (true){
		   Macro.print(v.ToString());
		}*/
		Macro.EndTest();
	}
	
	

	public static void main(String[] argv) {
		DoTest();
	}



}// end class


