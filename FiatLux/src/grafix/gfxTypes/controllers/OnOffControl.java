package grafix.gfxTypes.controllers;

public class OnOffControl extends Radio2Control {

	public OnOffControl(String label, boolean defSelect) {
		super(label, "off", "on");
		if (defSelect){
			m_Action2Button.setSelected(true);
		}
	}
	
	/** override this one if needed **/
	protected void ActionSelection(boolean b) {}
	
	@Override
	final protected void ActionSelection1() {
		ActionSelection(false);
	}

	@Override
	final protected void ActionSelection2() {
		ActionSelection(true);
	}
	
}
