package grafix.gfxTypes.controllers;

import java.awt.Component;

import grafix.gfxTypes.elements.FLPanel;


/************************************
 * this class is for controlling and displaying doubles 
 * users can set a range of validity for the input
 * @author Nazim Fates
 *******************************************/
abstract public class MultiDoubleController extends FLPanel {
	
	abstract protected double ReadValArg(int arg);
	abstract protected void SetValArg(int arg, double val);
	abstract protected String[] getParamLabels(); //for the construction

	/*--------------------
	 * constants and attributes
	 --------------------*/
	private static final int DEF_TEXTLEN = 3;
	public boolean VERTICALALIGNBYEIGHT=true;
	//private ArrayList<DoubleController> m_ArrayDControl; 
	
	/*--------------------
	 * construction
	 --------------------*/
	
	/** default constructor **/
	public MultiDoubleController() {
		this(DEF_TEXTLEN);
	}
	
	/** constructor with labels, default values and text len **/
	private MultiDoubleController(int in_textlen) {
		if (VERTICALALIGNBYEIGHT) {
			this.SetGridBagLayout();
		}
		String [] labels= getParamLabels();
		int size= labels.length;
		for (int pos=0; pos<size; pos++){
			OneControl control= new OneControl(pos, labels[pos]);
			control.setAlignmentX(Component.LEFT_ALIGNMENT);
			if (VERTICALALIGNBYEIGHT) {
				this.AddGridBag(control, pos/8, pos%8);
			} else {
				this.Add(control);
			}
		}		
	}

	
	/*--------------------
	 * actions
	 --------------------*/

	class OneControl extends DoubleController {

		private static final int TXTLEN = 6;
		int m_pos;
		
		public OneControl(int pos, String label) {
			super(label,TXTLEN); 
			m_pos= pos;
			Update();
		}

		@Override
		final protected double ReadDouble() {
			return ReadValArg(m_pos);
		}

		@Override
		final protected void SetDouble(double val) {
			SetValArg(m_pos, val);	
		}
		
	}
	
	
	/*--------------------
	 * get & set
	 --------------------*/
	
	
	

	/*--------------------
	 * test
	 --------------------*/
	
}
