package grafix.gfxTypes.controllers;

import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;

import components.types.FLString;
import main.Macro;
import main.MathMacro;
/************************************
 * This class is for controlling and displaying doubles. <br>
 * Users can set a range of validity for the input, or add
 * a Slider Controller.
 * @author Nazim Fat&egraves
 * @author Olivier Bour&eacute
 *******************************************/
abstract public class DoubleController extends ReadDisplaySliderControl {
	
	/*--------------------------------------------------------------------
	 * ABSTRACT
	 * ------------------------------------------------------------------*/
	
	/** Get the double from Owner */
	abstract protected double ReadDouble();
	/** Set the double to Owner */
	abstract protected void SetDouble(double val);
	
	/*--------------------------------------------------------------------
	 * CONSTANTS
	 * ------------------------------------------------------------------*/
	
	private static final int DEF_TEXTLEN = 3;
	public enum CONVERSIONRATE { ONE, PERCENT }
	
	/*--------------------------------------------------------------------
	 * ATTRIBUTES
	 * ------------------------------------------------------------------*/
	
	boolean m_checkBounds= false;
	double m_LimitLo= Double.NEGATIVE_INFINITY;
	double m_LimitHi= Double.POSITIVE_INFINITY;
	
	// The Double and Integer limits for the Slider
	double 	m_Dmin, m_Dmax;
	int 	m_Imin, m_Imax;
	
	double m_ConversionFactor; 
	
	/*--------------------------------------------------------------------
	 * CONSTRUCTOR
	 * ------------------------------------------------------------------*/
	
	public DoubleController(String in_str, int in_textlen) {
		this(in_str, in_textlen, CONVERSIONRATE.ONE);
	}

	/* conversion factor is used for percent controls */
	public DoubleController(String in_str, CONVERSIONRATE conversionfactor) {
		this(in_str, DEF_TEXTLEN, conversionfactor);
	}

	private DoubleController(String in_str, int in_textlen, CONVERSIONRATE conversionfactor) {
		super(in_str, in_textlen, in_textlen);
		switch (conversionfactor) {
		case PERCENT:
			m_ConversionFactor= MathMacro.HUNDRED;
			m_LimitHi= MathMacro.HUNDRED;
			m_LimitLo= 0;
			break;
		case ONE:
			m_ConversionFactor= 1;
			break;
		}
		Update();
	}

	public DoubleController(String in_label) {
		this(in_label, DEF_TEXTLEN);
	}
	
	/** ticks computes by how many intervals the slider is divided 
	 * (when the user moves cursor) : typically use 100 ticks **/ 
	public void AddSlider(double min, double max, int ticks) {
		m_Dmin = min; 	m_Dmax = max;
		m_Imin = 0;		m_Imax = ticks;
		ActivateSlider(m_Imin, m_Imax);
	}
	
	public void AddSlider() {
		AddSlider(1000);
	}
	
	public void AddSlider(int ticks) {
		double min = 0;
		double max = 1; 
		
		m_Dmin = min; 	m_Dmax = max;
		m_Imin = 0;		m_Imax = ticks;
		ActivateSlider(m_Imin, m_Imax);
	}

	/*--------------------------------------------------------------------
	 * ACTIONS
	 * ------------------------------------------------------------------*/

	@Override
	public void Read(String s) {
		if (!s.isEmpty()){
			try{
				double val= FLString.ParseDouble(s);
				if (m_checkBounds && (val < m_LimitLo) || (val>m_LimitHi)){
					Macro.UserMessage("please enter a number between " + m_LimitLo + " and " + m_LimitHi);
					ClearReadzone();
				} else {
					SetDouble(val / m_ConversionFactor);
					Update();
				}
			} catch (NumberFormatException e){
				FLString.ParsingError(m_FieldName, s);
				ClearReadzone();
			}
		}
	}
	
	@Override
	public void stateChanged(ChangeEvent e) {
		// Get the slider 
		JSlider source = (JSlider) e.getSource();
						
		// Convert the value of the slider to Double
		double doubleval = ConvertIntToDouble(source.getValue());

		// Set whatever owns this controller
		SetDouble(doubleval);
		
		// Update the display
		Update();
	}

	/*--------------------------------------------------------------------
	 * GET & SET
	 * ------------------------------------------------------------------*/

	@Override
	public String GetDisplayFromOwner() {
		return FLString.EMPTYSTRING + (float) (ReadDouble() * m_ConversionFactor);
	}
	
	@Override
	public int GetValueFromOwner() {
		return ConvertDoubleToInt(ReadDouble() * m_ConversionFactor);
	}
	
	/* lo and hi are inclusive (i.e., valid numbers) */
	public void SetValidityRange(double LimitLo, double LimitHi){
		m_checkBounds= true;
		m_LimitLo= LimitLo;
		m_LimitHi= LimitHi;
	}
	
	private int ConvertDoubleToInt(double val) {
		double res = (m_Imax - m_Imin) * (val-m_Dmin) / (m_Dmax - m_Dmin) + m_Imin;
		return (int) res;
	}

	private double ConvertIntToDouble(int val) {
		double res = (m_Dmax - m_Dmin) * (val-m_Imin) / (double) (m_Imax - m_Imin) + m_Dmin;
		return res;
	}
}