package grafix.gfxTypes.controllers;

import grafix.gfxTypes.elements.FLTextArea;

/**
 * simple view in text mode 
 * use it when an owner needs to update a text area 
 * 
 * @author Nazim Fates
 *
 */
public class UpdatableTextDisplay extends FLTextArea {

	public UpdatableTextDisplay(int in_columns) {
		super(in_columns);
	}

	public UpdatableTextDisplay(int in_columns, String initialText) {
		this(in_columns);
		SetText(initialText);
	}

	public void NotifyModification(String newText){
		this.SetText(newText);
	}
	
}