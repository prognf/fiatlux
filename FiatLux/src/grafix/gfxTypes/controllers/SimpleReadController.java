package grafix.gfxTypes.controllers;

import grafix.gfxTypes.elements.FLPanel;
import grafix.gfxTypes.elements.FLTextField;

/*-------------------------------------------------------------------------------
-  A simpel way to combine a text and a read area
- @author Nazim Fates
------------------------------------------------------------------------------*/

abstract public class SimpleReadController extends FLPanel {

	// main method : how to deal with input
	abstract public void ReadAndParseInput(String readInput);
	
	class FLTextFieldRead extends FLTextField {

		protected FLTextFieldRead(int in_len) {
			super(in_len);
		}

		@Override
		public void parseInput(String input) {
			ReadAndParseInput(input);
		}
		
	}
	
	/*----------------------------------------------------------------------------
	  - attributes
	  --------------------------------------------------------------------------*/

	
	
	/*---------------------------------------------------------------------------
	  - construction
	  --------------------------------------------------------------------------*/

	public SimpleReadController(String textLabel, int len) {
		AddLabel(textLabel);
		Add( new FLTextFieldRead(len) );
	}

	
	/*---------------------------------------------------------------------------
	  - actions
	  --------------------------------------------------------------------------*/
	
}
	
	
	
