package grafix.gfxTypes.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;

/************************************
 * The class Radio2PosControl is a control with two positions action1 and action2
 * an action can be associated to each button 
 * @author  Nazim Fates
 * @version     %I%, %G%   
 *******************************************/
import grafix.gfxTypes.elements.FLPanel;
import main.Macro;



abstract public class Radio3Control extends FLPanel {

	
	abstract protected void  Action1();
	abstract protected void  Action2();
	abstract protected void  Action3();
	
	public void AttachActionToButton1(ActionListener action) {
		m_Action1Button.addActionListener(action);
	}

	public void AttachActionToButton2(ActionListener action) {
		m_Action2Button.addActionListener(action);
	}

	public void AttachActionToButton3(ActionListener action) {
		m_Action3Button.addActionListener(action);
	}

	
		JRadioButton m_Action1Button, m_Action2Button, m_Action3Button;

	/* constructor : button one is ON by default */
	public Radio3Control(String label, String action1, String action2, String action3) {

		m_Action1Button = new JRadioButton(action1);
		m_Action2Button = new JRadioButton(action2);
		m_Action3Button = new JRadioButton(action3);

		ButtonGroup group = new ButtonGroup();
		group.add(m_Action1Button);
		group.add(m_Action2Button);
		group.add(m_Action3Button);

		// default selection
		m_Action1Button.setSelected(true);
		m_Action1Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {Action1();}
		});
		m_Action2Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {Action2();}
		});
		m_Action3Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {Action3();}
		});
		
		// panel management
		AddLabel(label);
		this.add(m_Action1Button);
		this.add(m_Action2Button);
		this.add(m_Action3Button);
	}

	public void SetSelection(int num){
		switch (num){
		case 1:
			m_Action1Button.setSelected(true); break;
		case 2:
			m_Action2Button.setSelected(true); break;
		case 3:
			m_Action3Button.setSelected(true); break;
		default:
			Macro.FatalError("Bad selection in SetSelection:" + num);		
		}			
	}
	

}
