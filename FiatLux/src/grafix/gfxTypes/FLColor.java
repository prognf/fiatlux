package grafix.gfxTypes;

import java.awt.Color;

import components.randomNumbers.FLRandomGenerator;

/*--------------------
 * Class FLColor is an other name for Color
 * 
 * @author Nazim Fates
 *--------------------*/
public class FLColor extends Color {

	final static public int 
	NOP=0, MAX = 255, 
	Q12= MAX / 2, Q13=  MAX / 3, Q14=  MAX / 4,
	Q23= 2*MAX/3, Q34= 3*MAX/4, Q45= 4*MAX/5, Q56= 5*MAX/6, Q67 = 6* MAX / 7,
	Q78 = 12 * MAX / 13;

	final static public FLColor 
	c_black = 		new FLColor(NOP, NOP, NOP),
	c_white = 		new FLColor(MAX, MAX, MAX),
	c_red = 		new FLColor(MAX, NOP, NOP),
	c_lightred = 	new FLColor(MAX, Q45, Q45),
	c_medred = 		new FLColor(MAX, Q23, Q23),
	c_darkred = 	new FLColor(Q13, NOP, NOP),
	c_green = 		new FLColor(NOP, MAX, NOP),
	c_lightgreen = 	new FLColor(Q45, MAX, Q45),
	c_darkgreen = 	new FLColor(NOP, Q13, NOP),
	c_darkgreen2 = 	new FLColor(NOP, Q12, NOP),
	c_darkergreen = new FLColor(NOP, Q14, NOP),
	c_lightblue =   new FLColor(Q23, Q23, MAX),
	c_blue = 		new FLColor(NOP, NOP, MAX),
	c_blue2 = 		new FLColor(NOP, NOP, Q34),
	c_darkblue = 	new FLColor(NOP, NOP, Q23),
	c_cyan = 		new FLColor(NOP, MAX, MAX),
	c_yellow = 		new FLColor(MAX, MAX, NOP),
	c_lime = 		new FLColor(Q45, MAX, NOP),
	c_lightyellow = new FLColor(MAX, MAX, Q23),
	c_medyellow =	new FLColor(MAX, MAX, Q23),
	c_purple = 		new FLColor(MAX, NOP, MAX),
	c_lightpurple =	new FLColor(MAX, Q23, MAX),
	c_lightgrey = 	new FLColor(Q56, Q56, Q56),
	c_grey0 = 		new FLColor(Q45, Q45, Q45),
	c_grey1 = 		new FLColor(Q34, Q34, Q34),
	c_grey2 = 		new FLColor(Q23, Q23, Q23),
	c_grey3 = 		new FLColor(Q12, Q12, Q12),
	c_grey4 = 		new FLColor(Q13, Q13, Q13),
	c_superRed = 	new FLColor(MAX, Q12, Q12),
	c_brown = 		new FLColor(Q23, Q13, NOP),
	c_lightbrown = 	new FLColor(Q34, Q23, NOP),
	c_orange = 		new FLColor(MAX, Q23, Q13),
	c_lightorange = new FLColor(MAX, Q45, Q12),
	c_darkorange =	new FLColor(Q78, Q12, NOP),
	c_syellow = 	new FLColor(Q34, Q12, NOP),
	c_lightcyan = 	new FLColor(Q45, MAX, MAX),
	c_zstyolle = 	new FLColor(Q13, Q13, Q14),
	c_grenat= new FLColor(Color.decode("#6E0B14") ),
	DEF_GHOSTCOLOR = c_grey1;

	public FLColor(int r, int g, int b){
		super(r,g,b);
	}

	/** copy constr. */
	public FLColor(Color color) {
		super(color.getRed(), color.getGreen(), color.getBlue());
	}

	public Color brighter(double factor) {
		int r = getRed();
		int g = getGreen();
		int b = getBlue();
		int alpha = getAlpha();

		int i = (int)(1.0/(1.0 - factor));
		if ( r == 0 && g == 0 && b == 0) {
			return new Color(i, i, i, alpha);
		}

		if ( r > 0 && r < i ) {
			r = i;
		}

		if ( g > 0 && g < i ) {
			g = i;
		}

		if ( b > 0 && b < i ) {
			b = i;
		}

		return new Color(Math.min((int)(r/factor), 255), Math.min((int)(g/factor), 255), Math.min((int)(b/factor), 255), alpha);
	}

	public Color darker(double factor) {
		return new Color(Math.max((int)(getRed() * factor), 0), Math.max((int)(getGreen() * factor), 0), Math.max((int)(getBlue() * factor), 0), getAlpha());
	}

	public FLColor applyNoise(FLRandomGenerator rnd, int range) {
		int r = getRed();
		int g = getGreen();
		int b = getBlue();
		int dr=	rnd.RandomInt(2*range+1)-range;
		int dg=	rnd.RandomInt(2*range+1)-range;
		int db=	rnd.RandomInt(2*range+1)-range;
		return new FLColor(sat(r+dr), sat(g+dg), sat(b+db));
	}

	private int sat(int val) {
		if (val<0) return 0;
		if (val>255) return 255;
		return val;
	}
}
