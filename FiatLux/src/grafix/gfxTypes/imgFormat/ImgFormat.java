package grafix.gfxTypes.imgFormat;

public enum ImgFormat {
	
	SVG ("svg"),
	PNG ("png"),
	BMP ("bmp"),
	TXT("txt");
	
	static public ImgFormat DEFIMGSAVE = ImgFormat.PNG;
	
	final String m_extension;

	ImgFormat(String extension) {
		m_extension= extension;
	}
	
	public String extension() {
		return "."+m_extension;
	}
	
}

// formats
//public final static String 
//s_PDDL=".pddl",
//s_SVG=".svg", SVGFORMAT= "svg",
//zzzs_TEX=".tex", zzzTEXFORMAT= "tex",	
//s_PNG=".png", PNGFORMAT= "png",
//s_BMP=".bmp", BMPFORMAT= "bmp";	
