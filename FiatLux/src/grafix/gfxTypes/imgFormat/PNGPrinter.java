package grafix.gfxTypes.imgFormat;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import grafix.gfxTypes.FilePaintable;
import main.Macro;


public class PNGPrinter extends ImgPrinter {
	
	public final String FORMAT = "png";
	public final String EXT = ".png";

	@Override
	public void SaveImg(FilePaintable paintableObject, String in_filename,
			String in_message) {

		//  image object creation and setting
		BufferedImage image = PrepareRecording(paintableObject, in_message);

		// 	writing 
		String filename = in_filename + EXT;
		File PNGfile = new File(filename);

		try {
			ImageIO.write(image, FORMAT, PNGfile);
		} catch (Exception e) {
			Macro.FatalError("Error when writing file: " + filename );
		}
		Macro.print(3, "Wrote file : " + filename);

	}

	@Override
	public String getExtension() {
		return FORMAT;
	}
	
	static private BufferedImage PrepareRecording(FilePaintable paintableObject, String in_message){
		BufferedImage image = 
			new BufferedImage(paintableObject.getWidthInPixels(), 
					paintableObject.getHeightInPixels(),
					BufferedImage.TYPE_INT_RGB);
		Graphics2D g = image.createGraphics();
		paintableObject.paintComponentForRecording(g, in_message);
		g.dispose();
		return image;
	} 
}
