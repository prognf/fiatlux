package grafix.gfxTypes.imgFormat;

import grafix.gfxTypes.FilePaintable;

abstract public class ImgPrinter {
	
	/**
	 * 
	 * @param paintableObject
	 * @param in_filename
	 * @param in_message
	 */
	abstract public void SaveImg(
			FilePaintable paintableObject, 
			String in_filename, 
			String in_message);
	
	
	/**
	 * @return the extension of the output file
	 */
	abstract public String getExtension();

}

