package grafix.gfxTypes.imgFormat;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;

import org.apache.batik.dom.GenericDOMImplementation;
import org.apache.batik.svggen.SVGGraphics2D;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;

import grafix.gfxTypes.FilePaintable;
import main.Macro;


public class SVGPrinter extends ImgPrinter {
	
	public final String FORMAT = "SVG";
	public final String EXT = ".svg";
	
	@Override
	public void SaveImg(
			FilePaintable paintableObject,
			String in_filename,
			String in_message) {
		
		String filename = in_filename + EXT;
		File FLCfile = new File(filename);
		
		try {
			FileWriter fstream = new FileWriter(FLCfile);
			BufferedWriter out = new BufferedWriter(fstream);
			PrepareSVGRecording(paintableObject, out, in_message);
		} catch (Exception e) {
			Macro.FatalError("Error when writing file: " + filename );
		}

		Macro.print(3, "Wrote file : " + filename);
	}

	@Override
	public String getExtension() {
		return FORMAT;
	}
	
	/**
	 * @param paintableObject
	 * @param in_message
	 * @return
	 */
	private void PrepareSVGRecording(FilePaintable paintableObject, Writer out, String in_message){
		try {
			
		DOMImplementation domImpl = 
            GenericDOMImplementation.getDOMImplementation();
		
        String svgNS = "http://www.w3.org/2000/svg";
        Document document = domImpl.createDocument(svgNS, "svg", null);
		
        SVGGraphics2D g = new SVGGraphics2D(document);

		paintableObject.paintComponentForRecording(g, in_message);
		
		g.stream(out);

		} catch (Exception e) {
			e.printStackTrace();
		}
	} 
}
