package grafix.gfxTypes.imgFormat;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import grafix.gfxTypes.FilePaintable;
import main.Macro;

public class BMPPrinter extends ImgPrinter {
	
	public final String zFORMAT = "BMP";
	public final String EXT = ".bmp";
	
	@Override
	public void SaveImg(
			FilePaintable paintableObject, 
			String in_filename,
			String in_message) {

		//  image object creation and setting
		BufferedImage image = PrepareRecording(paintableObject, in_message);

		//  writing 
		String filename = in_filename + getExtension();
		BMPFile bmpfile= new BMPFile();

		bmpfile.saveBitmap(filename, image, 
				paintableObject.getWidthInPixels(), paintableObject.getHeightInPixels());
		Macro.print(3, "Wrote file : " + filename);
	}

	@Override
	public String getExtension() {
		return EXT;
	}
	
	protected BufferedImage PrepareRecording(FilePaintable paintableObject, String in_message){
		BufferedImage image = 
			new BufferedImage(paintableObject.getWidthInPixels(), 
					paintableObject.getHeightInPixels(),
					BufferedImage.TYPE_INT_RGB);
		Graphics2D g = image.createGraphics();
		paintableObject.paintComponentForRecording(g, in_message);
		g.dispose();
		return image;
	} 
}
