package grafix.gfxTypes;

import grafix.gfxTypes.elements.FLTextField;


/** TODO improve this one **/
public class StringTextField extends FLTextField {

	String m_text="";
	
	/** @param len : display length **/
	public StringTextField(int len) {
		super(len);
	}

	/** @param len : display length ;  @param initText : initial text **/
	public StringTextField(int len, String initText) {
		this(len);
		SetText(initText);
	}
	
	private void SetText(String text) {
		m_text= text;
		this.setText(text);
	}

	@Override
	/** simple set : no parsing here **/
	public void parseInput(String input) {
		m_text= input;		
	}
	
	public String GetText(){
		return m_text;
	}

}
