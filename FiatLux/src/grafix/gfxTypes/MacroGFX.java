package grafix.gfxTypes;

import javax.swing.JOptionPane;

import components.types.FLString;
import grafix.gfxTypes.imgFormat.BMPPrinter;
import grafix.gfxTypes.imgFormat.ImgFormat;
import grafix.gfxTypes.imgFormat.PNGPrinter;
import grafix.gfxTypes.imgFormat.SVGPrinter;
import main.Macro;

/******************************************
 * this class holds the graphical macro commands 
 * @author Nazim Fates
 *******************************************/ 
public class MacroGFX {
	
	/*--------------------
     *constants
    --------------------*/
    
	public final static String CLASSNAME="MacroGFX";

	public final static String COPYRIGHT= "FiatLux";
	
	/* topology labels */
	public final static String 
	HOLE_RATE= "hole ratio (%)", 
	MISSINGLINK_RATE="missingLink ratio (%)",
	S_INERT_RATE= "initial-1 (%)",
	S_RNDLNK_RATE="random rewiring ratio",
	S_ADDLINK="add link";
	
	public final static String 
	TORICALWORLD="Torical", 
	LIMITEDWORLD="Bounded",
	FLSIMULATION="FiatLux Simulation";
	
	public static final String NOTITLE= "NO TITLE"; // used for window constructors

	static public void Save2Dimage(
			FilePaintable paintableObject,
			String in_filename,
			String message,
			ImgFormat format){
		
		switch(format){
		case SVG:
			new SVGPrinter().SaveImg(paintableObject, in_filename, message);
			break;
		case PNG:
			new PNGPrinter().SaveImg(paintableObject, in_filename, message);
			break;
		case BMP:
			new BMPPrinter().SaveImg(paintableObject, in_filename, message);
			break;
		default:
			Macro.FatalError("Save : unknown record format");	
		}
	}

	/** reads a String with a dialog window */
	public static String ReadStringInWindow(String inputText) {
		return JOptionPane.showInputDialog(inputText);
	}

	/** reads an integer with a dialog window 
	 * returns CANCEL VALUE if the user has pressed CANCEL */
	public static int ReadIntegerInWindow() {
		int val = 0;
		boolean success= false;
		do {
			//Ask user to choose problem type
		String intString = ReadStringInWindow("Enter integer: ");
		try {
			if (intString != null){
				val = FLString.String2Int(intString);
			} else { // CANCEL PRESSED ! 
				// do nothing !
			}
			success= true;
		} catch (NumberFormatException e){
			Macro.UserWarning("Couldnot convert string: [" + intString + "] to int");
		}
		} 
		while (!success);
		return val;
	}

	
}
