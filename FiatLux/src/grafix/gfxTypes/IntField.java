package grafix.gfxTypes;

import grafix.gfxTypes.controllers.IntController;
import grafix.gfxTypes.elements.FLActionButton;


/*--------------------
 * graphical object that holds an integer passively * 
 * @author Nazim Fates
*--------------------*/

public class IntField extends IntController {
	/*--------------------
	 * attributes
	 --------------------*/

	private static final int ZERO = 0, DEFLEN=3;
	int m_intValue;

	/*--------------------
	 * construction
	--------------------*/

	public IntField(String in_str, int in_textlen, int defaultvalue) {
		super(in_str, in_textlen);
		m_intValue= defaultvalue;
		Update();
	}

	public IntField(String in_str, int in_textlen) {
		this(in_str, in_textlen, ZERO);
	}

	public IntField(String in_str) {
		this(in_str, DEFLEN, ZERO);
	}

	/** adds + and - controls **/
	public void AddPlusMinusButtons(){
		Add( new PlusButton(), new MinusButton() );
	}

	/*--------------------
	 * implementations
	--------------------*/

	@Override
	protected int ReadInt() {
		return m_intValue;
	}

	@Override
	protected void SetInt(int val) {
		m_intValue= val;
	}



	/*--------------------
	 * get / set
	--------------------*/

	public int GetValue(){
		return m_intValue;
	} 

	/* setting the value "from outside" and updating display */
	public void SetValue(int val) {
		SetInt(val);
		Update();
	}

	/* doubles current value */
	public void Mult2() {
		m_intValue *= 2;
		Update();
	}

	/* divides by 2 current value */
	public void Div2() {
		m_intValue /= 2;
		Update();
	}

	/* ------------------------ GFX ++  ----------------*/

	class PlusButton extends FLActionButton {

		public PlusButton() {
			super("+");
		}

		@Override
		public void DoAction() {
			m_intValue++;
			Update();
		}

	}

	class MinusButton extends FLActionButton {

		public MinusButton() {
			super("-");
		}

		@Override
		public void DoAction() {
			m_intValue--;
			Update();
		}

	}

}
