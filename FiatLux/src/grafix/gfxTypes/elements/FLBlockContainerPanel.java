package grafix.gfxTypes.elements;

import static java.awt.GridBagConstraints.FIRST_LINE_START;

import java.awt.Component;

import javax.swing.JComponent;

/* N. Gauville */
public class FLBlockContainerPanel extends FLPanel {
	private int lastId = 0;

	public FLBlockContainerPanel() {
		super();

		SetGridBagLayout();
		GetGridBagC().insets.set(5, 5, 5, 5);
		GetGridBagC().weighty = 0.5;
		GetGridBagC().ipadx = 15;
		GetGridBagC().ipady = 15;
		SetAnchor(FIRST_LINE_START);
	}

	public void addBlock(JComponent panel, int x, int y) {
		AddGridBag(panel, x, y);

		if (panel instanceof FLBlockPanel) {
			((FLBlockPanel) panel).setContainer(this);
		}
	}

	public void updateContainer(FLBlockPanel source) {
		// Adapt the window size to the new size
		Component c = this;
		while (c.getParent() != null) {
			c = c.getParent();
		}
		c.setSize(c.getPreferredSize());

		// Auto hide panels mode (disabled until user preferences option)
		/*int maxHeight = 550;

		if (getHeight() > maxHeight) {
			for (int i = 1, j = getComponentCount(); i < j; i++) {
				if (getComponent(i) instanceof FLBlockPanel && getComponent(i) != source) {
					((FLBlockPanel) getComponent(i)).hideContent();
					doLayout();
					if (getHeight() <= maxHeight) {
						break;
					}
				}
			}
		}*/
	}
}
