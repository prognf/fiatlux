package grafix.gfxTypes.elements;

import javax.swing.JComponent;
import javax.swing.JTabbedPane;

import main.Macro;



/*--------------------
 * extends JTabbedPane
 * 
 * @author Nazim Fates
*--------------------*/


public class FLTabbedPane extends JTabbedPane {
	/*--------------------
	 * attributes
	 --------------------*/
	
	/*--------------------
	 * construction
	 --------------------*/
		
	/*public FLTabbedPane(int tabPlacement, int tabLayoutPolicy) {
		super(tabPlacement, tabLayoutPolicy);
	}

	public FLTabbedPane(int tabPlacement) {
		super(tabPlacement);
	}*/

	public FLTabbedPane() {
		super();
	}
	
	public FLTabbedPane(String [] in_title, JComponent [] in_tab){
		for (int i=0; i < in_title.length; i ++){
			this.AddTab(in_title[i], in_tab[i]);
		}
	}
	
	/*--------------------
	 * actions
	 --------------------*/
	
	public void AddTab(String in_title, JComponent in_tab){
		addTab(in_title, in_tab);
	}
	
	/*--------------------
	 * get & set
	 --------------------*/
	
	/** override of getSelectedIndex **/
	public int GetSelectedTabIndex(){
		return getSelectedIndex();
	}
	
	public void SetSelectedIndex(int selectIndex) {
		setSelectedIndex(selectIndex);		
	}
	
	/*--------------------
	 * test
	 --------------------*/
	
	public static void DoTest() {

		Macro.BeginTest("FLTabbedPane");
		FLTabbedPane test  = new FLTabbedPane();
		FLPanel tryP= new FLPanel();
		test.addTab("Tab 1", tryP);
		
		FLButton button= new FLButton("koukou");
		test.addTab("Tab 2", button);
		
		FLFrame frame= new FLFrame("test");
		frame.addPanel(test);
		frame.packAndShow();

		Macro.print(" yop");
		Macro.EndTest();
	}
}
