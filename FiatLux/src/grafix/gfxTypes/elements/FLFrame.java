package grafix.gfxTypes.elements;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.ScrollPaneConstants;

import main.Macro;



/*--------------------
 * Extension of JFrame, window management
 * 
 * @author Nazim Fates
*--------------------*/

public class FLFrame extends JFrame {	

	final static String CLASSNAME= Thread.currentThread().getStackTrace()[1].getClassName();

	/*----------------------------------------------------------------------------
	  - attributes
	  --------------------------------------------------------------------------*/

	/*---------------------------------------------------------------------------
	  - construction
	  --------------------------------------------------------------------------*/
	
	public FLFrame(String in_str) {
		super(in_str);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); 
		// close window if clicked
	}
	
	/** creates a frame and shows it **/
	public static FLFrame NewFrame(String label, JComponent panel) {
		FLFrame frame= new FLFrame(label);
		frame.addPanel(panel);
		frame.packAndShow();
		frame.AddExitSystemWhenClosedFeature();
		return frame;
	}
	
	/*---------------------------------------------------------------------------
	  - actions
	  --------------------------------------------------------------------------*/
	/** packs & displays the window */
	public void packAndShow(){
		pack();
		setVisible(true);
	}
	
	/** packs & displays the window */
	public void Show(){
		setVisible(true);
	}

	/** closes the current window */
	protected void CloseWindow(){
		dispose();
	}

	/*---------------------------------------------------------------------------
	  - how to use this class
	  --------------------------------------------------------------------------*/
	/** adding a component to a frame necessitates this.getContentPane() **/
	final public void addPanel(JComponent panel) {
		getContentPane().add(panel);
	}

	final public void AddPanelWithScrollPane(FLPanel panel) {
		FLScrollPane scrollpane = new FLScrollPane(panel);
		//scrollpane.HideHorizontalScrollBar();
		scrollpane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		getContentPane().add(scrollpane);
	}

	/** close this window = exit system */
	final public void AddExitSystemWhenClosedFeature() {
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				Macro.systemExit();
			}
		});
	}
	
	public FLActionButton GetCloseButton(String closeLabel){
		return new FLActionButton(closeLabel) {
			@Override
			public void DoAction() {
				CloseWindow();
			}
		};
	}
	
	/*---------------------------------------------------------------------------
	  - test
	  --------------------------------------------------------------------------*/
	public static void DoTest(){
		Macro.BeginTest(CLASSNAME);
		Macro.EndTest();
	}
	
	
	
	
	public static void main(String [] argv){
		DoTest();
	}
}
