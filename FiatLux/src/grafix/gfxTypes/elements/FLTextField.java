package grafix.gfxTypes.elements;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JTextField;

import components.types.FLString;
import main.Macro;



/************************************
 * active JTextField
 * @author Nazim Fates
 *******************************************/ 

/* actionlistener is for detecting "Enter"
 * focuslistener is for detecting lost focus
 * ParseInput is triggered when losing focus or when Enter is pressed
 */
public abstract class FLTextField extends JTextField 
implements FocusListener, ActionListener {	
	
	// this is to prevent double messages when focus is lost because 
	// of an action in the parsing process
	// change this with thread management ?
	boolean m_FocusListeningOn = true;
	
	/* ParseInput is triggered when losing focus or when Enter is pressed */
	abstract public void parseInput(String input);
		
	/* empty TextField with given length */ 
	protected FLTextField(int in_len){
		super(in_len);
		this.addFocusListener(this);
		this.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent e) {
		String input = getText();
		try {
			m_FocusListeningOn= false;
			parseInput(input);
		} catch (Exception eListener) {
			Macro.print(1, this, "Parse failed :" + input);
		}
		m_FocusListeningOn= true;
	}
	
	public void focusGained(FocusEvent e) {
	//	Macro.print("Gained");
	}
	
	/** WARNING : this may cause "parseInput" to be called twice... **/
	public void focusLost(FocusEvent e) {
		// Macro.Debug("focus:" + e.paramString());
		if (m_FocusListeningOn){
			parseInput(getText());		
		}
	}
	
	final public void Clear(){
		setText(FLString.EMPTYSTRING);
	}
	
}
