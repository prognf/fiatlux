package grafix.gfxTypes.elements;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.border.Border;

import components.randomNumbers.FLRandomGenerator;
import components.types.DoublePar;
import components.types.IntPar;
import components.types.ProbaPar;
import main.Macro;

//import Resources.RandomNumbers.FLRandomGenerator;

/*--------------------
 * FLPanel is a wrapper class for JPanel
 *
 * @author Nazim Fates
*--------------------*/

public class FLPanel extends JPanel {
	// attribute
	GridBagConstraints m_GridBagC;

	public boolean contentVisible = true;

	public static int PANEL_SIZE = 250;
	public static Dimension REDUCED_DIMENSIONS = new Dimension(PANEL_SIZE, 40);

	/*---------------------------------------------------------------------------
	  - ADD functions
	  --------------------------------------------------------------------------*/
	public void Add(final Component comp) {
		if (comp != null) {
			add(comp);
		}
	}

	public void Add(final Object... panelList) {
		for (Object panel : panelList) {
			if (panel instanceof JComponent){
				add((Component) panel);
			} else if (panel instanceof IntPar){
				add(((IntPar) panel).GetControl());
			}else if (panel instanceof ProbaPar){
				add(((ProbaPar) panel).GetControl());
			} else if (panel instanceof DoublePar){
				add(((DoublePar) panel).GetControl());
			} else if (panel instanceof FLRandomGenerator){
				add(((FLRandomGenerator) panel).GetSeedControl());
			} else {
				if (panel==null) {
					Macro.SystemWarning(" null panel sent");
				} else {
				Macro.SystemWarning("add::unrecognized type of component: " +
						panel.getClass().getCanonicalName());
				}
			}

		}
	}

	public void AddIfNotEmpty(FLPanel panel) {
		if (panel!=null){
			Add(panel);
		}
	}


	// NEW
	/*
	 * public void AddColoredLegend(String in_legend, FLColor color) {
	 * AddSeparator(); FLLabel lab = new FLLabel(in_legend) ;
	 * lab.setOpaque(true); lab.setBackground(color) ; //lab.setSize(50,500);
	 * add(lab); //add( new FLLabel(in_legend)); AddSeparator(); }
	 */

	public void AddBlackBorder() {
		Border cadre = BorderFactory.createLineBorder(Color.black);
		setBorder(cadre);
	}

	/** for use with GridBagLayout 
	 * use SetGridBagLayout() at construction */
	public void AddGridBag(final JComponent component, final int x, final int y) {
		m_GridBagC.gridx = x;
		m_GridBagC.gridy = y;
		add(component, m_GridBagC);
	}

	/** for use with GridBagLayout 
	 * use SetGridBagLayout() at construction */
	public void AddGridBagLabel(final FLLabel label) {
		SetAnchor(GridBagConstraints.PAGE_START);
		m_GridBagC.weighty = 0.9; // request any extra vertical space
		m_GridBagC.gridwidth = 2; // centering
		AddGridBag(label, 0, 0);

		GetGridBagC().gridwidth = 1; // reset par
		GetGridBagC().weighty = 0.5; // reset par
	}

	public void AddGridBagY(final JComponent ... panel){
		SetGridBagLayout();
		m_GridBagC.weightx = 0.5;
		m_GridBagC.weighty = 0.5;
		int y = 10;
		for (JComponent component:panel){
			AddGridBag(component, 0, y);
			y++;
		}
	}

	/** for decorating */
	public void AddLegend(final String in_legend) {
		AddSeparator();
		add(new FLLabel(in_legend));
		AddSeparator();
	}

	public void AddScrollPane(final JTextArea m_ModelInfo) {
		// this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		JScrollPane scBar = new JScrollPane(m_ModelInfo);
		this.add(scBar, BorderLayout.SOUTH);
	}

	/** adds a horizontal separator */
	public void AddSeparator() {
		add(new JSeparator());
	}

	protected void AddTwoSubPanelsLeftRight(final FLPanel panel1, final FLPanel panel2) {
		SetBoxLayoutX();
		panel1.setAlignmentY(0);
		panel1.setAlignmentY((float) 0.7);
		add(panel1);
		add(panel2);
	}

	/** adds a vertical separator */
	public void AddVerticalSeparator() {
		add(new JSeparator(SwingConstants.VERTICAL));
	}

	/** adds an empty label (hack) */
	public void AddVerticalSpace() {
		add(new FLLabel(" "));
	}

	/*---------------------------------------------------------------------------
	  - LABELS
	  --------------------------------------------------------------------------*/
	/** for decoration */
	public void AddLabel(final String in_lABEL) {
		Add(new FLLabel(in_lABEL));
	}

	/** adds to the panel a label and list situated below
	 * **/
	public void AssociateLabelAndList(final String in_label, final JComponent in_list) {
		FLPanel couple = JoinLabelAndList(in_label, in_list);
		add(couple);
	}

	public static FLPanel JoinLabelAndList(final String in_label, final JComponent in_list){
		return JoinLabelAndList(in_label, in_list, false);
	}

	/** returns a panel formed by a label and list situated below
	 * **/
	public static FLPanel JoinLabelAndList(final String in_label, final JComponent in_list, boolean horizontal) {
		FLPanel couple = new FLPanel();
		couple.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		couple.add(new FLLabel(in_label), c);
		c.gridx = horizontal?1:0;
		c.gridy = horizontal?0:1;
		couple.add(in_list, c);
		return couple;
	}

	/*---------------------------------------------------------------------------
	  - GRID BAGS
	  --------------------------------------------------------------------------*/
	public GridBagConstraints GetGridBagC() {
		return m_GridBagC;
	}

	public void SetAnchor(final int anchorVal) {
		if (m_GridBagC == null) {
			SetGridBagLayout();
		}

		m_GridBagC.anchor = anchorVal;
	}

	/* sets layout to horizontal */
	public void SetBoxLayoutX() {
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
	}

	/* sets layout to vertical */
	public void SetBoxLayoutY() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
	}
	
	
	/** sets layout to Grid Bag */
	public void SetGridBagLayout() {
		setLayout(new GridBagLayout());
		m_GridBagC = new GridBagConstraints();
	}

	/** sets layout to Border **/
	public void SetBorderLayout(){
		setLayout(new BorderLayout());
	}

	/*---------------------------------------------------------------------------
	  - OTHERS
	  --------------------------------------------------------------------------*/

	/** replaces an old content by a new one */
	public void SetNewPanel(final FLPanel newPanel) {
		removeAll();
		validate(); // IMPORTANT
		add(newPanel);
		validate(); // IMPORTANT
	}

	/** creates an empty panel **/
	public static FLPanel NewPanel() {
		FLPanel p = new FLPanel();
		return p;
	}

	public static FLPanel NewPanel(final JComponent... panel) {
		FLPanel p = new FLPanel();
		for (JComponent current : panel){
			p.Add(current);
		}
		return p;
	}

	/** in input each object is added depending on its type
	 * USE with care
	 */
	public static FLPanel NewPanelConstruct(final Object... panel) {
		FLPanel p = new FLPanel();
		for (Object current : panel){

			p.Add(current);
		}
		return p;
	}

	/** new panel with vertically aligned components */
	public static FLPanel NewPanelVertical(final JComponent... panel) {
		FLPanel p = new FLPanel();
		p.SetBoxLayoutY();
		p.Add(panel);
		return p;
	}

	public static FLPanel NewPanel(FLPanel panel, FLRandomGenerator randomizer) {
		return NewPanel(panel, randomizer.GetSeedControl());
	}

	public void AddRand(FLPanel panel, FLRandomGenerator randomizer) {
		FLPanel pseed= new FLPanel();
		//pseed.SetGridBagLayout();
		//pseed.GetGridBagC().fill=
			//GridBagConstraints.HORIZONTAL;
		// left- right max dist.
		panel.setAlignmentX(LEFT_ALIGNMENT);
		pseed.Add(panel);
		FLPanel seedControl = randomizer.GetSeedControl();
		seedControl.setAlignmentY(RIGHT_ALIGNMENT);
		pseed.add(seedControl);
		Add(pseed);
	}

	public void AddRand(FLRandomGenerator randomizer) {
		Add(randomizer.GetSeedControl());
	}

	public static FLPanel NewPanel(DoublePar par, FLRandomGenerator randomizer) {
		/*FLPanel p = new FLPanel();
		p.SetGridBagLayout();
		//p.GetGridBagC().fill=
			//	GridBagConstraints.HORIZONTAL;
		p.AddGridBag(par.GetControl(), 1, 1);
		//p.Add(new FLLabel("kikou"));
		p.AddGridBag(randomizer.GetSeedControl(), 2, 1);

		return p;*/
		return NewPanel(par.GetControl(), randomizer.GetSeedControl());
	}

	public static FLPanel NewPanel(IntPar par,
			FLRandomGenerator randomizer) {
		return NewPanel(par.GetControl(), randomizer.GetSeedControl());
	}

	/** constructing a list of ProbaPar
	 */
	public static FLPanel NewPanel(ProbaPar ... probaParArray){
		FLPanel p = new FLPanel();
		for(ProbaPar param : probaParArray){
			p.Add(param.GetControl());
		}
		return p;
	}

	/** shows this panel independently in a new frame **/
	public void showInNewFrame(String frameTitle) {
		FLFrame frame= new FLFrame(frameTitle);
		frame.add(this);
		frame.packAndShow();
	}
	
}
