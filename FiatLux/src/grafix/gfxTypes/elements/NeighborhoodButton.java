package grafix.gfxTypes.elements;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JComponent;

import grafix.gfxTypes.FLColor;
import topology.basics.LinearTopology;
import topology.basics.PlanarTopology;
import topology.basics.SuperTopology;

/** selection of topology by Nicolas Gauville */
public class NeighborhoodButton extends FLButton {
    private SuperTopology m_topology;
    private boolean selected = false;
    private boolean m_hover = false;
    private boolean m_clicked = false;

    public NeighborhoodButton(SuperTopology topology) {
        super("");

        m_topology = topology;

        JComponent n, l;
        FLPanel content = FLPanel.NewPanelVertical(n = new NeighborhoodIcon(topology, 40, 100), l = new FLLabel(topology.GetName()));
        n.setAlignmentX(CENTER_ALIGNMENT);
        l.setAlignmentX(CENTER_ALIGNMENT);
        add(content);
        setMargin(new Insets(1, 1, 1, 1));
        content.setOpaque(false);

        setBackground(Color.WHITE);
        setPreferredSize(new Dimension(100, 65));
        setBorderPainted(false);
        setFocusPainted(false);
        setCursor(new Cursor(Cursor.HAND_CURSOR));

        addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
                m_clicked = true;
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                m_clicked = false;
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                m_hover = true;
            }

            @Override
            public void mouseExited(MouseEvent e) {
                m_hover = false;
            }
        });
    }

    public PlanarTopology getTopology() {
        return (PlanarTopology) m_topology;
    }

    public LinearTopology getLinearTopology() {
        return (LinearTopology) m_topology;
    }

    public void selectButton() {
        selected = true;
        repaint();
    }

    public void unselectButton() {
        selected =  false;
        repaint();
    }

    @Override
    public void paintComponent(Graphics g) {
        int width = getWidth() - 1;
        int height = getHeight() - 1;
        int arc = 20;

        Graphics2D graphics = (Graphics2D) g;

        Color background1a = new Color(220, 255, 220);
        Color background1b = (new FLColor(new Color(220, 255, 220))).darker(0.9);

        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics.clearRect(0, 0, width, height);

        if (selected) {
            graphics.setPaint(new GradientPaint(0, 0, background1a, 0, height, background1b, false));
            graphics.fillRoundRect(0, 0, width, height, arc, arc);
            graphics.setColor(FLColor.c_green.brighter());
            graphics.setStroke(new BasicStroke(1));
            graphics.drawRoundRect(0, 0, width, height, arc, arc);
            graphics.setStroke(new BasicStroke());
        } else if (m_hover) {
            graphics.setColor(FLColor.c_white.darker(0.9));
            graphics.fillRoundRect(0, 0, width, height, arc, arc);
        }
    }

}