package grafix.gfxTypes.elements;

import java.awt.Font;

import javax.swing.JLabel;

public class FLLabel extends JLabel {

	public enum typeFont { NORMAL, MEDIUM, BIG }

    public FLLabel(String label, typeFont in_fontType){
		super(label);
		switch (in_fontType) {
			case BIG:
				Font f = new Font("SansSerif", Font.BOLD, 20);
				this.setFont(f);
				break;
			case MEDIUM:
				this.setFont(new Font("SansSerif", Font.BOLD, 14));
				break;
		}
	}

	public FLLabel(String label) {
		super(label);
	}

	public static FLLabel newObject(String text) {
		return new FLLabel(text);
	}
	
}
