package grafix.gfxTypes.elements;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JPanel;

import components.types.IntegerList;
import grafix.gfxTypes.FLColor;
import topology.basics.DXDYTable;
import topology.basics.LinearTopology;
import topology.basics.PlanarTopology;
import topology.basics.SuperTopology;
import topology.zoo.CustomLinearTopology;

/*** class coded by Nicolas Gauville **/
public class NeighborhoodIcon extends JPanel {
    private static final int MAXNEIGHBSIZE = 50, MAXNEIGHBPOS=MAXNEIGHBSIZE/2;
	private int size, xSize, ySize, nbCells, cellSize, decal;
    private int[][] cells;
    private int cx, cy;
    private String name;
    private Color color;

    public NeighborhoodIcon(SuperTopology topology, int pSize, int prSize) {
        super();

        name = topology.GetName();
        color = FLColor.c_lightgreen;
        /// Automatically creates ICON
        if (topology instanceof LinearTopology) {
        	if (!( topology instanceof CustomLinearTopology)) {//don't do this for Custom1D
            initLinearTopology((LinearTopology) topology, pSize, prSize);
        	}
        } else if (topology instanceof PlanarTopology) {
            initPlanarTopology((PlanarTopology) topology, pSize, prSize);
        }
    }

    /** TODO : examine this **/
    public void initLinearTopology(LinearTopology topology, int pSize, int prSize) {
        DXDYTable table = new DXDYTable();
        topology.SetSize(MAXNEIGHBSIZE);
        IntegerList indices = topology.GetNeighbourhoodTable(MAXNEIGHBPOS);
        for (Integer i : indices) {
            table.Add(i, 0);
        }

        cx = MAXNEIGHBPOS;
        cy = 0;

        init(table, pSize, prSize);
    }

    public void initPlanarTopology(PlanarTopology topology, int pSize, int prSize) {
        DXDYTable table = topology.GetDXDYTable(0, 0);

        cx = cy = 0;

        init(table, pSize, prSize);
    }

    /** take the table from topology and builds the "cells" array
     * which will be used by the component to draw the graphical item (icon) */
    private void init(DXDYTable table, int pSize, int prSize) {
        size = pSize;
        decal = (int) ((double) (prSize - pSize) / 2.0);

        cells = new int[table.GetSize()][2];
        nbCells = table.GetSize();

        int minX = Integer.MAX_VALUE, minY = Integer.MAX_VALUE;
        int maxX = Integer.MIN_VALUE, maxY = Integer.MIN_VALUE;

        for (int i = 0; i < table.GetSize(); i++) {
            cells[i][0] = table.GetX(i);
            cells[i][1] = table.GetY(i);

            if (table.GetX(i) < minX) {
                minX = table.GetX(i);
            }

            if (table.GetX(i) > maxX) {
                maxX = table.GetX(i);
            }

            if (table.GetY(i) < minY) {
                minY = table.GetY(i);
            }

            if (table.GetY(i) > maxY) {
                maxY = table.GetY(i);
            }
        }

        while (hasNegativeIndex(0)) {
            increaseIndex(0);
        }

        while (hasNegativeIndex(1)) {
            increaseIndex(1);
        }

        int minIndex = getMinIndex(0);
        if (minIndex > 0) {
            decreaseIndex(0, minIndex);
        }

        xSize = 1 + maxX - minX;
        ySize = 1 + maxY - minY;
        cellSize = (int) ((double) size / (double) Math.max(xSize, ySize));

        // reverse top/bottom
        for (int i = 0; i < nbCells; i++) {
            cells[i][1] = (ySize - 1) - cells[i][1];
        }
        cy = (ySize - 1) - cy;

        setPreferredSize(new Dimension(size + decal, size + 4));
        setSize(size + decal + 4, size + 4);
        setBounds(0, 0, size + decal + 4, size + 4);
        setOpaque(false);
    }

    @Override
    public void paintComponent(Graphics g) {
        Graphics2D graphics = (Graphics2D) g;
        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        if (name.equals("Hexagonal")) {
            int hexagonRadius = (int) ((double) size / 2.5);
            int x = size;
            int y = size / 2;

            int[] px = new int[6];
            int[] py = new int[6];

            for (int i = 0; i < 6; i++) {
                px[i] = (int) (x + hexagonRadius * Math.cos(2.0 * Math.PI * ((double) i / 6.0)));
                py[i] = (int) (y + hexagonRadius * Math.sin(2.0 * Math.PI * ((double) i / 6.0)));
            }

            graphics.setColor(color);
            graphics.fillPolygon(px, py, 6);
            graphics.setColor(Color.RED);
            graphics.setStroke(new BasicStroke(2));
            graphics.drawPolygon(px, py, 6);
        } else if (name.equals("RandomMoore")) {
            int hCellSize = (int) ((double) size / 5.0);
            for (int x = 0; x < 5; x++) {
                for (int y = 0; y < 5; y++) {
                    int px = x * hCellSize + decal + 2;
                    int py = y * hCellSize + 2;

                    Color color = new Color(FLColor.c_lightblue.getRed(), FLColor.c_lightblue.getGreen(), FLColor.c_lightblue.getBlue(), (int) (Math.random() * 200.0));
                    graphics.setColor(color);
                    graphics.fillRoundRect(px, py, hCellSize, hCellSize, 0, 0);
                    graphics.setColor(Color.BLACK);
                    graphics.setStroke(new BasicStroke(1));
                    graphics.drawRoundRect(px, py, hCellSize, hCellSize, 0, 0);
                    graphics.setStroke(new BasicStroke());
                }
            }

            graphics.setColor(Color.RED);
            graphics.setStroke(new BasicStroke(2));
            graphics.drawRoundRect(2 * hCellSize + decal + 2, 2 * hCellSize + 2, hCellSize, hCellSize, 0, 0);
        } else if (name.equals("Margolus")) {
            int hCellSize = (int) ((double) size / 2.2);
            for (int x = 0; x < 2; x++) {
                for (int y = 0; y < 2; y++) {
                    int px = x * hCellSize + decal + 2;
                    int py = y * hCellSize + 2;

                    graphics.setColor(color);
                    graphics.fillRoundRect(px, py, hCellSize, hCellSize, 0, 0);
                    graphics.setColor(Color.BLACK);
                    graphics.setStroke(new BasicStroke(1));
                    graphics.drawRoundRect(px, py, hCellSize, hCellSize, 0, 0);
                    graphics.setStroke(new BasicStroke());
                }
            }

            graphics.setColor(Color.RED);
            graphics.setStroke(new BasicStroke(2));
            graphics.drawRoundRect(decal + 2, 2, hCellSize * 2, hCellSize * 2, 0, 0);
        } else if (name.equals("Circular")) {
            int hCellSize = (int) ((double) size / 5.0);

            graphics.setColor(Color.BLACK);
            graphics.setStroke(new BasicStroke(1));

            for (int x = 0; x < 5; x++) {
                for (int y = 0; y < 5; y++) {
                    int px = x * hCellSize + decal + 2;
                    int py = y * hCellSize + 2;

                    graphics.drawRoundRect(px, py, hCellSize, hCellSize, 0, 0);
                }
            }

            int circleRadius =  (int) ((double) hCellSize * 3.5);
            int circleX =  (int) (2.5 * hCellSize + decal + 2 - circleRadius * 0.5);
            int circleY =  (int) (2.5 * hCellSize + 2 - circleRadius * 0.5);
            int lineWidth =  (int) ((double) circleRadius * 0.5);

            graphics.setColor(Color.RED);
            graphics.setStroke(new BasicStroke(2));
            graphics.drawOval(circleX, circleY, circleRadius, circleRadius);
            graphics.setColor(FLColor.c_superRed);
            graphics.drawLine(circleX + lineWidth, circleY + lineWidth, circleX + 2 * lineWidth, circleY + lineWidth);
        } else { // GENERAL CASE
            for (int i = 0; i < nbCells; i++) {
                int px = cells[i][0] * cellSize + decal + 2;
                int py = cells[i][1] * cellSize + 2;

                graphics.setColor(color);
                graphics.fillRoundRect(px, py, cellSize, cellSize, 0, 0);
                graphics.setColor(Color.BLACK);
                graphics.setStroke(new BasicStroke(1));
                graphics.drawRoundRect(px, py, cellSize, cellSize, 0, 0);
                graphics.setStroke(new BasicStroke());
            }

            graphics.setColor(Color.RED);
            graphics.setStroke(new BasicStroke(2));
            graphics.drawRoundRect(cx * cellSize + decal + 2, cy * cellSize + 2, cellSize, cellSize, 0, 0);
        }
    }

    private boolean hasNegativeIndex(int coord) {
        if ((coord == 0 && cx < 0) || (coord == 1 && cy < 0)) {
            return true;
        }

        for (int i = 0; i < nbCells; i++) {
            if (cells[i][coord] < 0) {
                return true;
            }
        }

        return false;
    }

    private void increaseIndex(int coord) {
        if (coord == 0) {
            cx++;
        } else if (coord == 1) {
            cy++;
        }

        for (int i = 0; i < nbCells; i++) {
            cells[i][coord]++;
        }
    }

    private void decreaseIndex(int coord, int amount) {
        if (coord == 0) {
            cx -= amount;
        } else if (coord == 1) {
            cy -= amount;
        }

        for (int i = 0; i < nbCells; i++) {
            cells[i][coord]-= amount;
        }
    }

    private int getMinIndex(int coord) {
        int min = (coord == 0 ? cx : cy);
        for (int i = 0; i < nbCells; i++) {
            if (cells[i][coord] < min) {
                min = cells[i][coord];
            }
        }

        return min;
    }
}