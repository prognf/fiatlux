package grafix.gfxTypes.elements;

import javax.swing.JSlider;

/*--------------------
 * other name for JSlider
 * 
 * @author Bryszkowski Alexandre
*--------------------*/

public class FLSlider extends JSlider{

	private FLSlider() {
		super();
	}
}

