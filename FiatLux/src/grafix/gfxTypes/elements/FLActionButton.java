package grafix.gfxTypes.elements;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/* when one action only is needed */
abstract public class FLActionButton extends FLButton 
implements ActionListener {

	/* what shall we do when the button is pressed ? */
	public abstract void DoAction();
	
	
	public FLActionButton(String buttonText) {
		super(buttonText);
		AddActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		DoAction();
	}
	
}
