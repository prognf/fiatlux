package grafix.gfxTypes.elements;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JTextPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;

import grafix.gfxTypes.FLColor;

/**
 * Extended JTextPane with syntax coloration for Fiatlux
 * by N. Gauville
 */
public class FLCodePanel extends JTextPane {
    /**
     * Keywords highlighted in blue (and bold text)
     */
    final static String[] highlightBlue = {
        "public", "private", "protected", "final", "static",
        "class", "extends", "implements", "super", "import",
        "package", "return", "if", "else", "break", "switch",
        "case"
    };

    /**
     * Types, highlighted in red (and italic)
     */
    final static String[] highlightRed = {
        "String", "int", "boolean", "void", "float", "double"
    };

    /**
     * Constants, highlighted in violet (and bold text)
     */
    final static String[] highlightViolet = { // Simple Constants
         "ALIVE", "DEAD"
    };

    /**
     * Fiatlux main constants, highlighted in bold green
     */
    final static String[] highlightGreen = {
        // Topologies constants
        "Topologies", "GKL", "Moore9", "Moore8", "MooreR5", "vonNeumann", "vonNeumann5", "WCE", "Circular",
        "Margolus", "ExtendedMoore", "RandomMoore", "Hexagonal", "Sultra", "LineR1", "LineR2", "LineR3",
        "LineR4", "OuterLineR1", "Cros", "LVOORHEE", "Toom", "ExtendedToom", "ToomInv",

        // Colors
        "Colors", "WHITE", "RED", "BLUE", "GREEN", "YELLOW", "BLACK", "GRAY",

        // Bounds
        "BoundConditions", "Toric", "Free", "Inert"
    };

    /**
     * Comments syntax (in gray)
     * TODO: we can add support for multiline comment, and double-slash comments
     */
    final static String[] highlightGray = {
        "\\/\\*.*\\*\\/"
    };

    private Style m_normal, m_blue, m_red, m_green, m_gray, m_violet;
    private FLCodeLineIndicatorPane m_lines;

    public FLCodePanel() {
        super();

        m_lines = new FLCodeLineIndicatorPane();

        setDocument(new DefaultStyledDocument() {
            public void insertString(int arg0, String arg1, AttributeSet arg2) throws BadLocationException {
                super.insertString(arg0, arg1, arg2);
                updateHighlight();
            }

            public void remove(int arg0, int arg1) throws BadLocationException {
                super.remove(arg0, arg1);
                updateHighlight();
            }
        });

        initStyles();

        addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {}

            @Override
            public void keyPressed(KeyEvent e) {}

            @Override
            public void keyReleased(KeyEvent e) {
                updateHighlight();
            }
        });
    }

    public FLCodeLineIndicatorPane getLines() {
        return m_lines;
    }

    @Override
    public void setText(String text) {
        super.setText(text);
        updateLines();
    }

    private void initStyles() {
        DefaultStyledDocument doc = (DefaultStyledDocument) getDocument();

        m_normal = doc.addStyle("normal", null);
        m_blue = doc.addStyle("blue", null);
        m_red = doc.addStyle("red", null);
        m_green = doc.addStyle("green", null);
        m_gray = doc.addStyle("gray", null);
        m_violet = doc.addStyle("violet", null);

        StyleConstants.setForeground(m_blue, FLColor.c_darkblue);
        StyleConstants.setForeground(m_red, FLColor.c_darkred);
        StyleConstants.setForeground(m_green, FLColor.c_darkgreen);
        StyleConstants.setForeground(m_gray, Color.gray);
        StyleConstants.setForeground(m_violet, Color.MAGENTA.darker());
        StyleConstants.setBold(m_blue, true);
        StyleConstants.setBold(m_green, true);
        StyleConstants.setBold(m_violet, true);
        StyleConstants.setItalic(m_red, true);
    }

    private void updateHighlight() {
        removeHighlights();
        highlightWords(highlightBlue, m_blue);
        highlightWords(highlightGreen, m_green);
        highlightWords(highlightRed, m_red);
        highlightWords(highlightGray, m_gray);
        highlightWords(highlightViolet, m_violet);
    }

    private void removeHighlights() {
        try {
            DefaultStyledDocument doc = (DefaultStyledDocument) getDocument();
            String text = doc.getText(0, doc.getLength());
            doc.setCharacterAttributes(0, text.length() - 1, m_normal, true);
        } catch (BadLocationException e) {
        }
    }

    private void highlightWords(String[] words, Style style) {
        try {
            DefaultStyledDocument doc = (DefaultStyledDocument) getDocument();
            String text = doc.getText(0, doc.getLength());

            for (String word : words) {
                Pattern pattern = Pattern.compile(word, Pattern.MULTILINE);
                Matcher matcher = pattern.matcher(text);

                while (matcher.find()) {
                    doc.setCharacterAttributes(matcher.start(), matcher.end() - matcher.start(), style, false);
                }
            }

            updateLines();
        } catch (BadLocationException e) {
        }
    }

    public void updateLines() {
        m_lines.update(this, getText().split(System.getProperty("line.separator")).length);
    }
}
