package grafix.gfxTypes.elements;

import javax.swing.JList;
import javax.swing.event.ListSelectionListener;

/*--------------------
 * allows the selection of a string in a list
--------------------*/
public class FLList extends JList {

	final static int ZERO=0;
	/*--------------------
	 * Attributes
	 --------------------*/

	/*--------------------
	 *  Constructor
	 --------------------*/

	/** "final" constructor */
	public FLList(String[] in_list, int initialSelectionRank) throws IllegalArgumentException {
		super(in_list);

		if (initialSelectionRank >= getModel().getSize()){
			IllegalArgumentException e = 
					new IllegalArgumentException(
							" selected index out of range: " + initialSelectionRank );
			this.setSelectedIndex(0);
			throw e;
		} else{
			this.setSelectedIndex(initialSelectionRank); 
		}		
	}

	

	public FLList(String[] in_list, ListSelectionListener in_listener){
		this(in_list);
		this.addListSelectionListener(in_listener);
	}

	public FLList(String[] in_list, int InitialSelectionRank, ListSelectionListener in_listener) throws Exception {
		this(in_list,InitialSelectionRank);
		this.addListSelectionListener(in_listener);
	}

	public FLList(String[] in_list) {
		this(in_list,ZERO); // by default first item is selected
	}


	public FLList(Object[] values) {
		this(values,ZERO);
	}

	public FLList(Object[] values, int InitialSelectionRank) {
		this(ExtractString(values),InitialSelectionRank);
	}

	/** applies the toString method to all elements */
	private static String[] ExtractString(Object[] values) {
		int size= values.length;
		String [] data = new String [size];
		for (int i=0; i< size; i++){
			data[i]= values[i].toString();
		}
		return data;
	}

	/*--------------------
	 * Get & Set 
	 --------------------*/

	/** MAIN USE **/
	public int GetSelectedItemRank() {
		return this.getSelectedIndex();
	}

	/** may return null if no item is selected **/
	public String GetSelectedItemName(){
		String s= (String)this.getSelectedValue(); 
		return s;
	} 

	/* test this ! */
	/*public int GetListSize(){
		return getLastVisibleIndex();
	}*/

	public void SelectItem(int select){
		this.setSelectedIndex(select);
	}

	/*--------------------
	 * use in subclasses
	 --------------------*/
	/*
	 * public void valueChanged(ListSelectionEvent e) {
		Macro.print( "List selection:"+ this.getSelectedIndex() );
	}*/
}
