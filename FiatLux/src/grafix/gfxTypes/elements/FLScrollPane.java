package grafix.gfxTypes.elements;

import javax.swing.JScrollPane;

/*extension of the class JScrollPane*/
public class FLScrollPane extends JScrollPane{

	/*public FLScrollPane() {
		super();
	}*/
	
	public FLScrollPane(FLPanel in_panel) {
		super(in_panel);
	}
	
	/*hide the horizontal scrollbar*/
	public void HideHorizontalScrollBar() {
		setHorizontalScrollBar(null);
	}
	
	/*hide the vertical scrollbar*/
	public void HideVerticalScrollBar() {
		setVerticalScrollBar(null);
	}
}
