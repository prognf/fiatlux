package grafix.gfxTypes.elements;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;

import grafix.gfxTypes.FLColor;

/************************************
 * FLButton = other name for JButton
 * use FLActinButton for simple handling of actions
 * @author Nazim Fates
 *******************************************/ 

public class FLButton extends JButton {
    /**
     * Set to true by defineAsPanelButtonStylized
     * If stylized = true, render a rounded button with gradient and hover effect
     */
    private boolean stylized = false;

    /**
     * Is the mouse over the button
     */
    private boolean m_over = false;

    /**
     * Is the button pressed
     */
    private boolean m_clicked = false;

    /**
     * If true, display a button with only top or bottom rounded corners
     * Only for stylized button (after call defineAsPanelButtonStylized)
     */
    public boolean onlyTopCorners = false, onlyBottomCorners = false;

    public FLButton(String in_str){
    	super(in_str);
    }

    public void AddActionListener(ActionListener listener){
    	addActionListener(listener);
    }

    /**
     * Change the foreground and background colors of the button, for
     * buttons in FLBLockPanel (this method does not draw gradients or rounded corners)
     * @param color : background color
     */
    public void defineAsPanelButton(Color color) {
        FLColor fcolor = new FLColor(color);
        setCursor(new Cursor(Cursor.HAND_CURSOR));
        setBackground(fcolor.darker(0.85));
        setForeground(fcolor.darker(0.3));
        setBorderPainted(false);
        setFocusPainted(false);
        setMargin(new Insets(4, 6, 4, 6));
        repaint();
    }

    /**
     * Change the skin of the button to a stylized button : rounded corners
     * and gradient background
     * @param color : background color
     */
    public void defineAsPanelButtonStylized(Color color) {
        stylized = true;
        defineAsPanelButton(color);

        /**
         * Add a space in the caption if there is an icon
         */
        String tx = "";
        if (getIcon() != null) {
            tx = "     ";
        }

        FLPanel content = FLPanel.NewPanel();
        FLLabel label = new FLLabel(tx + getText());
        label.setForeground(getForeground());
        content.add(label);
        content.setOpaque(false);
        add(content);
        setMargin(new Insets(1, 2, 1, 2));

        /**
         * Mouse listener to handle mouseover and mousepress events,
         * in order to change the background color when the button
         * is clicked or the mouse is over the button
         */
        addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
                m_clicked = true;
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                m_clicked = false;
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                m_over = true;
            }

            @Override
            public void mouseExited(MouseEvent e) {
                m_over = false;
            }
        });

        repaint();
    }

    /**
     * Override of paintComponent method in order to draw the background
     * only if it is a stylized button (after call defineAsPanelButtonStylized)
     * @param g
     */
    @Override
    public void paintComponent(Graphics g) {
        if (!stylized) {
            super.paintComponent(g);
            return;
        }

        int width = getWidth() - 1;
        int height = getHeight() - 1;
        int arc = 10;

        /**
         * Define colors
         */
        Color background = getBackground();
        Color background1a = brighter(background, 0.9); // Gradient top color
        Color background1b = darker(background, 0.9); // Gradient bottomn color
        Color background2 = darker(background, 0.8); // Borders

        if (m_clicked) {
            /**
             * If the button is pressed, display a darker version
             */
            double clickedFactor = 0.9;
            background1a = darker(background1a, clickedFactor);
            background1b = darker(background1b, clickedFactor);
            background2 = darker(background2, clickedFactor);
        } else if (m_over) {
            /**
             * If the mouse is over the button, display a brighter version
             */
            double hoverFactor = 0.9;
            background1a = brighter(background1a, hoverFactor);
            background1b = brighter(background1b, hoverFactor);
            background2 = brighter(background2, hoverFactor);
        }

        Graphics2D graphics = (Graphics2D) g;
        graphics.clearRect(-1, -1, width + 2, height + 2);
        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        /**
         * Draw background
         */
        graphics.setPaint(new GradientPaint(0, 0, background1a, 0, height, background1b, false));
        graphics.fillRoundRect(0, 0, width, height, arc, arc);

        if (onlyBottomCorners) {
            graphics.fillRect(0, 0, width, 2 * arc);
        }

        if (onlyTopCorners) {
            graphics.fillRect(0, 2 * arc, width, height - 2 * arc);
        }

        /**
         * Draw borders (only for buttons with all borders rounded)
         */
        if (!onlyBottomCorners && !onlyTopCorners) {
            graphics.setColor(background2);
            graphics.setStroke(new BasicStroke(1));
            graphics.drawRoundRect(0, 0, width, height, arc, arc);
        }

        /**
         * Can prevent some rendering problems with swing ...
         */
        graphics.setStroke(new BasicStroke(0));

        /**
         * Draw the icon if needed
         */
        if (getIcon() != null) {
            int iconY = (int) (((double) (height - getIcon().getIconHeight())) / 2.0);
            getIcon().paintIcon(this, g, 8, iconY);
        }
    }

    /**
     * helpers methods to make a color darker
     * @param inColor : color
     * @param factor : between 0 and 1
     * @return Color
     */
    private Color darker(Color inColor, double factor) {
        return (new FLColor(inColor)).darker(factor);
    }
    private Color brighter(Color inColor, double factor) {
        return (new FLColor(inColor)).brighter(factor);
    }

    /**
     * Methods called by FLBlockButton when a button is used for a block title
     * Only UI changes here
     * @param color
     */
    public void defineAsPanelTitleButton(Color color) {
        defineAsPanelTitleButton(color, FLPanel.PANEL_SIZE);
    }
    public void defineAsPanelTitleButton(Color color, int preferredSize) {
        FLColor fcolor = new FLColor(color);
        setBackground(fcolor);
        setForeground(fcolor.darker(0.3));
        setBorderPainted(false);
        setFocusPainted(false);
        setOpaque(false);
        //setMargin(new Insets(4, 6, 4, 6));
        setFont(new Font("SansSerif", Font.BOLD, 20));
        setPreferredSize(new Dimension(preferredSize - 10, 30));
        setAlignmentX(CENTER_ALIGNMENT);
        repaint();
    }
}
