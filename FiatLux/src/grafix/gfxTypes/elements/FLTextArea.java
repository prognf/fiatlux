package grafix.gfxTypes.elements;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JTextArea;

/*--------------------
 * for displaying text, wrapper for for JTextArea 
 * @author Nazim Fates
*--------------------*/

public class FLTextArea extends JTextArea {

	final static int ONELINE=1; 
	
	/* one line constructor */
	public FLTextArea(int in_columns) {
		this(ONELINE, in_columns); 
	}
	
	/* lines , columns */
	public FLTextArea(int in_lines, int in_columns) {
		super(in_lines, in_columns);
		// prevents edition
		this.setEditable(false);
	}
	
	public FLTextArea(String in_str) {
		super(in_str);
	}

	public FLTextArea(int textareasize, Color textBackground) {
		this(textareasize);
		setBackground(textBackground);
	}

	public FLTextArea(int in_lines, int textareasize, Color textBackground) {
		this(in_lines, textareasize);
		setBackground(textBackground);
	}

	public void SetHelp(String helpText) {
		setToolTipText(helpText);
	}
	
	public void SetFontMid(){
		Font font = new Font(Font.SANS_SERIF,Font.PLAIN, 18);
		setFont(font);		
	}
	
	final public void SetText(String s){super.setText(s);}

}
