package grafix.gfxTypes.elements;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.JComponent;

import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.IconManager;
import main.FiatLuxProperties;

/**
 * FLBlockPanel : a reduceable, stylized container panel
 * @author Nicolas Gauville
 */
public class FLBlockPanel extends FLPanel {
	/**
	 * Is the panel reduced or not
	 */
	public boolean contentVisible = true;

	/**
	 * If set to true, the panel can be reduced by clicking its title
	 * Also draw a carret on the top left corner to indicate that the panel is reduceable
	 */
	public boolean m_handleClick = false;

	/**
	 * Default height of the panel (when displayed)
	 */
	private int defaultSize = 100;

	/**
	 * The panel style and behavior is activated only when isPanelBlock = true,
	 * after some configuration with the defineAsWindowBlockPanel method
	 */
	private boolean isPanelBlock = false;

	/**
	 * Container for the content of the BlockPanel
	 */
	private FLBlockContainerPanel container;

	private Color blockBackground;
	private Color blockText;
	private Dimension reduced_dimensions;
	private FLButton title;
	private int defaultWidth = 250;

	/**
	 * Tab ID (to memorize users preferences)
	 */
	private FiatLuxProperties.SIDEBAR_TABS tabId;

	/**
	 * Link to the panel content, to adapt the size (height) of the panel
	 * automatically
	 */
	private FLPanel m_sub;

	public FLBlockPanel() {
		super();
		reduced_dimensions = new Dimension(defaultWidth, 30);
	}

	/**
	 * Used to add the panel title (param title of defineAsWindowBlockPanel)
	 * @param button
	 */
	public void AddGridBagButton(final FLButton button) {
		SetAnchor(GridBagConstraints.PAGE_START);
		m_GridBagC.weighty = 0.9; // request any extra vertical space
		m_GridBagC.gridwidth = 2; // centering
		AddGridBag(button, 0, 0);

		GetGridBagC().gridwidth = 1; // reset par
		GetGridBagC().weighty = 0.5; // reset par
	}

	public void setDefaultWidth(int width) {
		defaultWidth = width;
		title.setPreferredSize(new Dimension(width - 10, 30));
		if (contentVisible) {
			showContent();
		} else {
			hideContent();
		}
	}

	public void setDefaultHeight(int height) {
		defaultSize = height;
		setPreferredSize(new Dimension(defaultWidth, height));
		if (contentVisible) {
			showContent();
		} else {
			hideContent();
		}
	}

	/**
	 * Adapt size automatically to the size of the m_sub FLPanel
	 */
	public void adaptSize() {
		if (m_sub != null) {
			setDefaultHeight((int) m_sub.getPreferredSize().getHeight() + 30);
		}
	}

	public void adaptSize(FLPanel pSub) {
		m_sub = pSub;
		adaptSize();
	}

	/**
	 * Define tab_id in order to memorize tab default status (showContent or hideContent)
	 * @param tab
	 */
	public void setTabId(FiatLuxProperties.SIDEBAR_TABS tab) {
		boolean savedState = FiatLuxProperties.getInstance().getDefaultSidebarTabStatus(tab);
		if (savedState) {
			showContent();
		} else {
			hideContent();
		}
		
		tabId = tab;
	}

	/**
	 * Activate the blockpanel skin and behavior
	 * @param color : color of the borders and top of the panel
	 * @param title : the title (an FLButton because we can click on it to display/reduce the content)
	 * @param preferredSize : default height of the panel
	 */
	public void defineAsWindowBlockPanel(Color color, FLButton title, int preferredSize) {
		defineAsWindowBlockPanel(color, title, preferredSize, true);
	}

	/**
	 * Same method as the previous one, but with an handleClick parameter
	 * If set to false, the panel won't be reduceable
	 * @param color : color of the borders and top of the panel
	 * @param title : the title (an FLButton because we can click on it to display/reduce the content)
	 * @param preferredSize : default height of the panel
	 * @param handleClick : allow the user to reduce the panel by clicking on its title
	 */
	public void defineAsWindowBlockPanel(Color color, FLButton title, int preferredSize, boolean handleClick) {
		isPanelBlock = true;
		blockBackground = color;
		blockText = (new FLColor(color)).darker(0.7);
		setOpaque(false);

		title.setForeground(blockText);

		if (preferredSize > 0) {
			setPreferredSize(new Dimension(defaultWidth, preferredSize));
			defaultSize = preferredSize;
		} else {
			defaultSize = 100;
		}

		title.defineAsPanelTitleButton(color, defaultWidth);
		this.title = title;

		if (handleClick) {
			m_handleClick = true;
			title.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					contentVisible = !contentVisible;
					if (contentVisible) {
						showContent();
					} else {
						hideContent();
					}
				}
			});
		}

		setPreferredSize(new Dimension(defaultWidth, contentVisible ? defaultSize : 40));
		setSize(defaultWidth, contentVisible ? defaultSize : 40);

		if (!contentVisible) {
			setSize(reduced_dimensions);
		}

		repaint();
	}

	/**
	 * Reduce the panel (hide its content)
	 */
	public void hideContent() {
		contentVisible = false;
		Component[] cps = getComponents();
		for (Component c : cps) {
			if (!(c instanceof FLButton && c.getPreferredSize().width == defaultWidth - 10)) {
				c.setVisible(false);
			}
		}
		setPreferredSize(reduced_dimensions);
		setSize(reduced_dimensions);
		repaint();

		if (container != null) { // update the window size to the new configuration
			container.updateContainer(null);
		}

		if (tabId != null) {
			FiatLuxProperties.getInstance().setDefaultSidebarTabStatus(tabId, false);
		}
	}

	/**
	 * Expand the panel (display its content)
	 */
	public void showContent() {
		contentVisible = true;
		Component[] cps = getComponents();
		for (Component c : cps) {
			if (!(c instanceof FLButton && c.getPreferredSize().width == PANEL_SIZE - 10)) {
				c.setVisible(true);
			}
		}
		setPreferredSize(new Dimension(defaultWidth, defaultSize));
		repaint();

		if (container != null) { // update the window size to the new configuration
			container.updateContainer(this);
		}

		if (tabId != null) {
			FiatLuxProperties.getInstance().setDefaultSidebarTabStatus(tabId, true);
		}
	}

	/**
	 * Adapt the style of the inner components of the panel content
	 * @param components
	 */
	public void installBlockPanelStyle(JComponent ...components) {
		installBlockPanelStyle(false, components);
	}

	/**
	 * Same as the previous method, but use stylized button if stylized is set to true
	 * (wich means buttons with rounded corners and gradient background)
	 * @param stylized
	 * @param components
	 */
	public void installBlockPanelStyle(boolean stylized, JComponent ...components) {
		if (blockBackground == null) {
			if (getParent() != null && this.getParent() instanceof FLBlockPanel) {
				((FLBlockPanel) getParent()).installBlockPanelStyle(stylized, components);
			}

			return;
		}

		for (Component c : components) {
			if (c instanceof FLBlockPanel) {
				((FLBlockPanel) c).blockBackground = blockBackground;
				((FLBlockPanel) c).blockText = blockText;
			} else if(c instanceof FLButton) {
				if (stylized) {
					((FLButton) c).defineAsPanelButtonStylized(blockBackground);
				} else {
					((FLButton) c).defineAsPanelButton(blockBackground);
				}
			} else {
				c.setBackground(blockBackground);
			}
		}
	}

	/**
	 * Set the container element
	 * Used if we want the panels to interract each over (reduce unused panels when there is
	 * not enough place vertically)
	 * @param cnt
	 */
	public void setContainer(FLBlockContainerPanel cnt) {
		container = cnt;
	}

	/**
	 * Override of the paintComponent method in order to draw the stylized background
	 * @param g
	 */
	@Override
	public void paintComponent(Graphics g) {
		if (!isPanelBlock) {
			super.paintComponent(g);
			return;
		}

		int width = getWidth() - 1;
		int height = getHeight() - 1;
		int arc = 15;

		Color background = blockBackground;
		Color background1a = background;
		Color background1b = (new FLColor(background)).darker(0.9);
		Color background2 = (new FLColor(background)).darker(0.8);

		Graphics2D graphics = (Graphics2D) g;

		graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		graphics.setPaint(new GradientPaint(0, 0, background1a, 0, 36, background1b, false));
		graphics.fillRoundRect(0, 0, width, height, arc, arc);

		graphics.setColor(background2);
		graphics.setStroke(new BasicStroke(2));
		graphics.drawRoundRect(0, 0, width, height, arc, arc);
		graphics.setStroke(new BasicStroke());

		if (getHeight() > 80) {
			graphics.setColor(Color.WHITE);
			graphics.fillRoundRect(2, 36, width - 4, height - 38, arc, arc);
			graphics.fillRoundRect(2, 36, width - 4, 20, 0, 0);
		}

		if (m_handleClick) {
			boolean isPanelReduced = !(getHeight() > 80);
			Icon arrow = IconManager.getUIIcon(isPanelReduced ? "displayPane" : "hidePane");
			double topBarSize = isPanelReduced ? 46 : 36;
			int iconMargin = (int) ((topBarSize - ((double) arrow.getIconHeight())) / 2.0);
			arrow.paintIcon(this, g, iconMargin, iconMargin);
		}
	}

	/**
	 * Add a legend label
	 * @param in_legend : legend text
	 */
	@Override
	public void AddLegend(final String in_legend) {
		FLLabel label = new FLLabel(in_legend, FLLabel.typeFont.MEDIUM);
		label.getInsets().set(0, 0, 0, 0);
		label.setAlignmentX(CENTER_ALIGNMENT);
		add(label);
	}

	public void AddElement(JComponent component) {
		component.setOpaque(false);
		component.getInsets().set(0, 0, 0, 0);
		Add(component);
	}
}
