package grafix.gfxTypes.elements;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JTextPane;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;

/*
 * by N. Gauville
 */
public class FLCodeLineIndicatorPane extends JTextPane {
    private DefaultStyledDocument m_document;
    private Style m_default;

    public FLCodeLineIndicatorPane() {
        super();
        setEditable(false);
        setBackground(Color.lightGray);

        m_document = new DefaultStyledDocument();
        setDocument(m_document);
        m_default = m_document.addStyle("default", null);
        StyleConstants.setAlignment(m_default, StyleConstants.ALIGN_RIGHT);
        StyleConstants.setBold(m_default, true);
        m_document.setCharacterAttributes(0, getText().length(), m_default, false);
    }

    public void update(JTextPane pane, int nbLines) {
        StringBuilder sb = new StringBuilder();

        for (int i = 1; i <= nbLines + 1; i++) {
            sb.append(i).append(System.getProperty("line.separator"));
        }

        setText(sb.toString());
        m_document.setCharacterAttributes(0, getText().length(), m_default, false);

        pane.setSize(new Dimension(pane.getParent().getParent().getWidth() - 40, pane.getParent().getParent().getHeight()));
        setPreferredSize(new Dimension(30, pane.getHeight()));
        setSize(new Dimension(30, pane.getHeight()));
    }
}
