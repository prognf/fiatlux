package grafix.gfxTypes;

import java.awt.Color;
import java.util.ArrayList;

import components.randomNumbers.FLRandomGenerator;
import components.types.FLString;
import components.types.FLStringList;
import components.types.IntegerList;
import main.Convert;
import main.Macro;

public class PaintToolKit {

	final static String CLASSNAME= Thread.currentThread().getStackTrace()[1].getClassName();
	final static int [] COLWEBSAFE= {  0, 51, 102, 153, 204, 255 };
	final static int HALF= 128;
	final static int MAX= FLColor.MAX;

	/*--------------------
	 * attributes
	 --------------------*/	
	//special for tracking palettes
	static int PALETTE_ID=1;

	private ArrayList<FLColor> m_colorList;
	final int m_id;

	/*--------------------
	 * construction
	 --------------------*/

	public PaintToolKit() {
		ClearPalette();
		m_id= PALETTE_ID;
		PALETTE_ID++;
	}

	/** empty COLORS : don't forget to initialize ! 
	 * TODO: suppress this one ??? */
	public PaintToolKit(int nColors) {
		this();
		m_colorList= new ArrayList<FLColor>(nColors);
		for (int i=0; i< nColors; i++){
			m_colorList.add(null);
		}
	}

	public PaintToolKit(FLColor [] palette) {
		this();
		for (FLColor col : palette){
			AddColor(col);
		}
	}

	/*--------------------
	 * actions
	 --------------------*/
	public void ClearPalette() {
		m_colorList= new ArrayList<FLColor>();
	}

	public void AddColor(FLColor col) {
		m_colorList.add(col);
	}
	
	public void AddColorM(FLColor ... tabCol){
		for (FLColor col : tabCol){
			AddColor(col);
		}
	}

	public void AddColorRGB(int r, int g, int b) {
		AddColor(new FLColor(r,g,b));
	}
	
	/* HSB between 0 and 1 */
	public void AddColorHSB(float hue, float sat, float bri) {

		Color myRGBColor = Color.getHSBColor(hue, sat, bri);
		
		AddColor( new FLColor(myRGBColor));
	}
	

	/*--------------------
	 * get & set
	 --------------------*/


	public FLColor GetColor(int state){
		try{
			return m_colorList.get(state);
		} catch (ArrayIndexOutOfBoundsException e){
			Macro.SystemWarning("palette : unrecognized state:" + state);
			return null;
		}
	}

	public FLColor GetColorModulo(int state){
		int index= state% GetSize();
		return GetColor(index);
	}


	public int GetSize() {
		return m_colorList.size();
	}

	public void SetColor(int state, FLColor in_color){
		try{
			if (state>=GetSize()) {
				Macro.FatalError("state:" + state + "can not be set - check palette size");
			}
			m_colorList.set(state, in_color);
		} catch (ArrayIndexOutOfBoundsException e){
			Macro.FatalError("palette: impossible to set state :" + state);
		}
	}



	/** red shift palette */
	public void SetColorsRedShift(int maxstate) {
		ClearPalette();
		AddColor( FLColor.c_white );
		for (int i = 1; i <= maxstate; i++){
			AddColorRGB(255 * i / maxstate, 100, 100);
		}		
	}

	/** 2D gradient */
	public void Set2DGradient(double maxStateX, double maxStateY) {
		for (double y = 0; y < maxStateY; y++) {
			for (double x = 0; x < maxStateX; x++) {
				double lightRatio  = 1.0 - (x / maxStateX) * 0.4;
				double redGreenRatio= (y / maxStateY);

				AddColorRGB((int) (255.0 * redGreenRatio * lightRatio), (int) (255.0 * (1 - redGreenRatio) * lightRatio),  100);

			}
		}

		AddColor(FLColor.c_black);
	}





	//--------------------------------------------------------------------------
	//- STATIC 
	//--------------------------------------------------------------------------

	/** three-color palette */
	public static PaintToolKit GetDefaultPalette() {
		PaintToolKit palette= PaintToolKit.EmptyPalette();
		palette.AddColor( FLColor.c_white );
		palette.AddColor( FLColor.c_blue );
		palette.AddColor( FLColor.c_red); 
		return palette; 
	}	

	public static PaintToolKit EmptyPalette() {
		PaintToolKit palette= new PaintToolKit();
		return palette;
	}

	/**
	 * gradient of colors with indices from 0 to Num (included) and Num+1
	 * special
	 */
	static public PaintToolKit GetRainbow(int num){
		PaintToolKit outcol= PaintToolKit.EmptyPalette();
		outcol.AddColor( FLColor.c_black);
		for (int i = 1; i <= num; i++){
			int val = MAX * i / num;
			outcol.AddColorRGB(val, MAX- val, 0);
		}
		outcol.AddColor( FLColor.c_blue );
		return outcol;
	}

	/** gradient of colors with 256 colors */
	static public PaintToolKit GetNiceRainbow(){
		FLColor [] outcol= new FLColor[MAX +1];

		for (int i = 0; i <HALF; i++){
			int val = i * 2;
			outcol[i] = new FLColor(HALF + i, HALF + i, MAX - val );
		}
		for (int i = 0; i < HALF; i++){
			int val = i * 2;
			outcol[i + HALF] = new FLColor(MAX, MAX - val, HALF+i);
		}

		return new PaintToolKit(outcol);
	}
	//OC
	static public PaintToolKit GetCompleteRainbow(int num){
		System.out.println("Number of states palette : "+num);
		PaintToolKit outcol= PaintToolKit.EmptyPalette();
		outcol.AddColor(FLColor.c_white);
		for(int i=1; i<num;i++){
			float h = (float)(1.0/(num-1)*(i-1));
			System.out.println("H : "+h);
			FLColor color= new FLColor( FLColor.getHSBColor(h, 1,1) );
			outcol.AddColor(color);
			//FLColor col = FLColor.getHSBColor( h,0,100);
		}
		return outcol;
	}
	//\OC

	/** gradient */
	static public PaintToolKit GetGradient(int num){
		FLColor [] outcol= new FLColor[num + 2];
		for (int i = 0; i < num; i++){
			int red= i * MAX / num;
			int green= red;
			int blue= 0;
			outcol[i] = new FLColor(red, green, blue);
		}
		outcol[num] = FLColor.c_white;
		outcol[num+1] = FLColor.c_red;
		return new PaintToolKit( outcol );
	}



	/** B/W 256 colors 
	 * color 0 = all white*/
	static public PaintToolKit GetBWpalette(){
		FLColor [] outcol= new FLColor[MAX +1];

		for (int i = 0; i <=MAX; i++){
			int val = MAX - i;
			outcol[i] = new FLColor(val, val, val);
		}

		return new PaintToolKit(outcol);
	}

	/** BW ncolors is the number of colors which are not the zero color */
	static public PaintToolKit GetBWpalette(int ncolors){
		PaintToolKit palette= PaintToolKit.EmptyPalette();
		palette.AddColor(FLColor.c_white);
		for (int i = 1; i <= ncolors ; i++){
			int val = i * MAX / ncolors;
			palette.AddColorRGB(val, val, val);
		}
		return palette;
	}

	/** BW with increasing darkness 
	 * @param ncol : real number of colors  (incl. zero) */
	static public PaintToolKit GetBW2palette(int ncol){

		PaintToolKit palette= new PaintToolKit(ncol);
		for (int i = 0; i < ncol ; i++){
			int val = (ncol -1 - i) * MAX / (ncol-1);
			palette.SetColor(i, new FLColor(val, val, val) );
		}
		return palette;
	}

	/** BW ncolors is the number of colors which are not the zero color */
	static public PaintToolKit zzzGetGRDoublepalette(int ncolors){

		PaintToolKit palette= new PaintToolKit(2*ncolors+1);
		palette.SetColor(ncolors, FLColor.c_white);
		for (int i = ncolors-1; i < 0 ; i--){
			int val = i * MAX / ncolors;
			palette.SetColor(i, new FLColor(val, 0, 0) );
		}
		for (int i = ncolors+1; i > 2*ncolors+1 ; i++){
			int val = i * MAX / ncolors;
			palette.SetColor(i, new FLColor(0, val, 0) );
		}
		return palette;
	}


	final static FLColor [] OCTOPUSCOLOR= new FLColor [] { 
		FLColor.c_white, FLColor.c_blue,
		FLColor.c_yellow, FLColor.c_green,
		FLColor.c_red, FLColor.c_orange,
		FLColor.c_purple, FLColor.c_black};

	static public PaintToolKit GetOctopusColors(){
		return new PaintToolKit(OCTOPUSCOLOR);
	}

	/** BW inverted 
	 * convention: ncolors is the number of colors which are not the zero color */
	static public PaintToolKit GetInverseBWpalette(int ncolors){
		PaintToolKit palette= new PaintToolKit(ncolors+1);
		palette.SetColor(0, FLColor.c_black);
		for (int i = 1; i <= ncolors ; i++){
			int val = i * MAX / ncolors;
			palette.SetColor(i, new FLColor(MAX - val, MAX - val, MAX - val) );
		}	
		return palette;
	}

	//--------------------------------------------------------------------------
	//- I/O
	//--------------------------------------------------------------------------

	public void Print() {
		Macro.fPrint(" palette: id=%d size:%d", m_id, GetSize() );
		for (FLColor col : m_colorList){
			Macro.print( "col " + col.toString());
		}
	}

	//--------------------------------------------------------------------------
	//- SULTRA KOREA
	//--------------------------------------------------------------------------


	final static int NCOLBYCHANNEL = 6;
	/** random colors, all different, taken from "safeweb" repertoire 
	 * take Ncol <= 216 
	 * @param rndSource */
	static public PaintToolKit GetRandomPaletteSafeWeb(int Ncol, FLRandomGenerator rndSource ){
		PaintToolKit palette= PaintToolKit.EmptyPalette();
		int Ntot= NCOLBYCHANNEL * NCOLBYCHANNEL * NCOLBYCHANNEL;
		IntegerList triplets= IntegerList.getKamongN(Ncol, Ntot, rndSource);
		//IntegerList triplets= new IntegerList();
		/*for (int i=0; i< Ncol; i++){
			int i2= i * 216 / 130;
			triplets.Add(i2);
		}*/

		for (int col3 : triplets){
			String rep3= Convert.Decimal2StringBaseN(col3, 3, NCOLBYCHANNEL);
			IntegerList Itriplet= Convert.StringToDigitList(rep3);
			FLColor color= new FLColor(col(Itriplet,0),col(Itriplet,1),col(Itriplet,2));
			palette.AddColor(color);
		}
		return palette;
	}

	static public PaintToolKit GetLeLorrainPalette(int Ncol){
		PaintToolKit palette= new PaintToolKit(Ncol);
		for (int i=0; i<Ncol; i++){
			float d=  i / (float)Ncol ;
			float h= Rescale(d,0,.8f);
			float s= Rescale(d,0,1.f);
			float b= 1;//Rescale(d,1f,.5f);

			FLColor color= new FLColor( FLColor.getHSBColor(h, s, b) );
			palette.AddColor(color);
		}
		return palette;
	}

	private static float Rescale(float s, float a, float b) {
		return s *a + (1-s) * b;
	}

	private static int col(IntegerList itriplet, int i) {
		int pos= itriplet.Get(i);
		return COLWEBSAFE[pos];
	}
	//--------------------------------------------------------------------------
	//- RANDOM
	//--------------------------------------------------------------------------

	/** random colors with from 0 to Ncol (included) **/
	static public PaintToolKit GetRandomPalette(FLRandomGenerator rnd, int Ncol){
		FLColor [] outcol= new FLColor[Ncol + 1];
		for (int i = 0; i <= Ncol; i++){
			outcol[i] = GetRandomColor(rnd);
		}
		return new PaintToolKit(outcol);
	}

	/** random colors with from 0 to Ncol (included) 
	 * link with GlobalRandomizer **/
	static public PaintToolKit GetRandomPalette(int Ncol){
		return GetRandomPalette( FLRandomGenerator.GetGlobalRandomizer(), Ncol);
	}

	/** "bright" random colors with from 0 to Ncol (included) */
	static public PaintToolKit GetRandomPaletteBright(FLRandomGenerator rnd,int Ncol){
		PaintToolKit outcol= PaintToolKit.EmptyPalette();
		for (int i = 0; i <= Ncol; i++){
			int ibright= RandomInt(rnd, 3);
			int r= ((ibright==0)?MAX:RandomInt(rnd, MAX/2));
			int g= ibright==1?MAX*2/3:RandomInt(rnd, MAX/2);
			int b= ibright==2?MAX:RandomInt(rnd, MAX/2);
			outcol.AddColorRGB(r,g,b);
		}
		return outcol;
	}

	/** link with GlobalRandomizer **/
	public static FLColor GetRandomColor(FLRandomGenerator rnd) {
		return new FLColor( rnd.RandomInt(MAX),	rnd.RandomInt(MAX),	rnd.RandomInt(MAX) );
	}

	/** link with GlobalRandomizer **/
	private static int RandomInt(FLRandomGenerator rnd, int max) {
		return FLRandomGenerator.GetGlobalRandomizer().RandomInt(max);
	}

	public static PaintToolKit LoadPalette(String filename){
		Macro.print("Loading palette from file:" + filename);
		PaintToolKit palette= new PaintToolKit();

		FLStringList colors= FLStringList.OpenFile(filename);
		for (String str : colors){
			FLStringList tri= FLString.ParseString(str);
			try{
				if (str.isEmpty()){
					Macro.print("Found empty line when loading palette");
				} else{
					int 
					r= FLString.String2Int( tri.get(0) ),
					g= FLString.String2Int( tri.get(1) ),
					b= FLString.String2Int( tri.get(2) );
					palette.AddColorRGB(r, g, b);
					int s = palette.GetSize();
					//Macro.fDebug("adding color: %d %d %d | size %d", r, g, b, s);
				}
			} catch (Exception e){
				Macro.FatalError(e, "failed to parse string:["+str+"]");
			}
		}
		return palette;
	}
	/*--------------------
	 * test
	 *--------------------*/
	public static void DoTest(){		
		Macro.BeginTest(CLASSNAME);
		Macro.EndTest();
	}





}
