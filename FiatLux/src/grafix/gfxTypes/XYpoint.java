package grafix.gfxTypes;

import components.types.FLString;

public class XYpoint {
	
	public double m_x, m_y;
	
	public XYpoint(double x, double y){
		m_x= x; m_y=y;
	}

	@Override
	public String toString(){
		return "("+ FLString.DoublePrecision3( m_x ) + "," + FLString.DoublePrecision3( m_y ) + ")";
	}

	/* vector obtained by ref to point origin */
	public XYpoint Vector(XYpoint origin) {
		return new XYpoint(m_x - origin.m_x, m_y - origin.m_y);
	}

	public double CircleRadius() {
		return Math.sqrt(m_x * m_x + m_y*m_y - 1.);
	}

	public double Norm() {
		return Math.sqrt(m_x * m_x + m_y*m_y );
	}
	
	

}
