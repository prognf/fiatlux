package grafix.gfxTypes;

import java.awt.Color;
import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;


public class FLColorListRender extends DefaultListCellRenderer {
	
		
	public static final Color 
		COL_SELECTION= FLColor.RED;
	private Color [] m_COLOR; 
	/*--------------------
	 * Attributes
	 --------------------*/
	
	int m_Limit1D, m_limit2D;
	
	/*--------------------
	 * Constructor
	 --------------------*/

	public FLColorListRender(int limit1D, int limit2D) {
		super();
		m_Limit1D = limit1D;
		m_limit2D = limit2D;
		m_COLOR=new Color [] { FLColor.c_lightyellow, FLColor.c_lightgreen, FLColor.c_lightpurple};
	}

	/*--------------------
	 * main
	 --------------------*/
	
	public void SetColors(Color blue, Color cyan, Color pink){
		m_COLOR[0]= blue;
		m_COLOR[1]= cyan;
		m_COLOR[2]= pink;
	}
	
	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
		super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
		if (!(value instanceof String)) return this;
		//String s = (String)value;
		setForeground(FLColor.black);
		if ( index <= m_Limit1D ) {
			setBackground(m_COLOR[0]);
		} else if ( index <= m_limit2D ){
			setBackground(m_COLOR[1]);
		} else {
			setBackground(m_COLOR[2]);
		}
		if (isSelected) {
			setBorder(BorderFactory.createLineBorder(COL_SELECTION, 2));
		} else {
			setBorder(BorderFactory.createLineBorder(getBackground(), 2));
		}
		return this;
	}

	public void SetColorsBlueRedCyan() {
		SetColors(FLColor.c_lightcyan, FLColor.c_lightred, FLColor.c_lightblue);		
	}
}

