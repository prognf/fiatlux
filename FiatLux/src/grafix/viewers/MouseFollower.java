package grafix.viewers;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import components.types.IntC;
import components.types.IntCm;


public class MouseFollower 
implements MouseMotionListener, MouseListener{

	private int m_XMouseFollower, m_YMouseFollower;
	public int m_xCoord=-1, m_yCoord=-1; // access to the last click
	
	final private MouseInteractive m_followed; 
	
	public MouseFollower(MouseInteractive interactive_object) {
		m_followed= interactive_object;
	}

	/*--------------------
	 * implementations
	 --------------------*/
	
	/* Blank mouse listener methods */

	public void mouseClicked(MouseEvent event) {
	}

	public void mouseEntered(MouseEvent event) {
	}

	public void mouseExited(MouseEvent event) {
	}

	public void mouseReleased(MouseEvent event) {
	}

	/** main method when mouse is pressed */
	public void mousePressed(MouseEvent event) {
		//Macro.Debug("mouse pressed");
		// Get coordinates
		m_xCoord = event.getX();
		m_yCoord = event.getY();
		IntC XYcell= m_followed.GetCellFromCoordinates(m_xCoord, m_yCoord);
		int x = XYcell.X(), y = XYcell.Y();
		if (IsInside(x,y)){
			m_followed.ProcessMouseEventXY(x,y,event);
			// for dragging detection
			m_XMouseFollower = x;
			m_YMouseFollower = y;
		}		
		//Macro.Debug("button : " + event.getButton());
	}

	/** says if a cell (x,y) is "inside" the grid **/
	final private boolean IsInside(int x, int y){
		IntC xySize= m_followed.GetXYsize();
		return 	(x< xySize.X()) && (x>=0) && 
				(y< xySize.Y()) && (y>=0);
	}


	public void mouseDragged(MouseEvent event) {
		//Macro.Debug("mouse dragged");
		int xCoord = event.getX();
		int yCoord = event.getY();
		IntC XYcell= m_followed.GetCellFromCoordinates(xCoord, yCoord);
		int x1 = XYcell.X(), y1 = XYcell.Y();
		if (((x1 != m_XMouseFollower) || (y1 != m_YMouseFollower)) // mouse has moved 
			&& IsInside(x1,y1) ){ //inside bounds
			m_followed.ProcessMouseEventXY(x1, y1,event);
			// for dragging detection
			m_XMouseFollower = x1;
			m_YMouseFollower = y1;
		
		}
	}

	public void mouseMoved(MouseEvent e) {
		// nothing
	}

	public IntCm GetLastClickCoord(){
		return new IntCm(m_xCoord, m_yCoord);
	}
	
	
}
