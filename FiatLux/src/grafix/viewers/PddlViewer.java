package grafix.viewers;

import java.awt.Color;
import java.awt.geom.Ellipse2D;

import components.arrays.RegularDynArray;
import components.types.IntC;
import grafix.gfxTypes.FLColor;


public class PddlViewer extends OneRegisterAutomatonViewer {

	private static final Color COL_HIGHLIGHT = FLColor.RED;

	
	int m_posHighlight;
	boolean m_isHighLightOn= false;
	
	public final static String RUNPLANNER_COMMAND="./runLAMA"; //"./runFF";
		
	
	public PddlViewer(IntC inCellSize, 
			IntC inGridSize, RegularDynArray inAutomaton) {
		super(inCellSize, inGridSize, inAutomaton);
	}

	
	/**
	 */
	@Override
	protected void DrawPostEffect() {
		if (m_isHighLightOn){
			HighLightCell(m_posHighlight);
		}
	}
	
	public void SetPosHighLight(int pos){
		m_isHighLightOn= true;
		m_posHighlight= pos;
		UpdateView();
	}


	/** suppresses the highlight **/
	public void SetHighLightOff() {
		m_isHighLightOn= false;		
	}
	
	/** highlights a particular cell **/
	final protected void HighLightCell(int pos){
		int x = pos % getXsize();
		int y = pos / getXsize();
		HighLightCell(x, y);
	}
	
	/** highlights a particular cell **/
	final protected void HighLightCell(int x, int y){
		m_graphix.setColor(COL_HIGHLIGHT);
		int Xpos = getXpos(x);
		int Ypos = getYpos(y);
		float diam= m_SquareSize / 5;
		float dx = (m_SquareSize - diam)/ 2 , dy=dx;
		Ellipse2D circle = new Ellipse2D.Float(Xpos + dx, Ypos + dy, diam, diam);
		m_g2D.fill(circle);
	}
	

}
