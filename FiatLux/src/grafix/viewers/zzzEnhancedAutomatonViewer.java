package grafix.viewers;

import components.allCells.OneRegisterIntCell;
import components.arrays.RegularDynArray;
import components.types.IntC;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.elements.FLPanel;


/*--------------------
 * neighbour-dependent colour
 * @author Nazim Fates
*--------------------*/
public class zzzEnhancedAutomatonViewer extends OneRegisterAutomatonViewer {


	private static final boolean zzDEF_SELECT = true;
	private boolean m_enhancedDisplay= zzDEF_SELECT;

	public zzzEnhancedAutomatonViewer(IntC in_CellSize,
			IntC in_GridSize, RegularDynArray in_Automaton) {
		super(in_CellSize, in_GridSize, in_Automaton);
		PaintToolKit palette= PaintToolKit.GetRandomPalette(512);
		palette.SetColor(0, FLColor.c_white);
		palette.SetColor(1, FLColor.c_blue);
		this.SetPalette(palette);
	}

	/** returns the state of a cell */
	public /*final*/ int GetCellColorNumXY(int x, int y) {
		OneRegisterIntCell cell = GetCell(x,y);
		int st= cell.GetState();
		if (m_enhancedDisplay && (st==1) ){
			int neighbCode= cell.GetNeighbourhoodStateCode();
			return neighbCode;
		} else{
			return st;
		}
	}

	/** overrides  */
	@Override
	public FLPanel GetSpecificPanel(){
		FLPanel p = super.GetSpecificPanel();
		p.Add(new RegularAutomatonViewer.EnhancedDisplayMode());
		return	p;	
	}



}
