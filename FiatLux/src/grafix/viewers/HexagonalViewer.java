package grafix.viewers;

import java.awt.event.MouseEvent;

import components.allCells.OneRegisterIntCell;
import components.arrays.GenericCellArray;
import components.arrays.RegularDynArray;
import components.types.IntC;
import components.types.IntCm;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.elements.FLList;
import grafix.gfxTypes.elements.FLPanel;
import topology.basics.SuperTopology;

/*--------------------
 * allows the user to click and change the state of a cell
 * @author Nazim Fates
*--------------------*/

//TODO: why is this not a RegularAutomatonViewer ???
public class HexagonalViewer extends GridViewer 
implements MouseInteractive {

	// ratio for hexagonal Y-axis compression
	protected final static double SQRT3DIV2 = Math.sqrt(3)/2.;

	
	/*--------------------
	 * attributes
	 --------------------*/
	
	protected GenericCellArray<OneRegisterIntCell> m_array;
	
	private final static String [] MODE={"flip","apply rule"}; 
	protected FLList m_MouseMode;
	//protected Radio2Control m_DisplayStabilityControl ;

	
	/*--------------------
	 *  construction
	 --------------------*/
	
	
	public HexagonalViewer(IntC in_CellSize, IntC in_GridSize,
			RegularDynArray in_automaton) {
		
		super(in_CellSize, in_GridSize);
		m_array= new  GenericCellArray<OneRegisterIntCell>(in_automaton);
		m_MouseMode = new FLList(MODE);
		ActivateMouseFollower(this);
		
		//
		m_backgrndCoord.Add(m_CellDist/2,0);
		ActivateMouseFollower(this);
	}

	final protected OneRegisterIntCell GetCell(int pos){
		return m_array.GetCell(pos);
	}
	

	/*--------------------
	 * main
	 --------------------*/

	/** main component */
	protected void DrawSystemState() {

			for (int x = 0; x < getXsize(); x++)
			for (int y = 0; y < getYsize(); y++) {
				int state = GetCellStateXY(x, y); 
				FLColor col;
				if (state != SuperTopology.GHOSTSTATE) {
					col = GetColor(state);
				} else {
					// color for "ghost" cells (holes)
					col = m_GhostColor;
				}

				SetColor(col);								
					DrawCircleTriangularLattice(x, y);	
				}
	}
	
	/** draws one circle at position x, y 
	 * on a triangular lattice (offset for "odd" positions) */
	final private void DrawCircleTriangularLattice(int x, int y){
		int R= GetCellDist();
		int Xpos = XG + x * R + (y % 2) * (R/2);
		int DeltaY= (int) Math.ceil(y * R * SQRT3DIV2);
		int Ypos = YG + DeltaY;
		m_graphix.fillOval(Xpos, Ypos, m_SquareSize+1, m_SquareSize+1);		
	}
	/*--------------------
	 * mapping methods
	 --------------------*/
	
	final private OneRegisterIntCell GetCell(int x, int y){
		return GetCell(Map2D(x, y));
	}

	
	/* returns the state of a cell */
	final protected int GetCellStateXY(int x, int y) {
		return GetCell(x,y).GetState();
	}
	
	//returns if a cell stable
	protected boolean IsStableXY(int x, int y){
		return GetCell(x,y).IsStable();		
	}

	@Override
	final public void ProcessMouseEventXY(int x, int y, MouseEvent event) {
		if (m_MouseMode.GetSelectedItemRank() == 0){// flip mode
			ChangeCellState(x,y);
		} else { //local rule mode
			ApplyLocalRule(x,y);  
		}
		// updating
		UpdateView();
	}

	final private void ChangeCellState(int x, int y) {
		// inverting cell's state
		GetCell(x,y).InitState(1 - GetCellStateXY(x, y));
	}
	
	final private void ApplyLocalRule(int x, int y) {
		// inverting cell's state
		GetCell(x,y).sig_UpdateBuffer();
		GetCell(x,y).sig_MakeTransition();
	}
	
	/*--------------------
	 * get / set
	 --------------------*/
	
	@Override
	protected IntCm GetXYWindowSize() {
		IntCm max= GetXYmax();
		max.Add(2*XG,2*YG);
		return max; 
	}
	
	protected IntCm GetBackGroundDimensions() {
		return GetXYmax();
	}
	
	private IntCm GetXYmax() {
		int xmax= m_CellDist * getXsize() + m_CellDist / 2;
		int ymax= (int) Math.ceil(m_CellDist * (getYsize()-1) * SQRT3DIV2)
				+ (m_SquareSize+1);
		return new IntCm(xmax, ymax);
	}


	@Override
	public FLPanel GetSpecificPanel(){
		FLPanel p = new FLPanel();
		p.add(m_MouseMode);
		//p.add(m_DisplayStabilityControl);
		return	p;	
	}

	/** to which cell does the pixel clicked correspond ? */
	@Override
	public IntC GetCellFromCoordinates(int xCoord, int yCoord){
		
		int R= GetCellDist();
		int dy = R / 2;
		int DeltaY= (int) Math.ceil(R * SQRT3DIV2);
		int y= (yCoord - YG)  /  DeltaY ;
		int dx= (y%2 == 0) ? 0 : R / 2 ;
		int x= (xCoord - XG - dx) / GetCellDist();
		//String s= "R:" + R + " ( " + (xCoord - XG)  + "," + (yCoord - YG) + ") -> ("  + x + "," + y + ")";
		//Macro.Debug(s);
		return new IntC ( x, y);

	} 
	
}
