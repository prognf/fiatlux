package grafix.viewers;

import components.allCells.OneRegisterIntCell;
import components.arrays.GenericCellArray;
import experiment.samplers.CAsimulationSampler;
import grafix.gfxTypes.FLColor;
import main.Macro;
import topology.basics.LinearTopology;

/*--------------------
 * enables the display of a 1D automaton the model passed
 * in the constructor is used to interpret the colors
 * 
 * @author Nazim Fates
*--------------------*/
public class LineAutomatonViewerDifference extends LineAutomatonViewer {

	// updating with regular states or enhanced states (neighb. colors)
	public static final int REGULAR = 0, ENHANCED=1;
	public static int FeedMode= REGULAR;


	/*--------------------
	 * attributes
	 --------------------*/

	GenericCellArray<OneRegisterIntCell> m_arrayA;
	GenericCellArray<OneRegisterIntCell> m_arrayB;
	
	/*--------------------
	 * construction
	 --------------------*/


	/**  **/
	/*protected LineAutomatonViewerDifference(IntCouple cellPixSize, LinearTopology in_topo, 
			RegularDynArray in_automatonA, RegularDynArray in_automatonB, int in_Tsize) {
		*/
	public LineAutomatonViewerDifference(CAsimulationSampler samplerA, CAsimulationSampler samplerB) {
		super(
				((GridViewer)samplerA.GetAutomatonViewer()).GetPixCellSize(), 
				 (LinearTopology)samplerA.GetTopologyManager(), 
				 samplerA.GetAutomaton(), 
				 ((LineAutomatonViewerDefault)samplerA.GetAutomatonViewer()).getYsize());
		m_arrayA = new GenericCellArray<OneRegisterIntCell>(samplerA.GetAutomaton());
		m_arrayB = new GenericCellArray<OneRegisterIntCell>(samplerB.GetAutomaton());
	
			//SetPalette( PaintToolKit.GetBW2palette(4) );
		FLColor [] COL = 
				{ FLColor.c_white, 
				FLColor.c_red, FLColor.c_darkgreen,
				FLColor.c_lightgrey};
		SetPalette( COL );
	}

	/*--------------------
	 * actions
	 --------------------*/



	protected void FlipCellState(int index) {
		Macro.SystemWarning(" FlipCellState not implemented ");
	}

	

	@Override
	public void UpdateBuffer(int[] timeBuffer) {
		int Lsize= timeBuffer.length;
		switch(FeedMode){
		case REGULAR:
			for(int pos=0; pos< Lsize; pos++) {
				timeBuffer[pos] = GetState(pos) ;
			}
			break;
		case ENHANCED:
			for(int pos=0; pos< Lsize; pos++) {
				int left= GetState( (pos -1 + Lsize) % Lsize ) ;
				int center= GetState( pos );
				int right= GetState( (pos + 1 ) % Lsize ) ;
				int st = 4 * left + 2* center + right;
				timeBuffer[pos] = st;
			}	
			break;
		default:
			Macro.FatalError("Unexpected feedmode value");
		}

	}

	private int GetState(int pos) {
		 int col=
				 2 * m_arrayA.GetCell(pos).GetState() + 
				 m_arrayB.GetCell(pos).GetState();
		 return col;
		//m_arrayA.GetStateForDisplay(pos)==m_arrayB.GetStateForDisplay(pos)?0:1;
	}


}
