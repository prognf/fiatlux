package grafix.viewers;

import java.awt.event.MouseEvent;

import components.types.IntC;


public interface MouseInteractive {

	/** mouse click on cell (x,y) */
	void ProcessMouseEventXY(int x, int y, MouseEvent event);

	/** from mouse click on device to x,y coordinates **/
	IntC GetCellFromCoordinates(int xcoord, int ycoord);
	
	/** to determine if the click is "inside" the grid or not **/
	IntC GetXYsize();
	
}
