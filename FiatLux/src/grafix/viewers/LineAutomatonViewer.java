package grafix.viewers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;

import components.arrays.RegularDynArray;
import components.arrays.TimeBuffer;
import components.arrays.TimeReadable;
import components.types.FLsignal;
import components.types.IntC;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.IconManager;
import grafix.gfxTypes.controllers.FLCheckBox;
import grafix.gfxTypes.controllers.IntController;
import grafix.gfxTypes.elements.FLActionButton;
import grafix.gfxTypes.elements.FLButton;
import grafix.gfxTypes.elements.FLFrame;
import grafix.gfxTypes.elements.FLPanel;
import initializers.CAinit.OneRegisterSettableSystem;
import main.Macro;
import topology.basics.LinearTopology;

/* 
 * holds the Time Buffer for space-time diagrams 
 * 
 * WARNING : there works with a "lazy" update mode:
 * call sig_updtae when the initial condition has changed!
 */
public abstract class LineAutomatonViewer extends RegularAutomatonViewer 
implements MouseInteractive {

	//private final static String CLASSNAME="LineAutomatonViewer";
	private static final boolean DF_RADAR = false;
	private static final String LABEL_VIEWERSETTINGS = "Viewer settings";

	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------

	/** different ways to add colors in the buffers 
	 *  one has to put int values (color codes) in time buffer **/
	public abstract void UpdateBuffer(int[] timeBuffer);

	/** for user interaction */
	abstract protected void FlipCellState(int x);

	//--------------------------------------------------------------------------
	//- attributes
	//--------------------------------------------------------------------------

	protected TimeBuffer m_buffer; // "memory" for space-time diagrams
	private FLPanel m_colorPanel;
	int m_period= 1;
	boolean m_radarmode = DF_RADAR;
	TimeReadable m_timeReadable;
	OneRegisterSettableSystem m_oneRegisterSystem;
	int m_shift=0; // shift when viewing
	private FLPanel m_blinkControl;
	//--------------------------------------------------------------------------
	//- construction
	//--------------------------------------------------------------------------
	protected LineAutomatonViewer(IntC in_CellPixSize, 
			TimeReadable in_system, int Lsize, int Tsize) {
		super(in_CellPixSize, new IntC(Lsize, Tsize));
		m_timeReadable= in_system;
		m_buffer = new TimeBuffer(Tsize, Lsize, this) ;
		ActivateMouseFollower(this);
	}


	/** the automaton is used only to read the time 
	 * TODO: the topology is not used **/
	protected LineAutomatonViewer(IntC in_CellPixSize, LinearTopology in_topo, 
			RegularDynArray in_automaton, int Tsize) {		
		this(in_CellPixSize, in_automaton, in_automaton.GetSize(), Tsize);
	}



	//--------------------------------------------------------------------------
	//- signals
	//--------------------------------------------------------------------------


	@Override
	public void ReceiveSignal(FLsignal sig){
		if (Macro.TRACESIGNAL){
			Macro.print(" Line automaton signal: " + sig);
		}
		// sending to Super
		super.ReceiveSignal(sig);
		// specific
		switch(sig){
		case init:
			m_buffer.sig_Init();
			break;
		case update:
			m_buffer.sig_Upate();
			break;
		case nextStep:
			int time = m_timeReadable.GetTime();
			//Macro.Debug("viewer time " + time + " per " + m_period);
			if ( (time - 1) % m_period == 0){ // time -1 allows us to put a new line at time t=1 
				m_buffer.sig_FeedLine();
			}
			UpdateView();
			break;
		}
	}


	//--------------------------------------------------------------------------
	//- Get / Set 
	//--------------------------------------------------------------------------

	/** buffer size */
	public int GetTsize(){
		return m_buffer.GetTsize();
	}
	
	public void SetBufferFeedPeriod(int in_period){
		m_period= in_period;
	}

	public void SetBlinkControlPanel(FLPanel blinkControl){
		m_blinkControl = blinkControl;
	}

	//--------------------------------------------------------------------------
	//- GFX
	//--------------------------------------------------------------------------
	@Override
	final public void ProcessMouseEventXY(int x, int y, MouseEvent event) {
		int shiftedX= x + 1;
		//Macro.Debug("click on: x:" + shiftedX);
		//+ " y:" + y + " Y=" + GetYsize());
		if (y== getYsize() - 1){
			//if (m_MouseMode.GetSelectedItemRank() == 0){// flip mode
			//Macro.Debug("flip cell called");
			FlipCellState(x);
			/*	} else { //local rule mode
				ApplyLocalRule(x,y);  
			}*/
			//UpdateView();
			ReceiveSignal(FLsignal.update);
		}
		//int oldcol= GetCellColorNumXY(x, y);
		//int newcol= GetCellColorNumXY(x, y);
		//Macro.fDebug(" col > old:%d new:%d ",oldcol, newcol);
	}


	/** main: used to find the color of cell (x,y) */
	public int GetCellColorNumXY(int x, int y) {
		int c= RegularGetCellColorNumXY(x, y);
		if (c== TimeBuffer.RESET_STATE){
			return TimeBuffer.RESET_STATE;
		} else {
			switch(GetFeedMode()){
			case REGULAR:
				return c; 
			case ENHANCED:
				int 
				l= RegularGetCellColorNumXY(x-1, y),
				r= RegularGetCellColorNumXY(x+1, y);
				return 4*l+2*c+r;
			default:
				return Macro.ERRORint;
			}
		}
	}
	public void setColorPanel(FLPanel colorPanel){
		m_colorPanel = colorPanel;
	}


	/** main: used to find the color of cell (x,y) */
	public int RegularGetCellColorNumXY(int x, int y) {
		int newx= (x + getXsize() + m_shift * (m_buffer.GetTsize() - 1 - y) ) % getXsize();
		int newy= getYsize() - 1 - y;
		// x line = cell number ; y buffer number
		if (m_radarmode){
			return m_buffer.GetBufferCellStateRadar(y, newx);
		} else {
			return m_buffer.GetBufferCellState(newy, newx);
		}
	}

	/** returns if a cell stable */
	protected boolean IsStableXY(int x, int y){
		Macro.FatalError("Not implemented");
		return false;
	}



	class PeriodController extends IntController{

		public PeriodController() {
			super("buffer period");
		}

		@Override
		protected int ReadInt() {
			return m_period;
		}

		@Override
		protected void SetInt(int val) {
			m_period= val;		
		}

	}

	/** GFX control */
	public FLPanel GetSpecificPanel(){
		//TODO : ajout boutton viewer 1D
		FLPanel panel = super.GetSpecificPanel();//
		OpenControlWindowButton button = new OpenControlWindowButton(this);
		button.defineAsPanelButton(FLColor.c_lightred);
		panel.AddGridBag(button,0,1);

		//FLPanel panel2 = FLPanel.NewPanelVertical(new PeriodController(), new DisplayModeController(), new Shiftcontroller(), new ShiftButtons());
		//panel.AddGridBag(panel2,0,1);
		panel.setOpaque(false);
		if (super.GetFeedMode() == GFXMODE.ENHANCED){//a bit curious
			panel.AddGridBag(new RegularAutomatonViewer.EnhancedDisplayMode(),0,10);
		}
		return panel;
	}
	/* more controls in a separate window */
	class OpenControlWindowButton extends FLActionButton {

		//boolean m_default;
		public OpenControlWindowButton(LineAutomatonViewer automaton) {
			super(LABEL_VIEWERSETTINGS);
			setIcon(IconManager.getUIIcon("configMin"));
			defineAsPanelButtonStylized(FLColor.c_lightred);
			//m_default = (automaton instanceof LineAutomatonViewerDefault);
		}

		@Override
		public void DoAction() {
			JLabel colors = new JLabel("Change colors : ");
			colors.setAlignmentX(CENTER_ALIGNMENT);

			FLPanel panel2 = FLPanel.NewPanelVertical(new PeriodController(), new DisplayModeController(), new ShiftPanel(), colors, m_colorPanel);
			if(m_blinkControl!=null){
				panel2.Add(m_blinkControl);
			}


			FLFrame frame = new FLFrame("control viewer");
			frame.addPanel(panel2);
			frame.packAndShow();
			/*if(m_default){
				LineAutomatonViewerDefault.BlinkControl() blink = new LineAutomatonViewerDefault.BlinkControl();
				panel2.Add(new LineAutomatonViewerDefault.BlinkControl());
			}*/

		}

	}


	class DisplayModeController extends FLCheckBox{

		public DisplayModeController() {
			super("Radar mode", m_radarmode);
			setAlignmentX(CENTER_ALIGNMENT);
			setOpaque(false);
		}

		@Override
		protected void SelectionOff() {
			m_radarmode= false;
		}

		@Override
		protected void SelectionOn() {
			m_radarmode= true;		
		}

	}

	/* other model of action handling **/
	//OC
	class ShiftPanel extends FLPanel implements ActionListener  {
		private Shiftcontroller m_shiftCont;
		FLButton m_plus = new FLButton("+");
		FLButton m_minus = new FLButton("-");

		public ShiftPanel(){
			m_shiftCont = new Shiftcontroller();
			m_plus.AddActionListener(this);
			m_minus.AddActionListener(this);

			//this.SetBoxLayoutY();
			this.Add(m_shiftCont);
			Add(m_plus, m_minus);
		}

		@Override
		public void actionPerformed(ActionEvent event) {
			if (event.getSource() == m_plus) {
				m_shift++;
			} else{
				m_shift--;
			}
			m_shiftCont.Update();
			UpdateView();
		}

	}

	//\OC

	class Shiftcontroller extends IntController {

		public Shiftcontroller() {
			super("shift");
		}

		@Override
		protected int ReadInt() {
			return m_shift;
		}

		@Override
		protected void SetInt(int val) {
			m_shift= val;
			UpdateView();
		}
		public void UpdateVal(){
			this.Update();

		}
	}

}
