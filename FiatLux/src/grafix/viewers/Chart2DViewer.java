package grafix.viewers;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JComponent;

import components.types.IntC;
import main.Macro;


/*--------------------*****
 * An Chart2DViewer  is a Jcomponent
 * it is the base class for displaying the states of square lattice
 * @author Nazim Fates
 * Date of creation: Nov. 2001 
 *******************************************/ 
public class Chart2DViewer extends JComponent{
    
    // implicit :
    // abstract void paintComponent();

    /*--------------------
     * Attributes 
    --------------------*/
    
    static final int NUMSTATE= 101; // color display
    static private Color [] myColor= new Color[NUMSTATE];
   
    // C: length of a cell, CellDist : distance beetween 2 cells
    int m_XRadius; 
    int m_YRadius;
    
    int m_Isize, m_Jsize;
    int [][] m_data;

    int 
	m_XwindowSize= 0, 
	m_YwindowSize= 0;
   
    /*--------------------
    // Get & Set methods
   --------------------*/
    
    public void paintComponent(Graphics g){
	for (int i=0; i<m_Isize; i++){
	    for (int j=0; j<m_Jsize; j++){
		PaintCell(g,i,j);
		}
	}
    }

    /* the convention used is the one of matrices :
       i is the line, j is the column */
    final private void PaintCell(Graphics g, int i, int j){ 
	int data= m_data[i][j];
	try{
	    g.setColor( myColor[ data ] );
	    g.fillRect( i * m_XRadius, j * m_YRadius, 
			m_XRadius, m_YRadius);
	}
	catch(Exception e){
	    Macro.FatalError( this, "Error when trying to paint coord : (" + i + ","+ j + ") with value :" + data);
	}
    } 

    /* entries ar esupposed to belong to [0,1] */
    final public void SetArrayValues(double [][] in_value){
	for (int i=0; i<m_Isize; i++){
	    for (int j=0; j<m_Jsize; j++){
		int temp=  (int)(in_value[i][j] * 100);
		try{
		    m_data[i][j]= temp;
		} catch (Exception e){
		    Macro.FatalError( this, "SetArrayValues", 
				      "Error when trying to copy coord : (" + i + ","+ j + ") with value :" + temp);
		}
	    }
	}
	repaint();
    }
  

    /*--------------------
     * Constructor
    --------------------*/
    public Chart2DViewer(IntC in_size){
	m_Isize= in_size.X();
	m_Jsize= in_size.Y();
	/* mem alloc & initialising COLOR array */
	myColor[0]= new Color(0,0,0);
	for (int i=1; i<NUMSTATE; i++){
	    int col_r= (255*i)/NUMSTATE;
	    int col_b= (255*(i - NUMSTATE/2))/NUMSTATE;
	    if (col_b<0) 
		col_b = 0;
	    if (col_r>255)
		col_r = 255;
	    myColor[i]=new Color(col_r, 0, 255 - col_r);
	}
	/* mem alloc m_data array */
	m_data= new int [m_Isize][m_Jsize];

	m_XRadius= 300 / ( m_Isize  );
	m_YRadius= 300 / ( m_Jsize  );

	m_XwindowSize= m_Isize * m_XRadius ;
	m_YwindowSize= m_Jsize * m_YRadius ; 

    }

    /*--------------------
     * Get & Set methods
    --------------------*/
 
    public Dimension getPreferredSize(){
	return new Dimension(  m_XwindowSize, m_YwindowSize );
    }
    
    public Dimension getMinimumSize(){
	return new Dimension(  m_XwindowSize, m_YwindowSize );
    } 

    public Dimension getMaximumSize(){
	return new Dimension(  m_XwindowSize, m_YwindowSize );
    } 

    protected void SetWindowSize(int in_XwindowSize,int in_YwindowSize){
	m_XwindowSize=in_XwindowSize;
	m_YwindowSize=in_YwindowSize;
	Macro.print(1, this, "Window size set to : ("+
			m_XwindowSize+ " , "+
			m_YwindowSize+" )" );
    }
   
}
