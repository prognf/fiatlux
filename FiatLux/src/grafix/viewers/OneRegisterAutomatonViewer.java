package grafix.viewers;

import java.awt.event.MouseEvent;

import components.allCells.OneRegisterIntCell;
import components.arrays.GenericCellArray;
import components.arrays.RegularDynArray;
import components.types.DoublePar;
import components.types.IntC;
import components.types.IntPar;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.IconManager;
import grafix.gfxTypes.controllers.OnOffControl;
import grafix.gfxTypes.controllers.Radio2Control;
import grafix.gfxTypes.elements.FLActionButton;
import grafix.gfxTypes.elements.FLFrame;
import grafix.gfxTypes.elements.FLList;
import grafix.gfxTypes.elements.FLPanel;
import grafix.windows.SimulationWindowSingle;
import grafix.windows.OneDimensionalCAcontrolPanel;
import main.Macro;

/*--------------------
 * allows the user to click and change the state of a cell
 * automaton viewers may modify the state of the cells
 * a local timer is used for the shifts
 * @author Nazim 
+ Océane Chazé zqsd
 *--------------------*/
public class OneRegisterAutomatonViewer extends RegularAutomatonViewer
implements MouseInteractive {

	private static final boolean DEFSELECT = false;
	private final static String [] MODE={"flip", "set", "apply rule"};

	/*--------------------
	 * attributes
	 --------------------*/
	protected GenericCellArray<OneRegisterIntCell> m_array; // related array of cells
	//	protected RegularDynArray m_automaton; // for accessing the time



	private final FLList m_MouseMode;
	private Radio2Control m_DisplayStabilityControl ;
	private IntPar m_setCellValOnClick= new IntPar("setCellVal", 1);

	private SimulationWindowSingle.MoveButton m_zButton, m_qButton, m_sButton, m_dButton;
	private FLPanel m_colorPanel;

	/*-------------------------
	 * construction
	 *-------------------------*/

	public OneRegisterAutomatonViewer(IntC cellSize, IntC gridSize,
			RegularDynArray automaton) {
		super(cellSize, gridSize);
		m_array= new GenericCellArray<OneRegisterIntCell>(automaton);
		m_shift_x = 0;
		m_shift_y = 0;
		m_shift_dx= new DoublePar("DX", 0);
		m_shift_dy= new DoublePar("DY", 0);

		ActivateMouseFollower(this);
		m_MouseMode= new FLList(MODE);
	}

	/*-------------------------
	 * implements & override
	 *-------------------------*/


	/** returns the state of a cell 
	 * this adds a "layer" to decide whether the state is return or something else !
	 * TODO : is this layer useful ??? */
	public /*final*/ int GetCellColorNumXY(int x, int y) {
		OneRegisterIntCell cell = GetCell(x,y);
		switch (GetFeedMode()){
		case REGULAR:
			return cell.GetState();
		case ENHANCED:
			return cell.GetNeighbourhoodStateCode();
		default :
			return Macro.ERRORint;
		}
	}





	//returns if a cell stable
	protected boolean IsStableXY(int x, int y){
		return GetCell(x,y).IsStable();		
	}

	/*--------------------
	 * get / set
	 --------------------*/

	final protected OneRegisterIntCell GetCell(int pos){
		return m_array.get(pos);
	}

	/** MAPPING from 2D to 1D : returning cell with torical boundary cond. **/ 
	protected final OneRegisterIntCell GetCell(int x, int y){
		//int 	x2= super.GetNewX(x) - (int)(m_shift_dx.GetVal() * m_time) ,
		//		y2= super.GetNewY(y) - (int)(m_shift_dy.GetVal() * m_time) + m_shift_y;

		return GetCell(Map2D(super.GetNewX(x), super.GetNewY(y)));
	}

	/*--------------------
	 * GFX
	 --------------------*/
	public FLPanel GetSpecificPanel() {
		FLPanel p = super.GetSpecificPanel();
		p.setOpaque(false);
		OpenControlWindowButton button = new OpenControlWindowButton();
		button.defineAsPanelButton(FLColor.c_lightred);
		p.AddGridBag(button,0,1);
		if (super.GetFeedMode() == GFXMODE.ENHANCED) { //a bit curious
			p.Add(new RegularAutomatonViewer.EnhancedDisplayMode());
		}
		return p;
	}

	class StabilityControl extends OnOffControl {
		public StabilityControl() {
			super("stability:", DEFSELECT);
		}
		@Override
		protected void ActionSelection(boolean state) {
			SetStabilityDisplay(state);
			UpdateView();
		}
	}


	/** TODO : transfer this one above 
	Océane CHAZE**/
	class OpenControlWindowButton extends FLActionButton {
		public OpenControlWindowButton() {
			super("Viewer settings");
			setIcon(IconManager.getUIIcon("configMin"));
			defineAsPanelButtonStylized(FLColor.c_lightred);
		}

		@Override
		public void DoAction() {

			m_DisplayStabilityControl= new StabilityControl();
			FLPanel stability = new FLPanel();
			stability.Add(m_DisplayStabilityControl);

			FLPanel setClick = new FLPanel();
			setClick.SetBoxLayoutX();

			setClick.Add(m_MouseMode);
			setClick.Add(m_setCellValOnClick);
			
			FLPanel shift_zqsd= new FLPanel();
			shift_zqsd.SetGridBagLayout();
			shift_zqsd.AddGridBag(m_qButton,0,1);
			shift_zqsd.AddGridBag(m_dButton,2,1);
			shift_zqsd.AddGridBag(m_zButton,1,0);
			shift_zqsd.AddGridBag(m_sButton,1,2);

			FLPanel shift = new FLPanel();
			shift.SetGridBagLayout();
			shift.AddGridBag(m_shift_dx.GetControl(),0,0);
			shift.AddGridBag( m_shift_dy.GetControl(),1,0);

			FLPanel pane = new OneDimensionalCAcontrolPanel(stability, m_MouseMode,m_setCellValOnClick, m_colorPanel,shift, shift_zqsd);


			FLFrame frame = new FLFrame("control viewer");
			frame.addPanel(pane);
			frame.packAndShow();
		}

	}

	public void setMoveButtons(SimulationWindowSingle.MoveButton z, 
			SimulationWindowSingle.MoveButton q, 
			SimulationWindowSingle.MoveButton s, 
			SimulationWindowSingle.MoveButton d){
		m_zButton = z;
		m_qButton = q;
		m_sButton = s;
		m_dButton = d;
	}

	public void setColorPanel(FLPanel colorPanel){
		m_colorPanel = colorPanel;
	}

	/** a viewer may change a cell's state (mouse) */
	public void ProcessMouseEventXY(int x, int y, MouseEvent event) {
		//Macro.Debug(" x,y: "+ x + "," + y + "  :" + GetCell(x, y).GetInfo() );
		switch (m_MouseMode.GetSelectedItemRank()){
		case 0: // flip mode
			int q= m_setCellValOnClick.GetVal() - GetCellColorNumXY(x, y);
			if (q<0) {
				q=0;
			}
			GetCell(x,y).InitState(q);
			break;
		case 1: // set val directly
			GetCell(x,y).InitState(m_setCellValOnClick.GetVal());
			break;
		case 2: // apply rule mode
			GetCell(x,y).sig_UpdateBuffer();
			GetCell(x,y).sig_MakeTransition();
			break;
		default:
			//Macro.print(GetCell(x, y).GetInfo());
			break;
		}
		// updating
		UpdateView();
	}







}
