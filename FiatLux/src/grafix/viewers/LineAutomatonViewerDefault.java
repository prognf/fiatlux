package grafix.viewers;

import components.allCells.OneRegisterIntCell;
import components.arrays.GenericCellArray;
import components.arrays.RegularDynArray;
import components.types.IntC;
import experiment.samplers.CAsampler;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.controllers.OnOffControl;
import grafix.gfxTypes.elements.FLPanel;
import topology.basics.LinearTopology;

/*--------------------
 * enables the display of a 1D automaton the model passed
 * in the constructor is used to interpret the colors
 * @author Nazim Fates
 *--------------------*/
public class LineAutomatonViewerDefault extends LineAutomatonViewer {

	/*--------------------
	 * attributes
	 --------------------*/

	GenericCellArray<OneRegisterIntCell> m_array;

	private static final boolean DEFBLINK = false;

	private static final boolean CENTRALCELL_HIGHLIGHT = false; // for BABEL 30 
	boolean m_blinkingLight= DEFBLINK;

	/*--------------------
	 * construction
	 --------------------*/

	/** regular constructor **/
	public LineAutomatonViewerDefault(IntC cellPixSize, LinearTopology in_topo, 
			RegularDynArray in_automaton, int in_Tsize) {
		super(cellPixSize, in_topo, in_automaton, in_Tsize);
		m_array = new GenericCellArray<OneRegisterIntCell>(in_automaton);
	}

	/** special constructor : form a Sampler **/
	public LineAutomatonViewerDefault(IntC cellPixSize, CAsampler sampler, int Tsize) {
		this(cellPixSize, (LinearTopology)sampler.GetTopologyManager(), sampler.GetAutomaton(), Tsize);
		this.SetPalette(PaintToolKit.GetDefaultPalette());
	}

	/*--------------------
	 * actions
	 --------------------*/

	protected void FlipCellState(int index) {

		int olds= m_array.GetCell(index).GetState();
		int news = 1 - olds;
		//Macro.fDebug("Flip on %d: %d -> %d ",index,olds,news);
		m_array.GetCell(index).SetState(news);	
	}

	/** main: used to find the color of cell (x,y) */
	/*	public int GetCellColorNumXY(int x, int y) {
		int Lsize = m_array.GetSize();
		int newy= GetYsize() - 1 - y;

		if(GetFeedMode()==GFXMODE.ENHANCED){
				int left= 	GetBufferCellState(newy, x-1) ;
				int center= GetBufferCellState(newy, x);
				int right= 	GetBufferCellState(newy, x+1) ;
				if (left== TimeBuffer.RESET_STATE){
					return TimeBuffer.RESET_STATE;
				}
				int st = 4 * left + 2* center + right;
				return st;
		} else {
			return m_buffer.GetBufferCellState(newy, x);
		}
	}
	 */
	/*
	private int GetBufferCellState(int time, int x) {
		if (m_radarmode){
			return m_buffer.GetBufferCellStateRadar(time, x);
		} else {
			return m_buffer.GetBufferCellState(time, x);
		}
	}*/

	@Override
	/** one has to put int values (color codes) in time buffer **/
	public void UpdateBuffer(int[] timeBuffer) {
		int Lsize= timeBuffer.length;
		int t= m_buffer.GetTime();
		for(int pos=0; pos< Lsize; pos++) {
			int state= GetStateForDisplay(pos);
			boolean blink= m_blinkingLight && (t%2==1);
			timeBuffer[pos] = blink?1-state: state  ;
			if (CENTRALCELL_HIGHLIGHT) {/* hack for the central cell*/
				if (pos==(Lsize/2)) {
					timeBuffer[pos]= state*2;
				}
			}
		}

		/*switch(GetFeedMode()){
		case REGULAR:
			for(int pos=0; pos< Lsize; pos++) {
				timeBuffer[pos] = GetStateForDisplay(pos) ;
			}
			break;
		case ENHANCED:
			for(int pos=0; pos< Lsize; pos++) {
				int left= GetStateForDisplay( (pos -1 + Lsize) % Lsize ) ;
				int center= GetStateForDisplay( pos );
				int right= GetStateForDisplay( (pos + 1 ) % Lsize ) ;
				int st = 4 * left + 2* center + right;
				timeBuffer[pos] = st;
			}	
			break;
		default:
			Macro.FatalError("Unexpected feedmode value");
		}*/

	}


	/** GFX control */
	public FLPanel GetSpecificPanel(){
		FLPanel pane = new FLPanel();
		pane.Add(new BlinkControl());

		FLPanel panel = super.GetSpecificPanel();
		super.SetBlinkControlPanel(pane);
		//panel.AddGridBag(new BlinkControl(),0,2);
		return panel;	
	}


	class BlinkControl extends OnOffControl{
		private static final String STR_BLNK = "blinking light";

		public BlinkControl() {
			super(STR_BLNK, m_blinkingLight);
		}

		/** override this one if needed **/
		protected void ActionSelection(boolean b) {
			m_blinkingLight= b;
		}
	}

	private int GetStateForDisplay(int pos) {
		return m_array.GetCell(pos).GetState();
	}

}
