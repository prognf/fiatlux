package grafix.viewers;

import javax.swing.JFrame;

import ptolemy.plot.Plot;

/************************************
 * A DataViewer is a JFrame used to view data
 * @author Nazim Fates
 * Date of creation: Apr. 2002
 *******************************************/
public class DataViewer extends JFrame {

	//e.g. :   0.5  =  50 percent augmentation of range limits
	private static final double GRAPHxSIZE_INCREASE_FACTOR = 0.5;
	private static final double GRAPHySIZE_INCREASE_FACTOR = 0.1;

	boolean m_CONNECTEDPOINTS = false;

	int m_Colour = 0;

	/** We use a constructor here so that we can call methods
	 *  directly on the Frame.  The main method is static
	 *  so getting at the Frame is a little trickier.
	 */
	Plot m_Plot = new Plot();

	/*--------------------
	 * construction methods
	--------------------*/

	public DataViewer() {

		// Set the size of the toplevel window.
		setSize(800, 300);

		// Create the left plot by calling methods.
		m_Plot.setSize(500, 400);
		m_Plot.setButtons(true);
		m_Plot.setTitle("Unnamed Plot");
		m_Plot.setYRange(0, 100);
		m_Plot.setXRange(0, 100);
		m_Plot.setXLabel("Xvalue");
		m_Plot.setYLabel("Yvalue");
		m_Plot.setMarksStyle("none");
		m_Plot.setImpulses(true);

		getContentPane().add(m_Plot);

		this.setVisible(true);
	}

	
	
	
	/*--------------------
	 * dynamical methods
	--------------------*/
	public void Reset() {
		m_Plot.clear(false); //false = do not forget all settings
		repaint();
	}

	public void AddPoint(double x, double y) {
		m_Plot.addPoint(m_Colour, x, y, m_CONNECTEDPOINTS);
		
		double xMax = m_Plot.getXRange()[1];
		double xMin = m_Plot.getXRange()[0];
		double yMax = m_Plot.getYRange()[1];
		double yMin = m_Plot.getYRange()[0];
		
		if (x > xMax){
			double newXmax = x+ Math.abs((GRAPHxSIZE_INCREASE_FACTOR)*x);
			m_Plot.setXRange(xMin, newXmax);
		}
		if (x < xMin){
			double newXmin = x- Math.abs((GRAPHxSIZE_INCREASE_FACTOR)*x);
			m_Plot.setXRange(newXmin, xMax);
		}
		if (y > yMax){
			double newYmax = y+ Math.abs((GRAPHySIZE_INCREASE_FACTOR)*y);
			m_Plot.setYRange(yMin, newYmax);
		}
		if (y < yMin){
			double newYmin = y- Math.abs((GRAPHySIZE_INCREASE_FACTOR)*y);
			m_Plot.setYRange(newYmin, yMax);
		}
	}

	public void AddPoint(int x, int y) {
		AddPoint((double) x, (double) y);
	}

	/*--------------------
	 * Get/Set methods
	--------------------*/

	public void ChangeColour() {
		m_Colour++;
	}

	public void SetConnected(boolean connect) {
		m_CONNECTEDPOINTS = connect;
	}

	public void SetPhasePlotStyle() {
		m_Plot.setBars(false);
		m_Plot.setImpulses(false);
		// m_Plot.setConnected(true);
		this.SetConnected(true);
		m_Plot.setMarksStyle("points");
	}

	public void SetLabels(String XaxisLabel, String YaxisLabel) {
		m_Plot.setXLabel(XaxisLabel);
		m_Plot.setYLabel(YaxisLabel);
	}

	public void SetTitle(String s) {
		m_Plot.setTitle(s);
	}

	public void SetBars(boolean val) {
		m_Plot.setBars(val);
	}

	public void SetRange(int Xmin, int Xmax, int Ymin, int Ymax) {
		m_Plot.setXRange(Xmin, Xmax);
		m_Plot.setYRange(Ymin, Ymax);
	}
	
	public void SetRange(double Xmin, double Xmax, double Ymin, double Ymax) {
		m_Plot.setXRange(Xmin, Xmax);
		m_Plot.setYRange(Ymin, Ymax);
	}
	

	
}
