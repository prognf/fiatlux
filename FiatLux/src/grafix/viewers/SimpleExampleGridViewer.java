package grafix.viewers;

import components.types.IntC;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.elements.FLFrame;

/** ONLY FOR TESTING **/
public class SimpleExampleGridViewer extends GridViewer {

	private static final int NCOLORS = 7;


	public SimpleExampleGridViewer(IntC inCellPixSize) {
		super(inCellPixSize, new IntC(10, 10));
		//SetPalette( PaintToolKit.GetBWpalette(NCOLORS));
		//SetPalette( PaintToolKit.GetRainbow(NCOLORS));
		SetPalette( PaintToolKit.GetRandomPalette(NCOLORS));
	}

	/**
	 * main method
	 * 	 */
	@Override
	protected void DrawSystemState(){
		m_g2D.setColor(FLColor.c_cyan);
		//Rectangle rect = new Rectangle(10, 20);
		//m_g2D.fillRect(10, 20, 100, 200);
		
		DrawSquare(0, 0);
		
		//Macro.fDebug(" sz %d %d",GetXsize(), GetYsize());
		for (int x = 0; x < getXsize(); x++) {
			for (int y = 0; y < getYsize(); y++) {
				int state = (x+y) % NCOLORS; 
				DrawSquareColor(x, y, state);	
			}
		}
	}

	private static void TestSimple() {
		FLFrame frame= new FLFrame("test simple grid");
		IntC CellPixSize= new IntC(40, 2);
		SimpleExampleGridViewer viewer= new SimpleExampleGridViewer(CellPixSize);
		frame.addPanel(viewer);
		frame.packAndShow();		
	}

	public static void main (String[] args){
		TestSimple();
		
	}


	
}
