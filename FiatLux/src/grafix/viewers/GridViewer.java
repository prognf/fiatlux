package grafix.viewers;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.event.KeyListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;

import javax.swing.JCheckBox;

import architecture.multiAgent.tools.DIR;
import components.arrays.GridSystem;
import components.types.DoublePar;
import components.types.FLStringList;
import components.types.IntC;
import components.types.IntCm;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.elements.FLPanel;
import main.Macro;
import main.MathMacro;

/*--------------------
 * displays grids and
 * manages info on display
 *--------------------*/
public abstract class GridViewer extends AutomatonViewer {
	final static String CLASSNAME= Thread.currentThread().getStackTrace()[1].getClassName();
	private static final float DEFAULT_CIRCLE_RATIO = 3.F/5.F;
	private JCheckBox m_showGrid;
	private FLPanel m_colorDescriptor;
	protected DoublePar m_shift_dx, m_shift_dy; /// shifting to follow translating patterns

	protected int m_shift_x, m_shift_y; //shifting the grid with ZQSD keys

	/** A changer OC **/
	public FLPanel GetSpecificPanel(){
		FLPanel pane = new FLPanel();
		//uper.fill = GridBagConstraints.HORIZONTAL
		pane.setOpaque(false);
		pane.SetGridBagLayout();
		pane.Add(m_showGrid);
		if(m_colorDescriptor!=null){
			pane.AddGridBag(m_colorDescriptor,0,2);
		}



		//pane.Add(m_showGrid);
		return pane;
	}

	public void setColorDescriptor(FLPanel colorDescriptor){
		m_colorDescriptor = colorDescriptor;
	}
	
	// implicit :
	// abstract void paintComponent();
	// abstract public void sig_NextStep();
	private void sig_Init() {
		m_time=0;
	}
	@Override
	public void sig_NextStep() {
		m_time++;
	}
	/** if a subviewer needs to know the time for a particular way of drawing **/
	final protected int GetTime() {
		return m_time;
	}

	protected abstract void DrawSystemState();
	//private float m_ResizeCoef= 1;

	@Override
	final public void paintComponent(Graphics gc) {
		//Macro.Debug(" paint");
		SetGFXdevice(gc);
		DrawSystemState();
	}

	/*--------------------
	 * constants
	 --------------------*/

	/*--------------------
	 * attributes
	 --------------------*/
	protected Graphics m_graphix;
	protected Graphics2D m_g2D;

	protected FLColor m_GhostColor = FLColor.DEF_GHOSTCOLOR;
	final protected int m_initCellDist; // distance between 2 cells
	protected int m_CellDist; // distance between 2 cells
	final protected int m_initSquareSize;     // actual size of the cells
	protected int m_SquareSize;     // actual size of the cells
	private IntC m_XYsize;  // grid size (number of cells)
	private boolean m_visibleGrid;
	private int m_time;

	private  Rectangle [][] m_square; // drawing squares

	private MouseFollower m_MouseFollower;
	private KeyListener m_ArrowListener;

	// do we draw a black background ? (useful for SVG)
	protected boolean m_drawBackground = true;
	// background dimensions (does not take account of margins)
	protected IntCm m_backgrndCoord; 

	// special for LGCA
	private AffineTransform m_origin;


	/*--------------------
	 * Constructor
	 --------------------*/
	/** cells dimension (in pixels) in input */
	public GridViewer(IntC cellPixSize, IntC gridDim) {
		m_CellDist = cellPixSize.X() + cellPixSize.Y();
		m_initCellDist = cellPixSize.X() + cellPixSize.Y();
		m_SquareSize = cellPixSize.X();
		m_initSquareSize = cellPixSize.X();
		m_visibleGrid = false;

		//this.setDoubleBuffered(true);
		this.setOpaque(true); // for optimisation of the drawing

		m_XYsize = gridDim;
		if (m_XYsize.prodXY() == 0) {
			Macro.FatalError("Init problem with Viewer.");
		} else {
			Macro.print(3, this, "constr: size " + m_XYsize);
		}
		// for background drawing
		
		//FIXME : curious mechanism 
		m_backgrndCoord= GetBackGroundDimensions();
		UpdateWindowSize(); 
		CreateCellSquares();

	}

	/** extracts the grid size from gridsystem **/
	public GridViewer(IntC in_PixSize, GridSystem in_system) {
		this(in_PixSize, in_system.GetXYsize());
	}

	/** override if needed 
	 * typically : HexagonalViewer has another way of determining the background **/
	protected IntCm GetBackGroundDimensions() {
		return IntCm.Product(m_CellDist,m_XYsize);
	}

	/** regular use : in construction methods 
	 * rectangles are pre-calculated 
	 * TODO : circles for stability display */
	void CreateCellSquares() {
		int Xsize = m_XYsize.X(), Ysize= m_XYsize.Y();
		//Macro.Debug("Creating squares..."+ m_XYsize);
		m_square = new Rectangle [Xsize][Ysize];
		for (int x=0; x < Xsize; x++){
			for (int y=0; y< Ysize; y++){
				int Xpos = getXpos(x);
				int Ypos = getYpos(y);
				m_square[x][y]= 
						new Rectangle(Xpos, Ypos, m_SquareSize, m_SquareSize);			
			}
		}
	}

	/** called by Subclasses constructors */
	protected void ActivateMouseFollower(MouseInteractive interactiveObject){
		m_MouseFollower= new MouseFollower(interactiveObject);
		addMouseListener(m_MouseFollower);
		addMouseMotionListener(m_MouseFollower);
		// Macro.Debug("MOUSE FOLLOW ON");
	}

	/*--------------------
	 * GFX methods
	 --------------------*/
	protected void SetGFXdevice(Graphics gc) {
		m_graphix= gc;
		m_g2D= (Graphics2D)gc;		
		if (m_drawBackground) {
			DrawBackground();
		}
		// for translation and coming back to the origin
		m_origin = m_g2D.getTransform();

	}

	/** draws one square at position x, y */
	final protected void DrawSquare(int x, int y){
		Rectangle square= m_square[x][y];
		//Macro.Debug("square "+square);
		m_g2D.fill(square);
	}

	/** draws one square at position x, y with the colour associated to state */
	final protected void DrawSquareColor(int x, int y, int state){
		SetColor(GetColor(state));
		DrawSquare(x, y);
	}

	/** draws a circle in a particular cell with a ratio in [0,1] **/
	final protected void DrawCellCircle(int x, int y, float ratio){
		int Xpos = getXpos(x);
		int Ypos = getYpos(y);
		float diam= ratio * m_SquareSize ;
		float dx = (m_SquareSize - diam)/ 2 , dy=dx;
		Ellipse2D circle = 
				new Ellipse2D.Float(Xpos + dx, Ypos + dy, diam, diam);
		m_g2D.fill(circle);
	}
	
	/** draws a circle in a particular cell **/
	final protected void DrawCellCircle(int x, int y){
		DrawCellCircle(x, y, DEFAULT_CIRCLE_RATIO);
	}

	/** draws a circle in a particular cell **/
	final protected void DrawCellCircle(IntC xy){
		DrawCellCircle(xy.X(), xy.Y());
	}
	
	/** draws a circle in a particular cell : Y inverted **/
	final protected void DrawCellCircleYinv(IntC xy){
		DrawCellCircle(xy.X(), m_XYsize.Y() - 1 - xy.Y());
	}

	/** draws a circle in a particular cell **/
	final protected void DrawDiamond(IntC xy){
		int Xpos = getXpos(xy.X()), Ypos = getYpos(xy.Y());
		Polygon diamond = new Polygon();
		int C= m_SquareSize;
		diamond.addPoint(C/2, C);
		diamond.addPoint(C,C/2);
		diamond.addPoint(C/2, 0);
		diamond.addPoint(0,C/2);
		diamond.translate(Xpos, Ypos);
		m_graphix.fillPolygon(diamond);
	}

	/** TODO replace by pre-calc ?? */
	final protected void DrawCellTriangle(int x, int y, DIR dir){
		int Xpos = getXpos(x), Ypos = getYpos(y);
		switch(dir){
		case NORTH:
			int [] XXN = { Xpos, Xpos + m_SquareSize, Xpos + m_SquareSize/2};
			int [] YYN = { Ypos + m_SquareSize, Ypos+ m_SquareSize, Ypos };
			m_graphix.fillPolygon(XXN, YYN, 3);
			break;
		case SOUTH:
			int [] XXS = { Xpos, Xpos + m_SquareSize, Xpos + m_SquareSize/2};
			int [] YYS = { Ypos, Ypos, Ypos + m_SquareSize};
			m_graphix.fillPolygon(XXS, YYS, 3);
			break;
		case EAST:
			int [] XXE = { Xpos, Xpos, Xpos + m_SquareSize};
			int [] YYE = { Ypos, Ypos + m_SquareSize, Ypos + m_SquareSize/2};
			m_graphix.fillPolygon(XXE, YYE, 3);
			break;
		case WEST:
			int [] XXW = { Xpos+ m_SquareSize, Xpos+ m_SquareSize, Xpos };
			int [] YYW = { Ypos, Ypos + m_SquareSize, Ypos + m_SquareSize/2};
			m_graphix.fillPolygon(XXW, YYW, 3);
			break;
		}
	}

	/** possible speed-up by precomputation of triangles **/
	final protected void DrawCellParticle(int x, int y, DIR dir){
		int 	Xpos = getXpos(x),
				Ypos = getYpos(y),
				d1 = m_SquareSize/3,
				d2 = m_SquareSize-d1;

		switch (dir) {
		case NORTH:
			int [] XXN = { Xpos+d1, Xpos+d2, Xpos + m_SquareSize/2};
			int [] YYN = { Ypos+d1, Ypos+d1, Ypos };
			m_graphix.fillPolygon(XXN, YYN, 3);
			break;
		case SOUTH:
			int [] XXS = { Xpos+d1, Xpos+d2, Xpos + m_SquareSize/2};
			int [] YYS = { Ypos+d2, Ypos+d2, Ypos + m_SquareSize};
			m_graphix.fillPolygon(XXS, YYS, 3);
			break;
		case EAST:
			int [] XXE = { Xpos+d2, Xpos+d2, Xpos + m_SquareSize};
			int [] YYE = { Ypos+d1, Ypos+d2, Ypos + m_SquareSize/2};
			m_graphix.fillPolygon(XXE, YYE, 3);
			break;
		case WEST:
			int [] XXW = { Xpos+d1, Xpos+d1, Xpos };
			int [] YYW = { Ypos+d1, Ypos+d2, Ypos + m_SquareSize/2};
			m_graphix.fillPolygon(XXW, YYW, 3);
			break;
		}
	}

	public void drawLineAbsoluteCoord(IntC posA, IntC posB){
		m_graphix.drawLine(posA.X(), posA.Y(), posB.X(), posB.Y());
	}

	protected void WriteTextInCell(int x, int y, String str) {
		int Xpos = getXpos(x), Ypos = getYpos(y) + m_CellDist / 2;
		m_graphix.drawString(str, Xpos, Ypos);
	}

	/** draws a background */
	public void DrawUniformCellularBackGround(Color col) {
		/*for (int x = 0; x < GetXsize(); x++) {
			for (int y = 0; y < GetYsize(); y++) {				 
				m_graphix.setColor(col);								
				DrawSquare(x, y);	
			}
		}*/
		// hack to have something with correct margins on the right
		int 	XX= getXsize()*m_SquareSize+m_CellDist, 
				YY= getYsize()*m_SquareSize+m_CellDist;

		Rectangle background= new Rectangle(0,0,XX,YY);
		m_graphix.setColor(col);	
		m_g2D.fill(background);
	}	

	/** by default : draws a grid 
	 * counter-intuitive : first we draw the "background" with "frontCol"
	 * then the cells with "backCol"**/
	final protected void DrawGrid(FLColor frontCol, FLColor backCol){
		SetColor(frontCol);
		DrawBackground();
		SetColor(backCol);
		for (int y = 0; y < getYsize(); y++) {
			for (int x = 0; x < getXsize(); x++)
				DrawSquare(x, y);
		}
	}



	/** by default : dran with col index 0 **/
	final protected void DrawBackgroundCol0(){
		SetColorIndex(0);
		DrawBackground();
	}

	final protected void DrawBackground(){
		//m_graphix.fillRect(XG, YG,
		// XG + m_backgrndCoord.X(), YG + m_backgrndCoord.Y());
		m_graphix.fillRect(0, 0, 
				2 * XG + m_backgrndCoord.X(), 2 * YG + m_backgrndCoord.Y());
	} 

	//OC
	/** Used to resize the GridViewer when the window is resized **/
	public void resizeGrid(IntC viewerSize){
		m_CellDist = (int)(viewerSize.X()/(GetXYsize().X()));
		drawGrid();
	}
	/** Used to update the grid appearance when resized or when the grid is drawn **/
	public void drawGrid(){
		if(m_visibleGrid){
			m_SquareSize = m_CellDist-1;
		}
		else{
			m_SquareSize = m_CellDist;
		}
		CreateCellSquares();
	}
	/** Used when the user change the visibility of the grid **/
	public void ChangeGridVisibility(){
		m_visibleGrid = !m_visibleGrid;
		drawGrid();

	}
	//\OC
	//--------------------------------------------------------------------------
	//- polygon methods
	//--------------------------------------------------------------------------


	public void translateOrigin(int xpos, int ypos) {
		m_graphix.translate(xpos, ypos);
	}

	public void translateOrigin(IntC xypos) {
		translateOrigin(xypos.X(),xypos.Y());
	}
	
	final protected void FillPolygon(Polygon p){
		m_graphix.fillPolygon(p);
	}

	final protected void ResetOrigin(){
		m_g2D.setTransform(m_origin);
	}


	/*--------------------
	 * Get & Set methods
	 --------------------*/


	/** to which cell does pixel (xpos,ypos) correspond ? */
	public IntC GetCellFromCoordinates(int xCoord, int yCoord){
		int 	x= (xCoord - XG) / GetCellDist(), 
				y= (yCoord - YG) / GetCellDist();
		//Macro.Debug( " x " + xCoord + " y " + yCoord);
		int newy = m_XYsize.Y() - 1 - y;
		return new IntC ( x , newy);
	} 

	/** to which cell does pixel (xpos,ypos) correspond ? */
	public IntC GetCellFromCoordinates(IntC xyCoord){
		return GetCellFromCoordinates(xyCoord.X(),xyCoord.Y());
	}

	protected int GetCellDist() {
		return m_CellDist;
	}


	protected IntC GetXYWindowSize() {
		return new
				IntC(	(int)(2 * XG + getXsize() * GetCellDist()),
				(int)(2 * YG + getYsize() * GetCellDist()));
	}

	/** unit :  nb of cells */
	final protected int getXsize(){
		return m_XYsize.X();	
	}

	/** unit :  nb of cells */
	protected final int getYsize(){
		return m_XYsize.Y();
	}

	/** unit :  nb of cells */
	final public IntC GetXYsize(){
		return m_XYsize;
	}


	final protected int getXpos(int x) {
		return XG + x * GetCellDist();
	}

	final protected int getYpos(int y) {
		return YG + (getYsize() - 1 - y) * GetCellDist();
	}
	
	final protected IntC getXYpos(int x,int y){
		return new IntC(getXpos(x), getYpos(y));
	}

	/** MAPPING from 2D to 1D returning pos (int) **/ 
	final protected int Map2D(int x, int y){
		return x + getXsize() * y;
	}

	/** print method */
	public FLStringList printAutomatonState(){
		FLStringList list = new FLStringList();
		list.Add(""+ m_XYsize);
		for (int x = 0; x < getXsize(); x++) {
			for (int y = 0; y < getYsize(); y++) {
				list.Add(GetCellFromCoordinates(x, y).toString());
			}
		}
		return list;
	}	

	protected void SetColor(Color color) {
		m_graphix.setColor(color);
	}

	protected void SetColorIndex(int colnum) {
		Color color= GetColor(colnum);
		m_graphix.setColor(color);
	}


	/** dangerous !
	 * used only for "copying" automaton viewers */
	public IntC GetPixCellSize() {
		return new IntC( GetCellDist(), m_CellDist - m_SquareSize);
	}

	/** for LGCA **/
	public void ComputeFourTriangles(Polygon tN, Polygon tE,
			Polygon tS, Polygon tW) {
		//Macro.Debug("computing four triangles...");
		int		l= m_SquareSize, h= m_SquareSize/2,
				a = m_SquareSize/3,	b = m_SquareSize-a;
	//	tN= new Polygon();		tE= new Polygon();
	//	tS= new Polygon();		tW= new Polygon();
		ComputeTriangle(tN, a, a, b, a, h, 0);
		ComputeTriangle(tE, b, a, b, b, l, h);
		ComputeTriangle(tS, a, b, b, b, h, l);
		ComputeTriangle(tW, a, a, a, b, 0, h);

	}
	
	public void ComputeFourHeadCenteredTriangles(Polygon tN, Polygon tE,
			Polygon tS, Polygon tW) {
		int		h= m_SquareSize/2, a = m_SquareSize/4;
		ComputeTriangle(tN, 0, 0, -a, h, a, h);
		ComputeTriangle(tS, 0, 0, -a, -h, a, -h);
		ComputeTriangle(tW, 0, 0, h, -a, h, a);
		ComputeTriangle(tE, 0, 0, -h, -a, -h, a);
	}
	
	private void ComputeTriangle(Polygon p, 
			int x1, int y1, int x2, int y2, int x3, int y3) {
		p.addPoint(x1, y1);
		p.addPoint(x2, y2);
		p.addPoint(x3, y3);
	}


	protected IntCm GetLastClickCoordfromMouseFollower() {
		return m_MouseFollower.GetLastClickCoord();
	}

	//OC
	public void setVisibleGrid(JCheckBox visibleGrid){
		m_showGrid = visibleGrid;
	}

	public void zoom(boolean zoom){
		if(zoom){
			m_CellDist +=1;
		}
		else{
			m_CellDist -=1;
		}
		drawGrid();
	}

	/** Return new positions of the cell after shift + shift with time*/
	protected int GetNewX(int posX){
		int 	x2= posX + m_shift_x;
		int newX= MathMacro.Wrap(x2, getXsize());
		return newX;
	}
	protected int GetNewY(int posY){
		int y2= posY+ m_shift_y;
		int newY= MathMacro.Wrap(y2, getYsize());
		return newY;
	}
		//\OC




	public void pressZ(){
		m_shift_y +=1;
	}
	public void pressQ(){
		m_shift_x -=1;
	}
	public void pressS(){
		m_shift_y -=1;
	}
	public void pressD(){
		m_shift_x +=1;
	}

	/*public void center(){
		m_shift_x = (this.getWidthInPixels()-m_CellDist*GetXsize())/(2*GetXsize());
		m_shift_y = (this.getHeightInPixels()-m_CellDist*GetYsize())/(2*GetYsize());
		//this.getHeightInPixels()
	}*/

	/*public class MouseWheelEvent implements MouseWheelListener {
		public MouseWheelEvent() {

		}

		@Override
		public void mouseWheelMoved(java.awt.event.MouseWheelEvent e) {
			int notches = e.getWheelRotation();
			if (notches < 0) {
				System.out.println("Mouse wheel moved UP ");
			} else {
				System.out.println("Mouse wheel moved DOWN ");
			}
		}
	}*/

	//\OC

	
}