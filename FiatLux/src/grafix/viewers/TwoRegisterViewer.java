package grafix.viewers;

import components.allCells.TwoRegisterCell;
import components.arrays.GenericCellArray;
import components.arrays.RegularDynArray;
import components.types.DoublePar;
import components.types.IntC;
import main.Macro;


abstract public class TwoRegisterViewer extends RegularAutomatonViewer {

	/*--------------------
	 * Attributes
	 --------------------*/
	private GenericCellArray<TwoRegisterCell> m_genArray;
	

	public TwoRegisterViewer(IntC in_CellSize, IntC in_GridSize, RegularDynArray in_Automaton) {
		super(in_CellSize,in_GridSize);
		m_genArray= new GenericCellArray<TwoRegisterCell>(in_Automaton);
		m_shift_x = 0;
		m_shift_y = 0;
		m_shift_dx= new DoublePar("DX", 0);
		m_shift_dy= new DoublePar("DY", 0);
	}

	final private TwoRegisterCell GetCell(int x, int y){
		return m_genArray.GetCell(Map2D(super.GetNewX(x), super.GetNewY(y)));
	}
	

	protected int GetStateOneXY(int x, int y) {
		return GetCell(x,y).GetStateOne(  );
	}

	
	protected int GetStateTwoXY(int x, int y) {		
		return GetCell(x,y).GetStateTwo( );
	}

	protected void SetStateOneXY(int x, int y, int state) {
		GetCell(x,y).SetStateOne(state);
	}
	
	protected void zzzSetStateTwoXY(int x, int y, int state) {
		GetCell(x,y).SetStateTwo(state);
	}

	
	
	/*--------------------
	 * Other methods
	 --------------------*/
	
	protected boolean IsStableXY(int x, int y) {
		Macro.FatalError("Not implemented");
		return false;
	}

}
