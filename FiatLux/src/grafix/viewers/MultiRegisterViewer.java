package grafix.viewers;

import components.allCells.MultiRegisterCell;
import components.arrays.GenericCellArray;
import components.arrays.RegularDynArray;
import components.types.DoublePar;
import components.types.IntC;
import main.Macro;


abstract public class MultiRegisterViewer extends RegularAutomatonViewer {

	private static final int REGISTERZERO = 0, REGISTERONE=1;
	/*--------------------
	 * Attributes
	 --------------------*/
	private GenericCellArray<MultiRegisterCell> m_genArray;
	

	public MultiRegisterViewer(IntC in_CellSize, IntC in_GridSize, RegularDynArray automaton) {
		super(in_CellSize,in_GridSize);
		m_genArray= new GenericCellArray<MultiRegisterCell>(automaton);
		m_shift_x = 0;
		m_shift_y = 0;
		m_shift_dx= new DoublePar("DX", 0);
		m_shift_dy= new DoublePar("DY", 0);
	}

	final private MultiRegisterCell GetCell(int x, int y){
		return m_genArray.GetCell(Map2D(super.GetNewX(x), super.GetNewY(y)));
	}

	protected int GetStateIXY(int register, int x, int y){ return GetCell(x,y).GetStateI(register);}
	
	/*--------------------
	 * special for two states (see also "Chemical"...)
	 --------------------*/
	

	protected int GetStateAXY(int x, int y) {
		return GetCell(x,y).GetStateA();
	}

	protected int GetStateBXY(int x, int y) {		
		return GetCell(x,y).GetStateB();
	}

	protected void SetStateAXY(int x, int y, int state) {
		GetCell(x,y).SetStateA(state);
	}
	
	protected void zzzSetStateBXY(int x, int y, int state) {
		GetCell(x,y).SetStateB(state);
	}

	
	
	/*--------------------
	 * Other methods
	 --------------------*/
	
	protected boolean IsStableXY(int x, int y) {
		Macro.FatalError("Not implemented");
		return false;
	}

}
