package grafix.viewers;

import java.awt.Color;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;

import components.types.IntC;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.controllers.OnOffControl;
import main.Macro;
import topology.basics.SuperTopology;

/*--------------------
 * A RegularAutomatonViewer implements a standard painting procedure 
 * it manages a palette
*--------------------*/
abstract public class RegularAutomatonViewer extends GridViewer {
	/**
	 * @return the colour index associated to the cell state
	 */
	public abstract int GetCellColorNumXY(int x, int y);

	/**
	 * @return true if the cell is stable
	 */
	abstract protected boolean IsStableXY(int x, int y);

	/*--------------------
	 *  constants
	 --------------------*/	

	private static final Color COL_UNSTABLECELL = FLColor.BLACK;
	final private static boolean DEF_SHOWSTABILITY= false;

	/*--------------------
	 *  attributes
	 --------------------*/	

	BufferedImage m_bImage;

	protected boolean m_StabilityDisplay = DEF_SHOWSTABILITY; 


	final private  Ellipse2D [][] m_circle; // drawing circles for unstable cells

	// updating with regular states or enhanced states (neighb. colors)
	public enum GFXMODE { REGULAR, ENHANCED }

	private GFXMODE m_FeedMode= GFXMODE.REGULAR;



	//--------------------------------------------------------------------------
	//- construction
	//--------------------------------------------------------------------------


	public RegularAutomatonViewer(IntC in_CellPixSize, IntC in_Gridsize) {
		super(in_CellPixSize,in_Gridsize);
		//pre-calculation of circles
		int Xsize= getXsize(), Ysize= getYsize();
		m_circle = new Ellipse2D [Xsize][Ysize];
		for (int x=0; x < Xsize; x++){
			for (int y=0; y< Ysize; y++){
				int Xpos = getXpos(x);
				int Ypos = getYpos(y);

				float rad= m_SquareSize / 2;
				int dx = m_SquareSize/4, dy=dx;
				m_circle[x][y] = new Ellipse2D.Float(Xpos + dx, Ypos + dy, rad, rad); 
				//g.fillOval(Xpos + rad, Ypos + rad, 2*rad , 2*rad);
			}
		}
		//SetXYsize(in_Gridsize);

	}

	/*--------------------
	 * main
	 --------------------*/
	/** main component : draws a coloured grid  
	 * possibly adds circles for unstable cells */
	@Override
	protected void DrawSystemState() {
		super.DrawUniformCellularBackGround(FLColor.black);
		//Macro.print(6, "painting...");
		//		m_bImage = (BufferedImage) createImage(getWidth(), getHeight());

		
		for (int x = 0; x < getXsize(); x++) {
			for (int y = 0; y < getYsize(); y++) {
				int state = GetCellColorNumXY(x, y); 
				FLColor col;
				if (state != SuperTopology.GHOSTSTATE) {
					col = GetColor(state);
				} else {
					//color for "ghost" cells (holes)
					col = m_GhostColor;
				}

				SetColor(col);								
				DrawSquare(x, y);

			}
		}
		// drawing unstability
		SetColor(COL_UNSTABLECELL);		
		if (m_StabilityDisplay){
			for (int x = 0; x < getXsize(); x++) {
				for (int y = 0; y < getYsize(); y++) {
					if (!IsStableXY(x,y)){DrawUnstabilitySymbol(x, y);}
				}
			}
		}

		DrawPostEffect();
	}

	

	/**
	 * Override this if post drawing is needed
	 * @param gc the Graphics2D
	 */
	
	protected void DrawPostEffect() {
		//by default, do nothing
		//		/Macro.Debug("printing palette");
		//m_palette.Print();
	}

	/** sets stability display on or off **/
	public void SetStabilityDisplay(boolean stabilityDisplay) {
		m_StabilityDisplay = stabilityDisplay;
	}

	final protected void DrawUnstabilitySymbol(int x, int y){
		m_g2D.fill(m_circle[x][y]);
	}
	
	protected GFXMODE GetFeedMode(){
		return m_FeedMode;
	}

	public void SetEnhanced(PaintToolKit paint){
		Macro.print(5,"Enhanced mode on...");
		m_FeedMode= GFXMODE.ENHANCED;
		SetPalette(paint);
	}


	/** control of the enhanced display **/
	protected class EnhancedDisplayMode extends OnOffControl {

		public EnhancedDisplayMode() {
			super("col+:", m_FeedMode==GFXMODE.ENHANCED);
		}

		@Override
		protected void ActionSelection(boolean state) {
			m_FeedMode=state?GFXMODE.ENHANCED:GFXMODE.REGULAR;
			UpdateView();
		}

	}

	final public int GetCellColorNumXY(IntC xy) {
		return GetCellColorNumXY(xy.X(), xy.Y());
	}
	
	/*
	 * MOUSE
	 */
	
	

}
