package grafix.viewers;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JComponent;

import components.types.FLStringList;
import components.types.FLsignal;
import components.types.IntC;
import components.types.Monitor;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.FilePaintable;
import grafix.gfxTypes.MacroGFX;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.elements.FLPanel;
import grafix.gfxTypes.imgFormat.ImgFormat;
import main.Macro;


abstract public class AutomatonViewer extends JComponent 
implements FilePaintable, Monitor
{

	//final 
	public enum RecordStyle { BASIC, MESSAGE }

	public final static int
	XSHIFT_MESSAGE=40, YSHIFT_MESSAGE=12; // for messages when recording in files

	abstract protected IntC GetXYWindowSize();

	/** abstract: override if a panel is needed to control the viewer */
	public abstract FLPanel GetSpecificPanel();


	// TODO : check why reset of margins DOES NOT work
	protected int XG = 1, YG = 1, SMALLMARGIN=1 ; // "margins"
	private final static int MARGIN_BASIC= 0;

	// palette
	private PaintToolKit m_palette;


	private IntC m_XYwindowSize;
	private Color m_MessageRecordingColor = FLColor.white; 
	private RecordStyle ini_RecordStyle = RecordStyle.BASIC;
	RecordStyle m_RecordStyle = ini_RecordStyle ;   		   // record style (with or without margins, etc.)
	private Dimension m_dimension;
	private float m_resizeCoef  =1;

	//OC
	private IntC m_InitWindowSize;
	public void setm_InitWindowSize(IntC initWindowSize){
		m_InitWindowSize = initWindowSize;
	}
	//\OC

	/** implements FilePaintable */
	@Override
	public int getWidthInPixels(){
		return m_XYwindowSize.X();
	}

	/** implements FilePaintable */
	@Override
	public int getHeightInPixels(){
		return m_XYwindowSize.Y();
	}


	/** called during the construction 
	 * after margins have changed
	 * and while resizing the window */
	public void UpdateWindowSize() {
		m_XYwindowSize = GetXYWindowSize();
		Macro.print(2, this, "window size set to : " + m_XYwindowSize);
		m_dimension= new Dimension((int)(m_XYwindowSize.X()),(int)( m_XYwindowSize.Y()));
	}

	//OC
	public void setResizeCoef(float resizeCoef){
		m_resizeCoef = resizeCoef;
	}
	public void setWindowSize(IntC windowSize){m_XYwindowSize = windowSize;}
	//\OC

	public Dimension getPreferredSize() {
		return m_dimension;
	}

	public Dimension getMinimumSize() {
		return m_dimension;
	}

	public Dimension getMaximumSize() {
		return m_dimension;
	}

	public float getResizeCoef(){
		return m_resizeCoef;
	}

	/*--------------------
	 * signals
	 --------------------*/
	@Override
	public void ReceiveSignal(FLsignal sig){
		//Macro.Debug(" sigreceived:" + sig);
		switch(sig){
		case init:
			// ???
			break;
		case update:
			UpdateView();
			break;
		case nextStep:
			sig_NextStep();
			break;
		}
	}

	final protected void UpdateView() {
		this.repaint();		
	}

	/** override this if needed */
	protected void sig_NextStep() {
		//Macro.Debug(" I will do nothing for next step... <ACHTUNG !>");
		// by default do nothing		
	}


	public void io_Save(String filename, String Message, ImgFormat SaveFormat){
		//SetMargins(SMALLMARGIN, SMALLMARGIN);
		// recording
		if (SaveFormat==ImgFormat.TXT) {
			writeAsText(filename);
		} else { // GFX format
			FilePaintable paintableObject= this;
			MacroGFX.Save2Dimage(paintableObject, filename, Message, SaveFormat);
		}
	}

	final static  String SEP="", ENDLINE="";
	
	protected void writeAsText(String filename) {
		if (this instanceof RegularAutomatonViewer) {
			FLStringList output= new FLStringList();
			RegularAutomatonViewer av=(RegularAutomatonViewer) this;
			int X= av.getXsize() , Y= av.getYsize();
			for (int y=0; y<Y;y++) {
				String line="";
				for (int x=0; x<X;x++) {
					line+= av.GetCellColorNumXY(x,y)+SEP;
				}
				line+=ENDLINE;
				output.Add(line);
			}
			output.WriteToFile(filename+ImgFormat.TXT.extension());
			
		} else {
			Macro.print("*** SORRY UNSUPPORTED VIEWER FOR TXT system state export");
		}
		
	}



	public void SetMargins(int xMargin, int yMargin) {
		XG= xMargin;
		YG = yMargin;
		UpdateWindowSize();
	}

	/*  FilePaintable : printing some more info */
	public void paintComponentForRecording(Graphics g, String in_message) {

		//Macro.Debug("rec. style " + m_RecordStyle);
		switch(m_RecordStyle){
		case MESSAGE:
			g.setColor(m_MessageRecordingColor);
			// we print the copyright ???
			String message = MacroGFX.COPYRIGHT;
			message += " " + in_message;
			g.drawString(message, XSHIFT_MESSAGE, YSHIFT_MESSAGE);
			break;
		case BASIC:
		default:
			break;
		}

		//Paints the actual stuff
		this.paintComponent(g);
	}

	/* setting the record style (basic, etc...) */
	public void SetRecordStyle(RecordStyle STYLE){
		m_RecordStyle= STYLE;
		if (m_RecordStyle == RecordStyle.BASIC){
			XG=MARGIN_BASIC; YG=MARGIN_BASIC;
			UpdateWindowSize();
			//Macro.Debug("Window size updated");
		} else {
			Macro.FatalError("Unrecognized record style");
		}
	}

	/*--------------------
	 * Palette methods
	 --------------------*/
	final protected FLColor GetColor(int colIndex) {
		try{
			FLColor c= m_palette.GetColor(colIndex); 
			return c;
		} catch (NullPointerException e){
			Macro.FatalError(e,"Empty palette? Use SetPalette before...");
			return null;
		} catch (Exception e){
			//e.printStackTrace();
			Macro.FatalError(e,"color error with index:" + colIndex + 
					" size:" + m_palette.GetSize() +
					". Use SetPalette before...");
			return null;
		}
	}

	/** setting the colors for the viewer */
	public void SetPalette(PaintToolKit palette){
		//Macro.Debug("setting new palette of size :" + palette.GetSize() );
		//Exception e= new Exception();
		//e.printStackTrace();
		//palette.Print();
		m_palette= palette;
	}

	@Deprecated
	/** use SetPalette with PaintToolKit instead **/
	protected void SetPalette(FLColor[] colArray) {
		SetPalette( new PaintToolKit(colArray));
	}

	protected void CreatePalette(FLColor ... colorList) {
		m_palette= new PaintToolKit();
		for (FLColor color : colorList){
			m_palette.AddColor(color);
		}
	}

	/**/
	public void drawCircle(Graphics g, int x, int y, int R) {
		g.drawOval(x - R, y - R, R*2, R*2);
	}

	public void fillCircle(Graphics g, int x, int y, int R) {
		g.fillOval(x - R, y - R, R*2, R*2);
	}


}
