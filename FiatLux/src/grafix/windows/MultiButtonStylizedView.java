package grafix.windows;

import java.awt.Color;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import grafix.gfxTypes.IconManager;
import grafix.gfxTypes.elements.FLButton;
import grafix.gfxTypes.elements.FLPanel;

/************************************
 * for experiments that use n buttons 
 * @author : Nazim Fates
 * Date of creation: Nov. 2002
 *******************************************/ 


public abstract class MultiButtonStylizedView extends FLPanel implements ActionListener {
	abstract protected void DoAction(int index);

	final int m_NBUTTONS;
	final FLButton [] m_Button;

	public MultiButtonStylizedView(String[] labels, String[] icons){
		m_NBUTTONS = labels.length;
		m_Button = new FLButton[m_NBUTTONS];

		Insets margin = new Insets(10, 10, 5, 10); // for margins
		for (int i=0; i < m_NBUTTONS; i++ ){
			m_Button[i] = new FLButton(labels[i]);
			m_Button[i].setIcon(IconManager.getUIIcon(icons[i]));
			m_Button[i].setBackground(Color.DARK_GRAY);
			m_Button[i].setBorderPainted(false);
			m_Button[i].setFocusPainted(false);
			m_Button[i].setMargin(margin);
			m_Button[i].setForeground(Color.WHITE);
			m_Button[i].setVerticalTextPosition(JButton.BOTTOM);
			m_Button[i].setHorizontalTextPosition(JButton.CENTER);
		}

		for (int i=0; i< m_NBUTTONS; i++ ){
			m_Button[i].addActionListener(this);
		}

	}

	public FLButton getButtonById(int id) {
		return m_Button[id];
	}

	public FLPanel GetButtonPanel() {
		FLPanel panel = new FLPanel();
		panel.setOpaque(false);
		panel.SetGridBagLayout();
        panel.GetGridBagC().weightx = 0.5;

		for (int i = 0; i < m_NBUTTONS; i++ ) {
			panel.AddGridBag(m_Button[i], i, 10);
		}

		FLPanel container = FLPanel.NewPanel(panel);
		container.getInsets().set(0, 0, 0, 0);
		return container;
	}

	public void actionPerformed(ActionEvent e){
		for (int i = 0; i < m_NBUTTONS; i++){
			if (e.getSource() == m_Button[i]){
				DoAction(i);
				return;
			}
		}
	}
}
