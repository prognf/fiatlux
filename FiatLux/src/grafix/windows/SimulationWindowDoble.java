package grafix.windows;

import java.util.ArrayList;

import components.randomNumbers.FLRandomGenerator;
import components.types.FLsignal;
import components.types.Monitor;
import components.types.RuleCode;
import experiment.samplers.CAsampler;
import experiment.samplers.CAsimulationSampler;
import experiment.samplers.SuperSampler;
import grafix.gfxTypes.elements.FLActionButton;
import grafix.gfxTypes.elements.FLFrame;
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.LineAutomatonViewerDifference;
import main.Macro;
import models.CAmodels.tabled.ECAmodel;
import updatingScheme.AlphaScheme;
import updatingScheme.UpdatingScheme;

/******************
 * 
 * class constructs a simulation window 
 * Left: Divided into regions Actions, Simulations, Info, Plots
 * Right: Viewer for current configuration 
 */

public class SimulationWindowDoble extends SimulationWindowAbstract {

	private static final String NAME = "SimulationWindowDoble";
	private static final String TXT_SHOW_DIFF = "Show difference";
	private static final String TXT_SYNCH_UPD = "Coalescence EXP ECA 46";
	
	// main linked objects
	protected SuperSampler m_samplerA, m_samplerB;
	LineAutomatonViewerDifference m_diffViewer; 

	private java.util.ArrayList<Monitor> m_updatable; 

	public SimulationWindowDoble(String in_str, SuperSampler samplerA, SuperSampler samplerB) {
		super(in_str);
		m_samplerA = samplerA;
		m_samplerB = samplerB;

		m_updatable= new ArrayList<Monitor>(); 
		m_updatable.add(samplerA);
		m_updatable.add(samplerB);
		// adding a third element : diff viewer
		if (m_samplerA instanceof CAsimulationSampler){
			if ( ((CAsimulationSampler)m_samplerA).GetModelName() == ECAmodel.NAME) {
				m_diffViewer= 
						new LineAutomatonViewerDifference(
								(CAsimulationSampler)m_samplerA, (CAsimulationSampler)m_samplerB);
				m_updatable.add(m_diffViewer);
			}
		}
		/*		if (m_samplerA instanceof ConfigRecorder){ //FIXME Hack here !
			ConfigRecorder recorder= (ConfigRecorder) m_samplerA;
			m_SavePanel= new SaveFeaturesPanel(recorder);
			in_Sampler.AddUpdatable(m_SavePanel);
		}*/
		//sig_UpdateView();
	}

	/** Creates all that appears in an experiment 
	 * can be called externally (e.g. for applets) **/
	final public FLPanel GetSimulationPanel() {
		FLPanel leftPanel = super.GetLeftCommonPanel();
		leftPanel.AddGridBagY(new ShowdiffButton());
		leftPanel.AddGridBagY(new SynchUpdateButton());
		
		// RIGHT - SAMPLER
		FLPanel gridAndInitA = FLPanel.NewPanelVertical(
				m_samplerA.GetSimulationWidgets(),
				m_samplerA.GetSimulationView()) ;

		FLPanel gridAndInitB = FLPanel.NewPanelVertical(
				m_samplerB.GetSimulationWidgets(),
				m_samplerB.GetSimulationView()) ;

		// main arrangement of panels
		FLPanel MainPanel = new FLPanel();
		MainPanel.SetBoxLayoutX();

		//MainPanel.Add(leftPanel, gridAndInitA, gridAndInitB);
		MainPanel.Add(leftPanel);

		// first independent frame
		FLFrame frameIndep= new FLFrame("Simulation doble : evolutions ");
		FLPanel twoSimulationsPanels= new FLPanel();
		twoSimulationsPanels.Add(gridAndInitA, gridAndInitB);
		frameIndep.addPanel(twoSimulationsPanels);
		frameIndep.packAndShow();


		return MainPanel;
	}

	/*-----------------------------------------------------------------------
	 * OBJECTS, BUTTONS, FIELDS
	 *---------------------------------------------------------------------*/

	/** opens a plot in a new window */
	protected void OpenPlot(){
		Macro.UserWarning("Not implemented !");
	}



	/** nothing here */
	protected void SetPlotterSelector(){
		//Macro.UserWarning("SetPlotterSelector Not implemented !");
	}

	class ShowdiffButton extends FLActionButton {

		public ShowdiffButton() {
			super(TXT_SHOW_DIFF);
		}
		// second independent frame
		@Override
		public void DoAction() {
			FLFrame diffFrame= new FLFrame("Simulation doble : difference ");
			diffFrame.addPanel(m_diffViewer);
			diffFrame.packAndShow();
		}

	}

	
	class SynchUpdateButton extends FLActionButton {

		public SynchUpdateButton() {
			super(TXT_SYNCH_UPD);
		}
		
		@Override
		public void DoAction() {
			CAsampler samplerA= (CAsimulationSampler)m_samplerA;
			CAsampler samplerB= (CAsimulationSampler)m_samplerB;
			UpdatingScheme sA= samplerA.GetUpdatingScheme();
			UpdatingScheme sB= samplerB.GetUpdatingScheme();
			
			int newSeed= FLRandomGenerator.GetNewSeed();
			sA.GetRandomizer().setSeedFixedAndReset(newSeed);
			sB.GetRandomizer().setSeedFixedAndReset(newSeed);
			AlphaScheme ssA= (AlphaScheme)sA;
			AlphaScheme ssB= (AlphaScheme)sB;
			ssA.SetSynchronyRate(0);
			ssB.SetSynchronyRate(0);
			ECAmodel mA= (ECAmodel)(samplerA.GetCellularModel());
			ECAmodel mB= (ECAmodel)(samplerB.GetCellularModel());
			mA.SetRule(new RuleCode(46));
			mB.SetRule(new RuleCode(46));
			this.repaint();
		}

	}

	/*-----------------------------------------------------------------------
	 * SIGNALS
	 *---------------------------------------------------------------------*/

	/** updates the information of the Window 
	 * should be improved (uses only model A info **/
	protected void sig_UpdateWindowInfo() {
		//modelName.setText("Model A: "+ m_samplerA.GetSimulationInfo());
		modelLabel.setText("Model A: "+ m_samplerA.GetSimulationInfo());
		m_TopologyInfo.setText( m_samplerA.GetTopologyInfo());
		m_TopologyInfoM.setText( m_samplerA.GetTopologyInfo());
		m_SimulationState.setText(S_SIMULATIONSTATE1 + m_samplerA.GetTime() + S_SIMULATIONSTATE2 + (m_samplerA.GetTime() > 1 ? "s" : ""));
		m_SimulationStateM.setText(S_SIMULATIONSTATE1 + m_samplerA.GetTime() + S_SIMULATIONSTATE2 + (m_samplerA.GetTime() > 1 ? "s" : ""));
	}

	protected void sig_Close(){
		Macro.print(4,"closing window");
		m_samplerA.sig_Close();
		m_samplerB.sig_Close();
		this.stopThread();
		CloseWindow();  // 
	}

	/* reset of experiment */
	public void sig_Init() {
		Macro.PrintInitSignal(2, this);
		for (Monitor simu : m_updatable){
			simu.ReceiveSignal(FLsignal.init);
		}
	}


	/* one basic step */
	public void sig_NextStep() {
		for (Monitor simu : m_updatable){
			simu.ReceiveSignal(FLsignal.nextStep);
		}
	}

	/*  shows the current gen */
	@Override
	final public void sig_UpdateView() {
		for (Monitor simu : m_updatable){
			simu.ReceiveSignal(FLsignal.update);
		}
		sig_UpdateWindowInfo();
	}

	/*-----------------------------------------------------------------------
	 * OTHERS
	 *---------------------------------------------------------------------*/
}
