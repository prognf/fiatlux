package grafix.windows;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;

import grafix.gfxTypes.elements.FLActionButton;
import grafix.gfxTypes.elements.FLFrame;
import grafix.gfxTypes.elements.FLPanel;
import main.FiatLux;
import main.FiatLuxProperties;
import main.Macro;
import main.designPanels.CAdesignerPanel;
import main.designPanels.IPSdesignerPanel;
import main.designPanels.LGdesignerPanel;
import main.designPanels.MASdesignerPanel;

/*--------------------
 * model design window
 * will display four designerPanels that can create a Sampler & SamplerWindow
 *--------------------*/
public class SimulationWindowCreator 
extends FLFrame {

	final static int DEF_SELECTTOPO=1;

	/*--------------------
	 * Attributes
	 --------------------*/

	FLPanel m_mainpanel = new FLPanel();
	FLPanel m_selectTypeOfSystem= new FLPanel();
	SelectSystemButton m_CAbutton, m_LGbutton, m_MASbutton, m_IPSbutton;
	
	
	//OC
	final static Color colorSelectedButton = Color.getHSBColor(0,0,83);//Color of SelectPaneButton when selected
	int buttonWidth = 180;//Width of SelectPaneButton
	final static Border border = BorderFactory.createLineBorder(new Color(122,138,153) );
	final static Border emptyBorder = BorderFactory.createEmptyBorder();
	//\OC

	/*--------------------
	 * Constructor
	 --------------------*/

	public SimulationWindowCreator() {
		super(FiatLux.FIATLUX_VERSION);


		AddExitSystemWhenClosedFeature();

		// adding the window panels
		this.addPanel(GetSpecificPanel());

		// Use AddScrollPane for scroll pane
		WindowListener listener = new WindowListener();
		listener.setWindow(this);
		this.addComponentListener(listener);

		
		// to be improved : selection of SystemType by reading properties (simulation of button clicking) 
		int selectedIndexPanel=FiatLuxProperties.GetSystemType();
		final SelectSystemButton [] buttonArray = { m_CAbutton, m_LGbutton, m_MASbutton, m_IPSbutton }; // for auto-selection
		if (selectedIndexPanel<4) { // number of buttons
			buttonArray[selectedIndexPanel].DoAction();
		} else {
			Macro.SystemWarning(" something wrong with systemTypeSelection (initial)");
		}
	}

	/*--------------------
	 * main
	 --------------------*/
	/** the order for adding tab is important 
	 * association button <-> panel*/
	protected JComponent GetSpecificPanel() {
		m_selectTypeOfSystem.SetBoxLayoutY();
		FLPanel buttonPanel = new FLPanel();
		//m_mainpanel.SetGridBagLayout();
		m_mainpanel.SetBorderLayout();
		buttonPanel.SetBoxLayoutY();
		buttonPanel.setPreferredSize(new Dimension(buttonWidth,500));
		buttonPanel.setBackground(Color.WHITE);

		// CA
		CAdesignerPanel CApanel = new CAdesignerPanel();
		m_CAbutton = new SelectSystemButton("Cellular Automata", CApanel); // association button <-> panel
		buttonPanel.Add(m_CAbutton);
	
		//  LGCA (Olivier)
		final LGdesignerPanel LGCApanel = new LGdesignerPanel();
		FLPanel lgca = LGCApanel.GetPanelForExternalInclusion(); 
		m_LGbutton = new SelectSystemButton("LGCA",lgca);  // association button <-> panel
		buttonPanel.Add(m_LGbutton);

		// SMA
		final MASdesignerPanel MASpanel = new MASdesignerPanel();
		FLPanel mas = MASpanel.GetPanelForExternalInclusion();
		m_MASbutton = new SelectSystemButton("Multi-Agent",mas);  // association button <-> panel
		buttonPanel.Add(m_MASbutton);

		// IPS
		final IPSdesignerPanel IPSpanel = new IPSdesignerPanel();
		FLPanel ips = IPSpanel.GetPanelForExternalInclusion();
		m_IPSbutton = new SelectSystemButton("Interacting Particles",ips);  // association button <-> panel
		buttonPanel.Add(m_IPSbutton);

		// main panel
		
		m_selectTypeOfSystem = CApanel;

		m_mainpanel.add(buttonPanel,BorderLayout.WEST);
		m_mainpanel.add(m_selectTypeOfSystem,BorderLayout.CENTER); //initial adding

		Border emptyBorder = BorderFactory.createEmptyBorder();
		m_CAbutton.setBackground(colorSelectedButton);
		m_CAbutton.setBorder(emptyBorder);

		LGCApanel.updateSelection();
		MASpanel.updateSelection();
		IPSpanel.updateSelection();

		return m_mainpanel;
	}

	/*--------------------
	 *  Get & Set methods
	 --------------------*/
	private void selectSystemType(FLPanel systemPanel) {
		m_mainpanel.remove(m_selectTypeOfSystem);
		//Macro.Debug("Change button OC1");
		m_selectTypeOfSystem = systemPanel; // 
		SwingUtilities.updateComponentTreeUI(this); // ???
		setBackground(Color.WHITE);

		m_mainpanel.add(m_selectTypeOfSystem,BorderLayout.CENTER);
		//UpdateButtonSize();
		m_CAbutton.setBackground(Color.WHITE);
		m_CAbutton.setBorder(border);
		m_LGbutton.setBackground(Color.WHITE);
		m_LGbutton.setBorder(border);
		m_MASbutton.setBackground(Color.WHITE);
		m_MASbutton.setBorder(border);
		m_IPSbutton.setBackground(Color.WHITE);
		m_IPSbutton.setBorder(border);
		
		m_selectTypeOfSystem.setBorder(emptyBorder);
		
	}

	//OC
	class SelectSystemButton extends FLActionButton {
		
		private FLPanel m_associatedSystemPanel;

		public SelectSystemButton(String name, FLPanel panel){
			super(name);
			FLPanel content = FLPanel.NewPanelVertical();
			content.setAlignmentX(CENTER_ALIGNMENT);
			content.setAlignmentY(CENTER_ALIGNMENT);
			setCursor(new Cursor(Cursor.HAND_CURSOR));
			add(content);
			setBackground(Color.WHITE);
			m_associatedSystemPanel = panel;
		}

		// update selection of system
		@Override
		public void DoAction() {
			selectSystemType(m_associatedSystemPanel);
			setBackground(colorSelectedButton);
		}
		
		public void UpdateSize(float height){
			//setSize(new Dimension(170,(int)(height/4)));
			setPreferredSize(new Dimension(buttonWidth,(int)(height/4)));
			setMinimumSize(new Dimension(buttonWidth,100));
			setMaximumSize(new Dimension(500,500));
		}
		public void UpdateLocation(float height, int nb){
			setLocation(0,(int)(height*nb/4));
		}
	}

	public void UpdateButtonSize(){
		float height = this.getHeight();
		m_CAbutton.UpdateSize(height);
		m_CAbutton.UpdateLocation(height,0);
		m_LGbutton.UpdateSize(height);
		m_LGbutton.UpdateLocation(height,1);
		m_MASbutton.UpdateSize(height);
		m_MASbutton.UpdateLocation(height,2);
		m_IPSbutton.UpdateSize(height);
		m_IPSbutton.UpdateLocation(height,3);
	}


	//OC
	/*-----------------------------------------------------------------------
	 * LISTENER
	 *---------------------------------------------------------------------*/

	private class WindowListener implements ComponentListener {
		private SimulationWindowCreator m_window;

		public void setWindow(SimulationWindowCreator window) {
			m_window = window;
		}

		@Override
		public void componentResized(ComponentEvent e) {
			UpdateButtonSize();
		}

		@Override
		public void componentMoved(ComponentEvent e) {
		}

		@Override
		public void componentShown(ComponentEvent e) {
		}

		@Override
		public void componentHidden(ComponentEvent e) {
		}
	}
		//\OC
}
