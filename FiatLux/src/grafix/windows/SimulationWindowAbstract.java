package grafix.windows;

import static java.awt.GridBagConstraints.NORTH;

import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.elements.FLBlockContainerPanel;
import grafix.gfxTypes.elements.FLBlockPanel;
import grafix.gfxTypes.elements.FLLabel;
import grafix.gfxTypes.elements.FLPanel;
import grafix.gfxTypes.elements.FLTextArea;


/************************************
 * for viewing one OR several experiments 
 * @author Nazim Fates
 *******************************************/

public abstract class SimulationWindowAbstract extends ChronosWindow {
	/** main abstract method */
	abstract protected FLPanel GetSimulationPanel();

	/** updates the information of the Window **/
//	/abstract protected void sig_UpdateWindowInfo();

	/** /** returns a new dataPlotterSelector to
	 * allow the user to open data monitoring **/
	//abstract void SetPlotterSelector(); 
	//abstract void OpenPlot(); // manage this for multi plot

	/*-**********************************************************
	 * Constants
	--------------------*/

	private static final String TIP_SIMUTIME = "How many generations have elapsed...";
	protected static final String S_SIMULATIONSTATE1 = "Time: ";
	protected static final String S_SIMULATIONSTATE2 = " iteration";
	protected static final int TEXTAREASIZE = 12, N_TEXTAREA1D = 4;

	final static FLColor 
		TEXT_BACKGROUND1 = FLColor.c_lightyellow,
		TEXT_BACKGROUND2 = FLColor.c_lightpurple,
		COL_ACTION= FLColor.c_lightgreen,
		COL_SIMU_AREA= FLColor.c_lightyellow,
		COL_SAMPLER = FLColor.c_grey1,
		COL_INFOAREA = FLColor.c_lightpurple;
	public static final FLColor COL_PLOTAREA = FLColor.c_lightblue,
		COL_INITAREA = FLColor.c_lightgreen;

	/* attributes
	--------------------*/

	protected SaveFeaturesPanel m_savePanel;

	// display info
	FLTextArea m_TopologyInfo, m_SimulationState;
	//JTextArea m_ModelInfo;
	//FLButton modelName = new FLButton("Simu Info");
	FLLabel modelLabel = new FLLabel("Simu Info", FLLabel.typeFont.BIG);
	FLLabel m_TopologyInfoM, m_SimulationStateM;

	protected FLPanel m_plotPanel; // initiated by SimulationWindowSingle (only)


	/* **********************************************************
	 * Constructor
	--------------------*/

	public SimulationWindowAbstract(String in_str) {
		super(in_str);
		m_TopologyInfo = new FLTextArea(TEXTAREASIZE);
		m_TopologyInfoM = new FLLabel(" ");//, FLLabel.typeFont.BIG);
		m_SimulationStateM = new FLLabel(" ");//, FLLabel.typeFont.BIG);
		m_SimulationState = new FLTextArea(TEXTAREASIZE);
		m_SimulationState.SetHelp(TIP_SIMUTIME);
		m_SimulationState.SetFontMid();
		
		// in order not to have a gray background
		m_SimulationState.setOpaque(false);
		m_TopologyInfo.setOpaque(false);
		m_SimulationStateM.setOpaque(false);
		m_TopologyInfoM.setOpaque(false);
	}

	/** shows window with a vertical scrollpane **/
	public void BuildAndDisplay(){
		FLPanel mainpanel = GetSimulationPanel();	
		if (main.FiatLux.withScrollPane){// scroll pane
			this.AddPanelWithScrollPane(mainpanel);
		} else {
			this.add(mainpanel);
		}
		packAndShow();
	}

	/** this is the part that should be common to various SimulationWindow types **/
	public FLBlockContainerPanel GetLeftCommonPanel() {
		return GetLeftCommonPanel(true);
	}

	/**  TODO : TOO COMPLEX **/
	public FLBlockContainerPanel GetLeftCommonPanel(boolean withDynamics) {
		// RIGHT PANEL 2 - SAVE SETTINGS
		FLPanel middlePanel;
		if (m_savePanel != null){
			middlePanel = m_savePanel;
			// m_SavePanel.defineAsWindowBlockPanel(COL_SIMU_AREA, m_SavePanel.getLabelButton(), 100);
		} else {
			middlePanel = new FLPanel();  // empty
		}

		// RIGHT PANEL 3 - SIMU INFO
		FLBlockPanel infopanel = new FLBlockPanel();
		//infopanel.AddGridBagButton(modelName);
		infopanel.AddGridBagY(m_SimulationState, m_TopologyInfo);//, pmodelscroll); // time, etc.
		//infopanel.defineAsWindowBlockPanel(FLColor.c_lightblue, modelName, 80);
		infopanel.GetGridBagC().insets.set(10, 5, 5, 5);
		infopanel.GetGridBagC().anchor = NORTH;

		// RIGHT PANEL 4 - PLOTTING
		// set up m_plotPanel before !
		FLBlockContainerPanel rightPanel = new FLBlockContainerPanel();

		if (withDynamics) {
			// RIGHT PANEL 1 - DYNAMICS
			FLPanel actionButtons = super.GetDynamicsPanel();
			rightPanel.addBlock(actionButtons, 0, 5);
			rightPanel.addBlock(infopanel, 0, 10);
		}

		rightPanel.addBlock(middlePanel, 0, 130); // to the bottom (less important than some other panels)
		if (m_plotPanel != null) {
			rightPanel.addBlock(m_plotPanel, 0, 120); // to the bottom (less important than some other panels)
		}
		return rightPanel;
	}

	@Override
	public void doToggleFullScreen(boolean fullScreenModeEnabled) {
	}
}
