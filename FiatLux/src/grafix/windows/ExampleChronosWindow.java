package grafix.windows;

import grafix.gfxTypes.IntField;
import grafix.gfxTypes.elements.FLPanel;
import main.Macro;


public class ExampleChronosWindow extends ChronosWindow {

	IntField m_countField = new IntField("count", 2, 1000);

	public ExampleChronosWindow(){
		super("example for ChronosWindow");
		FLPanel houhou= new FLPanel();	
		houhou.SetBoxLayoutY();
		houhou.add( super.GetDynamicsPanel() );
		houhou.add( super.GetControlPanel() );
		houhou.add( m_countField );
		addPanel(houhou);
		pack();
	 }	

	public void sig_Init() {
		m_countField.SetValue(0);
	}

	public void sig_NextStep() {
		m_countField.SetValue(m_countField.GetValue() + 1);
	}
	
	public void sig_UpdateView() {
		Macro.print("coucou");
	}

	/* saving  */
	 public void sig_Save(){
			Macro.print("saving not implemented");
	}

	protected void sig_Close() {
		CloseWindow();
		Macro.print("bye");
		Macro.systemExit();
	}
	
	public final static void DoTest(){
		ExampleChronosWindow w = new ExampleChronosWindow();
		w.setVisible(true);
		for (int i=0; i<1000;){}
    }

	public void doToggleFullScreen(boolean fullScreenModeEnabled) {
	}
}
