package grafix.windows;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import grafix.gfxTypes.elements.FLButton;
import grafix.gfxTypes.elements.FLPanel;

/************************************
 * for experiments that use n buttons 
 * @author : Nazim Fates
 * Date of creation: Nov. 2002
 *******************************************/ 


public abstract class MultiButtonView extends FLPanel implements ActionListener {
	abstract protected void DoAction(int index);

	/*--------------------
	 * Attributes 
	--------------------*/

	// buttons
	int m_NBUTTONS;
	FLButton [] m_Button = new FLButton[m_NBUTTONS];

	/*--------------------
	 * Constructor
	--------------------*/

	// the automaton will be used for updating the next gen
	public MultiButtonView(String [] labels){
		//super(in_title);
		m_NBUTTONS = labels.length;
		// memory allocation for graphics 
		m_Button = new FLButton[m_NBUTTONS];
		for (int i=0; i < m_NBUTTONS; i++ ){
			m_Button[i]= new FLButton( labels[i] );
		}

		for (int i=0; i< m_NBUTTONS; i++ ){
			m_Button[i].addActionListener(this);
		}

	}

	public void SetLabels(String [] in_Label){
		for (int i=0; i< m_NBUTTONS; i++ ){
			m_Button[i].setText( in_Label[i] );
		}
	}

	/* access to the buttons */
	public FLPanel GetButtonPanel() {
		FLPanel p1= new FLPanel();	
		for (int i=0; i < m_NBUTTONS; i++ ){
			p1.add( m_Button[i]);
		}	
		return p1;	
	}

	/*--------------------
	 * Other methods
	--------------------*/

	public void actionPerformed(ActionEvent e){
		for (int i=0; i< m_NBUTTONS; i++){
			if (e.getSource() == m_Button[i]){
				DoAction(i);
				return;
			}
		}
	}
}
