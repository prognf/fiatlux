package grafix.windows;

import javax.swing.JLabel;

import components.types.IntPar;
import grafix.gfxTypes.elements.FLList;
import grafix.gfxTypes.elements.FLPanel;

/*--------------------
 * Control 1D CA settings window
 * @author Oceane Chaze
 *--------------------*/

public class OneDimensionalCAcontrolPanel extends FLPanel {

    public OneDimensionalCAcontrolPanel(FLPanel stability, FLList mouseMode, IntPar setCellValOnClick, FLPanel colorPanel, FLPanel shift, FLPanel shift_zqsd){
        super();

        SetBoxLayoutY();
        setAlignmentY(0);
        Add(stability);
        AddSeparator();

        FLPanel setClick = new FLPanel();
        setClick.SetBoxLayoutX();

        JLabel mouse = new JLabel("Change mouse mode : ");
        mouse.setAlignmentX(CENTER_ALIGNMENT);
        Add(mouse);
        setClick.Add(mouseMode);
        setClick.Add(setCellValOnClick);

        Add(setClick);
        JLabel color = new JLabel("Change colors : ");
        color.setAlignmentX(CENTER_ALIGNMENT);
        Add(color);
        Add(colorPanel);

        AddSeparator();
        JLabel zqsd = new JLabel("Shift : ");
        zqsd.setAlignmentX(CENTER_ALIGNMENT);
        Add(zqsd);
        Add(shift_zqsd);
        AddSeparator();
        JLabel spts = new JLabel("Shift per time step : ");
        spts.setAlignmentX(CENTER_ALIGNMENT);
        Add(spts);

        Add(shift);

        AddSeparator();
    }
}
