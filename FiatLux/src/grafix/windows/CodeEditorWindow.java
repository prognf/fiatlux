package grafix.windows;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFileChooser;

import grafix.gfxTypes.IconManager;
import grafix.gfxTypes.elements.FLButton;
import grafix.gfxTypes.elements.FLCodePanel;
import grafix.gfxTypes.elements.FLFrame;
import grafix.gfxTypes.elements.FLPanel;
import models.CAmodels.simplified.FLJavaCompiler;
import models.CAmodels.simplified.FLjavaSource;

public class CodeEditorWindow extends FLFrame implements ActionListener {
	
	
	private static final int SIZEX_COREEDITOR = 700, SIZEY_COREEDITOR=500;

	/** decides if the model can be edited **/
	public static boolean isModelEditable(String modelName) {
		URL url = CodeEditorWindow.class.getClass().getResource("/components/codepresets/models/" + modelName + ".txt");
		return url != null;
	}

	private FLCodePanel m_code;
	private String m_type;
	private FLButton m_run, m_exit, m_save, m_new, m_preset, m_open;

	public CodeEditorWindow(String code) {
		super("Fiatlux Code Editor");

		m_type = code;
		m_code = new FLCodePanel();
		m_code.setPreferredSize(new Dimension(SIZEX_COREEDITOR, SIZEY_COREEDITOR));

		FLPanel container = FLPanel.NewPanel();
		container.SetGridBagLayout();
		container.SetAnchor(GridBagConstraints.PAGE_START);
		container.AddGridBag(m_code.getLines(), 0, 0);
		container.AddGridBag(m_code, 1, 0);
		container.getInsets().set(0, 0, 0, 0);

		//Insets margin = new Insets(10, 10, 5, 10); // for margins
		m_run = getButton("Run", "single");
		m_save = getButton("Save", "save");
		m_new = getButton("New", "new");
		m_open = getButton("Open", "open");
		m_preset = getButton("Presets", "preset");
		m_exit = getButton("Close", "close");
		FLButton sep = getButton(" ", "separator");

		FLPanel bar = FLPanel.NewPanel(m_run, m_preset, sep, m_new, m_open, m_save, m_exit);
		bar.setBackground(Color.DARK_GRAY);

		FLPanel barContainer = FLPanel.NewPanel(bar);
		barContainer.setBackground(Color.white);

		add(FLPanel.NewPanelVertical(m_code, barContainer));

		StringBuilder sb = new StringBuilder();
		URL url = findFileUrl(code);
		String path;
		if (url != null) { // we look in the presets 
			path="/components/codepresets/" + code + ".txt";
			//EditPresetTextModels(code, sb);
		} else {  // NULL URL // we look in the models directory
			path="/components/codepresets/models/" + code + ".txt";
		}
		//Macro.Debug(" PATH URL:" + path);
		InputStream instream = getClass().getResourceAsStream(path);
		//Macro.Debug("instream: " + instream);
		m_type = "Advanced";
		try (Scanner scanner =  new Scanner(instream)) {
			while (scanner.hasNextLine()){
				String nextLine = scanner.nextLine();
				String nextLineTrimmed = nextLine.trim();

				sb.append(nextLine).append(System.getProperty("line.separator"));

				switch (nextLineTrimmed) { // to choose which model to inherit from
				case "/*TYPE=BinarySimplifiedModel*/":
					m_type = "GeneralBinary";
					break;
				case "/*TYPE=BinaryTotalisticSimplifiedModel*/":
					m_type = "BinaryTotalistic";
					break;
				case "/*TYPE=AdvancedModel*/":
					m_type = "Advanced";
					break;
				}
			}
			m_code.setText(sb.toString());
		}
	}

	/*zzzprivate void EditPresetTextModels(String code, StringBuilder sb) {
		String path="/codepresets/" + code + ".txt";
		Macro.Debug(" PATH NON EMPTY URL:" + path);
		InputStream instream = getClass().getResourceAsStream(path);
		Macro.Debug("instream: " + instream);
		try (Scanner scanner =  new Scanner(instream)) {
			while (scanner.hasNextLine()){
				sb.append(scanner.nextLine()).append(System.getProperty("line.separator"));
			}
			m_code.setText(sb.toString());
			m_code.updateLines();
		}
	}**/

	private URL findFileUrl(String code) {
		return getClass().getResource("/components/codepresets/" + code + ".txt");
	}

	private String getEntireCode() {
		StringBuilder entireClassCode = new StringBuilder();
		String modelName = "UserModelTest";
		String linebreak = System.getProperty("line.separator");
		String superClass = "AbstractSimplifiedModel";

		if (m_type.equals("GeneralBinary")) {
			superClass = "BinarySimplifiedModel";
		} else if (m_type.equals("BinaryTotalistic")) {
			superClass = "BinaryTotalisticSimplifiedModel";
		} else if (m_type.equals("Advanced")) {
			superClass = "AdvancedModel";
		}

		entireClassCode.append("package models.CAmodels.simplified;").append(linebreak)
		.append("import components.types.*;").append(linebreak)
		.append("import grafix.gfxTypes.elements.*;").append(linebreak)
		.append("import initializers.CAinit.*;").append(linebreak)
		.append("import main.tables.*;").append(linebreak)
		.append("public class ").append(modelName).append(" extends ").append(superClass).append(" {").append(linebreak)
		.append("public static final String NAME = \"").append(modelName).append("\";").append(linebreak)
		.append(m_code.getText()).append(linebreak).append("}");

		return entireClassCode.toString();
	}

	public void addCodePreset(String id) {
		String linebreak = System.getProperty("line.separator");
		StringBuilder sb = new StringBuilder(m_code.getText());
		sb.append(linebreak).append(linebreak);

		if (id.equals("Grid size")) {
			sb.append("/*Set grid size*/").append(linebreak)
			.append("public IntC getDefaultGridSize() {").append(linebreak)
			.append("    return new IntC(100, 100);").append(linebreak)
			.append("}");
		} else if (id.equals("Palette")) {
			sb.append("/*Set palette*/").append(linebreak)
			.append("public void setStates() {").append(linebreak)
			.append("    addState(Colors.WHITE);").append(linebreak)
			.append("    addState(Colors.BLUE);").append(linebreak)
			.append("}");
		} else if (id.equals("Screen size")) {
			sb.append("/*Set screen size*/").append(linebreak)
			.append("public double getScreenSize() {").append(linebreak)
			.append("    return 550.0;").append(linebreak)
			.append("}").append(linebreak).append(linebreak)
			.append("/*Set borders*/").append(linebreak)
			.append("public int getBorder() {").append(linebreak)
			.append("    return 0;").append(linebreak)
			.append("}");
		} else if (id.equals("Initializer")) {
			sb.append("/*Set initialiser*/").append(linebreak)
			.append("public SuperInitializer GetDefaultInitializer() {").append(linebreak)
			.append("    return new BinaryInitializer();").append(linebreak)
			.append("}");
		} else if (id.equals("Measuring devices")) {
			sb.append("/*Mesuring devices*/").append(linebreak)
			.append("@Override").append(linebreak)
			.append("public String[] GetPlotterSelection2D() {").append(linebreak)
			.append("    return PlotterSelectControl.defaultSelect;").append(linebreak)
			.append("}");
		} else if (id.equals("Random test")) {
			sb.append("/*Random test example*/").append(linebreak)
			.append("public void doRandomTestExemple() {").append(linebreak)
			.append("    if (RandomEventDouble(0.5)) {").append(linebreak)
			.append("        /* This line can be executed with a probability of 0.5 */").append(linebreak)
			.append("    }").append(linebreak)
			.append("}");
		}

		m_code.setText(sb.toString());
	}

	/* MAIN : COMPILES and EXECUTES **/
	@Override
	public void actionPerformed(ActionEvent e) {
		String modelName = "UserModelTest";

		if (e.getSource() == m_exit) {
			CloseWindow();
		} else if (e.getSource() == m_run) { /////// RUN COMMAND
			String code = getEntireCode();
			String longName = "models.CAmodels.simplified." + modelName;
			FLjavaSource source = new FLjavaSource(modelName, code, longName);
			FLJavaCompiler compiler = new FLJavaCompiler(source);
			compiler.compile();
			compiler.execute();
		} else if (e.getSource() == m_preset) {
			(new NGCodePresetsWindow(this)).packAndShow();
		} else if (e.getSource() == m_new) {
			(new NewCAChoicesWindow()).packAndShow();
		} else if (e.getSource() == m_open) {
			JFileChooser fileChooser = new JFileChooser();
			String linebreak = System.getProperty("line.separator");
			StringBuilder sb = new StringBuilder();
			int r = fileChooser.showOpenDialog(this);

			if (r == JFileChooser.APPROVE_OPTION) {
				File file = fileChooser.getSelectedFile();

				try (Scanner scanner =  new Scanner(file)) {
					while (scanner.hasNextLine()){
						sb.append(scanner.nextLine()).append(linebreak);
					}
					m_code.setText(sb.toString());
				} catch (IOException error) {
					error.printStackTrace();
				}
			}
		} else if (e.getSource() == m_save) {
			JFileChooser fileChooser = new JFileChooser();
			int r = fileChooser.showSaveDialog(this);
			if (r == JFileChooser.APPROVE_OPTION) {
				File f = fileChooser.getSelectedFile();
				try {
					FileOutputStream fo = new FileOutputStream(f);
					BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fo , StandardCharsets.UTF_8));
					bw.write(m_code.getText());
					bw.newLine();
					bw.flush();
					fo.close();
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
	}

	/** returns a button with a given legend & icon **/
	private FLButton getButton(String name, String icon) {
		FLButton button = new FLButton(name);
		button.setIcon(IconManager.getUIIcon(icon));
		button.setBackground(Color.DARK_GRAY);
		button.setForeground(Color.WHITE);
		button.setBorderPainted(false);
		button.setFocusPainted(false);
		button.setMargin(new Insets(10, 10, 5, 10));
		button.setVerticalTextPosition(JButton.BOTTOM);
		button.setHorizontalTextPosition(JButton.CENTER);
		button.addActionListener(this);

		return button;
	}
}
