package grafix.windows;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextPane;

import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.IconManager;
import grafix.gfxTypes.elements.FLBlockPanel;
import grafix.gfxTypes.elements.FLButton;
import grafix.gfxTypes.elements.FLFrame;
import grafix.gfxTypes.elements.FLPanel;
import main.FiatLuxProperties;

public class ConfigurationWindow extends FLFrame implements ActionListener {
    final static String SAVE_TITLE = "Save preferences";
    final static String SAVE_BUTTON = "Save";
    final static String REMOVE_BUTTON = "Remove";
    final static String SAVE_CONTENT_NO = "Fiatlux preferences are not stored now." +
    		" If you want your preferences to be saved, click the Save button below." +
    		" A file named 'user.properties' will be created in the current directory.";
    final static String SAVE_CONTENT_YES = "Fiatlux preferences are stored in 'user.properties'." +
    		" If you want to remove your preferences, click the Remove button below.";

    private FLButton m_saveButton, m_removeButton;
    private JTextPane m_saveTextPane;

    public ConfigurationWindow() {
        super("Fiatlux Configuration");

        FLBlockPanel saveContainer = new FLBlockPanel();
        FLButton saveTitle = new FLButton(SAVE_TITLE);
        m_saveButton = new FLButton(SAVE_BUTTON);
        m_removeButton = new FLButton(REMOVE_BUTTON);
        saveTitle.setIcon(IconManager.getUIIcon("saveMini"));
        m_saveButton.setIcon(IconManager.getUIIcon("saveMini"));
        m_removeButton.setIcon(IconManager.getUIIcon("removeMini"));
        saveContainer.defineAsWindowBlockPanel(FLColor.c_lightgrey, saveTitle, 220, false);
        saveContainer.setDefaultWidth(600);
        m_saveTextPane = new JTextPane();
        m_saveTextPane.setEditable(false);
        updateSaveStatus();
        m_saveTextPane.setSize(550, 40);
        m_saveTextPane.setPreferredSize(new Dimension(550, 90));
        saveContainer.AddGridBagButton(saveTitle);
        FLPanel buttons = FLPanel.NewPanel(m_saveButton, m_removeButton);
        m_saveButton.defineAsPanelButtonStylized(FLColor.c_lightgreen);
        m_removeButton.defineAsPanelButtonStylized(FLColor.c_lightred);
        FLPanel lines = FLPanel.NewPanelVertical(m_saveTextPane, buttons);
        lines.setOpaque(false);
        buttons.setOpaque(false);
        saveContainer.AddGridBagY(lines);

        add(FLPanel.NewPanel(saveContainer));

        m_saveButton.addActionListener(this);
        m_removeButton.addActionListener(this);
    }

    private void updateSaveStatus() {
        m_saveButton.setVisible(!FiatLuxProperties.SAVE_PREFERENCES);
        m_removeButton.setVisible(FiatLuxProperties.SAVE_PREFERENCES);
        m_saveTextPane.setText(FiatLuxProperties.SAVE_PREFERENCES ? SAVE_CONTENT_YES : SAVE_CONTENT_NO);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == m_saveButton) {
            FiatLuxProperties.enableSave();
            updateSaveStatus();
        } else if (e.getSource() == m_removeButton) {
            FiatLuxProperties.disableSave();
            updateSaveStatus();
        }
    }
}
