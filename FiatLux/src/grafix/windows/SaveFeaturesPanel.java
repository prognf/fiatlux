package grafix.windows;

import static grafix.windows.SimulationWindowAbstract.COL_SIMU_AREA;

import javax.swing.JCheckBox;

import components.types.FLString;
import components.types.FLsignal;
import components.types.Monitor;
import experiment.toolbox.ConfigRecorder;
import grafix.gfxTypes.IconManager;
import grafix.gfxTypes.controllers.PolyChoicePanel;
import grafix.gfxTypes.elements.FLActionButton;
import grafix.gfxTypes.elements.FLBlockPanel;
import grafix.gfxTypes.elements.FLButton;
import grafix.gfxTypes.elements.FLLabel;
import grafix.gfxTypes.elements.FLPanel;
import grafix.gfxTypes.elements.FLTextField;
import grafix.gfxTypes.imgFormat.ImgFormat;
import main.FiatLux;
import main.FiatLuxProperties;

/*-------------------------------------------------------------------------------
- "Save Panel"
- @author Nazim Fates
------------------------------------------------------------------------------*/
public class SaveFeaturesPanel extends FLBlockPanel
	implements Monitor {

	final protected static String
	SAVE = "Save",
	REC = "Rec",
	SRCPRINT = "Print Source",
	SETUP = "Setup",
	FORMAT = "Change format",
	SUFFIX = "suffix:";


	private static final boolean DEF_SELECT = false;
	private static final String TXT_ENTERNEWSUFFIX = "enter new suffix";
	private static final String SETTINGS = "Record";

	/*----------------------------------------------------------------------------
	  - attributes
	  --------------------------------------------------------------------------*/
	ConfigRecorder m_recorder; // main linked object

	JCheckBox m_CheckRecord= new JCheckBox(REC);
	JCheckBox m_CheckSrcPrint= new JCheckBox(SRCPRINT);
	FLActionButton m_SaveButton = new SaveConfigButton();
	FLButton label = new FLButton(SETTINGS);

	//TODO
	//SaveSetUpButton saveSetUpButton = new SaveSetUpButton();

	private FLPanel m_triplet = new FLPanel();
	/*---------------------------------------------------------------------------
	  - construction
	  --------------------------------------------------------------------------*/

	public SaveFeaturesPanel(ConfigRecorder sampler) {
		m_recorder= sampler;		
		// set up
		label.setIcon(IconManager.getUIIcon("record"));
		ChangeFormatButton changeButton = new ChangeFormatButton();
		SaveSetUpButton saveSetUpButton = new SaveSetUpButton();
		m_triplet.Add(m_SaveButton, m_CheckRecord, m_CheckSrcPrint);
		FLPanel suffixPanel = new FLPanel();
		suffixPanel.Add(new FLLabel(SUFFIX), saveSetUpButton);
		AddGridBagButton(label);
		AddGridBagY(m_triplet, suffixPanel, changeButton);
		m_CheckRecord.setSelected(DEF_SELECT);

		// SetBackgroundColor(COL_SIMU_AREA);
		m_triplet.setOpaque(false);
		m_CheckRecord.setOpaque(false);
		m_CheckSrcPrint.setOpaque(false);

		suffixPanel.setOpaque(false);
		defineAsWindowBlockPanel(COL_SIMU_AREA, label, 140);
		installBlockPanelStyle(true, m_SaveButton, changeButton.getButton(), saveSetUpButton);

		setTabId(FiatLuxProperties.SIDEBAR_TABS.RECORD);
	}

	public FLButton getLabelButton() {
		return label;
	}

	static ImgFormat[] TAB_FORMAT = ImgFormat.values();

	class ChangeFormatButton extends PolyChoicePanel {
		public ChangeFormatButton() {
			super(CHANGE, 
					FLString.toStringArray(TAB_FORMAT),
					FiatLux.DEFAULT_FORMAT_SELECT_INDEX );

			setOpaque(false);
		}

		protected void Update() {
			ImgFormat currentFormat = TAB_FORMAT[super.m_indexChoice]; 
			m_recorder.io_SetRecordFormat(currentFormat);
		}

	}

	/*---------------------------------------------------------------------------
	  - signals
	  --------------------------------------------------------------------------*/
	@Override
	public void ReceiveSignal(FLsignal sig){
		switch(sig){
		case init:
		case nextStep:
			sig_RecordFilm();		
			break;
		case update:
			break;
		}
	}
	
	/** for one button action of recording only **/
	public void sig_Save() {
		if (IsSrcPrintOn()) {
			m_recorder.io_SaveState();
		}
		m_recorder.io_SaveImage();
	}

	/** for recording films **/
	void sig_RecordFilm() {		
		if ( IsRecordOn() ) {
			m_recorder.io_SaveImage();	
			int delta = 1;
		}
	} 

	/*---------------------------------------------------------------------------
	  - get & set
	  --------------------------------------------------------------------------*/

	public boolean IsRecordOn(){
		return m_CheckRecord.isSelected();
	}

	public boolean IsSrcPrintOn(){
		return m_CheckSrcPrint.isSelected();
	}

	/** save config button **/
	class SaveConfigButton extends FLActionButton{
		public SaveConfigButton() {
			super(SAVE);
			setIcon(IconManager.getUIIcon("saveMini"));
		}

		@Override
		public void DoAction() { sig_Save(); }
	}

	class SaveSetUpButton extends FLTextField {
		protected SaveSetUpButton() {
			super(6);
		}

		@Override
		public void parseInput(String input) {
			m_recorder.io_SetRecordSuffix(input);
			this.setText(input);
		}
	}
}


