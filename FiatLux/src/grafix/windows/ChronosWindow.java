package grafix.windows;
import static grafix.gfxTypes.elements.FLPanel.PANEL_SIZE;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.KeyStroke;

import grafix.gfxTypes.IconManager;
import grafix.gfxTypes.IntField;
import grafix.gfxTypes.controllers.IntController;
import grafix.gfxTypes.elements.FLButton;
import grafix.gfxTypes.elements.FLFrame;
import grafix.gfxTypes.elements.FLLabel;
import grafix.gfxTypes.elements.FLPanel;
import main.FiatLuxProperties;
import main.Macro;

/************************************
 * This class creates a window for managing Init / Start / Stop
 * it also manages manual / automatic saving of configurations
 * the idea is that this window can send four types of signals
 * Init, NextStep, Display and Save  
 * @author : Nazim Fates
 *******************************************/ 
abstract public class ChronosWindow extends FLFrame 
implements ActionListener, Runnable {
	abstract protected void sig_Init(); // init  
	abstract protected void sig_NextStep(); // calculate 
	abstract protected void sig_UpdateView(); // view update

	abstract protected void sig_Close(); // close current configuration

	/*--------------------
	 * constants
	--------------------*/

	final protected static String m_ClassName= "ChronosWindow";
	final protected static int I_FRAMESPEED = 10;
	final protected static String 
	INIT="Init", START= "Start", STOP="Stop", NEXT="Next", 
	SPEED="steps/sec", STROBOSCOPE="stroboscope";
	final protected static String CLOSE="Close", FULLSCREEN = "Minimize", NORMALSCREEN = "Normal";
	// features
	private final static String 
	S_MULTISTEP="Drop", S_MULTISTEPINCREMENT="steps";

	private final static int SZ_MULTISTEP = 4 ; // size of the field
	private static final int DEF_INCREMENT = 100;	


	/*--------------------
	 * Attributes 
	--------------------*/
	private int m_WindowTime = 0; // counting steps of calculus, this one is used for recording

	// buttons
	FLButton m_InitButton, m_StartStopButton, m_NextButton, m_FullScreenButton, m_CloseButton, m_separator;

	// controls
	private IntField m_SpeedField = new IntField(SPEED, 4, I_FRAMESPEED);
	protected IntField m_StroboField = new IntField(STROBOSCOPE, 3, FiatLuxProperties.DEF_STROBVALUE);
	protected MultiStepControl m_MultiStepControl= null ;

	// start-stop thread
	private boolean m_StartStopFlag;    
	protected boolean m_fullScreen = false;

	private FLButton m_MultiStepButton;
	private int m_MultiStepIncrement = 	DEF_INCREMENT;
	private FLPanel m_dynamicsPanel = new FLPanel();
	private FLPanel m_timePanel = new FLPanel();
	private FLLabel m_iterations = new FLLabel("0 iterations");
	private FLPanel m_controls = new FLPanel();
	private FLPanel m_dynamicsControls = new FLPanel();

	/*--------------------
	 * Constructor
	--------------------*/
	public ChronosWindow(String in_str) {
		super(in_str);

		m_InitButton = new FLButton(INIT);
		m_StartStopButton = new FLButton(START);
		m_NextButton = new FLButton(NEXT);
		m_CloseButton = new FLButton(CLOSE);
		m_MultiStepButton = new FLButton(S_MULTISTEP);
		m_FullScreenButton = new FLButton(FULLSCREEN);
		m_separator = new FLButton(" ");

		m_InitButton.addActionListener(this);
		m_NextButton.addActionListener(this);
		m_StartStopButton.addActionListener(this);
		m_CloseButton.addActionListener(this);
		m_MultiStepButton.addActionListener(this);
		m_FullScreenButton.addActionListener(this);


		// close this window => close signal 
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				sig_Close(); // abstract
			}
		});

		JComponent comp= this.getRootPane(); 
		KeyStroke 
		strokeRight = KeyStroke.getKeyStroke("RIGHT"),
		strokeSpace = KeyStroke.getKeyStroke("SPACE"),
		strokeDown = KeyStroke.getKeyStroke("DOWN");

		Action 
		nextA = new ActionKeyStroke(1), iniA= new ActionKeyStroke(2), zloupA= new ActionKeyStroke(3);

		comp.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(strokeRight,"next");
		comp.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(strokeSpace,"ini");
		comp.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(strokeDown,"zloup"); //jump
		comp.getActionMap().put("next", nextA);
		comp.getActionMap().put("ini", iniA);
		comp.getActionMap().put("zloup", zloupA);

		AddKeyMap(comp);
	}

	class ActionKeyStroke extends AbstractAction {
		final private int m_id;

		public ActionKeyStroke(int id) {
			m_id= id;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			//Macro.Debug("key stroke detected");
			switch(m_id) {
				case 1: WindowNextStepWithDisplayUpdate(); break;
				case 2: WindowInit(); break;
				case 3: MultiStep(); break;
			}

		}
	}

	private void AddKeyMap(JComponent comp) {
		Macro.print(4,"Adding keyboard shortcuts");

		comp.getActionMap().put("ini", new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent e){
				WindowInit();
			}
		});
		comp.getInputMap().put(KeyStroke.getKeyStroke("DOWN"), "jump");
		comp.getActionMap().put("jump", new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent e){
				MultiStep();
			}
		});

		Macro.print(4, this, "finalize : called before destroying object...");
	}

	/* debugging */
	protected void finalize() {
	}

	/*--------------------
	 * Init,Start,Next buttons
	 * TODO : factorize this
	--------------------*/
	
	protected FLPanel GetDynamicsPanel() {
		// general combination
		m_controls.SetGridBagLayout();

		FLPanel firstRow = new FLPanel();
		firstRow.setOpaque(false);
		firstRow.SetGridBagLayout();
        firstRow.GetGridBagC().weightx= 0.5;

		m_controls.GetGridBagC().fill = GridBagConstraints.VERTICAL;
		m_controls.GetGridBagC().insets.set(10, 5, 5, 5);
		m_controls.GetGridBagC().weightx= 0.5;

		firstRow.AddGridBag(m_InitButton, 0, 10);
		firstRow.AddGridBag(m_StartStopButton, 1, 10);
		firstRow.AddGridBag(m_NextButton, 2, 10);
		firstRow.AddGridBag(m_MultiStepButton, 3, 10);
		firstRow.AddGridBag(m_separator, 4, 10);
		firstRow.AddGridBag(m_FullScreenButton, 5, 10);
		firstRow.AddGridBag(m_CloseButton, 6, 10);

		m_controls.AddGridBag(firstRow, 0, 5);

		m_InitButton.setBackground(Color.DARK_GRAY);
		m_NextButton.setBackground(Color.DARK_GRAY);
		m_StartStopButton.setBackground(Color.DARK_GRAY);
		m_CloseButton.setBackground(Color.DARK_GRAY);
		m_CloseButton.setBackground(Color.DARK_GRAY);
		m_MultiStepButton.setBackground(Color.DARK_GRAY);
		m_FullScreenButton.setBackground(Color.DARK_GRAY);
		m_separator.setBackground(Color.DARK_GRAY);

		m_InitButton.setBorderPainted(false);
		m_NextButton.setBorderPainted(false);
		m_StartStopButton.setBorderPainted(false);
		m_CloseButton.setBorderPainted(false);
		m_CloseButton.setBorderPainted(false);
		m_MultiStepButton.setBorderPainted(false);
		m_FullScreenButton.setBorderPainted(false);
		m_separator.setBorderPainted(false);

		m_InitButton.setFocusPainted(false);
		m_NextButton.setFocusPainted(false);
		m_StartStopButton.setFocusPainted(false);
		m_CloseButton.setFocusPainted(false);
		m_CloseButton.setFocusPainted(false);
		m_MultiStepButton.setFocusPainted(false);
		m_FullScreenButton.setFocusPainted(false);
		m_separator.setFocusPainted(false);

		Insets margin = new Insets(10, 10, 5, 10);
		m_InitButton.setMargin(margin);
		m_NextButton.setMargin(margin);
		m_StartStopButton.setMargin(margin);
		m_CloseButton.setMargin(margin);
		m_CloseButton.setMargin(margin);
		m_MultiStepButton.setMargin(margin);
		m_FullScreenButton.setMargin(margin);
		m_separator.setMargin(margin);

		m_InitButton.setIcon(IconManager.getUIIcon("init3"));
		m_NextButton.setIcon(IconManager.getUIIcon("next"));
		m_StartStopButton.setIcon(IconManager.getUIIcon("play"));
		m_MultiStepButton.setIcon(IconManager.getUIIcon("fastForward"));
		m_CloseButton.setIcon(IconManager.getUIIcon("close"));
		m_FullScreenButton.setIcon(IconManager.getUIIcon("zoomMin"));
		m_separator.setIcon(IconManager.getUIIcon("separator"));

		m_InitButton.setForeground(Color.WHITE);
		m_NextButton.setForeground(Color.WHITE);
		m_StartStopButton.setForeground(Color.WHITE);
		m_MultiStepButton.setForeground(Color.WHITE);
		m_CloseButton.setForeground(Color.WHITE);
		m_FullScreenButton.setForeground(Color.WHITE);

		m_InitButton.setVerticalTextPosition(JButton.BOTTOM);
		m_InitButton.setHorizontalTextPosition(JButton.CENTER);
		m_NextButton.setVerticalTextPosition(JButton.BOTTOM);
		m_NextButton.setHorizontalTextPosition(JButton.CENTER);
		m_StartStopButton.setVerticalTextPosition(JButton.BOTTOM);
		m_StartStopButton.setHorizontalTextPosition(JButton.CENTER);
		m_MultiStepButton.setVerticalTextPosition(JButton.BOTTOM);
		m_MultiStepButton.setHorizontalTextPosition(JButton.CENTER);
		m_CloseButton.setVerticalTextPosition(JButton.BOTTOM);
		m_CloseButton.setHorizontalTextPosition(JButton.CENTER);
		m_FullScreenButton.setVerticalTextPosition(JButton.BOTTOM);
		m_FullScreenButton.setHorizontalTextPosition(JButton.CENTER);
		m_separator.setVerticalTextPosition(JButton.BOTTOM);
		m_separator.setHorizontalTextPosition(JButton.CENTER);

		m_controls.setOpaque(false);

		m_timePanel = FLPanel.NewPanel(m_iterations, m_dynamicsControls = GetDynamicsViewPanel());
		m_iterations.setOpaque(false);
		m_timePanel.setOpaque(false);
		m_dynamicsPanel = FLPanel.NewPanelVertical(m_controls, m_timePanel);
		m_dynamicsPanel.setOpaque(false);

		return m_dynamicsPanel;
	}

	public void updateTimeDisplay(int iterations) {
		m_iterations.setText(iterations + " iteration" + (iterations > 1 ? "s" : ""));
	}

	public FLPanel GetDynamicsViewPanel() {
		m_MultiStepControl = new MultiStepControl();
		m_MultiStepControl.setBackground(Color.DARK_GRAY);

		FLPanel secondRow = FLPanel.NewPanel(m_StroboField, m_MultiStepControl, m_SpeedField);
		m_StroboField.minimize();
		m_MultiStepControl.minimize();
		m_SpeedField.minimize();
		secondRow.setOpaque(false);
		// secondRow.SetGridBagLayout();
		// secondRow.GetGridBagC().weighty = 0.5;
		// secondRow.setSize(PANEL_SIZE - 10, 90);
		// secondRow.GetGridBagC().insets.set(5, 5, 5, 5);
		// secondRow.AddGridBag(m_StroboField, 0, 10);
		// secondRow.AddGridBag(m_MultiStepControl, 0, 20);
		// secondRow.AddGridBag(m_SpeedField, 0, 30);
		return secondRow;
	}

	/**  returns panel with Speed and Strobo controls */  
	protected FLPanel GetControlPanel(){
		FLPanel p = new FLPanel();
		p.setOpaque(false);
		p.setPreferredSize(new Dimension(PANEL_SIZE, 120));
		p.add(m_SpeedField);
		p.add(m_StroboField);
		return p;
	}

	/*--------------------
	 * get / set
	--------------------*/

	protected int GetWindowTime(){
		return m_WindowTime;
	}

	public void SetIncrementStep(int increment) {
		m_MultiStepIncrement= increment;
	}

	/*-----------------------------------------------------------------------
	 * THREADS MANAGEMENT
	 * see <a href="http://docs.oracle.com/javase/1.4.2/docs/guide/misc/threadPrimitiveDeprecation.html">link</a>.
	 * --------------------------------------------------------------------*/
	private volatile Thread m_thread;
	private boolean threadSuspended;

	/*	  thread management */
	public void run() {
		Thread thisThread = Thread.currentThread();

		while (m_thread == thisThread) {
			// check that the sampler is not suspend
			while (threadSuspended)
				suspendThread();

			// and here we continue !
			WindowNextStep();

			// sets the stroboscope
			if (m_WindowTime % m_StroboField.GetValue() == 0) {
				sig_UpdateView();

				// sets the update frequency
				try {Thread.sleep(1000 / m_SpeedField.GetValue());
				} catch (InterruptedException e) {}
			}
		}
	}

	/**Resumes the window sampler thread*/ 
	public void resumeThread() {
		synchronized (m_thread) {
			m_thread.notify();
		}
	}

	/**Pauses the window sampler thread*/ 
	public void suspendThread() {
		synchronized (m_thread) {
			try {m_thread.wait();} 
			catch (InterruptedException e) {}
		}
	}

	/**Stops the window sampler thread (for quitting)*/
	public void stopThread() {
		if (m_thread != null) {
			synchronized (m_thread) {
				m_thread = null;
			}
		}
	}

	/*	  thread management */
	private void ChangeStartStop() {
		threadSuspended = false;
		m_StartStopFlag = !m_StartStopFlag;
		if (m_StartStopFlag) {
			m_StartStopButton.setText(m_fullScreen ? "" : STOP);
			m_StartStopButton.setIcon(IconManager.getUIIcon("pause"));

			if (m_thread == null) {
				m_thread = new Thread(this);
				m_thread.start();
			} else if (!threadSuspended) {
				resumeThread();
			}

		} else {
			m_StartStopButton.setText(m_fullScreen ? "" : START);
			m_StartStopButton.setIcon(IconManager.getUIIcon("play"));
			threadSuspended = true;
		}
	}

	public void doToggleFullScreen(boolean fullScreenModeEnabled) {
	}

	protected void toggleFullScreen() {
		m_fullScreen = !m_fullScreen;
		m_FullScreenButton.setIcon(IconManager.getUIIcon(m_fullScreen ? "zoomMax" : "zoomMin"));
		m_FullScreenButton.setText(m_fullScreen ? NORMALSCREEN : FULLSCREEN);
		m_iterations.setForeground(m_fullScreen ? Color.WHITE : Color.BLACK);
		m_dynamicsControls.setVisible(!m_fullScreen);

		if (m_controls != null && m_controls.GetGridBagC() != null) {
			// m_controls can be null in non-CA models
			// prevent crash when lauching a non-CA model with fullscreen enabled by default
			m_controls.GetGridBagC().insets.set(m_fullScreen ? 0 : 10, m_fullScreen ? 0 : 5, m_fullScreen ? 0 : 5, m_fullScreen ? 0 : 5);
		} else {
			// postpone "toggleFullScreen" (in order to enable fs by default in non-CA models)
			// Not a very good practice ... replace with smth else if possible
			(new Timer()).schedule(new TimerTask() {
				@Override
				public void run() {
					toggleFullScreen();
					toggleFullScreen();
				}
			}, 250);
		}

		m_InitButton.setText(m_fullScreen ? "" : INIT);
		m_StartStopButton.setText(m_fullScreen ? "" : (m_StartStopFlag ? STOP : START));
		m_NextButton.setText(m_fullScreen ? "" : NEXT);
		m_CloseButton.setText(m_fullScreen ? "" : CLOSE);
		m_MultiStepButton.setText(m_fullScreen ? "" : S_MULTISTEP);
		m_FullScreenButton.setText(m_fullScreen ? "" : FULLSCREEN);

		FiatLuxProperties.getInstance().setMinimizedMode(m_fullScreen);

		this.doToggleFullScreen(m_fullScreen);
	}

	/*--------------------
	 * ACTIONS HANDLING	
	 --------------------*/

	/** creates the interface to configure the MultiStep */
	class MultiStepControl extends IntController {
		protected MultiStepControl() {
			super(S_MULTISTEPINCREMENT, SZ_MULTISTEP);
		}

		@Override
		protected int ReadInt() {
			return m_MultiStepIncrement;
		}

		@Override
		protected void SetInt(int val) {
			m_MultiStepIncrement= val;			
		}		
	}

	/**	"MultiStep" function  */
	public void MultiStep() {
		boolean change = false;

		if (m_StartStopFlag) {
			/**
			 * If the simulation is playing, stop it in ordrer
			 * to prevent bugs (two updates at the same time, wich can
			 * break the player
 			 */
			ChangeStartStop();
			change = true;
		}

		for (int i = 0; i < m_MultiStepControl.ReadInt(); i++) {
			sig_NextStep();
		}
		sig_UpdateView();

		if (change) {
			ChangeStartStop();
		}
	}

	/** one step beyond but not necessary with display */
	private void WindowNextStep() {
		m_WindowTime++;
		sig_NextStep();
	}

	/** one step beyond with display */
	private void WindowNextStepWithDisplayUpdate() {
		WindowNextStep();
		sig_UpdateView();
	}

	private void WindowInit() {
		m_WindowTime = 0;
		sig_Init();
		sig_UpdateView();
	}

	public void actionPerformed(ActionEvent evt) {
		try {
			if (evt.getSource() == m_NextButton) {
				WindowNextStepWithDisplayUpdate();
			} else if (evt.getSource() == m_InitButton) {
				WindowInit();	
			} else if (evt.getSource() == m_StartStopButton) {
				ChangeStartStop();
			} else if (evt.getSource() == m_CloseButton) {
				sig_Close();
			} else if (evt.getSource() == m_MultiStepButton) {
				MultiStep();
			} else if (evt.getSource() == m_FullScreenButton) {
				toggleFullScreen();
			}
		} catch (NullPointerException exc) {
			Macro.FatalError(exc, "Null pointer detected : check init procedures ");
		}
	}
}
