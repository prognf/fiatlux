package grafix.windows;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.IconManager;
import grafix.gfxTypes.elements.FLBlockPanel;
import grafix.gfxTypes.elements.FLButton;
import grafix.gfxTypes.elements.FLFrame;
import grafix.gfxTypes.elements.FLPanel;

public class NGCodePresetsWindow extends FLFrame implements ActionListener {
    final static String[] LABELS = {"Grid size", "Palette", "Screen size", "Initializer", "Measuring devices", "Random test"};
    final static String[] ICONS = {"presetGridSize", "presetColors", "presetSize", "presetInitializer", "presetMeasures", "presetRandom"};
    static final String TITLE = "Add code preset";

    private FLBlockPanel m_container;
    private CodeEditorWindow m_window;

    public NGCodePresetsWindow(CodeEditorWindow window) {
        super(TITLE);

        m_window = window;
        m_container = new FLBlockPanel();

        FLButton title = new FLButton(TITLE);
        FLPanel container = new FLPanel();
        container.SetGridBagLayout();
        container.setOpaque(false);
        m_container.defineAsWindowBlockPanel(FLColor.c_lightblue, title, 300, false);
        m_container.setDefaultWidth(650);
        m_container.AddGridBagButton(title);
        m_container.AddGridBagY(container);

        for (int i = 0; i < LABELS.length; i++) {
            FLButton b = new FLButton(LABELS[i]);
            b.setBackground(Color.WHITE);
            b.setBorderPainted(false);
            b.setFocusPainted(false);
            b.setIcon(IconManager.getUIIcon(ICONS[i]));
            b.setAlignmentX(CENTER_ALIGNMENT);
            b.setAlignmentX(CENTER_ALIGNMENT);
            b.setVerticalTextPosition(JButton.BOTTOM);
            b.setHorizontalTextPosition(JButton.CENTER);
            container.AddGridBag(b, i % 4, (int) ((double) i / 4.0));
            b.AddActionListener(this);
        }

        FLPanel mainContainer = FLPanel.NewPanel(m_container);
        mainContainer.setOpaque(false);
        mainContainer.getInsets().set(15, 5, 15, 10);
        add(mainContainer);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() instanceof FLButton) {
            m_window.addCodePreset(((FLButton) e.getSource()).getText());
            CloseWindow();
        }
    }
}
