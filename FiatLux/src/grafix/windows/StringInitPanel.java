package grafix.windows;

import java.awt.Font;
import java.io.InputStream;
import java.util.Scanner;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

import experiment.samplers.CAsampler;
import grafix.gfxTypes.elements.FLPanel;


/************************************
 * Corresponding to the window using to initialise from a string
 * @author Océane Chazé
 *******************************************/

public class StringInitPanel extends FLPanel {

    public StringInitPanel(JTextField cmd, CAsampler.GoInitButton go, JTextPane errorMsg ){

        JLabel title = new JLabel("Create your initial configuration : ");
        Font font = new Font("SansSerif", Font.BOLD, 20);
        title.setFont(font);

        //Text area, to enter the initialisation string
        FLPanel textBar = new FLPanel();
        textBar.Add(cmd);
        textBar.Add(go);
        Border borderSearch = new EmptyBorder(0,0,0,25);
        textBar.setBorder(borderSearch);

        //Explanations on functions : in a html file
        JTextPane m_help = new JTextPane();
        m_help.setContentType("text/html");

        String str = "";

        String path = "/initializers/CAinit/InitLanguageTools/helpInit.html";
        InputStream default_is = getClass().getResourceAsStream(path);
        String line = "";
        try (Scanner scan =  new Scanner(default_is)) {
            while (scan.hasNextLine()) {
                str +=  scan.nextLine();
            }
        }

        m_help.setText(str);


        SetBoxLayoutY();
        Add(title);
        Add(textBar);
        Add(errorMsg);
        Add(m_help);
        Border border = new EmptyBorder(10, 10, 10,10);
        setBorder(border);


    }
}
