package grafix.windows;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import grafix.gfxTypes.IconManager;
import grafix.gfxTypes.elements.FLBlockPanel;
import grafix.gfxTypes.elements.FLButton;
import grafix.gfxTypes.elements.FLFrame;
import grafix.gfxTypes.elements.FLPanel;

public class NewCAChoicesWindow extends FLFrame implements ActionListener {
    final static String[] LABELS = {"Binary Totalistic", "General Binary", "Advanced"};
    final static String[] ICONS = {"newTotalisticModel", "newBinaryModel", "newAdvancedModel"};

    private FLBlockPanel m_container;

    public NewCAChoicesWindow() {
        super("Fiatlux - New cellular automata");

        m_container = new FLBlockPanel();

        FLButton title = new FLButton("Code presets");
        FLPanel container = new FLPanel();
        container.setLayout(new FlowLayout(FlowLayout.LEADING));
        container.setOpaque(false);
        m_container.defineAsWindowBlockPanel(Color.gray, title, 200, false);
        m_container.setDefaultWidth(500);
        m_container.AddGridBagButton(title);
        m_container.AddGridBagY(container);

        for (int i = 0; i < LABELS.length; i++) {
            FLButton b = new FLButton(LABELS[i]);
            b.setBackground(Color.WHITE);
            b.setBorderPainted(false);
            b.setFocusPainted(false);
            b.setIcon(IconManager.getUIIcon(ICONS[i]));
            b.setAlignmentX(CENTER_ALIGNMENT);
            b.setAlignmentX(CENTER_ALIGNMENT);
            b.setVerticalTextPosition(JButton.BOTTOM);
            b.setHorizontalTextPosition(JButton.CENTER);
            container.add(b);
            b.AddActionListener(this);
        }

        FLPanel mainContainer = FLPanel.NewPanel(m_container);
        mainContainer.setOpaque(false);
        mainContainer.getInsets().set(10, 5, 5, 10);
        add(mainContainer);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        CodeEditorWindow cew = new CodeEditorWindow(((FLButton) e.getSource()).getText().replace(" ", ""));
        cew.packAndShow();
        CloseWindow();
    }
}
