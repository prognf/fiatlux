package grafix.windows;

import static java.awt.GridBagConstraints.PAGE_START;
import static java.lang.Float.min;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.InputEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import components.types.IntC;
import experiment.samplers.CAsampler;
import experiment.samplers.CAsimulationSampler;
import experiment.samplers.SuperSampler;
import experiment.toolbox.ConfigRecorder;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.IconManager;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.elements.FLActionButton;
import grafix.gfxTypes.elements.FLBlockContainerPanel;
import grafix.gfxTypes.elements.FLBlockPanel;
import grafix.gfxTypes.elements.FLButton;
import grafix.gfxTypes.elements.FLLabel;
import grafix.gfxTypes.elements.FLPanel;
import grafix.plotters.MeasuringAndPlotterArea;
import grafix.viewers.GridViewer;
import grafix.viewers.LineAutomatonViewer;
import grafix.viewers.OneRegisterAutomatonViewer;
import initializers.CAinit.InitPanel;
import main.FiatLuxProperties;
import main.Macro;

/******************
 * 
 * class constructs a simulation window 
 * Left: Divided into regions Actions, Simulations, Info, Plots -> true ???
 * Right: Viewer for current configuration -> true ???
 */

public class SimulationWindowSingle extends SimulationWindowAbstract {

	// main linked object
	protected SuperSampler m_sampler;
	private FLBlockContainerPanel m_sidebar;
	private FLPanel m_topbar;
	private FLPanel m_colorDescriptor;

	/**
	 * FIXME : problem with keys when eg some filename is typed
	 * @param windowLegend
	 * @param sampler
	 */
	public SimulationWindowSingle(String windowLegend, SuperSampler sampler) {
		super(windowLegend);
		m_sampler = sampler;

		// RECORD part
		if (m_sampler instanceof ConfigRecorder){ 
			ConfigRecorder recorder= (ConfigRecorder) m_sampler;
			m_savePanel= new SaveFeaturesPanel(recorder);
			sampler.addMonitor(m_savePanel);
		}
		sig_UpdateView();
		//OC
		FrameListen listener = new FrameListen();
		listener.setWindow(this);
		listener.setSampler(m_sampler);
		this.addComponentListener(listener);
		//\OC

		// OC : for moving the grid with ZQSD
		KeyStroke
		//Move
		strokeZ = KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.ALT_DOWN_MASK),
		strokeQ = KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.ALT_DOWN_MASK),
		strokeS = KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.ALT_DOWN_MASK),
		strokeD = KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.ALT_DOWN_MASK),
		//Clear
		strokeC = KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.ALT_DOWN_MASK),
		//Zoom
		strokeP = KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.ALT_DOWN_MASK),
		strokeM = KeyStroke.getKeyStroke(KeyEvent.VK_M,InputEvent.ALT_DOWN_MASK);
		//Control the simulation
		//strokeSpace = KeyStroke.getKeyStroke("SPACE"),
		//strokeRight = KeyStroke.getKeyStroke("RIGHT"),
		//strokeDown = KeyStroke.getKeyStroke("DOWN");
		Action
		z = new SimulationWindowSingle.ActionMoveStroke(1), q= new SimulationWindowSingle.ActionMoveStroke(2),
		s= new SimulationWindowSingle.ActionMoveStroke(3), d = new SimulationWindowSingle.ActionMoveStroke(4),
		c = new SimulationWindowSingle.ActionMoveStroke(5), p = new SimulationWindowSingle.ActionMoveStroke(6),
		m = new SimulationWindowSingle.ActionMoveStroke(7);// space = new SimulationWindowSingle.ActionMoveStroke(8),
		//right = new SimulationWindowSingle.ActionMoveStroke(9), down = new SimulationWindowSingle.ActionMoveStroke(10);

		m_sampler.GetViewer().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(strokeZ,"z");
		m_sampler.GetViewer().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(strokeQ,"q");
		m_sampler.GetViewer().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(strokeS,"s");
		m_sampler.GetViewer().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(strokeD,"d");
		m_sampler.GetViewer().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(strokeC,"c");
		m_sampler.GetViewer().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(strokeP,"p");
		m_sampler.GetViewer().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(strokeM,"m");
		//m_sampler.GetViewer().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(strokeSpace,"space");
		//m_sampler.GetViewer().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(strokeRight,"right");
		//m_sampler.GetViewer().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(strokeDown,"down");
		m_sampler.GetViewer().getActionMap().put("z", z);
		m_sampler.GetViewer().getActionMap().put("q", q);
		m_sampler.GetViewer().getActionMap().put("s", s);
		m_sampler.GetViewer().getActionMap().put("d", d);
		m_sampler.GetViewer().getActionMap().put("c", c);
		m_sampler.GetViewer().getActionMap().put("p", p);
		m_sampler.GetViewer().getActionMap().put("m", m);
		//m_sampler.GetViewer().getActionMap().put("space", space);
		//m_sampler.GetViewer().getActionMap().put("right", right);
		//m_sampler.GetViewer().getActionMap().put("down", down);

		AddKeyMap(m_sampler.GetViewer());

		//Description of colors :
		m_colorDescriptor = new FLPanel();
		m_colorDescriptor.SetBoxLayoutY();
		if(m_sampler instanceof CAsampler){
			CAsampler sampl = ((CAsampler)m_sampler);

			SetColorDescriptor(sampl);
			if(m_sampler.GetViewer() instanceof OneRegisterAutomatonViewer){
				OneRegisterAutomatonViewer view = ((OneRegisterAutomatonViewer)m_sampler.GetViewer());
				view.setColorPanel(m_colorDescriptor );
			}
			else if(m_sampler.GetViewer() instanceof LineAutomatonViewer){
				LineAutomatonViewer view = ((LineAutomatonViewer)m_sampler.GetViewer());
				view.setColorPanel(m_colorDescriptor);
			}
			else if(sampler.GetViewer() instanceof GridViewer){
				GridViewer grid = ((GridViewer)sampler.GetViewer());
				grid.setColorDescriptor(m_colorDescriptor);
			}
		}
		//\OC

	}
	//OC
	private void SetColorDescriptor(CAsampler sampl) {
		PaintToolKit palette = sampl.GetCellularModel().GetPalette();
		Insets margin = new Insets(6,10,6,10);
		if(palette != null){
			if (palette.GetSize() <= 8) {
				for (int i = 0; i < palette.GetSize(); i++) {
					colorSelectorButton but = new colorSelectorButton("");
					but.setBackground(palette.GetColor(i));
					but.setSampler(sampl);
					but.setPalette(palette);
					FLLabel lab = new FLLabel(" " + i);
					but.setState(i);
					but.setColor(palette.GetColor(i));
					FLPanel color = new FLPanel();
					color.SetBoxLayoutX();
					but.setMargin(margin);
					color.Add(but);
					color.Add(lab);
					m_colorDescriptor.Add(color);
				}
			}
			else {
				m_colorDescriptor.SetBoxLayoutX();
				FLPanel leftPane = new FLPanel();
				leftPane.SetBoxLayoutY();
				FLPanel rightPane = new FLPanel();
				rightPane.SetBoxLayoutY();
				for (int i = 0; i < palette.GetSize(); i++) {
					colorSelectorButton but = new colorSelectorButton("");
					but.setBackground(palette.GetColor(i));
					but.setSampler(sampl);
					but.setPalette(palette);
					FLLabel lab = new FLLabel(" " + i);
					but.setState(i);
					but.setColor(palette.GetColor(i));
					FLPanel color = new FLPanel();
					color.SetBoxLayoutX();
					but.setMargin(margin);
					color.Add(but);
					color.Add(lab);
					if (i % 2 == 0) {
						leftPane.Add(color);
					} else {
						rightPane.Add(color);
					}
				}
				leftPane.setBorder(new EmptyBorder(10, 10, 10, 10));
				m_colorDescriptor.Add(leftPane);
				m_colorDescriptor.Add(rightPane);
			}
		}
	}
	//\OC

	/** Creates all that appears in an experiment 
	 * can be called externally (e.g. for applets) 
	 * MODIF N. GAUVILLE 
	 * FIXME : grid visibilty button (O. Chazé)**/
	final public FLPanel GetSimulationPanel() {
		//Macro.Debug("building 'classical' simulation panel...");
		// TOP (rule)
		m_topbar = m_sampler.GetSimulationRuleControls();
		if (m_topbar != null) {
			m_topbar.setBackground(Color.lightGray);
			m_topbar.setOpaque(true);
		}

		// LEFT
		m_plotPanel = new MeasuringAndPlotterArea(m_sampler);
		m_sidebar = super.GetLeftCommonPanel(false);

		FLPanel widgets = m_sampler.GetSimulationWidgets();
		insertWidgets(widgets); // see this function of improvement
		FLBlockPanel viewerPanel = new FLBlockPanel();
		FLButton viewerButton = new FLButton("Viewer");
		viewerButton.setIcon(IconManager.getUIIcon("rule"));
		viewerPanel.defineAsWindowBlockPanel(FLColor.c_lightred, viewerButton, 100);

		addButtonsVisibleAndKeys(); // work of O. Chazé

		// viewer panel
		FLPanel viewerpanelContent = new FLPanel();
		FLPanel viewerSubPanel = m_sampler.GetViewer().GetSpecificPanel();
		viewerPanel.AddGridBagButton(viewerButton);
		// viewerpanelContent.AddGridBagY(super.GetDynamicsViewPanel());
		viewerpanelContent.setPreferredSize(new Dimension(240, 170));
		viewerpanelContent.setOpaque(false);
		if (viewerSubPanel != null) {
			viewerSubPanel.setOpaque(false);
			viewerpanelContent.AddGridBagY(viewerSubPanel);
		}
		viewerPanel.AddGridBagY(viewerpanelContent);
		viewerPanel.setTabId(FiatLuxProperties.SIDEBAR_TABS.VIEWER);
		viewerPanel.adaptSize(viewerpanelContent);

		if (viewerSubPanel != null && viewerSubPanel.getComponentCount() > 0) {
			m_sidebar.addBlock(viewerPanel, 0, 21);
		}

		// RIGHT - SAMPLER
		FLPanel gridAndInit = new FLPanel() ;
		gridAndInit.SetGridBagLayout();
		gridAndInit.SetAnchor(PAGE_START);
		gridAndInit.getInsets().set(0, 0, 0, 0);
		FLPanel simuPanel = FLPanel.NewPanel(m_sampler.GetViewer());
		simuPanel.setOpaque(false);
		if (simuPanel == null) {
			Macro.SystemWarning("sampler returned empty simulation panel !");
		} else {
			gridAndInit.AddGridBag(simuPanel, 0, 0);
		}
		gridAndInit.AddGridBag(super.GetDynamicsPanel(), 0, 10);

		// main arrangement of panels
		FLPanel MainPanel = new FLPanel();
		MainPanel.SetGridBagLayout();
		MainPanel.GetGridBagC().anchor = PAGE_START;
		MainPanel.GetGridBagC().insets.set(0, 0, 0, 0);

		m_sidebar.setOpaque(true);
		m_sidebar.setOpaque(false);
		gridAndInit.setOpaque(false);
		simuPanel.setOpaque(false);

		// MainPanel.AddGridBag(new JLabel(" "), 0, 0);
		MainPanel.AddGridBag(gridAndInit, 10, 0);
		MainPanel.AddGridBag(m_sidebar, 11, 0);
		m_sidebar.setMaximumSize(new Dimension(300, 2000));

		setTitle("Fiatlux Simulation - " + m_sampler.GetSimulationInfo() + " - " + m_sampler.GetTopologyInfo());

		// (only add the topbar if not empty
		return m_topbar == null ? MainPanel : FLPanel.NewPanelVertical(m_topbar, MainPanel);
	}

	/*-----------------------------------------------------------------------
	 * OBJECTS, BUTTONS, FIELDS
	 *---------------------------------------------------------------------*/

	/** work of O. Chazé **/
	private void addButtonsVisibleAndKeys() {
		//OC : checkbox : grid visible or not
		JCheckBox grid = new JCheckBox("Visible Grid");
		grid.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				m_sampler.GetViewer().setWindowSize(new IntC(getWidth(),getHeight()));
				if (m_sampler.GetViewer() instanceof GridViewer) {
					GridViewer g = (GridViewer) m_sampler.GetViewer();
					g.ChangeGridVisibility();
					sig_UpdateView();
					m_sampler.GetViewer().UpdateWindowSize();//"+exp
				}
				m_sampler.GetViewer().UpdateWindowSize();
			}
		});

		if(m_sampler.GetViewer() instanceof GridViewer){
			GridViewer view = (GridViewer) m_sampler.GetViewer();
			view.setVisibleGrid(grid);
		}

		if(m_sampler.GetViewer() instanceof OneRegisterAutomatonViewer){
			OneRegisterAutomatonViewer view = ((OneRegisterAutomatonViewer)m_sampler.GetViewer());
			view.setMoveButtons(new MoveButton(0),new MoveButton(1), new MoveButton(2), new MoveButton(3) );
		}
		//\OC		
	}
	/*-----------------------------------------------------------------------
	 * SIGNALS
	 *---------------------------------------------------------------------*/
	/*** from N. Gauville : strange logic?
	 * decompisition-recomposition of what has been composed?
	 */
	private void insertWidgets(FLPanel widgets) {
		if (widgets!=null) {
			int ind = 10 * (m_sidebar.getComponentCount() + 1);
			Component[] components = widgets.getComponents();
			for (Component c : components) {
				if (c instanceof InitPanel) {
					m_sidebar.addBlock((JComponent) c, 0, 12);
				} else {
					m_sidebar.addBlock((JComponent) c, 0, ind);
					ind += 10;
				}
			}
		}		
	}
	protected void sig_Close() {
		Macro.print(4,"closing window");
		m_sampler.sig_Close();
		this.stopThread();
		CloseWindow();  // 
	}

	/** reset of experiment */
	public void sig_Init() {
		Macro.PrintInitSignal(2, this);
		m_sampler.sig_Init();

		if (FiatLuxProperties.MINIMIZED_MODE != m_fullScreen) {
			toggleFullScreen();
		}
	}


	/* one basic step */
	public void sig_NextStep() {
		m_sampler.sig_NextStep();
	}

	@Override
	/** shows the current gen */
	final public void sig_UpdateView() {
		m_sampler.sig_Update();
		sig_UpdateWindowInfo();
	}

	/** updates the information of the Window **/
	protected void sig_UpdateWindowInfo() {
		//modelName.setText(m_sampler.GetSimulationInfo());//FIXEME : model name updated at each time step!? 
		//(Wolfram  code recalculated)
		modelLabel.setText(m_sampler.GetSimulationInfo());
		m_TopologyInfo.setText(m_sampler.GetTopologyInfo());
		m_SimulationState.setText(S_SIMULATIONSTATE1 + m_sampler.GetTime() + S_SIMULATIONSTATE2 + (m_sampler.GetTime() > 1 ? "s" : ""));
		m_TopologyInfoM.setText(m_sampler.GetTopologyInfo());
		m_SimulationStateM.setText(S_SIMULATIONSTATE1 + m_sampler.GetTime() + S_SIMULATIONSTATE2 + (m_sampler.GetTime() > 1 ? "s" : ""));
		if (m_TopologyInfoM.getParent() != null) {
			m_TopologyInfoM.getParent().setPreferredSize(new Dimension(m_sampler.GetViewer().getWidth(), 50));
		}

		updateTimeDisplay(m_sampler.GetTime());
	}

	@Override
	public void doToggleFullScreen(boolean fullScreenModeEnabled) {
		if (m_sidebar != null && m_sidebar.getParent() != null) {
			m_sidebar.getParent().setBackground(fullScreenModeEnabled ? Color.darkGray : UIManager.getColor("Panel.background"));
			m_sidebar.setVisible(!fullScreenModeEnabled);
		}
		setSize(getPreferredSize());
	}

	//OC
	/*-----------------------------------------------------------------------
	 * LISTENER
	 *---------------------------------------------------------------------*/

	private class FrameListen implements ComponentListener {
		private SimulationWindowSingle m_window;
		//Initial size of the window
		private int m_initWindowWidth;
		private int m_initWindowHeight;
		private boolean firstResize = true;
		private SuperSampler m_sampler;
		private int m_initViewerWidth;
		private int m_initViewerHeight;

		public void setWindow(SimulationWindowSingle window){
			m_window = window;
		}
		public void setInitWindowSize(int initWindowWidth, int initWindowHeight){
			m_initWindowWidth = initWindowWidth;
			m_initWindowHeight = initWindowHeight;

		}
		public void setSampler(SuperSampler sampler){
			m_sampler = sampler;
		}
		public void componentHidden(ComponentEvent arg0) {
		}
		public void componentMoved(ComponentEvent arg0) {
		}
		public void componentResized(ComponentEvent arg0) {
			if(firstResize){
				m_initWindowWidth = m_window.getWidth();
				m_initWindowHeight = getHeight();
				firstResize = false;
				m_initViewerWidth = m_sampler.GetViewer().getWidth();
				m_initViewerHeight = m_sampler.GetViewer().getHeight();
				//m_sampler.GetViewer().setm_InitWindowSize(new IntC());
			}
			else {

				int min = (int)(min(m_window.getWidth()-m_initWindowWidth,m_window.getHeight()-m_initWindowHeight));
				int viewWidth = m_initViewerWidth + min;
				int viewHeight = m_initViewerHeight + min;
				m_sampler.GetViewer().setWindowSize(new IntC(m_window.getWidth(),m_window.getHeight()));

				if (m_sampler.GetViewer() instanceof GridViewer) {
					GridViewer g = (GridViewer) m_sampler.GetViewer();
					g.resizeGrid(new IntC(viewWidth,viewHeight));
				}

				m_sampler.GetViewer().setSize(new Dimension(viewWidth,viewHeight));
				m_sampler.GetViewer().UpdateWindowSize();


			}
		}
		public void componentShown(ComponentEvent arg0) {

		}
	}
	//\OC

	//OC
	/** to open a colorChoser to chose colors of each state **/
	public class colorSelectorButton extends FLButton{
		Color m_currentColor;
		PaintToolKit m_palette;
		int m_state;
		CAsampler m_sampler;
		public colorSelectorButton(String in_str) {
			super(in_str);;
			m_currentColor = new Color(255,255,255);
			addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					Color newColor = JColorChooser.showDialog(null, "Choose a color", m_currentColor);
					setColor(newColor);
				}
			});
		}
		public void setColor(Color color){
			//setPalette(m_sampler.GetCellularModel().GetPalette());
			/* Warning : Originally, the palette is defined in the model, but is not updated then. Only palette in the viewer is up to date */
			if(color!=null){
				m_currentColor = color;
				this.setBackground(color);
				m_palette.SetColor(m_state, new FLColor(m_currentColor));
				m_sampler.GetViewer().SetPalette(m_palette);
				sig_UpdateView();
			}

		}
		public void setPalette(PaintToolKit palette){
			m_palette = palette;
		}
		public void setSampler(CAsampler sampler) { m_sampler = sampler; }
		public void setState(int state){
			m_state = state;
		}


	}


	/** to move the grid with ZQSD key**/
	private void AddKeyMap(JComponent comp) {

		Macro.print(4, this, "finalize : called before destroying object...");
	}
	public class ActionMoveStroke extends AbstractAction {
		final private int m_id;

		public ActionMoveStroke(int id) {
			m_id= id;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			//Macro.Debug("key stroke detected");
			switch(m_id) {
			case 1: pressZ(); break;
			case 2: pressQ();break;
			case 3: pressS(); break;
			case 4: pressD(); break;
			case 5: pressC(); break;
			case 6 : pressP(); break;
			case 7 : pressM(); break;
			}

		}
	}

	public class MoveButton extends FLActionButton {
		final int m_move;
		public MoveButton(int move){
			super("");
			m_move = move;
			switch(move){
			case 0 : this.setText("Z"); break;
			case 1 : this.setText("Q");break;
			case 2 : this.setText("S");break;
			case 3 : this.setText("D");break;
			}

		}
		@Override
		public void DoAction(){
			switch(m_move){
			case 0 : pressZ(); break;
			case 1 : pressQ();break;
			case 2 : pressS();break;
			case 3 : pressD();break;
			}

		}
	}

	public void pressZ() {
		if (m_sampler.GetViewer() instanceof GridViewer){
			((GridViewer)m_sampler.GetViewer()).pressZ();
			sig_UpdateView();
		}
	}
	public void pressQ(){
		if (m_sampler.GetViewer() instanceof GridViewer){
			((GridViewer)m_sampler.GetViewer()).pressQ();
			sig_UpdateView();
		}
	}
	public void pressS(){
		if (m_sampler.GetViewer() instanceof GridViewer){
			((GridViewer)m_sampler.GetViewer()).pressS();
			sig_UpdateView();
		}
	}
	public void pressD(){
		if (m_sampler.GetViewer() instanceof GridViewer){
			((GridViewer)m_sampler.GetViewer()).pressD();
			sig_UpdateView();
		}
	}

	public void pressC(){
		if (m_sampler instanceof CAsimulationSampler) {
			((CAsimulationSampler) m_sampler).ClearArray();
		}
		/*if(m_sampler.GetViewer() instanceof OneRegisterAutomatonViewer){
			((OneRegisterAutomatonViewer)m_sampler.GetViewer()).pressD();
			sig_UpdateView();
		}*/
	}

	public void pressP(){
		if (m_sampler.GetViewer() instanceof GridViewer){
			GridViewer view = (GridViewer) (m_sampler.GetViewer());
			view.zoom(true);
			sig_UpdateView();
		}
	}

	public void pressM(){
		if (m_sampler.GetViewer() instanceof GridViewer){
			GridViewer view = (GridViewer) (m_sampler.GetViewer());
			view.zoom(false);
			sig_UpdateView();
		}
	}
	//\OC
	/*-----------------------------------------------------------------------
	 * OTHERS
	 *---------------------------------------------------------------------*/

}
