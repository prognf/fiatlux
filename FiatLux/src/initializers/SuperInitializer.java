package initializers;

import javax.swing.JComponent;

import architecture.GraphicallyControllable;
import components.arrays.SuperSystem;
import components.randomNumbers.StochasticSource;
import components.types.FLsignal;
import components.types.IntegerList;
import components.types.Monitor;
import main.Macro;
import topology.basics.SuperTopology;

abstract public class SuperInitializer 
extends StochasticSource
implements Monitor, GraphicallyControllable
{

	public abstract void LinkTo(SuperSystem system, SuperTopology topologyManager);
	
	/** used for transmitting the init signal to the system
	 * (if necessary) 
	 */
	abstract protected void PreInit();
	
	/** main init part **/
	abstract protected void SubInit();

	/*---------------------------------------------------------------------------
	  - construction
	  --------------------------------------------------------------------------*/
	protected SuperInitializer() {
		super();
	}

	/*---------------------------------------------------------------------------
	  - signals
	  --------------------------------------------------------------------------*/
	@Override
	public void ReceiveSignal(FLsignal sig){
		switch(sig){
		case init:
			sig_Init();
			break;
		case update:
			sig_Update();
			break;
		case nextStep:
			sig_NextStep();
			break;
		}
	}

	final private void sig_Init(){
		Macro.print(4, "Initialiser : sig_Init");
		GetRandomizer().sig_ResetRandomizer();
		PreInit();
		SubInit();
	}

	/** override if needed */
	public void sig_NextStep() {}

	/** override if needed */
	public void sig_Update() {}


	/*---------------------------------------------------------------------------
	  - gfx
	  --------------------------------------------------------------------------*/


	/** override if needed **/
	public JComponent GetSpecificPanel(){
		return null;
	}



	/*---------------------------------------------------------------------------
	  - others : change palce of this ???
	  --------------------------------------------------------------------------*/


	/** returns a list of k random positions in the grid of size n
	 * k should be smaller than the grid size
	 */
	protected IntegerList RandomKpositions(int k, int n){
		if (k>n){
			Macro.SystemWarning(" Random positions : number of pos greater than grid size - setting k ="  + n);
			k= n;
		}
		return IntegerList.getKamongN(k, n, GetRandomizer());
	}
	

}
