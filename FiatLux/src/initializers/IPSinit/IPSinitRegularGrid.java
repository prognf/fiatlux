package initializers.IPSinit;

import architecture.interactingParticleSystems.InteractingParticleSystemPlanarAbstract;
import architecture.interactingParticleSystems.InteractingParticleSystemRegular;
import components.arrays.SuperSystem;
import components.types.IntegerList;
import initializers.SuperInitializer;
import topology.basics.PlanarTopology;
import topology.basics.SuperTopology;

/** why is this clas is complex ? 
 * because of cell sorting **/
abstract public class IPSinitRegularGrid extends SuperInitializer {
	/*---------------------------------------------------------------------------
	  - abstract
	  --------------------------------------------------------------------------*/
	abstract public String GetName();

	/** main function **/
	abstract public void performInitProcedure();

	/*----------------------------------------------------------------------------
	  - attributes
	  --------------------------------------------------------------------------*/
	PlanarTopology m_topo;
	private InteractingParticleSystemPlanarAbstract m_sys;
	IntegerList m_allCellpos;
	boolean m_reconstruct ; // shall we rebuild the neighbourhood after each init ?

	/*---------------------------------------------------------------------------
	  - construction
	  --------------------------------------------------------------------------*/
	public IPSinitRegularGrid(boolean reconstructLinks) {
		super();
		m_reconstruct= reconstructLinks;
	}

	public void LinkTo(SuperSystem sys, SuperTopology topo) {
		m_sys= (InteractingParticleSystemPlanarAbstract)sys;	
		m_topo= (PlanarTopology)topo;
		m_allCellpos= IntegerList.NintegersFromZero( GetSize() );
		//TODO : clarify this
		if (sys instanceof InteractingParticleSystemRegular) {
			if (!m_reconstruct){
				((InteractingParticleSystemRegular)m_sys).ConstructExchangeLinks();
			}
		}
	}

	/*---------------------------------------------------------------------------
	  - actions
	  --------------------------------------------------------------------------*/
	@Override
	protected void PreInit() {
	}

	protected void SubInit() {
		performInitProcedure();
		if (m_sys instanceof InteractingParticleSystemRegular) {
			if (m_reconstruct){
				((InteractingParticleSystemRegular)m_sys).ConstructExchangeLinks();
			}
		}
	}

	/*---------------------------------------------------------------------------
	  - protected get & set
	  --------------------------------------------------------------------------*/

	protected void TagBorderCell(int x, int y) {
		if (m_sys instanceof InteractingParticleSystemRegular) {
				((InteractingParticleSystemRegular)m_sys).TagBorderCell(x, y);
		}
	}

	protected void InitCellState(int pos, int state){
		m_sys.SetCellState(pos, state);
	}

	protected void InitCellStateXY(int x, int y, int state){
		int pos= m_topo.GetIndexMap2D(x, y);
		InitCellState(pos, state);
	}

	protected int GetCellState(int pos) {
		return m_sys.GetCellState(pos);
	}

	/** for iterating over all cells **/
	protected IntegerList GetAllCellPos() {
		return m_allCellpos;
	}

	protected int GetSize() {
		return m_topo.GetSize();
	}

	protected int GetXsize() {
		return m_sys.GetXYsize().X();
	}

	protected int GetYsize() {
		return m_sys.GetXYsize().Y();
	}

	/*---------------------------------------------------------------------------
	  - general selection code to object
	  --------------------------------------------------------------------------*/

	public enum CODE_IPS_INIT { Particles, CellSorting, Bernoulli, Sultra}

	/** general, calld by IPSsampler **/
	static public IPSinitRegularGrid TypeToInitializier(CODE_IPS_INIT ipsInitType){
		switch(ipsInitType){
		case Bernoulli:
			return new BernoulliIPSinit();
		case Particles:
			return new ParticlesIPSinit();
		case CellSorting:
			return new ThreeTypesIPSinit();
		case Sultra:
			return new SultraWeavingIPSinit();
		default:
			return null;
		}		
	}

	/*---------------------------------------------------------------------------
	  - test
	  --------------------------------------------------------------------------*/

	final static String CLASSNAME= Thread.currentThread().getStackTrace()[1].getClassName();
}
