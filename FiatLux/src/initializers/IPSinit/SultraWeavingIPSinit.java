package initializers.IPSinit;

import components.types.IntPar;
import grafix.gfxTypes.elements.FLPanel;

public class SultraWeavingIPSinit extends IPSinitRegularGrid {


	private static final String NAME = "SultraWeavingInit";
	private static final String NPARTICLES = "# particles";
	private static final int REP = 5; //SultraSpecialTM.LT
	
	
	IntPar m_RepCell = new IntPar("Rep Par:", REP); // repetition step

	public SultraWeavingIPSinit() {
		super(false);	
	}
	
	
	@Override
	public void performInitProcedure() { 
		
		int X=m_topo.GetXsize(), Y= m_topo.GetYsize();
		int repStep= m_RepCell.GetVal();
		int currentWire=0;
		int count= -1;
		for (int y=0 ; y < Y; y++){
			for (int x=0 ; x < X; x++){
				count = count +1; 
				if (count == repStep){
					count=0;
					currentWire++;
				}
				InitCellStateXY(x, y, currentWire);
			}
		}
	}	

	//@Override
	public FLPanel GetSpecificPanel() {
		FLPanel p = FLPanel.NewPanelVertical(m_RepCell.GetControl(), GetRandomizer().GetSeedControl());
		p.setOpaque(false);
		return p;
	}


	@Override
	public String GetName() {
		return NAME;
	}

}
