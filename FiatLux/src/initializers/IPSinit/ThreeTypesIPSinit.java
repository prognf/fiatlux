package initializers.IPSinit;

import components.arrays.SuperSystem;
import components.types.IntPar;
import components.types.IntegerList;
import grafix.gfxTypes.elements.FLPanel;
import topology.basics.SuperTopology;

public class ThreeTypesIPSinit extends IPSinitRegularGrid{

	public ThreeTypesIPSinit() {
		super(true); // reconstruction needed a priori
	}


	private static final String NAME = "3typeInit";
	
	private static final String Dens1 = "Dens1", Part1= "Part1";
	private static final double DEF_dens = 0;
	private static final int DEF_part= 200;
	private static final int DIVFACTOR = 2; // priority over the previous setting
	private static final int BORDER = 0, ONE=1, TWO=2;
	
	IntPar m_part1 = new IntPar(Part1, DEF_part);


	@Override
	public void LinkTo(SuperSystem sys, SuperTopology topo) {
		super.LinkTo(sys, topo);	
		int intPartSetting= (m_topo.GetXsize() - 2 ) * (m_topo.GetYsize() - 2 ) / DIVFACTOR;
		m_part1.SetVal(intPartSetting);	
		
	}

	@Override
	/** particle init **/
	public void performInitProcedure() {
	int part= m_part1.GetVal();

		// background setting
		for (int pos : GetAllCellPos()){
			InitCellState(pos, TWO);
		}

		// border setting
		int X= m_topo.GetXsize(), Y= m_topo.GetYsize();
		for (int x=0; x< X; x++){
			InitCellStateBorder(x, 0);
			InitCellStateBorder(x, Y-1);
		}
		for (int y=0; y< Y; y++){
			InitCellStateBorder(0, y);
			InitCellStateBorder(X-1, y);
		}
		// non-border list construction
		IntegerList nonBorder= new IntegerList();
		for (int pos : GetAllCellPos()){
			if ( GetCellState(pos) != BORDER ){
				nonBorder.addVal(pos);
			}
		}
		// 1-cells setting
		IntegerList ones= nonBorder.extractStronglyKRandomElements(part, GetRandomizer());
		for (int pos: ones){
			InitCellState(pos, ONE);
		}
	}


	private void InitCellStateBorder(int x, int y) {
		InitCellStateXY(x, y, BORDER);
		TagBorderCell(x, y);
	}
	
	//@Override
	public FLPanel GetSpecificPanel() {
		FLPanel p = FLPanel.NewPanelVertical(m_part1.GetControl(), GetRandomizer().GetSeedControl());
		p.setOpaque(false);
		return p;
	}


	@Override
	public String GetName() {
		return NAME;
	}

	
}
