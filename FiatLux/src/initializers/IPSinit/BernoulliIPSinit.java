package initializers.IPSinit;

import components.types.FLStringList;
import grafix.gfxTypes.controllers.DoubleController;
import grafix.gfxTypes.elements.FLPanel;

public class BernoulliIPSinit extends IPSinitRegularGrid {

	private static final String NAME = "BernoulliInit";
	private static final String DENS = "density (in %)";
	private static final String INITCONFIGFILE = "init.flc";

	double m_ProbaToOne= 0.5;
	//String [] INITCHOICE = { "Bernoulli", "file" };
	//FLList m_gfxInitChoice = new FLList(INITCHOICE);

	public BernoulliIPSinit() {
		super(false);
	}

	private void Bernoulli() {
		for (int pos = 0; pos < GetSize(); pos++) {
			InitCellState(pos, RandomEventDouble(m_ProbaToOne)?1:0);
		}
	}

	@Override
	public void performInitProcedure() {
		//int choice= m_gfxInitChoice.GetSelectedItemRank(); 
		//if (choice==0){
			Bernoulli();
		//} else {
		//	ReadConfig();
		//}
	}

	
	/**
	 * SPECIAL : work with Irène
	 */
	private void ReadConfig() {
		FLStringList read= FLStringList.OpenFile(INITCONFIGFILE);
		int size= read.size(); 
		if (size!=0){
			int X= GetXsize(), Y= GetYsize();
			for (int y=0; y< Y; y++){
				String s = read.Get(y % size);
				int len = s.length();
				for (int x=0; x< X; x++){
					char c= s.charAt(x % len);
					int state = c % 2;
					InitCellStateXY(x, y, state);
				}
			}
		}
	}

	//@Override
	public FLPanel GetSpecificPanel() {
		FLPanel p = FLPanel.NewPanelVertical(new InitDensityControl(), /*m_gfxInitChoice,*/ GetRandomizer().GetSeedControl());
		p.setOpaque(false);
		return p;
	}


	public void SetInitDens(double val) {
		m_ProbaToOne= val;			
	}

	public String GetName() {
		return NAME;
	}

	private class InitDensityControl extends DoubleController {

		public InitDensityControl() {
			super(DENS, DoubleController.CONVERSIONRATE.PERCENT);
		}

		@Override
		public double ReadDouble() {
			return m_ProbaToOne;
		}

		@Override
		public void SetDouble(double val) {
			SetInitDens(val);			
		}		

	}

	

}
