package initializers.IPSinit;

import components.types.IntegerList;
import grafix.gfxTypes.controllers.IntController;
import grafix.gfxTypes.elements.FLPanel;

public class ParticlesIPSinit extends IPSinitRegularGrid {



	private static final String NAME = "IPSparticlesInit";
	private static final String NPARTICLES = "# particles";
	private static final boolean PART = false;

	private int m_Nparticles=50;

	public ParticlesIPSinit() {
		super(false);	
	}

	@Override
	public void performInitProcedure() { // "lazy" init
		int L= m_topo.GetSize();
		if (PART){
			IntegerList ones= IntegerList.getKamongN(m_Nparticles, L, GetRandomizer());
			for (int pos : ones){
				InitCellState(pos, 1);
			}
		} else {
			int X= m_topo.GetXsize(), Y= m_topo.GetYsize();
			for (int x=0; x < X; x++){
				for (int y=0; y < Y; y++){
					//int state = (y + (x+1)/2 ) % 2;
					int z=(x>=X/2)?1:0;
					int state = (x+y+z) % 2;
					InitCellStateXY(x, y, state);
				}
			}
		}
	}

	//@Override
	public FLPanel GetSpecificPanel() {
		FLPanel p = FLPanel.NewPanelVertical(new Nparticles(), GetRandomizer().GetSeedControl());
		p.setOpaque(false);
		return p;
	}


	class Nparticles extends IntController{
		public Nparticles() {
			super(NPARTICLES);
		}

		@Override
		protected int ReadInt() {
			return m_Nparticles;
		}

		@Override
		protected void SetInt(int val) {
			m_Nparticles= val;			
		}		
	}

	@Override
	public String GetName() {
		return NAME;
	}

}
