package initializers.IPSinit;

import architecture.interactingParticleSystems.InteractingParticleSystemPlanarAbstract;
import components.arrays.SuperSystem;
import grafix.gfxTypes.controllers.DoubleController;
import grafix.gfxTypes.elements.FLPanel;
import initializers.SuperInitializer;
import topology.basics.SuperTopology;

public class IPSinitSimple extends SuperInitializer {
	private static final String NAME = "SimpleIPSinit";
	private static final String DENS = "density (in %)";
	private static final String INITCONFIGFILE = "init.flc";

	double m_ProbaToOne = 0.5;
	private InteractingParticleSystemPlanarAbstract m_sys;

	@Override
	public void LinkTo(SuperSystem sys, SuperTopology topo) {
		m_sys= (InteractingParticleSystemPlanarAbstract)sys;	
	}
	
	
	private void Bernoulli() {
		for (int pos = 0; pos < GetSize(); pos++) {
			InitCellState(pos, RandomEventDouble(m_ProbaToOne)?1:0);
		}
	}
	private void InitCellState(int pos, int state) {
		m_sys.SetCellState(pos, state);
	}


	private int GetSize() {
		return m_sys.GetNsize();
	}


	@Override
	protected void PreInit() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void SubInit() {
		Bernoulli();
	}
	
	public void SetInitDens(double val) {
		m_ProbaToOne= val;			
	}

	public String GetName() {
		return NAME;
	}
	
	private class InitDensityControl extends DoubleController {
		public InitDensityControl() {
			super(DENS, DoubleController.CONVERSIONRATE.PERCENT);
		}

		@Override
		public double ReadDouble() {
			return m_ProbaToOne;
		}

		@Override
		public void SetDouble(double val) {
			SetInitDens(val);			
		}		

	}
	
	//@Override
	public FLPanel GetSpecificPanel() {
		FLPanel p = FLPanel.NewPanelVertical(new InitDensityControl(), GetRandomizer().GetSeedControl());
		p.setOpaque(false);
		return p;
	}
}
