package initializers.CAinit;

import components.allCells.MultiRegisterCell;

abstract public class MultiRegisterInitializer extends ArrayInitializer {

	//protected void LinkTo(RegularDynArray in_automaton, SuperTopology in_topo) {
	//protected void SubInit() {

	/** CAST HERE **/
	public MultiRegisterCell GetCell(int pos) {
		return (MultiRegisterCell) super.GetCell(pos);
	}


	public int GetCellStateI(int pos, int register){
		return GetCell(pos).GetStateI(register);
	}

	public void SetCellStateI(int pos, int register, int state){
		GetCell(pos).SetStateI(register, state);
	}

	protected int GetLsize() {
		return super.GetSize();
	}

	
	
}
