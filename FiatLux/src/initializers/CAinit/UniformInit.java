package initializers.CAinit;

import grafix.gfxTypes.controllers.IntController;
import grafix.gfxTypes.elements.FLPanel;

public class UniformInit extends OneRegisterInitializer {

	private static final String RANGE_TXT="range";
	private int m_StateRange = 2;

	@Override
	protected void SubInit() {
		for (int i = 0; i < GetLsize(); i++) {
			int st= RandomInt(m_StateRange); 
			InitState(i, st);
		}
	}

	public void SetStateRange(int range){
		m_StateRange= range;
	}
	
	
	@Override
	public FLPanel GetSpecificPanel() {
		FLPanel p = FLPanel.NewPanel(new RangeSetting(), this.GetRandomizer());
		p.setOpaque(false);
		return p;
	}
	
	class RangeSetting extends IntController{

		public RangeSetting() {
			super(RANGE_TXT);
		}

		@Override
		protected int ReadInt() {
			return m_StateRange;
		}

		@Override
		protected void SetInt(int val) {
			m_StateRange= val;
		}

	}
	
}
