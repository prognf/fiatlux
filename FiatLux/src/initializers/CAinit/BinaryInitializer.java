package initializers.CAinit;

import java.awt.Dimension;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import components.types.FLString;
import components.types.FLsignal;
import components.types.IntegerList;
import grafix.gfxTypes.controllers.DoubleController;
import grafix.gfxTypes.controllers.IntController;
import grafix.gfxTypes.elements.FLPanel;
import grafix.gfxTypes.elements.FLTabbedPane;
import grafix.gfxTypes.elements.FLTextField;
import main.Macro;


/*--------------------
 * initialises arrays with two states
 * @author Nazim Fates
 *--------------------*/
public class BinaryInitializer extends OneRegisterInitializer {


	final static int Lsq= 5;//special square init

	private static final String REGIONS_TXT = "length";
	final static String	S_RATE="Rate (%)";
	private static final int SZ_PATTERN_PARSER = 8;
	private static final byte NSTATES = 2, StateInit = NSTATES- 1;

	private static final int DEFREGIONSIZE = 1; // constant to put in main file

	/*----------------------------------------------------------------------------
	  - attributes
	  --------------------------------------------------------------------------*/

	private double m_ProbaToOne = 0.5;
	public enum INITSTYLE 
	{ Bernoulli, Pattern, OneRegion, TwoRegions, ReadConfig, InSquare}

	private INITSTYLE m_InitStyle;
	int [] m_initPattern= { 0 };
	String m_FrontPattern = "1"; // not used

	int m_RegionSize=DEFREGIONSIZE;

	class Configuration extends IntegerList {

		public Configuration(int Lsize) {
			super(Lsize,0);
		}

		/** empty list : use with care **/
		public Configuration() {
		}

	}


	/*---------------------------------------------------------------------------
	  - construction
	  --------------------------------------------------------------------------*/
	public BinaryInitializer(){
		super();
		m_InitStyle = INITSTYLE.Bernoulli; //default
	}

	/*---------------------------------------------------------------------------
	  - actions
	  --------------------------------------------------------------------------*/

	/** MAIN : called by sig_Init
	 * random init with state values in {0, StateRange - 1} FillRate determines
	 * how many cells are not in state 0
	 */

	protected void  SubInit() {
		//Macro.Debug("BINARYINIT: init style is now:" + m_InitStyle);
		/*if( m_InitStyle==INITSTYLE.Bernoulli) {
			Macro.Debug(" dini (probaToone):"  + m_ProbaToOne);
		}*/
		switch( m_InitStyle){
		case Bernoulli:
			Bernoulli(); break;
		case Pattern:
			PatternInit(); break;
		case OneRegion:
			OneRegionInit(); break;
		case TwoRegions:
			TwoRegionsInit(); break;
		case InSquare:
			InSquareInit(); break;
		default:
			Macro.FatalError(" unrecognised init style");
		} 
	} 

	/** NEW : interpreting commands **/
	public void interpretCommand(String strCmd) {
		//Macro.Debug(" ** parsing :" + strCmd);
		Configuration res= parseCmd(strCmd);
		res.Print();
		initConfiguration(res);
		res.Print();
		super.ReceiveSignal(FLsignal.init);
	}



	private void initConfiguration(Configuration res) {
		//Macro.Debug(" >>>> init : setting vals");
		res.Print();
		if (res.GetSize()!= GetLsize()) {
			Macro.FatalError("problem with config. size [init]");
		}
		for (int pos=0; pos< GetLsize(); pos++) {
			InitState(pos, res.Get(pos));
		}
	}

	private Configuration parseCmd(String strCmd) {
		//Macro.Debug(" PARSING:" + strCmd);
		char firstLetter=strCmd.charAt(0);
		switch (firstLetter) {
		case 'S':
			Configuration config= new Configuration(  ); // empty
			if (config.GetSize()!=0) {
				Macro.FatalError("should be empty");
			}
			int delta=1;
			int seed=getIntAfterChar(delta, strCmd);
			this.SetSeed(seed);
			for (int i = 0; i < GetLsize(); i++) {
				int st= RandomEventDouble(m_ProbaToOne)?StateInit:0; 
				config.add(st);
			}
			return config;
		case 'R':
			String rest=strCmd.substring(1);
			Configuration conf= parseCmd(rest);
			conf.Print();
			Configuration rev= reverseConfig(conf);
			rev.Print();
			return rev;
		}
		return null;
	}

	/** TOUDOU : very bad !! **/
	private Configuration reverseConfig(Configuration config) {
		//Collections.reverse(config);

		int L = GetLsize();
		Configuration niou= new Configuration(L);
		for (int pos=0; pos < L; pos++) {
			niou.set(pos, config.Get(L-1-pos));
		}
		return niou;
	}

	/** extracts int after at a given position **/
	private int getIntAfterChar(int delta, String strCmd) {
		return FLString.ParseInt(strCmd.substring(delta));
	}

	/** using the String codes for setting init style **/
	public void SetInitCode(String initCode){
		char Icode = initCode.charAt(0);
		switch (Icode){
		case 'R':
			int len= FLString.IParseInString(initCode, "R");
			m_RegionSize= len;
			SetInitStyle(INITSTYLE.OneRegion);
			break;
		default:
			Macro.SystemWarning("Unrecognised init code:" + initCode);
		}
	}

	/** Bernoulli **/
	protected void Bernoulli() {
		for (int i = 0; i < GetLsize(); i++) {
			int st= RandomEventDouble(m_ProbaToOne)?StateInit:0; 
			InitState(i, st);
		}
	}

	/** two regions **/
	private void TwoRegionsInit() { 
		int shift= GetLsize() / 2 - 2 * m_RegionSize;
		int distance= 2 * m_RegionSize;
		for (int dx=0; dx < m_RegionSize; dx++){
			InitState(shift + dx, StateInit);
			InitState(shift + distance + dx, StateInit);
		}
	}


	/** one region with a front pattern over a background pattern **/
	private void OneRegionInit(){
		int L= GetLsize();
		int [] background= m_initPattern;
		byte [] front= m_FrontPattern.getBytes();

		int start= L/2 - m_RegionSize / 2, 
				end= start + m_RegionSize - 1 ;
		for (int i=0; i<L; i++){
			int state;
			if ((i>=start) && (i<=end)){
				state= front[i % front.length]% NSTATES ;
			} else {
				state= background[i % background.length] % NSTATES;
			}	    	
			InitState(i, state);
		}
	}



	/** special 2D for the study of Life's transient */
	public void InSquareInit(){
		int X= 2, Y= 2;
		for (int point=0; point<10;point++){
			int x= RandomInt(Lsq);
			int y= RandomInt(Lsq);
			InitStateXY(X+x, Y+y,1);
		}
	}

	/** one repeated pattern **/
	public void PatternInit(){
		int L= GetLsize();

		for (int i=0; i<L; i++){
			int j= i % m_initPattern.length;
			int state= m_initPattern[j] % NSTATES;
			InitState(i, state);
		}
	}

	/** LOAD a config : splits a link with white spaces and transforms it into an array of integers **/
	private static int[] String2IntArray(String line) {
		String [] read= line.split(" ");
		int size=read.length;
		int [] data = new int[size]; 
		for (int i=0; i < size; i++){
			data[i]= FLString.String2Int( read[i] );
		}
		return data;
	}

	/*---------------------------------------------------------------------------
	  - get & set
	  --------------------------------------------------------------------------*/
	public void SetInitPattern(String pattern) {
		if (pattern.length()==0){
			Macro.SystemWarning(" empty pattern in the setting");
		} else {
			byte [] p= pattern.getBytes();
			m_initPattern = new int[p.length];
			for (int i=0; i< p.length; i++){
				m_initPattern[i]= (int)p[i];
			}
		}
	}

	/** external setting of d_ini **/
	public void SetInitRate(double dens) {
		m_ProbaToOne= dens;		
		Macro.print("Init. density set to "+dens);
	} 



	/** do not put public : risk of side effects with GFX **/
	private void SetInitStyle(INITSTYLE style){
		m_InitStyle= style;
		//Macro.Debug("*$*$* init style set to :" + m_InitStyle);
	}

	/** external change **/
	public void ForceInitStyle(INITSTYLE style){
		SetInitStyle(style);
	}

	/** use with greatest care !**/
	public void ExternalInitCell(int pos, int state) {
		InitState(pos, state);
	}

	/*---------------------------------------------------------------------------
	  - GFX
	  --------------------------------------------------------------------------*/

	@Override
	public FLPanel GetSpecificPanel() {
		FLPanel tab_bernoulli= FLPanel.NewPanel();
		tab_bernoulli.AddRand( new InitDensityControl(), GetRandomizer());
		tab_bernoulli.SetBoxLayoutY();

		FLPanel tab_Pattern= FLPanel.NewPanel(new PatternParser());
		FLPanel tab_FrontBack= FLPanel.NewPanelVertical(new RegionsLengthSetting(), new PatternParser());
		FLPanel tab_Regions= FLPanel.NewPanelVertical(new RegionsLengthSetting());
		// FLPanel tab_Read= FLPanel.NewPanel();

		InitTabPane tab = new InitTabPane();
		tab.setPreferredSize(new Dimension(FLPanel.PANEL_SIZE - 20, 120));
		tab.AddTab("Bernoullli", tab_bernoulli);
		tab.AddTab("Pattern", tab_Pattern);
		tab.AddTab("1 Region", tab_FrontBack);
		tab.AddTab("2 Regions", tab_Regions);
		// tab.AddTab("Read config.", tab_Read);

		FLPanel p = new FLPanel();
		p.Add(tab);
		p.setOpaque(false);
		return p;
	}


	class InitTabPane extends FLTabbedPane implements ChangeListener {
		private InitTabPane(){
			this.addChangeListener(this);
		}

		@Override
		public void stateChanged(ChangeEvent arg0) {
			int index= GetSelectedTabIndex();
			//			Macro.Debug("===== state change with index :" + index);
			switch(index){
			case 0:
				SetInitStyle( INITSTYLE.Bernoulli ); 
				break;
			case 1:
				SetInitStyle( INITSTYLE.Pattern );
				break;
			case 2:
				SetInitStyle( INITSTYLE.OneRegion );
				break;
			case 3:
				SetInitStyle( INITSTYLE.TwoRegions );
				break;
			case 4:
				SetInitStyle(INITSTYLE.ReadConfig);
				break;
			}
		}
	}

	class RegionsLengthSetting extends IntController{

		public RegionsLengthSetting() {
			super(REGIONS_TXT);
		}

		@Override
		protected int ReadInt() {
			return m_RegionSize;
		}

		@Override
		protected void SetInt(int val) {
			m_RegionSize= val;
		}

	}

	class PatternParser extends FLTextField {

		protected PatternParser() {
			super(SZ_PATTERN_PARSER);
			//this.setText(m_InitPattern);
		}

		@Override
		public void parseInput(String input) {
			if (input.length()>0){
				SetInitPattern(input);
			}
		}

	}	

	private class InitDensityControl extends DoubleController{

		public InitDensityControl() {
			super(S_RATE, DoubleController.CONVERSIONRATE.PERCENT);
		}

		@Override
		public double ReadDouble() {
			return m_ProbaToOne;
		}

		@Override
		public void SetDouble(double val) {
			SetInitRate(val);			
		}

	}

	//--------------------------------------------------------------------------
	//- test
	//--------------------------------------------------------------------------

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();

}


