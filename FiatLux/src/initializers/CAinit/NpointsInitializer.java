package initializers.CAinit; 

import components.types.IntPar;
///import CellularAutomata.*;
import grafix.gfxTypes.elements.FLPanel;


/************************************
 * sets an exact number of points calculated with Npoints
  *******************************************/ 
public class NpointsInitializer extends  OneRegisterInitializer {   

	final static int DEF_NPOINTS=0, ONE=1;
	final static String S_NPOINTS= "N points:";
	
	private IntPar m_Npoints= new IntPar(S_NPOINTS, DEF_NPOINTS);
	
	
	
	static int [] XRB= { 2, 0, 0, 2, 4, 1, 3, 0, 2, 1};
	static int [] YRB= {12,12,10,10,15, 9,13, 9,13, 12};
	//static int [] Y= { 9, 9, 7, 7,12, 6,10, 6,10, 9};
	
	static int [] XDM= { 0, 1, 1, 1, 1, 2, 2, 2, 2, 3};
	static int [] YDM= { 0, 1, 2, 3, 6, 1, 3, 5, 6, 4};

	static int [] XYF= { 0, 1, 2, 2, 3, 3, 3, 3, 4, 4};
	static int [] YYF= { 0, 2, 1, 3, 1, 2, 4, 5, 3, 5};

	
	
	public void SubInit(){
		//initMartinetto(); 
		int Xm = GetXsize()/2, Ym=GetYsize()/2;
		initArray(XYF, YYF, Xm, Ym);
		return;
		/*int Lsize= GetLsize();
		IntegerList points= IntegerList.GetKAmongNsorted(m_Npoints.GetVal(), Lsize, GetRandomizer() );
		for (int pos : points){
			InitState(pos, ONE);
		}*/
	}
	
	public void initArray(int []X, int [] Y, int dx, int dy) {
		assert(X.length==Y.length);
		for (int i=0; i <X.length; i++) {
			InitStateXY(X[i]+dx, Y[i]+dy,1);
		}
	}
	
	public void initMartinetto() {
		int Xm = GetXsize()/2, Ym=GetYsize()/2;
		assert(XDM.length==YDM.length);
		for (int i=0; i <XDM.length; i++) {
			InitStateXY(XDM[i]+Xm, YDM[i]+Ym,1);
		}
	}

	public FLPanel GetSpecificPanel() {
		 FLPanel p = FLPanel.NewPanel(m_Npoints, GetRandomizer());
		 p.setOpaque(false);
		 return p;
	}  
	
	/** setting the number of 1s **/
	public void SetNPoints(int val){
		m_Npoints.SetVal(val);
	}
	
	/** use only after linking operation ! 
	 * rounding operation **/ 
	public void SetByDensity(double dens){
		int points= (int) (dens * GetLsize());
		//Macro.Debug("npoints to set : " + points + " real dens: " + ((double)points/ GetLsize()) );
		SetNPoints(points);
	}
		
}
