package initializers.CAinit;

import components.allCells.SuperCell;
import components.arrays.RegularDynArray;
import components.arrays.SuperSystem;
import components.types.FLsignal;
import initializers.SuperInitializer;
import main.Macro;
import topology.basics.SuperTopology;

/*--------------------
 * Used for the initialisation of an automaton 
 * transmits the sig_Init to the linked automaton
 * @author : Nazim Fates
 *--------------------*/
abstract public class ArrayInitializer extends SuperInitializer {	

	//INHERITED abstract protected void SubInit(); 			// main init function
	//INHERITED abstract public FLPanel GetSpecificPanel(); // user control

	/*--------------------
	 * attributes
	 --------------------*/

	// used to send the init signal (clear) before 
	// running the SubInit() procedure
	private RegularDynArray m_dynArray; 

	// is the initializer already linked ?
	private boolean linked = false;


	/*--------------------
	 * construction
	 --------------------*/

	public ArrayInitializer() {
		super();
	}


	/*--------------------
	 * main
	 --------------------*/

	/** call this method before using the initializer */
	@Override
	public void LinkTo(SuperSystem system, SuperTopology topo){
		//Macro.Debug("Main link to has been called");
		if (system == null){
			Macro.SystemWarning("the automaton given has not been initialized");
		}
		linked= true;
		m_dynArray= (RegularDynArray)system;
	}

	@Override
	protected void PreInit(){
		Macro.print(5,"initializer init");
		try{
			m_dynArray.ReceiveSignal(FLsignal.init);
		} catch(NullPointerException e ){
			if (!linked){
				Macro.FatalError(e, "Link this intializer before using it !");
			}
			Macro.FatalError(e, "!Error in initializer with linked status :" + linked);			
		}
	}


	final public int GetSize() {
		return m_dynArray.GetSize();
	}

	protected SuperCell GetCell(int pos) {
		return m_dynArray.GetCell(pos);
	}

	protected int RandomPos() {
		return RandomInt(GetSize());
	}
	
		
	
}
