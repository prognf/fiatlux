package initializers.CAinit;

import components.allCells.TwoRegisterCell;
import components.arrays.SuperSystem;
import components.types.IntC;
import components.types.IntegerList;
import topology.basics.PlanarTopology;
import topology.basics.SuperTopology;

abstract public class TwoRegisterPlaneInitializer extends ArrayInitializer {

    private IntC m_XYsize;

    /** implementation : casting involved */
    @Override
    public void LinkTo(SuperSystem system, final SuperTopology topo) {
    	super.LinkTo(system, topo);
        PlanarTopology topo2D = (PlanarTopology) topo;
        m_XYsize = topo2D.GetXYsize();
    }

    protected TwoRegisterCell GetCell(int pos) {
        return (TwoRegisterCell)super.GetCell(pos);
    }

    public int GetCellStateOne(final int cell) {
        return GetCell(cell).GetStateOne();
    }

    public int GetCellStateTwo(final int cell) {
        return GetCell(cell).GetStateTwo();
    }

    public int GetXsize() {
        return m_XYsize.X();
    }

    public int GetYsize() {
        return m_XYsize.Y();
    }
    
    public void SetCellStateOne(final int pos, final int state) {
        GetCell(pos).SetStateOne(state);
    }

    public void SetCellStateOneXY(final int x, final int y, final int state) {
    	int pos= m_XYsize.Map2Dto1D(x, y);
        SetCellStateOne(pos, state);
    }

    public void SetCellStateTwo(final int cell, final int state) {
        GetCell(cell).SetStateTwo(state);
    }

    public void SetCellStateTwoXY(final int x, final int y, final int state) {
    	int pos= m_XYsize.Map2Dto1D(x, y);
        SetCellStateTwo(pos, state);
    }

    /** dirty **/
	protected IntegerList RandomKpositions(int k) {
		return RandomKpositions(k, GetSize());
	}
	

}
