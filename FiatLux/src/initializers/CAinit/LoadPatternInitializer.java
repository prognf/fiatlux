package initializers.CAinit; 

///import CellularAutomata.*;


/************************************
 * loading pattern from file
 * @author Nazim Fates
 *******************************************/ 
public class LoadPatternInitializer extends  OneRegisterInitializer {
	final static String SOURCEFILE= "init.txt";
	
	public void SubInit(){
		int X= GetXsize(), Y= GetYsize();
		for (int y=0; y< Y; y++){
			for (int x=0; x< X; x++){
				boolean condA = (x-y)%X==0;
				boolean condB = (x+y)%X==0;
				int state=(condA||condB)?1:0;
				InitStateXY(x, y, state%2);
			}
		}
		
	}

	/*public FLPanel GetSpecificPanel() {
		 //FLPanel p = FLPanel.NewPanel(m_Npoints, GetRandomizer()) ;
		 return p;
	} */
}
