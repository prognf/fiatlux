package initializers.CAinit;

import architecture.interactingParticleSystems.InteractingParticleSystemOneDSampler.MODELTYPE;
import main.Macro;


/*--------------------
 * initialises arrays with two states
 * @author Nazim Fates
 *--------------------*/
public class InitializerEvenOdd extends OneRegisterInitializer {

	/*----------------------------------------------------------------------------
	  - attributes
	  --------------------------------------------------------------------------*/

	
	final MODELTYPE m_modeltype;
	
	/*---------------------------------------------------------------------------
	  - construction
	  --------------------------------------------------------------------------*/
	public InitializerEvenOdd(MODELTYPE modeltype){
		super();
		m_modeltype= modeltype;
	}

	/*---------------------------------------------------------------------------
	  - actions
	  --------------------------------------------------------------------------*/

	/** MAIN : called by sig_Init
	 * random init with state values in {0, StateRange - 1} FillRate determines
	 * how many cells are not in state 0
	 */

	protected void  SubInit() {
		//evenInitWithFlip();
		initPattern();
	} 


	/** Bernoulli 
	 * for the even model we forece an even initial configuration **/
	protected void evenInitWithFlip() {
		int N= GetLsize();
		int [] array = new int [N];
		int count=0;
		for (int i=0 ; i < N ; i++) {
			array[i]= this.RandomInt(2); //uniform
			count+= array[i];
		}
		int flip=0;
		if (m_modeltype==MODELTYPE.even) {// possibility of flipping the whole configuration
			flip= (count%2); 	
		}
		for (int i=0 ; i < N ; i++) { // flip or no flip
			InitState(i, (array[i]+flip)%2 );
		}
		Macro.Debug("init even odd done");
	}
	
	/** Bernoulli 
	 * for the even model we forece an even initial configuration **/
	int [] PTRN = {1,1,1,0,0,0,1,1,1,0,0};
	protected void initPattern() {
		int N= GetLsize();
		for (int i=0 ; i < N ; i++) {
			int q= PTRN[i%PTRN.length];
			InitState(i, q);
		}
	}
	
}