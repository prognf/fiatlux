package initializers.CAinit;

import static grafix.windows.SimulationWindowAbstract.COL_INITAREA;

import javax.swing.JComponent;

import experiment.samplers.CAsimulationSampler;
import experiment.samplers.SuperSampler;
import grafix.gfxTypes.IconManager;
import grafix.gfxTypes.elements.FLBlockPanel;
import grafix.gfxTypes.elements.FLButton;
import grafix.gfxTypes.elements.FLPanel;
import main.FiatLuxProperties;

/** Graphical panel of the initializer **/
public class InitPanel extends FLBlockPanel {


	SuperSampler m_sampler;
	private FLButton m_title;


	public InitPanel(CAsimulationSampler sampler) {
		this(sampler, sampler.GetInitializer().GetSpecificPanel());
	}

	public InitPanel(SuperSampler sampler, JComponent InitSpecificPanel) {
		m_sampler = sampler;
		m_title = new FLButton("Initializer");
		m_title.setIcon(IconManager.getUIIcon("init2"));

		defineAsWindowBlockPanel(COL_INITAREA, m_title, 220);
		AddGridBagButton(m_title);

		FLPanel sub = new FLPanel();
		sub.setOpaque(false);
		sub.SetGridBagLayout();
		sub.GetGridBagC().insets.set(10, 5, 5, 5);
		sub.GetGridBagC().weightx = 0.2;


		if (InitSpecificPanel != null) {
			sub.AddGridBag(InitSpecificPanel, 0, 10);
		}

		AddGridBagY(sub);
		// special for CA sampler

		if (m_sampler.GetSamplerSpecificInitPanel() != null) {
			sub.AddGridBag(m_sampler.GetSamplerSpecificInitPanel(), 0, 20);
		}

		//if (m_sampler != null && m_sampler instanceof CAsampler) {


		//ClearButton clr = new ClearButton();
		//FLPanel seedC = ((CAsampler)m_sampler).GetInitializer().GetRandomizer().GetSeedControl();
		//ImportInitStateButton initStateButton = new ImportInitStateButton();
		//InitFromStringButton initFromStringButton = new InitFromStringButton();


		setTabId(FiatLuxProperties.SIDEBAR_TABS.INITIALIZER);

		adaptSize(sub);
	}
}





