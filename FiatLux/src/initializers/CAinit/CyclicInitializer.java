package initializers.CAinit;

import grafix.gfxTypes.IntField;
import grafix.gfxTypes.elements.FLPanel;

/*--------------------
 * The Cyclic model is another toy model
*--------------------*/

public class CyclicInitializer extends OneRegisterInitializer {
	IntField mF_CyclicNum = new IntField("Cyclic departures:", 3, 1);

	/*--------------------
	 * abstract methods definition
	 --------------------*/
	public FLPanel GetSpecificPanel() {
		FLPanel p = new FLPanel();
		p.add(mF_CyclicNum);
		p.setOpaque(false);
		return p;
	}

	/*--------------------
	 * Other methods
	 --------------------*/
	public CyclicInitializer() {
		super();
	}

	protected void SubInit() {
		int size = GetLsize();
		for (int pos = 0; pos < size; pos++) {
			int state = RandomInt(models.CAmodels.nState.CyclicModel.NUMSTATE);
			InitState(pos, state);
		}
	}
}