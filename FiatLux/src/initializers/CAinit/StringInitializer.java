package initializers.CAinit;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import components.arrays.CharArray;
import initializers.CAinit.InitLanguageTools.Analyse;

/*--------------------
 * initialises arrays from a string
 * @author Oceane Chaze
 *--------------------*/

public class StringInitializer extends OneRegisterInitializer {

    private ArrayList<Character> m_code;
    private CharArray result;
    private String m_errrorMessage;


    public StringInitializer(String array, int dim, int width, int height, int nbStates) throws IOException {
        //Temp file to store the string
        File file = new File("file.tmp");
        FileWriter writer = new FileWriter(file);
        writer.write(array);
        writer.close();

        m_code = new ArrayList<>();
        for(char c :"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray()) {
            m_code.add(c);
        }

        Analyse analyse = new Analyse(file, dim, width, height, nbStates);
        result = analyse.getResult();
        m_errrorMessage = analyse.getErrorMessage();

    }

    public StringInitializer(String path, int width, int height, int nbStates){
        m_code = new ArrayList<>();
        for(char c :"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray()) {
            m_code.add(c);
        }
        String extension = "";
        int i = path.lastIndexOf('.');
        if (i >= 0) { extension = path.substring(i+1); }
        result = new CharArray(path, width, height, nbStates, extension.equals("rle"));

        //m_errrorMessage = "";
    }

    @Override
    protected void SubInit() {
        int pos = 0;
        for(char c : result){

            int index = m_code.indexOf(c);
            if(index!=-1){
                InitState(pos,index);
            }
            else{
                InitState(pos,m_code.size());
                m_code.add(c);
            }
            pos++;
        }

    }

    public String getErrorMessage(){
        return m_errrorMessage;

    }

}
