package initializers.CAinit;

import components.arrays.SuperSystem;
import components.types.IntC;
import components.types.IntegerList;
import initializers.SuperInitializer;
import main.Macro;
import topology.basics.PlanarTopology;
import topology.basics.SuperTopology;

/*--------------------
 * A SimpleInitDevice controls OneRegister arrays 
 * @author : Nazim Fates
*--------------------*/

public abstract class OneRegisterInitializer extends SuperInitializer {


	// main linked objet 
	private OneRegisterSettableSystem m_system;
	
	
	/*--------------------
	 * Attributes
	 --------------------*/
	
	// used as a feature to do 2D mapping
	private IntC m_XYsize;

	/*--------------------
	 * 
	 --------------------*/

	public void LinkTo(SuperSystem system, SuperTopology topo){
		Macro.Debug("Linking initializer to system of type :" + system.getClass().getCanonicalName());
		m_system = (OneRegisterSettableSystem)system;
		if (system == null){
			Macro.FatalError("System given not initialized");
		}
		// TODO : what is this ???
		if ((topo!= null) && (topo.GetDimension()==2)) {
				 	m_XYsize= ((PlanarTopology)topo).GetXYsize();
		}
	}
	
	/*--------------------
	 * main method
	 --------------------*/

	/** direct setting of a cell state */
	protected void InitState(int pos, int state) {
		m_system.setState(pos, state);
	}

	/*--------------------
	 * protected methods
	 --------------------*/

	final protected int GetLsize() {
		return m_system.getSize();
	}
	
	final protected int GetSize() {
		return m_system.getSize();
	}


	/** direct setting of a cell state */
	protected void InitStateKRandom(int k, int state) {
		IntegerList posList= super.RandomKpositions(k,GetLsize());
		for (int pos : posList){
			InitState(pos, state);
		}
	}
	
	@Override
	final protected void PreInit() {
		// do nthing by default
	}

	/*--------------------
	 * Other methods
	 --------------------*/

	protected void ClearArray(){
		for (int pos=0; pos > GetLsize(); pos++){
			InitState(pos, 0);
		}
	}

	/*--------------------
	 * Get / Set
	 --------------------*/
	
	/** 2D specific **/ 
	public int GetXsize() {
		return m_XYsize.X();
	}
	
	/** 2D specific **/ 
	public int GetYsize() {
		return m_XYsize.Y();
	}
	
	/** 2D specific **/ 
	protected void InitStateXY(int x, int y, int in_state) {
		//wrapping
		int x2= Math.floorMod(x,GetXsize()), y2= Math.floorMod(y, GetYsize()); 
		int pos = m_XYsize.Map2Dto1D(x2, y2);
		InitState(pos, in_state);
	}
	
	/** 2D specific **/ 
	protected void InitStateXY(IntC posXY, int in_state) {
		int pos = m_XYsize.Map2Dto1D(posXY);
		InitState(pos, in_state);
	}
	
	/** 2D specific **/ 
	protected void CenterPointInit(int state) {
		int xPos = GetXsize()/2;
		int yPos = GetYsize()/2;
		//SetAutomatonStateXY(xPos, yPos, m_model.GetMValue());
		InitStateXY(xPos, yPos, state);
	}

	/** dirty **/
	protected IntegerList RandomKpositions(int k) {
		return RandomKpositions(k, GetSize());
	}
	
	protected int RandomPos() {
		return RandomInt(GetSize());
	}

	
	
}// end class

