package initializers.CAinit;

import components.types.IntegerList;
import grafix.gfxTypes.IntField;
import grafix.gfxTypes.elements.FLPanel;

/*--------------------
 * initializes n particules in a field
 * used by RandomWalk Model  
 --------------------*/
public class ReceptorInitializer extends OneRegisterInitializer {
	
	final static String NPARTICLES = "# of particles";
	final static int NDEFAULT= 10; 
	
	/*--------------------
	 * Attributes
	 --------------------*/
	IntField mf_Nparticules= new IntField(NPARTICLES, 2, NDEFAULT);
	
	protected void SubInit() {
		int k = mf_Nparticules.GetValue(), n = GetLsize();
		IntegerList posPart= IntegerList.getKamongN(k, n, this.GetRandomizer());
		for (int pos : posPart){
			InitState(pos, 1);			
		}
	}

	public FLPanel GetSpecificPanel() {
		return mf_Nparticules;
	}
	
}
