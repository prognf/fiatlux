grammar InitGrammar;

options{
    language = Java;
    output = AST;
    k=1;
    backtrack = false;

}
tokens{
    ROOT;
    Func;
    Arg;
}

root
    : program -> ^(ROOT program);


program
    : elem (( '&'^ | '|'^ | '+'^ ) program)?
    | '('! program ')'! (( '&'^ | '|'^ | '+'^ ) program)?
    ;



elem
    : ID '(' list ')' -> ^(ID list)
    ;


 list
    : arg (','! list)?
    |
    ;



arg
    : ID ('(' list ')' -> ^(ID list) )?
    | INT
    ;





ID  : ('a'..'z' | 'A'..'Z')('a'..'z' | 'A'..'Z' | '0'..'9')* ;
INT : ('0'..'9'|'.')+ ;