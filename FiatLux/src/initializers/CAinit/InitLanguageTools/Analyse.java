package initializers.CAinit.InitLanguageTools;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.antlr.runtime.ANTLRInputStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.Tree;

import components.arrays.CharArray;
import initializers.CAinit.BinaryInitializer;
import main.Macro;

/*--------------------
 * Create the tree corresponding to the string given by StringInitializer
 * and analyse this tree to create the CharArray config corresponding to the string
 * @author Océane Chazé
 *--------------------*/

public class Analyse {

    private static Tree m_tree;
    private static int m_dim, m_width, m_height, m_nbStates;
    private static String m_errorMessage="";

    public Analyse(File file, int dim, int width, int height, int nbStates) {
        InitGrammarParser parser = null;
        try {
            parser = createParser(file);
            m_tree = getTree(parser);
        } catch (IOException | RecognitionException e) {
            e.printStackTrace();
        }
        m_dim = dim;
        m_width = width;
        m_height = height;
        if(m_dim==1){
            m_height = 1;
        }
        m_nbStates = nbStates;
    }

    /** translate the entire tree contained in the class to its associated CharArray **/
    public static CharArray getResult(){
        m_errorMessage = "";
        return translate(m_tree);
    }

    /** Create the parser related to the file content **/
    private InitGrammarParser createParser(File file) throws IOException {
        FileInputStream inputStream = new FileInputStream(file);
        ANTLRInputStream input = new ANTLRInputStream(inputStream);
        InitGrammarLexer lexer = new InitGrammarLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        inputStream.close();
        return new InitGrammarParser(tokens);
    }

    /** Create the tree related to the parser **/
    private Tree getTree(InitGrammarParser parser) throws RecognitionException {
        Tree tree = (Tree) parser.root().getTree();
        return tree;
    }

    /** Print the content of t in terminal **/
    private static void printTree(Tree t){
        Macro.print(4,t.toString());
        for(int i=0; i<t.getChildCount();i++){
            printTree(t.getChild(i));
        }

    }

    /** translate a node to a charArray **/
    private static CharArray translate(Tree node){
        //System.out.println("Translate "+node.toString());
        CharArray result = new CharArray(m_width,m_height,m_nbStates);
        switch (node.toString()){
            case "ROOT" :
                result = translate(node.getChild(0));
                break;
            case "+":
                result = translate(node.getChild(0));
                result.Xor(translate(node.getChild(1)));
                break;
            case "&":
                result = translate(node.getChild(0));
                result.And(translate(node.getChild(1)));
                break;
            case "|":
                result = translate(node.getChild(0));
                result.Or(translate(node.getChild(1)));
                break;
            default ://Fonction
                result = functionAssignation(node);
                break;
        }
        return result;

    }

    /** translate a function node (name of the function) to its corresponding CharArray **/
    private static CharArray functionAssignation(Tree node){
        CharArray result = new CharArray(m_width, m_height, m_nbStates);
        switch (node.toString()){
            case "Bernoulli":

                if(node.getChildCount()==1){
                    double arg0 = Double.parseDouble(node.getChild(0).toString());
                    result.Bernoulli(arg0,'1','0');
                }
                else if(node.getChildCount()==2){
                    double arg0 = Double.parseDouble(node.getChild(0).toString());
                    result.Bernoulli(arg0,node.getChild(1).toString().charAt(0),'0');
                }
                else if(node.getChildCount()==3){
                    double arg0 = Double.parseDouble(node.getChild(0).toString());
                    result.Bernoulli(arg0,node.getChild(1).toString().charAt(0),node.getChild(2).toString().charAt(0));
                }
                else{

                }
                break;
            case "Rectangle":
                if(node.getChildCount()!=5 && node.getChildCount()!=4){
                   warningArg(node.toString(),5);
                }
                else if (node.getChildCount()==4){
                    int arg0, arg1, arg2, arg3;
                    arg0 = Integer.parseInt(node.getChild(0).toString());
                    arg1 = Integer.parseInt(node.getChild(1).toString());
                    arg2 = Integer.parseInt(node.getChild(2).toString());
                    arg3 = Integer.parseInt(node.getChild(3).toString());
                    result.Rectangle(arg0,arg1,arg2,arg3,'1');

                }
                else {
                    int arg0, arg1, arg2, arg3;
                    arg0 = evalInt(node.getChild(0));
                    arg1 = evalInt(node.getChild(1));
                    arg2 = evalInt(node.getChild(2));
                    arg3 = evalInt(node.getChild(3));
                    result.Rectangle(arg0,arg1,arg2,arg3,node.getChild(4).toString().charAt(0));
                }
                break;
            case "CenterRectangle":
                if(node.getChildCount()==2){
                    int arg0, arg1;
                    arg0 = Integer.parseInt(node.getChild(0).toString());
                    arg1 = Integer.parseInt(node.getChild(1).toString());
                    result.CenterRectangle(arg0,arg1,'1');

                }
                else if (node.getChildCount()==3){
                    int arg0, arg1;
                    arg0 = Integer.parseInt(node.getChild(0).toString());
                    arg1 = Integer.parseInt(node.getChild(1).toString());
                    result.CenterRectangle(arg0,arg1,node.getChild(2).toString().charAt(0));

                }
                else {
                    warningArg(node.toString(),5);
                }
                break;
            case "Line":
                if(node.getChildCount()==2){
                    int arg0 = evalInt(node.getChild(1));
                    result.Line(arg0, node.getChild(1).toString().charAt(0));
                }
                else if(node.getChildCount()==1){
                    int arg0 = evalInt(node.getChild(1));
                    result.Line(arg0, '1');
                }
                else{
                    warningArg(node.toString(),2);
                }
                break;
            case "CenterLine":
                if(node.getChildCount()==0){
                    int arg0 = evalInt(node.getChild(1));
                    result.CenterLine('1');
                }
                else if(node.getChildCount()==1){
                    result.CenterLine(node.getChild(1).toString().charAt(0));
                }
                else{
                    warningArg(node.toString(),2);
                }
                break;
            case "Column":
                if(node.getChildCount()==2){
                    int arg0 = evalInt(node.getChild(1));
                    result.Column(arg0,node.getChild(1).toString().charAt(0));
                }
                else if (node.getChildCount()==1){
                    int arg0 = evalInt(node.getChild(1));
                    result.Column(arg0,'1');
                }
                else{
                    warningArg(node.toString(),2);
                }
                break;
            case "CenterColumn":
                if(node.getChildCount()==0){
                    result.CenterColumn('1');
                }
                else if (node.getChildCount()==1){
                    result.CenterColumn(node.getChild(1).toString().charAt(0));
                }
                else{
                    warningArg(node.toString(),2);
                }
                break;

            case "Frame":
                if(node.getChildCount()==5){
                    int arg0 = evalInt(node.getChild(0));
                    int arg1 = evalInt(node.getChild(1));
                    int arg2 = evalInt(node.getChild(2));
                    int arg3 = evalInt(node.getChild(3));
                    result.Frame(arg0, arg1,arg2, arg3, node.getChild(4).toString().charAt(0));
                }
                else if (node.getChildCount()==4){
                    int arg0 = evalInt(node.getChild(0));
                    int arg1 = evalInt(node.getChild(1));
                    int arg2 = evalInt(node.getChild(2));
                    int arg3 = evalInt(node.getChild(3));
                    result.Frame(arg0, arg1,arg2, arg3, '1');
                }
                else{
                    warningArg(node.toString(),5);
                }
                break;
            case "CenterFrame":
                if(node.getChildCount()==3){
                    int arg0 = evalInt(node.getChild(0));
                    int arg1 = evalInt(node.getChild(1));
                    result.CenterFrame(arg0, arg1,node.getChild(2).toString().charAt(0));
                }
                else if (node.getChildCount()==2){
                    int arg0 = evalInt(node.getChild(0));
                    int arg1 = evalInt(node.getChild(1));
                    result.CenterFrame(arg0, arg1, '1');
                }
                else{
                    warningArg(node.toString(),5);
                }
                break;
            case "SmallCheck":
                if(node.getChildCount()==5){
                    int arg0 = evalInt(node.getChild(0));
                    int arg1 = evalInt(node.getChild(1));
                    int arg2 = evalInt(node.getChild(2));
                    int arg3 = evalInt(node.getChild(3));
                    result.SmallCheck(arg0, arg1,arg2, arg3, node.getChild(4).toString().charAt(0));
                }
                else if (node.getChildCount()==6){
                    int arg0 = evalInt(node.getChild(0));
                    int arg1 = evalInt(node.getChild(1));
                    int arg2 = evalInt(node.getChild(2));
                    int arg3 = evalInt(node.getChild(3));
                    result.SmallCheck(arg0, arg1, arg2, arg3, node.getChild(4).toString().charAt(0) ,node.getChild(5).toString().charAt(0));
            }
                else if (node.getChildCount()==4){
                    int arg0 = evalInt(node.getChild(0));
                    int arg1 = evalInt(node.getChild(1));
                    int arg2 = evalInt(node.getChild(2));
                    int arg3 = evalInt(node.getChild(3));
                    result.SmallCheck(arg0, arg1, arg2, arg3, '1' ,'0');
                }
                else{
                    warningArg(node.toString(),5);
                }
                break;
            case "CenterCheck":
                if(node.getChildCount()==4){
                    int arg0 = evalInt(node.getChild(0));
                    int arg1 = evalInt(node.getChild(1));
                    result.CenterCheck(arg0, arg1,node.getChild(2).toString().charAt(0),node.getChild(3).toString().charAt(0));
                }
                if(node.getChildCount()==3){
                    int arg0 = evalInt(node.getChild(0));
                    int arg1 = evalInt(node.getChild(1));
                    result.CenterCheck(arg0, arg1, node.getChild(2).toString().charAt(0),'0');
                }
                else if (node.getChildCount()==2){
                    int arg0 = evalInt(node.getChild(0));
                    int arg1 = evalInt(node.getChild(1));
                    result.CenterCheck(arg0, arg1, '1' ,'0');
                }
                else{
                    warningArg(node.toString(),5);
                }
                break;
            case "Check":
                if(node.getChildCount()==1){
                    result.Check(node.getChild(0).toString().charAt(0));
                }
                else if(node.getChildCount()==2){
                    result.Check(node.getChild(0).toString().charAt(0),node.getChild(1).toString().charAt(0));
                }
                else{
                    warningArg(node.toString(),1);
                }
                break;
            case "Set":
                if(node.getChildCount()!=3){
                    warningArg(node.toString(),3);
                }
                else{
                    int arg0 = evalInt(node.getChild(0));
                    int arg1 = evalInt(node.getChild(1));
                    result.Set(arg0, arg1, node.getChild(2).toString().charAt(0));
                }
                break;

            default:
                Macro.SystemWarning("Unknown Function : "+node.toString());
                m_errorMessage = m_errorMessage+"<p  style=\"color: red;\">Unknown Function <b>"+node.toString()+"</b></p>\n";

        }
        return result;
    }


    //Transform a node that must be a double (expected argument of function) into a double, either directly or in evaluating if it's a function (ex : random)
    public static int evalInt(Tree node){
        int result=0;
        if(node.getChildCount() == 0){
            result = Integer.parseInt((node).toString());
        }
        else{
            if(node.toString().equals("Rand")){
                BinaryInitializer bin = new BinaryInitializer();
                result = bin.RandomInt(Integer.parseInt(node.getChild(0).toString()));
            }
            else{
                Macro.SystemWarning("Unknown internal Function : "+node.toString());
            }
        }
        return result;
    }

    /** create the warning message when a function don't have the right number of arguments **/
    public static void warningArg(String func, int[] nbArg){
        m_errorMessage += " <p  style=\"color: red;\">Error : incorrect number of arguments for function "+func+". Expected : <b></p>";
        for(int i : nbArg) {
            m_errorMessage += i + " ; ";
        }
        m_errorMessage += "</b>\n";

    }
    public static void warningArg(String func, int nbArg){
        Macro.SystemWarning("Error : incorrect number of arguments for function "+func+". Expected : "+ nbArg+ " ;");
        m_errorMessage += "<p  style=\"color: red;\"><b>Error</b> : incorrect number of arguments for function <b>"+func+"</b>. Expected : <b>"+ nbArg+ "</b> ;</p>\n";
    }

    public String getErrorMessage(){
        return m_errorMessage;
    }

}
