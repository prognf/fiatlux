// $ANTLR 3.5.2 InitGrammar.g 2020-07-20 10:42:00
package initializers.CAinit.InitLanguageTools;

import org.antlr.runtime.BitSet;
import org.antlr.runtime.NoViableAltException;
import org.antlr.runtime.Parser;
import org.antlr.runtime.ParserRuleReturnScope;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.RecognizerSharedState;
import org.antlr.runtime.Token;
import org.antlr.runtime.TokenStream;
import org.antlr.runtime.tree.CommonTreeAdaptor;
import org.antlr.runtime.tree.RewriteRuleSubtreeStream;
import org.antlr.runtime.tree.RewriteRuleTokenStream;
import org.antlr.runtime.tree.TreeAdaptor;

@SuppressWarnings("all")
public class InitGrammarParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "Arg", "Func", "ID", "INT", "ROOT", 
		"'&'", "'('", "')'", "'+'", "','", "'|'"
	};
	public static final int EOF=-1;
	public static final int T__9=9;
	public static final int T__10=10;
	public static final int T__11=11;
	public static final int T__12=12;
	public static final int T__13=13;
	public static final int T__14=14;
	public static final int Arg=4;
	public static final int Func=5;
	public static final int ID=6;
	public static final int INT=7;
	public static final int ROOT=8;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public InitGrammarParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public InitGrammarParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	protected TreeAdaptor adaptor = new CommonTreeAdaptor();

	public void setTreeAdaptor(TreeAdaptor adaptor) {
		this.adaptor = adaptor;
	}
	public TreeAdaptor getTreeAdaptor() {
		return adaptor;
	}
	@Override public String[] getTokenNames() { return InitGrammarParser.tokenNames; }
	@Override public String getGrammarFileName() { return "InitGrammar.g"; }


	public static class root_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "root"
	// InitGrammar.g:16:1: root : program -> ^( ROOT program ) ;
	public final InitGrammarParser.root_return root() throws RecognitionException {
		InitGrammarParser.root_return retval = new InitGrammarParser.root_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope program1 =null;

		RewriteRuleSubtreeStream stream_program=new RewriteRuleSubtreeStream(adaptor,"rule program");

		try {
			// InitGrammar.g:17:5: ( program -> ^( ROOT program ) )
			// InitGrammar.g:17:7: program
			{
			pushFollow(FOLLOW_program_in_root88);
			program1=program();
			state._fsp--;

			stream_program.add(program1.getTree());
			// AST REWRITE
			// elements: program
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 17:15: -> ^( ROOT program )
			{
				// InitGrammar.g:17:18: ^( ROOT program )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(ROOT, "ROOT"), root_1);
				adaptor.addChild(root_1, stream_program.nextTree());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "root"


	public static class program_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "program"
	// InitGrammar.g:20:1: program : ( elem ( ( '&' ^| '|' ^| '+' ^) program )? | '(' ! program ')' ! ( ( '&' ^| '|' ^| '+' ^) program )? );
	public final InitGrammarParser.program_return program() throws RecognitionException {
		InitGrammarParser.program_return retval = new InitGrammarParser.program_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal3=null;
		Token char_literal4=null;
		Token char_literal5=null;
		Token char_literal7=null;
		Token char_literal9=null;
		Token char_literal10=null;
		Token char_literal11=null;
		Token char_literal12=null;
		ParserRuleReturnScope elem2 =null;
		ParserRuleReturnScope program6 =null;
		ParserRuleReturnScope program8 =null;
		ParserRuleReturnScope program13 =null;

		Object char_literal3_tree=null;
		Object char_literal4_tree=null;
		Object char_literal5_tree=null;
		Object char_literal7_tree=null;
		Object char_literal9_tree=null;
		Object char_literal10_tree=null;
		Object char_literal11_tree=null;
		Object char_literal12_tree=null;

		try {
			// InitGrammar.g:21:5: ( elem ( ( '&' ^| '|' ^| '+' ^) program )? | '(' ! program ')' ! ( ( '&' ^| '|' ^| '+' ^) program )? )
			int alt5=2;
			int LA5_0 = input.LA(1);
			if ( (LA5_0==ID) ) {
				alt5=1;
			}
			else if ( (LA5_0==10) ) {
				alt5=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 5, 0, input);
				throw nvae;
			}

			switch (alt5) {
				case 1 :
					// InitGrammar.g:21:7: elem ( ( '&' ^| '|' ^| '+' ^) program )?
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_elem_in_program109);
					elem2=elem();
					state._fsp--;

					adaptor.addChild(root_0, elem2.getTree());

					// InitGrammar.g:21:12: ( ( '&' ^| '|' ^| '+' ^) program )?
					int alt2=2;
					int LA2_0 = input.LA(1);
					if ( (LA2_0==9||LA2_0==12||LA2_0==14) ) {
						alt2=1;
					}
					switch (alt2) {
						case 1 :
							// InitGrammar.g:21:13: ( '&' ^| '|' ^| '+' ^) program
							{
							// InitGrammar.g:21:13: ( '&' ^| '|' ^| '+' ^)
							int alt1=3;
							switch ( input.LA(1) ) {
							case 9:
								{
								alt1=1;
								}
								break;
							case 14:
								{
								alt1=2;
								}
								break;
							case 12:
								{
								alt1=3;
								}
								break;
							default:
								NoViableAltException nvae =
									new NoViableAltException("", 1, 0, input);
								throw nvae;
							}
							switch (alt1) {
								case 1 :
									// InitGrammar.g:21:15: '&' ^
									{
									char_literal3=(Token)match(input,9,FOLLOW_9_in_program114); 
									char_literal3_tree = (Object)adaptor.create(char_literal3);
									root_0 = (Object)adaptor.becomeRoot(char_literal3_tree, root_0);

									}
									break;
								case 2 :
									// InitGrammar.g:21:22: '|' ^
									{
									char_literal4=(Token)match(input,14,FOLLOW_14_in_program119); 
									char_literal4_tree = (Object)adaptor.create(char_literal4);
									root_0 = (Object)adaptor.becomeRoot(char_literal4_tree, root_0);

									}
									break;
								case 3 :
									// InitGrammar.g:21:29: '+' ^
									{
									char_literal5=(Token)match(input,12,FOLLOW_12_in_program124); 
									char_literal5_tree = (Object)adaptor.create(char_literal5);
									root_0 = (Object)adaptor.becomeRoot(char_literal5_tree, root_0);

									}
									break;

							}

							pushFollow(FOLLOW_program_in_program129);
							program6=program();
							state._fsp--;

							adaptor.addChild(root_0, program6.getTree());

							}
							break;

					}

					}
					break;
				case 2 :
					// InitGrammar.g:22:7: '(' ! program ')' ! ( ( '&' ^| '|' ^| '+' ^) program )?
					{
					root_0 = (Object)adaptor.nil();


					char_literal7=(Token)match(input,10,FOLLOW_10_in_program139); 
					pushFollow(FOLLOW_program_in_program142);
					program8=program();
					state._fsp--;

					adaptor.addChild(root_0, program8.getTree());

					char_literal9=(Token)match(input,11,FOLLOW_11_in_program144); 
					// InitGrammar.g:22:25: ( ( '&' ^| '|' ^| '+' ^) program )?
					int alt4=2;
					int LA4_0 = input.LA(1);
					if ( (LA4_0==9||LA4_0==12||LA4_0==14) ) {
						alt4=1;
					}
					switch (alt4) {
						case 1 :
							// InitGrammar.g:22:26: ( '&' ^| '|' ^| '+' ^) program
							{
							// InitGrammar.g:22:26: ( '&' ^| '|' ^| '+' ^)
							int alt3=3;
							switch ( input.LA(1) ) {
							case 9:
								{
								alt3=1;
								}
								break;
							case 14:
								{
								alt3=2;
								}
								break;
							case 12:
								{
								alt3=3;
								}
								break;
							default:
								NoViableAltException nvae =
									new NoViableAltException("", 3, 0, input);
								throw nvae;
							}
							switch (alt3) {
								case 1 :
									// InitGrammar.g:22:28: '&' ^
									{
									char_literal10=(Token)match(input,9,FOLLOW_9_in_program150); 
									char_literal10_tree = (Object)adaptor.create(char_literal10);
									root_0 = (Object)adaptor.becomeRoot(char_literal10_tree, root_0);

									}
									break;
								case 2 :
									// InitGrammar.g:22:35: '|' ^
									{
									char_literal11=(Token)match(input,14,FOLLOW_14_in_program155); 
									char_literal11_tree = (Object)adaptor.create(char_literal11);
									root_0 = (Object)adaptor.becomeRoot(char_literal11_tree, root_0);

									}
									break;
								case 3 :
									// InitGrammar.g:22:42: '+' ^
									{
									char_literal12=(Token)match(input,12,FOLLOW_12_in_program160); 
									char_literal12_tree = (Object)adaptor.create(char_literal12);
									root_0 = (Object)adaptor.becomeRoot(char_literal12_tree, root_0);

									}
									break;

							}

							pushFollow(FOLLOW_program_in_program165);
							program13=program();
							state._fsp--;

							adaptor.addChild(root_0, program13.getTree());

							}
							break;

					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "program"


	public static class elem_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "elem"
	// InitGrammar.g:27:1: elem : ID '(' list ')' -> ^( ID list ) ;
	public final InitGrammarParser.elem_return elem() throws RecognitionException {
		InitGrammarParser.elem_return retval = new InitGrammarParser.elem_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token ID14=null;
		Token char_literal15=null;
		Token char_literal17=null;
		ParserRuleReturnScope list16 =null;

		Object ID14_tree=null;
		Object char_literal15_tree=null;
		Object char_literal17_tree=null;
		RewriteRuleTokenStream stream_11=new RewriteRuleTokenStream(adaptor,"token 11");
		RewriteRuleTokenStream stream_ID=new RewriteRuleTokenStream(adaptor,"token ID");
		RewriteRuleTokenStream stream_10=new RewriteRuleTokenStream(adaptor,"token 10");
		RewriteRuleSubtreeStream stream_list=new RewriteRuleSubtreeStream(adaptor,"rule list");

		try {
			// InitGrammar.g:28:5: ( ID '(' list ')' -> ^( ID list ) )
			// InitGrammar.g:28:7: ID '(' list ')'
			{
			ID14=(Token)match(input,ID,FOLLOW_ID_in_elem186);  
			stream_ID.add(ID14);

			char_literal15=(Token)match(input,10,FOLLOW_10_in_elem188);  
			stream_10.add(char_literal15);

			pushFollow(FOLLOW_list_in_elem190);
			list16=list();
			state._fsp--;

			stream_list.add(list16.getTree());
			char_literal17=(Token)match(input,11,FOLLOW_11_in_elem192);  
			stream_11.add(char_literal17);

			// AST REWRITE
			// elements: ID, list
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 28:23: -> ^( ID list )
			{
				// InitGrammar.g:28:26: ^( ID list )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot(stream_ID.nextNode(), root_1);
				adaptor.addChild(root_1, stream_list.nextTree());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "elem"


	public static class list_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "list"
	// InitGrammar.g:32:2: list : ( arg ( ',' ! list )? |);
	public final InitGrammarParser.list_return list() throws RecognitionException {
		InitGrammarParser.list_return retval = new InitGrammarParser.list_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal19=null;
		ParserRuleReturnScope arg18 =null;
		ParserRuleReturnScope list20 =null;

		Object char_literal19_tree=null;

		try {
			// InitGrammar.g:33:5: ( arg ( ',' ! list )? |)
			int alt7=2;
			int LA7_0 = input.LA(1);
			if ( ((LA7_0 >= ID && LA7_0 <= INT)) ) {
				alt7=1;
			}
			else if ( (LA7_0==11) ) {
				alt7=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 7, 0, input);
				throw nvae;
			}

			switch (alt7) {
				case 1 :
					// InitGrammar.g:33:7: arg ( ',' ! list )?
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_arg_in_list221);
					arg18=arg();
					state._fsp--;

					adaptor.addChild(root_0, arg18.getTree());

					// InitGrammar.g:33:11: ( ',' ! list )?
					int alt6=2;
					int LA6_0 = input.LA(1);
					if ( (LA6_0==13) ) {
						alt6=1;
					}
					switch (alt6) {
						case 1 :
							// InitGrammar.g:33:12: ',' ! list
							{
							char_literal19=(Token)match(input,13,FOLLOW_13_in_list224); 
							pushFollow(FOLLOW_list_in_list227);
							list20=list();
							state._fsp--;

							adaptor.addChild(root_0, list20.getTree());

							}
							break;

					}

					}
					break;
				case 2 :
					// InitGrammar.g:35:5: 
					{
					root_0 = (Object)adaptor.nil();


					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "list"


	public static class arg_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "arg"
	// InitGrammar.g:39:1: arg : ( ID ( '(' list ')' -> ^( ID list ) )? | INT );
	public final InitGrammarParser.arg_return arg() throws RecognitionException {
		InitGrammarParser.arg_return retval = new InitGrammarParser.arg_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token ID21=null;
		Token char_literal22=null;
		Token char_literal24=null;
		Token INT25=null;
		ParserRuleReturnScope list23 =null;

		Object ID21_tree=null;
		Object char_literal22_tree=null;
		Object char_literal24_tree=null;
		Object INT25_tree=null;
		RewriteRuleTokenStream stream_11=new RewriteRuleTokenStream(adaptor,"token 11");
		RewriteRuleTokenStream stream_ID=new RewriteRuleTokenStream(adaptor,"token ID");
		RewriteRuleTokenStream stream_10=new RewriteRuleTokenStream(adaptor,"token 10");
		RewriteRuleSubtreeStream stream_list=new RewriteRuleSubtreeStream(adaptor,"rule list");

		try {
			// InitGrammar.g:40:5: ( ID ( '(' list ')' -> ^( ID list ) )? | INT )
			int alt9=2;
			int LA9_0 = input.LA(1);
			if ( (LA9_0==ID) ) {
				alt9=1;
			}
			else if ( (LA9_0==INT) ) {
				alt9=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 9, 0, input);
				throw nvae;
			}

			switch (alt9) {
				case 1 :
					// InitGrammar.g:40:7: ID ( '(' list ')' -> ^( ID list ) )?
					{
					ID21=(Token)match(input,ID,FOLLOW_ID_in_arg255);  
					stream_ID.add(ID21);

					// InitGrammar.g:40:10: ( '(' list ')' -> ^( ID list ) )?
					int alt8=2;
					int LA8_0 = input.LA(1);
					if ( (LA8_0==10) ) {
						alt8=1;
					}
					switch (alt8) {
						case 1 :
							// InitGrammar.g:40:11: '(' list ')'
							{
							char_literal22=(Token)match(input,10,FOLLOW_10_in_arg258);  
							stream_10.add(char_literal22);

							pushFollow(FOLLOW_list_in_arg260);
							list23=list();
							state._fsp--;

							stream_list.add(list23.getTree());
							char_literal24=(Token)match(input,11,FOLLOW_11_in_arg262);  
							stream_11.add(char_literal24);

							// AST REWRITE
							// elements: ID, list
							// token labels: 
							// rule labels: retval
							// token list labels: 
							// rule list labels: 
							// wildcard labels: 
							retval.tree = root_0;
							RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

							root_0 = (Object)adaptor.nil();
							// 40:24: -> ^( ID list )
							{
								// InitGrammar.g:40:27: ^( ID list )
								{
								Object root_1 = (Object)adaptor.nil();
								root_1 = (Object)adaptor.becomeRoot(stream_ID.nextNode(), root_1);
								adaptor.addChild(root_1, stream_list.nextTree());
								adaptor.addChild(root_0, root_1);
								}

							}


							retval.tree = root_0;

							}
							break;

					}

					}
					break;
				case 2 :
					// InitGrammar.g:41:7: INT
					{
					root_0 = (Object)adaptor.nil();


					INT25=(Token)match(input,INT,FOLLOW_INT_in_arg281); 
					INT25_tree = (Object)adaptor.create(INT25);
					adaptor.addChild(root_0, INT25_tree);

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "arg"

	// Delegated rules



	public static final BitSet FOLLOW_program_in_root88 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_elem_in_program109 = new BitSet(new long[]{0x0000000000005202L});
	public static final BitSet FOLLOW_9_in_program114 = new BitSet(new long[]{0x0000000000000440L});
	public static final BitSet FOLLOW_14_in_program119 = new BitSet(new long[]{0x0000000000000440L});
	public static final BitSet FOLLOW_12_in_program124 = new BitSet(new long[]{0x0000000000000440L});
	public static final BitSet FOLLOW_program_in_program129 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_10_in_program139 = new BitSet(new long[]{0x0000000000000440L});
	public static final BitSet FOLLOW_program_in_program142 = new BitSet(new long[]{0x0000000000000800L});
	public static final BitSet FOLLOW_11_in_program144 = new BitSet(new long[]{0x0000000000005202L});
	public static final BitSet FOLLOW_9_in_program150 = new BitSet(new long[]{0x0000000000000440L});
	public static final BitSet FOLLOW_14_in_program155 = new BitSet(new long[]{0x0000000000000440L});
	public static final BitSet FOLLOW_12_in_program160 = new BitSet(new long[]{0x0000000000000440L});
	public static final BitSet FOLLOW_program_in_program165 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_elem186 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_10_in_elem188 = new BitSet(new long[]{0x00000000000008C0L});
	public static final BitSet FOLLOW_list_in_elem190 = new BitSet(new long[]{0x0000000000000800L});
	public static final BitSet FOLLOW_11_in_elem192 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_arg_in_list221 = new BitSet(new long[]{0x0000000000002002L});
	public static final BitSet FOLLOW_13_in_list224 = new BitSet(new long[]{0x00000000000000C0L});
	public static final BitSet FOLLOW_list_in_list227 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_arg255 = new BitSet(new long[]{0x0000000000000402L});
	public static final BitSet FOLLOW_10_in_arg258 = new BitSet(new long[]{0x00000000000008C0L});
	public static final BitSet FOLLOW_list_in_arg260 = new BitSet(new long[]{0x0000000000000800L});
	public static final BitSet FOLLOW_11_in_arg262 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_INT_in_arg281 = new BitSet(new long[]{0x0000000000000002L});
}
