package initializers.CAinit;

import components.types.IntC;
import main.MathMacro;
import topology.basics.PlanarTopology;
import topology.basics.SuperTopology;
/*--------------------
 * Linked to a planar topology
  * @author : Nazim Fates
*--------------------*/
public class zzzPlaneInitHelper  {
	
	private PlanarTopology m_topo;

	public zzzPlaneInitHelper(SuperTopology in_topo){
		m_topo= (PlanarTopology)in_topo;
	}
	
	public int GetXsize(){
		return m_topo.GetXsize();
	}
	
	public int GetYsize(){
		return m_topo.GetYsize();
	}
	
	/* mapping */
	final public int GetPosXY(IntC xy){
		return GetPosXY(xy.X(), xy.Y());
	} 
	
	/* mapping 2D -> 1D*/
	public int GetPosXY(int x, int y){		
		int pos= MathMacro.Map2Dto1D(x, y, GetXsize(), GetYsize());
		return pos;
		//super.InitAutomatonState(pos,state);
	}
	

	
}
