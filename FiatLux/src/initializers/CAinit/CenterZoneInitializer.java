package initializers.CAinit;

import java.util.ArrayList;

import components.types.IntC;

/*--------------------
 * a OneZoneInitializer is a particular kind of Initializer 
 * sets zones 
 * --------------------*/
public class CenterZoneInitializer extends OneRegisterInitializer {
	ArrayList<IntC> m_zoneList;	

	public CenterZoneInitializer() {
		resetZones();
	}

	public void SubInit() {
		ClearArray();
		for (IntC zone : m_zoneList){
			int pos=zone.X(), len = zone.Y();
			for (int i = pos; i < pos + len; i++) {
				InitState(i, 1);			
			}
		}
	}
	
	/** for reinitialization **/
	public void resetZones() {
		m_zoneList= new ArrayList<IntC>();
	}

	/** main external call **/
	public void AddZone(int pos, int len) {
		m_zoneList.add(new IntC(pos,len));
	}
	
	public void SetZoneCentered(int len) {
		int pos = (GetLsize() - len)/2;
		AddZone(pos,len);
	}

	public void SetZoneLengthHalf() {
		SetZoneCentered(GetLsize()/2);		
	}
}
