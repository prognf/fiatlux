package initializers.MASinit;

import architecture.multiAgent.tools.MultiAgentSystem;
import architecture.multiAgent.tools.SituatedAgent;
import components.arrays.SuperSystem;
import grafix.gfxTypes.elements.FLPanel;
import initializers.SuperInitializer;
import topology.basics.SuperTopology;


abstract public class MultiAgentInitializer extends SuperInitializer {
	
	abstract public FLPanel GetSpecificPanel();

//	final static String CLASSNAME= Thread.currentThread().getStackTrace()[1].getClassName();

	@Override
	public void LinkTo(SuperSystem system, SuperTopology topologyManager) {
	//not used
	}
	
	/*--------------------
	 * attributes
	 --------------------*/
	private MultiAgentSystem m_system;

	/*--------------------
	 * construction
	 --------------------*/
	public MultiAgentInitializer(MultiAgentSystem in_System) {
		super();
		m_system= in_System;
	}

	/*--------------------
	 * actions
	 --------------------*/

	public void PreInit() {
		// TODO : needed or not ? m_system.sig_Init();	
	}

	/*--------------------
	 * creations
	 --------------------*/

	protected void CreateAgents(int n_agents) {
		SituatedAgent [] listagent= new SituatedAgent[n_agents];
		
		for (int i=0; i< n_agents; i++){
			// TODO labels management
			listagent[i]= m_system.GetNewAgent(i);			
		}
		m_system.SetAgentList(listagent);
		m_system.ResetAgentList();		 
	}

	/*--------------------
	 * get & set
	 --------------------*/

	/** for use in sub-classes */
	final protected MultiAgentSystem Getsystem(){
		return m_system;
	}

	/** square absorbing border */
	protected void MakeBorders(int state) {
		int Xmax = GetXsize() -1, Ymax = GetYsize() -1 ;
		for (int x=0; x <= Xmax ; x++){
			SetEnvCell(x, 0, state);
			SetEnvCell(x, Ymax, state);
		}
		for (int y=0; y <= Ymax ; y++){
			SetEnvCell(0, y, state);
			SetEnvCell(Xmax, y, state);
		}		
	}


	public void SetEnvCell(int x, int y, int newstate){
		m_system.GetCellXY(x,y).SetState(newstate);	
	}


	final protected int GetXsize() {
		return m_system.GetXsize();
	}

	final protected int GetYsize() {
		return m_system.GetYsize();
	}


}
