package architecture.dynamicalSystems;

import java.util.ArrayList;

import components.randomNumbers.StochasticSource;
import components.types.FLStringList;
import components.types.FLsignal;
import components.types.FLtimer;
import components.types.Monitor;
import main.Macro;



/*-------------------------------------------------------------------------------
- 
- @author Nazim Fates
------------------------------------------------------------------------------*/
public class FragmentationSystem extends StochasticSource
implements Monitor {

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();
	private static final int Nsteps = 8000000;
	private static final boolean PRN = false;
	private static final boolean DECORRELATION = false;
	//private static final int SIZE = 16;/*, INITLEN=8;*/

	//--------------------------------------------------------------------------
	//- attributes
	//--------------------------------------------------------------------------

	int m_N; // size
	FLtimer m_time= new FLtimer();
	// collection of fragments
	ArrayList<Fragment> m_aquarium, m_birthList, m_deathList;
	private int m_cellUpdated= -1;
	public int m_Strobo=1; 

	//--------------------------------------------------------------------------
	//- construction
	//--------------------------------------------------------------------------

	public FragmentationSystem(int Nsize){
		m_N= Nsize;
		//this.SetSeed(280676);
	}

	//--------------------------------------------------------------------------
	//- signals
	//--------------------------------------------------------------------------

	@Override
	public void ReceiveSignal(FLsignal sig) {
		m_time.ReceiveSignal(sig);
		switch(sig){
		case init:
			ResetAquarium(); 
			ResetBirthAndDeathLists();
			break;
		case nextStep:
			m_cellUpdated= RandomInt(m_N);
			NextStep(m_cellUpdated);
			break;
		case update:
			break;
		}
		if ((m_time.GetTime() % m_Strobo ==0) && (PRN)){
			PrintState();
		}

	}

	/** UPDATING RULE **/
	private void UpdateFragment(Fragment f, int cellUpdated) {
		int h= f.GetHead(), t= f.GetTail();
		if (f.m_size==m_N){ // size N
			BreakIn(f, cellUpdated);
		} else if (cellUpdated == ((h - 1 + m_N) % m_N) ){ // grow left
			f.GrowLeft();
		} else if (cellUpdated == ((t+1)%m_N)){ // grow right
			f.GrowRight();
		} else if (cellUpdated == t){ // on tail 
			//Macro.Debug(">on tail>> h:" + h + " t:"+t);
			if (h==t){ // killing size 1
				Remove(f);
			} else { // regular shrink
				f.ShrinkRight();	
			}
		} else { // 
			int delta= (cellUpdated>h)?(cellUpdated-h):(cellUpdated+m_N-h);
			if ((delta>=1) && (delta<=f.m_size-2)){ // break in two pieces
				BreakInTwoAtDelta(f,delta);
			}
		}
	}


	private void ResetAquarium() {
		m_aquarium= new ArrayList<FragmentationSystem.Fragment>();
		m_aquarium.add(new Fragment(this,0,m_N));
	}

	private void ResetBirthAndDeathLists() {
		m_birthList= new ArrayList<FragmentationSystem.Fragment>();		
		m_deathList= new ArrayList<FragmentationSystem.Fragment>();		
	}

	private void NextStep(int cellUpdated) {
		for (Fragment f : m_aquarium){
			UpdateFragment(f,cellUpdated);
		}
		m_aquarium.removeAll(m_deathList);
		//m_aquarium.addAll(m_birthList);
		for (Fragment f : m_birthList){ // avoiding copies 
			if (!m_aquarium.contains(f)){
				m_aquarium.add(f);
			}
		}

		ResetBirthAndDeathLists();
	}

	private void BreakIn(Fragment f, int cellUpdated) {
		f.m_head= (cellUpdated+1)%m_N;
		f.m_size--;
	}

	private void BreakInTwoAtDelta(Fragment f, int delta) {
		int sizeA= delta, sizeB= f.m_size -1 - delta;
		int hA, hB;
		if (DECORRELATION){
			hA= RandomInt(m_N); hB= RandomInt(m_N);
		} else {
			hA= f.m_head; hB= (hA + delta +1 )%m_N;
		}
		m_birthList.add(new Fragment(this, hA, sizeA));
		m_birthList.add(new Fragment(this, hB, sizeB));
		Remove(f);		
	}

	private void Remove(Fragment f) {
		m_deathList.add(f);
	}

	//--------------------------------------------------------------------------
	//- I/O
	//--------------------------------------------------------------------------

	public int OneExp(){
		boolean loop=true;
		ReceiveSignal(FLsignal.init);
		while (loop){
			ReceiveSignal(FLsignal.nextStep);
			loop= (!IsEmpty()) && (m_time.GetTime() < Nsteps);
		}
		return m_time.GetTime();
	}



	public void PrintState(){
		FLStringList s= new FLStringList();
		String infoLine= UpdatingLine(m_cellUpdated) + " t: "+m_time + " upd: " + m_cellUpdated;  
		s.add( infoLine );
		for (Fragment f : m_aquarium){
			String line= "";
			line += f.toString();
			line += String.format(" [%d,%d](%d) ", f.GetHead(), f.GetTail(), f.m_size);
			s.Add( line );
		}
		//Macro.CR();
		s.PrintRaw();

	}

	private String UpdatingLine(int previouslyUpdated) {
		char [] str= new char[m_N];
		for (int i=0; i<m_N; i++){
			str[i]=i==previouslyUpdated?'V':'-';
		}
		return String.valueOf(str);
	}

	private boolean IsEmpty() {
		return m_aquarium.size()==0;
	}

	//--------------------------------------------------------------------------
	//- inner class
	//--------------------------------------------------------------------------


	class Fragment {
		FragmentationSystem m_sys;
		private int m_head, m_size;

		public Fragment(FragmentationSystem sys, int beg, int size) {
			m_sys= sys;
			m_head= beg;
			m_size= size;
		}

		public int GetHead() {
			return m_head;
		}
		public int GetTail() {
			return (m_head + m_size - 1) % m_sys.m_N;
		}

		public void GrowLeft() {
			m_head=(m_head==0)?m_N:m_head-1;
			m_size++;
		}

		public void GrowRight() {
			m_size++;
		}

		public void ShrinkRight() {
			m_size--;
		}

		public String toString(){
			StringBuilder str= new StringBuilder();
			for (int i=0; i< m_N; i++){ 
				boolean inside = ((i+m_N-m_head)%m_N) < m_size;
				str.append(inside ? "X" : ".");
			}
			return str.toString();
		}

		@Override
		public int hashCode(){
			return m_size * m_N + m_head;
		}

		@Override
		public boolean equals(Object toCompare){
			return 
					(toCompare != null) && 
					(toCompare instanceof Fragment) && 
					(toCompare.hashCode() == hashCode());
		}

	}


	//--------------------------------------------------------------------------
	//- test
	//--------------------------------------------------------------------------

	/*public static void DoTest() {
		Macro.BeginTest(CLASSNAME);
		//		FragmentationSystem sys = new FragmentationSystem(SIZE);

		Macro.EndTest();
	}

	static public void main(String [] arg){
		DoTest();
	}*/

}
