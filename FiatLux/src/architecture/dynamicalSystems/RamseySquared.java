package architecture.dynamicalSystems;

import java.util.ArrayList;

import components.arrays.GridSystem;
import components.types.FLsignal;
import components.types.IntC;
import components.types.Triplet;
import main.Macro;
import main.MathMacro;

/*-------------------------------------------------------------------------------
- 
------------------------------------------------------------------------------*/

public class RamseySquared extends GridSystem {

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();

	private static final int L = 10;

	private static final IntC XY = new IntC(L,L);

	//--------------------------------------------------------------------------
	//- attributes
	//--------------------------------------------------------------------------

	int [][] m_data;
	int m_X, m_Y;

	ArrayList<Triplet> m_nonvalid;

	//--------------------------------------------------------------------------
	//- construction
	//--------------------------------------------------------------------------

	public RamseySquared(IntC XYsize) {
		super(XYsize);
		m_X= XYsize.X();
		m_Y= XYsize.Y();
		m_data= new int[m_X][m_Y];
	}

	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------

	void initRandom() {
		for (int y=0; y<m_Y;y++) {
			for (int x=0; x < m_X; x++) {
				m_data[x][y]= RandomInt(2);
			}
		}
	}


	boolean checkValid() {
		m_nonvalid= new ArrayList<>();//cleaning
		int L= MathMacro.Min(m_X, m_Y);
		boolean isClean=true;
		for (int l=1; l<L; l++) { 
			for (int y=0; y<m_Y-l; y++) {
				for (int x=0; x<m_X-l; x++) {
					if (isSquare(x,y,l)) {
						//Macro.fPrint("square at: (%d,%d)-(%d,%d)",x,y,x+l,y+l);
						m_nonvalid.add(new Triplet(x,y,l));
						isClean=false;
					}
				}
			}
		}//for
		if (isClean) {
			Macro.print("*** square is clean ***");
		} else {
			Macro.print(" nsq : " + m_nonvalid.size());
		}
		return isClean;
	}

	boolean isSquare(int x, int y, int l) {
		return ( eqTwo(x,y,x+l,y) && eqTwo(x+l,y,x+l,y+l) && eqTwo(x,y,x,y+l));
	}


	private boolean eqTwo(int x, int y, int x2, int y2) {
		return (m_data[x][y] == m_data[x2][y2]);
	}

	void selectOneAtRandomAndChange(){
		Triplet tsel= MathMacro.SelectOneItemAtRandom(m_nonvalid,this);
		Macro.fPrint(" selected t: %s", tsel);
		int dx= RandomTrueFalse()?tsel.z:0;  // selecting the "corner" to change
		int dy= RandomTrueFalse()?tsel.z:0;
		int xc= tsel.x + dx;
		int yc= tsel.y + dy;
		m_data[xc][yc]= 1 - m_data[xc][yc];
		//return tsel;
	}


	//--------------------------------------------------------------------------
	//- I/O
	//--------------------------------------------------------------------------

	void printSquare() {
		Macro.SeparationLine();
		for (int y=m_Y-1; y>=0; y--) {
			StringBuilder ln= new StringBuilder();
			for (int x=0; x<m_X; x++) {
				ln.append(m_data[x][y]);
			}
			Macro.print(ln.toString());
		}
		Macro.SeparationLine();
	}

	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------

	public void ReceiveSignal(FLsignal sig){
		switch(sig){
		case init:
			sig_Init();
			break;
		case update:
			sig_Update();
			break;
		case nextStep:
			sig_NextStep();
			break;
		}
	}

	public void sig_Init() {
		super.sig_Init();
		initRandom();
		
	}

	public void sig_NextStep() {
		TimeClick();
		//printSquare();
		selectOneAtRandomAndChange();
	}

	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------
	public static void main(String[] argv) {
		//CommandParser parser = new CommandParser(CLASSNAME, "", argv);
		//	int exp= parser.IParse("EXP=");
		RamseySquared exp= new RamseySquared(XY);
		exp.sig_Init();
		boolean isValid = exp.checkValid();
		while (!isValid) {
			exp.sig_NextStep();
			isValid = exp.checkValid();
		}
		Macro.SeparationLine();
		Macro.SeparationLine();
		
		exp.printSquare();
	}

}

