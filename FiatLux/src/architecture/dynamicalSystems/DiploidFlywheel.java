package architecture.dynamicalSystems;

import components.arrays.DynSystem;
import components.types.FLsignal;
import main.Macro;

/*
 * implements an "optimized" diploid code...
 */
public class DiploidFlywheel extends DynSystem{


	private static final double LAMBDA = 0.5;
	private static final double DINI = 0.3;
	boolean m_x, m_y;
	boolean [] m_array;
	double m_lambda;

	public DiploidFlywheel(int in_Lsize) {
		super(in_Lsize);
		m_array=new boolean[in_Lsize];
		ResetComponents();
		m_lambda=LAMBDA;
	}


	@Override
	protected void ResetComponents() {
		int L=GetSize();
		for (int pos=0; pos<L;pos++) {
			m_array[pos]= RandomEventDouble(DINI); 
					//RandomTrueFalse();
		}
	}

	/** processing signals **/
	public void ReceiveSignal(FLsignal sig){
		//Macro.Debug("youp !" + sig);
		super.ReceiveSignal(sig); //TODO :seems broken ???!!!
		switch(sig){
		case init:
			break;
		case update:
			break;
		case nextStep:
			oneStep();
			break;
		}
	}


	protected void oneStep() {
		int L= GetSize();
		boolean z;
		//i=0
		m_x= getState(L-2);
		m_y= getState(L-1);
		z= getState(0);
		m_array[0]= stateMachine(z);
		//i=1
		m_x= m_y;
		m_y= z;
		z= getState(1);
		m_array[1]= stateMachine(z);
		// gen. case
		for (int i=2; i< L; i++) {
			m_x=m_y;
			m_y=z;
			z=getState(i);
			m_array[i]= stateMachine(z);
		}

	}

	private boolean stateMachine(boolean z) {
		if (m_x==m_y) { // OO or XX
			return m_x?
					DrawNumber(m_lambda):DrawNumber(1-m_lambda);
		} else {
			if (m_x && (!m_y)) { // XO
				return z?
						true:DrawNumber(1-m_lambda);
			} else {  //OX
				return z?
						DrawNumber(m_lambda):false;
			}
		}
	}

	final private boolean DrawNumber(double proba) {
		return RandomEventDouble(proba);
	}

	private boolean getState(int pos) {
		return m_array[pos%GetSize()];
	}


	public void PrnState() {
		int L=  GetSize(), T= GetTime();
		String s="";
		for (int pos=0; pos < L; pos++) {
			s+=getState(pos+L+T)?'X':'O';
		}
		Macro.fPrint("%5d >> %s", GetTime(), s);
	}
}
