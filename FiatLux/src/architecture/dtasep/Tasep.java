package architecture.dtasep;
import java.util.ArrayList;

import components.types.IntegerList;
import main.ParticulesAgglomeration;

/** by Quentin LADEVEZE */
public class Tasep extends ArrayList<Site> implements ParticulesAgglomeration{


	private Tasep m_otherTasep;
	private String m_name;
	
	/* +1 = right directed
	 * -1 = left directed
	 */
	private int m_sens;


	Tasep(int a, String n,int s){
		int i = 0;
		while (size()<a){
			Site c = new Site(i, this);
			i++;
			add(c);
		}

		m_name = n;
		m_sens = s;
	}

	//initialisation de l'attribut "estDynamique" des cases
	public void upDateDynamicSitesAttribute(){
		for (Site caseCanal : this){
			caseCanal.UpdateDynamicAttribute();
			//Macro.Debug(" case " + caseCanal + " set to: " + caseCanal.IsDynamic());
		}
	}

	public String toString(){
		return toString();
	}

	public void SetOtherCanal(Tasep t){
		m_otherTasep = t;
	}

	

	public Tasep getAutreCanal(){
		return m_otherTasep;
	}

	public String getName(){
		return m_name;
	}

	public int getSens(){
		return m_sens;
	}
	
	public int getPopulation(){
		int res = 0;
		for (Site c  : this){
			if (c.IsOccupied()){
				res++;
			}
		}
		return res;
	}

	public String Display(){
		StringBuilder s = new StringBuilder();
		for (Site c: this){
			if (c.IsOccupied())
				s.append("* ");
			else 
				s.append("_ ");
		}
		return s.toString();
	}
	
	public int GetSize(){
		return this.size();
	}
	
	public int GetState(int pos){
		return this.get(pos).Content();
		 
	}
	
	

	////////////////////////////////////////////////////
	//initialization methods
	////////////////////////////////////////////////////
	
	/** only used below in "InitCellState" **/
	private void SetCellState(int pos, boolean state){
		if (state)
			get(pos).SetOccupied();
		else
			get(pos).SetFree();

	}
	
	/** init of sites neighborhoods relations**/
	public void InitializationNeighborhoodRelationsSites(){
		for (Site c : this){
			c.setPrecedent();
			c.setSuccesseur();
			c.setVoisinVertical();
		}
	}
	
	private void InitCellState(int pos, boolean state){
		SetCellState(pos, state);
		get(pos).InitDynamismAttribute();
	}

	/** only used during init of sites neighborhoods relations **/
	public Site GetNextSite(int n){
		return get((n+1)%size());
	}
	
	public Site GetPrecedentSite(int n){
		return get((n-1+size())%size());
	}
	

	
	public void SetInitialState(IntegerList list) {
		for (int i = 0; i < size() ; i++){
			InitCellState(i, false);
		}
		for (int pos : list){	
			InitCellState(pos, true);
		}
		
	}

}