package architecture.dtasep;

import components.types.DoubleCouple;

public class ParticleBalanceMD extends DTasepMeasuringDevice {

	public static final String NAME = "particleBal";
	public static final DoubleCouple RANGE = new DoubleCouple(-1, 1);
	

	public ParticleBalanceMD (){
		
	}

	
	@Override
	public double GetMeasure() {
		int popTasep1 = m_dtasep.GetPopTasep1();
		int total = m_dtasep.GetPopTotal();
		double res = (2*popTasep1 - total)/ (double) total;
		return 	res;
	}

	@Override
	public String GetName() {
		return NAME;
	}
	
	@Override
	public DoubleCouple GetMinMax() {
		return RANGE;
	}


	@Override
	public String MDFilesuffix() {
		return "Delta";
	}
	
	

}
