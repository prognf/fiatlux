package architecture.dtasep;

import components.arrays.SuperSystem;
import components.types.DoublePar;
import components.types.IntegerList;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.IconManager;
import grafix.gfxTypes.controllers.DoubleController;
import grafix.gfxTypes.elements.FLBlockPanel;
import grafix.gfxTypes.elements.FLButton;
import grafix.gfxTypes.elements.FLPanel;
import initializers.SuperInitializer;
import topology.basics.SuperTopology;

public class DoubleTasepInitializer extends SuperInitializer {
	private static final String TXTPAR1 = "Density 1", TXTPAR2 = "Density 2";
	private static final double DEFVAL1 = 0.4, DEFVAL2 = 0.4;

	private DoubleTasep m_dtasep;
	private DoublePar m_d1, m_d2;

	
	@Override
	public void LinkTo(SuperSystem system, SuperTopology topologyManager) {
	//not used
	}
	
	@Override
	protected void PreInit() {
	}

	@Override
	protected void SubInit() {
		//Macro.Debug("sub init appelé");
		IntegerList list1 = CreationOccupiedSitesList(m_d1.GetVal(), m_dtasep.m_tasep1);
		IntegerList list2 = CreationOccupiedSitesList(m_d2.GetVal(), m_dtasep.m_tasep2);
		m_dtasep.SetInitialState(list1, list2);

	}

	DoubleTasepInitializer(DoubleTasep dtasep){
		m_d1 = new DoublePar(TXTPAR1, DEFVAL1);
		m_d2 = new DoublePar(TXTPAR2, DEFVAL2);
		m_dtasep= dtasep;

	}

	//remplissage des Tasep avec des particules de manière aléatoire
	public IntegerList CreationOccupiedSitesList(double density, Tasep t){
		int size = m_dtasep.GetSize();
		int nbInit = (int) (size*density);
		IntegerList list = IntegerList.getKamongN(nbInit, size, this.GetRandomizer());
		return list;
	}

	public FLPanel GetSimulationPanel() {
		FLBlockPanel p = new FLBlockPanel();
		FLButton t = new FLButton("Initializer");
		t.setIcon(IconManager.getUIIcon("init2"));
		p.defineAsWindowBlockPanel(FLColor.c_lightgreen, t, 220);
		p.AddGridBagButton(t);

		DoubleController d1 = m_d1.GetControl(), d2 = m_d2.GetControl();
		d1.minimize();
		d2.minimize();
		FLPanel q = FLPanel.NewPanelVertical(d1, d2, this.GetRandomizer().GetSeedControl());
		q.setOpaque(false);
		p.AddGridBagY(q);

		return p;
	}

	public void SetDensity1(double initdenst1) {
		m_d1.SetVal(initdenst1);
	}

	public void SetDensity2(double initdenst2) {
		m_d2.SetVal(initdenst2);
	}

	
}
