package architecture.dtasep;
import java.util.ArrayList;

import components.types.FLsignal;
import components.types.Monitor;


public class zzzMeasures implements Monitor{

	Tasep tasep1;
	Tasep tasep2;
	int total;
	int popTasep1;
	double parametre_2;

	zzzMeasures(DoubleTasep t){
		tasep1 = t.m_tasep1;
		tasep2 = t.m_tasep2;
		parametre_2 = -99999;
		popTasep1 = t.m_tasep1.getPopulation();
		total = t.m_tasep1.getPopulation() + t.m_tasep2.getPopulation();
	}

	
	public void UpdateParam1(Site c){
		if (c.GetTasep()==tasep1)
			popTasep1++;
		else popTasep1--;
	}


	public double getParam_1(){ 
		double res = (2*popTasep1 - total)/ (double) total;
		return 	res;
	}

	public double getParam_2(){
		return parametre_2;
	}

	public double calculParametre_2(){
	    double somme = 0;
	       
	        ArrayList<Integer> tailleDesNuages1 = tailleDesNuages(tasep1);
	        ArrayList<Integer> tailleDesNuages2 = tailleDesNuages(tasep2);
	       
	       
	       
	        for (Integer i : tailleDesNuages1)
	            somme += i*i;
	        for (Integer i : tailleDesNuages2)
	            somme += i*i;
	        parametre_2  = Math.sqrt( somme / (double) (tailleDesNuages1.size()+tailleDesNuages2.size() ) );
	       
	       
	        return parametre_2;
	    }
	   
	    public ArrayList<Integer> tailleDesNuages(Tasep t){
	        int size = t.size();
	        int debutComptage = 0;
	        ArrayList<Integer> results = new ArrayList<Integer>();
	        int i = 0;
	        boolean firstIteration = true;
	        int maxSize = 0;
	       
	        while (t.get(i).IsOccupied() && maxSize <= size){
	            i = (i+1) % size;
	            maxSize++;
	        }
	        if (maxSize >= size){
	        	results.add(size);
	        	return results;
	        }
	        	

	        while (!t.get(i).IsOccupied() && maxSize <= size){
	            i = (i+1) % size;
	            maxSize++;
	        }
	        if (maxSize >= size){
	        	return results;
	        }
	        debutComptage = i;
	       
	        while(i != debutComptage || firstIteration) {
	            int heapStart = i;
	            int heapEnd = i;
	           
	            while (t.get(i).IsOccupied()){
	                i = (i+1)%size;
	            }
	            heapEnd = i;
	            results.add((heapEnd - heapStart+size)%size);
	       
	            while (!t.get(i).IsOccupied()){
	                i = (i+1)%size;
	            }
	            firstIteration = false;
	        }
	        return results;   
	    }
/** TODO **/
	@Override
	public void ReceiveSignal(FLsignal sig) {
		// TODO Auto-generated method stub
		
	}



}
