package architecture.dtasep;

import java.util.ArrayList;

import components.types.DoublePar;
import components.types.IntPar;
import grafix.gfxTypes.controllers.DoubleController;
import grafix.gfxTypes.elements.FLPanel;
import main.Macro;
import models.SuperModel;

public class Movement extends SuperModel {
	/* control of movement mode */
	private static final String TXTMVTRULE = "Movement rule :";
	private static final int NORMALMODEL = 0, 
			RDNMODEL = 1,
			SWARMINGMODEL = 2,
			MIXEDMODEL = 3;

	private static final boolean PRINTDYNAMICSITES = false;
	private static final String NAME = "Highway";
	private static final String TXTBETA = "beta";
	private static final double DEFBETA = 0.1;

	/* parameters of jumping probability in swarming mode*/

	private DoubleTasep m_dtasep;
	private ArrayList<Site> m_dynamicSites, m_occupiedSites;
	private DoublePar m_beta;
	private IntPar m_movementRule;
	/* parameters of jumping probability in swarming mode*/
	private double m_sigma;
	private SigmaControl m_sigmaControl;

	Movement(DoubleTasep dtasep) {
		m_dynamicSites = new ArrayList<Site>();
		m_occupiedSites = new ArrayList<Site>();
		m_dtasep = dtasep;
		m_beta = new DoublePar(TXTBETA, DEFBETA);
		m_movementRule = new IntPar(TXTMVTRULE, MIXEDMODEL);
		m_sigmaControl = new SigmaControl();
		m_sigmaControl.SetDouble(0.0);
	}

	/*------------------------
	 *initialization
	 *-------------------------*/
	public void InitializationSitesLists(){
		InitializationDynamicSitesList();
		InitializationOccupiedSitesList();
	}


	public void InitializationDynamicSitesList(){
		m_dynamicSites.clear();
		for(Site c: m_dtasep.m_tasep1){
			if (c.IsDynamic()) {
				m_dynamicSites.add(c);
			}
		}

		for(Site c: m_dtasep.m_tasep2){
			if (c.IsDynamic()) {
				m_dynamicSites.add(c);
			}
		}
	}

	public void InitializationOccupiedSitesList(){
		m_occupiedSites.clear();
		for(Site c: m_dtasep.m_tasep1){
			if (c.IsOccupied()) {
				m_occupiedSites.add(c);
			}
		}

		for(Site c: m_dtasep.m_tasep2){
			if (c.IsOccupied()) {
				m_occupiedSites.add(c);
			}
		}
	}

	/*-------------------
	 * for movement
	 *-------------------*/

	/** main method to call **/
	public void MovementStep(){
		switch (m_movementRule.GetVal()){
		case NORMALMODEL:
			NormalModelMovementStep();
			break;
		case RDNMODEL:
			/* WARNING : only to be used when each site has its vertical neighbour free*/
			RandomBehaviour();
			//SuperRandomWalk();
			break;
		case SWARMINGMODEL:
			SwarmingModelMovementStep();
			break;
		case MIXEDMODEL:
			MixedModelMovementStep();
			break;
		default:
			Macro.FatalError("Unexpected Movement Mode value. Please enter a int between 0 and 3");
		}
	}

	private int MixedModelMovementStep() {
		Site c = SelectMovementSite_MixedMode();

		if (c == null) {
			Macro.print("No possible move");
			return -1;
		}

		if (c.GetVerticalNeighbour().IsOccupied()){
			ForwardMovement(c);
		} else {
			double leapProbability = CalculateLeapProba(c);
			if (RandomEventDouble(leapProbability))
				VerticalLeap(c);
			else ForwardMovement(c);
		}
		return 1;
	}

	private Site SelectMovementSite_MixedMode() {
		return SelectMovementSite_NormalMode();
	}

	private void SwarmingModelMovementStep() {
		//	Macro.Debug("d     " + m_dynamicSites.toString());
		//	Macro.Debug("o     " + m_occupiedSites.toString());

		Site c = SelectMovementSite_SwarmingMode();
		if (c.GetNextSite().IsOccupied()&&c.GetVerticalNeighbour().IsFree()) {
			VerticalLeap(c);
		} else if (c.GetVerticalNeighbour().IsOccupied()&&c.GetNextSite().IsFree()) {
			ForwardMovement(c);
		} else if (c.GetNextSite().IsFree()&&c.GetVerticalNeighbour().IsFree()){
			double leapProbability = CalculateLeapProba(c);
			if (RandomEventDouble(leapProbability)) {
				VerticalLeap(c);
			} else {
				ForwardMovement(c);
			}
		}
	}

	private double CalculateLeapProba(Site c) {
		int localDelta = -1;
		localDelta -= c.GetNextSite().Content();
		localDelta -= c.GetPrecedentSite().Content(); 

		Site VN = c.GetVerticalNeighbour();
		localDelta += VN.Content() + VN.GetNextSite().Content() + VN.GetPrecedentSite().Content();

		//double proba = m_K*Math.exp(m_sigma*localDelta);
		double proba  =  Math.exp(m_sigma*localDelta)/(Math.exp(m_sigma*localDelta)+Math.exp(-m_sigma*localDelta));
		return proba;
	}

	private Site SelectMovementSite_SwarmingMode() {
		int size = m_occupiedSites.size();

		if (size == 0) {
			Macro.print("Doubletasep is empty, can't chose a particle !!");
		}
		int selected= RandomInt(size); 
		return m_occupiedSites.get(selected);
	}

	private int NormalModelMovementStep(){
		Site c = SelectMovementSite_NormalMode();
		if (c == null){
			return -1;
		}

		if (c.GetPrecedentSite().IsOccupied()){
			ForwardMovement(c);
		} else if (c.GetVerticalNeighbour().IsOccupied() ) {
			ForwardMovement(c);
		} else if (RandomEventDouble(m_beta.GetVal())) {
			VerticalLeap(c);
		} else {
			ForwardMovement(c);
		}

		return 1;
	}


	private void SuperRandomWalk() {
		if (RandomInt(2)==0){
			//if (m_dtasep.GetPopTasep1()!=m_dtasep.GetSize())
			m_dtasep.m_popTasep1++;
		} else {
			//if (m_dtasep.GetPopTasep1()!=0)
			m_dtasep.m_popTasep1--;
		}
	}

	/** WARNING : only to be used when each site has its vertical neighbour free*/
	void RandomBehaviour(){
		Site c = RDNSelectMovementSite();
		if (RandomEventDouble(m_beta.GetVal())){
			if (c != null)
				VerticalLeap(c);
		}
	}

	private Site RDNSelectMovementSite() {
		int pos = RandomInt(m_dtasep.GetSize());
		Site 
		c1= m_dtasep.m_tasep1.get(pos), 
		c2= m_dtasep.m_tasep2.get(pos);
		if (c1.IsOccupied())
			return c1;
		else if (c2.IsOccupied())
			return c2;
		else return null;
	}

	/** chose a random dynamic sites among their list **/
	public Site SelectMovementSite_NormalMode(){
		int size = m_dynamicSites.size();
		if (size == 0){
			Macro.print("La situation est bloquée, aucune particule ne peut bouger !!");
			return null;
		}

		int selected= RandomInt(size); 
		return m_dynamicSites.get(selected);
	}

	/** for movement or user-made modifications**/
	private void EmptyingSite(Site c){

		c.SetFree();
		m_occupiedSites.remove(c);
		c.UpdateDynamicAttribute();
		m_dynamicSites.remove(c);

		//his predecessor becomes dynamic 
		Site cc = c.GetPrecedentSite();
		cc.UpdateDynamicAttribute();
		if (cc.IsDynamic()){
			m_dynamicSites.add(cc);
		}
	}

	/** for movement or user-made modifications**/
	public void FillingNewSite(Site c){
		c.SetOccupied();
		m_occupiedSites.add(c);
		c.UpdateDynamicAttribute();
		if (c.IsDynamic()){
			m_dynamicSites.add(c);
		}	

	}

	/** jumping from a tasep to another**/
	private void VerticalLeap(Site c) {

		//ParticleBalance has to be updated
		m_dtasep.UpdatePopTasep(c);

		EmptyingSite(c);
		Site cc = c.GetVerticalNeighbour();
		FillingNewSite(cc);
		cc.GetPrecedentSite().UpdateDynamicAttribute();

		if(cc.GetPrecedentSite().IsNotDynamic()){
			m_dynamicSites.remove(cc.GetPrecedentSite());
			//because this movement could destroy a particle's dynamism
		}
	}

	/** Moving forward.  **/
	private void ForwardMovement(Site c) {
		EmptyingSite(c);
		FillingNewSite(c.GetNextSite());

	}


	/*--------------------
	 * User Interface
	 *--------------------*/


	public void Display(){
		if(PRINTDYNAMICSITES)
			System.out.println("Cases dynamiques : "+ toString());
	}

	public String toString(){
		return m_dynamicSites.toString();
	}

	public String GetName(){
		return NAME;
	}

	public FLPanel GetSimulationPanel() {
		FLPanel p= FLPanel.NewPanel(m_beta.GetControl());
		p.Add(m_movementRule.GetControl());
		p.Add(m_sigmaControl);
		p.add(this.GetRandomizer().GetSeedControl());
		p.setOpaque(false);
		return p;
	}

	public void SetBeta(double beta) {
		m_beta.SetVal(beta);
	}

	public double GetBeta() {
		return m_beta.GetVal();
	}
	
	public void DisplayBeta(){
		String s = ((Double) m_beta.GetVal()).toString();
		Macro.print("Beta value: "+s);
	}

	class SigmaControl extends DoubleController {
		public SigmaControl() {
			super("Sigma");
			//AddSlider(0., 5., 50);
		}

		@Override
		protected double ReadDouble() {
			return m_sigma;
		}

		@Override
		protected void SetDouble(double val) {
			SetSigma(val);
		}

		public void SetSigma(double val){
			m_sigma = val;
		}
	}

	public double GetSigma() {
		return m_sigmaControl.ReadDouble();
	}
	
	public void SetSigma(double sigma) {
		m_sigmaControl.SetDouble(sigma);
	}

	public void SetMovementRule(int i) {
		m_movementRule.SetVal(i);
	}

	public double GetNbDynCells() {
		return m_dynamicSites.size();
	}
}