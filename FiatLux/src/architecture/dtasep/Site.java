package architecture.dtasep;



public class Site {

	private boolean m_isOccupied;
	private final int m_number;
	private boolean m_isDynamic;
	private final Tasep m_tasep;
	private Site m_verticalNeighbour;
	private Site m_nextSite;
	private Site m_precedentSite;


	public Site(int a, Tasep t){
		m_isOccupied =  false;
		m_number = a;
		m_tasep = t;
	}

	public String toString(){

		String s = m_tasep.getName()+" " + Integer.toString(m_number);
		if (m_isOccupied)
			s += "*";
		return s;
	}


	public void SetOccupied(){
		m_isOccupied = true;
	}

	public void SetFree(){
		m_isOccupied = false;
	}

	

	public boolean IsOccupied(){
		return m_isOccupied;
	}
	
	public boolean IsFree(){
		return (!m_isOccupied);
	}
	
	public boolean IsNotDynamic() {
		return !m_isDynamic;
	}

	public boolean IsDynamic() {
		return m_isDynamic;
	}
	
	/** returns 1 is occupied, 0 otherwise */
	public int Content(){
		return m_isOccupied?1:0;
	}

	public Tasep GetTasep(){
		return m_tasep;
	}

	public Site GetNextSite(){
		return m_nextSite;
	}

	public Site GetPrecedentSite(){
		return m_precedentSite;
	}

	public Site GetVerticalNeighbour(){
		return m_verticalNeighbour;
	}

	/** has to be cast every time the site is impacted by a move (his own or not)**/
	public void UpdateDynamicAttribute(){
		if (m_isOccupied && m_nextSite.IsFree())
			m_isDynamic = true;
		
		else 
			m_isDynamic = false;
		
	}

	/////////////////////////////////////////////////////
	// initialization methods
	/////////////////////////////////////////////////////
	
	/** init of relations of cells neighborhood **/
	public void setSuccesseur(){
		if (m_tasep.getSens()==1)
			m_nextSite = m_tasep.GetNextSite(m_number);
		else 
			m_nextSite = m_tasep.GetPrecedentSite(m_number);
	}
	/** init of relations of cells neighborhood **/
	public void setPrecedent(){
		if (m_tasep.getSens()==1)
			m_precedentSite = m_tasep.GetPrecedentSite(m_number);
		else 
			m_precedentSite = m_tasep.GetNextSite(m_number);
		
	}
	/** init of relations of cells neighborhood **/
	public void setVoisinVertical(){
		m_verticalNeighbour = m_tasep.getAutreCanal().get(m_number);
	}


	
	/**Warning : has to be cast for each site before Movement.initializationDynamicSitesList**/
	public void InitDynamismAttribute() {
		m_precedentSite.UpdateDynamicAttribute();
		this.UpdateDynamicAttribute();
		//if (IsDynamic())
		//	Macro.Debug(" case " + toString() + " set to: " + this.IsDynamic());
	}

	

}