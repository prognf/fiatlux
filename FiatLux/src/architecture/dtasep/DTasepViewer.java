package architecture.dtasep;

import components.types.IntC;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.viewers.LineAutomatonViewer;

public class DTasepViewer extends LineAutomatonViewer {

	final static FLColor [] col = {
		FLColor.c_white, FLColor.c_blue2, FLColor.c_green, FLColor.c_red};


	private DoubleTasep m_dtasep;


	protected DTasepViewer(IntC in_CellPixSize, int Tsize, DoubleTasep dtasep) {
		super(in_CellPixSize, dtasep, dtasep.GetSize(), Tsize);
		m_dtasep = dtasep;
		PaintToolKit palette= new PaintToolKit(col);
		this.SetPalette(palette);
	}

	@Override
	public void UpdateBuffer(int[] timeBuffer) {
		//Macro.Debug(" *** SIZE BUFFER :" + timeBuffer.length );
		for (int pos = 0; pos < timeBuffer.length; pos++){
			timeBuffer[pos] = m_dtasep.ConvertToIntArray(pos);
		}

	}

	@Override
	protected void FlipCellState(int x) {
		// TODO Auto-generated method stub

	}


}
