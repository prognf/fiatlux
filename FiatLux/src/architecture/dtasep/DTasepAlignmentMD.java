package architecture.dtasep;

import components.types.DoubleCouple;

public class DTasepAlignmentMD extends DTasepMeasuringDevice {

	public static final String NAME = "Particles Alignement";
	public static final DoubleCouple RANGE = new DoubleCouple(0, 1);

	public DTasepAlignmentMD(){

	}

	@Override
	public DoubleCouple GetMinMax() {
		return RANGE;
	}
	
	@Override
	public double GetMeasure() {
		return Alignment4Sites();
	}
	/** return the alignment : 
	 * for each cell
	 * 		+1 if precedent and next sites are occupied
	 * 		1/2 if one is occupied, one empty
	 *		0 if both are empty
	 */
	public double Alignment2Sites(){
		double result = 0.;
		for (Site i : m_dtasep.m_tasep1){
			if (i.IsOccupied()){
				if (i.GetNextSite().IsOccupied())
					result += 0.5;
				if (i.GetPrecedentSite().IsOccupied())
					result += 0.5;
			}
		}
		for (Site i : m_dtasep.m_tasep2){
			if (i.IsOccupied()){
				if (i.GetNextSite().IsOccupied())
					result += 0.5;
				if (i.GetPrecedentSite().IsOccupied())
					result += 0.5;
			}
		}
		return result / (double) m_dtasep.m_popTotal;
	}
	
	/** return the alignment 4-sites neighborhood **/
	public double Alignment4Sites(){
		double result = 0.;
		for (Site i : m_dtasep.m_tasep1){
			if (i.IsOccupied()){
				if (i.GetNextSite().GetNextSite().IsOccupied())
					result +=0.25;
				if (i.GetNextSite().IsOccupied())
					result += 0.25;
				if (i.GetPrecedentSite().IsOccupied())
					result += 0.25;
				if (i.GetPrecedentSite().GetPrecedentSite().IsOccupied())
					result += 0.25;
			}
		}
		for (Site i : m_dtasep.m_tasep2){
			if (i.IsOccupied()){
				if (i.GetNextSite().GetNextSite().IsOccupied())
					result +=0.25;
				if (i.GetNextSite().IsOccupied())
					result += 0.25;
				if (i.GetPrecedentSite().IsOccupied())
					result += 0.25;
				if (i.GetPrecedentSite().GetPrecedentSite().IsOccupied())
					result += 0.25;
			}
		}
		return result / (double) m_dtasep.m_popTotal;
	}
	
	
	@Override
	public String GetName() {
		return NAME;
	}

	@Override
	public String MDFilesuffix() {
		return "Algnmt";
	}

}
