package architecture.dtasep;

import components.types.IntC;
import components.types.Monitor;
import experiment.samplers.SuperSampler;
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.AutomatonViewer;
import main.Macro;

//TODO : How does this work ?
public class TasepSampler extends SuperSampler {
	private static final String[] PLOTTERSELECTION = 
		{ParticleBalanceMD.NAME,
		DTasepAlignmentMD.NAME,
		DynamicCellsDensityMD.NAME,
	};


	//--------------------------------------------------------------------------
	//- attributes
	//--------------------------------------------------------------------------

	private DoubleTasep m_doubleTasep;
	private ParticleBalanceMD m_particleBalance;
	private DTasepAlignmentMD m_alignment;
	private DynamicCellsDensityMD m_dynamicDens;
	private Movement m_movement;
	private int m_size;
	private DoubleTasepInitializer m_initializer;
	private DTasepViewer m_viewer;



	//--------------------------------------------------------------------------
	//- construction
	//--------------------------------------------------------------------------

	public TasepSampler(int size){
		m_size= size;
		m_doubleTasep = new DoubleTasep(m_size);
		m_movement = new Movement(m_doubleTasep);
		m_doubleTasep.setMovement(m_movement);
		m_initializer = new DoubleTasepInitializer(m_doubleTasep);
		m_particleBalance = new ParticleBalanceMD();
		m_alignment = new DTasepAlignmentMD();
		m_dynamicDens = new DynamicCellsDensityMD();
		m_particleBalance.LinkTo(this);
		m_alignment.LinkTo(this);
		m_dynamicDens.LinkTo(this);

		addMonitor(m_initializer);
		addMonitor(m_movement);

	}
	
	public void AddViewer(IntC cellSize, int Tbuffer){
		m_viewer = new DTasepViewer(cellSize, Tbuffer ,m_doubleTasep);
		addMonitor(m_viewer);
	}

	/** advances one step and updates monitors **/
	/*final public void sig_NextStep(){
		super.sig_NextStep();
		//PrintMD();
	}*/

	
	public void PrintMD(){
		double val = m_particleBalance.GetMeasure();
		Macro.fPrint(" bal: %f  ", val);
	}

	//--------------------------------------------------------------------------
	//- abstract methods definition
	//--------------------------------------------------------------------------

	@Override
	public int GetTime() {
		return m_doubleTasep.GetTime();
	}

	@Override
	protected Monitor GetLinkedSystemAsMonitor() {
		return m_doubleTasep;
	}

	@Override
	protected String GetModelName() {
		return m_movement.GetName();
	}

	@Override
	public String GetSimulationInfo() {
		return " size: " + m_size;
	}

	@Override
	public String GetTopologyInfo() {
		return "TASEP topo";
	}

	@Override
	public FLPanel GetSimulationWidgets() {
		FLPanel p = FLPanel.NewPanel(m_initializer.GetSimulationPanel());
		p.setOpaque(false);
		return p;
	}

	@Override
	public FLPanel GetSimulationRuleControls() {
		FLPanel p= FLPanel.NewPanel();
		p.SetBoxLayoutY();
		// p.Add(m_initializer.GetSimulationPanel());
		p.Add(m_movement.GetSimulationPanel());
		p.setOpaque(false);
		return p;
	}

	@Override
	public FLPanel GetSimulationView() {
		return FLPanel.NewPanel(m_viewer);
	}

	@Override
	public AutomatonViewer GetViewer() {
		return m_viewer;
	}

	@Override
	public String[] GetPlotterSelection() {
		return PLOTTERSELECTION;
	}

	public DoubleTasep GetDoubleTasep() {
		return m_doubleTasep;
	}

	public AutomatonViewer GetVieweInitConditionsr() {
		return m_viewer;
	}

	public Object GetSysSeed() {
		return m_movement.GetSeed();
	}

	public Object GetInitSeed() {
		return m_initializer.GetSeed();
	}

	public void SetSeedInitSys(int seedini, int seedsys) {
		m_initializer.SetSeed(seedini);
		m_movement.SetSeed(seedsys);
	}

	public void SetInitdensBeta(double initdenst1, double initdenst2, double beta) {
		m_movement.SetBeta(beta);
		m_initializer.SetDensity1(initdenst1);
		m_initializer.SetDensity2(initdenst2);
		m_doubleTasep.m_densityTasep1 = initdenst1;
		m_doubleTasep.m_densityTasep2 = initdenst2;
	}
	
	public void SetInitdensSigma(double initdenst1, double initdenst2, double sigma) {
		m_movement.SetSigma(sigma);
		m_initializer.SetDensity1(initdenst1);
		m_initializer.SetDensity2(initdenst2);
		m_doubleTasep.m_densityTasep1 = initdenst1;
		m_doubleTasep.m_densityTasep2 = initdenst2;
	}

	public String GetInitDensTasep1() {
	Double d = m_doubleTasep.m_densityTasep1;
		return d.toString();
	}

	public String GetInitDensTasep2() {
		Double d = m_doubleTasep.m_densityTasep2;
		return d.toString();
	}

	public double GetBeta() {
		return GetDoubleTasep().GetBeta();
	}

	public double GetSigma() {
		return GetMovement().GetSigma();
	}

	private Movement GetMovement() {
		return m_movement;
	}

	public void PrintBeta() {
		m_movement.DisplayBeta();
	}

	public void SetMovementRule(int i) {
		m_movement.SetMovementRule(i);
	}
	


	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------


	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------



}
