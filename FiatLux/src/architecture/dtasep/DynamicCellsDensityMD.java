package architecture.dtasep;

import components.types.DoubleCouple;

public class DynamicCellsDensityMD extends DTasepMeasuringDevice {

	public static final String NAME = "Dynamic Cells Density";
	public static final DoubleCouple RANGE = new DoubleCouple(0, 1);
	
	public DynamicCellsDensityMD(){
		
	}
	
	@Override
	public DoubleCouple GetMinMax() {
		return RANGE;
	}

	@Override
	public double GetMeasure() {
		return DynamicSitesDensity();
	}

	@Override
	public String GetName() {
		return NAME;
	}

	public double DynamicSitesDensity(){
		double result = m_dtasep.m_movement.GetNbDynCells() /((double) m_dtasep.m_popTotal);
		return result;
	}

	@Override
	public String MDFilesuffix() {
		return "DynDens";
	}

	
}
