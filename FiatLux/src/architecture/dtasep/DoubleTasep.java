package architecture.dtasep;

import components.arrays.TimeReadable;
import components.types.FLsignal;
import components.types.IntegerList;
import components.types.Monitor;
import main.Macro;


public class DoubleTasep implements Monitor, TimeReadable {


	Tasep m_tasep1, m_tasep2;
	double m_densityTasep1, m_densityTasep2;
	int m_time;
	Movement m_movement;
	private int m_size;
	
	int m_popTotal;
	int m_popTasep1;

	DoubleTasep(int size){

		m_size = size;
		m_tasep1 = new Tasep(size, "t1",1);
		m_tasep2 = new Tasep(size, "t2",-1);

		m_tasep1.SetOtherCanal(m_tasep2);
		m_tasep2.SetOtherCanal(m_tasep1);

		m_tasep1.InitializationNeighborhoodRelationsSites();
		m_tasep2.InitializationNeighborhoodRelationsSites();


	}

	
	public void sig_Init(){
		//Macro.print("system initialized");
		m_time = 0;
		m_movement.InitializationSitesLists();

	}

	public void sig_NextStep(){
		//m_movement.MovementStep();
		m_movement.MovementStep();
		m_time++;
		
	}

	public void sig_Update(){
		Macro.print("system updated");
		m_movement.InitializationSitesLists();
	}

	public int GetTime() {
		return m_time;
	}

	public void Affichage(){ 
		Macro.PrintEmphasis(" t:" + m_time);
		Macro.print(m_tasep1.Display());
		Macro.print(m_tasep2.Display());
		Macro.CR();
		Macro.fPrint("tasep1/total:  %d/%d"  , m_popTasep1, m_popTotal);
	}

	/** initialize sites contents **/
	public void SetInitialState(IntegerList list1, IntegerList list2){
		m_tasep1.SetInitialState(list1);
		m_tasep2.SetInitialState(list2);
		UpdateOptimizationParameters();
	}
	
	
	public void UpdateOptimizationParameters(){
		m_movement.InitializationSitesLists();
		m_popTasep1 = m_tasep1.getPopulation();
		m_popTotal = m_tasep1.getPopulation()+m_tasep2.getPopulation();
	}

	@Override
	public void ReceiveSignal(FLsignal sig) {
		switch (sig) {
		case init:
			sig_Init();
			break;
		case nextStep: 
			sig_NextStep();
			break;
		case update:
			sig_Update();
			break;
		}
	}

	public void setMovement(Movement movement) {
		m_movement= movement;
	}

	public int GetSize() {
		return m_size;
	}

	/** give the viewer a color code of the doubletasep
	 * 0 = free site
	 * 1 = particle on the upper canal
	 * 2 = particle on the lower canal
	 * 3 = 2 particles
	 * **/
	public int ConvertToIntArray(int pos){
		int code =  
				(m_tasep1.get(pos).IsOccupied()?1:0) + 
				(m_tasep2.get(pos).IsOccupied()?2:0); 
		return code;

	}


	public void UpdatePopTasep(Site c) {
		if (c.GetTasep()==m_tasep1)
			m_popTasep1--;
		else m_popTasep1++;
	}


	public int GetPopTasep1() {
		return m_popTasep1;
	}

	public int GetPopTotal() {
		return m_popTotal;
	}


	public double GetBeta() {
		return m_movement.GetBeta();
	}


	public Tasep GetTasep1() {
		return m_tasep1;
	}


	public Tasep GetTasep2() {
		return m_tasep2;
	}

}
