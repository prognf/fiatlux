package architecture.dtasep;

import experiment.measuring.MeasuringDevice;
import experiment.measuring.general.DecimalParameterMeasurer;
import experiment.samplers.SuperSampler;
import main.Macro;

abstract public class DTasepMeasuringDevice  extends MeasuringDevice 
implements DecimalParameterMeasurer {

	protected DoubleTasep m_dtasep;

	public void LinkTo(SuperSampler in_Sampler){
		Macro.print(5, "Linking sampler to the measuring device:" + GetName());
		TasepSampler t = (TasepSampler) in_Sampler;
		m_dtasep = t.GetDoubleTasep();
	}

	@Override
	abstract public double   GetMeasure();

	@Override
	abstract public String  GetName();
	
	/**for writting files**/
	abstract public String MDFilesuffix();

	
}



