package architecture.dtasep;

import experiment.samplers.SuperSampler;
import grafix.windows.SimulationWindowSingle;

public class TasepWindow {

	public static void main (String[] args){

		OpenTasepWindow();
	
	}

	private static void OpenTasepWindow() {
		SuperSampler sampler = new TasepSampler(20);
		SimulationWindowSingle exp=
				new SimulationWindowSingle("Théo's simulation",sampler);
		exp.sig_Init();
		exp.BuildAndDisplay();
		
	}
	
}
