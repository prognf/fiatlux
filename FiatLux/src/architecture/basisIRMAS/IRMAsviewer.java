package architecture.basisIRMAS;

import components.types.IntC;
import grafix.viewers.GridViewer;


abstract public class IRMAsviewer extends GridViewer 
{


	//--------------------------------------------------------------------------
	//- attributes
	//--------------------------------------------------------------------------

	abstract protected void DrawEnvironmentAgents();
	

	//--------------------------------------------------------------------------
	//- attributes
	//--------------------------------------------------------------------------

	protected IRMAsystem m_IRsystem;

	//--------------------------------------------------------------------------
	//- construction
	//--------------------------------------------------------------------------

	public IRMAsviewer(IRMAsystem in_system, IntC in_PixSize) {
		super(in_PixSize, in_system);

		// attributes init
		m_IRsystem= in_system;

	}

	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------


	/** main component 
	 * X-axis: from left to right
	 * Y-axis :from TOP to BOTTOM !
	 * */
	protected void DrawSystemState() {
		DrawEnvironmentAgents();
	}


	

	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------


	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------


}

