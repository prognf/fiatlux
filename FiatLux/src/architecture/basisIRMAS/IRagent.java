package architecture.basisIRMAS;

import architecture.multiAgent.tools.DIR;
import components.randomNumbers.FLRandomGenerator;
import components.types.IntC;
import components.types.IntCm;



abstract public class IRagent {

	abstract public void sig_PutInfluence();
	abstract public void sig_MakeReaction();
	
	protected final int m_index;				// index of the agent
	protected final CellMoveInfluence m_inflLocal; 	// fixed influence
	protected final CellMoveInfluence m_inflMove;

	protected final IRMAsystem m_system;  	// link to the system
	final protected FLRandomGenerator m_rand;
	
	protected IntCm m_xypos;		// index of the cell in the environment

	protected IntCm m_xytarget; 	// index of the cell where the influence is put
	// updated by the environment !

	protected DIR m_dir;				// direction
	

	public IRagent(IRMAsystem system, int index) {
		m_system= system;
		m_index= index;
		m_rand= m_system.GetRandomizer();
		m_inflLocal= new CellMoveInfluence(m_index);
		m_inflMove= new CellMoveInfluence(m_index);
		
		
		m_xypos= new IntCm(-9999, -9999);
		m_xytarget= new IntCm(-9999, -9999);
		
		SelectRandomDir();
		
		
	}

	protected void SelectRandomDir() {
		m_dir= DIR.RandomDir4(m_rand);		
	}

	public void SetPos(IntC xypos) {
		m_xypos.SetVal(xypos);		
	}
	
	public IntC GetPos(){
		return m_xypos;
	}

	public DIR GetDir() {
		return m_dir;
	}


}
