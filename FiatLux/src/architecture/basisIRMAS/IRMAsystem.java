package architecture.basisIRMAS;

import java.awt.Component;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import components.arrays.GridSystem;
import components.types.FLString;
import components.types.FLsignal;
import components.types.IntC;
import components.types.IntegerList;
import components.types.Monitor;
import grafix.viewers.GridViewer;
import main.Macro;


/*-------------------------------------------------------------------------------
- 
- @author Nazim Fates
- August 2014
------------------------------------------------------------------------------*/

abstract public class IRMAsystem extends GridSystem 
implements Monitor {

	//--------------------------------------------------------------------------
	//- abstractions
	//--------------------------------------------------------------------------

	abstract protected  void CalculateInfluences();
	abstract protected void MakeReaction();


	//--------------------------------------------------------------------------
	//- attributes
	//--------------------------------------------------------------------------

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();


	protected int m_Nagent;							// number of agents
	private ArrayList<IRagent> 		m_agentList;	// list of agents

	protected Map<Integer,ArrayList<Influence>> 	m_inflRegister; 	// mapping : cellpos -> list of influences

	//--------------------------------------------------------------------------
	//- construction
	//--------------------------------------------------------------------------

	private IntC[] m_map2D;

	public IRMAsystem(IntC XYsize, int Nagent) {
		super(XYsize);
		// array
		ResetInfluenceArray();

		// agents
		m_Nagent= Nagent;
		m_agentList= new ArrayList<IRagent>(m_Nagent);
		CreateNewAgents(m_Nagent,m_agentList);

		PlaceAgents(Nagent);
	}

	private void CreateNewAgents(int nagent, ArrayList<IRagent> agentList) {
		for (int index=0; index < nagent; index++){
			// adding new agent
			IRagent agent= GetNewAgent(this,index);
			m_agentList.add(agent);
		}
		
	}
	
	abstract protected IRagent GetNewAgent(IRMAsystem mAsystem, int index);
	
	private void PlaceAgents(int Nagent) {
		IntegerList posList= IntegerList.getKamongN(Nagent, GetNsize(), GetRandomizer());
		for (int index=0; index < Nagent; index++){
			// adding new agent
			int pos= posList.Get(index);
			IntC xypos= m_XYsize.Map1Dto2D(pos);
			GetAgent(index).SetPos(xypos);
		}
	}

	private void PutInfluenceInRegister(Influence infl, int cellIndex) {
		if (m_inflRegister.containsKey(cellIndex)){
			// adding new value in the list associated to the key
			m_inflRegister.get(cellIndex).add(infl);
		} else { 
			// inserting new key
			ArrayList<Influence> lst= new ArrayList<Influence>();
			lst.add(infl);
			m_inflRegister.put(cellIndex,lst);
		}
	}

	//--------------------------------------------------------------------------
	//- signals
	//--------------------------------------------------------------------------

	
	/*---------------------------------------------------------------------------
	  - signals
	  --------------------------------------------------------------------------*/
	@Override
	public void ReceiveSignal(FLsignal sig){
		switch(sig){
		case init:
			sig_Init();
			break;
		case update:
			sig_Update();
			break;
		case nextStep:
			sig_NextStep();
			break;
		}
	}
	
	public void sig_Init() {
		super.sig_Init();
		PlaceAgents(GetAgentNumber());
	}

	public void sig_NextStep() {
		TimeClick();
		CalculateInfluences();
		MakeReaction();
	}


	

	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------

	

	/** indexInfl : integer label of the influence **/ 
	private void PutInfluence(IntC xypos, Influence infl){
		int pos= m_XYsize.Map2Dto1D(xypos);
		PutInfluenceInRegister(infl, pos);
	}

	/** puts local influence in the same place **/
	public void PutLocalInfluence(IRagent agent) {
		Influence infl= agent.m_inflLocal;
		PutInfluence(agent.m_xypos, infl);
	}
	
	/** puts move influence in the same place (no move) **/
	public void PutMoveInfluence(IRagent agent) {
		Influence infl= agent.m_inflMove;
		PutInfluence(agent.m_xypos, infl);
	}

	/** puts an influence in a cell relatively to current pos
	 * could be replaced be pre-calculus **/
	public void PutMoveInfluenceWithOffset(IRagent agent, IntC dxy) {
		// updating m_xytarget directly in the agent !! (dangerous ?)
		agent.m_xytarget= agent.m_xypos.DuplicateM();
		agent.m_xytarget.AddFromPosTorical(dxy, m_XYsize);
		PutInfluence(agent.m_xytarget, agent.m_inflMove);
	}
	
	/** puts an influence in a cell relatively to current pos
	 * could be replaced be pre-calculus **/
	public void PutMoveInfluenceClosedBC(IRagent agent, IntC dxy) {
		// updating m_xytarget directly in the agent !! (dangerous ?)
		agent.m_xytarget.AddFromPosClosedBC(agent.m_xypos, dxy, m_XYsize);
		PutInfluence(agent.m_xytarget, agent.m_inflMove);
	}
	
	//--------------------------------------------------------------------------
	//- small procedures
	//--------------------------------------------------------------------------

	protected final void ResetInfluenceArray() {
		m_inflRegister= new TreeMap<Integer,ArrayList<Influence>>() ;		
	}

	/*
	final private int Map(int targetX, int targetY) {
		return targetX + m_Xsize * targetY;
	}*/


	//--------------------------------------------------------------------------
	//- get / set
	//--------------------------------------------------------------------------


	public int GetAgentNumber() {
		return m_Nagent;
	}


	public IRagent GetAgent(int agentIndex) {
		return m_agentList.get(agentIndex);
	}


	abstract public GridViewer GetViewer(IntC cellSizeForViewer);

	
	

	//--------------------------------------------------------------------------
	//- others
	//--------------------------------------------------------------------------

	private void MakeTest() {
		PutDataTst(1,10);
		PutDataTst(2,20);
		PutDataTst(3,30);
		PutDataTst(4,10);
		PutDataTst(5,30);
		for (Entry<Integer, ArrayList<Influence>> val : m_inflRegister.entrySet()){
			Macro.print(
					" associates : " + val.getKey() 
					+ " ->>>" + FLString.ToStringNoSep(val.getValue()) ) ;
		}
	}

	private void PutDataTst(int pos, int indexAegnt) {
		PutInfluenceInRegister(new CellMoveInfluence(indexAegnt),pos);
		
	}

	final public ArrayList<IRagent> GetAgentList() {
		return m_agentList;
	}
	
	public Component GetSimulationPanel() {
			return null;
	}

	


}
