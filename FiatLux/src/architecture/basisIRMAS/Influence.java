package architecture.basisIRMAS;



abstract public class Influence {

	//FIXME : hide this
	final private int m_agentIndex;
	public enum REACTION { request, valid, nonvalid }

    private REACTION m_status;
	
	public Influence(int agentIndex) {
		m_agentIndex= agentIndex;
		m_status= REACTION.request;
	}

	//@override
	public String toString(){
		return "" + m_agentIndex;
	}

	public void SetStatus(REACTION reaction) {
		m_status= reaction;
	}
	
	public boolean IsValid(){
		//Macro.Debug(" status : " + m_status);
		return m_status.equals(REACTION.valid);
	}
	
}
