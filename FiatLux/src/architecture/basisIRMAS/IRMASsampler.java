package architecture.basisIRMAS;

import components.types.IntC;
import components.types.Monitor;
import experiment.samplers.SuperSampler;
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.AutomatonViewer;
import grafix.viewers.GridViewer;

public class IRMASsampler extends SuperSampler /*implements InteractiveSimulable*/ {
	private static final String NAME = "NQueens testing...";
	IRMAsystem m_system;
	GridViewer m_viewer;

	public IRMASsampler(IRMAsystem system, IntC cellSizeForViewer){
		m_system = system;
		m_viewer = m_system.GetViewer(cellSizeForViewer);
		addMonitor(m_viewer);
	}

	@Override
	protected String GetModelName() {
		return NAME;
	}	

	@Override
	public int GetTime() {
		return m_system.GetTime();
	}


	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------

	@Override
	public String GetSimulationInfo() {
		return "hi ha " + m_system.GetTime();
	}

	@Override
	public String GetTopologyInfo() {
		return "X,Y:" + m_system.GetXYsize();
	}

	@Override
	public FLPanel GetSimulationWidgets() {
		FLPanel p = new FLPanel();
		p.SetBoxLayoutY();
		p.Add(m_system.GetSimulationPanel());
		return p;
	}

	@Override
	public FLPanel GetSimulationRuleControls() {
		return null;
	}

	@Override
	public FLPanel GetSimulationView() {
		return FLPanel.NewPanel(m_viewer);
	}

	@Override
	public AutomatonViewer GetViewer() {
		return m_viewer;
	}

	@Override
	public String[] GetPlotterSelection() {
		return null;
	}

	@Override
	protected Monitor GetLinkedSystemAsMonitor() {
		return m_system;
	}

	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------


	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------



}
