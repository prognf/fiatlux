package architecture.rigidLatticeGas;

import components.types.IntC;
import components.types.Monitor;
import experiment.samplers.SuperSampler;
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.AutomatonViewer;
import main.Macro;

/** TODO : finish this ? collaboration with Irène ? **/
public class RigidLGsampler extends SuperSampler {
	private static final String MODELNAME = "RigidLG";
	BinLGsystem m_system;
	RigidLGviewer m_viewer;
	
	public RigidLGsampler(BinLGsystem system, IntC CellPixSize, int Tsize) {
		m_system= system;		
		m_viewer= new RigidLGviewer(CellPixSize, m_system, Tsize);
		addMonitor(m_viewer);
		Integer a= 1;
		Integer b= 2;
		Integer c= b;
		b= a;
		a= c;
		Macro.fPrint(" a:%d b:%d c:%d", a, b, c);
	}

	@Override
	public int GetTime() {
		return m_system.GetTime();
	}

	@Override
	protected String GetModelName() {
		return MODELNAME;
	}

	/*---------------------------------------------------------------------------
	  - implementation : SamplerSimulable
	  --------------------------------------------------------------------------*/
	@Override
	public String GetSimulationInfo() {
		String info= "t:" + GetTime() ; 
		return info;
	}

	@Override
	public String GetTopologyInfo() {
		return " ring of size:" + m_system.GetSize();
	}

	@Override
	public FLPanel GetSimulationWidgets() {
		return new FLPanel();
	}

	@Override
	public FLPanel GetSimulationRuleControls() {
		return null;
	}

	@Override
	public FLPanel GetSimulationView() {
		FLPanel p = new FLPanel();
		p.Add(m_viewer);
		return p;
	}

	@Override
	public AutomatonViewer GetViewer() {
		return m_viewer;
	}

	@Override
	public String[] GetPlotterSelection() {
		return null;
	}

	@Override
	protected Monitor GetLinkedSystemAsMonitor() {
		return m_system;
	}

}
