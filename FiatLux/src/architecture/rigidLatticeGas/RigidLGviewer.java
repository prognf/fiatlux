package architecture.rigidLatticeGas;

import components.types.IntC;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.viewers.LineAutomatonViewer;


/** 1D model of lattice gas **/
public class RigidLGviewer extends LineAutomatonViewer {

	final static FLColor [] col = {
		FLColor.c_white, FLColor.c_green, FLColor.c_orange, FLColor.c_blue};
	
	BinLGsystem m_system;
	
	protected RigidLGviewer(IntC in_CellPixSize, 	BinLGsystem system, int Tsize ) {
		super(in_CellPixSize, system, system.GetSize(), Tsize);
		m_system= system;
	
		PaintToolKit palette= new PaintToolKit(col);
		this.SetPalette(palette);
	}

	@Override
	public void UpdateBuffer(int[] timeBuffer) {
		int size= timeBuffer.length;
		//Macro.Debug(" filling array of size : " + size);
		for (int pos=0; pos< size; pos++){
			int state= m_system.GetCellState(pos);
			timeBuffer[pos]= state;
		}
	}

	@Override
	protected void FlipCellState(int pos) {
		int st=  m_system.GetCellState(pos);
		st = (st + 1) %  4;
		int lval= st%2, rval= st /2;
		m_system.SetCellStateExternal(pos, lval, rval);
	}
	
	
}
