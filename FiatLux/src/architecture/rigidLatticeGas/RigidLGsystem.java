package architecture.rigidLatticeGas;

import java.util.ArrayList;

import components.arrays.TimeReadable;
import main.Macro;

public class RigidLGsystem implements TimeReadable {
		
		final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();
		
		/*----------------------------------------------------------------------------
		  - attributes
		  --------------------------------------------------------------------------*/
		
		int m_size;
		ArrayList<RigidCell> m_arrayHi, m_arrayHa, m_currentArray;
		
		int m_time; 
		
		/*---------------------------------------------------------------------------
		  - construction
		  --------------------------------------------------------------------------*/
		
		public RigidLGsystem(int size) {
			m_size= size;
			// creating cells 
			m_arrayHi= new ArrayList<RigidCell>(m_size);
			m_arrayHa= new ArrayList<RigidCell>(m_size);
			for (int pos=0; pos < m_size;pos++){
				m_arrayHi.add( new RigidCell() );
				m_arrayHa.add( new RigidCell() );
			}
			// adding links
			for (int pos=0; pos < m_size;pos++){
				int posL = (pos - 1 + m_size) % m_size;
				int posR = (pos + 1) % m_size;
				RigidCell //crossing odd and even times
					leftHi= mHi(posL),
					leftHa= mHa(posL),
					rightHi= mHi(posR),
					rightHa= mHa(posR);
				mHi(pos).SetLeftRightNeighb(rightHa.m_valL, leftHa.m_valR);
				mHa(pos).SetLeftRightNeighb(rightHi.m_valL, leftHi.m_valR);
			}
			
		}
		
		final private RigidCell GetCell(int pos) {
			return m_currentArray.get(pos);
		}
		
		final private RigidCell mHi(int pos){
			return m_arrayHi.get(pos);
		}
		
		final private RigidCell mHa(int pos){
			return m_arrayHa.get(pos);
		}

		/*---------------------------------------------------------------------------
		  - signals
		  --------------------------------------------------------------------------*/
		
		public void sig_Init() {
			m_time=0;			
			m_currentArray= m_arrayHi;
		}

		
		public void sig_NextStep() {
			PrintState();
			m_time++;
			m_currentArray= (m_time%2==0)?m_arrayHi:m_arrayHa;
			for (RigidCell cell : m_currentArray){
				cell.Propagate();
			}	
			PrintState();
		}
		/*---------------------------------------------------------------------------
		  - implementations & overrides
		  --------------------------------------------------------------------------*/
		
		private void PrintState() {
			for (RigidCell cell : m_currentArray){
				Macro.printNOCR(cell.GetState()+":");
			}	
			Macro.CR();
		}

		/*---------------------------------------------------------------------------
		  - get & set
		  --------------------------------------------------------------------------*/
		public int GetTime() {
			return m_time;
		}

		public int GetSize() {
			return m_size;
		}
		
		public int GetCellState(int pos) {
			return GetCell(pos).GetState();
		}

		public void SetCellStateExternal(int pos, int lval, int rval) {
			GetCell(pos).SetValLR(lval, rval);		
		}
		/*---------------------------------------------------------------------------
		  - test
		  --------------------------------------------------------------------------*/
		
		public static void DoTest(){
			Macro.BeginTest(CLASSNAME);
			Macro.EndTest();
		}

		

		

	

}
