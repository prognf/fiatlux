package architecture.rigidLatticeGas;

import java.util.ArrayList;

import components.arrays.TimeReadable;
import components.randomNumbers.StochasticSource;
import components.types.FLsignal;
import components.types.Monitor;
import main.Macro;
import main.MathMacro;

/*
 * work with Irene 4 june 2015
 */
public class BinLGsystem extends StochasticSource
implements TimeReadable, Monitor

{
		
		final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();
		private static final double BETA = 1.;
		private static final double INITPROBA = 0.5;
		
		/*----------------------------------------------------------------------------
		  - attributes
		  --------------------------------------------------------------------------*/
		
		int m_size;
		ArrayList<BinCell> m_arrayHi, m_arrayHa, m_currentArray;
		
		int m_time; 
		private double[] m_Z, m_trans;
		
		/*---------------------------------------------------------------------------
		  - construction
		  --------------------------------------------------------------------------*/
		
		public BinLGsystem(int size) {
			m_size= size;
			// creating cells 
			m_arrayHi= new ArrayList<BinCell>(m_size);
			m_arrayHa= new ArrayList<BinCell>(m_size);
			m_currentArray= m_arrayHi;
			for (int pos=0; pos < m_size;pos++){
				m_arrayHi.add( new BinCell() );
				m_arrayHa.add( new BinCell() );
			}
			// adding links
			for (int pos=0; pos < m_size;pos++){
				int posL = (pos - 1 + m_size) % m_size;
				int posR = (pos + 1) % m_size;
				
				mHi(pos).SetNeighb(mHa(posL), mHa(pos), mHa(posR));
				mHa(pos).SetNeighb(mHi(posL), mHi(pos), mHi(posR));
			}
		
			// init
			for (BinCell cell : m_currentArray){
				cell.Init();
			}	
			CalculateProba();
		}
		

		private void CalculateProba() {
			m_Z = new double[3];
			m_trans= new double[5];
			for (int i=0; i < 3; i++){
				m_Z[i]= MathMacro.Exp(i*BETA) + MathMacro.Exp(-i*BETA);
			}
			for (int d=-2 ; d<=2; d++){
				int absd= MathMacro.Abs(d);
				m_trans[d+2]= MathMacro.Exp(d*BETA) / m_Z[absd];
				//Macro.Debug(d + " ==> " + m_trans[d+2]);
			}
		}

		final private BinCell GetCell(int pos) {
			return m_currentArray.get(pos);
		}
		
		final private BinCell mHi(int pos){
			return m_arrayHi.get(pos);
		}
		
		final private BinCell mHa(int pos){
			return m_arrayHa.get(pos);
		}

		/*---------------------------------------------------------------------------
		  - signals
		  --------------------------------------------------------------------------*/
		public void ReceiveSignal(FLsignal sig){
			switch(sig){
			case init:
				sig_Init();
				break;
			case update:
				break;
			case nextStep:
				sig_NextStep();
				break;
			}
		}
		
		public void sig_Init() {
			m_time=0;			
			m_currentArray= m_arrayHi;
			for (BinCell cell : m_currentArray){
				boolean presR=
						RandomEventDouble(INITPROBA);
				if (presR){
					cell.SetValLR(0, 1);
				}
			}
		}
		
		public void sig_NextStep() {
			PrintState();
			m_time++;
			m_currentArray= (m_time%2==0)?m_arrayHi:m_arrayHa;
			if (m_time%2==0){
				Interact();
			} else {
				Propagate();	
			}		
				
			//PrintState();
		}
		
		
		
		private void Propagate() {
			for (BinCell cell : m_currentArray){
				cell.Propagate();
			}
		}

		public void Interact(){
			for (BinCell cell : m_currentArray){
				// reading previous state
				boolean	 one=cell.m_NeighbC.OneParticle();
				// if different
				if ( one ){
					// reading neighbours dir. field
					int fieldL= cell.m_NeighbL.DirField(),
						fieldR= cell.m_NeighbR.DirField();
					int sim= cell.m_NeighbC.DirField() * (fieldL + fieldR);
					// number between -2 and 2
					int index= sim + 2;
					double probaToStay= m_trans[index];
					if (RandomEventDouble(probaToStay)){
						cell.CopyStay();
					} else{
						cell.CopyFlip();
					}
				} else {
					cell.CopyFlip();
				}
			}	
			
		}
		/*---------------------------------------------------------------------------
		  - implementations & overrides
		  --------------------------------------------------------------------------*/
		
		
		private void PrintState() {
			for (BinCell cell : m_currentArray){
				Macro.printNOCR(cell.GetState()+":");
			}	
			Macro.CR();
		}

		/*---------------------------------------------------------------------------
		  - get & set
		  --------------------------------------------------------------------------*/
		public int GetTime() {
			return m_time;
		}

		public int GetSize() {
			return m_size;
		}
		
		public int GetCellState(int pos) {
			return GetCell(pos).GetState();
		}

		public void SetCellStateExternal(int pos, int lval, int rval) {
			GetCell(pos).SetValLR(lval, rval);		
		}
		/*---------------------------------------------------------------------------
		  - test
		  --------------------------------------------------------------------------*/
		
		public static void DoTest(){
			Macro.BeginTest(CLASSNAME);
			Macro.EndTest();
		}

		

}
