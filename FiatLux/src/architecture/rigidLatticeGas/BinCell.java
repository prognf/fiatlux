package architecture.rigidLatticeGas;


public class BinCell {

	
	ChannelBin m_valL, m_valR;
	BinCell m_NeighbL, m_NeighbC, m_NeighbR;	
	
	/** left and right neighbours for propagation */
	BinCell(){
	}
	
	public void Init() {
		m_valL= new ChannelBin();
		m_valR= new ChannelBin();
	}
	
	public int GetState() {
		return m_valL.m_val + 2 * m_valR.m_val;
	}

	public void SetValLR(int vLeft, int vRight) {
		m_valL.m_val= vLeft;
		m_valR.m_val= vRight;
	}

	public void SetNeighb(BinCell lNeighb, BinCell cNeighb, BinCell rNeighb){
		m_NeighbL= lNeighb;
		m_NeighbC= cNeighb;
		m_NeighbR= rNeighb;		
	}
	
	
	public void Propagate() {
		m_valL= m_NeighbR.m_valL;
		m_valR= m_NeighbL.m_valR;
	}
	
	public void CopyStay(){
		m_valL= m_NeighbC.m_valL;
		m_valR= m_NeighbC.m_valR;
	}
	
	public void CopyFlip(){
		m_valL= m_NeighbC.m_valR;
		m_valR= m_NeighbC.m_valL;
	}

	int DirField() {		
		return - m_valL.m_val + m_valR.m_val;
	}

	/** says if the cells contains one particle **/
	public boolean OneParticle() {
		return m_valL.m_val != m_valR.m_val;
	}

	
	
}
