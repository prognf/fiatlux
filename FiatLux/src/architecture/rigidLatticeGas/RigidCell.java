package architecture.rigidLatticeGas;

public class RigidCell {

	
	Integer m_valL, m_valR;
	Integer m_leftNeighb, m_rightNeighb;
	
	
	/** left and right neighbours for propagation */
	RigidCell(){
		m_valL=0;
		m_valR=0;
	}
	
	public void SetLeftRightNeighb(Integer leftNeighb, Integer rightNeighb){
		m_leftNeighb= leftNeighb;
		m_rightNeighb= rightNeighb;		
	}

	/*public void SetLeftRightNeighb(RigidCell leftNeighb, RigidCell rightNeighb){
		m_leftNeighb= leftNeighb;
		m_rightNeighb= rightNeighb;		
	}*/
	
	public int GetState() {
		return m_valL + 2 * m_valR;
	}

	public void SetValLR(int vLeft, int vRight) {
		m_valL= vLeft;
		m_valR= vRight;
	}

	/** preparation of the next state **/
	/** public void Propagate() {
		m_valL= m_rightNeighb;
		m_valR= m_leftNeighb;
	}*/
	
	public void Propagate() {
		m_valL= m_rightNeighb;
		m_valR= m_leftNeighb;
	}
	
	public void Interact(){
		
	}
	
}
