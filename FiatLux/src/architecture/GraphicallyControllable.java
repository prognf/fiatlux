package architecture;

import javax.swing.JComponent;

public interface GraphicallyControllable {
	
	JComponent GetSpecificPanel();

}
