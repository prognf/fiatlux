package architecture.latticeKinesis;

import java.util.ArrayList;

import architecture.latticeKinesis.LatticeKinesisSystem.DIAGONALDIR;
import architecture.latticeKinesis.Tile.TileTwoChannels;
import components.randomNumbers.FLRandomGenerator;
import components.types.IntC;
import components.types.IntegerList;
import main.Macro;
import main.MathMacro;

public class TileTable {

	// STRANGE usage : this will be called only once when the class is in use...
	// permutations (for particle movements)
	// change this usage ?
	static final ArrayList<ArrayList<IntegerList>> 
	m_listPermutations	=ConstructPermutationList();

	
	
	// index of direction as a function of x+1 and y+1
	static final int[][] LINE = 
		{{6,7,8}, {5, 0, 1 } , { 4 ,3, 2}};


	private static final int NCHANNEL = LatticeKinesisSystem.NCHANNEL;

	private static final int NTILE = 
			MathMacro.TwoToPowerIntResult(NCHANNEL);

	static final int 
	NORTH = LatticeKinesisSystem.NORTH;


	static final int EAST  = LatticeKinesisSystem.EAST;


	static final int SOUTH = LatticeKinesisSystem.SOUTH;


	static final int WEST  = LatticeKinesisSystem.WEST;


	//--------------------------------------------------------------------------
	//- attributes
	//--------------------------------------------------------------------------

	private static final int NLIST = NCHANNEL + 1;  

	final static Tile [] m_tabTile= new Tile[NTILE] ;

	private static final int NCHANNELS = 4;



	final FLRandomGenerator m_rand;


	//--------------------------------------------------------------------------
	//- construction
	//--------------------------------------------------------------------------


	// all tiles with same number of particles
	ArrayList<ArrayList<Tile>> m_listSamePop= 
			new ArrayList<ArrayList<Tile>> (NLIST);


	public TileTable(FLRandomGenerator randomizer){
		//Macro.Debug(" Init config Tab");
		m_rand = randomizer;

		// creating n lists
		for (int ipop=0; ipop < NLIST; ipop++){
			ArrayList<Tile> lst = new ArrayList<Tile>();
			m_listSamePop.add( lst );
		}

		// adding tiles
		for (int tileCode=0; tileCode< NTILE; tileCode++){
			Tile tile = new Tile(tileCode);
			//adding in Tab
			m_tabTile[tileCode]= tile; 
			// updating lists by pop
			int pop = tile.m_pop;
			@SuppressWarnings("unused")
			boolean blocked = SwarmingLGCAsys.HORIZONTALMODE && 
					((tile.w==1) || (tile.s==1));
			if (!blocked){
				m_listSamePop.get(pop).add(tile);
			}
		}
		//		Macro.Debug(" lst 2 " + m_listSamePop.get(2));
		

	}

	public void CalculateReflectingBorders(){
		// for each tile creating the tables of reflexting borders
		for (Tile tile : m_tabTile){
			tile.CalculateReflectingBordersTileTable();
		}
	}
	
	private static ArrayList<ArrayList<IntegerList>> ConstructPermutationList() {
		Macro.print(" constructing permutation lists");
		// permutations for all sizes to 4 included
		ArrayList<ArrayList<IntegerList>> perm= 
				new ArrayList<ArrayList<IntegerList>>(NCHANNEL+1);
		for (int iperm=0; iperm <= NCHANNEL; iperm++){
			ArrayList<IntegerList> element= IntegerList.GetPermutations(iperm);
			perm.add(iperm,element); 
		}
		return perm;
	}

	//--------------------------------------------------------------------------
	//- selection of tiles
	//--------------------------------------------------------------------------



	/** entry : configCode ; output : ConfigCode **/
	public Tile GetRandomConfigCodeWithSamePop(Tile tile) {
		int pop= tile.m_pop;
		switch (pop){
		case 0:
		case 4:
			return tile;
		case 1:
			return ChooseInList(1);
		case 2:
			return ChooseInList(2);
		case 3:
			return ChooseInList(3);
		default:
			Macro.SystemWarning("Unexpected value in ConfigTable");
			return null;
		}
	}

	private Tile ChooseInList(int listNum) {
		ArrayList<Tile> lst= m_listSamePop.get(listNum);
		int index= m_rand.RandomInt(lst.size());
		Tile tile= lst.get(index);
		return tile;
	}

	//--------------------------------------------------------------------------
	//- reflecting borders
	//--------------------------------------------------------------------------

	
	private void MoveParticle(IntegerList newtile, int ch, int target) {
		newtile.set(ch, 0);
		newtile.set(target, 1);
	}

	public TileTwoChannels zzzGetTileForCleanChannels(Tile tileToClean, DIAGONALDIR dir) {
		TileTwoChannels result = tileToClean.new TileTwoChannels();
		int ch1= dir.m_ch1, ch2= dir.m_ch2;
		boolean free1= tileToClean.IsFree(ch1), free2=tileToClean.IsFree(ch2);
		IntegerList legals= new IntegerList();
		for (int freech : tileToClean.m_freeChannels){
			if ((freech != ch1)&&(freech!=ch2)){
				legals.addVal(freech);
			}
		}
		int target1, target2;

		IntegerList newtile= tileToClean.ToIntList();

		if (free1 && free2){
			return null;
		} else {
			target1= free1?ch1:legals.ChooseOneAtRandom(m_rand);
			if (!free1){
				legals.RemoveItem(target1);
				MoveParticle(newtile, ch1, target1);
			} 
			target2 = free2?ch2:legals.ChooseOneAtRandom(m_rand);
			if (!free2){
				legals.RemoveItem(target2);  // TODO : Not useful
				MoveParticle(newtile, ch2, target2);
			}
		}
		result.m_channel1= target1;
		result.m_channel2= target2;
		int newCode= newtile.toIntByDecimalConversion();
		result.m_tile= m_tabTile[newCode]; 
		return result;
	}

	//--------------------------------------------------------------------------
	//- get / set
	//--------------------------------------------------------------------------

	/** selects one random permutation of a given size 
	 * not static because of the random generator */ 
	public IntegerList GetRandomPermutation(int size) {
		ArrayList<IntegerList> permList = m_listPermutations.get(size);
		int irand= m_rand.RandomInt(permList.size());
		return permList.get(irand);
	}


	static IntC GetLocalFieldFromConfigInfo(int configCode){
		return m_tabTile[configCode].m_localField;
	}

	public static Tile GetTile(int tileCode) {
		return m_tabTile[tileCode];
	}

	public static Tile GetTileByCh(int n, int e, int s, int w) {
		int tileCode= (n << 0) + (e << 1) + (s << 2) + (w << 3);
		//Macro.FormatDebug(" %d %d %d %d > %d",n,e,s,w,tileCode);
		return GetTile(tileCode);
	}

	/** sets the state of a particular cell according to a given configuration
	 * this configuration is identified by its code (could be changed) **/
	public static void SetState(int[] cellState, Tile tile) {
		cellState[NORTH]= tile.n;
		cellState[EAST]	= tile.e;
		cellState[SOUTH]= tile.s;
		cellState[WEST]	= tile.w;
	}

	public ArrayList<Tile> GetListSamePop(int pop) {
		return m_listSamePop.get(pop);
	}
	//--------------------------------------------------------------------------
	//- TILE
	//--------------------------------------------------------------------------


	public void PrintTableState(){
		for (Tile tile : m_tabTile){
			Macro.fPrint(" tile: %s |-| corner Cl:%s |-| Border Cl: %s ", 
					tile, tile.m_listCleanedCorner, tile.m_listCleanedBorder);
		}
	}
	
	/** TESTING **/
	static public void DoTest() {
		Macro.BeginTest("??");
		TileTable tab = new TileTable(FLRandomGenerator.GetGlobalRandomizer());
		tab.CalculateReflectingBorders();
		Macro.SeparationLine();
		tab.PrintTableState();
		//Macro.print( " lst 2 : " + m_listPermutations.get(2) );
		//Macro.print( " lst 4 : " + m_listPermutations.get(4) );
		//FLRandomGenerator randomizer= FLRandomGenerator.GetNewRandomizer();
		//TileTable tab = new TileTable(randomizer); 
		//IntegerList perm= tab.GetRandomPermutation(4);
		//Macro.print(" random drawn permutation : " + perm);
		Macro.EndTest();
	}



	public static void main(String[] argv) {
		DoTest();
	}





}
