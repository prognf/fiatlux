package architecture.latticeKinesis;

import components.arrays.SuperSystem;
import components.types.IntC;
import initializers.SuperInitializer;
import topology.basics.SuperTopology;


abstract public class LatticeKinesisInit extends SuperInitializer {
	protected LatticeKinesisSystem m_system;
	protected final IntC m_XYsize;

	public LatticeKinesisInit(LatticeKinesisSystem sys){
		m_system = sys;
		m_XYsize = m_system.GetXYsize();
	}

	@Override
	protected void PreInit() {
	}

	/* why is this necessary ? */
	final protected void ClearAllCells() {
		for (int pos=0 ; pos < m_system.GetNsize(); pos++){
			m_system.InitCellByChannels(pos,0,0,0,0); 	
		}
	}
	
	@Override
	public void LinkTo(SuperSystem sys, SuperTopology topo) {
	//not used	
	}
}
