package architecture.latticeKinesis; 

import java.util.ArrayList;

import components.types.IntC;
import grafix.gfxTypes.controllers.DoubleController;
import grafix.gfxTypes.elements.FLPanel;
import grafix.windows.SimulationWindowSingle;
import main.MathMacro;

public class SwarmingLGCAsys extends LatticeKinesisSystem {
	private static final String NAME = "swarming";
	private static final double DEFSIGMA = 2.;

	// al. pr. varies between -8 and +8 
	// (-(4 + 4) to (4 + 4) as product is obtained with x and y) 
	final static int MAXALIGN= (2* NCHANNEL), NALIGNMENTPRODUCT = 2 * MAXALIGN + 1; 
	final double [] m_weightByAlignmentProduct; // table of weights


	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------

	private double m_sigma = DEFSIGMA; // alignment coefficient
	final SigmaControl m_sigmaC;


	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------
	
	public SwarmingLGCAsys(IntC XYsize, boolean reflectingBorders) {
		super(XYsize, reflectingBorders);
		m_sigma = DEFSIGMA;
		m_weightByAlignmentProduct= new double[NALIGNMENTPRODUCT];
		CalculateWeightTable();
		m_sigmaC = new SigmaControl();
	}

	
	public SwarmingLGCAsys(IntC XYsize) {this(XYsize,DEFREFLECTINGBORDERS);}

	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------

	/** interaction step **/
	protected final Tile CalculateInteractionInTile(int pos) {
		Tile tile= GetTile(pos);
		int pop=tile.GetPop(); 
		Tile newTile;
		if (pop>=1){
			ArrayList<Tile> lstSamePop= m_tileTab.GetListSamePop(pop);
			IntC Nfield= GetNeighbFieldPos(pos);
			newTile= DrawConfigByWeight(lstSamePop, Nfield );
		} else {
			newTile= tile;
		}
		return newTile; 
	}

	/**
	 * selection by weight
	 */
	private final Tile DrawConfigByWeight(ArrayList<Tile> lstSamePop, IntC neighbField) {
		// constructing weight array
		ArrayList<Double> weightTab = new ArrayList<Double>();
		for (Tile dynamis : lstSamePop){
			double w= GetWeight(dynamis, neighbField);
			weightTab.add(w) ;
		}

		//DEBUG
		//for (int i=0; i <pop; i++){
		//int pop= lstSamePop.size();
		//Tile dynamis = lstSamePop.get(i);
		//Macro.Debug(" tuile :" + dynamis + " w: " + weightTab.get(i));
		//}
		// drawing randomly
		double randSample= GetRandomizer().RandomDouble();
		int indexR= MathMacro.RandomSelectByWeight(weightTab, randSample);
		double doubleW = weightTab.get(indexR); 
		/*double doubleW = Collections.max(weightTab);			
		int imax= weightTab.indexOf(doubleW);*/
		//String msg = FLString.ToString(weightTab) + "i: " + indexR +" d: " + doubleW;
		//Macro.Debug(msg);
		return lstSamePop.get(indexR);
	}

	private final double GetWeight(Tile dynamis, IntC neighbField) {
		IntC directorField= dynamis.GetLocalField();
		int weightProduct = directorField.ScalarProduct(neighbField);
		// DEBUG
		//if ((weightProduct>8) || (weightProduct<-8)){
		//	Macro.FormatDebug(" dir: %s  tile: %s  w:%d", neighbField, dynamis, weightProduct);
		//}
		return GetWeight(weightProduct);
	}

	/** associates to each alignment product a weight of appearance **/
	public void CalculateWeightTable(){
		for (int iweight=0 ; iweight < NALIGNMENTPRODUCT; iweight++){
			m_weightByAlignmentProduct[iweight]= Math.exp(m_sigma * iweight); 
		}
	}

	/** weight product should vary between -8 and +8 **/
	public double GetWeight(int weightProduct){
		return m_weightByAlignmentProduct[MAXALIGN + weightProduct];
	}

	private static final int NTEST = 10;

	public String GetCellInfo(int x, int y) {
		int pos= Map2Dto1D(x, y);
		//for (int i=0; i< NTEST; i++){
		//	Tile randTile= CalculateInteractionInTile(pos); 
		//	Macro.Debug(" rand found : " + randTile);
		//}
		return super.GetCellInfo(x, y);
	}

	public static void DoTest(){
		IntC XYsize= new IntC(3, 3);
		SwarmingLGCAsys sys = new SwarmingLGCAsys(XYsize);
		IntC pixCellSize = new IntC(40,1);
		LatticeKinesisSampler samp = new LatticeKinesisSampler(sys, pixCellSize);
		SimulationWindowSingle exp= new SimulationWindowSingle("test", samp);
		exp.BuildAndDisplay();
	}

	@Override	
	public FLPanel GetSpecificPanel(){
		FLPanel p = FLPanel.NewPanel(m_sigmaC, m_alphaI.GetControl());
		m_alphaI.GetControl().minimize();
		p.setOpaque(false);
		m_sigmaC.setOpaque(false);
		m_alphaI.GetControl().setOpaque(false);
		return p;
	}

	/** sets sigma and recalculate interaction table **/
	public void SetSigma(double sigma) {
		m_sigma = sigma;
		CalculateWeightTable();
		//m_sigmaC.Update();
		//Macro.Debug("sigma set to : " + m_sigma);
	}
	
	class SigmaControl extends DoubleController {
		public SigmaControl() {
			super("Sigma");
			AddSlider(0., 5., 50);
		}

		@Override
		protected double ReadDouble() {
			return m_sigma;
		}

		@Override
		protected void SetDouble(double val) {
			SetSigma(val);
		}
	}

	@Override
	public String GetModelName() {
		return NAME;
	}

	public double GetSigma() {
		return m_sigma;
	}
}
