package architecture.latticeKinesis;

import components.types.IntC;
import grafix.gfxTypes.elements.FLPanel;

/*** BASIS : very simple model with equal probability for new directions **/
public class LatticeKrandomCollisions extends LatticeKinesisSystem {

	private static final String NAME = "randomCollisions";
	private static final boolean ReflectingBorders = false;

	public LatticeKrandomCollisions(IntC XYsize) {
		super(XYsize, ReflectingBorders);
	}

	/** interaction step **/
	protected Tile CalculateInteractionInTile(int pos) {
		Tile tile= GetTile(pos);
		Tile newTile;
		int numpart=tile.GetPop(); 
		if (numpart>1){
			newTile= m_tileTab.GetRandomConfigCodeWithSamePop(tile);
		} else {
			newTile= tile;
		}
		//Macro.FormatDebug(" old cc : %d new cc: %d", configCode, newConfigCode);
		return newTile;
	}

	@Override
	public FLPanel GetSpecificPanel() {
		return null;
	}

	@Override
	public String GetModelName() {
		return NAME;
	}
	
}
