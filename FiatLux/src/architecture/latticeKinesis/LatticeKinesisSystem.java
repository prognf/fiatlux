package architecture.latticeKinesis;

import architecture.GraphicallyControllable;
import architecture.latticeKinesis.Tile.TileOneChannel;
import architecture.latticeKinesis.Tile.TileTwoChannels;
import components.arrays.GridSystem;
import components.types.DoublePar;
import components.types.FLStringList;
import components.types.FLsignal;
import components.types.IntC;
import components.types.IntCm;
import components.types.IntegerList;
import components.types.Monitor;
import main.Macro;


/*-------------------------------------------------------------------------------
	- re-implementation of lattice gas
	  August 2014
	- @author Nazim Fates
	------------------------------------------------------------------------------*/

//HERE particles could be defined with a list of objects (or identifiers) !
abstract public class LatticeKinesisSystem extends GridSystem 
implements Monitor, GraphicallyControllable
{

	abstract public String GetModelName();
	abstract protected  Tile CalculateInteractionInTile(int pos);
	//abstract public  	Component GetSpecificPanel();

	//--------------------------------------------------------------------------
	//- constants
	//--------------------------------------------------------------------------

	static final boolean TRACK = false; // follow particles or not ?
	static final boolean STOP_BETWEEN_I_AND_P = false; // separate the two inter-steps
	static final boolean blockPropagation = false; // interaction only (special)

	// for repeating n interaction steps
	private static final int NINTERACTIONREPEATED = 1;

	// horizontal mode
	static final boolean HORIZONTALMODE = false;  

	
	static final boolean DEFREFLECTINGBORDERS = false;

	private static final double INITALPHAI = 1.; // interaction asynchrony

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();

	final static int NCHANNEL= 4;

	// offset N,E,S,W
	final static int NORTH=0, EAST=1, SOUTH=2, WEST=3; 
	final static IntC [] DXY = { 
		new IntC(0, 1),
		new IntC(1, 0),
		new IntC(0, -1),
		new IntC(-1, 0),
	};

	enum DIAGONALDIR { 
		NORTHEAST(NORTH,EAST),
		SOUTHEAST(SOUTH,EAST),
		SOUTHWEST(SOUTH,WEST), 
		NORTHWEST(NORTH,WEST);

		DIAGONALDIR(int ch1, int ch2){
			m_ch1= ch1; m_ch2=ch2;
		}
		final int m_ch1, m_ch2;
	}


	// code for empty tile
	private static final int EMPTYTILE = 0;
	

	//--------------------------------------------------------------------------
	//- attributes
	//--------------------------------------------------------------------------
	final boolean m_reflectingBorders; // special

	private Tile [] 	m_tile, m_tileBuffer ; 			// state

	// code (int) of the particle at coord: [pos][channel]
	int [][] m_particleCur, m_particleBuf; // particles tracking


	// help for computing the next state
	final IntCm [] msyn_cellField;

	// topology
	final int [][] m_destinationPosCh;

	// transitions 
	protected final TileTable m_tileTab;


	// asynchronism : could be put in an independent object
	DoublePar m_alphaI= new DoublePar("alpha I", INITALPHAI);

	//--------------------------------------------------------------------------
	//- construction
	//--------------------------------------------------------------------------

	protected LatticeKinesisSystem(IntC XYsize, boolean reflectingBorders) {
		super(XYsize);
		m_reflectingBorders= reflectingBorders;
		m_tileTab = new TileTable(GetRandomizer());
		if (m_reflectingBorders){
			m_tileTab.CalculateReflectingBorders();
		}
		int Nsize= GetNsize();
		m_tile= new Tile[Nsize];
		m_tileBuffer= new Tile[Nsize];

		// next step calculus helpers
		msyn_cellField= new IntCm[Nsize];
		for (int pos=0; pos< Nsize; pos++){
			msyn_cellField[pos]= IntCm.GetNewObjectWithDummyVals();
		}

		m_destinationPosCh= new int[Nsize][NCHANNEL];
		CalculateNextPosTable();

		// particles
		if (TRACK){
			m_particleCur= new int[Nsize][NCHANNEL];
			m_particleBuf= new int [Nsize][NCHANNEL];
		}

	}

	/** "topology" method **/
	private void CalculateNextPosTable() {
		for (int x=0; x < GetXYsize().X() ; x++){
			for (int y=0; y < GetXYsize().Y(); y++){
				for (int ch=0; ch < NCHANNEL; ch++){
					int pos = Map2Dto1D(x, y);
					int newpos = Map2Dto1DoffsetBC(x, y, DXY[ch]);
					m_destinationPosCh[pos][ch]= newpos;
				}
			}
		}
	}

	//--------------------------------------------------------------------------
	//- signals
	//--------------------------------------------------------------------------

	boolean m_step;

	private int m_particleId;

	@Override
	public void ReceiveSignal(FLsignal sig){
		switch(sig){
		case init:
			sig_Init();
			break;
		case update:
			sig_Update();
			break;
		case nextStep:
			sig_NextStep();
			break;
		}
	}

	

	public void sig_NextStep() {
		if (STOP_BETWEEN_I_AND_P){
			NextStepStop();
		} else {
			NextStepRegular();
		}
	}


	private void NextStepRegular() {
		TimeClick();
		//---I
		DoInteraction();
		//---P
		if (!blockPropagation){
			Propagation();
		}
		//-- synch
		Synchronise();
	}

	private void NextStepStop() {
		m_step= !m_step;
		if (m_step){
			TimeClick(); // advance by one time step
			//---I
			DoInteraction();
			//-- synch
			Synchronise(); // useful only if the step is broken 
		} else {
			//---P
			if (!blockPropagation){
				Propagation();
			}
			//-- synch
			Synchronise();
		}		
	}


	private void DoInteraction() {
		for (int i=0; i < NINTERACTIONREPEATED; i++){
			CalculateInteraction();
			PerformInteraction();
			if (m_reflectingBorders){
				ApplyReflectingBorders();		
			}
		}
	}


	private void CalculateInteraction() {
		//	Macro.Debug("Interaction");

		// SPEEDUP : necessary step ? (only particles are moved)
		int Nsize= GetNsize();
		if (TRACK){
			m_particleBuf= new int [Nsize][NCHANNEL];
		}

		double alphaI= m_alphaI.GetVal();
		boolean asynchronismApplied= alphaI<1.F-Macro.EPSILON;
		for (int pos=0; pos < Nsize; pos++){
			Tile newTile;
			if (asynchronismApplied 
					&& (!RandomEventDouble(alphaI)) ){// cell NOT updated
				newTile= GetTile(pos); // identity
			} else {
				newTile = CalculateInteractionInTile(pos);
			}
			m_tileBuffer[pos]= newTile;
		}
	}

	private void PerformInteraction() {
		if (TRACK){
			int nsize= GetNsize();
			for (int pos=0; pos < nsize; pos++){
				IntegerList 
				oldOccupied = m_tile[pos].m_occupiedChannels,
				newOccupied= m_tileBuffer[pos].m_occupiedChannels;

				MoveParticlesFromStateToBuffer(
						m_particleCur[pos], m_particleBuf[pos], 
						oldOccupied, newOccupied);		
			}
		}
		if (TRACK) {
			int[][] tmp2= m_particleCur;
			m_particleCur= m_particleBuf;
			m_particleBuf= tmp2;
		}

		// exchange buffers
		Tile [] tmp = m_tile;
		m_tile= m_tileBuffer;
		m_tileBuffer= tmp;

	}

	final static int SFT=1;

	private void ApplyReflectingBorders() {
		int X= m_XYsize.X(), Y= m_XYsize.Y();
		int yN= Y-1, xE= X-1;
		for (int x=SFT; x < X-SFT; x++){
			ClearChannel(x, yN-SFT, NORTH);
			ClearChannel(x, SFT , SOUTH);
		}

		for (int y=SFT; y < Y-SFT; y++){
			ClearChannel(xE-SFT, y, EAST);
			ClearChannel(SFT, y, WEST);
		}

		ClearTwoChannels(SFT,SFT,DIAGONALDIR.SOUTHWEST);
		ClearTwoChannels(xE-SFT,SFT,DIAGONALDIR.SOUTHEAST);
		ClearTwoChannels(SFT,yN-SFT,DIAGONALDIR.NORTHWEST);
		ClearTwoChannels(xE-SFT,yN-SFT,DIAGONALDIR.NORTHEAST);
	}

	/** SPECIAL : reflecting borders **/
	private void ClearTwoChannels(int x, int y, DIAGONALDIR dir) {
		int pos = GetPos(x, y);
		Tile tileToClean= GetTile(pos);
		TileTwoChannels cleaned= tileToClean.GetTileForCleanCorner(dir,GetRandomizer());
		if (cleaned != null){
			//Macro.fDebug("Corner clean: dir: %s --) ch.: %s -> %s ",dir, tileToClean, cleaned);
			m_tile[pos]= cleaned.m_tile;
			// for tracking
			if (TRACK){
				Permute(m_particleCur[pos], dir.m_ch1, cleaned.m_channel1);
				Permute(m_particleCur[pos], dir.m_ch2, cleaned.m_channel2);
			}	
		}
	}


	private void Permute(int[] tile, int chA, int chB) {
		int partCode= tile[chA];
		tile[chA]= tile[chB];
		tile[chB]= partCode;
	}


	/** SPECIAL : reflecting borders **/
	private void ClearChannel(int x, int y, int ch) {
		int pos = GetPos(x, y);
		Tile tileToClean= GetTile(pos);
		TileOneChannel cleaned= tileToClean.GetTileForCleanChannel(ch, GetRandomizer());
		if (cleaned!=null){// not an empty channel

			m_tile[pos]= cleaned.m_tile;

			// for tracking
			if (TRACK){
				int chnew= cleaned.m_channel;
				int partId= m_particleCur[pos][ch]; 
				m_particleCur[pos][ch]=0;
				m_particleCur[pos][chnew]= partId; 
			}
		}
	}

	/** SPECIAL : reflecting borders **/
	private void ClearCell(int x, int y) {
		int pos = GetPos(x,y);
		m_tile[pos]= TileTable.GetTile(EMPTYTILE);
	}


	private int GetPos(int x, int y) {
		return m_XYsize.Map2Dto1D(x, y);
	}



	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------


	protected final void MoveParticlesFromStateToBuffer(int[] oldpos, int[] newpos, 
			IntegerList oldOccupied, IntegerList newOccupied) {
		// exchanging particles
		//DEBUGif ((oldOccupied.size() != numpart)||(newOccupied.size() != numpart)){
		//	Macro.FatalError(" houhouhou !" );
		//}
		int numpart= oldOccupied.size();
		if (numpart==0){
			// do nothing
		} else if (numpart==1){ // no permutation needed (small optimisation)
			int oldIndex= oldOccupied.Get(0); 
			int newIndex= newOccupied.Get(0);
			int partId=  oldpos[oldIndex];
			newpos[newIndex]= partId;
		} else {
			IntegerList permutation = m_tileTab.GetRandomPermutation(numpart);
			for (int i=0; i < numpart; i++){
				int oldIndex= oldOccupied.Get(i); 
				// additional step for the permutation of particles
				int permutedIndex= permutation.Get(i);
				int newIndex= newOccupied.Get(permutedIndex);
				int partId=  oldpos[oldIndex];
				newpos[newIndex]= partId;
			}
		}

	}


	/** updating the synchronised info (with current state) **/
	public void Synchronise() {
		SynchDirectorFields();
	}




	/** calculating the director field in each pos 
	 * SPEEDUP : accelerate by suppressing the loop on channels ? (precalc)
	 * one pass could be sufficient **/
	private void SynchDirectorFields() {
		int nsize= GetNsize();
		for (int pos=0; pos < nsize; pos++){
			IntCm f= msyn_cellField[pos];
			f.SetVal(0,0);
			for (int ch=0 ; ch < NCHANNEL ; ch++){
				int neighb= m_destinationPosCh[pos][ch];
				IntC fneighb= GetLocalField(neighb); 
				f.Add(fneighb);
			}
		}
	}


	/** field associated to pos **/
	final private IntC GetLocalField(int pos) {
		Tile tile= m_tile[pos];
		return tile.GetLocalField();
	}


	/** method chosen here : by code 
	 * SPEEDUP : use one loop instead of two 
	 * for this, use "source" PosCh instead of destination */
	private void Propagation(){
		int Nsize= GetNsize();
		// speedup : make this one a member ?
		int [] tmpCode = new int [Nsize];
		int[][] bufferparticle;
		if (TRACK){
			bufferparticle= new int[Nsize][NCHANNEL];
		}
		// pre-calculus
		for (int pos=0; pos < GetNsize(); pos++){
			for (int ch=0; ch < NCHANNEL; ch++){
				int newpos= m_destinationPosCh[pos][ch];
				//speed-up possible here (4calls)
				int state= GetCellStateChannel(pos,ch); 
				tmpCode[newpos] += (state<<ch); 
				if (TRACK){
					bufferparticle[newpos][ch]= m_particleCur[pos][ch];
				}
			}
		}
		// updating
		for (int pos=0; pos < Nsize; pos++){
			int tileCode= tmpCode[pos];
			m_tile[pos]= TileTable.GetTile(tileCode); 
		}
		if (TRACK) {
			int[][] tmp2= m_particleCur;
			m_particleCur= bufferparticle;
			bufferparticle= tmp2;
		}
	}

	//--------------------------------------------------------------------------
	//- init (external modifs)
	//--------------------------------------------------------------------------

	/** call for resetting naming of particles **/ 
	public void InitPrepare() {
		m_particleId=0;
	}

	/** call synchronisers after state modification **/
	public void	InitCellByChannels(int pos, int n, int e, int s ,int w){
		Tile tile= TileTable.GetTileByCh(n, e, s, w);
		InitCellByTile(pos, tile);
	}

	/** call synchronisers after state modification **/
	public void	InitCellByTile(int pos, Tile tile){
		m_tile[pos] = tile;
		if (TRACK){
			m_particleCur[pos][NORTH]	= AddParticle(tile.n);
			m_particleCur[pos][EAST]	= AddParticle(tile.e);
			m_particleCur[pos][SOUTH]	= AddParticle(tile.s);
			m_particleCur[pos][WEST]	= AddParticle(tile.w);
		}
	}


	private int AddParticle(int p) {
		if (p==0){
			return 0;
		} else {
			m_particleId++;
			return m_particleId;
		}
	}

	/** updating data **/ 
	public void InitClose() {
		Synchronise();
	}


	//--------------------------------------------------------------------------
	//- get / set 
	//--------------------------------------------------------------------------
	public double getAlphaI() {
		return m_alphaI.GetVal();
	}

	/** applying asynchronism **/
	public void setAlphaI(double alphaI) {
		m_alphaI.SetVal(alphaI);
	}

	public final Tile GetTile(int pos) {
		return m_tile[pos];
	}

	private int GetCellStateChannel(int pos, int ch) {
		Tile tile= GetTile(pos);
		switch (ch){
		case 0:
			return tile.n;
		case 1:
			return tile.e;
		case 2:
			return tile.s;
		case 3:
			return tile.w;
		default:
			Macro.FatalError(" unexpected value in Getstate");
			return -1;
		}
	}


	public int GetPop(int x, int y) {
		int pos = Map2Dto1D(x,y);
		return GetTile(pos).GetPop();
	}

	/** called only by the viewer **/
	public int GetCellStateChannel(int x, int y, int ch) {
		int pos = Map2Dto1D(x,y);
		return GetCellStateChannel(pos, ch);
	}

	/** called only by the viewer **/
	public int GetParticleId(int x, int y, int ch) {
		int pos = Map2Dto1D(x,y);
		return m_particleCur[pos][ch];
	}

	public LatticeKinesisViewer GetViewer(IntC pixCellSize) {
		return new LatticeKinesisViewer(this, pixCellSize);
	}

	public IntC GetNeighbFieldPos(int pos) {
		return msyn_cellField[pos];
	}



	//--------------------------------------------------------------------------
	//- config
	//--------------------------------------------------------------------------

	/** general state of the system **/
	public FLStringList GetState() {
		FLStringList out= new FLStringList();
		//Macro.Debug (" XY: " + GetXYsize());
		for (int y=0; y < GetXYsize().Y(); y++ ){
			for (int x=0; x < GetXYsize().X(); x++ ){
				out.Add(GetCellInfo(x, y));
			}
		}
		return out;
	}

	public String GetCellInfo(int x, int y) {
		int pos = Map2Dto1D(x, y);
		IntC 
		LF=GetLocalField(pos), NF= GetNeighbFieldPos(pos);
		StringBuilder part= new StringBuilder("ø");
		for (int ch=0; ch < NCHANNEL; ch++){
			part.append(":").append(GetParticleId(x, y, ch));
		}
		part.append(":ø");

		String s = String.format(" %d,%d> pos: %d  tile:%s  L-field:%s N-field:%s",
				x, y, pos, m_tile[pos], LF, NF);
		return s + part;
	}

	/** access to the current state of the system **/
	public Tile[] GetTileTab() {
		return m_tile;
	}
	
	public IntC GetSizeXY() {
		return m_XYsize;
	}
}
