package architecture.latticeKinesis;

import javax.swing.JComponent;

import architecture.GraphicallyControllable;
import components.randomNumbers.StochasticSource;
import components.types.IntC;
import components.types.Monitor;
import experiment.samplers.SuperSampler;
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.AutomatonViewer;
import main.Macro;
import main.tables.PlotterSelectControl;

public class LatticeKinesisSampler extends SuperSampler {

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();


	LatticeKinesisSystem m_system;
	LatticeKinesisViewer 	m_viewer;
	LatticeKinesisInit 	m_init;

	public LatticeKinesisSampler(LatticeKinesisSystem system, IntC pixCellSize) {
		m_system = system;
		m_viewer = m_system.GetViewer(pixCellSize);
		m_init = new SwarmingLmigInit(m_system);
		AddMonitors(m_init, m_viewer);
		//Macro.Debug("initiated:" + this.getClass().getCanonicalName() );
		super.sig_Init(); // automatic init after creation
	}

	/** no viewer ! Rq: m_system has been corrected to system **/
	public LatticeKinesisSampler(LatticeKinesisSystem system) {
		Macro.print(3,"Init of " + CLASSNAME);
		if (system==null) {
			Macro.FatalError(" null system given to " + CLASSNAME);
		}
		m_system= system;
		m_viewer= null;
		m_init = new SwarmingLmigInit(m_system);
		addMonitor(m_init);
		super.sig_Init(); // automatic init after creation
	}

	@Override
	public int GetTime() {
		return m_system.GetTime();
	}

	@Override
	protected String GetModelName() {
		return m_system.GetModelName();
	}

	@Override
	/* TODO : HACK !**/
	public String GetSimulationInfo() {
		String samplerInfo = "noinfo";
		if (m_system instanceof SwarmingLGCAsys) {
			IntC XY= m_system.GetXYsize();
			SwarmingLmigInit init2 = (SwarmingLmigInit) m_init;
			SwarmingLGCAsys sys2 = (SwarmingLGCAsys) m_system;
			samplerInfo =
					String.format("XY= %s dini=%.2f sigma=%.2f seed-ini=%d seed-sys=%d",
							XY, init2.GetDini(), sys2.GetSigma(),
							init2.GetSeed(), sys2.GetSeed());
		}
		return samplerInfo;
	}

	@Override
	public String GetTopologyInfo() {
		return String.format( " XY: %s ", m_system.GetSizeXY() );
	}


	//--------------------------------------------------------------------------
	//- GFX
	//--------------------------------------------------------------------------
	@Override
	public FLPanel GetSimulationWidgets() {
		return FLPanel.NewPanel();
	}

	@Override
	public FLPanel GetSimulationRuleControls() {
		FLPanel p = new FLPanel();
		p.setOpaque(false);
		p.SetGridBagLayout();
		//Macro.Debug(" zinzin kontrol:" + m_init + " :: " + m_system);
		AddFormHorizontal(p, 1, m_init);
		AddFormHorizontal(p, 2, m_system);
		return p;
	}

	@Override
	public FLPanel GetSimulationView() {
		return FLPanel.NewPanel(m_viewer);
	}

	private void AddFormHorizontal(FLPanel p, int y, GraphicallyControllable source) {
		JComponent sourcepanel= source.GetSpecificPanel();
		if (sourcepanel != null) {
			p.AddGridBag(sourcepanel, 1, y);
		}
		if (source instanceof StochasticSource){
			StochasticSource src2= (StochasticSource)source;
			p.AddGridBag(src2.GetRandomizer().GetSeedControl(),2, y);
		}
	}

	//--------------------------------------------------------------------------
	//- get / set 
	//--------------------------------------------------------------------------

	@Override
	public String[] GetPlotterSelection() {
		return PlotterSelectControl.MIGRATION_SELECT;
	}

	@Override
	protected Monitor GetLinkedSystemAsMonitor() {
		return m_system;
	}


	public int GetInitSeed() {
		return m_init.GetRandomizer().GetSeed();
	}

	// TODO : put this at the "right" place
	public double GetInitDens() {
		return ((SwarmingLmigInit)m_init).GetDini();
	}

	public int GetSysSeed() {
		return m_system.GetRandomizer().GetSeed();
	}

	public LatticeKinesisSystem GetSystem() {
		return m_system;
	}

	/** cast here !**/
	public void SetInitdensSigma(double initdens, double sigma) {
		SwarmingLGCAsys sys= (SwarmingLGCAsys)m_system;
		sys.SetSigma(sigma);
		SwarmingLmigInit init= (SwarmingLmigInit)m_init;
		init.SetInitRate(initdens);		
	}

	public void SetSeedInitSys(int seedini, int seedsys) {
		m_init.GetRandomizer().setSeedFixedAndReset(seedini);
		m_system.GetRandomizer().setSeedFixedAndReset(seedsys);
	}

	public AutomatonViewer GetViewer() {
		return m_viewer;
	}

	public LatticeKinesisInit GetInitializer() {
		return m_init;
	}



}
