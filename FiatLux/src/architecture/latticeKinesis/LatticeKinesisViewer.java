package architecture.latticeKinesis;

import java.awt.Color;
import java.awt.Polygon;
import java.awt.event.MouseEvent;

import architecture.GraphicallyControllable;
import architecture.multiAgent.tools.DIR;
import components.randomNumbers.FLRandomGenerator;
import components.types.IntC;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.controllers.FLCheckBox;
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.GridViewer;
import grafix.viewers.MouseInteractive;
import main.Macro;

public class LatticeKinesisViewer extends GridViewer 
implements MouseInteractive, GraphicallyControllable {

	private static final String LABELPARTICLE = "particles";
	private static final FLColor COLARROWS = 
			FLColor.c_black, BCKGRND= FLColor.c_lightgrey;
	private static final Color PARTICLECOLOR = FLColor.black;

	final static FLColor [] denscol = 
		{ BCKGRND, FLColor.c_cyan, 
		FLColor.c_green, FLColor.c_lightred, FLColor.c_lightpurple};

	private static final int NDIRCOL = 9;
	final FLColor [] dircol = new FLColor[NDIRCOL]; 

	final static int [] hueval = { 0, 40, 90, 140, 190, 215, 270, 315};

	private static final int NCOL = 50;

	//--------------------------------------------------------------------------
	//- attributes
	//--------------------------------------------------------------------------


	final PaintToolKit partcol; 


	boolean  m_ShowParticleColor= true;
	final LatticeKinesisSystem m_system;
	Polygon N, E, S, W;

	//--------------------------------------------------------------------------
	//- construction
	//--------------------------------------------------------------------------

	public LatticeKinesisViewer(LatticeKinesisSystem system,IntC in_CellPixSize) {
		super(in_CellPixSize, system);
		ActivateMouseFollower(this);
		m_system = system;

		ActivateMouseFollower(this);

		// palette
		dircol[0]= BCKGRND; // (0,0)
		for (int i=1; i<NDIRCOL; i++){
			float hue = hueval[i-1] / (float)360;
			dircol[i] = new FLColor( FLColor.getHSBColor(hue, 1.0f, 1f) );
		}
		// SetPalette( PaintToolKit.GetRainbow(NCOL));
		FLRandomGenerator rnd = FLRandomGenerator.GetNewRandomizer();
		rnd.setSeedFixedAndReset(31032018);
		partcol= PaintToolKit.GetRandomPaletteBright(rnd, NCOL);
		partcol.SetColor(1, FLColor.c_black );

		N= new Polygon();		E= new Polygon();
		S= new Polygon();		W= new Polygon();
		super.ComputeFourTriangles(N,E,S,W); // init four triangles

	}

	//--------------------------------------------------------------------------
	//- Draw methods
	//--------------------------------------------------------------------------



	/** 
	 * main component
	 *  **/
	protected void DrawSystemState() {
		// DrawDensity();
		// colored squared
		DrawDirectorField();

		if( LatticeKinesisSystem.TRACK){
			DrawParticlesColors();
		} else if (m_ShowParticleColor){
			DrawParticleFromTiles();
		}
	}


	private void DrawParticleFromTiles() {
		SetColor(PARTICLECOLOR);
		for (int x=0; x < getXsize(); x++){
			for (int y=0; y < getYsize(); y++){
				int pos= Map2D(x, y);
				Tile tile= m_system.GetTile(pos);
				DrawCellParticle(x, y, tile);
			}
		}
	}


	/** orientation of particles **/
	final protected void DrawCellParticle(int x, int y, Tile tile){
		int 	Xpos = getXpos(x),
				Ypos = getYpos(y);
		super.translateOrigin(Xpos, Ypos);	
		if (tile.n>0){FillPolygon(N);}
		if (tile.e>0){FillPolygon(E);}
		if (tile.s>0){FillPolygon(S);}
		if (tile.w>0){FillPolygon(W);}
		ResetOrigin();		
	}


	private void DrawDirectorField() {
		super.DrawUniformCellularBackGround(BCKGRND);
		for (int x=0; x < getXsize(); x++){
			for (int y=0; y < getYsize(); y++){
				int pos= Map2D(x, y);
				Tile tile= m_system.GetTile(pos);
				int ifield= tile.GetLocalFieldIndex();
				SetColor( dircol[ifield] );
				DrawSquare(x, y);
			}
		}

	}

	private void DrawDensity() {
		super.DrawUniformCellularBackGround(BCKGRND);
		for (int x=0; x < getXsize(); x++){
			for (int y=0; y < getYsize(); y++){
				int pop= m_system.GetPop(x,y);
				FLColor col = denscol[pop];
				SetColor(col);
				DrawSquare(x, y);
			}
		}
	}

	/** big speed-up possible here **/
	private void DrawParticlesColors() {
		//super.DrawUniformCellularBackGround(BCKGRND);

		SetColor(COLARROWS);
		for (int x=0; x < getXsize(); x++){
			for (int y=0; y < getYsize(); y++){
				for (int ch=0; ch < LatticeKinesisSystem.NCHANNEL; ch++){
					int particuleId= m_system.GetParticleId(x,y,ch);
					if (particuleId>0){// if particle is present
						int icol= 
								m_ShowParticleColor? 
										(particuleId % NCOL): 1 ;
										FLColor col = partcol.GetColor(icol);
										SetColor(col);
										DrawCellParticle(x, y, DIR.FromInt(ch));
					}
				}
			}
		}	
	}


	private void DrawCellsOrientedParticles() {
		super.DrawUniformCellularBackGround(BCKGRND);

		SetColor(COLARROWS);
		for (int x=0; x < getXsize(); x++){
			for (int y=0; y < getYsize(); y++){
				for (int ch=0; ch < LatticeKinesisSystem.NCHANNEL; ch++){
					if (m_system.GetCellStateChannel(x,y,ch)==1){
						DrawCellParticle(x, y, DIR.FromInt(ch));
					}
				}
			}	
		}

	}


	//--------------------------------------------------------------------------
	//- GFX
	//--------------------------------------------------------------------------



	class KlipKlop extends FLCheckBox {
		public KlipKlop() {
			super(LABELPARTICLE, m_ShowParticleColor);
			setOpaque(false);
		}
		protected void SelectionOn() {
			m_ShowParticleColor= true;
			UpdateView();
		}
		protected void SelectionOff() {
			m_ShowParticleColor= false;
			UpdateView();
		}

	}

	public FLPanel GetSpecificPanel(){
		FLPanel p = FLPanel.NewPanel(new KlipKlop());
		p.setOpaque(false);
		return p;
	}



	//--------------------------------------------------------------------------
	//- specific
	//--------------------------------------------------------------------------


	/** mouse click on cell */
	//@override
	public final void ProcessMouseEventXY(int x, int y, MouseEvent event) {
		Macro.print( GetCellInfo(x, y) );
	}


	private String GetCellInfo(int x, int y) {
		String info = m_system.GetCellInfo(x,y);

		return info;
	}

}
