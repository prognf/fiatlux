package architecture.latticeKinesis.measuringD;


import architecture.latticeKinesis.Tile;
import experiment.measuring.general.DecimalParameterMeasurer;

public class LkinesisConcentrationMD extends LatticeKinesisMD 
implements DecimalParameterMeasurer
{

	public static final String NAME = "concentration";

	@Override
	public String GetName() {
		return NAME;
	}
	

	public double GetMeasure() {
		int Npos= m_system.GetNsize();
		int occupied=0, sum=0;
		for (int pos=0; pos < Npos ; pos++ ){
			Tile tile= m_system.GetTile(pos);
			int pop= tile.GetPop();
			if (pop>0){
				occupied++;
				sum += pop; // no renormalisation 
			}
		}
		return sum/(double)occupied; 
	}

	
}
