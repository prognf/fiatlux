package architecture.latticeKinesis.measuringD;


import architecture.latticeKinesis.Tile;
import components.types.IntCm;
import experiment.measuring.general.DecimalParameterMeasurer;

public class LkinesisTransportMD extends LatticeKinesisMD 
implements DecimalParameterMeasurer
{

	public static final String NAME = "Transport";
	
	@Override
	public String GetName() {
		return NAME;
	}


	public double GetMeasure() {
		IntCm field=  new IntCm(0, 0);
		int Npart=0;
		for (Tile tile : m_system.GetTileTab() ){
			field.Add( tile.GetLocalField() );
			Npart += tile.GetPop();
		}
		int normInf = Math.abs(field.X()) + Math.abs(field.Y()); 
		return normInf / (double)Npart;
	}

	
}
