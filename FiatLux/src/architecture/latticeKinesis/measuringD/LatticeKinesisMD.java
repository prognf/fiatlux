package architecture.latticeKinesis.measuringD;

import architecture.latticeKinesis.LatticeKinesisSampler;
import architecture.latticeKinesis.LatticeKinesisSystem;
import components.types.DoubleCouple;
import experiment.measuring.MeasuringDevice;
import experiment.measuring.general.DecimalParameterMeasurer;
import experiment.samplers.SuperSampler;

abstract public class LatticeKinesisMD extends MeasuringDevice 
implements  DecimalParameterMeasurer
{

	LatticeKinesisSystem m_system;
	
	@Override
	final public void LinkTo(SuperSampler sampler) {
		LatticeKinesisSampler samp= (LatticeKinesisSampler)sampler;
		m_system= samp.GetSystem();
	}

	public DoubleCouple GetMinMax(){
		return null;
	}


}
