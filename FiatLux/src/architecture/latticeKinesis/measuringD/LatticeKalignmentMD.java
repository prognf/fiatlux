package architecture.latticeKinesis.measuringD;


import architecture.latticeKinesis.Tile;
import components.types.IntC;
import experiment.measuring.general.DecimalParameterMeasurer;

public class LatticeKalignmentMD extends LatticeKinesisMD 
implements DecimalParameterMeasurer
{

	public static final String NAME = "Alignment";

	@Override
	public String GetName() {
		return NAME;
	}
	

	public double GetMeasure() {
		long sumprod = 0;
		int Npos= m_system.GetNsize();
		int Npart=0;
		for (int pos=0; pos < Npos ; pos++ ){ // for each particle
			Tile tile= m_system.GetTile(pos);
			Npart += tile.GetPop();
			IntC f= tile.GetLocalField();
			IntC neighbF= m_system.GetNeighbFieldPos(pos); // this is why we need pos
			int prod = f.ScalarProduct(neighbF);
			sumprod += prod;
		}
		return 0.25 *  sumprod / (double)Npart;
	}

	
}
