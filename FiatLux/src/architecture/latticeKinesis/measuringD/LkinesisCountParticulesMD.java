package architecture.latticeKinesis.measuringD;


import architecture.latticeKinesis.Tile;
import experiment.measuring.general.DecimalParameterMeasurer;

public class LkinesisCountParticulesMD extends LatticeKinesisMD 
implements DecimalParameterMeasurer
{

	public static final String NAME = "count";

	@Override
	public String GetName() {
		return NAME;
	}
	

	public double GetMeasure() {
		int Npos= m_system.GetNsize();
		int Npart=0;
		for (int pos=0; pos < Npos ; pos++ ){
			Tile tile= m_system.GetTile(pos);
			Npart += tile.GetPop();
		}
		return Npart;
	}

	
}
