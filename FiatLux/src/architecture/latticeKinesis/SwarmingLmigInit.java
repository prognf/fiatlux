package architecture.latticeKinesis;

import java.util.ArrayList;

import javax.swing.JComponent;

import components.types.DoublePar;
import components.types.FLString;
import grafix.gfxTypes.controllers.PolyChoicePanel;
import grafix.gfxTypes.elements.FLPanel;


public class SwarmingLmigInit extends LatticeKinesisInit {

	private static int NORTH=0, EAST=1, SOUTH=2, WEST=3;
	private static final double DEF_DENS= 0.2;
	private static final int TWO= 2;
	private DoublePar m_dens= new  DoublePar("dens", DEF_DENS);

	enum INITTYPE { reg, twopercell, clust, horizontal}

    //private INITTYPE m_initType;
	TypePolyChoicePanel m_initTypeChoice;

	public SwarmingLmigInit(LatticeKinesisSystem sys){
		super(sys);
		//FiatLux.SetInitPar(m_dens,"dens-ini-migr");
		//m_initType= INITTYPE.reg;
		m_initTypeChoice= new TypePolyChoicePanel();
	}

	@Override
	protected void PreInit() {
	}


	protected void SubInit() {
		if (LatticeKinesisSystem.HORIZONTALMODE){
			HorizontalInit();
		} else {

			switch(m_initTypeChoice.GetIndex()){
			case horizontal:
				HorizontalInit();
				break;
			case clust:
				tstSubInit();
				break;
			case twopercell:
				TwoParticlesPerCell();
				break;
			case reg:
				if (m_system.m_reflectingBorders){
					reflectSubInit();
				} else {
					normalSubInit();
				}
				break;
			}
		}
	}

	private void HorizontalInit() {
		m_system.InitPrepare();
		ClearAllCells();
		for (int pos=0 ; pos < m_system.GetNsize(); pos++){
			int p1= RandPart(), p2= RandPart();
			m_system.InitCellByChannels(pos,p1,p2,0,0);
		}
		/*int X= m_XYsize.X(), Y= m_XYsize.Y();
		for (int x=0; x<X; x++){
			int e= RandPart(), w= RandPart();
			int pos= m_XYsize.Map2Dto1D(x, Y/2);	
			m_system.InitCellByChannels(pos,0,e,0,w);
		}*/
		
		m_system.InitClose();

	}

	private void TwoParticlesPerCell() {
		m_system.InitPrepare();
		ClearAllCells();
		for (int pos=0 ; pos < m_system.GetNsize(); pos++){
			ArrayList<Tile> lst= m_system.m_tileTab.GetListSamePop(TWO);
			int index= RandomInt( lst.size() );
			Tile ini= lst.get(index);
			m_system.InitCellByTile(pos, ini);
		}
		m_system.InitClose();

	}

	//@Override
	protected void reflectSubInit() {
		m_system.InitPrepare();
		ClearAllCells();
		int X= m_XYsize.X(), Y= m_XYsize.Y();
		for (int y=1; y<Y-1; y++){
			for (int x=1; x<X-1; x++){
				int n = RandPart(), e= RandPart(), s= RandPart(), w= RandPart();
				w=(x==1)?0:w;
				e=(x==X-1-1)?0:e;
				s=(y==1)?0:s;
				n=(y==Y-1-1)?0:n;
				int pos= m_XYsize.Map2Dto1D(x, y);			
				m_system.InitCellByChannels(pos,n,e,s,w);
			}
		}
		m_system.InitClose();
	}

	protected void normalSubInit() {
		m_system.InitPrepare();
		ClearAllCells();
		for (int pos=0 ; pos < m_system.GetNsize(); pos++){
			int n = RandPart(), e= RandPart(), s= RandPart(), w= RandPart();
			m_system.InitCellByChannels(pos,n,e,s,w); 	
		}
		m_system.InitClose();
	}



	protected void tstSubInit() {
		m_system.InitPrepare();
		ClearAllCells();

		int X= m_XYsize.X(), Y= m_XYsize.Y();

		int ymin= Y/2, ymax= 3*Y/4;
		for (int y=ymin ; y < ymax; y++){
			for (int x=X/2 ; x < 3*X/4; x++){
				int pos= m_XYsize.Map2Dto1D(x, y);
				m_system.InitCellByChannels(pos,1,0,0,0); 	
			}
		}
		m_system.InitClose();

	}

	private int RandPart() {
		return RandomEventDouble(m_dens.GetVal()) ? 1 : 0;
	}

	//@override
	public JComponent GetSpecificPanel(){
		FLPanel p = FLPanel.NewPanel(m_dens.GetControl(), m_initTypeChoice);
		m_dens.GetControl().setOpaque(false);
		m_initTypeChoice.setOpaque(false);
		p.setOpaque(false);
		return p;
	}

	public void SetInitRate(double val) {
		m_dens.SetVal(val);
		//Macro.Debug("init rate set to : " + m_dens.GetVal());
	}

	public void InitRoseTable() {
		m_system.InitCellByChannels(0,0,0,1,1);
		m_system.InitCellByChannels(1,0,0,1,0);
		m_system.InitCellByChannels(2,0,1,1,0);
		m_system.InitCellByChannels(3,0,0,0,1);
		m_system.InitCellByChannels(4,0,0,0,0);
		m_system.InitCellByChannels(5,0,1,0,0);
		m_system.InitCellByChannels(6,1,0,0,1);
		m_system.InitCellByChannels(7,1,0,0,0);
		m_system.InitCellByChannels(8,1,1,0,0);
	}

	public double GetDini() {
		return m_dens.GetVal();
	}

	class TypePolyChoicePanel extends PolyChoicePanel {
		private static final String SINITTYPE = "IintType";

		public TypePolyChoicePanel() {
			super(SINITTYPE, 
					FLString.toStringArray( INITTYPE.values()), 0);
		}

		public INITTYPE GetIndex() {
			return INITTYPE.values()[this.m_indexChoice];
		}

		@Override
		protected void Update() {
		}
	}
	

	
}
