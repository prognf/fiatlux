package architecture.interactingParticleSystems;

import components.allCells.SuperCell;

/** this is allowed only because the update is asynchronous !! **/
public class IPScell extends SuperCell {

	int m_state;
	
	public void sig_Reset() {
		m_state=0;		
	}

	public int GetState() {
		return m_state;
	}

	public void SetState(int state) {
		m_state= state;		
	}		

}
