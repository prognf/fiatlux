package architecture.interactingParticleSystems;

import java.awt.event.MouseEvent;

import components.types.IntC;
import components.types.IntCm;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.controllers.FLCheckBox;
import grafix.gfxTypes.elements.FLList;
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.MouseInteractive;
import grafix.viewers.RegularAutomatonViewer;
import main.Macro;

/** seems more 2D ???**/
public class IPSViewerRegular extends RegularAutomatonViewer implements MouseInteractive {
	
	private static final FLColor COL0= FLColor.c_brown, COL1 = FLColor.c_yellow;
	private static final String 
		PAIRSLABEL = "show pairs",
		CLICKLABEL = "click effect: ";
	
	
	InteractingParticleSystemPlanarAbstract m_system;
	boolean m_showPairs;
	private static String [] actionOnClick= { "exchg.", "rule", "flip" };
	private static FLList m_lstOnClick= new FLList(actionOnClick);
	
	public IPSViewerRegular(IntC cellPixSize, IntC xYsize){
		super(cellPixSize, xYsize);
		Macro.print(3, " Creating IPS viewer");
		ActivateMouseFollower(this);
	}

	public void LinkTo(InteractingParticleSystemPlanarAbstract system){
		m_system= system;
	}

	@Override
	protected void DrawPostEffect(){
		if (m_showPairs){ShowPairs();}
	}

	protected void ShowPairs(){
		int X = getXsize(), Y = getYsize();
		for (int x = 0; x < X; x++) {
			for (int y = 0; y < Y; y++) {
				int x2= (x+1) % X ; 

				int state = GetCellColorNumXY(x, y);
				int stateH= GetCellColorNumXY(x2, y);

				if ((state == 1) && (stateH == 1)) {
					SetColor(COL1);
					DrawHLink(x, y);
				}

				if ((state == 0) && (stateH == 0)) {
					SetColor(COL0);
					DrawHLink(x, y);
				}

				int y2= (y-1+Y) % Y ; 
				int stateV= GetCellColorNumXY(x, y2);
				if ((state == 1) && (stateV == 1)) {
					SetColor(COL1);
					DrawVLink(x, y);
				}

				if ((state == 0) && (stateV == 0)) {
					SetColor(COL0);
					DrawVLink(x, y);
				}

			}
		}
	}

	private void DrawHLink(int x, int y) {
		int Xpos = getXpos(x);
		int Ypos = getYpos(y);
		int h = m_SquareSize / 2;
		m_g2D.fillRect(Xpos+h, Ypos+h, m_SquareSize, m_SquareSize / 8);
	}

	private void DrawVLink(int x, int y) {
		int Xpos = getXpos(x);
		int Ypos = getYpos(y);
		int h = m_SquareSize / 2;
		m_g2D.fillRect(Xpos+h, Ypos+h, m_SquareSize / 8, m_SquareSize);
	}

	@Override
	public int GetCellColorNumXY(int x, int y) {
		return m_system.ReadCellState(x, y);
	}

	private void ShowCellInfo( int pos){
		/*	IntegerList neighb1= GetNeighbGraph(pos);
		int 
		N1= GetCellState(neighb1.Get(0)),
		NW1= GetCellState(neighb1.Get(1)),
		W1= GetCellState(neighb1.Get(2)),
		E1= GetCellState(neighb1.Get(6));
	IntegerList neighb2= GetNeighbGraph(pos2);
	int N2= GetCellState(neighb2.Get(0)),
		NW2= GetCellState(neighb2.Get(1)),
		W2= GetCellState(neighb2.Get(2)),
		E2= GetCellState(neighb2.Get(6)); */
	}

	//@Override
	public void ProcessMouseEventXY(int xpos, int ypos, MouseEvent event) {
		int sel = m_lstOnClick.GetSelectedItemRank();
		switch (sel){
			case 0: ProcessPairOfCells(xpos, ypos, false); break;
			case 1: ProcessPairOfCells(xpos, ypos, true); break;
			case 2: ProcessClickOnCell(new IntC(xpos,ypos)); break;
		}
		UpdateView();
	}

	private void ProcessClickOnCell(IntC xy) {
		// int cellPos= m_system.m_topoP.GetIndexMap2D(xy);
		// m_system.m_model.ProcessClickOnCell(cellPos);
		m_system.ProcessClickOnCell(xy);
	}

	/**
	 * transforms one click into an action on a pair
	 * applyRule : applies local rule  or exchange 
	 */
	private void ProcessPairOfCells(int x, int y, boolean applyRule) {
		IntCm lastC = GetLastClickCoordfromMouseFollower();
		int D= GetCellDist();
		IntCm delta = lastC.Duplicate();
		delta.Add(- x * D - D/ 2, - (getYsize() - y - 1) * D - D/ 2);
		delta.Mult(1.7);  // multiplicating factor for finding the other cell !!!
		delta.Add(lastC);
		//Macro.Debug(" IntC " + lastC);

		IntC newXY= GetCellFromCoordinates(delta);
	
		m_system.Exchange(new IntC(x,y), newXY);
		/*//Macro.Debug(" IntC " + lastC);
		int cellPosA= m_system.m_topoP.GetIndexMap2D(x, y);
		//Macro.Debug(" new :" + newXY);
		int cellPosB= m_system.m_topoP.GetIndexMap2DAsTorus(newXY);

		if (applyRule){
			m_system.ApplyTransition(cellPosA, cellPosB);
		} else {
			m_system.Exchange(cellPosA, cellPosB);
		}*/
		
	}

	@Override
	protected boolean IsStableXY(int x, int y) {
		return false;
	}

	@Override
	public FLPanel GetSpecificPanel() {

		FLPanel p = super.GetSpecificPanel();//FLPanel.JoinLabelAndList(CLICKLABEL, m_lstOnClick, true);
		//p.Add( JoinLabelAndList(CLICKLABEL,m_lstOnClick,true) );  
		p.setOpaque(false);
		m_lstOnClick.setOpaque(false);
		return FLPanel.NewPanel(new KlipKlop(), p );
	}

	class KlipKlop extends FLCheckBox {
		public KlipKlop() {
			super(PAIRSLABEL, m_showPairs);
		}

		protected void SelectionOn() {
			m_showPairs= true;
			UpdateView();
		}

		protected void SelectionOff() {
			m_showPairs= false;
			UpdateView();
		}

	}
}