package architecture.interactingParticleSystems;

import grafix.gfxTypes.elements.FLPanel;

public class IPSparityProblemStochos extends IPSlinearSystem {

	public IPSparityProblemStochos(int size) {
		super(size);
	}

	@Override
	/** PARITY Problem rule with IPS
	 * **/
	void ApplyTransition(int pos1, int pos2) {
		int L= GetCellState(pos1-1);
		int R= GetCellState(pos2+1);

		int qA=GetCellState(pos1);
		int qB=GetCellState(pos2);
		boolean coupleEqual= (qA==L) && (qB==R);
		boolean coupleZero= (qA==0)&&(qB==0);
		if ((!coupleEqual) /*&& (!coupleZero)*/)  {//balanced or not rule
			//int bias=(m_time/500)%2;
			/*if ((qA==0)&&(qB==0)&&(m_rand.RandomEventPercent(50)))
			return;*/
			setCellState(pos1, 1-qA); 	//flip 1
			setCellState(pos2, 1-qB);	//flip 2
		}
	}

	@Override
	protected FLPanel GetSpecificPanel() {
		return m_rand.GetSeedControl();
	}



}
