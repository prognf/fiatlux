package architecture.interactingParticleSystems;

import components.arrays.OneRegisterReadable;
import experiment.measuring.planar.OneRegisterPlanarReadable;
import experiment.samplers.SuperSampler;
import initializers.SuperInitializer;

abstract public class IPSsamplerAbstract extends SuperSampler 
implements OneRegisterReadable, OneRegisterPlanarReadable {

	public IPSsamplerAbstract() {
		super();
	}

	abstract public SuperInitializer GetInitializer();

	abstract public InteractingParticleSystemPlanarAbstract GetSystem();

	abstract public String GetExpConditions();

}
