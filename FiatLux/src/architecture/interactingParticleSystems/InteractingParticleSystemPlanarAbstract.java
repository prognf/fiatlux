package architecture.interactingParticleSystems;

import components.arrays.GridSystem;
import components.types.FLsignal;
import components.types.IntC;
import components.types.Monitor;
import grafix.gfxTypes.controllers.IntController;
import grafix.gfxTypes.elements.FLPanel;
import main.Macro;

/*--------------------
*--------------------*/
abstract public class InteractingParticleSystemPlanarAbstract  
extends GridSystem implements Monitor
{
	
	public enum UPDATEPOLICY { SEQUENTIAL_STEP}

    private UPDATEPOLICY m_UpdateMode= UPDATEPOLICY.SEQUENTIAL_STEP;
	private int m_TIMESCALE = 0;

	abstract public int GetCellState(int index);
	
	abstract public int ReadCellState(int x, int y);
	
	abstract public void SetCellState(int pos, int state);
	
	abstract public void ProcessClickOnCell(IntC xy);

	abstract public void Exchange(IntC intC, IntC newXY);

	abstract void ResetEnvironment();

	abstract public FLPanel GetSpecificPanel();

	
	/** sequential update  */
	abstract protected void ApplyOneRandomUpdate();
	

	public InteractingParticleSystemPlanarAbstract(IntC XYsize) {
		super(XYsize);
		m_TIMESCALE= XYsize.prodXY();
	}
	
	/** initialisation **/
	public void sig_Init(){
		super.sig_Init();
		ResetEnvironment();
	}
	
	/** Main Method  **/
	public void sig_NextStep(){
		TimeClick();
		switch(m_UpdateMode){
		case SEQUENTIAL_STEP:
			for (int count=0; count < m_TIMESCALE; count++){
				ApplyOneRandomUpdate();				
			}
			break;
		default:
			Macro.FatalError("UpdateMode has a non-defined value !");
		}
	}
	
	@Override
	/** processing signals **/
	public void ReceiveSignal(FLsignal sig){
		switch(sig){
			case init:
				sig_Init();
				break;
			case update:
				sig_Update();
				break;
			case nextStep:
				sig_NextStep();
				break;
		}
	}
	
	final protected FLPanel GetDefaultViewControl() {
		FLPanel p = FLPanel.NewPanelVertical(new TimeScaleControl(), this.GetRandomizer().GetSeedControl());
		p.setOpaque(false);
		return p;
	}
	
	class TimeScaleControl extends IntController {
		public TimeScaleControl() {
			super("Time Scale");
		}

		@Override
		protected int ReadInt() {
			return m_TIMESCALE;
		}

		@Override
		protected void SetInt(int val) {
			m_TIMESCALE= val;			
		}

	}

	abstract public String GetSimulationInfo();

	
}
