package architecture.interactingParticleSystems;

import components.arrays.SuperSystem;
import components.arrays.TimeReadable;
import components.randomNumbers.FLRandomGenerator;
import components.types.FLsignal;
import components.types.Monitor;
import grafix.gfxTypes.elements.FLPanel;
import initializers.CAinit.OneRegisterSettableSystem;
import main.Macro;

abstract public class IPSlinearSystem  
extends SuperSystem implements Monitor,TimeReadable,OneRegisterSettableSystem {

	abstract void ApplyTransition(int pos1, int pos2);
	protected abstract FLPanel GetSpecificPanel();
	//public abstract boolean hasUniformState();

	// main
	private int [] m_array;
	int m_size;
	int m_time=0;

	final FLRandomGenerator m_rand;

	public IPSlinearSystem(int size) {
		m_size=size;
		Macro.PrintInitSignal(4, this);
		m_array = new int[m_size];

		m_rand= FLRandomGenerator.GetNewRandomizer();
		//initArray();
	}

	@Override
	public int GetTime() {
		return m_time;
	}

	@Override
	public void ReceiveSignal(FLsignal sig) {
		switch(sig){
		case init:
			//Macro.Debug("IPS1D INIT");
			m_time=0;
			//clearArray();
			break;
		case nextStep:
			m_time++;
			ApplyOneRandomUpdate();
			break;
		case update:
			//Macro.Debug("IPS1D update signal received");
			break;
		}

	}
	/*--------------------
	 * signals (dynamics)
	 --------------------*/

	private void zzzinitArray() {
		Macro.print("ips init");
		/*for (int pos=0; pos < m_size;pos++) {
			int st= m_rand.RandomInt(2);
			m_array[pos]= st;
		}*/
		//m_init.ReceiveSignal(FLsignal.init);

	}

	/** sequential update  */
	public void ApplyOneRandomUpdate(){
		int pos1= m_rand.RandomInt(m_size);
		int pos2= (pos1+1)%m_size;
		ApplyTransition(pos1,pos2);
	}



	/*--------------------
	 * Get / Set : cells 
	 --------------------*/

	//	reading the state of a cell **/
	final public int GetCellState(IPScell pos){
		return pos.GetState();
	}


	//	reading the state of a cell **/
	final public int GetCellState(int pos) {
		pos=(pos+m_size)%m_size;
		return m_array[pos];
	}

	final public void setCellState(int pos, int state){
		m_array[pos]= state;
	}

	/*--------------------
	 * Get / Set : environment
	 --------------------*/

	public int GetSize() {
		return m_size;
	}

	public void FlipCell(int x) {
		m_array[x]= 1 - m_array[x];
	}


	@Override
	public void setState(int pos, int state) {
		m_array[pos]= state;
	}

	@Override
	public int getSize() {
		return m_size;
	}


	public boolean hasUniformState() {
		int q=m_array[0];
		for (int i=1;i<m_size;i++) {
			if (m_array[i]!=q)
				return false;
		}
		return true;
	}
	
	
}
