package architecture.interactingParticleSystems;

import javax.swing.JComponent;

import components.types.IntC;
import components.types.Monitor;
import experiment.samplers.SuperSampler;
import experiment.toolbox.ConfigRecorder;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.IconManager;
import grafix.gfxTypes.elements.FLBlockPanel;
import grafix.gfxTypes.elements.FLButton;
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.AutomatonViewer;
import grafix.windows.SaveFeaturesPanel;
import initializers.SuperInitializer;
import initializers.CAinit.InitPanel;
import initializers.CAinit.InitializerEvenOdd;

/*--------------------
 * the memory of IPS is a 1D array of cells
 *--------------------*/
/** this should follow more patterns 
 * FIXME : add a model class **/
public class InteractingParticleSystemOneDSampler extends SuperSampler implements ConfigRecorder 
{
	/*--------------------
	 * attributes
	 --------------------*/

	final static String NAME = "IPSdimer";

	private SuperInitializer m_init;
	final IPSlinearSystem m_system; //contains the transition rule

	// GFX
	IPSviewerLinear m_viewer= null;

	
	public enum MODELTYPE {blncd,even};
	public final static MODELTYPE m_modeltype= MODELTYPE.blncd;
	
	/*--------------------
	 * constructor
	 --------------------*/

	/** init with GFX **/
	public InteractingParticleSystemOneDSampler(int nsize, int Tsize, IntC cellPixSize) {
		this(nsize);
		m_viewer= new IPSviewerLinear(cellPixSize, m_system, Tsize);
		addMonitor(m_viewer); // don't forget this one !
	}
	
	
	/*no GFX here **/
	public InteractingParticleSystemOneDSampler(int nsize) {
		super();
		m_system=new IPSparityProblemStochos(nsize);  

		m_init= new InitializerEvenOdd(m_modeltype);
		m_init.LinkTo(m_system, null);
		this.addMonitor(m_init);		
	}


	/*--------------------
	 * other get / set
	 --------------------*/

	public IPSlinearSystem  getSystem() {
		return m_system;
	}
	
	public String GetSimulationInfo() {
		return "model:" + NAME + " size:" +m_system.GetSize() ;
	}

	public String GetTopologyInfo(){
		return "1D-IPS"; 
	}

	@Override
	protected Monitor GetLinkedSystemAsMonitor() {
		return m_system;
	}

	@Override
	//FIXME : modelname (real)
	protected String GetModelName() {
		return "" + NAME;
	}

	@Override
	public int GetTime() {
		return m_system.GetTime();
	}

	@Override
	public FLPanel GetSimulationWidgets() {
		FLPanel simulationpanel = new FLPanel();
		simulationpanel.SetBoxLayoutY();

		FLBlockPanel initializer = new InitPanel(this, m_init.GetSpecificPanel());
		simulationpanel.Add(initializer);

		FLBlockPanel timeContainer = new FLBlockPanel();
		FLButton timeTitle = new FLButton("Simulation");
		timeTitle.setIcon(IconManager.getUIIcon("sync"));
		timeContainer.defineAsWindowBlockPanel(FLColor.c_lightgrey, timeTitle, 100, true);
		timeContainer.AddGridBagButton(timeTitle);
		timeContainer.AddGridBagY(m_system.GetSpecificPanel());
		simulationpanel.Add(timeContainer);


		// RECORD part
		SaveFeaturesPanel savePanel= new SaveFeaturesPanel(this);
		this.addMonitor(savePanel);

		return simulationpanel;
	}

	@Override
	public FLPanel GetSimulationRuleControls() {
		return null;
	}

	@Override
	public JComponent GetSimulationView() {
		return FLPanel.NewPanel(m_viewer);
	}

	@Override
	public AutomatonViewer GetViewer() {
		return m_viewer;
	}

	@Override
	public String[] GetPlotterSelection() {
		return null;
	}


	@Override
	public void io_SaveImage() {
		io_SaveImage(getFileName(), m_viewer);

	}


	@Override
	public void io_SaveState() {
		// TODO Auto-generated method stub

	}


	public int GetSize() {
		return m_system.getSize();
	}


	/*--------------------
	 * GFX
	 --------------------*/




}
