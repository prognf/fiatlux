package architecture.interactingParticleSystems;

import components.types.FLString;
import components.types.IntC;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.viewers.LineAutomatonViewer;
import main.Macro;

public class IPSviewerLinear extends LineAutomatonViewer {

	final static FLColor [] col = {
			FLColor.c_white, FLColor.c_green, FLColor.c_blue, FLColor.c_darkblue};


	private static final char[] Q_ZERO_ONE = {'0','1'};


	private IPSlinearSystem m_system;


	protected IPSviewerLinear(IntC cellPixSize, IPSlinearSystem system, int Tsize) {
		super(cellPixSize, system, system.m_size, Tsize);
		Macro.print(3, " Creating IPS viewer linear");
		m_system = system;
		PaintToolKit palette= new PaintToolKit(col);
		this.SetPalette(palette);
	}

	@Override
	public void UpdateBuffer(int[] timeBuffer) {
		//Macro.Debug("updating buffer in viewer");
		/*for (int pos = 0; pos < timeBuffer.length; pos++){
			timeBuffer[pos] = m_system.GetCellState(pos);
		}*/

		String config="";
		for (int pos = 0; pos < timeBuffer.length; pos++){
			config+= m_system.GetCellState(pos)==0?'0':'1';
		}
		String configT= transposeWithEvenOddRegions(config);
		for (int pos = 0; pos < timeBuffer.length; pos++){
			char q= configT.charAt(pos);
			timeBuffer[pos] = (q-'A');
		}
	}

	@Override
	protected void FlipCellState(int x) {
		m_system.FlipCell(x);

	}

	/** transposed coding : A (0 even zone)/ B (0 odd zone) / C (1 even zone) / D (1 odd zone) */ 
	public static String transposeWithEvenOddRegions(String config) {
		int  N= config.length();
		// find the first 0 that is after a 1
		int startX= config.indexOf('1');
		if (startX==-1) {// all-0 configuration
			char q=(N%2==0)?'A':'B'; // A:even B:odd
			return FLString.NcopiesOfChar(N, q);
		}
		int startO=config.indexOf('0', startX+1); //0 following a 1
		if (startX==-1) {// all-1 configuration
			char q=(N%2==0)?'C':'D'; // A:even B:odd
			return FLString.NcopiesOfChar(N, q);
		}
		// 
		boolean loopEnd=false;
		int pos=startO, currentlen=0;
		int qindex=0;
		String transduced="";
		//Macro.fDebug("startO:%d ", startO);
		while (!loopEnd) {
			if (config.charAt(pos % N)==Q_ZERO_ONE[qindex]) {
				pos++;
				currentlen++;
				//Macro.fDebug("++ pos:%d", pos);
			} else { // end of zone
				//Macro.fDebug("cut!, pos:%d",pos);
				boolean evenlen=(currentlen%2==0);
				char qT=(qindex==0)?
						(evenlen?'A':'B'):
							(evenlen?'C':'D');
				transduced+=FLString.NcopiesOfChar(currentlen, qT);
				//Macro.Debug("transduced :"+transduced);
				currentlen=0;
				qindex=1-qindex;
			}
			loopEnd= (pos-N==(startO+1));
		}//while
		//reordering
		String cut=transduced.substring(N-startO);
		//Macro.fDebug("Fscut:%s", cut);
		String transduced2=cut+transduced.substring(0, N-startO);
		return transduced2;
	}

	static public void main(String [] arg){
		String [] words= {"001000110010111","11100111001","0001100011"};
		for (String word : words) {
			String transduced= transposeWithEvenOddRegions(word);
			Macro.fPrint("---------------\nO:%s\nT:%s",word,transduced);
		}
	}



}
