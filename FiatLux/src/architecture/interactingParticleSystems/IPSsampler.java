package architecture.interactingParticleSystems;

import components.types.IntC;
import components.types.Monitor;
import experiment.measuring.planar.IPS2D_CorrelationMD;
import experiment.measuring.planar.IPS2D_EnergyPairsMD;
import experiment.toolbox.ConfigRecorder;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.IconManager;
import grafix.gfxTypes.elements.FLBlockPanel;
import grafix.gfxTypes.elements.FLButton;
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.AutomatonViewer;
import initializers.CAinit.InitPanel;
import initializers.IPSinit.IPSinitRegularGrid;
import initializers.IPSinit.IPSinitRegularGrid.CODE_IPS_INIT;
import main.Macro;
import main.MathMacro;
import models.IPSmodels.IPSmodel;
import models.IPSmodels.IPSmodel.CODE_IPS_MODEL;
import topology.basics.PlanarTopology;
import topology.basics.TopologyCoder;

public class IPSsampler extends IPSsamplerAbstract implements  ConfigRecorder {
	/*--------------------
	 * definition
	 --------------------*/

	public final static String[] def_PlotSelect = {
		IPS2D_CorrelationMD.NAME,
		IPS2D_EnergyPairsMD.NAME
	};

	/*--------------------
	 * attributes
	 --------------------*/

	final private InteractingParticleSystemRegular m_system;
	final private IPSmodel m_model;
	final private IPSinitRegularGrid m_initializer;
	final private PlanarTopology m_topoP, m_topoE;

	private IPSViewerRegular m_viewer; //optional

	/*--------------------
	 *  construction
	 --------------------*/


	public IPSsampler(String c_topoP, String c_topoE, IntC XYsize, CODE_IPS_MODEL modelCode){
		Macro.print("4, building ips sampler...");
		PlanarTopology topoP = (PlanarTopology)TopologyCoder.GetTopologyFromCode(c_topoP);
		PlanarTopology topoE = (PlanarTopology)TopologyCoder.GetTopologyFromCode(c_topoE);
		topoP.SetSize(XYsize);
		topoP.MakeGraph(); //useful ??
		m_topoP= topoP;
		m_topoE= topoE;

		m_model= IPSmodel.CodeToModel(modelCode,topoP);

		m_system = new InteractingParticleSystemRegular(m_model, m_topoP, topoE);
		m_model.LinkTo(m_system);

		CODE_IPS_INIT ipsInitType = m_model.GetInitializerCode(); 
		m_initializer= IPSinitRegularGrid.TypeToInitializier(ipsInitType);
		Macro.print(4," Found init:" + m_initializer.GetName());
		m_initializer.LinkTo(m_system, m_topoP);
		super.AddMonitors(m_initializer, m_model);
	}

	public void AddViewer(IntC cellPixSize) {
		m_viewer = m_model.GetViewer(cellPixSize, m_topoP.GetXYsize());
		m_viewer.LinkTo(m_system);
		//m_Viewer.SetPalette(PaintToolKit.GetDefaultPalette());
		this.addMonitor(m_viewer);		
	}

	
	/*--------------------
	 * get / Set
	 --------------------*/

	/*ZZZ public IPSinit GetInitializer() {
		return m_initializer;
	}*/

	public int GetCellState(int index) {
		return m_system.GetCellState(index);
	}

	public InteractingParticleSystemPlanarAbstract GetSystem() {
		return m_system;
	}

	public IPSmodel GetIPSModel() {
		return m_model;
	}

	public IPSinitRegularGrid GetInitializer(){
		return m_initializer;
	}
	
	
	/*--------------------
	 * implementations
	 --------------------*/

	@Override
	/* works with an association key -> object */
	public String [] GetPlotterSelection() {
		return def_PlotSelect;
	}

	@Override
	public String GetTopologyInfo() {
		return "Topo P:" + m_topoP.GetName() + " TopoE:" + m_topoE.GetName() ;
	}

	@Override
	public String GetSimulationInfo() {
		return 
				m_system.GetSimulationInfo() 
				+ " TopoP:" + m_topoP.GetName() + " TopoE:" + m_topoE.GetName() + 
				 GetExpConditions() + " model:" + m_model.GetName();
	}

	@Override
	public String GetExpConditions() {
		return " L: " + m_topoP.GetSize();
	}
	
	@Override
	public int GetTime() {
		return m_system.GetTime();
	}

	public int GetSize() {
		return m_system.GetNsize();
	}

	@Override
	public FLPanel GetSimulationWidgets() {
		FLPanel simulationpanel = new FLPanel();
		simulationpanel.SetBoxLayoutY();

		FLBlockPanel initializer = new InitPanel(this, m_initializer.GetSpecificPanel());
		simulationpanel.Add(initializer);

		FLBlockPanel timeContainer = new FLBlockPanel();
		FLButton timeTitle = new FLButton("Simulation");
		timeTitle.setIcon(IconManager.getUIIcon("sync"));
		timeContainer.defineAsWindowBlockPanel(FLColor.c_lightgrey, timeTitle, 100, true);
		timeContainer.AddGridBagButton(timeTitle);
		timeContainer.AddGridBagY(m_system.GetSpecificPanel());
		simulationpanel.Add(timeContainer);

		return simulationpanel;
	}

	@Override
	public FLPanel GetSimulationRuleControls() {
		FLPanel p = m_model.GetViewControl();
		p.setOpaque(false);
		return p;
	}

	@Override
	public FLPanel GetSimulationView() {
		return FLPanel.NewPanel(m_viewer);
	}

	@Override
	public AutomatonViewer GetViewer() {
		return m_viewer;
	}

	@Override
	protected String GetModelName() {
		return m_model.GetName();
	}

	@Override
	/** for saving images **/
	public void io_SaveImage() {
		String filename = getFileName();
		io_SaveImage(filename, m_viewer);
	}

	@Override
	public void io_SaveState() {
		Macro.SystemWarning("Not implemented yet");
	}

	@Override
	protected Monitor GetLinkedSystemAsMonitor() {
		return m_system;
	}

	@Override
	public int GetState(int pos) {
		return m_system.GetCellState(pos);
	}

	
	/*--------------------
	 * OneRegisterPlanarReadable
	 --------------------*/

	@Override
	public int GetSizeX() {
		return m_topoP.GetXsize();
	}

	@Override
	public int GetSizeY() {
		return m_topoP.GetYsize();
	}

	@Override
	public int GetStateXY(int x, int y) {
		int pos= MathMacro.Map2Dto1D(x, y, GetSizeX(), GetSizeY());
		return m_system.GetCellState(pos);
	}



}
