package architecture.interactingParticleSystems;

import components.types.IntC;
import components.types.ProbaPar;
import grafix.gfxTypes.elements.FLPanel;
import main.Macro;

/*
 * memory = 2D array of cells
 */
public class InteractingParticleSystemsBimodal 
extends InteractingParticleSystemPlanarAbstract
{
	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();

	public static final String NAME = "bimodalIPS";

	ProbaPar m_p11= new ProbaPar("p11",.1);
	ProbaPar m_pDiag= new ProbaPar("pDiag",.1);
	ProbaPar m_pMaj= new ProbaPar("pMaj",.0);

	//--------------------------------------------------------------------------
	//- attributes
	//--------------------------------------------------------------------------

	protected IPScell[][] m_Array;
	int m_X, m_Y;

	//--------------------------------------------------------------------------
	//- construction
	//--------------------------------------------------------------------------

	public InteractingParticleSystemsBimodal(IntC XYsize) {
		super(XYsize);
		m_X= XYsize.X();
		m_Y= XYsize.Y();
		InitArray();
	}

	public IPScell GetCell(int x, int y) {
		return m_Array[x][y];
	}

	/** creates the grid */
	private void InitArray() {
		Macro.PrintInitSignal(4, this);
		m_Array = new IPScell[m_X][m_Y];
		for (int y = 0; y < m_Y; y++) {
			for (int x = 0; x < m_X; x++) {
				m_Array[x][y] = GetNewEnvCell();
			}
		}	// DO NOT FORGET TO set m_cellCouple later !!
	}


	/** creating cells specific to the system **/
	public IPScell GetNewEnvCell(){
		return new IPScell();
	}
	//--------------------------------------------------------------------------
	//- Set Param
	//--------------------------------------------------------------------------

	/** setting param lambda_1,1 **/
	public void SetParValues(double lambda, double chi, double epsilon){
		Macro.fPrint("Setting value to lambda:%.2f chi%.2f eps:%.4f" ,lambda,chi,epsilon);
		m_p11.SetVal(lambda);
		m_pDiag.SetVal(chi);
		m_pMaj.SetVal(epsilon);
	}

	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------

	@Override
	protected void ResetEnvironment() {
		for (int y = 0; y < m_Y; y++) {
			for (int x = 0; x < m_X; x++) {
				GetCell(x,y).sig_Reset();
			}
		}
	}

	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------

	final protected void ApplyOneRandomUpdate() {
		RandomUpdateForExchange();
	}

	private void RandomUpdateForExchange() {
		//Macro.Debug("current seed system: "+ GetSeed());
		int x= RandomInt(m_X);
		int y= RandomInt(m_Y);
		int x2=-1, y2=-1;
		int intType= RandomInt(4);
		switch(intType){
		case 0: // H interaction
			x2= (x+1) % m_X;
			y2= y;
			break;
		case 1: // V interaction
			x2= x;
			y2= (y+1) % m_Y;
			break;
		case 2: // Dp interaction
			x2= (x+1) % m_X;
			y2= (y+1) % m_Y;
			break;
		case 3:
			x2= (x+1) % m_X;
			y2= (y-1+m_Y) % m_Y;
			break;
		}
		IPScell cA= GetCell(x,y), cB= GetCell(x2, y2);
		int 
		sA= cA.GetState(), 
		sB= cB.GetState();
		if (RandomEventDouble(m_pMaj)){
			if (RandomInt(2)==0){
				ApplyMooreMaj(cA,x,y);
			} else {
				ApplyMooreMaj(cB,x2,y2);
			}
		} else if (sA != sB){ // not same state
			boolean exchange;
			if ((intType==0) || (intType==1)){ // H/V
				int 
				iA= CountSameAs(x,y,sA),
				iB= CountSameAs(x2,y2,sB);
				exchange= ApplyRuleHV(iA, iB);
			} else { // diag
				exchange= ApplyRuleDiag();
			}
			if (exchange){ 	// exchanging two cell states  
				m_Array[x][y]= cB;
				m_Array[x2][y2]= cA;
			}
		}
	}


	private void ApplyMooreMaj(IPScell c, int x, int y) {
		int sum=0;
		for (int dy=-1;dy<=1;dy++){
			for (int dx=-1;dx<=1;dx++){
				sum+= GetCellStateDelta(x, y, dx, dy);
			}
		}
		int newState= (sum>4)?1:0;
		c.SetState(newState);
	}

	private boolean ApplyRuleHV(int iA, int iB) {
		//return (iA>=1) && (iB>=1);
		return DifferentiatedRule(iA,iB);
	}

	private boolean DifferentiatedRule(int iA, int iB) {
		boolean r1=	(iA>=1) && (iB>=1) &&
				((iA+iB>2) || RandomEventDouble(m_p11) );
		/*		boolean r2= (CaseEq(iA,iB,1) && RandomEventDouble(mF_p11.GetVal()) )
				|| 	Case(iA,iB,1,2)||  Case(iA,iB,1,3)
				|| 	CaseEq(iA,iB,2)|| Case(iA,iB,2,3)|| 	CaseEq(iA,iB,3);*/
		return r1;
	}

	private boolean ApplyRuleDiag() {
		return RandomEventDouble(m_pDiag);
		//return (iA==4) || (iB==4);
		//return Case(iA,iB,4,0); 
	}

	private boolean CaseEq(int iA, int iB, int A) {
		return ((iA==A) && (iB==A));
	}

	private boolean Case(int iA, int iB, int A, int B) {
		return ((iA==A) && (iB==B)) || ((iA==B) && (iB==A));
	}

	/** assumes binary state **/
	private int CountSameAs(int x, int y, int sA) {
		int s=
				GetCellStateDelta(x,y,0,1)+
				GetCellStateDelta(x,y,1,0)+
				GetCellStateDelta(x,y,0,-1)+
				GetCellStateDelta(x,y,-1,0);
		return (sA==0)?4-s:s;
	}

	public String GetExpConditions() {
		return String.format("XY:%s lambda:%s chi:%s eps:%s", 
				GetXYsize(), m_p11, m_pDiag, m_pMaj);
	}
	
	private int GetCellStateDelta(int x, int y, int dx, int dy) {
		int xx= (x + dx + m_X) % m_X, yy= (y + dy + m_Y) % m_Y; 
		return GetCell(xx, yy).GetState();
	}

	public int GetCellState(int pos) {
		int x= pos % m_X;
		int y= pos / m_Y;
		return GetCell(x,y).GetState();
	}

	@Override
	public int ReadCellState(int x, int y) {
		return GetCell(x, y).GetState();
	}

	@Override
	public void ProcessClickOnCell(IntC xy) {
		int x= xy.X(), y= xy.Y();
		int s= GetCell(x, y).GetState();
		SetCellStateXY(x, y, 1-s);

	}

	public void SetCellStateXY(int x, int y, int s) {
		m_Array[x][y].SetState(s);
	}

	/** mapping 1D -> 2D !**/
	public void SetCellState(int pos, int s) {
		int x= pos % GetXYsize().X();
		int y= pos / GetXYsize().X();
		SetCellStateXY(x,y,s);
	}

	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------


	@Override
	public void Exchange(IntC cellposA, IntC cellposB) {
		IPScell tmp= m_Array[cellposA.X()][cellposA.Y()];
		m_Array[cellposA.X()][cellposA.Y()]= m_Array[cellposB.X()][cellposB.Y()];
		m_Array[cellposB.X()][cellposB.Y()]= tmp;
	}

	@Override
	public String GetSimulationInfo() {
		return "model:" + "BIMODAL" + " time:" + GetTime() ;
	}


	//--------------------------------------------------------------------------
	//- GFX
	//--------------------------------------------------------------------------

	@Override
	public FLPanel GetSpecificPanel() {
		FLPanel p = super.GetDefaultViewControl();
		FLPanel sub = FLPanel.NewPanelVertical(m_p11.GetControl(), m_pDiag.GetControl(), m_pMaj.GetControl());
		p.Add(sub);
		sub.setOpaque(false);
		p.setOpaque(false);
		return p;
	}
}


