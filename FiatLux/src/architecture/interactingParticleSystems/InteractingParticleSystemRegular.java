package architecture.interactingParticleSystems;

import java.util.ArrayList;

import components.types.IntC;
import components.types.IntegerList;
import grafix.gfxTypes.elements.FLPanel;
import main.Macro;
import models.IPSmodels.IPSmodel;
import topology.basics.PlanarTopology;

/*--------------------
 * the memory of IPS is a 1D array of cells
 * TODO : what are the "links" ???
 *--------------------*/
public class InteractingParticleSystemRegular  
extends InteractingParticleSystemPlanarAbstract
{


	/*--------------------
	 * definition
	 --------------------*/

	/*--------------------
	 * attributes
	 --------------------*/

	final static String NAME = "IPS";


	// main
	private IPScell[] m_Array; // ENVIRONMENT DO NOT MODIFY (optimisations needed !)

	protected FLPanel m_Controler = null; // associated View/Control

		public ArrayList<IntC> m_cellCouple; // for interactions  // TODO from indices to cells (for speed up) ???
	IntegerList[] m_NeighbGraph; // for exchanging cells
	IPSmodel m_model;

	// protected -> used by the viewer for detecting position of clicked cell
	protected PlanarTopology m_topoP; // perception topology
	private PlanarTopology m_topoE;   // exchange links topology

	/** FIXME : curious the size is in m_topoP **/  
	//int m_X, m_Y;

	private boolean [] m_CellTagBorder; // is a cell on the border ?

	final private boolean m_IsModelNonConservative; // can particle disappear ?

	
	/*--------------------
	 * main
	 --------------------*/

	/** applying rule in cell pos1 and pos2 **/
	public void ApplyRule(int pos1, int pos2) {
		// STEP A : exchange ?
		if (m_model.IsChangeApplied(pos1,pos2)){
			Exchange(pos1,pos2);	
		}
		// STEP B : change cell state
		if (m_IsModelNonConservative){
			ChangeStateAttempt(pos1);
			ChangeStateAttempt(pos2);
		}
	}
	
	/*--------------------
	 * constructor
	 --------------------*/

	public InteractingParticleSystemRegular(IPSmodel in_model,
			PlanarTopology topoPerception, PlanarTopology topoLinks) {
		super(topoPerception.GetXYsize());
		m_model= in_model;
		m_IsModelNonConservative= in_model.IsModelNonConservative();		

		m_topoP= topoPerception;
		m_topoE= topoLinks;
		m_NeighbGraph= m_topoP.GetTopologyGraph();

		createArray();

		// WARNING : m_cellCouple will be built LATER !!!
		m_CellTagBorder= new boolean[GetNsize()]; 
	}

	/** creates the grid */
	public void createArray() {
		Macro.PrintInitSignal(4, this);
		m_Array = new IPScell[GetNsize()];
		for (int pos = 0; pos < GetNsize(); pos++) {
			m_Array[pos] =  new IPScell();
		}
		// DO NOT FORGET TO set m_cellCouple later !!
	}

	/** SETTING LINKS WITH NON BORDER CELLS 
	 * should be called only after initialisation !!
	 * @field m_topoLinks defines the way cells are linked, 
	 * which can differ from the "perception" topology **/
	public void ConstructExchangeLinks() {
		Macro.print(4,"IPS : constructing cell exchange links");
		IntC XYsize= m_topoP.GetXYsize();
		m_topoE.SetSize(XYsize); 
		m_topoE.MakeGraph();
		ArrayList<IntC> links= new ArrayList<IntC>();

		for (int pos=0; pos< GetNsize(); pos++ ){
			if (!m_CellTagBorder[pos]){			// we consider only non-border cells
				IntegerList neighbours= m_topoE.GetNeighbourList(pos);
				for (int neighbIndex : neighbours){
					// NON SYMMETRY IS REMOVED !!
					/*if ( 	(neighbIndex < pos) &&// to avoid entering couples twice 
							(!m_CellTagBorder[neighbIndex]) )
					 */
					{  
						links.add( new IntC(neighbIndex, pos));
					}
				}
			}
		}
		// sets the array list
		m_cellCouple= links;
	}

	/*--------------------
	 * signals (dynamics)
	 --------------------*/

	/** sequential update  */
	public void ApplyOneRandomUpdate(){
		int size = m_cellCouple.size(); 
		int couple= RandomInt(size); 
		IntC c1c2= m_cellCouple.get(couple);
		int pos1= c1c2.X(), pos2= c1c2.Y();
		ApplyRule(pos1,pos2);
	}

	/** exchanging two cell states **/ 
	public void Exchange(int pos1, int pos2) {
		IPScell tmp= m_Array[pos1];
		m_Array[pos1]= m_Array[pos2];
		m_Array[pos2]= tmp;	
	}

	/** used by viewer for particular updates **/
	public void ApplyTransition(int cellPosA, int cellPosB) {
		ApplyRule(cellPosA, cellPosB);
	}


	private void ChangeStateAttempt(int pos) {
		if (m_model.IsStateChanged(pos)){
			m_Array[pos].SetState( 1 - m_Array[pos].GetState());
		}		
	}


	protected void ResetEnvironment() {
		//Macro.Debug("Reset Env");
		for (IPScell cell: m_Array){
			cell.sig_Reset();			
		}		
	}

	/*--------------------
	 * counting 
	 --------------------*/

	public int CountNumberState(int state) {
		int count = 0;
		for (IPScell cell: m_Array){
			if (cell.m_state == state){
				count++;
			}
		}	
		return count;
	}



	/** counts the number of occurrences of a state in the neighb **/
	/*public final int GetNeighbCountState(int state){
		int count=0;
		for (int pos : neighb1){
			sum += m_Array[pos].GetState();
		}
	}*/


	/*--------------------
	 * Get / Set : cells 
	 --------------------*/

	/** used by initializer : sets a cell as border cell **/
	public void TagBorderCell(int x, int y) {
		int pos= m_topoP.GetIndexMap2D(x, y);
		m_CellTagBorder[pos]= true;		
	}

	//	reading the state of a cell **/
	final public int GetCellState(IPScell pos){
		return pos.GetState();
	}


	//	reading the state of a cell **/
	final public int GetCellState(int pos) {
		try {
			return m_Array[pos].GetState();
		} catch (Exception e) {
			e.printStackTrace();
			Macro.FatalError("  pos " + pos);
			return -1;
		}
	}

	/** setting the state of a cell **/
	final public void SetCellState(int pos, int state) {		
		m_Array[pos].SetState(state);
	}

	public int ReadCellState(int x, int y) {
		int pos= m_topoP.GetIndexMap2D(x, y);
		return m_Array[pos].GetState();
	}	

	public void InitCellState(int x, int y, int state) {
		int pos= m_topoP.GetIndexMap2D(x, y);
		m_Array[pos].SetState(state);
	}

	/*--------------------
	 * Get / Set : environment
	 --------------------*/

	public int GetSize() {
		return m_Array.length;
	}

	/** FIXME : curious the size is in m_topoP **/  
	public IntC GetXYsize() {
		return m_topoP.GetXYsize();
	}

	public String GetAllStates() {
		StringBuilder str= new StringBuilder("[");
		for (IPScell cell: m_Array){
			str.append("::").append(cell.GetState());
		}
		str.append("]");
		return str.toString();
	}


	public IntegerList GetNearBorderCellIndices() {
		IntegerList cellList= new IntegerList();

		int szX =m_topoP.GetXYsize().X(), szY =m_topoP.GetXYsize().Y();

		for (int x=1; x < szX-1; x++){
			AddCell(cellList,x,1);
			AddCell(cellList,x,szY-2);			
		}

		for (int y=2; y < szX-2; y++){
			AddCell(cellList,1,y);
			AddCell(cellList,szX-2,y);			
		}	
		//Macro.Debug("I have constructed a list of near-border cells of size : "+ cellList.GetSize());

		return cellList;
	}

	private void AddCell(IntegerList cellList, int x, int y) {
		int pos= m_topoP.GetIndexMap2D(x, y);
		cellList.addVal(pos);
	}

	/** used for computing exchange rates **/
	final public IntegerList GetNeighbGraph(int pos) {
		return m_NeighbGraph[pos];
	}
	/*--------------------
	 * other get / set
	 --------------------*/

	public String GetSimulationInfo() {
		return "model:" + m_model.GetName() + " time:" + GetTime() ;
	}

	public String GetTopologyInfo(){
		return "" + m_topoP.toString(); 
	}



	/*--------------------
	 * GFX
	 --------------------*/

	@Override
	public FLPanel GetSpecificPanel() {
		return super.GetDefaultViewControl();
	}

	
	@Override
	public void ProcessClickOnCell(IntC xy) {
		
	}

	@Override
	public void Exchange(IntC intC, IntC newXY) {
	}


}
