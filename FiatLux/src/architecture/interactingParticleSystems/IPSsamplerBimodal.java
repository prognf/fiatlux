package architecture.interactingParticleSystems;

import components.types.IntC;
import components.types.Monitor;
import experiment.measuring.planar.DensityIpsMD;
import experiment.measuring.planar.IPS2D_CorrelationMD;
import experiment.measuring.planar.IPS2D_EnergyPairsMD;
import experiment.measuring.planar.OneRegisterPlanarReadable;
import experiment.measuring.planar.PairEnergyMD;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.IconManager;
import grafix.gfxTypes.elements.FLBlockPanel;
import grafix.gfxTypes.elements.FLButton;
import grafix.gfxTypes.elements.FLPanel;
import initializers.SuperInitializer;
import initializers.CAinit.InitPanel;
import initializers.IPSinit.IPSinitSimple;
import main.Macro;
import main.MathMacro;
import models.IPSmodels.IPSmodel;

public class IPSsamplerBimodal extends IPSsamplerAbstract implements OneRegisterPlanarReadable {
	/*--------------------
	 * definition
	 --------------------*/

	public final static String[] def_PlotSelect = {
		DensityIpsMD.NAME,
		PairEnergyMD.NAME,
		IPS2D_CorrelationMD.NAME,
		IPS2D_EnergyPairsMD.NAME,
	};

	/*--------------------
	 * attributes
	 --------------------*/

	final private InteractingParticleSystemsBimodal m_system;
	final private IPSinitSimple m_initializer;
	private IPSViewerRegular m_viewer; //optional

	/*--------------------
	 *  construction
	 --------------------*/

	/** special "PhaseSync experiment init + viewer **/
	public IPSsamplerBimodal(IntC XYsize, IntC XYpix){
		this(XYsize);
		m_viewer= IPSmodel.GetDefaultViewer(XYpix, XYsize);
		m_viewer.LinkTo(m_system);
		this.addMonitor(m_viewer);
	}

	/** init**/
	public IPSsamplerBimodal(IntC XYsize) {
		m_system = new InteractingParticleSystemsBimodal(XYsize);
		// initializer
		m_initializer = new IPSinitSimple();
		Macro.print(4," Found init:" + m_initializer.GetName());
		m_initializer.LinkTo(m_system,null);
		this.addMonitor(m_initializer);
	}
	

	/*--------------------
	 * get / Set
	 --------------------*/

	/*--------------------
	 * implementations
	 --------------------*/

	@Override
	/* works with an association key -> object */
	public String [] GetPlotterSelection() {
		return def_PlotSelect;
	}

	@Override
	public String GetTopologyInfo() {
		return " :: " + GetSizeX()+"," + GetSizeY()+ "::" ;
	}

	@Override
	public String GetSimulationInfo() {
		String strInfo= "(simulation info)";
		return strInfo;
	}

	@Override
	public int GetTime() {
		return m_system.GetTime();
	}

	public int GetSize() {
		return m_system.GetNsize();
	}

	@Override
	public FLPanel GetSimulationWidgets() {
		FLPanel simulationpanel = new FLPanel();
		simulationpanel.SetBoxLayoutY();

		simulationpanel.Add(new InitPanel(this, m_initializer.GetSpecificPanel()));

		FLPanel timeSub = m_system.GetSpecificPanel();
		FLBlockPanel timeContainer = new FLBlockPanel();
		FLButton timeTitle = new FLButton("Simulation");
		timeTitle.setIcon(IconManager.getUIIcon("sync"));
		timeContainer.defineAsWindowBlockPanel(FLColor.c_lightgrey, timeTitle, 100, true);
		timeContainer.AddGridBagButton(timeTitle);
		timeContainer.AddGridBagY(timeSub);
		timeContainer.adaptSize(timeSub);
		simulationpanel.Add(timeContainer);

		return simulationpanel;
	}

	@Override
	public FLPanel GetSimulationRuleControls() {
		return null;
	}

	@Override
	public FLPanel GetSimulationView() {
		return FLPanel.NewPanel(m_viewer);
	}

	@Override
	protected String GetModelName() {
		return InteractingParticleSystemsBimodal.NAME;
	}

	protected Monitor GetLinkedSystemAsMonitor() {
		return m_system;
	}

	public int GetState(int pos) {
		return m_system.GetCellState(pos);
	}
	
	@Override
	public int GetStateXY(int x, int y) {
		int pos= MathMacro.Map2Dto1D(x, y, m_system.m_X, m_system.m_Y);
		return m_system.GetCellState(pos);
	}

	@Override
	public int GetSizeX() {
		return m_system.m_X;
	}

	@Override
	public int GetSizeY() {
		return m_system.m_Y;
	}

	@Override
	public SuperInitializer GetInitializer() {
		return m_initializer;
	}

	@Override
	public InteractingParticleSystemPlanarAbstract GetSystem() {
		return m_system;
	}
	
	public IPSViewerRegular GetViewer(){
		return m_viewer;
	}

	@Override
	public String GetExpConditions() {
		return m_system.GetExpConditions();
	}
}
