package architecture.multiAgent.tools;

import java.util.ArrayList;

import architecture.multiAgent.tools.MultiAgentSystem.EXCLUSIONMODE;
import components.types.FLStringList;
import components.types.FLsignal;
import components.types.IVector;
import components.types.IntC;
import components.types.IntCm;
import grafix.gfxTypes.elements.FLPanel;
import main.Macro;
import models.MASmodels.turmite.TurmiteSystem;
import models.MASmodels.turmite.TurmiteSystemModel1;
import models.MASmodels.turmite.TurmitesInit;

public class TurmiteCycleDetect {

	enum INITTYPE {North2, East2, Line, SquareN, SquareRot, NorthWest}

    final static INITTYPE initType= INITTYPE.NorthWest;
	final static int MAXTIME=150000;
	final static int L= 400;
	final static IntC Size= new IntC(L,L);
	final static int BEG=1, END= 51, DPAR=1;
	//final static int BEG=1, END= 14, DPAR=2;
	private static final int SPEEDUP = 147; // testing cycles every n-th step
	private static final EXCLUSIONMODE exclmode = EXCLUSIONMODE.ALLOW_POLICY;
	private static final boolean PRINT = false;
	final static int EGSthreshold=10; // ad hoc ratio for detecting ever-growing squares

	TurmiteSystem m_sys;
	AdHocInit m_init;
	StateList m_sysOrbit;
	//boolean m_cycleFound= false; // for cycle analysis
	IntCm m_XYmin, m_XYmax; // bounding box	
	TranslationList m_DXY; // list of translations

	class StateList extends ArrayList<StateSample> implements ComparableElementsList{

		@Override
		public boolean LastEquals(int pos) {
			return AreEqual(size() - 1, pos);
		}

		@Override
		public boolean AreEqual(int pos1, int pos2) {
			StateSample e1= get( pos1 ), e2= get( pos2); 
			return e1.IsEqualTo(e2);
		}

	}

	class TranslationList  extends ArrayList<TranslationSample> implements ComparableElementsList {

		@Override
		public boolean add(TranslationSample e){
			return super.add(e);
		}
		@Override
		public boolean LastEquals(int pos) {
			return AreEqual(size() - 1, pos);
		}

		@Override
		public boolean AreEqual(int pos1, int pos2) {
			TranslationSample e1= get( pos1 ), e2= get( pos2);
			//Macro.DebugNOCR("@" + e1.m_Tcode + "=?=" + e2.m_Tcode);
			return e1.IsEqualTo(e2);
		}

		public TranslationSample GetLast() {
			return get(size() - 1 );
		}

	}

	public void DoExperiment(){
		FLStringList output= new FLStringList();
		for (int dx=BEG; dx <=END; dx+=DPAR){
			String oneExp = DoOneExperiment(dx);
			output.Add(oneExp);
		}
		output.WriteToFile(exclmode + "-DX-" + initType + "CycleAnalysis.dat");

	}	

	public String DoOneExperiment(int dx){

		m_sys =new TurmiteSystemModel1(Size);
		m_sys.SetExclusionMode(exclmode);
		m_init = new AdHocInit(m_sys);
		m_init.SetDX(dx);
		m_init.ReceiveSignal(FLsignal.init);
		m_sysOrbit= new StateList();
		m_DXY = new TranslationList(); 
		ReinitializeBoundingBox();
		StateSample config = new StateSample(m_sys);
		PushConfiguration(config);	
		//AddEmptyTranslation();
		CycleData cycleInfo= null;
		int time = m_sys.GetTime();
		if (PRINT) PrintConfigInfo(time, config);
		while ((cycleInfo==null) && time < MAXTIME){
			m_sys.sig_NextStep();
			time = m_sys.GetTime();
			config = new StateSample(m_sys);
			PushConfiguration(config);
			ComputeDXY(config,time);

			//outof bounds detection
			cycleInfo=AnalyseOutOfBounds();
			if (cycleInfo == null){
				if (time % SPEEDUP == 0){ 
					cycleInfo= SystemAnalysis(); // non-null means cycle was found
				}
			}
			// info
			if (PRINT){
				PrintConfigInfo(time,config);
			}
			TranslationSample dxyarray= m_DXY.GetLast();
			int dircode = dxyarray.m_Tcode;
			//dxyarray.ToString() +
			//Macro.DebugNOCR("T(" + dircode + ")T");
			//if (time % 1 == 0){
			//	Macro.CR(); Macro.CR();
			//}
		} // run is finished

		String out = " dx " + dx + " ";
		if (cycleInfo==null) {
			out+= " no detection for MAXTIME=" + MAXTIME ;
		} else{
			out += cycleInfo.toString();
		}
		Macro.print( out );
		return out;
	}

	/*private void AddEmptyTranslation() {
		m_DXY.add();		
	}*/

	private static int ToDircode(IntC[] dXYarray) {
		int size = dXYarray.length;
		if (size > 8){
			Macro.FatalError("DXY analysis : only up to eight agents are allowed !");
		}
		int code= 0;
        for (IntC aDXYarray : dXYarray) {
            code *= 10;
            code += ((aDXYarray.X() + 1) + 2 * (aDXYarray.Y() + 1));
        }
		return code;
	}

	private void PrintConfigInfo(int time, StateSample config) {
		String s = "t:" + time + " " +  
		config.ToString() + " |" +
		m_XYmin.ToString() + 
		":" + m_XYmax.ToString() + " nz:" + m_sys.CountNonZeroCells();
		Macro.print(s);
	}

	private void ComputeDXY(StateSample config, int time) {
		TranslationSample dxy = new TranslationSample( config, GetConfig(time -1)); 
		m_DXY.add(dxy);
	}

	/* resets the bounding box */
	private void ReinitializeBoundingBox() {
		m_XYmin= IntCm.WidestInterval();
		m_XYmax= IntCm.WidestInterval();	
	}

	interface ComparableElementsList {
		int size();
		boolean LastEquals(int pos);
		boolean AreEqual(int time, int t);
	}


	/** 
	 * looks for cycles  
	 * **/
	@SuppressWarnings("unused")
	final public CycleData SystemAnalysis(){
		ComparableElementsList targetlist= m_sysOrbit;// m_DXY;
		CycleData cycleST= FindCyle(targetlist);
		if ((cycleST != null) && PRINT) Macro.print( "PREV: " + cycleST.m_previousList );
		return cycleST;
	}


	private CycleData FindCyle(ComparableElementsList targetlist) {
		CycleData cycleST= null;
		int time= targetlist.size() - 1;
		int from = time -1;
		int previous= FindPrevious(targetlist, from);
		//Macro.DebugNOCR("time : " + time + " P:" + previous +" ~ " );	
		String previousList="";					
		while (previous >= (time/2) && (cycleST== null)) { 
			// does not enter here if no predecessor is found (-1) or if there is no chance for finding a cycle (<time/2)
			previousList += " <" + previous ;
			int len= HowMuchCycle(time, previous, targetlist);
			//Macro.DebugNOCR("<"+ len + ">");
			previousList += "[" + len + "] ";
			if (len >= time - previous){ // cycle found
				//Macro.Debug("cycle found");
				int startCycle= previous - len +1;
				int cycleLen= time - previous;
				IntCm bbXY = DifferenceWith(m_XYmax, m_XYmin); // bounding box
				bbXY.Add(1,1); 
				IntC [] diffXY= CycleData.GetTranslationVector( GetConfig(time), GetConfig(previous) );
				int nZero= m_sys.CountNonZeroCells();
				cycleST= new CycleData("cyc", startCycle, cycleLen, bbXY, nZero, diffXY, previousList);
			}
			int fromIndex= previous - 1;
			previous = FindPrevious(m_DXY, fromIndex);
		}
		return cycleST;
	}

	
	
	/** creates a new instance which coordinates 
	 * are subtracted to the current instance */
	static public IntCm DifferenceWith(IntC valA, IntC valB) {
		return new IntCm(valA.X() - valB.X(), valA.Y() - valB.Y());
	}
	
	private CycleData AnalyseOutOfBounds() {
		//if (m_sys.GetTime() > 17600)
		//	Macro.Debug("Min: " + m_XYmin + " Max: " + m_XYmax + BoundsHaveBeenReached());
		// out of bounds ? WE assume that turmites are initially in the MIDDLE of the grid (offset)	
		if ( BoundsHaveBeenReached() ){
			IVector counts= m_sys.CountStateCells();
			int ratio = counts.GetVal(2) / counts.GetVal(1);
			CycleData cycle= FindCyle(m_DXY);
			//Macro.Debug(" cycle found : " + cycle);

			if (cycle==null){
				if ( ratio > EGSthreshold ){
					return CycleData.EGS;
				} else {
					return CycleData.PathEscape;
				}
			} else {			
				if (cycle.m_cycleLen == 104){//translation
					cycle.m_type= "Path?";
				} else {
					cycle.m_type= "Glider?";
				}
				return cycle;
			} 
		}	else {
			return null;
		}
	}


	private boolean BoundsHaveBeenReached() {
		return (m_XYmax.X() >= m_sys.GetXsize() - 1) || 
		(m_XYmax.Y() >= m_sys.GetYsize() - 1) || 
		(m_XYmin.X() <= 0) || 
		(m_XYmax.Y() <= 0); 
	}

	private void PushConfiguration(StateSample config) {
		m_sysOrbit.add(config);

		for (SituatedAgent agent : m_sys.GetAgentList()){
			IntC XYcurrent= agent.GetPosition().GetXY();
			m_XYmin.CompareAndUpdateToMin(XYcurrent);
			m_XYmax.CompareAndUpdateToMax(XYcurrent);
		}


	}




	private int FindPrevious(ComparableElementsList data, int fromIndex){
		int t= fromIndex;
		int time= data.size() - 1;
		boolean search= true;
		while(search && t>=0){
			try{
				if ( data.AreEqual(time, t) ){
					search =false;
				} else {
					t--;
				}
			} catch (Exception e){
				Macro.FatalError(" fatal t " + t + " / " + time);
			}
		}
		return t;
	}

	
	/** TODO : put this a separate class ??? ***/
	class AdHocInit extends TurmitesInit {

		int m_par;

		public AdHocInit(TurmiteSystem in_system) {
			super(in_system);
		}

		@Override
		public FLPanel GetSpecificPanel() {
			return null;
		}

		@Override
		public void SubInit() {

			SetOffsetInTheMiddle();
			switch(initType){
			case North2:
				CreateAgents(2);
				PutAgentOffset(0, 0, 0, DIR.NORTH);
				PutAgentOffset(1, m_par, 0, DIR.NORTH);
				break;
			case East2:
				CreateAgents(2);
				PutAgentOffset(0, 0, 0, DIR.EAST);
				PutAgentOffset(1, m_par, 0, DIR.EAST);
				break;
			case NorthWest:
				CreateAgents(2);
				PutAgentOffset(0, 0, 0, DIR.NORTH);
				PutAgentOffset(1, m_par, 0, DIR.WEST);
				break;
			case Line:
				CreateAgents(m_par);
				for (int a=0; a< m_par; a++){
					PutAgentOffset(a, a, 0, DIR.NORTH);
				}
				break;
			case SquareN:
				CreateAgents(4);
				DIR dir = DIR.NORTH;
				PutAgentOffset(0, 0, 0, dir);
				PutAgentOffset(1,m_par, 0, dir);
				PutAgentOffset(2, 0, m_par, dir);
				PutAgentOffset(3, m_par, m_par, dir);
				break;
			case SquareRot:
				CreateAgents(4);
				PutAgentOffset(0, 0, 0, DIR.EAST);
				PutAgentOffset(1,m_par, 0, DIR.SOUTH);
				PutAgentOffset(2, m_par, m_par, DIR.WEST);
				PutAgentOffset(3, 0, m_par, DIR.NORTH);
				break;
				
			}

		}

		public void SetDX(int dx) {
			m_par = dx;
		}

	}

	private StateSample GetConfig(int t){
		return m_sysOrbit.get(t);
	}


	/* holds : dx, dy */
	static class TranslationSample  {
		IntC [] m_dXY;
		int [] m_states;
		int m_Tcode;

		protected TranslationSample(StateSample current, StateSample previous){
			IntC [] currentXYarray= current.m_pXY, previousXYarray= previous.m_pXY;
			int size = currentXYarray.length;
			m_dXY= new IntC [size];
			m_states = current.m_cellState;  // danger here ?
			for (int a=0; a < size; a++){
				m_dXY[a] = DifferenceWith(currentXYarray[a],previousXYarray[a] );
			}
			m_Tcode= ToDircode(m_dXY);
		}

		public boolean IsEqualTo(TranslationSample e2) {
			boolean equal = (m_Tcode==e2.m_Tcode);
			int a=0;
			while (equal && (a < m_states.length)){
				equal= equal &&  m_states[a] == e2.m_states[a];
				a++;
			}
			return equal;
		}

		public String ToString() {
			StringBuilder out= new StringBuilder();
			for (IntC trans : m_dXY){
				out.append(trans.ToString()).append(" : ");
			}
			return out.toString();
		}

		public TranslationSample() {
			int m_Tcode= -99999999;
		}

	}

	/* holds : position , dir , cell state */
	class StateSample  {

		int m_size;
		IntC [] m_pXY;
		DIR [] m_dir;
		int [] m_cellState;


		/* creates a new time stamp of the system */
		protected StateSample(TurmiteSystem sys){
			m_size =sys.GetAgentNumber();
			/*if (nAgent <= 0){
				Macro.FatalError(" empty agents ???" );
			}*/
			m_pXY = new IntC [m_size];
			m_dir= new DIR [m_size];
			m_cellState= new int[m_size];

			// for gliders and tr.
			int time= sys.GetTime();

			for (int a=0; a < m_size; a++){
				m_dir[a] = sys.GetOrientationAgent(a);
				EnvCell c = sys.GetPositionAgent(a);
				m_pXY[a] = c.GetXY();
				m_cellState[a] = c.GetState() % 2; // BEWARE OF STATES 2 !

			}

		}


		protected String ToString(){
			StringBuilder s = new StringBuilder();
			for (int a=0; a < m_size; a++){
				s.append(" [" + //a +
                        " ").append(m_pXY[a].ToString()).append(" ").append(m_dir[a].ShortLetter()).append(" c: ").append(m_cellState[a]).append("]");
			}
			return s.toString();
		}

		public boolean IsEqualTo(StateSample sample){
			boolean answer= true;
			int a=0;
			while (answer && (a < m_size)){
				answer= answer 
				&&  m_pXY[a].IsEqualTo( sample.m_pXY[a] )
				&&  m_dir[a] == sample.m_dir[a] 
				                             &&  m_cellState[a] == sample.m_cellState[a];
				a++;
			}
			return answer;
		}

	}

	private int HowMuchCycle(int time, int previous, ComparableElementsList datalist) {
		boolean match=true;
		int dt=1;
		while (match && (previous - dt >= 0) ){
			if (datalist.AreEqual(time-dt, previous-dt)){
				dt++;
			} else{
				match = false;
			}
		}		
		return dt;
	}


	final static String ClassName= Thread.currentThread().getStackTrace()[1].getClassName();
	/**
	 * TEST 
	 */
	public static void main(String[] argv) {
		Macro.BeginTest(ClassName);
		TurmiteCycleDetect exp = new TurmiteCycleDetect();
		exp.DoExperiment();
		Macro.EndTest();
	}

	static public class CycleData {

		final int m_startCycle, m_cycleLen, m_nZero;
		IntC m_bbXY;
		IntC [] m_diffXY;
		String m_type, m_previousList;

		public CycleData(String type, int startCycle, int cycleLen, IntC bbXY,
				int nZero, IntC [] diffXY, String previousList) {
			m_type= type;
			m_startCycle = startCycle;
			m_cycleLen = cycleLen;
			m_nZero = nZero;
			m_bbXY = bbXY;
			m_diffXY = diffXY;
			m_previousList= previousList;
		}

		public static IntC[] GetTranslationVector(StateSample config1, StateSample config2) {
			IntC [] x1= config1.m_pXY, x2= config2.m_pXY;
			int size = x1.length;
			IntC [] dx= new IntC[size];
			for (int a=0; a < size; a++){
				dx[a] = DifferenceWith(x1[a], x2[a]);
			}
			return dx;
		}

		public String toString() {
			String out=" ";
			if (m_startCycle < 0) {
				out+= m_type;
			} else {	
				out+= m_type + " " + m_cycleLen ;// + " (" + MathMacro.PrimeDecompose(m_cycleLen) +")";
				out+= " S: " + m_startCycle;
				out+= " <LX,LY>: " + m_bbXY.ToString();
				out += " nZ: " + m_nZero ;// + " (" + MathMacro.PrimeDecompose(m_nZero) + ")";
				out+= " Tr: " ;
                for (IntC aM_diffXY : m_diffXY) {
                    out += aM_diffXY;
                }
			}
			return out;
		}

		final public static CycleData OutOfBounds= new CycleData("OOB", -99, 0, null, 0, null, "");
		final public static CycleData EGS= new CycleData("EGS?", -99, 0, null, 0, null, "");
		public static final CycleData PathEscape = new CycleData("PathEscape?", -99, 0, null, 0, null, "");
    }

}
