package architecture.multiAgent.tools;

import main.Macro;

/*
 * I find that this class is NOT SO useful...
 * 
 */
abstract public class SituatedAgent {

	public enum UPDATEMODE { NOCONFLICT, CONFLICT}

    final static int ZERO=0;
	/*--------------------
	 * abstract
	--------------------*/

	/* agent produces influences after reading its environment */ 
	abstract public void sig_AgentPerceivePutInfluence();

	/* agent updates its internal state */
	abstract public void sig_InternalUpdate();
	
	/* the environment sends a message to the agent to 
	 * say if its influences-attempts are successful
	 */ 
	abstract public void sig_ConflictResolution(UPDATEMODE mode);
	
	/* agent updates its position and observable state */
	abstract public void sig_PositionObservableUpdate();

	
	/*--------------------
	 * attributes
	--------------------*/
	
	// this cell is the cell where the agent tries to move
	private EnvCell m_moveTargetBuffer = null;
	private EnvCell m_cellPos;
	private MultiAgentSystem m_system = null;
	
	private String m_Label;
	/*--------------------
	 * construction
	--------------------*/
	
	public SituatedAgent(MultiAgentSystem  system, String label) {
		m_system= system;
		m_Label= label;
	}	
	/*--------------------
	 * handling moves
	--------------------*/
	
	/* override this */
	public void sig_AgentReset(){
		m_moveTargetBuffer= null;
	}
	
	/* asked by the system to apply the exclusion principle */
	public EnvCell GetTarget() {
		return m_moveTargetBuffer;
	}
	
	protected void DoMove(){
		SetPosition(m_moveTargetBuffer);
	}
	
	/* information received form the environment */
	protected void SetMoveTarget(EnvCell target) {
		m_moveTargetBuffer= target;
	}
	
	/*--------------------
	 * protected methods (the "language" of MAS) 
	--------------------*/
		
	/* an agent asks the system what cell 
	 * is in a given direction
	 */
	protected EnvCell GetMyNeighbourAt(DIR dir){
		return m_system.GetAgentNeighbourCellAt(this, dir);
	}
	
	
	/*--------------------
	 * get  & set
	--------------------*/
	public String GetLabel(){
		return m_Label;
	}
	
	public int GetCurrentStateCell(){
		return m_system.GetCellState(m_cellPos);
	}
	
	public EnvCell GetPosition(){
		return m_cellPos;
	}
	
	public void SetPosition(EnvCell pos) {
		m_cellPos= pos;		
	}
	
	public void SetPositionXY(int x, int y) {
		EnvCell pos= GetSystem().GetCellXYtorus(x, y);
		m_cellPos= pos;		
	}
	
	
	protected MultiAgentSystem GetSystem() {
		return m_system;
	}
	
	/*--------------------
	 * test
	 --------------------*/
	
	public static void DoTest() {		
		Macro.BeginTest("Agent");

		DIR dir1 = DIR.NORTH;
		DIR dir2 = dir1.TurnRight();
		Macro.print("dir1:"  + dir1 + " dir2:"  + dir2);
		
		Macro.EndTest();
	}

	
	
	
	
	
	

}
