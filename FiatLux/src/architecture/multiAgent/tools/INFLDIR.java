package architecture.multiAgent.tools;

import components.types.IntC;

public enum INFLDIR {

	NORTH, EAST, SOUTH, WEST, HERE;
	
	final static int NDIR= 4;
	// N, E , S , W
	private final static IntC [] OFFSET={
		new IntC(0,1), new IntC(1,0), 
		new IntC(0,-1),new IntC(-1,0), new IntC(0,0)};
	
	
	public INFLDIR TurnRight(){
		return values()[ (ordinal() + 1)% NDIR ]; 
	}
	public INFLDIR TurnLeft(){
		return values()[ (ordinal() + 3)% NDIR ];
	}
	
	public int ToInt() {
		return ordinal();
	}		
	
	static public INFLDIR FromInt(int intcode){
		return values()[ intcode ];
	}
	
	/* check that it is not equal to HERE before calling */
	public DIR ToDir() {
		return DIR.FromInt( ToInt() );
	}
	
			
}

/*public final static int NORTH=0, EAST=1, SOUTH=2, WEST=3, HERE=4;

final int [] DX = {0, 1, 0, -1, 0}; 
final int [] DY = {1, 0, -1, 0, 0};

int m_direction;*/

