package architecture.multiAgent.tools;


import components.randomNumbers.FLRandomGenerator;
import components.types.IntC;


public enum DIR {

	NORTH, EAST, SOUTH, WEST;

	final static int NDIR= 4;
	// N, E , S , W
	private final static IntC [] OFFSET={
		new IntC(0,1), new IntC(1,0), 
		new IntC(0,-1),new IntC(-1,0)
	};

	private char [] Letter= {'N', 'E', 'S', 'W' };
	
	public char ShortLetter(){
		return Letter[ToInt()];
	}

	public DIR TurnRight(){
		return values()[ (ordinal() + 1)% NDIR ]; 
	}
	public DIR TurnLeft(){
		return values()[ (ordinal() + 3)% NDIR ];
	}

	public int ToInt() {
		return ordinal();
	}		

	/* from DIR TO INFLDIR */
	public INFLDIR ToInflDir(){
		return INFLDIR.FromInt(this.ToInt());
	}

	/* from direction to offset */
	public IntC Offset(){
		return OFFSET[ToInt()];
	}

	static public DIR FromInt(int intcode){
		return values()[ intcode ];
	}

	static public DIR FromIntModulo(int intcode){
		return values()[ (intcode + NDIR) % NDIR  ];
	}

	static public DIR RandomDir4(FLRandomGenerator randomizer) {
		return FromInt(randomizer.RandomInt( NDIR));
	}

	/* topology creation */
	public static IntC Offset(int dir) {
		return FromInt(dir).Offset();
	}		

}
