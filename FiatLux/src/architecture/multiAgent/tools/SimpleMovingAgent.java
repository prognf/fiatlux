package architecture.multiAgent.tools;

import java.util.ArrayList;


/*--------------------
 * implements moving agents
 * 
 * @author Nazim Fates
*--------------------*/
abstract public class SimpleMovingAgent extends SituatedAgent {

	abstract public void sig_AgentPerceivePutInfluence();
	
	
	// TODO : document this 
	abstract public void SetOrientation(DIR dir);


	/*--------------------
	 * attributes
	 --------------------*/

	// TODO : THIS ONE IS NOT MANDATORY YET !
	protected UPDATEMODE m_UpdateMode ; 

	/*--------------------
	 * construction
	--------------------*/

	public SimpleMovingAgent(MultiAgentSystem in_system){
		super(in_system,"simpleagent");
	}

	/*--------------------
	 * reset
	--------------------*/

	
	/*--------------------
	 * influences
	--------------------*/


	/*--------------------
	 * update
	--------------------*/

	@Override
	public void sig_ConflictResolution(UPDATEMODE mode) {
		m_UpdateMode= mode;
	}



	/*--------------------
	 * get / set
	--------------------*/


	
	
	protected final ArrayList<DIR> GetFreeAvailableNeighbs(MultiAgentSystem system) {
		ArrayList<DIR> availableNeighbs= new ArrayList<DIR>(); 
		for (DIR dir: DIR.values()){
			EnvCell look= system.GetAgentNeighbourCellAt(this,  dir);
			int pop= system.CountPresence(look);
			//Macro.Debug("Pop:" + pop);
			if (pop == 0){
				availableNeighbs.add(dir);
			}
		}
		return availableNeighbs;
	}

}


