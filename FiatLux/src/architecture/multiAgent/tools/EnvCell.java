package architecture.multiAgent.tools;

import components.types.IntC;
import main.Macro;

abstract public class EnvCell {
	
	
	/*--------------------
	 * signals
	--------------------*/
	/* global reset before running a simulation ! */
	abstract public void sig_Reset();
	
	/* takes flips into account 
	 * and resets infl counters
	 */ 
	abstract public void sig_Update();
	
	
	/*--------------------
	 * Attributes
	 --------------------*/
	private int m_x,m_y; // coordinates
	
	protected int m_state; // state 
	
	protected EnvCell [] m_Neighb= null;  // topology
	
	protected int m_countInflMove; // move influences
	
	/*--------------------
	 * constructor
	 --------------------*/
	public EnvCell(int in_x, int in_y){
		m_x= in_x; m_y= in_y;
	}
	
	private EnvCell(){}


    /*--------------------
     * toplogy
     --------------------*/
	/* N E S W neighbours */
	protected void SetNeighbourhood(EnvCell [] in_Neighb){
		m_Neighb= in_Neighb;
	}
	
	
	public final EnvCell GetNeighbAt(int dir){
		return m_Neighb[dir];
	}
	
	final EnvCell GetNeighbAt(INFLDIR dir){
		if (dir == INFLDIR.HERE){
			Macro.FatalError("Call to GetNeighbAt with HERE... ");
			return this;
		} else {
			return GetNeighbAt( dir.ToInt()); 
		}
	}
	
	final EnvCell GetNeighbAt(DIR dir){
			return GetNeighbAt( dir.ToInt()); 
	}
	
	final EnvCell GetNeighbAt(DIR8 dir){
		return GetNeighbAt( dir.ToInt()); 
}
	
	public int GetX(){
		return m_x;
	}
	
	public int GetY(){
		return m_y;
	}

	public IntC GetXY(){
		return new IntC(m_x,m_y);
	}
	
	
	/*--------------------
	 * get / set
	--------------------*/
	
	public int GetState(){
		return m_state;
	}	
	
	public void SetState(int in_state){
		m_state= in_state;
	}
	
	public void AddInflMove(){
		m_countInflMove++;
	}
	
	public int GetCountInflMove(){
		return m_countInflMove;
	}

	

	
}
