package architecture.multiAgent.tools;

import components.types.IntC;
import components.types.Monitor;
import experiment.samplers.SuperSampler;
import experiment.toolbox.ConfigRecorder;
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.AutomatonViewer;
import grafix.viewers.GridViewer;
import initializers.CAinit.InitPanel;
import initializers.MASinit.MultiAgentInitializer;
import main.Macro;

public class MultiAgentSampler extends SuperSampler implements ConfigRecorder {
	private MultiAgentSystem m_system ;
	GridViewer m_viewer;
	MultiAgentInitializer m_initializer;

	public MultiAgentSampler(MultiAgentSystem in_system, IntC CellSizeForViewer) {
		m_system = in_system;
		m_viewer= m_system.GetSystemViewer(CellSizeForViewer);
		this.addMonitor(m_viewer);
		m_initializer= m_system.GetNewInitializer();
		if (m_initializer != null){
			Macro.print(4, "Initializer added");
		} else {
			Macro.print(4, "No Initializer was found !!!");
		}
		addMonitor(m_initializer);
	}
	
		
	/*--------------------
	 * signals
	 --------------------*/
		
	@Override
	public FLPanel GetSimulationWidgets() {
		InitPanel ip = new InitPanel(this, m_initializer.GetSpecificPanel());
		ip.adaptSize();
		return FLPanel.NewPanel(ip);
	}

	@Override
	public FLPanel GetSimulationRuleControls() {
		return m_system.GetViewControl();
	}

	@Override
	public FLPanel GetSimulationView() {
		return FLPanel.NewPanel(m_viewer);
	}

	@Override
	public AutomatonViewer GetViewer() {
		return m_viewer;
	}

	/*--------------------
	 * I/O
	 --------------------*/
	
	@Override
	/* TODO : update this one **/
	public void io_SaveImage() {
		int time = m_system.GetTime();
		String savename = GetFileSaveName();
		String filename= Macro.GetFileName(savename, time, m_RecordSuffix);
		String message= "t:" + time + m_RecordSuffix;
		// record style (margins)
		m_viewer.SetRecordStyle(AutomatonViewer.RecordStyle.BASIC);
		
		m_viewer.io_Save(filename, message, m_RecordFormat);		
	}
	
	private String m_SaveName = null;
	
	
	public void SetSaveName(String in_SaveName){
		m_SaveName= in_SaveName;
	}
	
	/* returns ModelName if no save name was given */
	private String GetFileSaveName() {
		if (m_SaveName==null) {
			return 	m_system.GetModelName();
		} else {
			return m_SaveName;
		}
	}
	
	

	
	/*--------------------
	 * implementations
	 --------------------*/
	
	@Override
	public String GetSimulationInfo() {
		return m_system.GetSimulationInfo();
	}

	@Override
	public int GetTime() {
		return m_system.GetTime();
	}

	@Override
	/* hack here : no topology object */
	public String GetTopologyInfo() {
		return m_system.GetTopologyInfo();
	}

	@Override
	public String [] GetPlotterSelection() {
		return null;
	}


	@Override
	public void io_SaveState() {
		// ???
	}

	/*--------------------
	 * get / set
	 --------------------*/
	
		public MultiAgentInitializer GetInitializer() {
		return m_initializer;		
	}

	public MultiAgentSystem GetSystem(){
		return m_system;
	}


	@Override
	protected String GetModelName() {
		return m_system.GetModelName();
	}


	@Override
	protected Monitor GetLinkedSystemAsMonitor() {
		return GetSystem();
	}

	
}
