package architecture.multiAgent.tools;

import java.util.ArrayList;

import components.arrays.GridSystem;
import components.types.FLsignal;
import components.types.IntC;
import components.types.IntCm;
import components.types.Monitor;
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.GridViewer;
import initializers.MASinit.MultiAgentInitializer;
import main.Macro;
import models.MASmodels.turmite.TurmiteEnvCell;

/*--------------------
* USES SituatedAgent
* @author Nazim Fates
*--------------------*/
abstract public class MultiAgentSystem extends GridSystem
implements Monitor {

	final static int DEF_AGENTS= -1;
	public enum EXCLUSIONMODE { ALLOW_POLICY, EXCLUDE_POLICY}

    public enum UPDATEPOLICY { REGULAR, SYNCHRONOUS, SEQUENTIAL, SEQUENTIAL_STEP}


    /*--------------------
     * definition
     --------------------*/
	abstract public String GetModelName();

	// creating cells specific to the system
	abstract public EnvCell GetNewEnvCell(int x,int y);

	// creating agents specific to the system
	abstract public SituatedAgent GetNewAgent(int agentnum);

	abstract public FLPanel GetViewControl();//external call
	
	abstract public MultiAgentInitializer GetNewInitializer();

	/*--------------------
	 * attributes
	 --------------------*/

	String m_ClassName = "PlanarField";

	private EXCLUSIONMODE m_ExclusionMode = EXCLUSIONMODE.ALLOW_POLICY;//;EXCLUDE_POLICY
	private UPDATEPOLICY m_UpdateMode= UPDATEPOLICY.REGULAR;

	
	protected EnvCell[][] m_Array; // ENVIRONMENT
	private SituatedAgentList m_agentList ;   // AGENTS
	
	protected FLPanel m_Controler = null; // associated View/Control

	int m_CurrentTime; //TODO: clarify why this is different from time

	/*--------------------
	 * constructor
	 --------------------*/

	public MultiAgentSystem(IntC xysize) {
		super(xysize);
		InitArray();
	}

	/** inits & clears the field */
	public void InitArray() {
		Macro.PrintInitSignal(4, this);
		int Xsize= GetXsize(), Ysize= GetYsize();
		m_Array = new EnvCell[Xsize][Ysize];
		for (int x = 0; x < Xsize; x++) {
			for (int y = 0; y < Ysize; y++) {
				m_Array[x][y] = GetNewEnvCell(x, y);
			}
		}
		m_agentList = new SituatedAgentList();
		CreateTopology();
	}


	/*--------------------
	 * signals (dynamics)
	 --------------------*/
	
	@Override
	public void ReceiveSignal(FLsignal sig){
		switch(sig){
		case init:
			sig_Init();
			break;
		case update:
			sig_Update();
			break;
		case nextStep:
			sig_NextStep();
			break;
		}
	}
	
	/** taking influences into account to change the state of the cells 
	 * change to non-final if needed... */
	final protected void UpdateEnvironment() {
		int X= GetXsize(), Y= GetYsize();
		for (int y=0; y< Y; y++){
			for (int x=0; x< X ; x++){
				m_Array[x][y].sig_Update();
			}
		}
	}
	
	/** initialisation */
	public void sig_Init(){
		m_CurrentTime = 0;
		ResetEnvironment();
		ResetRandomizer();
		for ( SituatedAgent agent : GetAgentList() ){  
			agent.sig_AgentReset();
		}
	}

	

	/** Main Method  **/
	public void sig_NextStep(){
		m_CurrentTime++;
		switch(m_UpdateMode){
		case REGULAR:
			sig_NextStepAllEnvUpdate();
			break;
		case SYNCHRONOUS:
			sig_NextStepSynchronous();
			break;
		case SEQUENTIAL:
			sig_NextStepSequential();
			break;
		case SEQUENTIAL_STEP:
			sig_NextStepSequentialOne();
			break;
		default:
			Macro.FatalError("UpdateMode has a non-defined value !");
		}
		// Environment Updating
		UpdateEnvironment();
	}

	
	/** Main Method 
	 * Warning : for active environments **/
	public void sig_NextStepAllEnvUpdate(){
		// A. production of influences
		for ( SituatedAgent agent : GetAgentList() ){  
			agent.sig_AgentPerceivePutInfluence();
		}

		// B. internal state updating
		for ( SituatedAgent agent : GetAgentList() ){
			agent.sig_InternalUpdate();
		}

		// C. Conflicts resolution
		for ( SituatedAgent agent : GetAgentList() ){
			ConflictResolution(agent);			
		}		

		// D. Agents Updating
		for ( SituatedAgent agent : GetAgentList() ){  
			// D2 
			agent.sig_PositionObservableUpdate();			
		}	

		
	}
	

	/** Main Method 
	 * Warning : it is "optimised" for passive environments **/
	public void sig_NextStepSynchronous(){
		// A. production of influences
		for ( SituatedAgent agent : GetAgentList() ){  
			agent.sig_AgentPerceivePutInfluence();
		}

		// B. internal state updating
		for ( SituatedAgent agent : GetAgentList() ){
			agent.sig_InternalUpdate();
		}

		// C. Conflicts resolution
		for ( SituatedAgent agent : GetAgentList() ){
			ConflictResolution(agent);			
		}		

		// D. Agents Updating
		for ( SituatedAgent agent : GetAgentList() ){  
			// the two operations DO NOT commute
			// D1
			UpdateEnvironmentAgent(agent);
			// D2 
			agent.sig_PositionObservableUpdate();			
		}	

		// E. Environment Updating
		//UpdateEnvironment();
	}


	/* Sequential update  */
	public void sig_NextStepSequential(){

		// special
		for ( SituatedAgent agent : GetAgentList() ){  
			agent.sig_AgentPerceivePutInfluence();
			agent.sig_InternalUpdate();
			ConflictResolution(agent);	
			UpdateEnvironmentAgent(agent);
			agent.sig_PositionObservableUpdate();
			//UpdateEnvironment();
		}

	}

	/** Sequential update  */
	public void sig_NextStepSequentialOne(){
		int num = GetTime() % GetAgentNumber();
		TimeClick();
		SituatedAgent agent = GetAgent(num);
		// special
		agent.sig_AgentPerceivePutInfluence();
		agent.sig_InternalUpdate();
		ConflictResolution(agent);	
		UpdateEnvironmentAgent(agent);
		agent.sig_PositionObservableUpdate();
		//UpdateEnvironment();


	}


	//int m_agentnum= 0;
	/** Main Method  **/
	/** public void sig_NextStepSequential2(){
		// special
		m_agentnum= (m_agentnum+1) % GetAgentNumber();   
		SituatedAgent situatedAgent= agent; 
		situatedAgent.sig_AgentPerceiveDecide();
		situatedAgent.sig_InternalUpdate();
		ConflictResolution(situatedAgent);	
		UpdateEnvironmentAgent(situatedAgent);
		situatedAgent.sig_PositionObservableUpdate();
		//UpdateEnvironment();
	}**/



	/* updates the environment of one agent only
	 * and then its position and observable state */
	final private void UpdateEnvironmentAgent(SituatedAgent situatedAgent){
		EnvCell cell = GetAgentPos(situatedAgent);
		cell.sig_Update();
		// THESE LINES WERE MISSING !
		EnvCell target= situatedAgent.GetTarget();
		target.sig_Update();
		// 

	}	


	/* in all generality, this method should be 
	 * specific to each agent's type of conflict resolution 
	 */
	protected void ConflictResolution(SituatedAgent situatedAgent) {
		switch (m_ExclusionMode) {
		case EXCLUDE_POLICY:
			// target is used twice here !
			EnvCell target= situatedAgent.GetTarget();
			int countmove= target.GetCountInflMove();
			int countpresence= CountPresence(target);
			// if we are sure that countmove >=1
			if ((countmove + countpresence) == 1) {
				situatedAgent.sig_ConflictResolution(SituatedAgent.UPDATEMODE.NOCONFLICT);
			} else {
				situatedAgent.sig_ConflictResolution(SituatedAgent.UPDATEMODE.CONFLICT);
			}
			break;
		case ALLOW_POLICY:
			situatedAgent.sig_ConflictResolution(SituatedAgent.UPDATEMODE.NOCONFLICT);
			break;
		default:
			Macro.FatalError("Exclusion Mode not found :" + m_ExclusionMode);
		}
	}

	private void ResetEnvironment() {
		//Macro.Debug("Reset Env");
		for (int xcell=0; xcell<GetXsize(); xcell++){
			for (int ycell=0; ycell<GetYsize(); ycell++){
				m_Array[xcell][ycell].sig_Reset();
			}
		}		
	}
	
	/*--------------------
	 * topology
	 --------------------*/

	private void CreateTopology(){
		// topology creation
		for (int x = 0; x < GetXsize(); x++) {
			for (int y = 0; y < GetYsize(); y++) {
				// we create the neighb
				EnvCell [] neighb= new EnvCell[DIR.NDIR];
				for (int dir=0; dir < DIR.NDIR; dir++){
					IntCm target = new IntCm(x,y);
					// modified but was not controlled
					target.AddToricalBoundaries(DIR.Offset(dir), m_XYsize);
					neighb[dir] = GetCell(target);
				}
				// we set the neighb
				GetCellXY(x,y).SetNeighbourhood(neighb);
			}
		}
	}

	/** TOPOLOGY part
	 * returns neighbour of cell pos at "direction" dir 
	 * return original cell if "direction" is HERE */
	final public EnvCell GetAgentNeighbourCellAt(SituatedAgent in_agent, INFLDIR dir){
		EnvCell cell = in_agent.GetPosition();
		return cell.GetNeighbAt(dir);	
	}

	/** TOPOLOGY part
	 * returns neighbour of cell pos at "direction" dir 
	 * return original cell if "direction" is HERE */
	final public EnvCell GetAgentNeighbourCellAt(SituatedAgent in_agent, DIR dir){
		EnvCell cell = in_agent.GetPosition();
		return cell.GetNeighbAt(dir);	
	}

	
	/** TOPOLOGY part
	 * returns neighbour of cell pos at "direction" dir 
	 * return original cell if "direction" is HERE */
	final public EnvCell GetAgentNeighbourCellAt(SituatedAgent in_agent, DIR8 dir){
		EnvCell cell = in_agent.GetPosition();
		return cell.GetNeighbAt(dir);	
	}

	/*--------------------
	 * influence management
	 --------------------*/

	/** "language" of subclasses */
	private void PutMoveInfl(EnvCell in_pos) {
		in_pos.AddInflMove();		
	}

	/** the environment "orders" to the agent to remember where it should go */
	final public void PutMoveInfl(SituatedAgent in_agent, INFLDIR infdir) {
		EnvCell target;
		if (infdir== INFLDIR.HERE){
			target= in_agent.GetPosition();
		} else{
			target= GetAgentNeighbourCellAt(in_agent, infdir);
		}
		PutMoveInfl(target); // we put the influence in the system
		in_agent.SetMoveTarget(target); // we memorize the destination

	}
	
	
	/** TODO : factorize with the previous one **/
	final public void PutMoveInfl8(SituatedAgent in_agent, DIR8 dir) {
		EnvCell target;
		target= GetAgentNeighbourCellAt(in_agent, dir);
		PutMoveInfl(target); // we put the influence in the system
		in_agent.SetMoveTarget(target); // we memorize the destination

	}
	

	/** simple procedure */
	public void PutMoveInfl(SituatedAgent in_agent, DIR m_infl_dir) {
		EnvCell target= GetAgentNeighbourCellAt(in_agent, m_infl_dir);
		PutMoveInfl(target); // we put the influence in the system
		in_agent.SetMoveTarget(target); // we memorize the destination
	}

	/*--------------------
	 * Get / Set agents
	 * @return 
	 --------------------*/

	/** used for topology construction */ 
	final public EnvCell GetCell(IntC pos){
		return m_Array[pos.X()][pos.Y()];
	}
	
	public SituatedAgentList GetAgentList(){
		return m_agentList;
	}

	/** used by the initializer **/
	public void SetAgentList(SituatedAgent [] in_list){
		m_agentList= new SituatedAgentList(in_list);
	}

	/** used by the initializer **/
	public void SetAgentListWithList(ArrayList<Object> in_list){
		m_agentList=  new SituatedAgentList(in_list);
	}

	/** used by the initializer **/
	public void ResetAgentList(){
		for ( SituatedAgent agent : GetAgentList() ){  
			agent.sig_AgentReset();
        }
	}

	public void SetAgentPos(int agentnum, IntC posXY) {
		EnvCell cell= GetCell(posXY);
		SetAgentPos(agentnum, cell);		
	}

	public void SetAgentPos(int agentnum, EnvCell cell) {
		m_agentList.GetAgent(agentnum).SetPosition( cell );		
	}

	/** use with care ! */
	public SituatedAgent GetAgent(int agentnum){
		return m_agentList.GetAgent(agentnum);
	}

	final public int GetAgentNumber(){
		return m_agentList.GetLength();
	}

	/* called by the viewer */
	final public EnvCell GetAgentPos(int agentnum){
		return m_agentList.GetAgent(agentnum).GetPosition();
	}


	final private EnvCell GetAgentPos(SituatedAgent situatedAgent){
		return situatedAgent.GetPosition();
	}


	/*--------------------
	 * Get / Set updating modes
	 --------------------*/

	public EXCLUSIONMODE GetExclusionMode(){
		return m_ExclusionMode;
	}

	public void SetExclusionMode(EXCLUSIONMODE exclusionmode){
		m_ExclusionMode= exclusionmode;
	}


	public UPDATEPOLICY GetUpdateMode(){
		return m_UpdateMode;
	}

	public void SetUpdateMode(UPDATEPOLICY mode){
		m_UpdateMode= mode;
	}



	/*--------------------
	 * Get / Set environment
	 --------------------*/
	/** how many agents are located on this cell ?*/
	public int CountPresence(EnvCell cell){
		int count=0;
		for ( SituatedAgent agent : GetAgentList() ){  
			// this is not clean !
			if ( agent.GetPosition() == cell) {
				count++;
			}
		}
		return count;
	}

	public IntC GetXYsize() {
		return m_XYsize;
	}

	public int GetXsize() {
		return m_XYsize.X();
	}

	public int GetYsize() {
		return m_XYsize.Y();
	}

	final public int GetTime() {
		return m_CurrentTime;
	}


	/**	reading the state of a cell **/
	final public int GetCellState(EnvCell pos){
		return pos.GetState();
	}

	

	/**  */ 
	final public EnvCell GetCellXY(int x, int y){
		return m_Array[x][y];
	}

	/** used for init &  topology construction */ 
	final public EnvCell GetCellXYtorus(int x, int y){
		int 	x2= IntC.AddModulo(x,m_XYsize.X(),m_XYsize.X()),
				y2= IntC.AddModulo(y,m_XYsize.Y(),m_XYsize.Y());
		return m_Array[x2][y2];
	}

	/**	reading the state of a cell **/
	final public int GetCellState(int x, int y) {
		try {
			return m_Array[x][y].GetState();
		} catch (Exception e) {
			e.printStackTrace();
			Macro.FatalError(this, "GetCellState", "Size " + m_XYsize+ "  x,y: " + x + " , " + y);
			return -1;
		}
	}

	/** setting the state of a cell may be used by a turmite * */
	final public void SetCellState(int x, int y, int state) {
		m_Array[x][y].SetState(state);
	}

	//	reading the nb of move influence on a cell
	final public int GetCellCountInflMove(IntC pos){
		int x = pos.X(), y = pos.Y();

		try {
			return m_Array[x][y].GetCountInflMove();
		} catch (Exception e) {
			e.printStackTrace();
			Macro.FatalError(this, "GetCellState", "Size " + m_XYsize
					+ "  x,y: " + x + " , " + y);

			return -1;
		}
	}


	/*--------------------
	 * other get / set
	 --------------------*/
	public GridViewer GetSystemViewer(IntC CellSizeForViewer) {
		return new SimpleMultiAgentViewer(this, CellSizeForViewer);
	}


	public String GetSimulationInfo() {
		String excl = GetExclusionMode().toString();
		return GetModelName() + ":" +  excl + ":" + m_UpdateMode + ":" + TurmiteEnvCell.flipCellPolicy;
	}

	public String GetTopologyInfo(){
		return "" + m_XYsize; 
	}

	

	/*--------------------
	 * random numbers
	 --------------------*/

	/** reset the randomizer */
	final protected void ResetRandomizer() {
		GetRandomizer().sig_ResetRandomizer();
	}	



}
