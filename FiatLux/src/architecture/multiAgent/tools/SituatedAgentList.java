package architecture.multiAgent.tools;

import java.util.ArrayList;
import java.util.Arrays;


public class SituatedAgentList extends java.util.ArrayList<SituatedAgent>{
	
	public SituatedAgentList() {
		super();
	}
	
	public SituatedAgentList(SituatedAgent[] in_list) {
		this();
		addAll(Arrays.asList(in_list));
	}	

	/* auto cast of each object 
	 * objects must be of SituatedAgent type */
	public SituatedAgentList(ArrayList<Object> in_list) {
		this();
		for (Object agent : in_list) {
			add((SituatedAgent)agent);
		}
	}

	/* access to an agent */
	final public SituatedAgent GetAgent(int agentnum) {
		return get(agentnum);
	}

	public int GetLength() {
		return size();
	}

	/* used in the "for each" loop */
	/*
	public java.util.List<SituatedAgent> GetCollection(){
		return this;
	}
	*/
	
}
