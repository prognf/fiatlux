package architecture.multiAgent.tools;

import components.types.IntC;
import grafix.viewers.GridViewer;


/*--------------------
 * general framework for MAS display
*--------------------*/
abstract public class MultiAgentViewer extends GridViewer {

	abstract protected void DrawEnvironmentAgents();
	
	/*--------------------
	 * attributes
	 --------------------*/

	//int m_Xsize, m_Ysize; // number of cells
	protected MultiAgentSystem m_system; // related system

	/*--------------------
	 * Constructor
	 * PixSize is (cellsize, border)
	 --------------------*/
	
	public MultiAgentViewer(MultiAgentSystem in_system, IntC in_PixSize) {
		super(in_PixSize,in_system);
		// attributes init
		m_system = in_system;
}

	/*--------------------
	 * Other methods
	 --------------------*/


	/* main component 
	 * X-axis: from left to right
	 * Y-axis :from TOP to BOTTOM !
	 * */
	protected void DrawSystemState() {
		DrawEnvironmentAgents();
	}

	

	/*--------------------
	 * Test
	 --------------------*/

}
