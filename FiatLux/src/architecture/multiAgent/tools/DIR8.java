package architecture.multiAgent.tools;


import components.randomNumbers.FLRandomGenerator;
import components.types.IntC;


public enum DIR8 {

	N, NE, E, SE, S, SW, W, NW;	
	final static int NDIR= 8;
	
	// N, E , S , W
	private final static IntC [] OFFSET={
		new IntC(0,1), 
		new IntC(1,1),
		new IntC(1,0), 
		new IntC(1,-1),
		new IntC(0,-1),
		new IntC(-1,-1),
		new IntC(-1,0),
		new IntC(-1,1)
	};

	//private char [] Letter= {'N', 'E', 'S', 'W' };
	
	/*public char ShortLetter(){
		return Letter[ToInt()];
	}*/
	public int ToInt() {
		return ordinal();
	}		

	/* from direction to offset */
	public IntC Offset(){
		return OFFSET[ToInt()];
	}

	static public DIR8 FromInt(int intcode){
		return values()[ intcode ];
	}

	static public DIR8 FromIntModulo(int intcode){
		return values()[ (intcode + NDIR) % NDIR  ];
	}

	static public DIR8 RandomDir8(FLRandomGenerator randomizer) {
		return FromInt(randomizer.RandomInt( NDIR));
	}

	/* topology creation */
	public static IntC Offset(int dir) {
		return FromInt(dir).Offset();
	}		

}
