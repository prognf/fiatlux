package architecture.multiAgent.drawingAgents;

import architecture.multiAgent.tools.MultiAgentViewer;
import components.types.IntC;
import grafix.gfxTypes.PaintToolKit;

public class DrawingViewer extends MultiAgentViewer {

	
	
	public DrawingViewer(TestDrawingSystem in_system, IntC in_PixSize) {
		super(in_system, in_PixSize);
		SetPalette( PaintToolKit.GetBWpalette(3) );
	}

	@Override
	protected void DrawEnvironmentAgents() {
		for (int x = 0; x < getXsize(); x++){
			for (int y = 0; y < getYsize(); y++) {
				int state = m_system.GetCellState(x, y); 
				DrawSquareColor(x, y, state);
			}
		}
		int nagents= m_system.GetAgentNumber();		
		//TODO

	}

}
