package architecture.multiAgent.drawingAgents;

import architecture.multiAgent.tools.EnvCell;

public class DrawingEnvCell extends EnvCell {

	public DrawingEnvCell(int in_x, int in_y) {
		super(in_x, in_y);
	}

	@Override
	public void sig_Reset() {
		m_state= 0;		
	}

	@Override
	public void sig_Update() {
		// do nothing : passive cell
	}

}
