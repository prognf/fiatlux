package architecture.multiAgent.drawingAgents;

import architecture.multiAgent.tools.EnvCell;
import architecture.multiAgent.tools.MultiAgentSampler;
import architecture.multiAgent.tools.MultiAgentSystem;
import architecture.multiAgent.tools.SituatedAgent;
import components.types.IntC;
import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.GridViewer;
import grafix.windows.SimulationWindowSingle;
import initializers.MASinit.MultiAgentInitializer;

public class TestDrawingSystem extends MultiAgentSystem {

	private static final String NAME = "DRAWING";
	final static IntC GRIDSIZE= new IntC(250, 250);
	private static final int PIXSIZE=3;
	
	public TestDrawingSystem(IntC in_size) {
		super(in_size);
		SetUpdateMode(UPDATEPOLICY.REGULAR);
	}

	//@Override
	public FLPanel GetViewControl() {
		return null; 
	}
	
	@Override
	public GridViewer GetSystemViewer(IntC CellSizeForViewer) {
		// 0 = no border
		return new DrawingViewer(this, CellSizeForViewer );
	}

	@Override
	public String GetModelName() {
		return NAME;
	}

	@Override
	public EnvCell GetNewEnvCell(int x, int y) {
		return new DrawingEnvCell(x, y);
	}

	@Override
	public SituatedAgent GetNewAgent(int agentnum) {
		return new DrawingAgent(this);
	}

	@Override
	public MultiAgentInitializer GetNewInitializer() {
		return new DrawingInitializer(this);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		TestDrawingSystem system= new TestDrawingSystem(GRIDSIZE);
		IntC CellSizeForViewer= new IntC(PIXSIZE,0);
		MultiAgentSampler sampler= 
				new MultiAgentSampler(system, CellSizeForViewer);
		grafix.windows.SimulationWindowSingle
			window = new SimulationWindowSingle("Test Drawing", sampler);
		//window.PackAndShow();
		window.BuildAndDisplay();
	}

}
