package architecture.multiAgent.drawingAgents;

import architecture.multiAgent.tools.MultiAgentSystem;
import architecture.multiAgent.tools.SituatedAgent;
import architecture.multiAgent.tools.SituatedAgentList;
import grafix.gfxTypes.elements.FLPanel;
import initializers.MASinit.MultiAgentInitializer;

public class DrawingInitializer extends MultiAgentInitializer {
	public DrawingInitializer(MultiAgentSystem in_System) {
		super(in_System);
	}

	@Override
	public FLPanel GetSpecificPanel() {
		return new FLPanel();
	}

	@Override
	public void SubInit() {
		int X= Getsystem().GetXsize(), Y= Getsystem().GetYsize();
		super.CreateAgents(2);
		SituatedAgentList l= Getsystem().GetAgentList();
		for (SituatedAgent agent : l){
			int x= GetRandomizer().RandomInt(X);
			int y= GetRandomizer().RandomInt(Y);
			DrawingAgent ag= (DrawingAgent)agent;
			ag.SetPositionXY(x, y);
		}
	}
	
	
	
}
