package architecture.multiAgent.drawingAgents;

import architecture.multiAgent.tools.EnvCell;
import architecture.multiAgent.tools.SituatedAgent;

public class DrawingAgent extends SituatedAgent {

	private static final int DELTALEN = 7;
	TestDrawingSystem m_system;
	int m_Len=40;
	int m_dx, m_dy;
	int m_time=0;
	
	public DrawingAgent(TestDrawingSystem in_system) {
		super(in_system,"agentAnonyme");
		m_system= in_system;
		m_dx= 1;
		m_dy= 0;
	}
	
	
	@Override
	public void sig_AgentPerceivePutInfluence() {
		DrawingEnvCell currentC= (DrawingEnvCell)super.GetPosition();
		currentC.SetState(1);
		SetPosition(currentC);
	}

	
	@Override
	public void sig_InternalUpdate() {
	
		m_time++;
		if (m_time % m_Len == 0){
			int temp= m_dx;
			m_dx= -m_dy;
			m_dy= temp;
			m_Len -= DELTALEN;
		}	
	}
	

	private DrawingEnvCell GetCellXY(int x, int y) {
		return (DrawingEnvCell) m_system.GetCellXY(x, y);
	}
	
	@Override
	public void sig_PositionObservableUpdate() {
		EnvCell cell= GetPosition();
		int x= cell.GetX(), y = cell.GetY();
		SetPositionXY(x + m_dx, y + m_dy);
	}

	@Override
	public void sig_ConflictResolution(UPDATEMODE mode) {
		// TODO Auto-generated method stub		
	}

}
