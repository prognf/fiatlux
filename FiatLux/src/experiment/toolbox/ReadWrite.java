package experiment.toolbox;

import java.util.ArrayList;

import components.allCells.SuperCell;
import components.types.FLStringList;
import components.types.IntC;
import experiment.samplers.CAsimulationSampler;
import main.Macro;
import topology.basics.LinearTopology;
import topology.basics.PlanarTopology;

public class ReadWrite {
	
	public final static String s_RAW=".flraw", s_INI=".fli";

	static public void SaveFileRaw(CAsimulationSampler sampler) {
		FLStringList automatonState = new FLStringList();
		automatonState.Add("format=raw");
	
		int d= sampler.GetTopologyDimension();
		if ( d != 2){
			Macro.SystemWarning("dimension not taken into account yet...");
			return;
		}
		PlanarTopology topo2D= (PlanarTopology) sampler.GetTopologyManager();
		IntC XYsize= topo2D.GetXYsize();
		String secondLine= "size="+ XYsize.X() + "," + XYsize.Y() + " time=" + sampler.GetTime();
		automatonState.Add(secondLine);
	
		String thirdLine = 
			" seedini=" + sampler.GetInitializer().GetSeed() +
			" seedmod=" + sampler.GetCellularModel().GetSeed() +
			" seedeng=" + sampler.GetUpdatingScheme().GetSeed();
		automatonState.Add(thirdLine);
	
		String fourthLine = sampler.GetCellularModel().getCurrentSettings();
		automatonState.Add(fourthLine);
		//TODO fifthLine = initializer
		//TODO sixthLine = updatingScheme
	
		//here comes the automaton state
		ArrayList<SuperCell> cellList = 
				sampler.GetAutomaton().GetArrayAsCastedList();
		for (SuperCell cell : cellList) {
			automatonState.Add(cell.ToSimpleString());
		}
	
		String filename = sampler.getFileName();
		automatonState.WriteToFile(filename + s_RAW);
	
	}
	
	/** simple writing of the configuration in text*/
	static public void saveFileSimpleText(CAsimulationSampler sampler) {
		
		int d= sampler.GetTopologyDimension();
		if (d==1) {
			saveFileSimpleText1D(sampler);
		} else if ( d == 2){
			saveFileSimpleText2D(sampler);
		} else {
			Macro.SystemWarning(" dimension does not exist :" + d);
		}
		
	
	}

	private static void saveFileSimpleText1D(CAsimulationSampler sampler) {
		FLStringList outAutomatonState = new FLStringList();
		LinearTopology topo1D= (LinearTopology) sampler.GetTopologyManager();
		int X= topo1D.GetSize();
		
		
	}

	private static void saveFileSimpleText2D(CAsimulationSampler sampler) {
		FLStringList outAutomatonState = new FLStringList();
		PlanarTopology topo2D= (PlanarTopology) sampler.GetTopologyManager();
		IntC XYsize= topo2D.GetXYsize();
		// writing line by line automaton state
		for (int y=0; y < XYsize.Y(); y++){
			StringBuilder line= new StringBuilder();
			for (int x=0; x < XYsize.X(); x++){
				int pos= topo2D.GetIndexMap2D(x, y);
				line.append(sampler.GetAutomaton().GetCell(pos).ToSimpleString()).append(" ");
			}
			outAutomatonState.Add(line.toString());
		}
				
		String filename = sampler.getFileName();
		outAutomatonState.WriteToFile(filename);		
	}

}
