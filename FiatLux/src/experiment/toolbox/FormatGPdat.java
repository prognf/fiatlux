package experiment.toolbox;

import components.types.FLString;
import components.types.FLStringList;
import main.Macro;
import main.StatsMacro;
/*--------------------
 * manages GPdat format 
 --------------------*/
public class FormatGPdat {

	final static int TIMECOLUMN=0, YCOLUMN=1, ZEROSCOLUMN=3, NSAMPLESCOLUMN=4;
	public final static String GPDAT=".gpdat";
	
	/*--------------------
	 * attributes
	 --------------------*/
	String m_filename;
	FLStringList m_data; 
	double [] m_Xdata, m_Ydata;
	/*--------------------
	 * constructor
	 --------------------*/
	/* reading from a gpdat or fldat file */
	public FormatGPdat(String in_filename){				
		/*m_filename= in_filename;
		m_data = new FLStringList(m_filename);*/
	
		if (in_filename.endsWith(GPDAT)){
			Macro.print(" found GPdat file... special treatment (no transpose ??");
			// GPDAT
			m_data = new FLStringList(in_filename);
			m_filename= in_filename;
		} else {
		// FLDAT
			m_filename= in_filename;
			ReadFromFLdat(in_filename);
		}
	}
	
	public FormatGPdat(String filename, FLStringList stringlist){				
		m_filename= filename;
		m_data = stringlist;
	}
	
	
	
	/*--------------------
	 * get/set
	 --------------------*/
	/* the number of X values */
	public int GetSampleNumber(){
		return m_data.GetSize();
	} 
	
	/* the number of values associated to each X */
	public int GetSampleSize(){
		String firstline = m_data.Get(0); 
		int size= firstline.split(" ").length;
		return size -1 ;
	}
	
	/*** x coordinate of the last value */
	public String GetMaxTime(){
		String lastline = m_data.Get(GetSampleNumber() - 1); 
		String [] splitlastline= lastline.split(" ");
		return splitlastline[TIMECOLUMN];
	}
	
	/** number of zeros at the max time **/
	public String GetZerosRatioMaxTime(){
		String lastline = m_data.Get(GetSampleNumber() - 1); 
		String [] splitlastline= lastline.split(" ");
		return splitlastline[ZEROSCOLUMN] + " / " + splitlastline[NSAMPLESCOLUMN];
	}
	
	
	
	public String GetArrayStats(){
		return " MaxTime: " +  GetMaxTime() + " " + GetZerosRatioMaxTime();
	}
	
	/**	applies 
	 *  I/O writes the average and std deviation and zeros */
	public  void WriteFileMuSigma(){
		
			int nlines = GetSampleNumber();
			
			FLStringList output= new FLStringList();
			for (int line=0 ; line < nlines; line++){
				String sline = m_data.Get(line);
				//Macro.Debug("read line:" + sline);
				String [] newline= sline.split(" ");
				int n_samples = newline.length - 1;
				double [] ydata = new double[n_samples];
				int zeros =0;
				// shifting and converting
				for (int col=0; col < n_samples; col++){
					ydata[col] = FLString.String2Double(newline[col+1]);
					if (ydata[col]==0.0){zeros++;}
				}
				double mu= StatsMacro.Mean(ydata);
				double sigma= StatsMacro.sdKnuth(ydata);
				 
				// format  : average + stddev + zeros + nsamples
				String outline= "" + newline[0] + FLString.WHTSPC 
								+ mu + FLString.WHTSPC +  sigma + FLString.WHTSPC 
								+ zeros + FLString.WHTSPC + n_samples;  
				output.Add(outline);
				
			}
			String newfilename = FLString.RemoveExtension(m_filename) + "MS" + GPDAT;
			output.WriteToFile(newfilename);
			
	}
	
	/** from FLDAT TO GPDAT */
	public void ReadFromFLdat(String in_fldatfile){
		//Macro.Debug("input:  gnuplot style");
		FLStringList fldat= new FLStringList(in_fldatfile); // open file
		m_data= Transpose(fldat);
	}
	
	/** transpose a (a lines) * (b elements), separated by spaces, 
	 * into a (b lines) * (a elements), separated by spaces*/
	public static FLStringList Transpose(FLStringList fldat){
		//Macro.Debug("input:  gnuplot style");
		
		// parse file
		int n_samples= fldat.GetFirstItem().split(" ").length ;
		int sz_sample= fldat.size() ;
		
		String [][] data = new String [sz_sample][n_samples];
		
		for (int i=0; i < sz_sample; i++){
			String line= fldat.Get(i);
			data[i]= line.split(" ");
		}
				
		// construct the new array
		FLStringList transposed = new FLStringList();
		for (int line=0 ; line < n_samples ; line++){
			StringBuilder newline= new StringBuilder();
			for (int col=0; col < sz_sample; col++){
				newline.append(data[col][line]).append(FLString.WHTSPC);
			}
			transposed.Add(newline.toString());
		}
		return transposed;
	}
	
	/* writes the file (transposition of the matrix) */
	public void WriteFile(){
		// write GPDAT
		String newfilename = GetNewfileName(m_filename);
		m_data.WriteToFile(newfilename);			
	}
	
	
	private static String GetNewfileName(String filename) {
		return FLString.RemoveExtension(filename) + GPDAT;
	}

	private void ParseData(){
		int size= GetSampleNumber();
		m_Xdata = new double[size]; 
		m_Ydata = new double[size];
		for (int i=0; i< size; i++){
			String lastline = m_data.Get(i); 
			String [] splitline= lastline.split(" ");
			double xvalue= FLString.String2Double(splitline[TIMECOLUMN]);
			double yvalue= FLString.String2Double(splitline[YCOLUMN]);
			m_Ydata[i]= yvalue;
			m_Xdata[i]= xvalue;
		}
	}
	
	public double GetAverageAfterTransients(int tOffs) {
		ParseData();
		double sum = 0.0;
		int nValues = 0;

		for(int i = 0; i < GetSampleNumber(); i++) {
			if(m_Xdata[i] + Macro.EPSILON > (double)tOffs) {
				nValues ++;
				sum += m_Ydata[i];
			}
		}
		//assert(nValues > 0);
		if(nValues == 0){
			Macro.FatalError("Time offset bigger than last sample.");
		}
		return sum / (double) nValues;
	}
	
	
}
