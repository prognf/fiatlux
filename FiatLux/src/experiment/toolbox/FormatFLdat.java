package experiment.toolbox;
import components.types.FLString;
import components.types.FLStringList;
import components.types.SVector;
import main.Macro;
import main.StatsMacro;

/*--------------------
 * Manages FLdat format
 * 
 * @author Nazim Fates
 */
public class FormatFLdat {
	
	public final static String FLDAT= ".fldat";
	
	final static int XLINE=0, YLINESHIFT=1;
	
	int [] m_xdata;
	double [][] m_ydata;
	int m_nsamples;
	
	/* reading from a gpdat file */
	public FormatFLdat(String filename){
		
		if (!filename.endsWith(FLDAT)){
			Macro.FatalError("bad file format: fldat expected");
		} else {
			Macro.print(2,"Reading file " + filename );
			FLStringList data= new FLStringList(filename);
			
			int nlines= data.GetSize();
			m_nsamples = nlines -1;
			String s_xdata = data.Get(XLINE);
			m_xdata= StringToIntArray(s_xdata);
			m_ydata = new double [m_nsamples][]; 
			for (int sample=0; sample < m_nsamples; sample++){
				String s_ydata= data.Get(sample + YLINESHIFT);
				m_ydata[sample]= StringToDoubleArray(s_ydata);
			}
		}
	}
	
	/* converts a string to an array of int */ 
	final public static int [] StringToIntArray(String in_data){
		String  [] newdata = in_data.split(" ");
		int size = newdata.length;
		int [] intdata = new int[size];
		for (int i=0; i< size; i++){
			intdata[i] = FLString.String2Int(newdata[i]);
		}
		return intdata;
	} 
	
	/* converts a string to an array of int */ 
	final public static double [] StringToDoubleArray(String in_data){
		String  [] newdata = in_data.split(" ");
		int size = newdata.length;
		double [] doubledata = new double[size];
		for (int i=0; i< size; i++){
			doubledata[i] = FLString.String2Double(newdata[i]);
		}
		return doubledata;
	} 
	
	/* computes an average from tmin to end */
	private double [] AsymptoticAverage(int in_tmin){
		int imin =0;
		int t = m_xdata[imin];
		while (t<in_tmin){
			imin++;
			t = m_xdata[imin];
		}
		double [] average = new double[m_nsamples];
		for (int sample=0; sample < m_nsamples; sample++){
			average[sample]= StatsMacro.PartialAverageFromEnd(m_ydata[sample],imin);
		}
		return average;
	}
	
	/* each sample is associated to an average computed from tmin to the end
	 * returns the average and std dev. and number of these computed averages
	 */
	public SVector StatAsymptotic(int in_tmin){
		SVector stats = new SVector(3);
		double [] asymptotics=  AsymptoticAverage(in_tmin);
		double mu= StatsMacro.Mean(asymptotics);
		double sigma= StatsMacro.sdKnuth(asymptotics);
		stats.SetVal(0, mu);
		stats.SetVal(1, sigma);
		stats.SetVal(2, asymptotics.length);
		return stats;
	}
	
}
