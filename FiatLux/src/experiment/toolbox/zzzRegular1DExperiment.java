package experiment.toolbox;

import java.util.Date;

import components.types.FLString;
import components.types.FLStringList;
import main.Macro;
import main.commands.ToGPdat;

/**
 * a regular 1D experiment uses a fixed sampling procedure its main function is
 * to manage all I/O operations it allows to run simultaneous experiments on
 * different machines
 * 
 * @deprecated
 */
@Deprecated
abstract public class zzzRegular1DExperiment {

    /* returns current number with launching any experiment */
    static public int GetRunNum(final String filename) {
        // extension could have been removed
        boolean exists_a = Macro.FileExists(filename + FormatFLdat.FLDAT);
        boolean exists_b = Macro.FileExists(filename);
        // Macro.print( "Operating: "+ filename + " exists :" + exists);
        if (exists_a || exists_b) {
            FLStringList file = new FLStringList(filename);
            int nexp = file.GetSize(); // how many lines ?
            return nexp - 1;
        } else {
            return 0; // code for non-existing file
        }
    }

    /**
     * target output file where to put the data
     */
    private final String m_filename;

    /**
     * info on machine
     */
    public String m_tag;

    /*------------------------------------------------------------------------
     * constructor 
     *------------------------------------------------------------------------*/
    protected zzzRegular1DExperiment(final String in_tag, String in_filename) {
        // adding FLDAT suffix
        if (!in_filename.endsWith(FormatFLdat.FLDAT)) {
            in_filename += FormatFLdat.FLDAT;
        }
        m_filename = in_filename;
        m_tag = in_tag;
    }

    /* I/O this one is called before any sampling occurs */
    public void DoCreation() {
        // creation of parameters and experiment conditions

        // sampling time values with exponential rate
        String lineXpar = GetXSample().ToOneString();

        FLStringList outfile = new FLStringList();
        outfile.Add(lineXpar);
        outfile.WriteToFile(m_filename);

    }

    /*------------------------------------------------------------------------
     * abstract 
     *------------------------------------------------------------------------*/
    /**
     * returns one Y sample (e.g. time) it is better to call GetXsample rather
     * than recalculate the input !
     */
    abstract public FLStringList GetOneYSample();

    /** what settings ? */
    abstract public String GetSamplingConditions();

    /** name of X par (e.g. time) */
    abstract public String GetXParName();

    /** returns the X sample (e.g. time) */
    abstract public FLStringList GetXSample();

    public void Message(final String tag, final int run_num, final int Z, Date t1, Date t0) {
        String msg = "Run: " + run_num + " / " + Z + " done in: " + FLString.TimeFormat(t1, t0) + " by:" + m_tag;
        msg += " date :" + Macro.GetDateTime();
        Macro.print(3, msg);
    }

    /*--------------------
     * main
     --------------------*/

    public void zzzRunSamples(final int Z) {
        // we have to check wether the runs have to be continued or to be
        // started
        // run num is the run number retruned by the "Operate" function
        int run_num = GetRunNum(m_filename);
        // check
        if (run_num > 0) {
            Macro.print(1, "Restarting file : " + m_filename + " at position " + run_num + " / " + Z);
        } else {
            Macro.print(1, "Starting to process new file : " + m_filename + " for " + Z + " runs");
            DoCreation();
        }
        // continue until you reach Z
        while (run_num < Z) {
            Date t0 = Macro.GetTime();
            Macro.print(3, "Starting sampling for conditions:" + GetSamplingConditions());
            FLStringList sample = GetOneYSample(); // abstract method
            WriteSample(sample);
            run_num = GetRunNum(m_filename);
            Date t1 = Macro.GetTime();

            Message(m_tag, run_num, Z, t1, t0);

        }
        String[] arguments = new String[] { m_filename };
        ToGPdat.main(arguments);

        Macro.print(2, "Experiment done.");
    }

    /* I/O one sampling occured */
    public synchronized void WriteSample(final FLStringList in_sample) {
        String lineYpar = in_sample.ToOneString();
        FLStringList outfile = new FLStringList(m_filename);
        outfile.Add(lineYpar);
        outfile.WriteToFile(m_filename);
    }

}
