package experiment.toolbox;

import grafix.gfxTypes.imgFormat.ImgFormat;

public interface ConfigRecorder {

	
	/**
	 * @param in_suffix set a String to put after the output image
	 */
    void io_SetRecordSuffix(String in_suffix);
	
	/**
	 * @param in_newFormat set the format of the output image
	 */
    void io_SetRecordFormat(ImgFormat in_newFormat);
	
	/**
	 * Save an image snapshot of the automaton
	 */
    void io_SaveImage();
	
	/**
	 * Save a text snapshot of the automaton
	 */
    void io_SaveState();
	
}
