package experiment.toolbox;

import components.types.FLsignal;
import components.types.Monitor;
import experiment.measuring.OneRegisterMD;
import main.Macro;
import main.MathMacro;

/** one register ; time readable **/
public class CycleDetector extends OneRegisterMD
implements Monitor {

	private static final int DECA = 10/* how many letters are read */;
	public static final String NAME = "CycleDetector";
//	GenericCellArray<OneRegisterIntCell> m_array;
//	private RegularDynArray m_system;


	private void Iqra() {
		StringBuilder out = new StringBuilder();
		int SIZE= GetSize();
		int NDECA = SIZE / DECA;
		int [] read= new int[DECA];
		for (int i=0; i<NDECA; i++){
			int shift= DECA*i;
			for (int x=0; x<DECA; x++){
				int index = (shift + x)%SIZE;
				read[x]= GetState(index);
			}
			int bin2int = MathMacro.Tab2CodeInt(read);
			String hexa= Integer.toHexString(bin2int);
			out.append(":").append(hexa);
		}
		//int t= m_system.GetTime();
		//Macro.fPrint("time %d state: %s",t, out.toString());
	}
	
	@Override
	/** processing signals **/
	public void ReceiveSignal(FLsignal sig){
		switch(sig){
		case init:
			sig_Init();
			break;
		case update:
			sig_Update();
			break;
		case nextStep:
			break;
		}
	}
	
	public void sig_Update() {
		Iqra();
	}

	public void sig_Init() {
		Macro.print(" -init- " + NAME);
	}


	@Override
	public String GetName() {
		return NAME;
	}


}
