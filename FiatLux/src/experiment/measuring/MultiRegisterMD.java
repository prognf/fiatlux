package experiment.measuring;

import components.allCells.MultiRegisterCell;
import components.arrays.GenericCellArray;
import experiment.samplers.CAsampler;


abstract public class MultiRegisterMD extends CAmeasuringDevice {
	
	protected GenericCellArray<MultiRegisterCell> m_Array ;

	@SuppressWarnings("unchecked")
	public void LinkTo(CAsampler in_Sampler) {
		m_Array = in_Sampler.GetArrayAsList();
	}
	
	private final MultiRegisterCell GetCell(int pos){
		return m_Array.GetCell(pos);
	}
	
	protected final int GetStateN(int pos, int register){
		return GetCell(pos).GetStateI(register);
	}
	
	protected final int GetSize(){
		return m_Array.GetSize();
	}
	
	
}
