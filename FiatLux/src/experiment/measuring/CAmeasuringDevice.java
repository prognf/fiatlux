package experiment.measuring;

import components.types.DoubleCouple;
import experiment.measuring.general.DecimalParameterMeasurer;
import experiment.samplers.CAsampler;
import experiment.samplers.SuperSampler;
import main.Macro;


/*-------------------
 * used to perform measurements
 * works only with CAsampler
 *--------------------*/

public abstract class CAmeasuringDevice extends MeasuringDevice 
implements DecimalParameterMeasurer
{

	public void LinkTo(SuperSampler insampler){
		Macro.print(5, "Linking sampler to the measuring device:" + GetName());
		CAsampler sampler= (CAsampler) insampler;
		this.LinkTo(sampler);
	}
	
	abstract public void LinkTo(CAsampler in_Sampler);
	
	public DoubleCouple GetMinMax(){
		return null;
	}
	
}
