package experiment.measuring;

import experiment.measuring.general.DecimalParameterMeasurer;

public abstract class MASmeasuringDevice extends OneRegisterMD 
	implements DecimalParameterMeasurer
{
	
	public abstract double GetMeasure();
	
	
//	@Override
//	public abstract void LinkTo(OneRegisterReadable sampler);
	
	@Override
	public abstract String GetName();
}
