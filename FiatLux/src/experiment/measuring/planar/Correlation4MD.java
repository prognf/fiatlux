package experiment.measuring.planar;


/*--------------------
 *  measures the ratio of pairs in a square lattice
 *--------------------*/

public class Correlation4MD extends PlanarMeasuringDevice {

	public final static String NAME= "Corr4";

	/** name of the measure */
	public String GetName(){return NAME;}

	public Correlation4MD(){

	}

	/*--------------------
	 * Calculus methods
	 --------------------*/
	public double GetMeasure() {
		//
		int count= 0;
		//  
		for (int y = 0; y < GetYsize(); y++) {
			for (int x = 0; x < GetXsize(); x++){

				int center= GetState(x,y);
				int north= GetState(x,y+1);
				int east= GetState(x+1,y);
				int south= GetState(x,y-1);
				int west= GetState(x-1,y);
				count += (center==north) ? 1 : -1 ;
				count += (center==east)  ? 1 : -1 ;
				count += (center==south) ? 1 : -1 ;
				count += (center==west)  ? 1 : -1 ;
			}
		}// for
		return (double) (count) /(4 * (double) (GetXsize()*GetYsize())) ;
	}






}
