package experiment.measuring.planar;

import components.types.DoubleCouple;
import experiment.measuring.general.OneRegisterStandardIpsMD;

public class DensityIpsMD extends OneRegisterStandardIpsMD 
 {

	public static final String NAME = "DensityIps";
	private static final double DELTA = 0.5;

	@Override
	public String GetName() {
		return NAME;
	}

	@Override
	public double GetMeasure() {
		return CountSum()/(double)GetNsize() - DELTA;
	}

	public int CountSum() {
		int N= GetNsize();
		int sum=0;
		for (int pos=0; pos<N; pos++){
			sum += GetState(pos);
		}
		//Macro.fDebug("sum:%d", sum);
		return sum;
	}
	
	/** return code: 0 or 1 is the majority state, 2 means equality */
	public int GetMajorityState() {
		int N= GetNsize(), sum = CountSum();
		if (N==2*sum){
			return 2;
		} else {
			return (2*sum) / N ; // integer div
		}
	}
	
	
	/** code -1 not homogeneous ; 0 : 0^L attained ; 1 : 1^L attained **/
	public int isHomogeneousPointAttained() {
		int q= GetState(0);
		int pos=1;
		boolean flag=true;
		while ( (pos< GetNsize()) &&flag){
			flag= (GetState(pos)==q);
			pos++;
		}
		return flag?q:-1;
	}

	//@Override
	public DoubleCouple GetMinMax() {
		return new DoubleCouple(0+DELTA, 1+DELTA);
	}

}
