package experiment.measuring.planar;

import java.util.ArrayList;

import architecture.interactingParticleSystems.IPSsampler;
import architecture.interactingParticleSystems.InteractingParticleSystemPlanarAbstract;
import architecture.interactingParticleSystems.InteractingParticleSystemRegular;
import components.types.DoubleCouple;
import components.types.IntC;
import experiment.measuring.OneRegisterMD;
import experiment.measuring.general.DecimalParameterMeasurer;
import experiment.samplers.SuperSampler;
import main.Macro;

/** one register **/
abstract public class IPS2DmeasuringDevice extends OneRegisterMD 
implements DecimalParameterMeasurer {

	private InteractingParticleSystemPlanarAbstract m_ips;
	int m_Size;

	@Override
	public void LinkTo(SuperSampler insampler) {
		Macro.print(4, "Linking " + GetName()  + " MD to sampler");
		IPSsampler sampler = (IPSsampler) insampler; 		
		m_ips= sampler.GetSystem();
	}

	public DoubleCouple GetMinMax(){
		return null;
	}


	protected int GetCellState(int pos) {
		return m_ips.GetCellState(pos);
		// also use m_ips.getState(pos)
	}


	protected ArrayList<IntC> GetCellCoupleList() {
		InteractingParticleSystemRegular sys = (InteractingParticleSystemRegular)m_ips;
		return sys.m_cellCouple;
	}


	protected InteractingParticleSystemRegular GetSystem() {
		InteractingParticleSystemRegular sys = (InteractingParticleSystemRegular)m_ips;
		return sys;
	}

}
