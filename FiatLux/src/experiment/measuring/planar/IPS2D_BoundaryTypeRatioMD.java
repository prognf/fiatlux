package experiment.measuring.planar;

import architecture.interactingParticleSystems.InteractingParticleSystemRegular;
import components.types.IntegerList;
import experiment.measuring.general.DecimalParameterMeasurer;
import experiment.samplers.SuperSampler;
import main.Macro;

public class IPS2D_BoundaryTypeRatioMD  extends IPS2DmeasuringDevice
implements DecimalParameterMeasurer
{ 

	public static final String NAME = "IPS2D-BoundaryTypeRatio";
	static final int m_EMPTY = -1;
	
	public static final int m_TYPE_4CONNECTED=1;
	public static final int m_TYPE_8CONNECTED=2;
	private static final boolean CONTROL = true;

	private IntegerList m_NearBorderCellIndices;
	
			
	@Override
	public double GetMeasure() {
		
			int [] stateCount= new int [3]; // assuming init to zero
			
			for (int pos : m_NearBorderCellIndices){
				int state= GetCellState(pos);
				stateCount[state]++;
			}
				
			int size= m_NearBorderCellIndices.GetSize();
			if (CONTROL){
				if (stateCount[1] + stateCount[2] != size){
					Macro.FatalError("Problem in the counting of near-border cells");
				}
			}
			double ratio= (stateCount[2] - stateCount[1]) / (double)size; 
			return ratio;
	}

	@Override
	public String GetName() {
		return NAME;
	}

	@Override
	public void LinkTo(SuperSampler in_Sampler) {
		super.LinkTo(in_Sampler);
		InteractingParticleSystemRegular sys = GetSystem();
		m_NearBorderCellIndices= sys.GetNearBorderCellIndices();		
	}

	

	
}
