package experiment.measuring.planar;

import components.allCells.MultiRegisterCell;
import experiment.measuring.MultiRegisterMD;

public class LgcaCoverageMD extends MultiRegisterMD {

	public static final String NAME = "Coverage";

	@Override
	public double GetMeasure() {
		double counter = 0;
		for (MultiRegisterCell cell : m_Array) {
			if (cell.GetSumState()>0) {
				counter++;
			}
		}
		return counter/GetSize();
	}

	@Override
	public String GetName() {
		return NAME;
	}
}
