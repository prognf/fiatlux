package experiment.measuring.planar;

/** for the measuring devices that work on a 2D int plane **/
public interface OneRegisterPlanarReadable {

	int GetSizeX();
	
	int GetSizeY();
	
	int GetStateXY(int x, int y);
	
}
