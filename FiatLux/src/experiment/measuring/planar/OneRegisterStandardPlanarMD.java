package experiment.measuring.planar;

import architecture.interactingParticleSystems.IPSsamplerAbstract;
import components.arrays.OneRegisterReadable;
import experiment.measuring.OneRegisterMD;
import experiment.samplers.SuperSampler;
import main.Macro;

/** also for MDs that are not in the CA structure (ips, ...) **/
abstract public class OneRegisterStandardPlanarMD extends OneRegisterMD {

	private OneRegisterPlanarReadable m_readableSys;

	
	public void LinkToDirect(OneRegisterReadable sampler) {
		if (sampler instanceof IPSsamplerAbstract){
			m_readableSys= (IPSsamplerAbstract) sampler;
			super.LinkTo((SuperSampler)sampler);
		} else {
			Macro.SystemWarning(" unrecognized sampler");
		}
	}

	protected int GetStateXY(int x, int y){
		return m_readableSys.GetStateXY(x, y);
	}

	protected int GetXsize(){
		return m_readableSys.GetSizeX();
	}

	protected int GetYsize(){
		return m_readableSys.GetSizeY();
	}


}
