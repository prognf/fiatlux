package experiment.measuring.planar;

import experiment.measuring.OneRegisterMD;
import experiment.samplers.CAsampler;
import topology.basics.PlanarTopology;


/*--------------------
 * A PlanarMeasuringDevice will be used to perform measurement on 2D automata
*--------------------*/

/** OneRegister */
public abstract class PlanarMeasuringDevice extends OneRegisterMD {

	private int m_Xsize, m_Ysize;

	public void LinkTo(CAsampler in_sampler) {
		super.LinkTo(in_sampler);
		PlanarTopology topo2D= (PlanarTopology)in_sampler.GetTopologyManager();
		m_Xsize= topo2D.GetXsize();
		m_Ysize= topo2D.GetYsize();
	}

	// rq : possibility to change this with a pre-calculus & store in table
	protected final int GetState(int x, int y) {
		int index = ((x + m_Xsize)%m_Xsize) 
				+ m_Xsize  * ((y + m_Ysize)%m_Ysize);
		return GetState(index);
	}
	
	int GetXsize(){
		return m_Xsize;
	}
	
	int GetYsize(){
		return m_Ysize;
	}

}
