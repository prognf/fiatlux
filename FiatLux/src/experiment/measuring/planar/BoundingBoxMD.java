package experiment.measuring.planar;

import components.types.IntC;
import experiment.measuring.TwoRegisterMD;
import experiment.samplers.CAsampler;

/*--------------------
 --------------------*/

public class BoundingBoxMD extends TwoRegisterMD {
	
	public static final String NAME = "Bounding Box";
	
	/* name of the measure */
	public String GetName(){return NAME;}
	
	int m_Xsize, m_Ysize;
		
	public BoundingBoxMD(){
				
	}
	
	/* overrides : we memorize (x,y) size */ 
	public void LinkTo(CAsampler in_Sampler) {
		super.LinkTo(in_Sampler);
		IntC size= GetTopology().GetXYsize();
		m_Xsize= size.X();
		m_Ysize= size.Y();		
	}
	
	/*--------------------
	 * Calculus methods
	 --------------------*/
	public double GetMeasure() {
//		 min-max init is inversed !
		int xmin= m_Xsize, xmax= 0, ymin= m_Ysize, ymax= 0 ;
		// small hack consisting in reading the colours ! 
		for (int x = 0; x < m_Xsize; x++){
			for (int y = 0; y < m_Ysize; y++) {
				int pop= GetStateOne(x + m_Xsize  * y); 
				if (pop>0){
					if (x<xmin) xmin= x;
					if (x>xmax) xmax= x;
					if (y<ymin) ymin= y;
					if (y>ymax) ymax= y;
				}
			}
		}// for
		int dx= xmax - xmin;
		int dy= ymax - ymin; 
		return (double) (dx*dy) /(double) (m_Xsize*m_Ysize) ;
	}
	
}
