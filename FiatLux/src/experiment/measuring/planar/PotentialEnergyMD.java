package experiment.measuring.planar;

import components.types.IntC;
import experiment.measuring.TwoRegisterMD;

public class PotentialEnergyMD extends TwoRegisterMD { 

	public static final String NAME = "potential";
	static final int m_EMPTY = -1;
	
	public static final int m_TYPE_4CONNECTED=1;
	public static final int m_TYPE_8CONNECTED=2;

	// Sampler
	
	public PotentialEnergyMD() {
		
	}
	
		
	@Override
	public double GetMeasure() {
		// Note: use SingleReg array to get neighbourhood,
		// and TwoReg array to read the state
		double energy = 0.0;
		int numAgents = 0;
		int size = GetSize();
		
		double dist = 0;
		
		IntC cc = GetTopology().GetXYsize();
		
		for(int i = 0; i < size; i++) {
			if(GetStateOne(i) > 0) {
				numAgents++;
			}
		}
		
		float[][] posArray = new float[numAgents][2];
		int llpos = 0;
		for(int i = 0; i < size; i++) {
			if(GetStateOne(i) > 0) {
				// normalize with grid size
				float x = GetXFromIndex(i)/(float)cc.X();
				float y = GetYFromIndex(i)/(float)cc.Y();
				posArray[llpos][0] = x;
				posArray[llpos][1] = y;
				llpos++;
			}
		}

		for(int i = 0; i < numAgents; i++) {
			for(int j = 0; j < numAgents; j++) {
				float x0 = posArray[i][0];
				float y0 = posArray[i][1];
				float x1 = posArray[j][0];
				float y1 = posArray[j][1];
				double di;
				if(i != j) {
					di = Math.sqrt((x0-x1)*(x0-x1) + (y0-y1)*(y0-y1));
					dist += 1.0/di;
				}
			}
		}
		
		dist = dist / (float)(numAgents*numAgents);
		
		energy = dist;

		return energy;
	}

	


	@Override
	public String GetName() {
		return NAME;
	}

	

}
