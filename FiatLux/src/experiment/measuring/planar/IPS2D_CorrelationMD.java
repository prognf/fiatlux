package experiment.measuring.planar;

import components.types.IntC;
import experiment.measuring.general.DecimalParameterMeasurer;

public class IPS2D_CorrelationMD extends IPS2DmeasuringDevice 
implements DecimalParameterMeasurer
{ 

	public static final String NAME = "IPS2D-corr";
	
	static final int m_EMPTY = -1;
	
	public static final int m_TYPE_4CONNECTED=1;
	public static final int m_TYPE_8CONNECTED=2;
	

	@Override
	public double GetMeasure() {
		
			int countOnes= GetSystem().CountNumberState(1);
			int size = GetSize();
			double density= countOnes / (double) size;
		
			int countEqual= 0;
			
			for (IntC cellAB : GetCellCoupleList()){
				int stateA= GetCellState(cellAB.X()),
					stateB= GetCellState(cellAB.Y());
				if (stateA == stateB){
					countEqual ++;
				}
			}
			
			int linkSize= GetCellCoupleList().size();
			double rate= countEqual/ (double)linkSize;
			
			// renormalisation: expected  proba per link
			double expectedLinkRate= 1 - 2. * density * ( 1. - density ); 
			
			double ordrerP= (rate - expectedLinkRate) / (1 - expectedLinkRate);
			
			return ordrerP;
	}



	@Override
	public String GetName() {
		return NAME;
	}



	

	


}
/*

IntCouple size = m_TopologyManager.GetXYsize();

for(int x = 0; x < size.X(); x++) {
	for (int y=0; y < size.Y(); y++){
		int index = m_TopologyManager.GetIndexMap2D(x, y);
		if (m_Sampler.GetCellState(index)==1){
			state1++;
		}
		count ++;
	}
}

*/