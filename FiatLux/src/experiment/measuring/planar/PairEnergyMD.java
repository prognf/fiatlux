package experiment.measuring.planar;

import components.types.DoubleCouple;
import experiment.measuring.general.DecimalParameterMeasurer;

public class PairEnergyMD extends OneRegisterStandardPlanarMD 
implements DecimalParameterMeasurer {

	public static final String NAME = "PairEnergy";

	@Override
	public String GetName() {
		return NAME;
	}

	@Override
	public double GetMeasure() {
		int countPair[]= MakeMeasureTwoPairTypes();
		int minval= Math.min(countPair[0], countPair[1]);
		return minval;//countPair[0]+countPair[1];
	}

	private int[] MakeMeasureTwoPairTypes() {
		int countPair[]= new int[2];
		int X= GetXsize(), Y= GetYsize();
		for (int y=0; y< Y; y++){
			for (int x=0; x< X; x++){
				int xp= (x+1) % X;
				int yp= (y+1) % Y;
				int s= GetStateXY(x, y); 
				if (s==GetStateXY(xp, y)){ //horizontal pair
					countPair[s]++;
				}
				if (s==GetStateXY(x, yp)){ //vertical pair
					countPair[s]++;
				}
			}
		}
		//Macro.fDebug(" p0:%d p1:%d", countPair[0], countPair[1]);
		return countPair;
	}

	@Override
	public DoubleCouple GetMinMax() {
		return null;
	}

	public boolean isArchipelagoAttained() {
		int countPair[]= MakeMeasureTwoPairTypes();
		return (countPair[0]==0) || (countPair[1]==0);
	}

	

	
}
