package experiment.measuring.planar;

import components.allCells.MultiRegisterCell;
import experiment.measuring.MultiRegisterMD;

public class LgcaNumParticleMD extends MultiRegisterMD {
	
	public static final String NAME = "Particle count";

	@Override
	public double GetMeasure() {
		int counter = 0;
		for (MultiRegisterCell cell : m_Array) {
			counter += cell.GetSumState();
		}
		return counter;
	}

	@Override
	public String GetName() {
		return NAME;
	}
}
