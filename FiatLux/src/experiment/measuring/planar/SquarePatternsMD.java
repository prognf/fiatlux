package experiment.measuring.planar;

import components.types.IntC;
import experiment.samplers.CAsampler;
import experiment.samplers.SuperSampler;
import topology.basics.PlanarTopology;

/*--------------------
 *  measures the number of patterns in a square lattice
 *  FIXME : rewrite
 * @author Nazim Fates
*--------------------*/

public class SquarePatternsMD extends PlanarMeasuringDevice {
	
	public final static String NAME= "sqPatterns";
	
	/* name of the measure */
	public String GetName(){return NAME;}
	
	int m_Xsize, m_Ysize;
		
	public SquarePatternsMD(){
				
	}
	
	@Override
	/* OLD **/
	public void LinkTo(SuperSampler insampler) {
		// may produce an error !
		CAsampler sampler = (CAsampler) insampler;
		PlanarTopology topo2D= (PlanarTopology)sampler.GetTopologyManager();
		IntC size= topo2D.GetXYsize();
		m_Xsize= size.X();
		m_Ysize= size.Y();
		
		super.LinkTo(sampler);
	}
	
	/*--------------------
	 * Calculus methods
	 --------------------*/
	public double GetMeasure() {
		//
		int count= 0;
		//  
		for (int x = 0; x < m_Xsize; x++){
			for (int y = 0; y < m_Ysize; y++) {
				int center= GetState(x,y);
				int north= GetState(x,y+1);
				int east= GetState(x+1,y);
				int south= GetState(x,y-1);
				int west= GetState(x-1,y);
				if ( (center==north) && (center==east) && (center==south) && (center==west) ){
					count++;
				} 
			}
		}// for
		return (double) (count) /(double) (m_Xsize*m_Ysize) ;
	}

	

	
		
}
