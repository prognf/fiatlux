package experiment.measuring.planar;

import experiment.measuring.TwoRegisterMD;


// Purpose: study the r(t) of two amoebae in order to verify
// it wrt to the theoretical model
public class TwoAmoebaeDistanceMD extends TwoRegisterMD { 

	static final String m_NAME = "2amoebaeDist";
	static final int m_EMPTY = -1;
	
	
	public TwoAmoebaeDistanceMD() {
	}
	
	@Override
	public double GetMeasure() {
/*		// Note: use SingleReg array to get neighbourhood,
		// and TwoReg array to read the state
		int numColors = 0;
		int numAgents = 0;
		int size = m_Size;
		// init colors
		for(int i = 0; i < m_Size; i++) {
			m_colors[i] = m_EMPTY;
		}

		for(int i = 0; i < size; i++) {
			if(m_CellArrayTwoRegs.GetStateOne(i) > 0) {
				// count Agents
				//numAgents++;
				if(m_colors[i] == m_EMPTY) {
					RecursivePaint(i, numColors);
					//System.out.println();
					numColors++;
				}
			}
		}
		// we are interested in the number of colors but we have to normalize
		//System.out.println(numColors);
		return numColors; // (double) numAgents;
	*/
			return 0.0;
	}

	@Override
	public String GetName() {
		return m_NAME;
	}

	
}
