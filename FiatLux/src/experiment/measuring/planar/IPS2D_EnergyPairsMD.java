package experiment.measuring.planar;

import components.types.IntC;
import experiment.measuring.general.DecimalParameterMeasurer;

public class IPS2D_EnergyPairsMD extends IPS2DmeasuringDevice 
implements DecimalParameterMeasurer
{ 

	public static final String NAME = "IPS2D-energyP";

	@Override
	public double GetMeasure() {
			int countEqual= 0;
			for (IntC cellAB : GetCellCoupleList()){
				int stateA= GetCellState(cellAB.X()),
					stateB= GetCellState(cellAB.Y());
				if (stateA == stateB){
					countEqual ++;
				}
			}
			
			int linkSize= GetCellCoupleList().size();
			double rate= countEqual/ (double)linkSize;
			return rate;
	}

	

	



	@Override
	public String GetName() {
		return NAME;
	}


}
