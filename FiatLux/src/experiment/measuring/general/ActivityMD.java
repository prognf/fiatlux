package experiment.measuring.general;

import components.allCells.ClassicalCACell;
import components.arrays.GenericCellArray;
import experiment.measuring.CAmeasuringDevice;
import experiment.samplers.CAsampler;


/** 
 * @author Nazim Fates
 * note that this MD does not necessary apply to an array of OneRegisterCell,
 * which explains its "position" in the hierarchy 
 */

public class ActivityMD extends CAmeasuringDevice {	
	
	
	public static final String NAME = "activity";
	
	GenericCellArray<ClassicalCACell> m_arrayCAcell;
	
	/** call before using MD  */
	public void LinkTo(CAsampler in_sampler){
		//Macro.Debug("test activity");  TEST
		m_arrayCAcell= in_sampler.GetArrayAsList();
	}
	
	protected int GetState(int pos){
		return m_arrayCAcell.GetCell(pos).GetState();
	}
	
	protected int GetSize(){
		return m_arrayCAcell.GetSize();
	}
	
	@Override
	public double GetMeasure() {
		int count = 0;
		for (ClassicalCACell cell : m_arrayCAcell){
			if ( ! cell.IsStable() ){
				count++;
			}	
		}
		return count / (double) GetSize();
	}


	@Override
	public String GetName() {
		return NAME;
	}
	
}
