package experiment.measuring.general;

import architecture.interactingParticleSystems.IPSsamplerAbstract;
import components.arrays.OneRegisterReadable;
import experiment.measuring.OneRegisterMD;
import experiment.samplers.SuperSampler;
import main.Macro;

/** we try the use of MD that are not in the CA structure (ips, ...) **/
abstract public class OneRegisterStandardIpsMD extends OneRegisterMD
implements DecimalParameterMeasurer {

	private OneRegisterReadable m_readableSys;

	
	public void DirectLinkTo(SuperSampler sampler) {
		if (sampler instanceof IPSsamplerAbstract){
			m_readableSys= (IPSsamplerAbstract) sampler;
		} else {
			Macro.SystemWarning(" unrecognized sampler");
		}
	}
	

	protected int GetNsize(){
		return m_readableSys.GetSize();
	}

}
