package experiment.measuring.general;

import experiment.measuring.OneRegisterMD;
import experiment.samplers.CAsampler;
import experiment.samplers.SuperSampler;
import models.CAmodels.ClassicalModel;

/*--------------------
 * measures the number of cells which have state "maxstate"
 * uses CAsampler and ClassicalModel
 * @author Nazim Fates
*--------------------*/

public class MaxStateDensityMD extends OneRegisterMD implements DecimalParameterMeasurer {
	
	public static final String NAME = "MaxState";
	
	/* name of the measure */
	public String GetName(){return NAME;}
	
	int m_Xsize, m_Ysize;
	ClassicalModel m_model;
	
	public MaxStateDensityMD(){
		
	}
	
	/* overrides ! */
	@Override
	public void LinkTo(SuperSampler sampler) {
		super.LinkTo(sampler);
		CAsampler casampler= (CAsampler)sampler;
		// cast to check if the sampler has a good type
		m_model= (ClassicalModel) casampler.GetCellularModel();
	}
				
	/*--------------------
	 * Calculus methods
	 --------------------*/
	public double GetMeasure() {
		int count=0;
		int size= GetSize();
		int M= m_model.GetMaxStateValue();
		for (int pos= 0; pos < size; pos++){
			if (GetState(pos) == M){
				count++;
			}
		}
		return count / (double) size; 	
	}

}
