package experiment.measuring.general;

import components.types.DoubleCouple;

/** should not be necessarily OneRegister **/
public interface DecimalParameterMeasurer {

	double GetMeasure();

	String GetName();
	
	DoubleCouple GetMinMax();

}
