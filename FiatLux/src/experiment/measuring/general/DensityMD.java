package experiment.measuring.general;

import experiment.measuring.OneRegisterMD;
import main.Macro;

/*--------------------
 * A DensityMeasuringDevice will be used to measure density of a given state
 * in a OneRegisterCells array
--------------------*/
public class DensityMD extends OneRegisterMD implements DecimalParameterMeasurer {

	public static final String NAME = "densityMD";
	
	/* name of the measure */
	public String GetName(){return NAME;}
	
	public DensityMD(){}
	
	
	/*--------------------
	 * Calculus methods
	 --------------------*/
	public double GetMeasure() {		
		double dens= (double) CountNonZero() / (double) GetSize();
		//Macro.Debug("Measuring density for size:" + size + " val:" + dens);
		return dens;
	}

	/** how many cells in a non-zero state ?**/
	public int CountNonZero() {
		int count=0, size = GetSize();
		// for all cells of the automaton
		for (int i = 0; i < size; i++) {
			if (GetState(i) !=  0 )
				count++;
		}
		return count;
	}

	/** how many cells in a given state ?**/ 
	public int CountState(int state) {
		int count=0, size = GetSize();
		// for all cells of the automaton
		for (int i = 0; i < size; i++) {
			if (GetState(i) == state)
				count++;
		}
		return count;
	}

	public double measureStateDensity(int state) {
		return ((double) CountState(state) / (double) GetSize());
	}

	public double CountStateDensityWithoutFailure(int state, int failure) {
		return ((double) CountState(state) / (double) (GetSize() - CountState(failure)));
	}

	/** is my array in a all-null-state ? **/
	public boolean isAllZeroState(){
		for (int i=0; i< GetSize(); i++){
			if (GetState(i)!=0){
				return false;
			}
		}
		return true;
	}
	
	/*--------------------
	 * test methods
	 --------------------*/

	static public void DoTest() {
		Macro.BeginTest("DensityMeasuringDevice");
		Macro.EndTest();
	}

	

	
	

}
