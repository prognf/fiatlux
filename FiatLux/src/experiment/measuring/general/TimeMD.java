package experiment.measuring.general;

import components.arrays.TimeReadable;
import components.types.DoubleCouple;
import experiment.measuring.MeasuringDevice;
import experiment.samplers.SuperSampler;
import main.Macro;


/*--------------------
 * returns the time at each time step 
 * @author Nazim Fates
*--------------------*/

public class TimeMD extends MeasuringDevice 
implements DecimalParameterMeasurer
{

	final static String NAME="time";
	private static final double MINT=0, MAXT = 100;

	/* name of the measure */
	public String GetName(){return NAME;}

	TimeReadable m_device ;

	public void LinkTo(SuperSampler sampler) {
		m_device= (TimeReadable)sampler;
	}

	/*--------------------
	 * Calculus methods
	 --------------------*/
	public double GetMeasure() {
		try {
			int time = m_device.GetTime();
			return (double) time;
		} catch (NullPointerException e ){
			Macro.SystemWarning(e, "Use LinkTo(...) before");
			return Macro.ERRORdouble;
		}
	}

	@Override
	public DoubleCouple GetMinMax() {
		return new DoubleCouple(MINT, MAXT);
	}



}
