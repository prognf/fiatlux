package experiment.measuring.general;

import components.arrays.OneRegisterReadable;
import experiment.measuring.OneRegisterMD;


/*--------------------
 * measures the number of cells which have state "maxstate"
 * 
 * @author Nazim Fates
*--------------------*/

public class UniformStateMDgen extends OneRegisterMD 
{
	
	private static final int FIRSTCELL = 0, NOTUNIFORM = -1;
	private static final String NAME = "Uniform";

	OneRegisterReadable m_readable; // main link object
	
	/* -1 : not uniform , 0/1 : confirms uniform all-0all-1 
	 * array is supposed to have at least one cell*/
	public int IsUniformState(){
		int size= GetSize();
		int targetState = GetState(FIRSTCELL);
		int pos=1;
		while (pos < size){
			if (GetState(pos) != targetState){
				return NOTUNIFORM;
			}
			pos++;
		}
		return targetState;
	}
	
	/** code : 0, 1 for the state most present , -1 :equality **/
	public int MajState(){
		int excedent = 0;
		int size= GetSize();
		for (int pos=0; pos< size; pos++){
			excedent += 2 * GetState(pos) - 1;
		}
		if (excedent == 0){
			return -1;
		} else {
			return excedent>0 ? 1 : 0;
		}		
	}
	
	/** gives the parity of the current configuration **/
	public int countParity() {
		int count=0;
		for (int pos=0; pos< GetSize(); pos++){
			count += GetState(pos);
		}
		return count%2;
	}
	
	@Override
	public String GetName() {
		return NAME;
	}

	
}
