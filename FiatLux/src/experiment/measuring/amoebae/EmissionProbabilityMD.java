package experiment.measuring.amoebae;

import components.arrays.OneRegisterReadable;
import experiment.measuring.MASmeasuringDevice;
import models.MASmodels.amoebae.AmoebaeInfotaxisSampler;

public class EmissionProbabilityMD extends MASmeasuringDevice {
	public static String NAME = "Emission probability";
	private AmoebaeInfotaxisSampler m_sampler;

	@Override
	public double GetMeasure() {
		return this.m_sampler.getEmissionProbability();
	}

	public void LinkTo(OneRegisterReadable sampler){
		this.m_sampler = (AmoebaeInfotaxisSampler)sampler;
	}

	@Override
	public String GetName() {
		return NAME;
	}


}
