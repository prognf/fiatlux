package experiment.measuring;

import components.allCells.OneRegisterIntCell;
import components.arrays.GenericCellArray;
import components.types.DoubleCouple;
import experiment.samplers.CAsampler;
import experiment.samplers.SuperSampler;
import main.Macro;


/** measuring device for oneRegister array ; linked to a CAsampler*/
public abstract class OneRegisterMD extends MeasuringDevice {	
	
	GenericCellArray<OneRegisterIntCell> m_array; 
	
	@Override
	public void LinkTo(SuperSampler sampler) {
		CAsampler nsampler= (CAsampler) sampler;
		m_array= nsampler.GetArrayAsList();
		Macro.print(6,"Linking MD to sampler of size : " + m_array.GetSize());
	}
	
	public int GetState(int pos){
		return m_array.GetCell(pos).GetState();
	}
	
	public int GetSize(){
		return m_array.GetSize();
	}
	
	//@override
	public DoubleCouple GetMinMax() {
		return null;
	}
	
	
}
