package experiment.measuring.specific;

import experiment.measuring.general.DensityMD;

/**
 * @author Nicolas Gauville
 * @purpose Measure Green density
 */
public class GreenDensityMD extends DensityMD {
    public static final String NAME = "Green-density";
    public String GetName() {return NAME;}

    public GreenDensityMD(){
        super();
    }

    public double GetMeasure() {
        return (double) CountState(2) / (double) (GetSize() - CountState(3));
    }
}
