package experiment.measuring.specific;

import components.allCells.ClassicalCACell;
import components.arrays.GenericCellArray;
import experiment.measuring.CAmeasuringDevice;
import experiment.samplers.CAsampler;

/**
 * @author Nicolas Gauville
 * @purpose Experiment to explain RGB-a macroscopic organisation
 */
public class AverageNeighborhoodMD extends CAmeasuringDevice {
    public static final String NAME = "average-n";
    public static final int R = 0;
    public static final int G = 1;
    public static final int B = 2;

    GenericCellArray<ClassicalCACell> m_arrayCAcell;

    public void LinkTo(CAsampler in_sampler){
        m_arrayCAcell= in_sampler.GetArrayAsList();
    }

    protected int GetState(int pos){
        return m_arrayCAcell.GetCell(pos).GetState();
    }

    protected int GetSize(){
        return m_arrayCAcell.GetSize();
    }

    @Override
    public double GetMeasure() {
        int[] qt = new int[3];
        int[][] avgn = new int[3][3];
        int nb = 0;

        for (ClassicalCACell cell : m_arrayCAcell) {
            int cellStatus = cell.GetState();
            qt[cellStatus]++;
            avgn[cellStatus][R] += cell.CountOccurenceOf(R);
            avgn[cellStatus][G] += cell.CountOccurenceOf(G);
            avgn[cellStatus][B] += cell.CountOccurenceOf(B);

            if (cell.CountOccurenceOf(cellStatus) > 4) {
                nb++;
            }
        }

        System.out.println("R : " + qt[R] + " : (" + Math.floorDiv(100 * avgn[R][R], 8 * qt[R]) + ", " + Math.floorDiv(100 * avgn[R][G], (8 * qt[R])) + ", " + Math.floorDiv(100 * avgn[R][B], (8 * qt[R])) + ")");
        System.out.println("G : " + qt[G] + " : (" + Math.floorDiv(100 * avgn[G][R], 8 * qt[G]) + ", " + Math.floorDiv(100 * avgn[G][G], (8 * qt[G])) + ", " + Math.floorDiv(100 * avgn[G][B], (8 * qt[G])) + ")");
        System.out.println("B : " + qt[B] + " : (" + Math.floorDiv(100 * avgn[B][R], 8 * qt[B]) + ", " + Math.floorDiv(100 * avgn[B][G], (8 * qt[B])) + ", " + Math.floorDiv(100 * avgn[B][B], (8 * qt[B])) + ")");
        System.out.println(nb);

        return (double) nb / (double) GetSize();
    }


    @Override
    public String GetName() {
        return NAME;
    }

}