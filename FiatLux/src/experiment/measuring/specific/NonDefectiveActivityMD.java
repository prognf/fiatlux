package experiment.measuring.specific;

import components.allCells.ClassicalCACell;
import components.arrays.GenericCellArray;
import experiment.measuring.OneRegisterMD;
import experiment.measuring.general.DecimalParameterMeasurer;
import experiment.samplers.CAsampler;


/** 
 * @author Nazim Fates
 * note that this MD does not necessary apply to an array of OneRegisterCell,
 * which explains its "position" in the hierarchy 
 */

public class NonDefectiveActivityMD extends OneRegisterMD implements DecimalParameterMeasurer {
	
	
	public static final String NAME = "nd-activity";
	
	GenericCellArray<ClassicalCACell> m_arrayCAcell;
	
	public void LinkTo(CAsampler in_sampler){
		m_arrayCAcell= in_sampler.GetArrayAsList();
	}
	
	public double GetMeasure() {
		return GetMeasure(3);
	}

	public double GetMeasure(int defectiveId) {
		int count = 0;
		int total = 0;
		for (ClassicalCACell cell : m_arrayCAcell){
			if ( ! cell.IsStable() ){
				count++;
			}

			if (cell.GetState() != defectiveId) {
				total++;
			}
		}
		return count / (double) (total);
	}


	@Override
	public String GetName() {
		return NAME;
	}
	
}
