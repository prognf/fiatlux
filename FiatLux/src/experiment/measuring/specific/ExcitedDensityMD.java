package experiment.measuring.specific;

import experiment.measuring.TwoRegisterMD;
import experiment.samplers.CAsampler;
import models.CAmodels.chemical.DictyoModel;

/*--------------------
 * measures the number of excited cells in Dictyo model
 * @author Nazim Fates
*--------------------*/

public class ExcitedDensityMD extends TwoRegisterMD {
	
	/* name of the measure */
	public String GetName(){return "excited";}
	
	int m_Xsize, m_Ysize;
	DictyoModel m_model;
	
	public ExcitedDensityMD(){
		
	}
	
	/* overrides ! */
	public void LinkTo(CAsampler in_Sampler) {
		super.LinkTo(in_Sampler);
		// cast to check if the sampler has a good type
		m_model= (DictyoModel) in_Sampler.GetCellularModel();
	}
				
	/*--------------------
	 * Calculus methods
	 --------------------*/
	public double GetMeasure() {
		int count=0;
		int size= GetSize();
		int M= m_model.GetExcited();
		for (int pos= 0; pos < size; pos++){
			if (GetStateTwo(pos) == M){
				count++;
			}
		}
		return count / (double) size; 	
	}


}
