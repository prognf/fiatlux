package experiment.measuring.specific;

import experiment.measuring.TwoRegisterMD;
import models.CAmodels.multiRegister.NGRgbDiagModel;

/**
 * @author Nicolas Gauville
 * @purpose Measure alert state density
 */
public class AlertDensityMD extends TwoRegisterMD {
    public static final String NAME = "Alert-density";
    public String GetName() {return NAME;}

    private int state = NGRgbDiagModel.ALERT_B;

    public AlertDensityMD() {
        super();
    }

    public AlertDensityMD(int s) {
        super();
        state = s;
    }

    public double GetMeasure() { return (double) CountStateOne(state) / (double) GetSize(); }

    public double GetMeasure(int state) { return (double) CountStateOne(state) / (double) GetSize(); }

    public double GetMeasure(int state, int defectiveState) {
        return (double) CountStateOne(state) / (double) Math.max(1, GetSize() - CountStateOne(defectiveState));
    }

    private int CountStateOne(int state) {
        int count = 0, size = GetSize();

        for (int i = 0; i < size; i++) {
            if (GetStateOne(i) == state)
                count++;
        }
        return count;
    }
}
