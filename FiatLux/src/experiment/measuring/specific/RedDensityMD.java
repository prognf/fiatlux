package experiment.measuring.specific;

import experiment.measuring.general.DensityMD;

/**
 * @author Nicolas Gauville
 * @purpose Measure Red density
 */
public class RedDensityMD extends DensityMD {
    public static final String NAME = "Red-density";
    public String GetName() {return NAME;}

    public RedDensityMD(){
        super();
    }

    public double GetMeasure() {
        return (double) CountState(0) / (double) (GetSize() - CountState(3));
    }
}
