package experiment.measuring.specific;

import experiment.measuring.general.DensityMD;

/**
 * @author Nicolas Gauville
 * @purpose Measure Blue density
 */
public class BlueDensityMD extends DensityMD {
    public static final String NAME = "Blue-density";
    public String GetName() {return NAME;}

    public BlueDensityMD(){
        super();
    }

    public double GetMeasure() {
        return (double) CountState(1) / (double) (GetSize() - CountState(3));
    }
}
