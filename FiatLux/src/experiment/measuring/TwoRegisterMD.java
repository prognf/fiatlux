package experiment.measuring;

import components.allCells.TwoRegisterCell;
import components.arrays.GenericCellArray;
import components.types.IntC;
import experiment.samplers.CAsampler;
import topology.basics.PlanarTopology;

abstract public class TwoRegisterMD extends CAmeasuringDevice {
	
	private GenericCellArray<TwoRegisterCell> m_array;
	private PlanarTopology m_topo;
	
	public void LinkTo(CAsampler in_Sampler) {
		m_topo = (PlanarTopology)in_Sampler.GetTopologyManager();
		m_array= in_Sampler.GetArrayAsList(); 
	}
	
	protected int GetStateOne(int pos){
		return m_array.GetCell(pos).GetStateOne();
	}
	
	protected int GetStateTwo(int pos){
		return m_array.GetCell(pos).GetStateTwo();
	}
	
	
	protected int GetSize(){ 
		return m_array.GetSize();
	}
	
	
	protected PlanarTopology GetTopology() {
		return m_topo;
	}

	final public int GetXFromIndex(int i) {
		return i % m_topo.GetXsize();
	}

	final public int GetYFromIndex(int i) {
		return i / m_topo.GetXsize();
	}
	
	final public IntC GetXYFromIndex(int i) {
		return new IntC(GetXFromIndex(i),GetYFromIndex(i));
	}
	
}
