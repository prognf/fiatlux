package experiment.measuring;

import experiment.samplers.SuperSampler;
import main.Macro;

/*
 * For the use with DataPlotter, MeasuringDevice should implement:
 * 1) an empty constructor that is PUBLIC
 * 2) a NAME attribute (used as a key in the table)
 */
abstract public class MeasuringDevice {

	public MeasuringDevice() {
		Macro.print(5, "new measuring device...");
	}
	
	abstract public void LinkTo(SuperSampler sampler);
	
	/**  name of the measuring device */
	abstract public String GetName();
	

	
}
