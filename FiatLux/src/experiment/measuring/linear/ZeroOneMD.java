package experiment.measuring.linear;

import experiment.measuring.OneRegisterMD;
import experiment.measuring.general.DecimalParameterMeasurer;
import main.Macro;

/*--------------------
 * used to measure density of 11
 *--------------------*/
public class ZeroOneMD extends OneRegisterMD implements DecimalParameterMeasurer {

	public final static String NAME="ZeroOneMD";
	final static int DEFAULTSTATE=1;

	/* name of the measure */
	public String GetName(){return NAME;}

	public ZeroOneMD(){
	}

	/*--------------------
	 * Calculus methods
	 --------------------*/

	final boolean Match(int indexi, int indexj){
		return (GetState(indexi)==0) &&  (GetState(indexj)==1);
	}

	/** we assume torical topology **/
	public double GetMeasure() {
		try{
			int count=0, size = GetSize();
			// for all cells of the automaton
			for (int i = 0; i < size -1 ; i++) {
				if (Match(i,i+1) )
					count++;
			}
			if ( Match(size-1,0)){
				count++;
			} 
			return (double) count / (double) size;
		} catch (NullPointerException e){
			Macro.FatalError(e,"Failed to use measuring device :" +
					" check the use of LinkTo before");
			return Macro.ERRORdouble;
		}
	}

	/*--------------------
	 * test methods
	 --------------------*/



}
