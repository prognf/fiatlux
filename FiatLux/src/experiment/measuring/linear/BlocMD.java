package experiment.measuring.linear;

import components.types.FLsignal;
import components.types.RuleCode;
import experiment.measuring.OneRegisterMD;
import experiment.measuring.general.DecimalParameterMeasurer;
import experiment.samplers.CAsampler;
import initializers.CAinit.BinaryInitializer;
import main.Macro;

/*--------------------
 * used to measure density of 11
 *--------------------*/
public class BlocMD extends OneRegisterMD implements DecimalParameterMeasurer {

	public final static String CLASSNAME="BlocMD";
	public final static String NAME="bloc11";
	final static int DEFAULTSTATE=1;
	final private int m_state;

	/* name of the measure */
	public String GetName(){return NAME;}

	public BlocMD(){
		m_state = DEFAULTSTATE;
	}

	public BlocMD(int state){
		m_state = state;
	}

	/*--------------------
	 * Calculus methods
	 --------------------*/

	final boolean Compare(int indexi, int indexj){
		return (GetState(indexi)==m_state) &&  (GetState(indexj)==m_state);
	}

	/** we assume torical topology **/
	public double GetMeasure() {
		try{
			int count=0, size = GetSize();
			// for all cells of the automaton
			for (int i = 0; i < size -1 ; i++) {
				if (Compare(i,i+1) )
					count++;
			}
			if ( Compare(size-1,0)){
				count++;
			} 
			return (double) count / (double) size;
		} catch (NullPointerException e){
			Macro.FatalError(e,"Failed to use measuring device :" +
					" check the use of LinkTo before");
			return Macro.ERRORdouble;
		}
	}

	/*--------------------
	 * test methods
	 --------------------*/

	static public void DoTest() {
		Macro.BeginTest(CLASSNAME);

		CAsampler samp = CAsampler.GetECASampler(new RuleCode(142),100);

		// linking the measuring device to the automaton
		BlocMD Ext = new BlocMD();
		Ext.LinkTo(samp);
		//OLD samp.LinkMeasuringDevice(Ext);

		double dens = 0.5;
		BinaryInitializer init=  (BinaryInitializer)samp.GetInitializerAsSuper();
		init.SetInitRate(dens);
		init.ReceiveSignal(FLsignal.init);

		Macro.print(" d. bloc11  measured :" + Ext.GetMeasure());

		init.SetInitRate(dens/2);
		init.ReceiveSignal(FLsignal.init);

		Macro.print(" d. bloc11 measured :" + Ext.GetMeasure());

		Macro.EndTest();
	}



}
