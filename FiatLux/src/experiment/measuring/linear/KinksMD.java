package experiment.measuring.linear;

import experiment.measuring.OneRegisterMD;
import experiment.measuring.general.DecimalParameterMeasurer;


/*--------------------
 * used to measure density of "kinks", ie 01 or 10 blocks
 * in a OneRegisterCells array
 * we assume torical topology
 * @author Nazim Fates
*--------------------*/
public class KinksMD extends OneRegisterMD implements DecimalParameterMeasurer {

	public final static String CLASSNAME="KinksMD";
	public final static String NAME="kinks";
	
	/* name of the measure */
	public String GetName(){return NAME;}
		
	public KinksMD(){
	
	}
	
	/*--------------------
	 * Calculus methods
	 --------------------*/
	
	/** we assume torical topology **/
	public double GetMeasure() {
		int count=0, size = GetSize();
		// for all cells of the automaton
		for (int i = 0; i < size -1 ; i++) {
			if (GetState(i) !=  GetState(i+1) )
				count++;
		}
		if (GetState(size -1) != GetState(0)){
			count++;
		} 
		return (double) count / (double) size;
	}


	/*--------------------
	 * test methods
	 --------------------*/

	
}
