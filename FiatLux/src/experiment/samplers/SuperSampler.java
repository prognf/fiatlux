package experiment.samplers;

import javax.swing.JComponent;

import components.arrays.TimeReadable;
import components.types.FLsignal;
import components.types.Monitor;
import components.types.MonitorList;
import grafix.gfxTypes.elements.FLPanel;
import grafix.gfxTypes.imgFormat.ImgFormat;
import grafix.plotters.DataPlotter;
import grafix.viewers.AutomatonViewer;
import grafix.windows.SimulationWindowSingle;
import initializers.SuperInitializer;
import main.Macro;

/*-------------------------------------------------------------------------------
- The main goal of this class is to maintain a list of monitors 
- and to distribute the signals to them
------------------------------------------------------------------------------*/
abstract public class SuperSampler implements TimeReadable, Monitor {

	/** application of the init signal to the system **/ 
	abstract protected Monitor GetLinkedSystemAsMonitor();

	/** for I/O **/
	abstract protected String GetModelName();

	abstract public String GetSimulationInfo();
	abstract public String GetTopologyInfo();
	abstract public int GetTime();

	abstract public FLPanel GetSimulationWidgets(); // Sidebar panels
	abstract public FLPanel GetSimulationRuleControls(); // Top panel
	abstract public JComponent GetSimulationView();
	abstract public AutomatonViewer GetViewer();

	/** works with an association key -> object 
	 * see MeasuringAndPlotter area **/
	abstract public String [] GetPlotterSelection();

	/*--------------------------------------------------------------------
	 * ATTRIBUTES
	 * ------------------------------------------------------------------*/

	private MonitorList m_MonitorList = new MonitorList();

	private static final String NORECORDSUFFIX = null; 
	protected String m_RecordSuffix= NORECORDSUFFIX ; // insertion in the name of the file
	protected ImgFormat m_RecordFormat= ImgFormat.DEFIMGSAVE; // insertion in the name of the file

	/*--------------------------------------------------------------------
	 * SIGNALS
	 * ------------------------------------------------------------------*/

	/** transmits the signal to system + list of monitors **/
	final public void sig_Init(){
		Monitor system = GetLinkedSystemAsMonitor();
		if (system!=null){
			system.ReceiveSignal(FLsignal.init);
		}
		m_MonitorList.PrintComponents();
		m_MonitorList.ReceiveSignal(FLsignal.init);
		//TODO: why is this step necessary here and not in NextStep ?
		m_MonitorList.ReceiveSignal(FLsignal.update);
	}

	/** advances one step and updates monitors **/
	final  public void sig_NextStep(){
		GetLinkedSystemAsMonitor().ReceiveSignal(FLsignal.nextStep);
		m_MonitorList.ReceiveSignal(FLsignal.nextStep);
	}

	/** updates the monitors after an externalsig_Update change of the system */
	final public void sig_Update(){
		m_MonitorList.ReceiveSignal(FLsignal.update);
	}

	/** advances by T steps before updating monitors **/
	final public void NstepsAndUpdate(int T){
		for (int i=0; i<T; i++){
			Monitor automaton = GetLinkedSystemAsMonitor();
			automaton.ReceiveSignal(FLsignal.nextStep);
		}
		m_MonitorList.ReceiveSignal(FLsignal.nextStep);
	}

	/** advances by T steps, continuously updates monitors **/
	final public void NstepsWithNupdates(int T){
		for (int i=0; i<T; i++){
			Monitor automaton = GetLinkedSystemAsMonitor();
			automaton.ReceiveSignal(FLsignal.nextStep);
			m_MonitorList.ReceiveSignal(FLsignal.nextStep); 
		}
	}

	/** for interface InteractiveSimulable **/
	public void sig_Close(){		
		for (Monitor monitor : m_MonitorList) {
			if (monitor instanceof DataPlotter){
				((DataPlotter)monitor).sig_Close();
			}
		}	
	}

	public void ReceiveSignal(FLsignal sig) {
		switch(sig){
		case init:
			this.sig_Init();
			break;
		case nextStep:
			this.sig_NextStep();
			break;
		case update:
			this.sig_Update();
			break;
		}
	}


	/*--------------------------------------------------------------------
	 * UPDATABLES-MANAGEMENT
	 * ------------------------------------------------------------------*/

	public void addMonitor(Monitor monitor) {
		//Macro.Debug("adding monitor of type: " + Macro.GetClassName(monitor));
		if (monitor instanceof SuperInitializer) {
			for (Monitor monitorCursor : m_MonitorList) {
				if (monitorCursor instanceof SuperInitializer) {
					Macro.fPrint(" Monitor list can contain only one InitDevice... replacing");
					m_MonitorList.remove(monitorCursor);
					return;
				}
			}
		}
		if (m_MonitorList.contains(monitor)){
			Macro.SystemWarning(
					"You are trying to add a monitor that is already in the list of monitors"); 
		} else {
			m_MonitorList.Add(monitor);
		}
	}

	public void AddMonitors(Monitor monitorA, Monitor monitorB) {
		addMonitor(monitorA);
		addMonitor(monitorB);
	}
	
	/** for the replacement of a monitor **/
	public void removeMonitor(Monitor monitor) {
		m_MonitorList.remove(monitor);
	}

	/*--------------------------------------------------------------------
	 * I/O MANAGEMENT
	 * ------------------------------------------------------------------*/

	protected String GetMessage(){
		return GetModelName() + " T:" + GetTime();
	}

	public void io_SetRecordSuffix(String in_suffix) {
		m_RecordSuffix= in_suffix;
	}

	public void io_SetRecordFormat(ImgFormat format){
		m_RecordFormat= format;
	}



	public void io_SaveImage(String filename, AutomatonViewer viewer) {
		Macro.print(3,"exporting image with format " + m_RecordFormat);
		if (viewer!=null){
			viewer.io_Save(filename, GetMessage(), m_RecordFormat);
		} else {
			Macro.SystemWarning("Empty viewer - cannot save image !");
		}		
	}

	public String getFileName() {
		int time = GetTime();
		String filename= Macro.GetFileName(GetModelName(), time, m_RecordSuffix);
		return filename;
	}

	/*--------------------------------------------------------------------
	 * others
	 * ------------------------------------------------------------------*/


	/** for testing **/
	public void ShowInWindow() {
		SimulationWindowSingle sw = new SimulationWindowSingle("exp", this);
		sw.BuildAndDisplay();
	}

	//OC
	/** Specific panel for initialisation **/
	public FLPanel GetSamplerSpecificInitPanel(){
		return null;
	}
	//\OC
}
