package experiment.samplers;

import models.CAmodels.CellularModel;
import topology.basics.LinearTopology;
import topology.basics.SamplerInfoLinear;

/*--------------------
 * used for experiments in 1D 
 * no viewer associated
 * use LinearSimulationSampler when a viewer is needed
 * @author : Nazim Fates
*--------------------*/
public class LinearSampler extends CAsampler {

	public LinearSampler(SamplerInfoLinear info) {
		super.CreateNewSampler(info);
	}
	
	public LinearSampler(int Lsize, CellularModel in_model, LinearTopology in_topology) {
		in_topology.SetSize(Lsize);
		super.CreateNewSampler(in_model, Lsize, in_topology);
	}

	/** constructor for subclasses ?? **/
	protected LinearSampler() {
	
	}

	
	
}
