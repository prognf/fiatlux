package experiment.samplers;
import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.IOException;

import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import javax.swing.JTextPane;

import components.allCells.SuperCell;
import components.arrays.GenericCellArray;
import components.arrays.RegularDynArray;
import components.types.FLsignal;
import components.types.IntC;
import components.types.Monitor;
import components.types.RuleCode;
import grafix.gfxTypes.IconManager;
import grafix.gfxTypes.elements.FLActionButton;
import grafix.gfxTypes.elements.FLFrame;
import grafix.gfxTypes.elements.FLPanel;
import grafix.gfxTypes.elements.FLTextField;
import grafix.gfxTypes.imgFormat.ImgFormat;
import grafix.viewers.AutomatonViewer;
import grafix.viewers.GridViewer;
import grafix.windows.StringInitPanel;
import initializers.SuperInitializer;
import initializers.CAinit.StringInitializer;
import main.Macro;
import models.CAmodels.CellularModel;
import models.CAmodels.tabled.ECAmodel;
import topology.basics.LinearTopology;
import topology.basics.PlanarTopology;
import topology.basics.SamplerInfo;
import topology.basics.SuperTopology;
import topology.basics.TopologyCoder;
import topology.zoo.RadiusN_Topology;
import updatingScheme.FullyAsynchronousScheme;
import updatingScheme.UpdatingScheme;
import updatingScheme.gfx.UpdatingSchemeSelector;

/************************************
 * embeds all info about an experiment
 * creates an automaton (memory allocation) 
 * handles the signals Reset and NextStep
 * @author Nazim Fates
 *******************************************/

public abstract class CAsampler extends SuperSampler
{

	/* gets an object ID */
	public String GetObjectID(String Wcode) {
		return Wcode;
	}

	/*--------------------
	 * attributes 
	--------------------*/

	private CellularModel m_cellularModel;
	
	protected RegularDynArray m_system;
	
	protected SuperTopology m_topologyManager;
	
	protected SuperInitializer m_initializer;

	protected UpdatingScheme m_updatingScheme;

	protected AutomatonViewer m_viewer;

	/** Used for the InitPanel **/
	private final static String TXT_CLEAR = "Clear";
	private final static String TXT_IMPORT = "Import from file";
	private final static String TXT_INITSTRING = "Create your initialisation";
	private FLFrame m_initFromString;
	private JTextField m_cmd;
	private JTextPane m_errorMsg;


	/*--------------------
	 * construction
	--------------------*/

	/** main creation method 
	 * check that use of initializer is consistent **/
	protected void CreateNewSampler(
			CellularModel model, int Lsize, SuperTopology topo,
			UpdatingScheme scheme){
		SetCellularModel( model );
		m_topologyManager = topo;
		//Macro.Debug("Lsize : " + Lsize + " model: " + m_CellularModel.GetName());

		GenericCellArray<SuperCell> temp = 
				m_cellularModel.GetNewCellArray(Lsize);
		m_system = new RegularDynArray(temp);
		m_topologyManager.BuildTopology(m_system, m_cellularModel);

		// upd. scheme association
		this.SetUpdatingScheme(scheme);

		// init device
		if (m_initializer == null){
			Macro.print(5,"default initializer associated ");
			this.SetInitDevice(model.GetDefaultInitializer());
		} else {
			Macro.SystemWarning(" There might be a problem with initializer : check init, default was used ");
		}
		Macro.print(5,"New Sampler activated...");
	}

	/** we assume automaton's size is already set  
	 * updating scheme associated by default */
	protected void CreateNewSampler(
			CellularModel in_model, int Lsize, SuperTopology in_topo){
		// creates a default updating scheme	
		UpdatingScheme scheme = in_model.GetDefaultUpdatingScheme();
		Macro.print(5,"default updating scheme associated: " + scheme.GetName());
		CreateNewSampler(in_model,  Lsize, in_topo, scheme);

	}

	/** creation of a sampler from a SamplerInfo item */
	public void CreateNewSampler(SamplerInfo info) {
		UpdatingScheme us= 
				UpdatingSchemeSelector.GetUpdatingSchemeFromName(info.updatingSchemeName);
		if (us==null){
			CreateNewSampler(info.model, info.GetLSize(), info.GetTopology());
		} else {
			CreateNewSampler(info.model, info.GetLSize(), info.GetTopology(), us);
		}
	}



	public void CreateNewSampler(CellularModel in_model, IntC XYsize, PlanarTopology in_topo) {
		CreateNewSampler(in_model, XYsize.prodXY(), in_topo);
	}


	/*--------------------
	 * signals
	--------------------*/

	/** resets (empties) the array and updates the viewer **/
	final public void ClearArray(){
		m_system.sig_Init();
		m_viewer.ReceiveSignal(FLsignal.init); // For 1D viewer
		super.sig_Update();
	}


	/*--------------------
	 * Get & Set methods
	--------------------*/

	/* A. DynSystem */

	/** mainly used by GenericCellArray **/
	public final RegularDynArray GetAutomaton() {
		return m_system;
	}

	@Override
	final protected Monitor GetLinkedSystemAsMonitor() {
		return m_system;
	}

	public void SetFullyAsynchronousUpdatingScheme(){
		FullyAsynchronousScheme scheme = new FullyAsynchronousScheme();
		this.SetUpdatingScheme(scheme);
	}

	public UpdatingScheme GetUpdatingScheme() {
		return m_updatingScheme;
	}	

	/* B. Initializer */

	private void SetInitDevice(SuperInitializer initializer) {
		m_initializer = initializer; // CAST to check
		if (m_system == null)
			Macro.FatalError(this, " Please link system to sampler before the initializer ");
		if (m_initializer == null){
			Macro.FatalError("Empty initializer");
		} else{
			//Macro.Debug("Associating init to sampler, model :" + GetModelName());
			m_initializer.LinkTo(m_system, m_topologyManager);
		}
		addMonitor(m_initializer);
	}

	/** sets the Initializer and links it to the automaton and topology manager **/
	public void SetInitDeviceFromOutside(SuperInitializer initializer) {
		removeMonitor(m_initializer);
		SetInitDevice(initializer);
	}

	/** gets the Initializer as Array */
	public SuperInitializer GetInitializer() {
		return m_initializer;
	}

	/** gets the Initializer as Super */
	public SuperInitializer GetInitializerAsSuper() {
		return m_initializer;
	}

	/* C. Cellular Model */
	public void SetCellularModel(CellularModel model) {
		m_cellularModel = model;
		addMonitor(m_cellularModel);
	}

	public CellularModel GetCellularModel() {
		return m_cellularModel;
	}

	/* -> Model Name (e.g. "ECA232" ) **/
	public String GetModelName() {
		return m_cellularModel.GetName();
	}

	public void SetAutomatonViewer(AutomatonViewer in_AutomatonViewer) {
		m_viewer= in_AutomatonViewer;
		addMonitor(m_viewer);
	}

	final public AutomatonViewer GetAutomatonViewer() {
		return m_viewer;
	}

	/* E. Topology Manager */

	public int GetTopologyDimension() {
		return m_topologyManager.GetDimension();
	}

	public SuperTopology GetTopologyManager(){
		return m_topologyManager;
	}

	/*--------------------
	 * I/O
	 *------------------*/

	/** for saving images **/
	public void io_SaveImage() {
		io_SaveImage(getFileName());
	}

	/** for saving images **/
	public void io_SaveImage(String filename) {
		io_SaveImage(filename, m_viewer);
	}
	
	/** for saving images **/
	public void io_SaveImage(String filename, ImgFormat format) {
		io_SetRecordFormat(format);
		io_SaveImage(filename);
	}


	/*--------------------
	 * Get & Set methods for PARAMETERS
	--------------------*/

	/* returns the current time during experiment */
	public int GetTime() {
		return m_system.GetTime();
	}

	/* model & topology */
	//MODIF
	public String GetSamplerInfo() {
		// GetModelName() instead of m_CellularModel.GetName()
		String s = "Model: " + GetModelName() + " @ "
				+ m_topologyManager.GetName() + " <"
				+ m_topologyManager.GetBoundaryConditions() + " > LSize: "
				+ GetAutomaton().GetSize();
		return s;
	}

	public String GetTopologyName() {
		return m_topologyManager.GetName() ;
	}

	public String GetBoundaryCondition() {
		return m_topologyManager.GetBoundaryConditions() ;
	}

	public int GetSize() {
		return m_system.GetSize();
	}

	public int GetInitSeed() {
		return m_initializer.GetSeed();
	}

	public int GetModelSeed() {
		return m_cellularModel.GetSeed();
	}
	
	//--------------------------------------------------------------------------
	//- SET methods
	//--------------------------------------------------------------------------

	public void SetUpdatingScheme(UpdatingScheme scheme) {
		m_updatingScheme= scheme;
		scheme.LinkTo(m_system, m_topologyManager); 
		GetAutomaton().SetUpdatingScheme(scheme);
	}

	public void SetSeeds(int seedini, int seedmodel, int seedupdate) {
		m_initializer.SetSeed(seedini);
		m_cellularModel.SetSeed(seedmodel);
		m_updatingScheme.SetSeed(seedupdate);
	}

	public void print(){
		String info = GetSamplerInfo();
		String scheme = GetUpdatingScheme().GetName();
		Macro.print(" Time : " + GetTime() + "  sampler: " + info +  "  upd. scheme:" + scheme);
		GetAutomaton().PrintCompactInfo(); //print();
		Macro.SkipLines(2);
	}

	public static LinearSampler GetECASampler(RuleCode W, int Lsize) {
		CellularModel model = new ECAmodel(W);
		LinearTopology 	topo = new RadiusN_Topology(1);
		topo.SetSize(Lsize);

		LinearSampler samp = new LinearSampler(Lsize, model, topo);
		//samp.SetInitDevice(new initializers.CAinit.BinaryInitializer());
		return samp;
	}


	/** used mainly by measuring device to have an array of cell type
	 * which is MORE SECIFIC **/
	public <Z extends SuperCell> GenericCellArray<Z> GetArrayAsList() {
		return m_system.GetArrayAsCastedList(); 
	}

	@Override
	/** to be included as a part of the simulation window **/
	public FLPanel GetSimulationRuleControls() {
		FLPanel modelpanel = GetCellularModel().GetSpecificPanel();
		
		if (modelpanel != null) {
			// adding seed control
			//FIXME: the line below does not work, why ?
			//modelpanel.Add(GetCellularModel().GetRandomizer().GetSeedControl());
			modelpanel.setOpaque(false);
		}

		return modelpanel;
	}
	

	@Override
	public String GetSimulationInfo() {
		return GetCellularModel().GetName();
	}

	@Override
	public String GetTopologyInfo() {
		return null;
	}

	@Override
	public FLPanel GetSimulationWidgets() {
		return null;
	}

	@Override
	public JComponent GetSimulationView() {
		return null;
	}

	@Override
	public AutomatonViewer GetViewer() {
		return m_viewer;
	}

	@Override
	public String[] GetPlotterSelection() {
		return null;
	}

	//OC
	/** Specific panel in InitPanel : specificities for CA **/
	public FLPanel GetSamplerSpecificInitPanel(){
		FLPanel initPanel = new FLPanel();
		initPanel.SetGridBagLayout();
		ClearButton clr = new ClearButton();
		FLPanel seedC = GetInitializer().GetRandomizer().GetSeedControl();
		ImportInitStateButton initStateButton = new ImportInitStateButton();
		InitFromStringButton initFromStringButton = new InitFromStringButton();
		m_cmd = new FLTextField(50) {
			@Override
			public void parseInput(String input) {

			}
		};
		Font font = new Font("SansSerif", Font.PLAIN, 16);
		m_cmd.setFont(font);
		m_cmd.setLocation(10,10);






		GoInitButton go = new GoInitButton();



		initPanel.AddGridBag(clr, 0, 20);
		initPanel.AddGridBag(seedC, 0, 30);
		initPanel.AddGridBag(initStateButton,0,40);
		initPanel.AddGridBag(initFromStringButton,0,50);
		//sub.AddGridBag(m_cmd,0,50);
		//sub.AddGridBag(go, 0,60);
		//installBlockPanelStyle(true, clr);
		//FLPanel pane =  new FLPanel();




		m_initFromString = new FLFrame("Create init configuration");
		//m_initFromString.AddPanel(pane);
		m_errorMsg = new JTextPane();
		m_errorMsg.setForeground(Color.red);
		m_errorMsg.setContentType("text/html");

		m_initFromString.addPanel(new StringInitPanel(m_cmd, go, m_errorMsg));
		return initPanel;
	}
	//\OC


	/** Elements for initPanel **/

	/** clear triggers an Init signal to the linked system */
	class ClearButton extends FLActionButton {
		public ClearButton() {
			super(TXT_CLEAR);
			setIcon(IconManager.getUIIcon("newMini"));
			AddActionListener(this);
		}

		@Override
		public void DoAction() {
			ClearArray();
		}
	}

	//OC
	/** create a initializer from a text file */
	class ImportInitStateButton extends FLActionButton {
		public ImportInitStateButton() {
			super(TXT_IMPORT);
			setIcon(IconManager.getUIIcon("openMini"));
			//AddActionListener(this);
		}

		@Override
		public void DoAction() {
			JFileChooser fc = new JFileChooser();
			int returnValue = fc.showOpenDialog(this);
			if(returnValue==fc.APPROVE_OPTION) {
				File file = fc.getSelectedFile();
				//InitFromTxt initializer = new InitFromTxt();
				GridViewer grid = (GridViewer)(GetViewer());
				int width = grid.GetXYsize().X();
				int height = grid.GetXYsize().Y();
				int nbStates = GetCellularModel().GetPalette().GetSize();
				StringInitializer initializer = null;
				//try {
					initializer = new StringInitializer(file.getPath(),width,height,nbStates);
				//} catch (IOException e) {
				//	e.printStackTrace();
				//}
				//initializer.GetSpecificPanel(file.getPath());

				//SetInitDeviceFromOutside(initializer);
				//sig_Update();
				SetInitDeviceFromOutside(initializer);
				sig_Init();
			}
		}
	}

	/** create a initializer from a string **/
	class InitFromStringButton extends FLActionButton {
		public InitFromStringButton() {
			super(TXT_INITSTRING);
			setIcon(IconManager.getUIIcon("openMini"));
			AddActionListener(this);
		}

		@Override
		public void DoAction() {
			m_initFromString.packAndShow();

		}
	}
	public class GoInitButton extends FLActionButton{
		public GoInitButton(){
			super("Go !");
			AddActionListener(this);
		}
		@Override
		public void DoAction() {
			String str = m_cmd.getText();
			StringInitializer initializer = null;
			int nbStates = GetCellularModel().GetPalette().GetSize();
			int dim = TopologyCoder.getTopologyDimension(GetCellularModel().GetDefaultAssociatedTopology());
			GridViewer grid = (GridViewer)(GetViewer());

			int width = grid.GetXYsize().X();
			int height = grid.GetXYsize().Y();
			try {
				initializer = new StringInitializer(str, dim, width,height,nbStates);
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				Macro.SystemWarning(e, "problem when analyzing the string");
			}
			//initializer.GetSpecificPanel(file.getPath());
			m_errorMsg.setText(initializer.getErrorMessage());
			if(initializer!=null){
				SetInitDeviceFromOutside(initializer);
			}

			sig_Init();
			sig_Update();


		}
	}
	//\OC

}