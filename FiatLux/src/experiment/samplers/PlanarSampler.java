package experiment.samplers;

import components.types.IntC;
import grafix.viewers.AutomatonViewer;
import models.CAmodels.CellularModel;
import topology.basics.PlanarTopology;

/*--------------------
 * used for experiments in 2D 
 * 
 * @author : Nazim Fates
*--------------------*/
public class PlanarSampler extends CAsampler {
	public PlanarSampler(IntC XYsize, CellularModel model, PlanarTopology topology) {
		topology.SetSize(XYsize);
		super.CreateNewSampler(model, XYsize, topology);
	}

	/** associates an automaton viewer with the given pixel & border size */
	public void AddDefaultViewer(IntC SquareDim){
		CellularModel model = GetCellularModel();
		AutomatonViewer viewer= 
			model.GetPlaneAutomatonViewer
			(SquareDim, GetTopologyManager(), GetAutomaton());
		this.SetAutomatonViewer(viewer);
	}

}
