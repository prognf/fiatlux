package experiment.samplers;


import components.types.FLString;
import components.types.FLStringList;
import components.types.IntC;
import components.types.IntCm;
import components.types.IntegerList;
import experiment.toolbox.ConfigRecorder;
import experiment.toolbox.ReadWrite;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.IconManager;
import grafix.gfxTypes.MacroGFX;
import grafix.gfxTypes.elements.FLActionButton;
import grafix.gfxTypes.elements.FLBlockPanel;
import grafix.gfxTypes.elements.FLButton;
import grafix.gfxTypes.elements.FLPanel;
import grafix.gfxTypes.imgFormat.ImgFormat;
import grafix.viewers.AutomatonViewer;
import grafix.viewers.GridViewer;
import grafix.viewers.LineAutomatonViewer;
import grafix.viewers.PddlViewer;
import grafix.viewers.RegularAutomatonViewer;
import grafix.windows.SimulationWindowSingle;
import initializers.CAinit.InitPanel;
import initializers.CAinit.StringInitializer;
import main.FiatLuxProperties;
import main.Macro;
import models.CAmodels.CellularModel;
import models.CAmodels.binary.ParityModel;
import models.CAmodels.nState.RuleImporterModel;
import models.CAmodels.tabled.TotalisticModel;
import models.CAmodelsvar.aperiodicTiling.AperiodicTilingViewer;
import models.CAmodelsvar.hyperbolic.HyperbolicTopology;
import models.CAmodelsvar.hyperbolic.HyperbolicViewer;
import topology.basics.LinearTopology;
import topology.basics.PlanarTopology;
import topology.basics.SamplerInfoLinear;
import topology.basics.SamplerInfoPlanar;
import updatingScheme.PDDLsequentialScheme;
import updatingScheme.UpdatingScheme;

/*--------------------
 * used for simulation experiments 
 * it creates the visual context 
 * contains PDDL procedures
 *--------------------*/
public class CAsimulationSampler extends CAsampler implements ConfigRecorder {
	/*--------------------
	 * attributes
	 --------------------*/

	FLPanel m_samplerpanel;

	/*--------------------
	 * constructor
	 --------------------*/

	public CAsimulationSampler(SamplerInfoLinear info){
		this.CreateNewSampler(info);
		// automaton Viewer management
		// cast here
		LineAutomatonViewer av= 
				this.GetCellularModel().GetLineAutomatonViewer(
						info.cellPixSize, (LinearTopology)m_topologyManager, GetAutomaton(),info.T);
		this.SetAutomatonViewer(av);
	}

	public CAsimulationSampler(SamplerInfoPlanar info2D) {
		// 2D automaton
		super.CreateNewSampler(info2D);

		// automaton Viewer management	
		AutomatonViewer av = 
				GetCellularModel().GetPlaneAutomatonViewer(info2D.cellPixSize, m_topologyManager, GetAutomaton());
		this.SetAutomatonViewer(av);

	}

	public CAsimulationSampler(HyperbolicTopology topo, CellularModel myModel) {
		// 2D automaton
		int Lsize = topo.GetSize(); 
		if (myModel==null){
			Macro.SystemWarning("Oulala : empty model");
		}
		super.CreateNewSampler(myModel, Lsize, topo);
		// automaton Viewer management	
		HyperbolicViewer av= new HyperbolicViewer(topo, GetAutomaton());
		this.SetAutomatonViewer(av);
	}
	
	public CAsimulationSampler(AperiodicTilingViewer av, CellularModel model) {
		HyperbolicTopology topo= new HyperbolicTopology();
		int Lsize=1;
		super.CreateNewSampler(model, Lsize, topo);
		// 2D automaton
		this.SetAutomatonViewer(av);
	}

	/*--------------------
	 * signals
	 --------------------*/



	/*--------------------
	 * special
	 --------------------*/


	/** buffer size
	 * works only for Linear models **/
	public int GetTsize() {
		LineAutomatonViewer view1D= (LineAutomatonViewer)m_viewer;
		return view1D.GetTsize();
	}

	/*--------------------
	 * implementation of Simulable
	 --------------------*/

	@Override
	public String [] GetPlotterSelection() {
		String [] select= null;
		int dim = GetTopologyDimension();
		switch (dim){
		case 1:
			select= GetCellularModel().GetPlotterSelection1D();
			break;
		case 2:
			select= GetCellularModel().GetPlotterSelection2D();
			break;
		case HyperbolicTopology.SPECIALDIMHYPERBOLIC:
			select = GetCellularModel().GetPlotterSelection2D();
			break;
		default:
			Macro.UserWarning("unkown topology dimension:" + dim);
		}
		return select;
	}

	@Override
	public FLPanel GetSimulationWidgets() {
		FLPanel SamplerPanel = new FLPanel();
		SamplerPanel.SetBoxLayoutY();

		// model panel
		// FLPanel modelpanel = GetCellularModel().GetSpecificPanel();
		// if (modelpanel != null) {
		// 	FLBlockPanel modelContainer = new FLBlockPanel();
		// 	FLButton modtitle = new FLButton("Rule");
		// 	modtitle.setIcon(IconManager.getUIIcon("rule"));
		// 	modelContainer.AddGridBagButton(modtitle);
		// 	modelContainer.AddGridBagY(modelpanel);
		// 	modelpanel.setVisible(false);
		// 	modelContainer.contentVisible = false;
		// 	modelpanel.setOpaque(false);
		// 	SamplerPanel.add(modelContainer);
		// 	modelContainer.defineAsWindowBlockPanel(FLColor.c_lightgrey, modtitle);
		// }

		// system panel (update mode)

		// View/Control
		FLPanel automatonpanel = null;
		try {
			UpdatingScheme scheme = GetUpdatingScheme();
			automatonpanel = scheme.GetSpecificPanel();
		} catch (NullPointerException e){
			Macro.FatalError(e,"Updating scheme might not have been initialized - check Sampler construction");
		}

		if (automatonpanel != null) {
			FLBlockPanel autoContainer = new FLBlockPanel();
			FLButton actitle = new FLButton("Synchronism");
			actitle.setIcon(IconManager.getUIIcon("sync"));
			autoContainer.defineAsWindowBlockPanel(FLColor.c_grey0, actitle, 100);
			autoContainer.AddGridBagButton(actitle);
			autoContainer.AddGridBagY(automatonpanel);
			automatonpanel.setOpaque(false);
			SamplerPanel.add(autoContainer);

			autoContainer.setTabId(FiatLuxProperties.SIDEBAR_TABS.SYNCHRONISM);
		}

		// init panel
		InitPanel initPanel = new InitPanel(this);
		SamplerPanel.Add(initPanel);

		// topology manager panel
		FLPanel topologypanel = m_topologyManager.GetSpecificPanel();
		if (topologypanel != null) {
			SamplerPanel.add(topologypanel);
			Macro.print(3, this, "Panel construction : Topology panel added.");
		}

		// TODO : PDDL Only with totalistic
		// Macro.Debug(" model : " + GetModelName());
		if (GetCellularModel() instanceof TotalisticModel){
			SamplerPanel.Add(new LoadButton()); //TODO ???
			Macro.print(3, this, "PanelInit : PDDL panel added.");
		}

		return SamplerPanel;
	}

	@Override
	public FLPanel GetSimulationView() {
		FLPanel SamplerPanel = new FLPanel();
		SamplerPanel.SetBoxLayoutY();

		// Viewer panel
		FLPanel viewerpanel = m_viewer.GetSpecificPanel();
		if (viewerpanel != null) {
			SamplerPanel.add(viewerpanel);
		} else {
			Macro.print(5, "Viewer had no specific panel");
		}

		// the viewer itself
		if (m_viewer != null) {
			SamplerPanel.add(m_viewer);
			Macro.print(3, this, "PanelInit : Viewer added.");
		} else {
			Macro.FatalError(this, "PanelInit : No Viewer found.");
		}

		return SamplerPanel;
	}

	@Override
	public String GetTopologyInfo() {
		String topo = m_topologyManager.GetName();
		String BC = m_topologyManager.GetBoundaryConditions();
		String Gridsize = GetAutomaton().GetSize() + " cells";
		return 	topo + " : " +  BC + " : " + Gridsize;
	}

	@Override
	public void io_SaveState() {
		ReadWrite.saveFileSimpleText(this);
	}

	public void io_SaveImageSVGhomemade(String filename, boolean invertYaxis) {
		filename += ImgFormat.SVG.extension();
		AutomatonViewer v = this.GetAutomatonViewer();
		if (v instanceof RegularAutomatonViewer){
			RegularAutomatonViewer gv= (RegularAutomatonViewer) v;
			IntC XY = gv.GetXYsize();
			FLStringList svgfile = new FLStringList();
			AddSVGhead(svgfile);
			for (IntCm xy= IntCm.New() ; !xy.AtEnd(XY); xy.Increment(XY)){
				int st = gv.GetCellColorNumXY(xy);
				//Macro.fDebug("houhou x,y : %s -> %d", xy, st);
				int x= xy.X(), y= xy.Y();
				if (invertYaxis){
					y= XY.Y() - 1 - y;
				}
				svgfile.Add( FormatLineSVG(x,y,st) );
			}
			AddSVGtail(svgfile);
			svgfile.WriteToFile(filename);
		} else {
			Macro.SystemWarning("The viewer can not use this SVG function.");
		}
	}

	public void io_SaveImageSVGhomemade(boolean invertYaxis) {
		String filename= getFileName();
		io_SaveImageSVGhomemade(filename, invertYaxis);
	}


	private void AddSVGhead(FLStringList svgfile) {
		svgfile.add("<svg>");
	}

	private void AddSVGtail(FLStringList svgfile) {
		svgfile.add("</svg>");
	}

	private String FormatLineSVG(int x, int y, int st) {
		String col=(st==1)?"blue":"white";		
		String line = String.format("<rect x=\"%d\" y=\"%d\" fill=\"%s\"", x, y, col);
		line += " width=\"1\" height=\"1\" stroke=\"none\" />";
		return line;
	}

	/* for PDDL export */
	public void SaveFilePDDL() {
		FLStringList automatonState = new FLStringList();

		int d= m_topologyManager.GetDimension();
		if ( d != 2){
			Macro.SystemWarning("dimension not taken into account yet...");
			return;
		}
		PlanarTopology topo2D= (PlanarTopology) m_topologyManager;
		IntC XYsize= topo2D.GetXYsize();
		int X= XYsize.X(), Y = XYsize.Y();
		String header= "X="+ X + " Y=" + Y;
		automatonState.Add(header);

		for (int y=0; y< Y; y++){
			StringBuilder line= new StringBuilder();
			for (int x=0; x< X; x++){
				int pos= topo2D.GetIndexMap2D(x, y);
				line.append(GetAutomaton().GetCell(pos).ToSimpleString());
			}
			automatonState.Add(line.toString());
		}		
		String filename = PDDLsequentialScheme.FILENAMEINIT;
		automatonState.WriteToFile(filename);
		SaveModelInfo();
	}

	private void SaveModelInfo() {
		TotalisticModel model = (TotalisticModel)GetCellularModel();
		String bits = model.GetLookUpTable().GetScode();

		FLStringList output = new FLStringList();
		output.Add(bits);
		output.WriteToFile(PDDLsequentialScheme.FILENAMERULE);

	}

	/* loading pddl solutions */
	public void LoadSolutionPDDL(){
		PlanarTopology topo2D= (PlanarTopology) m_topologyManager;
		FLStringList inputSequence;
		if ( Macro.FileExists(PDDLsequentialScheme.FILENAME_LOAD)){
			inputSequence= new FLStringList(PDDLsequentialScheme.FILENAME_LOAD);
		} else {
			Macro.SystemWarning(" file " + PDDLsequentialScheme.FILENAME_LOAD +  " not found");
			return;
		}
		// parsing input file
		int seqSZ= inputSequence.size();
		IntegerList positionSequence= new IntegerList(); 
		for(String line : inputSequence){
			int x1= line.indexOf("X") + 1 , x2= line.indexOf("-");
			int y1= line.indexOf("Y") + 1 ;
			String xval= line.substring(x1,x2), yval = line.substring(y1);
			int x= FLString.String2Int(xval), y= FLString.String2Int(yval);
			int pos = topo2D.GetIndexMap2D(x, y);
			positionSequence.addVal(pos);
		}

		//Macro.Debug(" read :" + positionSequence.toString());

		PddlViewer viewer= (PddlViewer)m_viewer;
		PDDLsequentialScheme seq = new PDDLsequentialScheme(viewer);
		this.SetUpdatingScheme(seq);
		seq.SetUpdateSequence(positionSequence);

	}


	//PDDL
	/** A button for loading configurations used for PDDDL */
	class LoadButton extends FLActionButton {

		public LoadButton() {
			super("planner solve");
			defineAsPanelButtonStylized(FLColor.c_lightblue);
		}		

		@Override
		public void DoAction() {
			try {
				SaveFilePDDL();
				Macro.ExecuteSystemCommand(PddlViewer.RUNPLANNER_COMMAND);
				LoadSolutionPDDL();
			} catch (Exception e){
				Macro.SystemWarning(e, "Failure with LOAD button");
			}

		}
	}

	static public void Test1(){
		SimulationWindowSingle exp;
		CellularModel myModel= new ParityModel();
		//CellularModel myModel= new LifeModel();
		HyperbolicTopology topo= new HyperbolicTopology();
		CAsimulationSampler mysamp = new CAsimulationSampler(topo, myModel);
		exp = new SimulationWindowSingle(MacroGFX.FLSIMULATION,mysamp);
		//
		exp.BuildAndDisplay();
		exp.sig_Init();       // init signal sent automatically
	}

	public static void main(String [] argv){
		Test1();
	}

	public void ChangeInit(){
		if(GetCellularModel() instanceof RuleImporterModel){
			StringInitializer initializer = null;
			int nbStates = 8;
			String path = "/home/ochaze/Documents/Telecom2A/fiatlux/FiatLux/src/models/CAmodels/nState/LangtonConfig.txt";
			GridViewer grid = (GridViewer)(GetViewer());

			int width = grid.GetXYsize().X();
			int height = grid.GetXYsize().Y();
			try {
				initializer = new StringInitializer(path, width,height,nbStates);
				System.out.println("New initializer");
				//FLfileReader file = FLfileReader(path);
			} catch (Exception e) {
				Macro.SystemWarning(e, "problem when analyzing the string");
			}

			SetInitDeviceFromOutside(initializer);

			sig_Init();
			sig_Update();

		}

	}

}
