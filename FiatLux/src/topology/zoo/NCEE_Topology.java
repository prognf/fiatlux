package topology.zoo;

import java.util.ArrayList;

import components.types.IntC;
import topology.basics.DXDYTable;
import topology.basics.PlanarTopology;

/*--------------------
 * class NCEE_Topology (extended Toom topology)
*--------------------*/
public class NCEE_Topology extends PlanarTopology {

	public static final String NAME = "NCE-6";

	public NCEE_Topology() {
		super();
	}

	public DXDYTable GetDXDYTable(int x, int y) {
		//  North, Center, East
		int[] DX = { 0, 0, 0, 1, 1, 2 };
		int[] DY = { 2, 1, 0, 1, 0, 0 };
		return (new DXDYTable(DX, DY));
	}

	final public String GetName(){
		return NAME;
	}
	
	/** returns an array with all possible links between two cells
	 * links are IntCouples with the two node codes  
	 */
	public ArrayList<IntC> GetGraphLinkArray() {

		int X = GetXsize();
		int Y = GetYsize();
		
		ArrayList<IntC> links= new ArrayList<IntC>();
		// adding horizontal links 
		for (int x=1; x < X -2 ; x++){
			for (int y=1; y < Y - 1; y++){
				int node1 = GetIndexMap2D(x, y), node2= GetIndexMap2D(x+1, y);
				links.add( new IntC(node1, node2));
			}
		}
		// adding vertical links 
		for (int x=1; x < X - 1 ; x++){
			for (int y=1; y < Y - 2; y++){
				int node1 = GetIndexMap2D(x, y), node2= GetIndexMap2D(x, y+1);
				links.add( new IntC(node1, node2));
			}
		}
		return links;		
		
	}
}
