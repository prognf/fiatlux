package topology.zoo;

import topology.basics.DXDYTable;
import topology.basics.PlanarTopology;

/*--------------------
 * class vonNeumannToplogyManager
*--------------------*/
public class NESW_TM extends PlanarTopology {
	public static final String NAME = "NESW-4";
	
	public static int NORTH=0, EAST=1, SOUTH=2, WEST=3;
	
	//  north, east, south, west
	public static int[] DX = { 0, 1, 0, -1 };
	public static int[] DY = { 1, 0, -1, 0 };
	
	public NESW_TM() {
		super();
	}

	public DXDYTable GetDXDYTable(int x, int y) {
		return (new DXDYTable(DX, DY));
	}

	final public String GetName(){
		return NAME;
	}
}
