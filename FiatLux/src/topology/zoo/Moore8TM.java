package topology.zoo;

import topology.basics.DXDYTable;
import topology.basics.PlanarTopology;

/*--------------------
 * A Moore8ToplogyManager implements 8 nearest neighbours and not the cell itself
 * 
 *  7 0 1
 *  6 . 2
 *  5 4 3
 * @author Nazim Fates
*--------------------*/
public class Moore8TM extends PlanarTopology {
	public static final String NAME = "Moore8";
	
	public final static int N=0, NE=1, E=2, SE=3, S=4, SW=5, W=6, NW=7; 
	
	// start NORTH and turn clockwise
	public static int[] DX = { 0, 1, 1, 1, 0, -1, -1, -1 };
	public static int[] DY = { 1, 1, 0, -1, -1, -1, 0, 1 };

	public Moore8TM() {
		super();
	}

	/* we start from center go north and then turn clockwise */
	public DXDYTable GetDXDYTable(int x, int y) {
		return (new DXDYTable(DX, DY));
	}

	final public String GetName(){
		return NAME;
	}
}
