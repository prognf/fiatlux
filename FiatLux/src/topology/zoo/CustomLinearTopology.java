package topology.zoo;

import javax.swing.JOptionPane;

import components.types.FLString;
import components.types.IntegerList;
import topology.basics.LinearTopology;
/*--------------------
 * 1D topology
 *--------------------*/
public class CustomLinearTopology extends LinearTopology {

	public static final String NAME = "Custom1D";

	/*--------------------
	 *	 attributes
	--------------------*/

	boolean m_hasBeenCalled=false;  // one shot mechanism
	IntegerList m_indexTable;

	/*--------------------
	 * construction
	 --------------------*/

	public CustomLinearTopology() {
		super();
	}

	/*--------------------
	 * Get & Set methods
	 --------------------*/

	/** main **/
	protected int [] GetDXtable() {

		if (!m_hasBeenCalled) {
			String strNeighb= JOptionPane.showInputDialog("Enter neighb. in form of list of indices");
			m_indexTable= FLString.ParseNumbers(strNeighb);
			//Macro.Debug("******** parsed:******" + m_indexTable);
			
			m_hasBeenCalled=true;
		}
		return m_indexTable.ToIntArray();
	}

	final public String GetName(){
		return NAME;
	}

}
