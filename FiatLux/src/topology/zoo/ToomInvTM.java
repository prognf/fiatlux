package topology.zoo;

import topology.basics.DXDYTable;
import topology.basics.PlanarTopology;


/*--------------------
 * class Toom variant
*--------------------*/
public class ToomInvTM extends PlanarTopology {

	public static final String NAME = "Toom-Variant";

	public ToomInvTM() {
		super();
	}

	public DXDYTable GetDXDYTable(int x, int y) {
		int[] DX = { 0, 1, 0, 1 };
		int[] DY = { 1, 0, 1, 1 };
		return (new DXDYTable(DX, DY));
	}

	final public String GetName(){
		return NAME;
	}
}
