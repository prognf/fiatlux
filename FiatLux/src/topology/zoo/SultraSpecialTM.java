package topology.zoo;

import components.types.IntegerList;
import main.Macro;
import topology.basics.DXDYTable;
import topology.basics.PlanarTopology;


/*--------------------
 * class vonNeumannToplogyManager
*--------------------*/
public class SultraSpecialTM extends PlanarTopology {


	static final public  String NAME ="Sultra";

	static final public int[] DX = { -1, 1, };
	static final public int[] DY = { 0, 0 };

	private static final int BNDRY = 1;

	public static final int LT = 25;


	public SultraSpecialTM() {
		super();
	}


	public DXDYTable GetDXDYTable(int x, int y) {
		// predecessor, north, east, south, west
		return (new DXDYTable(DX, DY));
	}

	final public String GetName(){
		return NAME;
	}

	@Override
	public void MakeGraph() {
		int size= GetSize();
		int Xsize= GetXsize();
		int Ysize= GetXsize();
		Macro.print(2,"Generating special Sultra topology graph...");

		m_automatonGraph = new IntegerList[size];
		m_NeighbMaxSize= 2;

		for (int y = 0; y < Ysize; y++) {// for each cell
			for (int x = 0; x < Xsize; x++) {// for each cell
				
				//store in graph
				IntegerList neighblist = new IntegerList();
				AddXYpositionToList(x,y,-1,neighblist);
				AddXYpositionToList(x,y,+1,neighblist);
				
				int pos= GetIndexMap2D(x, y); 
				m_automatonGraph[pos] = neighblist;
			}
		}
	}


	/** examines if a neighbour should be added in graph **/
	private void AddXYpositionToList(int x, int y, int dx, IntegerList neighblist) {
		int x2= x+dx; // do we add x2 in the neighb list ?? 
		int TesselaX = (x / LT), TesselaX2 = (x2 / LT); 
		if ( ( TesselaX == TesselaX2 ) && ( x2 >= 0 ) ) {
			int pos= GetIndexMap2D(x2, y); 
			if (pos>=GetSize()){
				Macro.FatalError(" error of computation in Sultra topology");
			}
			neighblist.addVal(pos);
		}	
	}

}
