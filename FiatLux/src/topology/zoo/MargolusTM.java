package topology.zoo;

import components.arrays.RegularDynArray;
import main.Macro;
import models.CAmodelsvar.Margolus.MargolusCell;
import models.CAmodelsvar.Margolus.MargolusModel;
import models.CAmodelsvar.Margolus.MargolusTessella;
import models.CAmodelsvar.Margolus.MargolusUpdatingScheme;
import topology.basics.DXDYTable;
import topology.basics.PlanarTopology;

/*--------------------
 * class MooreToplogyManager
*--------------------*/
public class MargolusTM extends PlanarTopology {
	
	
	final static int [] QCDX= {0,1,1,0}, QCDY =  { 0,0,1, 1};
	
	public static final String NAME = "Margolus";

	public MargolusTM() {
		super();
	}
	
	public DXDYTable GetDXDYTable(int x, int y) {
		int[] DX = { };
		int[] DY = { };
		return (new DXDYTable(DX, DY));
	}
	
	protected int GetPredecessorPosition() {
		return -1;
	}
	
	/* the model is needed because models acts on tessela not on cells
	 */
	public void setEvenOddTessela(RegularDynArray in_Automaton, MargolusModel in_Model,
			MargolusUpdatingScheme inout_scheme) {

		int x = GetXsize();
		int y = GetYsize();
		
		if ((x%2==1)||(y%2==1)||(x==0)||(y==0)) {
			Macro.UserWarning("Dimension problem: "+x+" "+y);
		} else {
			int HalfX= x/2, HalfY= y/2;
			int Ntessella = HalfX * HalfY; 
			MargolusTessella [] outTessellaEven= new MargolusTessella[Ntessella];
			MargolusTessella [] outTessellaOdd = new MargolusTessella[Ntessella];
			
			for(int i=0; i < HalfX; i++){
				for(int j=0; j < HalfY; j++){
					int x_index= 2 * i, y_index= 2*j;
					MargolusCell[] quadriCellE = new MargolusCell[4];
					MargolusCell[] quadriCellO = new MargolusCell[4];
					 
					for (int pos=0; pos <4; pos++){
						MargolusCell c= (MargolusCell) in_Automaton.GetCell(GetCellMappedIndexXY(x_index + QCDX[pos] ,y_index + QCDY[pos]  ));
						if (c == null){
							Macro.FatalError(" c is null" );
						}
						quadriCellE[pos] = 	c;
					}
					outTessellaEven[j*HalfX+i] = new MargolusTessella(in_Model,quadriCellE);
					
					for (int pos=0; pos <4; pos++){
						MargolusCell c= (MargolusCell) in_Automaton.GetCell(GetCellMappedIndexXY(x_index + QCDX[pos] + 1,y_index + QCDY[pos] +1   ));
						if (c == null){
							Macro.FatalError(" c is null" );
						}
						quadriCellO[pos] = 	c;
					}
					outTessellaOdd[j*HalfX+i] = new MargolusTessella(in_Model,quadriCellO);
				}
			}//	//for 		
		
			inout_scheme.SetTessellaEvenOdd(outTessellaEven, outTessellaOdd);
		}//
		
		
	}
	
	final public String GetName(){
		return NAME;
	}

	@Override
	public boolean isExoticTopology() {
		return true;
	}
}
