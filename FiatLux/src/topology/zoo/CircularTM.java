package topology.zoo;

import main.Macro;
import topology.basics.DXDYTable;
import topology.basics.PlanarTopology;

/*--------------------
 * A CircularToplogyManager implements Circular topology
 * 
 * @author Nazim Fates
*--------------------*/

public class CircularTM extends PlanarTopology {

	public static final String NAME = "Circular";
	double m_Radius;

	/*--------------------
	 * Constructor 
	 --------------------*/

	public CircularTM() {
	}

	/*--------------------
	 * main
	 --------------------*/
	@Override
    public DXDYTable GetDXDYTable(int x, int y) {

		DXDYTable neighb= new DXDYTable();

		// integer X and Y size to consider
		int L = (int) java.lang.Math.ceil(m_Radius - Macro.EPSILON);
		// we examine the potential other neighbours
		for (int dx = -L; dx <= L; dx++)
			for (int dy = -L; dy <= L; dy++) {
				double xdist = (double) dx - 0.5 + RandomDouble();
				double ydist = (double) dy - 0.5 + RandomDouble();
				double dist = java.lang.Math.sqrt((xdist * xdist) + (ydist * ydist));
				// if "inside" circle		
				if (dist < m_Radius)  { 
					neighb.Add(dx,dy); // we add this neighbour
				}
			}
		return neighb;
	}


	/*--------------------
	 * Get / Set 
	 --------------------*/
	/* call before any operation */
	public void SetRadius(double in_radius){
		m_Radius = in_radius;
	}

	/* returns the radius */
	public double GetRadius(){
		return m_Radius;
	}


	final public String GetName(){
		return NAME;
	}

	@Override
	public boolean isExoticTopology() {
		return true;
	}
}
