package topology.zoo;

import topology.basics.LinearTopology;


/*--------------------
 * 1D topology
*--------------------*/
public class GKL_Topology extends LinearTopology {

	public static final String NAME = "GKL";

	final public int [] DX ={-3, -1, 1, 3};
	final public int NEIGHBSIZE=4;

	/*--------------------
	 *	 attributes
	--------------------*/

	/*--------------------
	 * construction
	 --------------------*/
	
	/*--------------------
	 * Get & Set methods
	 --------------------*/
	@Override
	protected int [] GetDXtable() {
		return DX;
	}
	
	final public String GetName(){
		return NAME;
	}


	
}
