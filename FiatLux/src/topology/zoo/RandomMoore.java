package topology.zoo;

import topology.basics.DXDYTable;
import topology.basics.PlanarTopology;

public class RandomMoore extends PlanarTopology {
	public static final String NAME = "RandomMoore";
	private int m_radius = 2;

	public RandomMoore() {
		super();
	}
	
	@Override
	public DXDYTable GetDXDYTable(int x, int y) {
		DXDYTable neighb = new DXDYTable();

		for (int dx = -m_radius; dx <= m_radius; dx++) {
			for (int dy = -m_radius; dy <= m_radius; dy++) {
				if (RandomEventDouble(0.35)) {
					neighb.Add(dx, dy);
				}
			}
		}

		return neighb;
	}
	
	final public String GetName(){
		return NAME;
	}

	@Override
	public boolean isExoticTopology() {
		return true;
	}
}
