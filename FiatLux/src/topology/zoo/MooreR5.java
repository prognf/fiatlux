package topology.zoo;

import topology.basics.DXDYTable;
import topology.basics.PlanarTopology;

public class MooreR5 extends PlanarTopology {
	public static final String NAME = "MooreR5";
	private int m_XRadius = 5; //5-4 0-1
	private int m_YRadius = 5;

	public MooreR5() {
		super();
	}
	
	@Override
	public DXDYTable GetDXDYTable(int x, int y) {
		DXDYTable neighb = new DXDYTable();

		int decalageX = 0;
		int decalageY = 0;

		for (int dx = -m_XRadius - decalageX; dx <= m_XRadius + decalageX; dx++) {
			for (int dy = -m_YRadius - decalageY; dy <= m_YRadius + decalageY; dy++) {
				neighb.Add(dx, dy);
			}
		}

		return neighb;
	}
	
	final public String GetName(){
		return NAME;
	}

	@Override
	public boolean isExoticTopology() {
		return true;
	}
}
