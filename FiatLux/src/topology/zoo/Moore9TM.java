package topology.zoo;

import topology.basics.DXDYTable;
import topology.basics.PlanarTopology;


/*--------------------
 * class MooreToplogyManager
 *  8 1 2
 *  7 0 3
 *  6 5 4
*--------------------*/
public class Moore9TM extends PlanarTopology {
	public static final String NAME = "Moore9";

	public final static int C=0, N=1, NE=2, E=3, SE=4, S=5, SW=6, W=7, NW=8; 
	
	public Moore9TM() {
		super();
	}

	/* we start from center go north and then turn clockwise */
	public DXDYTable GetDXDYTable(int x, int y) {
		int[] DX = { 0, 0, 1, 1, 1, 0, -1, -1, -1 };
		int[] DY = { 0, 1, 1, 0, -1, -1, -1, 0, 1 };
		return (new DXDYTable(DX, DY));
	}

	final public String GetName(){
		return NAME;
	}
}
