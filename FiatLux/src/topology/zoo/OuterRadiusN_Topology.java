package topology.zoo;

import topology.basics.LinearTopology;


/*--------------------
 * 1D topology : central cell is not included
 *--------------------*/
public class OuterRadiusN_Topology extends LinearTopology {


	/*--------------------
	 *	 attributes
	--------------------*/
	int m_leftIndex, m_rightIndex;          // neighb. indices
	int [] m_dxtable;
	
	/*--------------------
	 * construction
	 --------------------*/

	/** excluding ZERO from neighb **/
	public OuterRadiusN_Topology(int radius) {
		super();
		m_leftIndex= -radius;
		m_rightIndex= radius;
		int neighbSz= m_rightIndex - m_leftIndex +1;
		m_dxtable= new int[neighbSz];
		for (int i = 0; i<neighbSz; i++) {
			m_dxtable[i]= m_leftIndex + i;
		}
	}



	/*--------------------
	 * Get & Set methods
	 --------------------*/
	
	@Override
	protected int[] GetDXtable() {
		return m_dxtable;
	}


	final public String GetName(){
		if (-m_leftIndex == m_rightIndex){
			return "OuterLineR" + m_leftIndex;
		} else {
			return String.format("OuterLineR[%d,%d]",m_leftIndex, m_rightIndex);
		}
	}

	

}
