package topology.zoo;

import topology.basics.DXDYTable;
import topology.basics.PlanarTopology;

/*--------------------
 * class vonNeumannToplogyManager
*--------------------*/
public class vonNeumannTM extends PlanarTopology {
	static final public  String NAME = "vonNeumann";

	static final public int NORTH=1,EAST=2,SOUTH=3,WEST=4;
	// predecessor, north, east, south, west
	static final public int[] DX = { 0, 0, 1, 0, -1 };
	static final public int[] DY = { 0, 1, 0, -1, 0 };
	
	public vonNeumannTM() {
		super();
	}

	public DXDYTable GetDXDYTable(int x, int y) {
		return (new DXDYTable(DX, DY));
	}

	final public String GetName(){
		return NAME;
	}
}
