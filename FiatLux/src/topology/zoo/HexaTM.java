package topology.zoo;

import topology.basics.DXDYTable;
import topology.basics.PlanarTopology;

/*--------------------
 * A Moore8ToplogyManager implements 8 nearest neighbours and not the cell itsel
 * @author Nazim Fates
*--------------------*/
public class HexaTM extends PlanarTopology {
	public static final String NAME = "Hexagonal";
	
	/* we start from dir. left and then turn clockwise */
	int[] DXeven = { -1, -1, 0, 1,  0, -1};
	int[] DYeven = {  0, +1, 1, 0, -1, -1};

	int[] DXodd = { -1,  0, 1, 1, +1, 0};
	int[] DYodd = {  0, +1, 1, 0, -1, -1};

	public HexaTM() {
		super();
	}


	public DXDYTable GetDXDYTable(int x, int y) {
			return (y % 2 == 0) ?
					(new DXDYTable(DXeven, DYeven)) :
					(new DXDYTable(DXodd, DYodd)) ;
	}

	final public String GetName(){
		return NAME;
	}
}
