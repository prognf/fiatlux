package topology.zoo;

import topology.basics.DXDYTable;
import topology.basics.PlanarTopology;


/*--------------------
 * class vonNeumannToplogyManager
*--------------------*/
public class Cross4TM extends PlanarTopology {

	
	static final public  String NAME ="Cross4";

	static final public int[] DX = { -1, 1, 1, -1 };
	static final public int[] DY = { 1, 1, -1, -1 };
	
	
	public Cross4TM() {
		super();
	}

	
	public DXDYTable GetDXDYTable(int x, int y) {
		// predecessor, north, east, south, west
		return (new DXDYTable(DX, DY));
	}

	final public String GetName(){
		return NAME;
	}

}
