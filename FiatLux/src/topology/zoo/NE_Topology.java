package topology.zoo;

import topology.basics.DXDYTable;
import topology.basics.PlanarTopology;

/*--------------------
 * class NCE_Topology (Toom neigborhood)
*--------------------*/
public class NE_Topology extends PlanarTopology {
	
	public static final String NAME = "NE"; //NE-2

	public NE_Topology() {
		super();
	}

	public DXDYTable GetDXDYTable(int x, int y) {
		//  North, Center, East
		int[] DX = { 0, 1 };
		int[] DY = { 1, 0 };
		return (new DXDYTable(DX, DY));
	}

	final public String GetName(){
		return NAME;
	}
	
	
}
