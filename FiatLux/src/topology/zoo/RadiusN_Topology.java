package topology.zoo;

import topology.basics.LinearTopology;

/*--------------------
 * 1D topology
*--------------------*/
public class RadiusN_Topology extends LinearTopology {
	public static final String LINERPREFIX = "LineR";
	/*--------------------
	 *	 attributes
	--------------------*/
	int m_leftIndex, m_rightIndex;          // neighb. indices
	int [] m_dxtable;
	
	/*--------------------
	 * constuction
	 --------------------*/
	
	/** symmetric neighb construction **/
	public RadiusN_Topology(int radius) {
		this(-radius,radius);
	}

	/** Asymmetric neighb construction **/
	public RadiusN_Topology(int leftIndex, int rightIndex) {
		super();
		m_leftIndex= leftIndex;
		m_rightIndex= rightIndex;
		int neighbSz= m_rightIndex - m_leftIndex +1;
		m_dxtable= new int[neighbSz];
		for (int i = 0; i<neighbSz; i++) {
			m_dxtable[i]= m_leftIndex + i;
		}
	}
	
	/*--------------------
	 * Get & Set methods
	 --------------------*/
	
	@Override
	protected int[] GetDXtable() {
		return m_dxtable;
	}
	
	final public String GetName(){
		if (-m_leftIndex == m_rightIndex){
			return LINERPREFIX + m_rightIndex; 
		} else {
			return String.format("LineA[%d,%d]",m_leftIndex, m_rightIndex);
		}
	}
}