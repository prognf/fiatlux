package topology.zoo;

import topology.basics.DXDYTable;
import topology.basics.PlanarTopology;


/*--------------------
 * A CircularToplogyManager implements Circular topology
 * 
 * @author Nazim Fates
*--------------------*/

public class ExtendedMoore extends PlanarTopology {

	public static final String NAME = "MooreR2";
	int m_Radius=2;
	final boolean m_IsCenterCellExcluded;

	/*--------------------
	 * Constructor 
	 --------------------*/
	
	/* set excludeCenterCell=true for Kaleidoscope Life */
	public ExtendedMoore(boolean excludeCenterCell) {
		m_IsCenterCellExcluded= excludeCenterCell;
	}
	
	/*--------------------
	 * main 
	 --------------------*/

	@Override
    public DXDYTable GetDXDYTable(int x, int y) {
	
		// we put the neighbours in a temporary list	
		DXDYTable neighb= new DXDYTable();
		
		// we examine the potential other neighbours
		for (int dx = -m_Radius; dx <= m_Radius; dx++)
			for (int dy = -m_Radius; dy <= m_Radius; dy++) {

				/*
				 * predecessor ?
				 */
				if (m_IsCenterCellExcluded & (dx==0) && (dy ==0))  {
					// do nothing 
				} else {
					// we add this neighbour
					neighb.Add(dx,dy);
					
				}

			} // for

		return neighb;
	}
	
	/*--------------------
	 * Get / Set 
	 --------------------*/
	/* call before any operation */
	public void SetRadius(int in_radius){
		m_Radius = in_radius;
	}
	
	/* returns the radius */
	public int GetRadius(){
		return m_Radius;
	}

	final public String GetName(){
		return NAME;
	}

	@Override
	public boolean isExoticTopology() {
		return true;
	}
}
