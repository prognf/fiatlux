package topology.basics; 
import components.types.IntC;
import components.types.IntCm;
import components.types.IntegerList;

/*--------------------
 * topologies in 2D
 * a subclass specifies a topology by defining the method GetDXDYTable(...)
--------------------*/
public abstract class PlanarTopology extends SuperTopology{

	//public final static int NORTH=0, EAST=1, SOUTH=2, WEST=3;

	private final static String CLASSNAME = "PlanarTopology";

	/**	 the derived classes specify the relative coordinates of the neighbours */
	public abstract DXDYTable GetDXDYTable(int x, int y);

	/*--------------------
	 *	 attributes
	--------------------*/
	private IntC m_XYsize;

	/*--------------------
	 *	 implementations
	--------------------*/

	/** says if are on the edges of the grid */
	protected boolean IsOnBorder(int pos) {
		IntC XY = m_XYsize.Map1Dto2D(pos);
		return ( 	(XY.X()==0) || (XY.X()== GetXsize()-1) ||
					(XY.Y()==0) || (XY.Y()== GetYsize()-1) );
	}


	/*--------------------
	 *	 construction
	--------------------*/

	public PlanarTopology() {
		super();	
	}

	public void SetSize(IntC size) {
		m_XYsize= size;
	}


	/*--------------------
	 *	public  Get & Set methods
	--------------------*/

	/** returns the dimension of the space */
	public int GetDimension(){return 2;}

	/** X size */
	final public int GetXsize(){return m_XYsize.X();} 
	/** Y size */
	final public int GetYsize(){return m_XYsize.Y();}

	public IntC GetXYsize(){return m_XYsize;}

	public int GetSize() {return m_XYsize.prodXY();}	

	/** 2D -> 1D mapping */
	final public int GetIndexMap2D(int x, int y){
		return x + y * GetXsize();
	}

	/** 2D -> 1D mapping */
	final public int GetIndexMap2D(IntC XY){
		return GetIndexMap2D(XY.X(),XY.Y());
	}

	/** 2D -> 1D mapping */
	final public int zzGetIndexMap2DAsTorus(IntCm XY){
		XY.TorusTransform(GetXYsize());
		return GetIndexMap2D(XY);
	}


	/*--------------------
	 *	local  Get & Set methods
	--------------------*/

	/** mapping function */
	final protected int GetCellMappedIndexXY(int x, int y){
		if (isBoundaryConditionToric()){
			x = (x + GetXsize()) % GetXsize();
			y = (y + GetYsize()) % GetYsize();
		}		
		return GetIndexMap2D(x,y);
	}

	/** gives the indices of the cells that are neighbours to cell pos 
	 * */
	@Override
	protected IntegerList GetNeighbourhoodTable (int pos){

		// mapping 1D -> 2D
		IntC xy= m_XYsize.Map1Dto2D(pos);

		// neighbourhood construction 
		// we connect each cell to the 5 neighbours
		DXDYTable myDXDY= GetDXDYTable(xy.X(), xy.Y());

		// IndexTable will receive the index of the neighbours of cell 
		IntegerList IndexTable= new IntegerList(); 

		for (int k=0; k< myDXDY.GetSize(); k++){

			// mapping 2D -> 1D
			IntCm xyindex= xy.DuplicateM();
			xyindex.Add(myDXDY.GetX(k), myDXDY.GetY(k));
			//	if the cell to add is "outside"
			if ( IsOutside(xyindex) ){ 
				if ( isBoundaryConditionToric() ){ // toric BC
					xyindex.TorusTransform(m_XYsize);
					int targetindex= GetIndexMap2D(xyindex);	
					IndexTable.addVal( targetindex );
				} 
			} else { // free BC : accept only those inside
				int targetindex= GetIndexMap2D(xyindex);	
				IndexTable.addVal( targetindex);
			}
		} // for k

		return  IndexTable;
	}

	final private boolean IsOutside(IntC xy){
		return 
				(xy.X() < 0) || (xy.X() >= m_XYsize.X()) ||
				(xy.Y() < 0) || (xy.Y() >= m_XYsize.Y());
	}

}//class
