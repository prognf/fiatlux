package topology.basics;

import components.types.IntC;
import experiment.samplers.CAsimulationSampler;
import main.Macro;
import models.CAmodels.CellularModel;

public class SamplerInfoLinear extends SamplerInfo {

	public int N;
	public int T;

	public SamplerInfoLinear(String topoCode, CellularModel rule) {
		super(1, topoCode, rule);
	}

	/** to be used when no viewer is required **/
	public SamplerInfoLinear(String topoCode, CellularModel rule, int n){
		super(1, topoCode, rule);
		N = n;
	}
	
	public SamplerInfoLinear(int n, int t, IntC inCellPixSize,
			CellularModel rule, String topoCode) {
		this(topoCode, rule);		
		N= n;
		T= t;
		cellPixSize= inCellPixSize;
	}

	@Override
	public SuperTopology GetTopology(){
		Macro.print(2, "### creating topology from code:" + super.topoCode);
		LinearTopology topo= 
				(LinearTopology)TopologyCoder.GetTopologyFromCode(super.topoCode);
		if (inertCellRate>0){
			topo.SetInertCellRate(inertCellRate);
		}
		if (isBorderChanging){
			topo.SetBorderChangingOn();
		}
		// TODO : the use of SetSize here is not "natural"... 
		topo.SetSize(N); 
		return topo; 	
	}

	@Override
	public CAsimulationSampler GetSimulationSampler() {
		return new CAsimulationSampler(this); 
	}

	@Override
	public int GetLSize() {
		return N;
	}
}
