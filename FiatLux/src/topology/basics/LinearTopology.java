package topology.basics;


import components.types.IntegerList;

/*--------------------
 * 1D topology
--------------------*/
abstract public class LinearTopology extends SuperTopology {

	
	/**	 the derived classes specify the relative coordinates of the neighbours 
	 * in general, it will not take the index into account */
	protected abstract int [] GetDXtable(); 
	
	/*--------------------
	 *	 attributes
	--------------------*/
	protected int m_Lsize; // nb of cells
	
	/*--------------------
	 * construction
	 --------------------*/
	
	/** use with SetSize **/
	public LinearTopology() {
		super();	
	}

	/** FIXME : call before any operation 
	 * strange operation mode */
	public void SetSize(int Lsize) {
		m_Lsize = Math.max(1, Lsize);
	}
	
	final public int GetSize() {
		return m_Lsize;
	}

	
	/*--------------------
	 * MAIN
	 --------------------*/
	
	/* not final */
	final public IntegerList GetNeighbourhoodTable(int index) {
		int [] relativeNeighbTable= GetDXtable();
		IntegerList neighbIndexL= new IntegerList();
		for (int dx : relativeNeighbTable) {
			int neighbI= dx + index;
			if (!outOfBounds(neighbI)) {
				neighbIndexL.add(neighbI);
			} else { // out of bounds
				if (isBoundaryConditionToric()) {
					int newIndex = (neighbI + m_Lsize) % m_Lsize;
					neighbIndexL.add(newIndex);
				} // esle : nothing
			}
		}
		return neighbIndexL;
	}
	
	
	/*--------------------
	 * Get & Set methods
	 --------------------*/
	
	private boolean outOfBounds(int neighbI) {
		return (neighbI<0) || (neighbI>=m_Lsize);
	}

	final public int GetDimension() {
		return 1;
	}

	
	final protected boolean IsOnBorder(int index) {	
		return ((index == 0) || (index == m_Lsize - 1));
	}
	
	
	

	
	
}
