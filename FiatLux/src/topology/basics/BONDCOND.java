package topology.basics;

import java.awt.Color;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import components.types.FLString;
import components.types.FLStringList;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.IconManager;
import grafix.gfxTypes.elements.FLButton;
import grafix.gfxTypes.elements.FLFrame;
import grafix.gfxTypes.elements.FLList;
import grafix.gfxTypes.elements.FLPanel;
import main.Macro;

public enum BONDCOND {
	Toric, Free, Inert; 
	private final static String sT= "T", sF="F", sI= "I";
	private final static String [] PREFIX_BC= {sT, sF, sI};
	private static String strTORICAL="torical", strFREE="free",strINERT="inert";
	
	/*----------------------------------------------------------------------------
	  - attributes
	  --------------------------------------------------------------------------*/

	/*---------------------------------------------------------------------------
	  - construction
	  --------------------------------------------------------------------------*/

	private static int selected = 0;

	public static void setSelected(int value) {
		selected = value;
	}

	public static int getSelected() {
		return selected;
	}

	/* creates a list with the full names*/
	public static BoundcondPanel NewListControl() {
		BoundcondPanel bPanel = new BoundcondPanel();
		bPanel.setOpaque(false);
		Insets margin = new Insets(0, 5, 0, 5);
		FLButton bToric = new FLButton(Toric.toString());
		bToric.setIcon(IconManager.getUIIcon(strTORICAL));
		bToric.setForeground(Color.BLACK);
		bToric.setBackground(FLColor.c_lightblue.brighter(1.2));
		bToric.setBorderPainted(false);
		bToric.setFocusPainted(false);
		bToric.setHorizontalTextPosition(JButton.CENTER);
		bToric.setVerticalTextPosition(JButton.BOTTOM);
		bToric.setMargin(margin);

		FLButton bFree = new FLButton(Free.toString());
		bFree.setIcon(IconManager.getUIIcon(strFREE));
		bFree.setForeground(Color.BLACK);
		bFree.setBackground(Color.WHITE);
		bFree.setBorderPainted(false);
		bFree.setFocusPainted(false);
		bFree.setHorizontalTextPosition(JButton.CENTER);
		bFree.setVerticalTextPosition(JButton.BOTTOM);
		bFree.setMargin(margin);

		FLButton bInert = new FLButton(Inert.toString());
		bInert.setIcon(IconManager.getUIIcon(strINERT));
		bInert.setForeground(Color.BLACK);
		bInert.setBackground(Color.WHITE);
		bInert.setBorderPainted(false);
		bInert.setFocusPainted(false);
		bInert.setHorizontalTextPosition(JButton.CENTER);
		bInert.setVerticalTextPosition(JButton.BOTTOM);
		bInert.setMargin(margin);

		bPanel.Add(bToric, bFree, bInert);
		bPanel.bFree = bFree;
		bPanel.bToric = bToric;
		bPanel.bInert = bInert;

		bToric.AddActionListener(bPanel);
		bFree.AddActionListener(bPanel);
		bInert.AddActionListener(bPanel);

		return bPanel;
	}
	
	/*---------------------------------------------------------------------------
	  - actions
	  --------------------------------------------------------------------------*/
	
	public static void SelectItemFromCode(String code) {
		String prefix = BONDCOND.ParsePrefixFromcode(code);
		int index = GetIndexFromPrefix(prefix);
		setSelected(index);
	}
	
	/*---------------------------------------------------------------------------
	  - get & set
	  --------------------------------------------------------------------------*/
	/* returns an index from the prefix array*/
	public static int GetIndexFromPrefix(String prefix) {	
		return FLString.FindRankStringList(prefix,PREFIX_BC);
	}
	
	public static String GetPrefixFromIndex(int pos){
		return PREFIX_BC[pos];
	}
	
	public static BONDCOND GetBCfromCode(String topoCode) {
		String prefix= ParsePrefixFromcode(topoCode);
		return GetBCfromPrefix(prefix);
	}
	
	private static BONDCOND GetBCfromPrefix(String prefix) {
		int pos= FLString.FindRankStringList(prefix, PREFIX_BC);
		return values()[pos];
	}

	/* parsing the code */
	final static public String ParsePrefixFromcode(String in_code){
		return in_code.substring(0,1);
	}	
	
	static public String [] GetNames(){
		int sz= values().length;
		String [] val= new String [sz];
		for (int i=0; i< sz; i++){
			val[i]= values()[i].toString();
		}
		return val;
	}
	
	static public FLStringList Names(){
		FLStringList names= new FLStringList();
		for (BONDCOND val : values()){
			names.Add( val.toString() );
		}
		return names;
	}
	
	public boolean IsToric() {
		return this == Toric;
	}
	
	/*---------------------------------------------------------------------------
	  - test
	  --------------------------------------------------------------------------*/
	final static String CLASSNAME= Thread.currentThread().getStackTrace()[1].getClassName();
	
	public static void DoTest(){
		Macro.BeginTest(CLASSNAME);
		FLList select = new FLList( Names().ToArray() );
		FLFrame f = new FLFrame("tst");
		f.addPanel(select);
		f.packAndShow();
		Macro.EndTest();
	}
	
	
	public final static void main(String [] arg){
		DoTest();
	}

	static class BoundcondPanel extends FLPanel implements ActionListener {
		public FLButton bFree, bToric, bInert;

		@Override
		public void actionPerformed(ActionEvent e) {
			int selected = -1;
			if (e.getSource() == bToric) {
				selected = 0;
			} else if (e.getSource() == bFree) {
				selected = 1;
			} else if (e.getSource() == bInert) {
				selected = 2;
			}

			if (selected >= 0) {
				BONDCOND.setSelected(selected);
				Color selectedColor = FLColor.c_lightblue.brighter(1.2);
				bToric.setBackground(selected == 0 ? selectedColor : Color.WHITE);
				bFree.setBackground(selected == 1 ? selectedColor : Color.WHITE);
				bInert.setBackground(selected == 2 ? selectedColor : Color.WHITE);
			}

		}
	}
}
