package topology.basics;

import components.types.IntC;
import experiment.samplers.CAsimulationSampler;
import main.Macro;
import models.CAmodels.CellularModel;

/** TODO : document the use of this class ! **/
abstract public class SamplerInfo {

	abstract public SuperTopology GetTopology();
	abstract public int GetLSize();
	abstract public CAsimulationSampler GetSimulationSampler(); // central method
	
	public IntC cellPixSize;
	public final CellularModel model;
	int dim;
	public String updatingSchemeName;
	public final String topoCode;
	
	public double inertCellRate=-1.;
	public boolean isBorderChanging = false;

	protected SamplerInfo(int inDim, String topo, CellularModel rule){
		dim= inDim;
		topoCode= topo;
		model= rule;
		if (topoCode == null){
			Macro.FatalError(" null topo code");
		} else {
		//	Macro.Debug(" topo code set to" + topoCode);
		}
	}

}
