package topology.basics; 

import components.types.IntC;
import components.types.IntegerList;
import main.Macro;

/*--------------------
 * A DXDY Table simply wraps two integer lists
--------------------*/
public class DXDYTable{
	
	private IntegerList m_Xlist, m_Ylist;
	
	public DXDYTable(IntegerList in_DX, IntegerList in_DY){
		if (in_DX.GetSize() != in_DY.GetSize()){
			Macro.FatalError("Lists of unequal sizes");
		}
		m_Xlist= in_DX; 
		m_Ylist= in_DY;
	}
	
	public DXDYTable(){
		m_Xlist = new IntegerList();
		m_Ylist = new IntegerList();
	}
	
	public DXDYTable(int [] x, int [] y){
		this( new IntegerList(x), new IntegerList(y) );
	}
	
	public int GetX(int index){
		return m_Xlist.Get(index);
	}
	
	public int GetY(int index){
		return m_Ylist.Get(index);
	}
	
	public IntC GetXY(int index){
		return new IntC(GetX(index),GetY(index));
	}

	public int GetSize(){
		return m_Xlist.GetSize();
	}

	public void Add(int Xval, int Yval){
		m_Xlist.addVal(Xval);
		m_Ylist.addVal(Yval);
	}
}


