package topology.basics;

import static topology.zoo.RadiusN_Topology.LINERPREFIX;

import components.types.FLString;
import main.Macro;
import topology.zoo.CircularTM;
import topology.zoo.Cross4TM;
import topology.zoo.CustomLinearTopology;
import topology.zoo.ExtendedMoore;
import topology.zoo.GKL_Topology;
import topology.zoo.HexaTM;
import topology.zoo.MargolusTM;
import topology.zoo.Moore8TM;
import topology.zoo.Moore9TM;
import topology.zoo.MooreR5;
import topology.zoo.NCEE_Topology;
import topology.zoo.NCE_Topology;
import topology.zoo.NESW_TM;
import topology.zoo.NE_Topology;
import topology.zoo.RadiusN_Topology;
import topology.zoo.RandomMoore;
import topology.zoo.SultraSpecialTM;
import topology.zoo.ToomInvTM;
import topology.zoo.WCE_Topology;
import topology.zoo.vonNeumannTM;
/*--------------------
 * reading codes, producing Topology objects
*--------------------*/
public class TopologyCoder {
	// correspondence String -> integer
	public static String 
	LINER1= "L1", LINER2= "L2", LINER3 = "L3", LINER4="L4", LINEA4="LA4",
	OUTERLINER1="OL1",
	LVOORHEES="LVOORHEES",
	GKL="GK",
	CUSTOM1D="C1D",
	MOORE9="M9", MOORE8="M8", MOORER5="MR5", 
	vonNeumman5="V5", NESW="V4", CROSS="X4",
	NCE="T3", //TOOM
	WCE="WCE3", NCEE="T6", TOOMINV="TINV",
	CIRCULAR="CI",
	MARGOLUS="MA",
	EXTENDEDMOORE="EM",
	HEXAGONAL="HX",
	SULTRA="SLTR",
	RANDOMMOORE="RDM",
	NE="NE",
	COMP="COMP";


	// shortcuts : use T for toric before topocode and F for free
	public final static String 
	s_TLR1="TL1", s_TLR2="TL2", s_TLR3="TL3", s_TLA4="TLA4",
	s_TV5= "TV5", s_TV4="TV4", s_FV4 = "FV4", 
	s_FM8="FM8", s_FM9="FM9", 
	s_TM8="TM8", s_TM9="TM9", 
	s_Margolus="TMA", s_TEM="TEM",
	s_GKL="TGK", s_THX="THX",
	s_FNCE="F" + NCE, s_TMR5 = "TMR5", s_TNCE = "T" + NCE,
	DEFAULT_TOPO2D= s_FM9 ;


	/*--------------------
	 * attributes
	 --------------------*/


	// !!! same order : LISTNAME, SHORTNAME
	public final static String[] SHORTNAME_2D = {
		vonNeumman5,
		NESW,
		MOORE9,
		MOORE8,
		NCE, //TOOM
		HEXAGONAL,
		CIRCULAR,
		WCE,
		EXTENDEDMOORE,
		RANDOMMOORE,
		MARGOLUS,
		NE,
		MOORER5,
	};

	public final static String[] LISTNAME_2D = { 
		vonNeumannTM.NAME,
		NESW_TM.NAME,
		Moore9TM.NAME,
		Moore8TM.NAME,
		NCE_Topology.NAME,
		HexaTM.NAME,
		CircularTM.NAME,
		WCE_Topology.NAME,
		ExtendedMoore.NAME,
		RandomMoore.NAME,
		MargolusTM.NAME,
		NE_Topology.NAME,
		MooreR5.NAME,
	};

	// !!! same order : LISTNAME, SHORTNAME 
	public final static String[] SHORTNAME_1D = {LINER1, LINER2, LINER3, LINEA4, OUTERLINER1, GKL, CUSTOM1D, COMP};
	public final static String[] LISTNAME_1D = { LINERPREFIX+"1", LINERPREFIX+"2", LINERPREFIX+"3", "LineA[-1,2]", "LineA[0,1]", // these are in tune with the GetName of RadiusN topology
			GKL_Topology.NAME, CustomLinearTopology.NAME, CompleteGraphTopology.NAME}; // is this what is dipalyed on the interface ?

	/*--------------------
	 * actions
	 --------------------*/
	public static boolean IsCodeCircular(String latticeCode) {
		return (latticeCode.startsWith(CIRCULAR));
	}

	/** parsing the code (two letters) */
	final static public String ParseCodeToLattice(String in_code){
		return in_code.substring(1);
	}

	/** parsing the radius for circular topo */
	final static public double ParseRadius(String in_code){
		//Macro.Debug("parsing code : " + in_code);
		String nioucode = in_code.replace(FLString.NEWSEPARATOR,FLString.DOTSEPARATOR);
		String rad= nioucode.substring(2);
		return FLString.ParseDouble(rad);
	}

	/*--------------------
	 * parsing
	 --------------------*/

	
	/** IMPORTANT : from Code to Topology 
	 * Topo = lattice + boundaries*/
	public static SuperTopology GetTopologyFromCode(String TopoCode){
		BONDCOND bc=null;
		try{
			bc= BONDCOND.GetBCfromCode(TopoCode);
		} catch (Exception e){
			Macro.SystemWarning(e, "problem reading this code : " + TopoCode);
		}
		String latticeCode= ParseCodeToLattice(TopoCode);
		return GetTopologyFromCode(bc, latticeCode);
	}


	/** IMPORTANT : from Code to Topology 
	 * Topo = lattice + boundaries*/
	public static SuperTopology GetTopologyFromCode(BONDCOND BondCond, String latticeCode){

		SuperTopology nioutopo= null;
		//Macro.Debug("Parsing code: " + latticeCode);
		try{
			nioutopo = GetLattice(latticeCode);
			nioutopo.SetBoundaryConditions(BondCond);
		} catch (Exception e){
			Macro.UserWarning(e, "Could not parse code:" + latticeCode);
		}
		//Macro.Debug("returned code:" + nioutopo);
		return nioutopo;
	}

	/** from code to dimension */
	public static int GetTopologyDimensionFromCode(String in_code){
		int dim=2;
		String latticeCode= ParseCodeToLattice(in_code);
		if (FLString.FindRankStringList(latticeCode, SHORTNAME_1D)>-1){
			dim=1;
		}
		return dim;
	}

	public static int getTopologyDimension(String topocode) {
		String code = ParseCodeToLattice(topocode);

		String[] l1D = {LINER1, LINER2, LINER3, LINEA4, OUTERLINER1, GKL, LVOORHEES, COMP};

		for (String l1 : l1D) {
			if (l1.equals(code)) {
				return 1;
			}
		}
		return 2;
	}

	/** associates a code to a Topology object */
	public static SuperTopology GetLattice(String topocode){
		//Macro.Debug("Lattice to find : " + topocode);
		// ******* 1D TOPOLGIES ********
		if (topocode.equals(LINER1)){
			return new  RadiusN_Topology(1);
		} else if  (topocode.equals(LINER2)){
			return new RadiusN_Topology(2);
		} else if  (topocode.equals(LINER3)){
			return new RadiusN_Topology(3);
		}  else if  (topocode.equals(LINER4)){
			return new RadiusN_Topology(4);
		} else if  (topocode.equals(LINEA4)){
			return new RadiusN_Topology(-1,2); 
		} else if (topocode.equals(OUTERLINER1)){
			return new RadiusN_Topology(0,1);
		} else if  (topocode.equals(GKL)){
			return new GKL_Topology(); 
		} else if (topocode.equals(LVOORHEES)){
			return new RadiusN_Topology(-2,0);
		} else if (topocode.equals(CUSTOM1D)) {
			return new CustomLinearTopology();
		} else if (topocode.equals(COMP)) {
			return new CompleteGraphTopology();
		}
		// ******* 2D TOPOLGIES ********
		else if  (topocode.equals(MOORE9)){
			return new Moore9TM();
		} else if  (topocode.equals(MOORE8)) {
			return new Moore8TM();
		} else if (topocode.equals(MOORER5)) {
			return new MooreR5();
		} else if (topocode.equals(RANDOMMOORE)) {
			return new RandomMoore();
		} else if  (topocode.equals(vonNeumman5)){
			return  new vonNeumannTM();
		} else if  (topocode.equals(NESW)){
			return new NESW_TM();
		} else if (topocode.equals(CROSS)){
			return new Cross4TM();
		} else if  (topocode.equals(NCE)){
			return new NCE_Topology();
		} else if  (topocode.equals(WCE)) {
			return new WCE_Topology();
		} else if (topocode.equals(NCEE)) {
			return new NCEE_Topology();
		} else if (topocode.equals(TOOMINV)) {
			return new ToomInvTM();
		} else if  (topocode.equals(HEXAGONAL)){
			return new HexaTM(); 
		} else if  (IsCodeCircular(topocode)){
			double radius= ParseRadius(topocode);
			CircularTM nioutopo= new CircularTM();
			nioutopo.SetRadius(radius);
			//Macro.Warning("Circular topo chosen : radius " + radius);
			return nioutopo;

		} else if  (topocode.equals(MARGOLUS)){
			return  new MargolusTM();
		} else if  (topocode.equals(HEXAGONAL)){
			return new HexaTM(); 
		} else if  (topocode.equals(EXTENDEDMOORE)){
			return new ExtendedMoore(true); 
		} else if  (topocode.equals(SULTRA)){
			return new SultraSpecialTM(); 
		} else if  (topocode.equals(NE)){
			return new NE_Topology(); 
		} else {
			Macro.FatalError("Toplogy not found :" + topocode);
			return null;
		}
	}

	/*--------------------
	 * testing
	 --------------------*/

	public static void DoTest() {

		Macro.BeginTest("TopologySelector");
		String topocode="FM8";
		SuperTopology MyTopology=GetTopologyFromCode(topocode);
		Macro.print(MyTopology.GetBoundaryConditions()+":"+MyTopology.GetName());
		Macro.EndTest();
	}
}
