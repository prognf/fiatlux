package topology.basics;

import components.allCells.Cell;
import components.allCells.CellCastException;
import components.allCells.OneRegisterBlindCell;
import components.allCells.SuperCell;
import components.arrays.GenericCellArray;
import components.arrays.RegularDynArray;
import components.randomNumbers.StochasticSource;
import components.types.IntegerList;
import grafix.gfxTypes.elements.FLPanel;
import main.Macro;
import models.CAmodels.CellularModel;
import models.CAmodelsvar.Margolus.MargolusModel;

/*--------------------
 * creates the "interaction graph" of a system
 * @author Nazim Fates 
*--------------------*/
public abstract class SuperTopology extends StochasticSource {

	public final static int GHOSTSTATE = -99;

	/**
	 * @param pos index of the cell in the array
	 * @return an array of int containing the indices of the cells of the neighbourhood
	 */
	abstract protected IntegerList GetNeighbourhoodTable(int pos);

	/* dimension = 1 (1D) or 2 (2D) */
	abstract public int GetDimension();

	/* is a cell on the "border" of the grid ? */
	abstract protected boolean IsOnBorder(int index);

	abstract public String GetName();

	/*--------------------
	 * Attributes
	 --------------------*/
	final static int SIZEMAX = 100; // maximum allowed size of neighbourhood
	protected int m_NeighbMaxSize;    // real max size of neighbourhood

	// boundary conditions	
	final static BONDCOND DEF_BC = BONDCOND.Toric;
	private BONDCOND m_BoundaryCondition = DEF_BC ;  

	double 
	m_HoleRate 			= 0., 		// ghost frequency 
	m_ErrorRate 		= 0., 		// errors in links (add or remove)
	m_RandomLinkRate 	= 0.,   	// random links
	m_InertCellRate		= 0.;    	// proportion of 1s in InertCells

	boolean m_addLinks = false;

	// array of positions of ghost cells
	boolean [] m_IsGhost;

	// array of IntegerLists representing the neighbourhood for each cell
	protected IntegerList[] m_automatonGraph;

	// one cell for all "ghosts" (it is an inert cell)
	final Cell m_Ghost = new OneRegisterBlindCell(GHOSTSTATE);

	// 
	private boolean m_cellChange= false; // type of border cell

	/**
	 * number of cells
	 * @return
	 */	
	public abstract int GetSize();

	
	/*--------------------
	 * Constructor
	 --------------------*/
	protected SuperTopology() {
		m_NeighbMaxSize = -1;
		Macro.print(2, "New topology");
	}

	/*--------------------
	 * Get & Set methods
	 --------------------*/
	protected boolean isBoundaryConditionToric() {
		return m_BoundaryCondition==BONDCOND.Toric;
	}
	
	/* the border cells are Blind cells which change their state */
	public void SetBorderChangingOn(){
		//Macro.Debug(" topo change on !!!");
		m_cellChange= true;
	}	

	/**
	 * Do NOT use before graph has been generated. Otherwise returns -1.
	 * @return the actual maximal size of neighbourhood
	 */
	public int GetActualNeighbMaxSize(){
		return m_NeighbMaxSize;
	}


	/* true = add links , false = remove links */
	public void SetAddLink(boolean addLinks){
		m_addLinks= addLinks;
	}

	/** override if needed **/
	public FLPanel GetSpecificPanel() {
		return null;
	}

	/** settings in[0,1] */
	public void SetHoleMissingLinkRates(double in_holeRate, double in_MissingLinkRate) {
		Macro.print(4,"hole rate set to: " + in_holeRate + " m. link. rate :" + in_MissingLinkRate);
		m_HoleRate = in_holeRate ;
		m_ErrorRate = in_MissingLinkRate ;
	}

	/** settings in [0,1]  */
	public void SetRandomLinkRate(double in_Rate) {
		Macro.print(4,"random link rate set to: " + in_Rate);
		m_RandomLinkRate = in_Rate ;
	}


	/** setting in [0,1]  */
	public void SetInertCellRate(double in_rate){
		m_InertCellRate= in_rate;
	}

	/** boundary condition TopologySelector codes */
	public void SetBoundaryConditions(BONDCOND condition) {
		m_BoundaryCondition= condition;
	}

	/** limited , toric, fixed, etc. */
	public String GetBoundaryConditions() {
		return m_BoundaryCondition.toString();
	}

	/** access to the neighb. graph for one cell **/
	public IntegerList GetNeighbourList(int pos) {
		return m_automatonGraph[pos];
	}

	/** access to the graph **/
	public IntegerList[] GetTopologyGraph(){
		return m_automatonGraph;
	}

	public boolean isExoticTopology() { return false; }

	/*--------------------
	 * Other methods
	 --------------------*/



	/**
	 * Creates the graph corresponding to the topology
	 * @param size: the size of the grid
	 */
	public void MakeGraph() {
		int size= GetSize();
		Macro.print(4,"Generating topology graph with size: " + size);

		// the first thing to do is to change some cells into holes (ghosts/empty cells)
		SetGhostList(m_HoleRate, size);

		m_automatonGraph = new IntegerList[size];

		for (int pos = 0; pos < size; pos++) {// for each cell
			//ghost cell case
			if (m_IsGhost[pos]) {
				m_automatonGraph[pos] = null;
			}			
			//normal cell case
			else {	// get neighborhood
				IntegerList neighblist = GetNeighbourhoodTable(pos);//abstract
				int neighbSize = neighblist.size();				
				//computes new max NieghbSize on the whole grid
				if (neighbSize > m_NeighbMaxSize){
					m_NeighbMaxSize= neighbSize;
				}
				/* DEBUG
				// rewire with random links
				for (int neighb=0; neighb < neighbSize; neighb++){
					if (RandomEventDouble(m_RandomLinkRate)){
						int randomneighb= RandomInt(size);
						neighblist.set(neighb, randomneighb);
					}
				}*/				
				//store in graph
				m_automatonGraph[pos] = neighblist;
			}
		}
	}

	
	/** (re)builds the neigbourhood table of each cell 
	 * we assume the size of the automaton is set
	 */
	public void BuildTopology(final RegularDynArray inout_Automaton,
			CellularModel model) {
		int Lsize = inout_Automaton.GetSize();
		//Macro.Debug("building topology with size:" + Lsize);
		
		// Build a list of cells
		this.MakeGraph();

		// verification step ??
		model.InitialiseTopologyInfo(this);

		// Build cells and put them in list
		this.BuildCellsFromGraph(inout_Automaton, model, Lsize);

		if (model instanceof MargolusModel){
			Macro.print(2," For Margolus model, no cell neighb. is constructed ");
		} else {		
			for (int pos = 0; pos < Lsize; pos++) {
				IntegerList neighblist = this.GetNeighbFromGraph(pos, inout_Automaton);
				
				GenericCellArray<SuperCell> newListcell= 
						new GenericCellArray<SuperCell>();
				for (Integer integer : neighblist) {
					Cell neighb = (Cell) inout_Automaton.GetCell(integer);
					newListcell.add(neighb);
				}
				Cell cell = (Cell) inout_Automaton.GetCell(pos);
				cell.SetNeighbourhood(newListcell);
			}

			this.print(inout_Automaton);
		}
	}

	
	
	/**
	 * Builds the cells based on the generated graph
	 * @param in_Automaton
	 * @param in_CellularModel
	 * @param in_size
	 */
	private void BuildCellsFromGraph(
			RegularDynArray in_Automaton, 
			CellularModel in_CellularModel,
			int in_size)  {
		Macro.print(7, "Generating cells from graph...");

		//check that model is not empty
		if (in_CellularModel == null)
			Macro.FatalError(this, "MakeNewCells","Cellular Model is null");

		for (int pos = 0; pos < in_size; pos++) {
			SuperCell newcell;

			//ghost cell case
			if (m_IsGhost != null && 
					m_IsGhost.length > 0 &&
					m_IsGhost[pos]) 
			{newcell= m_Ghost;}

			//border cell case
			else if (m_BoundaryCondition == BONDCOND.Inert && IsOnBorder(pos)) {
				//generates 1 with probability m_InertCellRate
				int state = RandomEventDouble(m_InertCellRate)?1:0;
				try{			
					//Macro.Debug("kiki change state " + m_cellChange);
					newcell= in_CellularModel.GetNewBlindCell(state, m_cellChange);
				} catch(CellCastException e){
					Macro.UserWarning("Could not create InertCell!, model: " + 
							in_CellularModel.GetName());
					throw e;
				}
			} else { //normal cell case
				newcell= in_CellularModel.GetNewCell();
			}
			if (newcell == null){Macro.FatalError("Empty new cell !");} else {
				in_Automaton.SetCellArray(pos, newcell);
			}
			//newcell.SetLabel("c" + pos);
		}
	}

	/**
	 * Builds the neighbourhood of the cells based the generated graph
	 * @param in_automaton
	 * @param in_automatonGraph
	 */
	private IntegerList GetNeighbFromGraph(
			int pos,
			RegularDynArray in_automaton) {
		//Macro.print(3,"Building neighb cells from graph, cell:"+pos);
		//Macro.fDebug("mautomatonGraph ha size:%d", m_automatonGraph.length);
		IntegerList neighbours = m_automatonGraph[pos];
		IntegerList iNeighbList = new IntegerList();

		if (m_IsGhost[pos]) {}

		else {
			for (int neighbPos = 0; neighbPos < neighbours.size(); neighbPos++) {
				int neighbIndex = neighbours.Get(neighbPos);

				//Cell neighb = (Cell) in_automaton.GetCell(neighbIndex);
				if ( !RandomEventDouble(m_ErrorRate)){
					if (!m_IsGhost[neighbIndex]) {
						iNeighbList.addVal(neighbIndex);
					} else if (m_addLinks){
						iNeighbList.addVal(neighbIndex);
					}

				}
			}
		}
		return iNeighbList;
	}


	/* all ghost cells are stored in this list */
	private void SetGhostList(double rate, int in_size) {
		//Macro.Debug("new ghost list");
		m_IsGhost= new boolean[in_size];
		for (int i = 0; i < in_size; i++) {
			m_IsGhost[i]= RandomEventDouble(rate);	
		}
	}

	
	/** a few checks */
	public void print(RegularDynArray array) {
		int size= array.GetSize();
		int countNotGhost=0, countNeighb=0;
		for (int i=0; i< size; i++){
			if (!m_IsGhost[i]){
				countNotGhost++; 
				Cell cell = (Cell) array.GetCell(i);
				countNeighb += cell.GetNeighbourhoodSize();
			}
		}
		float avr = countNeighb / (float) countNotGhost;
		String s1 = 
			GetName() + ", L=" + size + 
			(countNotGhost!=size? (", ghosts : "+ countNotGhost):"") +
			", avr. neighb. size =" + avr;
		Macro.print(4, "topo info :" + s1);
		//Macro.SystemWarning("info call");	
	}

}
