package topology.basics;


import main.Macro;

/*--------------------
 * 1D topology
--------------------*/
public class CompleteGraphTopology extends LinearTopology {

	
	
	public static final String NAME = "CompleteGraphTopoAAA";
	
	/*--------------------
	 *	 attributes
	--------------------*/
	int [] dxdytableAllcells;
	
	/*--------------------
	 * construction
	 --------------------*/
	
	/** use with SetSize **/
	public CompleteGraphTopology() {
		super();
	}

	@Override
	public String GetName() {
		return NAME;
	}
	
	/** FIXME : call before any operation 
	 * strange operation mode */
	final public void SetSize(int Lsize) {
		super.SetSize(Lsize);
		Macro.print(2, "@$! init of CompleteGraph topology with size:"+Lsize);
		dxdytableAllcells= new int[m_Lsize]; // init of the table
		for (int pos=0; pos<m_Lsize; pos++) {
			dxdytableAllcells[pos]=pos;
		}
	}
	
	/*--------------------
	 * Get & Set methods
	 --------------------*/
	
	@Override
	protected int[] GetDXtable() {
		return dxdytableAllcells;
	}

	

}
