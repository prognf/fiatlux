package topology.basics;

import components.types.IntC;
import experiment.samplers.CAsimulationSampler;
import main.Macro;
import models.CAmodels.CellularModel;
import models.CAmodelsvar.hyperbolic.HyperbolicTopology;
import topology.gfx.SizeParManagerTwoD;
import topology.gfx.TopologicalConditionSelector2D;

public class SamplerInfoPlanar extends SamplerInfo {

	public IntC sizeXY;
	public double missingLinkRate, holeRate;



	/** update various fields after creation **/
	public SamplerInfoPlanar(String topoCode, CellularModel myModel) {
		super(2, topoCode, myModel);
	}

	public SamplerInfoPlanar(SizeParManagerTwoD twoDSizeParManager,
			CellularModel inModel, String inTopoCode) {

		this( twoDSizeParManager.GetXYValue(), 
				twoDSizeParManager.GetCellPixSize(),
				inModel, inTopoCode);
	}

	public SamplerInfoPlanar(IntC gridSize, IntC inCellPixSize,
			CellularModel inModel, String topoCode) {
		super(2, topoCode, inModel);
		sizeXY= gridSize;
		cellPixSize= inCellPixSize;
	}




	/** special for hyperbolic **/
	public static SamplerInfoPlanar NewPlanarSamplerInfoHyperbolic(CellularModel myModel) {
		SamplerInfoPlanar info= new SamplerInfoPlanar(myModel);
		return info;
	}

	/** used only for hyberbolic topo **/
	private SamplerInfoPlanar(CellularModel myModel){
		super(HyperbolicTopology.SPECIALDIMHYPERBOLIC, null, myModel);
	}


	@Override
	public CAsimulationSampler GetSimulationSampler() {
		//Macro.Debug( " sel model: " + this.model.GetName() );
		if (dim == HyperbolicTopology.SPECIALDIMHYPERBOLIC){
			HyperbolicTopology topo= new HyperbolicTopology();
			return new CAsimulationSampler(topo, model);
		} else {			
			return new CAsimulationSampler(this);
		}
	}


	// TODO : make a better structure
	public void SetErrorParameters(TopologicalConditionSelector2D BC_panel) {
		BC_panel.SetInfo(this);		
	}

	/* curious : used by CAsampler.CreateNewSampler() */
	public PlanarTopology GetTopology() {
		PlanarTopology topo=null;
		try{
			topo=	(PlanarTopology)TopologyCoder.GetTopologyFromCode(topoCode);
		} catch (Exception e) {
			Macro.SystemWarning(" Failed reading :" + topoCode);
		}
		if (inertCellRate>0){
			topo.SetInertCellRate(inertCellRate);
		}
		if (isBorderChanging){
			topo.SetBorderChangingOn();
		}
		// TODO : the use of SetSize here is not "natural"... 
		topo.SetSize(sizeXY); 
		return topo; 	
	}

	@Override
	public int GetLSize() {
		return sizeXY.prodXY();
	}

	public void Print() {
		String s= String.format("TOPO 2D: %d %s", sizeXY.prodXY(), topoCode);
		Macro.print(4,s);		
	}

}
