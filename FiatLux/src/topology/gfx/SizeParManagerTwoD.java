package topology.gfx;

import java.awt.event.ActionEvent;

import components.types.IntC;
import grafix.gfxTypes.IntField;
import grafix.gfxTypes.elements.FLPanel;
import main.FiatLuxProperties;

/*--------------------
 * SizeParManager specified for 2D 
 * @author Nazim Fates
*--------------------*/
public class SizeParManagerTwoD extends SizeParManager {
	/*--------------------
	 * attributes
	 --------------------*/
	IntField m_XField = new IntField("Size ", 3);
	IntField m_YField = new IntField("x ", 3);

	/*--------------------
	 * construction
	 --------------------*/
	public SizeParManagerTwoD() {
		super();

		SetBoxLayoutY();
		m_XField.minimize();
		m_YField.minimize();

		FLPanel sizePanel = FLPanel.NewPanel(m_XField, m_YField);
		sizePanel.setOpaque(false);

		AddElement(sizePanel);
		super.AddCommonPanel();
		UpdateParameters(FiatLuxProperties.TOPOLOGY_select);
	}

	/*--------------------
	 * implementations
	 --------------------*/
	private void UpdateParameters(FiatLuxProperties.sizeType select){
		int L = 10;
		switch (select) {
			case small: L = FiatLuxProperties.sL2D; break;
			case medium: L = FiatLuxProperties.mL2D; break;
			case large: L = FiatLuxProperties.bL2D; break;
		}

		m_XField.SetValue(L);
		m_YField.SetValue(L);

		int borderLen = FiatLuxProperties.border;

		mf_Border.SetValue(borderLen);
	}

	/*--------------------
	 * Get & Set 
	 --------------------*/
	/* dimension 2 */
	public int GetXValue() {
		return m_XField.GetValue();
	}

	public int GetYValue() {
		return m_YField.GetValue();
	}

	public IntC GetXYValue() {
		return new IntC(GetXValue(), GetYValue());
	}

	public void SetXY(IntC xy) {
		m_XField.SetValue(xy.X());
		m_YField.SetValue(xy.Y());
	}

	public IntC GetGridSize() {
		return new IntC(GetXValue(), GetYValue());
	}

	/** returns cell width and border in pixels 
	 * we divide max length by the number of cells
	 * */
	final public IntC GetCellPixSize(){
		int xcellWidth = mf_LWindow.GetValue() / GetXValue();
		int ycellWidth = mf_LWindow.GetValue() / GetYValue();
		int cellWidth = Math.min(xcellWidth, ycellWidth);
		if (cellWidth == 0) { // to avoid empty grids !
			cellWidth = 1;
		}
		return new IntC(cellWidth, GetBorder());
	}
	
	/* cell border in pixels */
	public int GetBorder() {
		return mf_Border.GetValue();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == m_LargerButton) {
			m_XField.Mult2();
			m_YField.Mult2();
		} else if (e.getSource() == m_SmallerButton) {
			m_XField.Div2();
			m_YField.Div2();
		} else {
			for (int i = 0; i < sizeButtons.length; i++) {
				if (e.getSource() == sizeButtons[i]) {
					mF_PresetListSelection = i;
					FiatLuxProperties.sizeType select = FiatLuxProperties.sizeType.values()[mF_PresetListSelection];
					UpdateParameters(select);
				}
			}
		}
	}
}
