package topology.gfx;

import static java.awt.GridBagConstraints.PAGE_START;

import java.util.Arrays;

import components.types.FLString;
import grafix.gfxTypes.elements.FLPanel;
import main.MathMacro;
import models.CAmodels.CellularModel;
import topology.basics.SamplerInfoLinear;
import topology.basics.TopologyCoder;

public class TopologySelectPanelOneD extends FLPanel {
	/*--------------------
	 * attributes
	 --------------------*/
	VisualTopologySelector1D m_TopoSelector;
	SizeParManagerOneD m_OneDSizeParManager= new SizeParManagerOneD();
	TopologicalConditionSelector1D mF_BoundaryCondition = new TopologicalConditionSelector1D();
	
	/*--------------------
	 * construction
	 --------------------*/
	
	public TopologySelectPanelOneD() {
		SetGridBagLayout();
		SetAnchor(PAGE_START);
		GetGridBagC().insets.set(5, 5, 5, 5);
		m_TopoSelector = new VisualTopologySelector1D();
		AddGridBag(m_TopoSelector, 0, 0);
		AddGridBag(m_OneDSizeParManager, 1, 0);

		m_OneDSizeParManager.AddElement(mF_BoundaryCondition);
	}

	/*--------------------
	 * actions
	 --------------------*/

	/* sets the topology with a given configuration (from code) */
	public void SelectTopology(String in_code) {
		String lattice = TopologyCoder.ParseCodeToLattice(in_code);
		int index = FLString.FindRankStringList(lattice, TopologyCoder.SHORTNAME_1D);
		m_TopoSelector.SelectItem(index);
		mF_BoundaryCondition.UpdateSelection(in_code);
		
	}
	//OC
	public void SetEnabledTopology(String code, boolean enable){
		//String lattice = TopologyCoder.ParseCodeToLattice("code");
		int index = Arrays.asList(TopologyCoder.SHORTNAME_1D).indexOf(code);
		//int index = FLString.FindRankStringList(lattice, TopologyCoder.SHORTNAME_1D);
		m_TopoSelector.SetEnableItem(index, enable);
	}
	//\OC
	/*--------------------
	 * get & set
	 --------------------*/
	public SamplerInfoLinear GetLinearSimulationSamplerInfo(CellularModel myModel) {
		String latticeCode = m_TopoSelector.GetSelectedLatticeCode(); 
		String BCprefix = mF_BoundaryCondition.GetSelectedBCcode(); 	
		String topocode = BCprefix + latticeCode;
		SamplerInfoLinear info = new SamplerInfoLinear(topocode, myModel);

		// modifications of the topology
		info.inertCellRate = (double) mF_BoundaryCondition.mF_InertRateField.GetValue()/MathMacro.HUNDRED;
		//SetInertCellRate( 
		// applies the changing border option
		info.isBorderChanging= mF_BoundaryCondition.mF_changingborders.isSelected();
		
		info.N= m_OneDSizeParManager.GetNValue();
		info.T= m_OneDSizeParManager.GetTValue();
		info.cellPixSize= m_OneDSizeParManager.GetCellPixSize();
	
		return info;
	}
}
