package topology.gfx;

import java.awt.Component;
import java.util.Arrays;
import java.util.Collection;

import grafix.gfxTypes.elements.FLPanel;
import grafix.gfxTypes.elements.FLTabbedPane;
import main.Macro;
import models.CAmodels.CellularModel;
import topology.basics.SamplerInfo;
import topology.basics.SamplerInfoPlanar;
import topology.basics.TopologyCoder;

public class TopologySelectPanel extends FLPanel 
{
	private static final int PANELINDEX1D = 0, PANELINDEX2D=1;
	/*--------------------
	 * attributes
	 --------------------*/
	FLTabbedPane m_TopoChoice = new FLTabbedPane();
	TopologySelectPanelOneD m_OneDpanel;
	TopologySelectPanelTwoD m_TwoDpanel;
	// FLPanel m_Hyper; // Empty tab ...

	/*--------------------
	 * construction
	 --------------------*/

	public TopologySelectPanel(){
		m_OneDpanel= new TopologySelectPanelOneD();
		m_TwoDpanel= new TopologySelectPanelTwoD();
		//m_OneDpanel.SetEnabledAll(m_OneDpanel,false);
		// m_Hyper = new FLPanel();
		m_TopoChoice.AddTab("1D",m_OneDpanel);
		m_TopoChoice.AddTab("2D",m_TwoDpanel);
		//m_TopoChoice.setEnabledAt(0,false);
		// m_TopoChoice.AddTab("Hyper", m_Hyper);
		Add(m_TopoChoice);
	}
	/*--------------------
	 * actions
	 --------------------*/

	public SamplerInfo GetSimulationSamplerInfo(CellularModel myModel) {
		// choice of dimension
		int rank= m_TopoChoice.GetSelectedTabIndex();
		switch(rank){
		case 0:
			return m_OneDpanel.GetLinearSimulationSamplerInfo(myModel); 
		case 1:
			return m_TwoDpanel.GetPlanarSimulationSamplerInfo(myModel);
		case 2: // special hyperbolic
			SamplerInfoPlanar samp = 
				SamplerInfoPlanar.NewPlanarSamplerInfoHyperbolic(myModel);
			return samp;
		default:
			Macro.SystemWarning(" bad topo selection ");
			return null;
		}
	}

	/*--------------------
	 * get & set
	 --------------------*/
	public int GetTopologyDimension() {
		return m_TopoChoice.GetSelectedTabIndex() + 1; 
	}

	/** called by sig_Update , auto selection*/
	public void SelectTopology(String associatedtopo) {
		//Macro.Debug("Code to find:"   + associatedtopo);
		int dim= TopologyCoder.GetTopologyDimensionFromCode(associatedtopo);
		switch (dim){
		case 1:
			m_TopoChoice.setSelectedIndex(PANELINDEX1D);
			m_OneDpanel.SelectTopology(associatedtopo);
			break;
		case 2:
			m_TopoChoice.setSelectedIndex(PANELINDEX2D);
			m_TwoDpanel.SelectTopology(associatedtopo);
			break;
		}
	}
	//OC
	public void SetEnabledTopology(Collection<String> disabledTopology, boolean enable){
		if(disabledTopology != null){
			int dim=1;
			for(String disabled : disabledTopology){

				if(Arrays.asList(TopologyCoder.SHORTNAME_1D).contains(disabled)){
					dim=1;
				}
				else if (Arrays.asList(TopologyCoder.SHORTNAME_2D).contains(disabled)){
					dim=2;
				}
				System.out.println(dim);
				//int dim= TopologyCoder.GetTopologyDimensionFromCode(disabled);
				switch (dim){
					case 1:
						//m_TopoChoice.setSelectedIndex(PANELINDEX1D);
						m_OneDpanel.SetEnabledTopology(disabled, enable);
						break;
					case 2:
						//m_TopoChoice.setSelectedIndex(PANELINDEX2D);
						m_TwoDpanel.SetEnabledTopology(disabled,enable);
						break;
				}
			}
		}
	}
	//TODO : Simplification ?
	public void DisableDimension(String dim){
		boolean OneD = false;
		boolean TwoD=false;
		if(dim != null){
			if(dim.equals("1D")){
				for (Component c : m_TopoChoice.getComponents()){
					if(c == m_OneDpanel){
						m_TopoChoice.remove(c);
					}
				}
			}
			else{
				for (Component c : m_TopoChoice.getComponents()){
					if(c == m_TwoDpanel){
						m_TopoChoice.remove(c);
					}
				}
			}
		}
		else {
			m_TopoChoice.removeAll();
			m_TopoChoice.AddTab("1D", m_OneDpanel);
			m_TopoChoice.AddTab("2D", m_TwoDpanel);
		}


	}
	//\OC
}
