package topology.gfx;


import components.types.FLString;
import grafix.gfxTypes.DoubleField;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.FLColorListRender;
import grafix.gfxTypes.elements.FLList;
import grafix.gfxTypes.elements.FLPanel;
import grafix.windows.SimulationWindowCreator;
import topology.basics.LinearTopology;
import topology.basics.TopologyCoder;

/*--------------------
 * selection of a Topology 
 * converts selection into a shortcode ! 
 * @ author Nazim Fates
*--------------------*/

public class TopologySelector1D extends FLPanel {

	private final static FLColor GREEN = FLColor.c_lightgreen ;
	
	final static int 
		DEFAULT_SELECTION= 0, 
		DEF_INERT=50,
		DEF_INTFIELDSIZE=3,
		LIMIT1= 0, LIMIT2= 2;  // for cell rendering;; 
	private final static double 
		DEFAULT_RADIUS= 2.5;
	public final static String SELECT_TOPO		= "Select Topology";
	
	/*--------------------
	 * Attributes
	 --------------------*/
	LinearTopology m_CurrentTopology;
	
	FLList mF_list; // main list with topologies
		
	DoubleField mF_Radius = 
		new DoubleField("Radius :", DEF_INTFIELDSIZE, DEFAULT_RADIUS);

	
	private SimulationWindowCreator m_WindowCreator;
	
	/*--------------------
	 * Constructor
	 * @param in_WindowCreator 
	 --------------------*/

	public TopologySelector1D() {
		// list creation
		FLPanel LabelAndList= new FLPanel();
		mF_list = new FLList(TopologyCoder.LISTNAME_1D);
		LabelAndList.AssociateLabelAndList(SELECT_TOPO,mF_list);
		
		// default selection
		mF_list.SelectItem(DEFAULT_SELECTION);
		//UpdateCurrentSelectedTopology();
		
		FLPanel topologyInfoPanel= new FLPanel();
				
		// layout production
		this.SetBoxLayoutY();
		this.add(LabelAndList);
		this.add(mF_Radius);
		mF_Radius.setVisible(false);
		AddVerticalSpace();
		this.add(topologyInfoPanel);
		
		// cosmetic TODO > remove
		mF_list.setCellRenderer(new FLColorListRender(LIMIT1, LIMIT2));
	}

	/*--------------------
	 * Get & Set methods
	 --------------------*/

	
	private String GetSelectedTopoName() {
		return mF_list.GetSelectedItemName();
	}

	public void SelectItem(int index) {
		mF_list.SelectItem(index);		
	}

	public String GetSelectedLatticeCode() {
		String toponame = GetSelectedTopoName();
		String latticeCode =  
			FLString.FindAssociatedStringArray(toponame, TopologyCoder.LISTNAME_1D, TopologyCoder.SHORTNAME_1D);
		return latticeCode;
	}

	/*--------------------
	 * Other methods
	 --------------------*/

}
