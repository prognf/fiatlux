package topology.gfx;

import static java.awt.GridBagConstraints.PAGE_START;

import java.util.Arrays;

import components.types.FLString;
import grafix.gfxTypes.elements.FLPanel;
import models.CAmodels.CellularModel;
import topology.basics.SamplerInfoPlanar;
import topology.basics.TopologyCoder;

/** construction of a PlanarSamplerInfo **/
public class TopologySelectPanelTwoD extends FLPanel{
	/*--------------------
	 * attributes
	 --------------------*/

	VisualTopologySelector2D m_TopoSelector;
	SizeParManagerTwoD m_TwoDSizeParManager = new SizeParManagerTwoD();
	TopologicalConditionSelector2D m_TC_panel = new TopologicalConditionSelector2D();

	/*--------------------
	 * construction
	 --------------------*/
	public TopologySelectPanelTwoD() {
		SetGridBagLayout();
		SetAnchor(PAGE_START);
		GetGridBagC().insets.set(5, 5, 5, 5);
		m_TopoSelector = new VisualTopologySelector2D();
		AddGridBag(m_TopoSelector, 0, 0);
		AddGridBag(m_TwoDSizeParManager, 1, 0);
		m_TwoDSizeParManager.AddElement(m_TC_panel);
	}

	/*--------------------
	 * actions
	 --------------------*/
	/* sets the topology with a given configuration (from code) */
	public void SelectTopology(String in_code) {
		//Macro.Debug("Selecting model with code : " + in_code);
		String lattice= TopologyCoder.ParseCodeToLattice(in_code);
		int index= FLString.FindRankStringList(lattice, TopologyCoder.SHORTNAME_2D);
		m_TopoSelector.SelectItem(index);
		m_TC_panel.UpdateSelection(in_code);

	}
	//OC
	/* enable or disable a button corresponding a topology */
	public void SetEnabledTopology(String code, boolean enable){
		//String lattice = TopologyCoder.ParseCodeToLattice("code");
		int index = Arrays.asList(TopologyCoder.SHORTNAME_2D).indexOf(code);
		//int index = FLString.FindRankStringList(lattice, TopologyCoder.SHORTNAME_1D);
		m_TopoSelector.SetEnableItem(index, enable);
	}
	//\OC


	/*--------------------
	 * get & set
	 --------------------*/
	public SamplerInfoPlanar GetPlanarSimulationSamplerInfo(CellularModel myModel) {

		String latticeCode = m_TopoSelector.GetSelectedLatticeCode(); 
		String BCprefix = m_TC_panel.GetSelectedBCcode(); 	
		String topoCode= BCprefix + latticeCode;

		SamplerInfoPlanar topo2D= new SamplerInfoPlanar(topoCode, myModel);
		topo2D.sizeXY= m_TwoDSizeParManager.GetGridSize();
		topo2D.cellPixSize= m_TwoDSizeParManager.GetCellPixSize();
		topo2D.SetErrorParameters(m_TC_panel);
		topo2D.Print();	
		return topo2D;
	}



	/*--------------------
	 * test
	 --------------------*/



}
