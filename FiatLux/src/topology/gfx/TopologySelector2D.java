package topology.gfx;


import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import components.types.FLString;
import grafix.gfxTypes.DoubleField;
import grafix.gfxTypes.FLColorListRender;
import grafix.gfxTypes.elements.FLList;
import grafix.gfxTypes.elements.FLPanel;
import topology.basics.PlanarTopology;
import topology.basics.TopologyCoder;

/*--------------------
 * selection of a Topology 
 * converts selection into a shortcode ! 
 * @ author Nazim Fates
*--------------------*/

public class TopologySelector2D extends FLPanel implements ListSelectionListener {

	// list for selection is in TopologyCoder.LISTNAME_2D 

	final static int 
	DEFAULT_SELECTION= 0,
	DEF_INTFIELDSIZE=3,
	LIMIT1= 2, LIMIT2= 3;  // for cell rendering;;
	private final static double DEFAULT_RADIUS= 2.5;
	public final static String 
	SELECT_TOPO		= "Select Topology";

	/*--------------------
	 * Attributes
	 --------------------*/
	PlanarTopology m_CurrentTopology;

	FLList mF_list; // main list with topologies

	DoubleField mF_Radius = new DoubleField("circular R.:", DEF_INTFIELDSIZE, DEFAULT_RADIUS);


	/*--------------------
	 * Constructor
	 * @param in_WindowCreator 
	 --------------------*/

	/* no call back */
	public TopologySelector2D() {
		// list creation
		FLPanel LabelAndList= new FLPanel();
		mF_list = new FLList(TopologyCoder.LISTNAME_2D, this);
		LabelAndList.AssociateLabelAndList(SELECT_TOPO,mF_list);

		// default selection
		mF_list.SelectItem(DEFAULT_SELECTION);

		// layout production
		this.SetBoxLayoutY();
		this.add(LabelAndList);
		this.add(mF_Radius);
		//mF_Radius.setVisible(false);

		// cosmetic
		mF_list.setCellRenderer(new FLColorListRender(LIMIT1, LIMIT2));
	}

	/*--------------------
	 * actions
	 --------------------*/
	
	
	public void SelectItem(int index) {
		mF_list.SelectItem(index);		
	}
	/*--------------------
	 * Get & Set methods
	 --------------------*/
	

	public int GetCurrentSelectedDimension() {
		return m_CurrentTopology.GetDimension();
	}

	
	// used only to control circular field
	public void valueChanged(ListSelectionEvent e) {
		String latticeCode = GetSelectedLatticeCode(); 
		if ( TopologyCoder.IsCodeCircular(latticeCode)){	
			mF_Radius.setVisible(true);
			mF_Radius.setEnabled(true);
		} else {
			mF_Radius.setVisible(false);
			mF_Radius.setFocusable(false);
			mF_Radius.setEnabled(false);
		}
		//UpdateCurrentSelectedTopology();
	}

	/* sets the topology with a given configuration (from code) */
	public void SelectTopology(String in_code) {
		String lattice= TopologyCoder.ParseCodeToLattice(in_code);
		int index= FLString.FindRankStringList(lattice, TopologyCoder.SHORTNAME_2D);
		mF_list.SelectItem(index);

		//m_BC_panel.UpdateSelection(in_code);
	}

	public String GetSelectedLatticeCode() {
		String toponame = GetSelectedTopoName();
		//Macro.Debug(" read" + toponame);
		String latticeCode =  
			FLString.FindAssociatedStringArray(toponame, 
					TopologyCoder.LISTNAME_2D, TopologyCoder.SHORTNAME_2D);
		if (TopologyCoder.IsCodeCircular(latticeCode)){
			// cast from double to String
			String radius= "" + (float)mF_Radius.GetValue();
			// reformat of lattice code
			latticeCode += radius.replace(FLString.DOTSEPARATOR,FLString.NEWSEPARATOR);
		}
		//Macro.Debug("COD:" + latticeCode);
		return latticeCode;
	}
	
	private String GetSelectedTopoName() {
		return mF_list.GetSelectedItemName();
	}

	

	/*--------------------
	 * Other methods
	 --------------------*/

}
