package topology.gfx;

import javax.swing.JCheckBox;
import javax.swing.JComponent;

import grafix.gfxTypes.DoubleField;
import grafix.gfxTypes.IntField;
import grafix.gfxTypes.MacroGFX;
import grafix.gfxTypes.elements.FLBlockPanel;
import grafix.gfxTypes.elements.FLPanel;
import main.MathMacro;
import topology.basics.BONDCOND;
import topology.basics.SamplerInfoPlanar;

public class TopologicalConditionSelector2D extends FLBlockPanel {
	final static int 
	DEFAULT_SELECTION= 0,
	DEF_INERT=50, DEF_RNDLNK=0,
	DEF_INTFIELDSIZE=3;

	public final static String  
	S_BOUNDARYCOND	= "Boundaries",
	S_TOPOERRORS = "Irregularities";

	/*--------------------
	 * attributes
	 --------------------*/
	JComponent mF_BoundaryCondition = BONDCOND.NewListControl();
	IntField
	mF_HoleRateField = new IntField(MacroGFX.HOLE_RATE, DEF_INTFIELDSIZE),
	mF_ErrorRateField = new IntField(MacroGFX.MISSINGLINK_RATE, DEF_INTFIELDSIZE),
	mF_InertRateField = new IntField(MacroGFX.S_INERT_RATE, DEF_INTFIELDSIZE, DEF_INERT);
	DoubleField mF_RandomLinkRate = new DoubleField(MacroGFX.S_RNDLNK_RATE, DEF_INTFIELDSIZE, DEF_RNDLNK);

	JCheckBox mF_AddLink= new JCheckBox(MacroGFX.S_ADDLINK);
	JCheckBox mF_changingborders= new JCheckBox("changing state", false);
	/*--------------------
	 * construction
	 --------------------*/	

	public TopologicalConditionSelector2D() {
		// FLButton title = new FLButton(S_BOUNDARYCOND);
		AddLegend(S_BOUNDARYCOND);
		// AddGridBagButton(title);
		// defineAsWindowBlockPanel(FLColor.c_lightblue, title, 150, false);
		SetBoxLayoutY();
		// AddElement(new JLabel(" ")); // temporaire
		AddElement(mF_BoundaryCondition);

		FLPanel borders = FLPanel.NewPanel(mF_InertRateField, mF_changingborders);
		borders.setOpaque(false);
		mF_changingborders.setOpaque(false);
		mF_InertRateField.minimize();

		AddElement(borders);
		// AddLegend(S_TOPOERRORS);
		// Add(mF_ErrorRateField, mF_HoleRateField, mF_RandomLinkRate);
		// Add(mF_AddLink);
	}

	/*--------------------
	 * actions
	 --------------------*/

	/* sets topology modifications (hole rate, etc.) */
	/** public void ModifyTopology(PlanarTopology currentTopology) {
		currentTopology.SetInertCellRate(mF_InertRateField.GetValue() / Macro.HUNDRED); 
		currentTopology.SetHoleMissingLinkRates(
				mF_HoleRateField.GetValue() / Macro.HUNDRED, 
				mF_ErrorRateField.GetValue() / Macro.HUNDRED);
		currentTopology.SetRandomLinkRate(mF_RandomLinkRate.GetValue() / Macro.HUNDRED);
		currentTopology.SetAddLink(mF_AddLink.isSelected()); // add or remove links
		// applies the changing border option
		if (mF_changingborders.isSelected()){
			currentTopology.SetBorderChangingOn();
		}
	} **/

	/* updates the current selection according to the code 
	 * used for automatic initial setting associated to the model */
	public void UpdateSelection(String in_code) {
		BONDCOND.SelectItemFromCode(in_code);
	}

	/*--------------------
	 * get & set
	 --------------------*/

	public String GetSelectedBCcode() {
		int select = BONDCOND.getSelected();
		String BCprefix = BONDCOND.GetPrefixFromIndex(select); 	
		return BCprefix;
	}

	public void SetInfo(SamplerInfoPlanar info) {
		info.inertCellRate= mF_InertRateField.GetValue() / MathMacro.HUNDRED;
		info.missingLinkRate= mF_ErrorRateField.GetValue() / MathMacro.HUNDRED;
		info.holeRate= mF_HoleRateField.GetValue() / MathMacro.HUNDRED; 
		info.isBorderChanging= mF_changingborders.isSelected();
	}
}
