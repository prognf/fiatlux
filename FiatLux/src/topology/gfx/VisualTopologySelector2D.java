package topology.gfx;

import static java.awt.GridBagConstraints.PAGE_START;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JComponent;

import components.types.FLString;
import grafix.gfxTypes.DoubleField;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.IconManager;
import grafix.gfxTypes.elements.FLBlockPanel;
import grafix.gfxTypes.elements.FLButton;
import grafix.gfxTypes.elements.FLLabel;
import grafix.gfxTypes.elements.FLPanel;
import grafix.gfxTypes.elements.NeighborhoodButton;
import topology.basics.PlanarTopology;
import topology.basics.TopologyCoder;

/**
 * selection of a Topology with visuals
 * @author Nazim Fatès, Nicolas Gauville
 **/
public class VisualTopologySelector2D extends FLBlockPanel implements ActionListener {
	final static int DEF_INTFIELDSIZE = 3;  // for cell rendering;
	private final static String TITLE = "Neighborhood";
	private final static double DEFAULT_RADIUS = 2.5;
	private ArrayList<NeighborhoodButton> buttons;
	private FLPanel container;

	PlanarTopology m_CurrentTopology;
	NeighborhoodButton m_currentButton;
	FLButton m_showMore;

	DoubleField mF_Radius = new DoubleField("circular R.:", DEF_INTFIELDSIZE, DEFAULT_RADIUS);

	public VisualTopologySelector2D() {
		FLButton title = new FLButton(TITLE);
		title.setIcon(IconManager.getUIIcon("neighborhood"));
		defineAsWindowBlockPanel(FLColor.c_lightgreen, title, 400, false);
		AddGridBagButton(title);

		setDefaultWidth(350);
		setAlignmentY(TOP_ALIGNMENT);
		SetAnchor(PAGE_START);
		container = new FLPanel();
		container.setPreferredSize(new Dimension(330, 310));
		container.setOpaque(false);
		container.setLayout(new FlowLayout(FlowLayout.LEADING));
		buttons = new ArrayList<>();

		for (String name : TopologyCoder.SHORTNAME_2D) {
			if (name.equals("CI")) { // TODO: change this line
				String radius = "" + (float) mF_Radius.GetValue();
				name += radius.replace(FLString.DOTSEPARATOR, FLString.NEWSEPARATOR);
			}

			PlanarTopology m = (PlanarTopology) TopologyCoder.GetLattice(name);
			if (m != null) {
				NeighborhoodButton topoBtn = new NeighborhoodButton(m);
				buttons.add(topoBtn);
				container.add(topoBtn);
				topoBtn.AddActionListener(this);

				if (m.isExoticTopology()) {
					topoBtn.setVisible(false);
				}
			}
		}

		m_showMore = new FLButton("");
		JComponent n, l; //n = new NeighborhoodIcon(topology, 40, 100)
		FLPanel content = FLPanel.NewPanelVertical(n = new FLLabel("+"), l = new FLLabel("Show more"));
		n.setFont(new Font(Font.SANS_SERIF,Font.BOLD, 40));
		n.setForeground(Color.GREEN.darker());
		n.setPreferredSize(new Dimension(80, 40));
		n.setAlignmentX(CENTER_ALIGNMENT);
		l.setAlignmentX(CENTER_ALIGNMENT);
		m_showMore.add(content);
		m_showMore.setMargin(new Insets(1, 1, 1, 1));
		content.setOpaque(false);
		m_showMore.setBackground(Color.WHITE);
		m_showMore.setPreferredSize(new Dimension(100, 65));
		m_showMore.setBorderPainted(false);
		m_showMore.setFocusPainted(false);
		container.add(m_showMore);
		m_showMore.AddActionListener(this);

		buttons.get(0).selectButton();
		m_CurrentTopology = buttons.get(0).getTopology();
		m_currentButton = buttons.get(0);

		FLPanel sv = FLPanel.NewPanelVertical(container, mF_Radius);
		AddGridBagY(sv);
		sv.setOpaque(false);
		mF_Radius.setVisible(false);
	}
	
	public void SelectItem(int index) {
		if (index < buttons.size()) {
			selectItem(buttons.get(index));
		}
	}
	//OC
	public void SetEnableItem(int index, boolean enable){
		buttons.get(index).setEnabled(enable);
	}

	//\OC
	/*--------------------
	 * Get & Set methods
	 --------------------*/
	public int GetCurrentSelectedDimension() {
		return m_CurrentTopology.GetDimension();
	}

	// used only to control circular field
	// public void valueChanged(ListSelectionEvent e) {
		// String latticeCode = GetSelectedLatticeCode();
		// if ( TopologyCoder.IsCodeCircular(latticeCode)){
		// 	mF_Radius.setVisible(true);
		// 	mF_Radius.setEnabled(true);
		// } else {
		// 	mF_Radius.setVisible(false);
		// 	mF_Radius.setFocusable(false);
		// 	mF_Radius.setEnabled(false);
		// }
		//UpdateCurrentSelectedTopology();
	// }

	/* sets the topology with a given configuration (from code) */
	public void SelectTopology(String in_code) {
		String lattice = TopologyCoder.ParseCodeToLattice(in_code);
		int index = FLString.FindRankStringList(lattice, TopologyCoder.SHORTNAME_2D);
		if (index < buttons.size()) {
			selectItem(buttons.get(index));
		}
		// m_BC_panel.UpdateSelection(in_code);
	}

	public String GetSelectedLatticeCode() {
		String latticeCode = FLString.FindAssociatedStringArray(GetSelectedTopoName(), TopologyCoder.LISTNAME_2D, TopologyCoder.SHORTNAME_2D);
		if (TopologyCoder.IsCodeCircular(latticeCode)) {
			// cast from double to String
			String radius = "" + (float) mF_Radius.GetValue();
			// reformat of lattice code
			latticeCode += radius.replace(FLString.DOTSEPARATOR,FLString.NEWSEPARATOR);
		}
		//Macro.Debug("COD:" + latticeCode);
		return latticeCode;
	}
	
	private String GetSelectedTopoName() {
		return m_CurrentTopology.GetName();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == m_showMore) {
			showMore();
			return;
		}

		if (e.getSource() instanceof NeighborhoodButton) {
			selectItem((NeighborhoodButton) e.getSource());
		}
	}

	private void showMore() {
		for (NeighborhoodButton nb : buttons) {
			nb.setVisible(true);
		}
		m_showMore.setVisible(false);
	}

	private void selectItem(NeighborhoodButton item) {
		for (NeighborhoodButton btn : buttons) {
			btn.unselectButton();
		}

		item.selectButton();
		m_CurrentTopology = item.getTopology();

		mF_Radius.setVisible(m_CurrentTopology.GetName().equals("Circular"));

		if (!item.isVisible()) {
			item.setVisible(true);
		}
	}
}
