package topology.gfx;

import javax.swing.JCheckBox;

import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.IntField;
import grafix.gfxTypes.MacroGFX;
import grafix.gfxTypes.elements.FLBlockPanel;
import grafix.gfxTypes.elements.FLPanel;
import topology.basics.BONDCOND;

public class TopologicalConditionSelector1D extends FLBlockPanel {
	final static int DEFAULT_SELECTION = 0, DEF_INERT = 0, DEF_INTFIELDSIZE = 3;

	public final static String S_BOUNDARYCOND = "Boundaries", S_TOPOERRORS = "Irregularities";
	private final static FLColor GREEN = FLColor.c_lightgreen;

	/*--------------------
	 * attributes
	 --------------------*/
	FLPanel mF_BoundaryCondition = BONDCOND.NewListControl();
	IntField
	mF_InertRateField = new IntField(MacroGFX.S_INERT_RATE, DEF_INTFIELDSIZE, DEF_INERT);
	JCheckBox mF_changingborders= new JCheckBox("changing state", false);

	/*--------------------
	 * construction
	 --------------------*/
	public TopologicalConditionSelector1D(){
		AddLegend(S_BOUNDARYCOND);
		SetBoxLayoutY();
		AddElement(mF_BoundaryCondition);

		FLPanel borders = FLPanel.NewPanel(mF_InertRateField, mF_changingborders);
		borders.setOpaque(false);
		mF_changingborders.setOpaque(false);
		mF_InertRateField.minimize();
		AddElement(borders);
	}

	/*--------------------
	 * actions
	 --------------------*/
	/** updates the current selection according to the code 
	 * used for automatic initial setting associated to the model */
	public void UpdateSelection(String in_code) {
		BONDCOND.SelectItemFromCode(in_code);
	}

	/*--------------------
	 * get & set
	 --------------------*/
	public String GetSelectedBCcode() {
		int select = BONDCOND.getSelected();
		String BCprefix = BONDCOND.GetPrefixFromIndex(select); 	
		return BCprefix;
	}
}
