package topology.gfx;

import static java.awt.GridBagConstraints.PAGE_START;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import components.types.FLString;
import grafix.gfxTypes.DoubleField;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.IconManager;
import grafix.gfxTypes.elements.FLBlockPanel;
import grafix.gfxTypes.elements.FLButton;
import grafix.gfxTypes.elements.FLPanel;
import grafix.gfxTypes.elements.NeighborhoodButton;
import topology.basics.LinearTopology;
import topology.basics.SuperTopology;
import topology.basics.TopologyCoder;

/*--------------------
 * selection of a Topology 
 * converts selection into a shortcode ! 
 * @ author Nazim Fates, Nicolas Gauville
*--------------------*/

public class VisualTopologySelector1D extends FLBlockPanel implements ActionListener {
	final static int DEFAULT_SELECTION = 0, DEF_INERT = 50, DEF_INTFIELDSIZE=3;
	public final static String SELECT_TOPO	= "Neighborhood";
	private final static double DEFAULT_RADIUS = 2.5;

	private ArrayList<NeighborhoodButton> buttons;
	private FLPanel container;

	NeighborhoodButton m_currentButton;
	LinearTopology m_CurrentTopology;
	DoubleField mF_Radius = new DoubleField("Radius :", DEF_INTFIELDSIZE, DEFAULT_RADIUS);

	public VisualTopologySelector1D() {
		FLButton title = new FLButton(SELECT_TOPO);
		title.setIcon(IconManager.getUIIcon("neighborhood"));
		defineAsWindowBlockPanel(FLColor.c_lightgreen, title, 400, false);
		AddGridBagButton(title);
		setBackground(Color.WHITE);

		setDefaultWidth(350);
		setAlignmentY(TOP_ALIGNMENT);
		SetAnchor(PAGE_START);
		container = new FLPanel();
		container.setPreferredSize(new Dimension(330, 340));
		container.setOpaque(false);
		container.setLayout(new FlowLayout(FlowLayout.LEADING));
		buttons = new ArrayList<>();

		for (String name : TopologyCoder.SHORTNAME_1D) {
			SuperTopology m = TopologyCoder.GetLattice(name);
			if (m != null) {
				NeighborhoodButton topoBtn = new NeighborhoodButton(m);
				buttons.add(topoBtn);
				container.add(topoBtn);
				topoBtn.AddActionListener(this);
			}
		}

		buttons.get(0).selectButton();
		m_CurrentTopology = buttons.get(0).getLinearTopology();
		m_currentButton = buttons.get(0);

		AddGridBagY(container);
	}

	/*--------------------
	 * Get & Set methods
	 --------------------*/
	private String GetSelectedTopoName() {
		return m_CurrentTopology.GetName();
	}

	public void SelectItem(int index) {
		if (index < buttons.size()) {
			selectItem(buttons.get(index));
		}
	}
	//OC
	public void SetEnableItem(int index, boolean enable){
		buttons.get(index).setEnabled(enable);
	}
	//\OC

	public String GetSelectedLatticeCode() {
		return FLString.FindAssociatedStringArray(GetSelectedTopoName(), TopologyCoder.LISTNAME_1D, TopologyCoder.SHORTNAME_1D);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof NeighborhoodButton) {
			selectItem((NeighborhoodButton) e.getSource());
		}
	}

	private void selectItem(NeighborhoodButton item) {
		for (NeighborhoodButton btn : buttons) {
			btn.unselectButton();
		}

		item.selectButton();
		m_CurrentTopology = item.getLinearTopology();
	}
}
