package topology.gfx;

import java.awt.Color;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.IconManager;
import grafix.gfxTypes.IntField;
import grafix.gfxTypes.elements.FLBlockPanel;
import grafix.gfxTypes.elements.FLButton;
import grafix.gfxTypes.elements.FLPanel;
import main.FiatLuxProperties;


/*--------------------
 * SizeParManager manages pixel size only 
 * 
 * @author Nazim Fates
*--------------------*/
abstract public class SizeParManager extends FLBlockPanel implements ActionListener /* for buttons */ {
	/*--------------------
	 *	 attributes
	--------------------*/
	final static String 
		CELLS ="Cell display",
		LWINDOWSIZE ="Screen size (px)",
		PRESETSIZE ="Presets",
		CELLBORDER ="Border",
		LINEAR_PARS ="1D size :",
		PLANAR_PARS ="2D size :";
	/* attributes */
	
	final IntField mf_LWindow = new IntField(LWINDOWSIZE, 3, FiatLuxProperties.DEF_LWINDOW);
	final IntField mf_Border = new IntField(CELLBORDER, 3, FiatLuxProperties.border);
	final static String [] PRESET={"Small", "Medium", "Large"};
	// adds list selection listener
	protected FLPanel mF_PresetList;//= new FLList(PRESET, DEF_SELECTION, this);
	protected FLButton[] sizeButtons = {new FLButton(PRESET[0]), new FLButton(PRESET[1]), new FLButton(PRESET[2])};
	protected int mF_PresetListSelection = 0;
	
	FLButton 
		m_LargerButton = new FLButton("+"),
		m_SmallerButton = new FLButton("-");

	public SizeParManager() {
		super();

		FLButton title = new FLButton("Grid");
		title.setIcon(IconManager.getUIIcon("rule"));
		defineAsWindowBlockPanel(FLColor.c_lightblue, title, 400, false);
		AddGridBagButton(title);

		// Buttons +, - UI enhancement
		m_SmallerButton.setForeground(FLColor.WHITE);
		m_LargerButton.setForeground(FLColor.WHITE);
		m_SmallerButton.defineAsPanelButtonStylized(FLColor.c_lightred);
		m_LargerButton.defineAsPanelButtonStylized(FLColor.c_lightgreen);
		m_SmallerButton.setSize(m_LargerButton.getSize());
		m_LargerButton.setSize(m_LargerButton.getSize());
		m_SmallerButton.setPreferredSize(m_LargerButton.getPreferredSize());
		m_LargerButton.setPreferredSize(m_LargerButton.getPreferredSize());
		m_SmallerButton.onlyBottomCorners = m_LargerButton.onlyTopCorners = true;
		Insets nullMargin = new Insets(0, 0, 0, 0);
		m_SmallerButton.setMargin(nullMargin);
		m_LargerButton.setMargin(nullMargin);

		setDefaultWidth(325);
	}
	
	/*--------------------
	 * construction
	 --------------------*/
	protected void AddCommonPanel() {
		// Grid size presets UI
		mF_PresetList = new FLPanel();
		mF_PresetList.setOpaque(false);

		Insets nullMargin = new Insets(0, 0, 0, 0);
		for (int i = 0; i < sizeButtons.length; i++) {
			sizeButtons[i].setIcon(IconManager.getUIIcon("grid" + (i + 1)));
			sizeButtons[i].setForeground(Color.BLACK);
			sizeButtons[i].setBackground(Color.WHITE);
			sizeButtons[i].setBorderPainted(false);
			sizeButtons[i].setFocusPainted(false);
			sizeButtons[i].setHorizontalTextPosition(JButton.CENTER);
			sizeButtons[i].setVerticalTextPosition(JButton.BOTTOM);
			sizeButtons[i].setMargin(nullMargin);
			sizeButtons[i].AddActionListener(this);
			mF_PresetList.Add(sizeButtons[i]);
		}

		FLPanel buttons = new FLPanel();
		buttons.SetGridBagLayout();
		buttons.setOpaque(false);
		buttons.GetGridBagC().insets.set(5, 5, 5, 5);
		buttons.AddGridBagY(m_LargerButton, m_SmallerButton);
		
		FLPanel listandbutton = FLPanel.NewPanel(mF_PresetList, buttons);
		listandbutton.setOpaque(false);
		
		AddLegend(PRESETSIZE);
		AddElement(listandbutton);
		
		int position = FiatLuxProperties.TOPOLOGY_select.ordinal();
		AddLegend(CELLS);

		FLPanel cellsDisplay = FLPanel.NewPanel(mf_LWindow, mf_Border);
		mf_LWindow.minimize();
		mf_Border.minimize();
		cellsDisplay.setOpaque(false);
		AddElement(cellsDisplay);
			
		// UI
		m_LargerButton.AddActionListener(this);
		m_SmallerButton.AddActionListener(this);
	}
	
	/*--------------------
	 * get / set
	 --------------------*/
	
	/* cell width in pixels */
	/*public int GetCellWidth() {
		return mf_CellWidth.GetValue();
	}*/
	
	/* cell border in pixels */
	public int GetBorder() {
		return mf_Border.GetValue();
	}

	/* sets cell width and border in pixels */
	/*final public void SetCellSize(IntCouple in_Cellsize){
		mf_CellWidth.SetValue(in_Cellsize.X());
		mf_Border.SetValue(in_Cellsize.Y());
	}*/
}
