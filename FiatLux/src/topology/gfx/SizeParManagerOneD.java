package topology.gfx;

import java.awt.event.ActionEvent;

import components.types.IntC;
import grafix.gfxTypes.IntField;
import grafix.gfxTypes.elements.FLPanel;
import main.FiatLuxProperties;

/*--------------------
 * SizeParManager specified for 1D  
 * @author Nazim Fates
*--------------------*/
public class SizeParManagerOneD extends SizeParManager {
	final static int  DEF_SIZEL=150, DEF_SIZET=100;
	
	final static String S_NCELLS= " N  cells", S_TBUFFER= " T buffer";

	/* attributes */
	IntField m_NField = new IntField(S_NCELLS);
	IntField m_TField = new IntField(S_TBUFFER);

	/*--------------------
	 * Constructor
	 --------------------*/
	public SizeParManagerOneD() {
		SetBoxLayoutY();
		// AddLegend(LINEAR_PARS);

		FLPanel sizeParams = FLPanel.NewPanel(m_NField, m_TField);
		sizeParams.setOpaque(false);

		AddElement(sizeParams);
		super.AddCommonPanel();
		UpdateParameters(FiatLuxProperties.TOPOLOGY_select);
	}

	public int GetNValue(){
		return m_NField.GetValue();
	}

	public int GetTValue() {
		return m_TField.GetValue();
	}

	/** returns cell width and border in pixels 
	 * we divide max length by the number of cells
	 * */
	final public IntC GetCellPixSize(){
		int xcellWidth = mf_LWindow.GetValue() / GetNValue();
		int ycellWidth = mf_LWindow.GetValue() / GetTValue();
		int cellWidth = Math.min(xcellWidth, ycellWidth);
		return new IntC(cellWidth, GetBorder());
	}
	
	/* cell border in pixels */
	public int GetBorder() {
		return mf_Border.GetValue();
	}

	private void UpdateParameters(FiatLuxProperties.sizeType select){		
		SetThreePar(select.ordinal());
		/*switch (select){
		case small:
			break;
		case medium:
			break;
		case large:
			break;
		}*/
	}
	
	private void SetThreePar(int i) {
		m_NField.SetValue(main.FiatLuxProperties.Nsize1D[i]);
		m_TField.SetValue(main.FiatLuxProperties.Tsize1D[i]);
		mf_Border.SetValue(main.FiatLuxProperties.border);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == m_LargerButton){
			m_NField.Mult2();
			m_TField.Mult2();
		} else if (e.getSource() == m_SmallerButton){
			m_NField.Div2();
			m_TField.Div2();
		} else {
			for (int i = 0; i < sizeButtons.length; i++) {
				if (e.getSource() == sizeButtons[i]) {
					mF_PresetListSelection = i;
					FiatLuxProperties.sizeType select = FiatLuxProperties.sizeType.values()[mF_PresetListSelection];
					UpdateParameters(select);
				}
			}
		}
	}
}
