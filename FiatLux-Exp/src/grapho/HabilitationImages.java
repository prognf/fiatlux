package grapho;

import components.types.IntC;
import experiment.samplers.CAsimulationSampler;
import grafix.gfxTypes.imgFormat.ImgFormat;
import grafix.viewers.LineAutomatonViewer;
import grafix.viewers.LineAutomatonViewerDefault;
import initializers.CAinit.BinaryInitializer;
import main.Macro;
import models.CAmodels.CellularModel;
import models.CAmodels.tabled.ECAmodel;
import topology.basics.SamplerInfoLinear;
import topology.basics.TopologyCoder;

public class HabilitationImages {

	
	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();
	private static final IntC CellPixSize = new IntC(40,1);
	private static final int N = 10;
	private static final int T = 5;
	
	/** main : processing command line * */
	public static void main(String[] argv) {
		Generate4symmetries();
	}

	private static void Generate4symmetries() {
		int W=255;
		CellularModel rule = ECAmodel.NewRule(W);
		String 	topoCode = TopologyCoder.s_TLR1;

		SamplerInfoLinear info = 
				new SamplerInfoLinear(N, T, CellPixSize, rule, topoCode);
		//Macro.Debug(" code :" + info.topoCode);
		CAsimulationSampler sampler= new CAsimulationSampler(info);
		LineAutomatonViewer viewer= new LineAutomatonViewerDefault(CellPixSize, sampler, T);
		sampler.SetAutomatonViewer(viewer);

		BinaryInitializer init = new BinaryInitializer();
		sampler.SetInitDeviceFromOutside(init);
		init.interpretCommand("RS30");
		//sampler.GetInitializer().ReceiveSignal(FLsignal.init);// sig_Init();
		sampler.sig_Update();
		for (int t=0; t<T-1; t++){
			sampler.sig_NextStep();
			//sampler.sig_Nsteps(N);
		}
		sampler.sig_Update();
		String filename= "ECA-" + W;
		sampler.io_SetRecordFormat(ImgFormat.SVG);
		sampler.io_SaveImage(filename, viewer);	
		sampler.io_SetRecordFormat(ImgFormat.PNG);
		sampler.io_SaveImage(filename, viewer);	
		
	}
}
