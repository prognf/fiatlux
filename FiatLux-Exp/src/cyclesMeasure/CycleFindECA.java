package cyclesMeasure;

import java.util.TreeMap;
import java.util.TreeSet;

import components.allCells.OneRegisterIntCell;
import components.arrays.GenericCellArray;
import components.arrays.RegularDynArray;
import components.types.IntC;
import experiment.samplers.CAsimulationSampler;
import initializers.CAinit.BinaryInitializer;
import main.Macro;
import main.commands.CommandParser;
import models.CAmodels.CellularModel;
import models.CAmodels.tabled.ECAmodel;
import topology.basics.SamplerInfoLinear;
import topology.basics.TopologyCoder;

public class CycleFindECA {


	private static final int W = 22;
	private static final IntC CellPixSize = new IntC(10,0);
	private static final int N = 100;
	private static final int T = 50;

	CAsimulationSampler m_sampler;
	RegularDynArray m_sys;

	CycleFindECA(){
		CellularModel 	rule = ECAmodel.NewRule(W);
		String 	topoCode = TopologyCoder.s_TLR1;

		SamplerInfoLinear info = 
				new SamplerInfoLinear(N,T, CellPixSize, rule, topoCode);
		//Macro.Debug(" code :" + info.topoCode);
		m_sampler= new CAsimulationSampler(info);
		BinaryInitializer init = new BinaryInitializer();
		init.SetSeed(280676);
		//init.SetInitCode("R4");
		m_sampler.SetInitDeviceFromOutside(init);
		m_sys= m_sampler.GetAutomaton();

	}


	class BiLong implements Comparable<BiLong>{
		public BiLong(long ival1, long ival2) { 
			val1= ival1; val2= ival2;}
		Long val1;
		Long val2;
		public String toString(){
			return String.format("(%d, %d)",val1,val2);
		}
		@Override
		public int compareTo(BiLong other) {
			int comp1=val1.compareTo(other.val1); 
			return (comp1!=0)?comp1:val2.compareTo(other.val2);
		}
		@Override
		public int hashCode() {
			return val1.hashCode() + val2.hashCode();
		}
		
		public boolean equals(Object o){
			BiLong other=(BiLong)o;
			return (other.val1==val1) && (other.val2==val2);
		}
	}
	
	private void Explore() {
		m_sampler.sig_Init();
		GenericCellArray<OneRegisterIntCell> array = m_sys.GetArrayAsCastedList();
		TreeSet<BiLong> image = new TreeSet<BiLong>();
		TreeMap<BiLong, Integer> occurence = new TreeMap<BiLong, Integer>();
		boolean notSeen= true;
		BiLong configcode=null;
		while (notSeen){
			Integer time= m_sampler.GetTime();
			configcode= ArrayToBiLong(array);
			notSeen= image.add(configcode);
			
			Macro.fPrint("t:%d new:%s config:%s", 
					time, notSeen, configcode);
			if (notSeen){
				occurence.put(configcode, time);
				m_sys.sig_NextStep();
			}		
		}
	//	Macro.Debug(""+ configcode + "::"+occurence.containsKey(configcode));
	//	Macro.Debug(""+occurence);
		Integer previous= occurence.get(configcode); 
		Macro.fPrint("time %s is the previous occurence of %s",
				previous, configcode );
	}

	private BiLong ArrayToBiLong(GenericCellArray<OneRegisterIntCell> array) {
		long 
			val1= BinaryTab2Decimal(array,0,N/2),
			val2= BinaryTab2Decimal(array,N/2,N);
	//	Macro.Debug(val1+":::"+ val2);
		return new BiLong(val1,val2);
	}

	private static void DoTest() {
		CycleFindECA exp = new CycleFindECA();
		exp.Explore();
	}

	/** from binary code to decimal 
	 * indices are considered in the interval [begin,end[ **/
	final static public long 
	BinaryTab2Decimal(GenericCellArray<OneRegisterIntCell> array,
			int begin, int end) {
		long out_RuleNum = 0L;
		for (int i = end-1; i >= begin; i--) {
			out_RuleNum <<= 1;
			// the bit of high weight must be taken into account first
			int val = array.get(i).GetState();
			out_RuleNum += val; 
		}
		return out_RuleNum;
	}


	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();

	public static void main(String[] argv) {
		Macro.BeginTest(CLASSNAME);
		CommandParser parser= new CommandParser(CLASSNAME, argv);
		DoTest();
		Macro.EndTest();
	}




}
