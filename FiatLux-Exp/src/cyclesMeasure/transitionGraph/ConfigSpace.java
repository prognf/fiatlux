package cyclesMeasure.transitionGraph;

import java.util.ArrayList;

import main.Macro;
import main.MathMacro;

public class ConfigSpace {

	int m_N; // size of the configurations;
	int m_sz; // size of the space
	int [] m_minimal; // minimal rep
	int [] m_period; // spatial period of a config.

	// a strong representative is a min. rep. with maximal spatial period
	private ArrayList <Integer> m_strongRepresentativesList;



	public ConfigSpace(int N){
		m_N= N;
		m_sz = MathMacro.TwoToPowerIntResult(m_N);
		m_minimal= new int[m_sz];
		m_period= new int[m_sz];
		m_strongRepresentativesList= new ArrayList<Integer>();
		java.util.Arrays.fill (m_minimal,-1);
		BuildSpace();
	}		


	private void BuildSpace() {
		for (int index=0; index< m_sz; index++){
			ProcessConfig(index);
		}
	}

	/** deals with a class of configurations **/
	private void ProcessConfig(int index) {
		int [] source= AnalyseRuleGlobalSynchProblem.IndexToTableState(m_N, index);
		
		m_period[index]= FindPeriod(source);
		if (m_minimal[index]==-1){	// analyse only those whose image is not yet set
			CalculateMinRepClass(index, source);
		}

	}
	
	/** pre-requisite : period of config index should have been calculated **/
	private void CalculateMinRepClass(int index, final int[] source) {
		int [] shift= new int[m_N];
		int [] imageIndex = new int[m_N]; // list of the indices of the images
		int minrep= index;
		imageIndex[0]= index;
		for (int sigma=1; sigma < m_N; sigma++){ // for each shift
			for (int i=0; i<m_N; i++){ // calculate image
				int loopedi= (i+sigma)%m_N;
				shift[i]= source[loopedi];
			}
			int shiftedIndex = AnalyseRuleGlobalSynchProblem.TableStateToIndex(shift); // associated index
			imageIndex[sigma]= shiftedIndex; // store
			if (shiftedIndex < minrep){ // compare
				minrep= shiftedIndex;
			}
		}
		// store min. rep.
		if (m_period[index]<0){ // no spatial period
			m_strongRepresentativesList.add(minrep);	
		}

		// assign to each image found the min. representative
		for (int indexToMap : imageIndex){
			m_minimal[indexToMap]= minrep;
		}

	}

	/** convention -1 means not periodic by a smaller period **/ 
	static private int FindPeriod(int[] source) {
		boolean foundPeriod= false;
		int lenmax = source.length/2;
		int period=0;
		//Macro.Debug(" LENMAX " + lenmax);
		while ((!foundPeriod) && (period < lenmax )){ // "naive algo"
			period++;
			foundPeriod= HasPeriod(source,period);
			//Macro.fDebug("%s %d %b", 
				//	MathMacro.ArrayToStringNoFormatting(source), period, foundPeriod);
			
		}
		return foundPeriod?period:-1;
	}


	static private boolean HasPeriod(int[] source, int period) {
		int N= source.length;
		if (N % period != 0){ // prerequisite
			return false;
		} else {		
			boolean equal= true;
			int maxComp= N - period;
			for (int i=0; i <  maxComp; i++){ // not optimised !!!
				equal= equal && (source[i]== source[i+period]);
			}
			return equal;
		}
	}

	

	/*---------------------------------------------------------------------------
	  - get / set
	  --------------------------------------------------------------------------*/


	public  int GetRepresentative(int index) {
		return m_minimal[index];
	}
	
	public  ArrayList<Integer> GetStrongRepresentativeList() {
		return m_strongRepresentativesList;
	}
	
	public  int GetSpatialPeriod(int index) {
		return m_period[index];
	}

	/*---------------------------------------------------------------------------
	  - I / O
	  --------------------------------------------------------------------------*/


	private void Print() {
		for (int index=0; index < m_sz; index++){
			int imIndex= GetRepresentative(index);
			int [] 	
					source = MathMacro.Decimal2BinaryTab(index, m_N),
					image= MathMacro.Decimal2BinaryTab(imIndex, m_N);
			String line= String.format( "%d  ; %s ->> %s ; %d  [%d]", 
					index, MathMacro.ArrayToStringWithFormatting(source),
					MathMacro.ArrayToStringWithFormatting(image), imIndex, m_period[index]);
			Macro.print(line);
		}
	}

	/*---------------------------------------------------------------------------
	  - main & test
	  --------------------------------------------------------------------------*/



	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();

	public static void main(String [] argv){
		Macro.BeginTest(CLASSNAME);
		ConfigSpace sp= new ConfigSpace(6);
		sp.Print();
		Macro.SeparationLine();
		ConfigSpace sp2= new ConfigSpace(2);
		ArrayList<Integer> list = sp2.GetStrongRepresentativeList();
		String out = list.toString();
		sp2.Print();
		Macro.print(" OUT :" + out);
		
		
	}


	





}
