package cyclesMeasure.transitionGraph;

import main.MathMacro;

import components.types.IntC;


public class DeBruijnNodeInfo {

	// position ofthe eight nodes
	private static final int[] 
			X = {0, 1, 2, 5, 1, 4, 5, 6},
			Y = {2, 4, 2, 4, 0, 2, 0, 2};

	
		String m_label; // string representation
		IntC m_pos; // DOT placement
		// depending on the bit that is met:
		DeBruijnNodeInfo m_succ0, m_succ1;
		
		public DeBruijnNodeInfo(int NBIT, int i) {
			// label
			String seq= MathMacro.Int2BinaryString(i, NBIT);
			m_label= seq;
			// edge labels
		
			// positions
			SetPos(X[i],Y[i]);
		}

		public String toString(){
			return m_label;
		}
		public void SetPos(int x, int y) {
			m_pos= new IntC(x,y);
		}
	
}
