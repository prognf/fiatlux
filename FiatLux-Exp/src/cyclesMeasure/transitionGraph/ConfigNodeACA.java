package cyclesMeasure.transitionGraph;

import main.Convert;
import main.Macro;

import components.types.IntC;

public class ConfigNodeACA {

	private int m_index;
	private String m_state;
	private String m_tcode;
	private String m_xorrep; // additional info (x_i XOR x_{i+1})


	public ConfigNodeACA(int index, int N) {
		m_index= index;
		m_state= Convert.DecimaltoBinaryString(index, N);
		m_tcode= Convert.BinaryToTcode(m_state);
		//Macro.Debug(m_state +" :::>" + t_code);
		int nsize= m_state.length();
		m_xorrep="";
		for  (int i=0; i< nsize; i++){
			int next= (i+1)% nsize;
			m_xorrep += (m_state.charAt(i)==m_state.charAt(next))?'.':"X";
		}
	}

	public static int StateToIndex(String state) {
		int cindex= Convert.BinaryStringtoDecimal(state);
		return cindex;
	}

	/** used by NextState: only one position is modified **/
	public String CopyWithReplacementAt(int pos, int t) {
		char [] st = m_state.toCharArray();
		char c;
		if (t==0){
			c='0';
		} else if (t==1){
			c='1';
		} else {
			Macro.SystemWarning(" int val not binary" );
			c= Character.MIN_VALUE;
		}
		st[pos]= c;
		String newval= new String(st);
		return newval;
	}

	public String toString(){
		return m_state;
		/*String  out= String.format(" %s (%d) ", m_state, m_index); 
		return out;*/
	}

	/** number of ones in a given state */
	public int GetNumberOfOnes() {
		int sum= 0;
		for (char c : m_state.toCharArray()){
			sum += Convert.CharToBinVal(c);
		}
		return sum;
	}

	public int GetIndex() {
		return m_index;
	}
	public int GetSize() {
		return m_state.length();
	}

	public int GetCellState(int pos, int dpos) {
		int index = IntC.AddModulo(pos, dpos, GetSize());
		char c = m_state.charAt(index);
		int val= Convert.CharToBinVal(c);
		return val;
	}

	public String GetBinaryCodeAsString() {
		return m_state;
	}

	public String GetTcode() {
		return m_tcode;
	}
	
	
	/** for each cell, says if its equal to its right neighbour **/
	public String GetXorRepresentation() {
		return m_xorrep;
	}

	public String GetPairsRepresentation() {
		int nsize= m_state.length();
		StringBuilder out= new StringBuilder();
		for  (int i=0; i< nsize; i++){
			int next= (i+1)% nsize;
			char c=m_xorrep.charAt(i), cn=m_xorrep.charAt(next);
			//out += (c==cn)?(m_xorrep.charAt(i)=='.'?"A":"B"):c;
			out.append(((c == 'X') & (cn == '.')) ? 'Z' : ' ');
		}
		return out.toString();
	}

}
