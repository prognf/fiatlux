package cyclesMeasure.transitionGraph;

import java.util.ArrayList;

import models.CAmodels.tabled.LookUpTableBinary;

public class AbstractTransitionGraph {

	//--------------------------------------------------------------------------
		//- attributes
		//--------------------------------------------------------------------------

		int m_N, m_size; // size : number of nodes = 2 ^N
		/*private*/ ArrayList<ConfigNodeACA> m_ConfigList ; // configspace
		ConfigNodeACA [][] m_TransitionGraph;
		LookUpTableBinary m_LocalRuleTable ;
		
		
		protected void CreateConfig() {
			m_ConfigList= new ArrayList<ConfigNodeACA>();
			for (int cindex=0; cindex < m_size; cindex++){
				m_ConfigList.add(new ConfigNodeACA(cindex, m_N));
			}		
		}
		//--------------------------------------------------------------------------
		//- get / set
		//--------------------------------------------------------------------------
		
		public ConfigNodeACA GetConfig(int cindex) {
			return m_ConfigList.get(cindex);
		}


		public ConfigNodeACA[] GetSucc(int cindex) {
			return m_TransitionGraph[cindex];
		}
	
}
