package cyclesMeasure.transitionGraph;

import java.util.ArrayList;

import components.types.RuleCode;
import main.Macro;
import main.MathMacro;
import main.commands.CommandParser;
import models.CAmodels.tabled.ECAlookUpTable;
import models.CAmodels.tabled.LookUpTableBinary;

/*-------------------------------------------------------------------------------
 * for the study of fully asynch. ECA
 * collaboration with Sukanta Das
 *-----------------------------------------------------------------------------*/

public class TransitionGraphFullyAsynch {


	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();
	private static final int OCTOPUS = 8; // ECA table


	//private static final int [] SIZEGEN = {8, 10};

	//--------------------------------------------------------------------------
	//- attributes
	//--------------------------------------------------------------------------

	int m_N, m_size; // size : number of nodes = 2 ^N
	ArrayList<ConfigNodeACA> m_ConfigList ; // configspace
	ConfigNodeACA [][] m_transitionGraph;
	LookUpTableBinary m_LocalRuleTable ;
	
	
	//--------------------------------------------------------------------------
	//- construction
	//--------------------------------------------------------------------------

	/** size = the number of cells in the ACA 
	 * @param W */
	public TransitionGraphFullyAsynch(RuleCode W, int N) {
		m_N= N;
		m_size= MathMacro.TwoToPowerIntResult(N);
		m_LocalRuleTable= new LookUpTableBinary(OCTOPUS);
		m_LocalRuleTable.SetRule(W);
		createConfigrations();
	}

	private void createConfigrations() {
		m_ConfigList= new ArrayList<ConfigNodeACA>();
		for (int cindex=0; cindex < m_size; cindex++){
			m_ConfigList.add(new ConfigNodeACA(cindex, m_N));
		}		
	}
	
	/** each configuration is associated with *n* configurations **/
	public void BuildGraph(){
		m_transitionGraph= new ConfigNodeACA[m_size][m_N];
		for (int cindex=0; cindex < m_size; cindex++){
			ConfigNodeACA source = GetConfig(cindex);
			for (int pos=0; pos < m_N; pos++){
				ConfigNodeACA next = CalculateNextState(source, pos);
				m_transitionGraph[cindex][pos]= next;
			}
			//Check(cindex);
		}
	}

	/*private void Check(int cindex) {
		String out= GetConfig(cindex) + " -> ";
		for (int pos=0; pos < m_N; pos++){
			out += m_TransitionGraph[cindex][pos] + ":";
		}
		//Macro.Debug( out );
	}*/

	/** inversion between the coding pos and the "real pos" (in the tab) **/
	private ConfigNodeACA CalculateNextState(ConfigNodeACA source, int tabpos) {

		int 
			x= source.GetCellState(tabpos,-1),
			y= source.GetCellState(tabpos, 0),
			z= source.GetCellState(tabpos, +1);
		int t = 4* x + 2* y + z;
		int T= m_LocalRuleTable.getBitTable(t);
		String nextSt= source.CopyWithReplacementAt(tabpos,T);
		int nextConfigIndex= ConfigNodeACA.StateToIndex(nextSt);
		ConfigNodeACA next= GetConfig(nextConfigIndex);
		return next;
	}

	/*private int GetCellState(int[] state, int pos, int dpos) {
		int newpos= IntC.AddModulo(pos, dpos, m_N);
		return state[newpos];
	}*/
	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------


	//--------------------------------------------------------------------------
	//- get / set
	//--------------------------------------------------------------------------
	public ConfigNodeACA GetConfig(int cindex) {
		return m_ConfigList.get(cindex);
	}


	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------

	public static void main(String [] argv){
		CommandParser parser = new CommandParser(CLASSNAME, "", argv);
		int Win= parser.IParse("W=");
		RuleCode W = new RuleCode(Win);
		int size= parser.IParse("N=");
		DoExp(W, size);
		/*for (int size : SIZEGEN){
			DoExp(W, size);	
		}*/
	}

	private static void DoExp(RuleCode W, int N) {
		TransitionGraphFullyAsynch gr = new TransitionGraphFullyAsynch(W,N);
		gr.BuildGraph();
		String Tcode= ECAlookUpTable.GetTransitionCodePadded(W).trim();
		String rulename= "ECA-W"+ W + "-" + Tcode + "-N-" + N + ".dot";
	    DotFile.ExportAsDotFileFullyAsynch(gr, rulename);
		
	}

	public ConfigNodeACA GetSucc(int cindex,int pos) {
		return m_transitionGraph[cindex][pos];
	}


}
