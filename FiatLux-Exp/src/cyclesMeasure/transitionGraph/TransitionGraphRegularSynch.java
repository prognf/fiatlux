package cyclesMeasure.transitionGraph;

import main.Macro;
import main.MathMacro;


/**** used for studying global synch problem **/
public class TransitionGraphRegularSynch  {

	//--------------------------------------------------------------------------
	//- attributes
	//--------------------------------------------------------------------------
	
	protected int m_size;   // number of node-configurations
	ConfigNodeA [] m_nodeArray; //array of node-configurations  TODO : change to list ?

	//--------------------------------------------------------------------------
	//- construction
	//--------------------------------------------------------------------------
	
	TransitionGraphRegularSynch(int N) {
		m_size= MathMacro.TwoToPowerIntResult(N);
		m_nodeArray = new ConfigNodeA[m_size];

		initClassical(N);
	}

	public void initClassical(int N){
		for (int pos=0; pos < m_size ; pos++){
			int [] state= AnalyseRuleGlobalSynchProblem.IndexToTableState(N,pos);
			m_nodeArray[pos]= new ConfigNodeGlobalSynch(pos, state); 
		}
	}

	/*---------------------------------------------------------------------------
	  - analysis
	  --------------------------------------------------------------------------*/


	/*---------------------------------------------------------------------------
	  - OUTPUT
	  --------------------------------------------------------------------------*/


	public void printGraph() {
		for (int pos=0; pos < m_size ; pos++){
			String s = String.format(" pos: %d | %s  ", 
					pos, getNode(pos).toString());
			Macro.print(s);
		}		
	}

	/*---------------------------------------------------------------------------
	  - get / set
	  --------------------------------------------------------------------------*/

	public void AddOrientedLink(int pos, int successorPos) {
		m_nodeArray[pos].AddSuccessor(m_nodeArray[successorPos]);
		m_nodeArray[successorPos].AddPredecessor(m_nodeArray[pos]);
	}

	protected ConfigNodeA getNode(int pos) {
		return m_nodeArray[pos];
	}

	public ConfigNodeA[] getNodeList() {
		return m_nodeArray;
	}


}
