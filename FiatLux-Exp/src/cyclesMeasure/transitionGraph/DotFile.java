package cyclesMeasure.transitionGraph;

import java.util.ArrayList;

import components.types.FLStringList;
import cyclesMeasure.transitionGraph.DeBruijnDiagram.NodeInfoTypeB;
import kuklo.CommunicationClass;
import kuklo.NodeList;
import kuklo.NodePatron;

public class DotFile extends FLStringList {

	static final int PENWIDTH = 6;


	private static final boolean NOARROW = true;


	private static final boolean PLACEMENTWITHRANK = false; // ordering of the nodes

	/** concentrated = false **/
	void AddDotFileHeadA(String title) {
		Add("digraph testG {");
		Add("	rankdir=LR");
		Add("	node [shape=box fontname=\"Courier\"];");
		Add("	label= \"" + title + "\";");
		Add("   labelloc=top;  // title location");
	}

	/** concentrated = true **/
	public void AddDotFileHeadB(String title) {
		Add("digraph testG {");
		Add("	rankdir=LR");
		Add("	concentrate=true  // to avoid two edges between two nodes");
		Add("	node [shape=box fontname=\"Courier\"];");
		Add("	label= \"" + title + "\";");
		Add("   labelloc=top;  // title location");
	}

	public void AddTail() {
		Add("}");//end of file
	}

	public void AddNodeWithPos(DeBruijnNodeInfo node) {
		String txt= String.format(" %s [pos=\"%d,%d!\"]",
				node, node.m_pos.X(), node.m_pos.Y());	
		Add(txt);
	}

	public void AddTwoEdges(NodeInfoTypeB node, String emphasisTcode) {
		String 
		txt0= Edge(node,0, emphasisTcode), 
		txt1= Edge(node,1, emphasisTcode);
		Add(txt0,txt1);
	}


	void AddEdgeStrong(DeBruijnNodeInfo nodeA, DeBruijnNodeInfo nodeB, String label, String color){
		String strongCmd= " penwidth=" + PENWIDTH;
		String edge= String.format(" %s -> %s [label=\"%s\" color=\"%s\" %s ];",
				nodeA, nodeB, label, color, strongCmd);
		Add(edge);
	}

	void AddEdge(DeBruijnNodeInfo nodeA, DeBruijnNodeInfo nodeB, String label, String color){
		String edge= String.format(" %s -> %s [label=\"%s\" color=\"%s\" ];",
				nodeA, nodeB, label, color);
		Add(edge);
	}

	void AddTwoEdges(DeBruijnNodeInfo node) {
		String col="black"; //EDGECOLOR
		AddEdge(node, node.m_succ0, "0", col);
		AddEdge(node, node.m_succ1, "1", col);
	}

	void AddTwoEdgesDifferentiated(DeBruijnNodeInfo node, int bit) {
		String col="black"; //EDGECOLOR
		if (bit==1){
			AddEdge(node, node.m_succ0, "0", col);
			AddEdgeStrong(node, node.m_succ1, "1", col);
		} else {
			AddEdgeStrong(node, node.m_succ0, "0", col);
			AddEdge(node, node.m_succ1, "1", col);
		}
	}

	/** if letter is found in string "emphasis", edge is in bold **/
	static String Edge(NodeInfoTypeB node, int bit, String emphasis) {
		//	int tr= node.m_tr[bit];
		String transLetter= node.m_transLabel[bit];
		String col="black"; //EDGECOLOR[tr]
		String edgeS= 
				String.format(" %s -> %s [label=\"[%s]\" color=\"%s\"", 
						node, node.m_succ[bit], transLetter, col);
		//Macro.fDebug(" test %s in %s", emphasis, transLetter);
		if ((emphasis.contains(transLetter))){
			edgeS += " penwidth=" + PENWIDTH;
		}
		edgeS += "] ;";
		return edgeS;
	}
	/*
	private void AddDotFilePlacement(FLStringList dotfile) {
		dotfile.Add(TwoPlacesAtSameRank(1, 4));
		dotfile.Add(TwoPlacesAtSameRank(3, 6));
	}*/
	/*
	private String TwoPlacesAtSameRank(int i, int j) {
		return "{ rank = same; "+ m_diagram[i] +"; " + m_diagram[j] + ";}";
	}*/


	/** for transition graph analysis ; not "compatible" with fully asynch. graph
	 * used for GLOBAL synchronisation problem studies 
	 * filterNonSynch = true by default : what is it for ?
	 * */
	static public void exportAsDotFileGlobalSynch(String filename, int Nsize, 
			final boolean filterNonSynch, TransitionGraphGlobalSynch graph){
		FLStringList dotfile = new FLStringList();
		dotfile.Add("digraph testG {");
		dotfile.Add("node [shape=record ];");

		ArrayList<ConfigNodeGlobalSynch> representativeNodeList= graph.GetNodeList();

		// graphInfo
		for (ConfigNodeGlobalSynch node : representativeNodeList){
			boolean includeingraph = filterNonSynch || node.m_height==-1;
			if (includeingraph){
				String line=  ""+ node.GetIndex(); //RepresentNode(node,Nsize);
				line +=  " -> ";
				ConfigNodeA succ= node.GetSucc();
				line += succ.GetIndex() + " ";
				dotfile.Add(line);
			}
		}

		//nodeInfo
		for (ConfigNodeGlobalSynch node : representativeNodeList){
			String nodeLine= node.GetIndex() + " [ label=\""
					+ node.GetLabel() + "\"]";
			dotfile.Add(nodeLine);
		}

		// graphical position of the nodes
		String placementInfo = "{ rank = same; 0; "+ (graph.m_size - 1) + ";}";
		dotfile.Add(placementInfo);

		dotfile.Add("}");//end of file
		//int maxH = Math.max(m_maxFromZero, m_maxFromOne);
		//String heightLine = String.format("# maxh:%d max-0:%d max-1:%d", maxH, m_maxFromZero, m_maxFromOne);
		//dotfile.Add(heightLine);
		dotfile.WriteToFile(filename);
	}


	/** for transition graph analysis : regular case
	 * */
	static public void exportAsDotFileRegularSynchUpdate(String filename, int Nsize, 
			final boolean filterNonSynch, TransitionGraphRegularSynch graph){
		FLStringList dotfile = new FLStringList();
		dotfile.Add("digraph testG {");
		dotfile.Add("node [shape=record ];");

		ConfigNodeA [] representativeNodeList= graph.getNodeList();

		// graphInfo
		for (ConfigNodeA node : representativeNodeList){
			String line=  ""+ node.GetIndex(); //RepresentNode(node,Nsize);
			line +=  " -> ";
			ConfigNodeA succ= node.GetSucc();
			line += succ.GetIndex() + " ";
			dotfile.Add(line);

		}

		//nodeInfo 
		for (ConfigNodeA node : representativeNodeList){
			String nodeLine= node.GetIndex() + " [ label=\"" + node.getLabel() +"\"";
			if (node.hasTagFixedPoint()) {
				nodeLine+= ", style=\"filled\", fillcolor=\"green\" ";
			} else if (node.hasCyclicTag()) {
				nodeLine+= ", style=\"filled\", fillcolor=\"yellow\" ";
			}
			nodeLine += "]";
			dotfile.Add(nodeLine);
		}

		// graphical position of the nodes
		if (PLACEMENTWITHRANK) {
			String placementInfo = "{ rank = same; 0; "+ (graph.m_size - 1) + ";}";
			dotfile.Add(placementInfo);
		}

		dotfile.Add("}");//end of file
		//int maxH = Math.max(m_maxFromZero, m_maxFromOne);
		//String heightLine = String.format("# maxh:%d max-0:%d max-1:%d", maxH, m_maxFromZero, m_maxFromOne);
		//dotfile.Add(heightLine);
		dotfile.WriteToFile(filename);
	}





	/** for transition fully asynch. graph*/
	static public void ExportAsDotFileFullyAsynch( 
			TransitionGraphFullyAsynch graph, String filename){
		FLStringList dotfile = new FLStringList();
		dotfile.Add("digraph testG {");
		dotfile.Add("node [shape=record ];");
		dotfile.Add("rankdir=\"LR\";");

		// graphInfo
		int nsize= graph.m_N;
		for (int cindex=0; cindex < graph.m_size; cindex++){
			ConfigNodeACA node= graph.GetConfig(cindex);
			for (int pos=0; pos<nsize; pos++) {
				String line=  ""+ node.GetIndex(); //RepresentNode(node,Nsize);
				line +=  " -> ";
				ConfigNodeACA succ= graph.GetSucc(cindex, pos);
				if (succ != node) { //remove loops
					line += succ.GetIndex() + " ";
					dotfile.Add(line);
				}
			}
		}

		//nodeInfo
		for (int cindex=0; cindex < graph.m_size; cindex++){
			ConfigNodeACA node= graph.GetConfig(cindex);
			String nodeLine= node.GetIndex() + " [ label=\""
					+ node.GetBinaryCodeAsString() + "\"]";
			dotfile.Add(nodeLine);
		}

		// graphical position of the nodes
		//rank nodes
		String rankLine= "R" + (0) + " ";
		for (int i=1;i<nsize+1;i++) {
			rankLine+= " -> R" + (i); // Ri : rank node, invert order
		}
		rankLine += ";";
		dotfile.Add(rankLine);

		StringBuffer[] placement = new StringBuffer[nsize+1];
		for (int i=0;i<nsize+1;i++) {
			String sprefix= "{ rank = same; ";
			sprefix += "R"+i +";"; // Ri : rank node
			placement[i]= new StringBuffer(sprefix);
		}
		for (int cindex=0; cindex < graph.m_size; cindex++){
			ConfigNodeACA node= graph.GetConfig(cindex);
			String binlabel= node.GetBinaryCodeAsString();
			// counting the number of ones
			long count = binlabel.chars().filter(ch -> ch == '1').count();
			placement[(int)count].append(cindex + ";");
		}
		for (int i=0;i<nsize+1;i++) {
			placement[i].append("}");
			dotfile.Add(""+placement[i]);
		}
		//

		dotfile.Add("}");//end of file
		//int maxH = Math.max(m_maxFromZero, m_maxFromOne);
		//String heightLine = String.format("# maxh:%d max-0:%d max-1:%d", maxH, m_maxFromZero, m_maxFromOne);
		//dotfile.Add(heightLine);
		dotfile.WriteToFile(filename);
	}

	public static String OneLineOneNode(NodePatron source, ArrayList<NodePatron> succList) {
		StringBuilder outline = new StringBuilder(source.getCode() + " -> { ");
		for ( NodePatron succ : succList){
			outline.append(succ.getCode()).append(" ; ");
		}
		outline.append(" } ");
		if (NOARROW){
			outline.append(" [arrowhead=none,arrowtail=none]");
		}
		outline.append(" ; ");
		return outline.toString();
	}

	/** one line **/
	private static String zzzOneLineOneNode(ConfigNodeACA source,
			ConfigNodeACA[] succList) {
		int srcId = source.GetIndex(); //RepresentNode(node,Nsize);
		StringBuilder line = new StringBuilder(" -> { ");
		boolean selfLoop= false;
		for (ConfigNodeACA succ : succList){
			boolean loop= (succ.GetIndex() == srcId); 
			if (!loop){
				line.append(succ.GetIndex()).append(" ; ");
			} else {
				selfLoop= true;
			}
		}
		line.append(" }; ");
		String styl="style=\"filled\" fillcolor=" + 
				(selfLoop?"\"white\"":"\"lightyellow\"") ; 
		String lbl= " label=\""+source.GetBinaryCodeAsString();
		lbl+= "\"";
		line = new StringBuilder(srcId + line.toString() + srcId + " [" + styl + lbl + "];");
		return line.toString();
	}

	/*** creates the "same rank" attributes for placing nodes 
	 * assumes the following mapping : index -> ranktab
	 * N=> ranks in {0,...,N} 
	 * @param rankArray **/
	public void WriteNodePositions(CommunicationClass commclass) {
		ArrayList<NodeList> nodeLevels = commclass.m_level;
		int numC= nodeLevels.size();
		String tagAux= "C"+ commclass.m_tag;
		// invisible nodes
		for (int rank=0 ; rank < numC; rank++){
			String lineInvis= AuxNode(rank, tagAux) + " [shape = none,label=\"\",width=0, height=0] ";
			//Macro.Debug(lineInvis);
			Add(lineInvis);
		}
		// alignment of nodes
		for (int rank=0 ; rank < numC ; rank++){
			StringBuilder line= new StringBuilder(" { rank=same ; " + AuxNode(rank, tagAux) + "  ");
			for (NodePatron node : nodeLevels.get(rank)){
				line.append(node.getCode()).append(" ; ");
			}
			line.append(" }");
			Add(line.toString());
		}
		// invisible line between invisible nodes
		// A0 -> A1 -> ... -> Ak
		StringBuilder auxLine= new StringBuilder();
		for (int rank=0 ; rank < numC-1; rank++){
			auxLine.append(AuxNode(rank, tagAux)).append(" -> ");
		}
		auxLine.append(AuxNode(numC - 1, tagAux)).append(" [style=invis]"); // last node
		Add(auxLine.toString());

	}


	/*** creates the "same rank" attributes for placing nodes **/
	/*private void DealWithNodePositions(FullyAsynchTransitionGraph gr) {
		ArrayList<ConfigNodeACA> nodelist = gr.m_ConfigList;
		int N= gr.m_N;
		// init
		IntegerList rankTab= new IntegerList();
		// sort by number of ones
		for (ConfigNodeACA node : nodelist){
			int val = node.GetNumberOfOnes();
			rankTab.add(val);
		}
		ProcessNodePositions(N, rankTab);
	}*/

	/** auxilliary node for linear placement with dot 
	 * @param tagAux **/
	private String AuxNode(int rank, String tagAux) {
		return tagAux + "A" + rank ;
	}


}
