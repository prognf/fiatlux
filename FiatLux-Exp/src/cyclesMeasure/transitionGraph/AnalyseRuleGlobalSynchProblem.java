package cyclesMeasure.transitionGraph;

import java.util.ArrayList;

import components.allCells.OneRegisterIntCell;
import components.arrays.GenericCellArray;
import components.arrays.RegularDynArray;
import components.types.FLsignal;
import components.types.RuleCode;
import experiment.samplers.LinearSampler;
import explore.satSolve.SatGen;
import initializers.CAinit.BinaryInitializer;
import initializers.CAinit.BinaryInitializer.INITSTYLE;
import main.Convert;
import main.FiatLuxProperties;
import main.Macro;
import main.MathMacro;
import main.commands.CommandParser;
import models.CAmodels.CellularModel;
import models.CAmodels.tabled.ECAmodel;
import models.CAmodels.tabled.GeneralTabledBinaryModel;
import topology.basics.LinearTopology;
import topology.zoo.RadiusN_Topology;

/* *
 * analysis of the transition graph of (a)synchronous rules 
 * **/
public class AnalyseRuleGlobalSynchProblem {


	public static final String CODEPREFIX = "rule";

	/*---------------------------------------------------------------------------
	  - attributes
	  --------------------------------------------------------------------------*/

	final RuleCode m_Wcode; //rule code
	final int m_N; // number of cells

	LinearSampler m_samp;
	private BinaryInitializer m_init; // for init patterns 
	private GenericCellArray<OneRegisterIntCell> m_array;
	TransitionGraphGlobalSynch m_graph; 


	int [] m_nextSt; // for decomposition decimal<-> binary


	/*---------------------------------------------------------------------------
	  - construction
	  --------------------------------------------------------------------------*/


	public AnalyseRuleGlobalSynchProblem(int K, RuleCode Wcode, int cellSize){
		m_N= cellSize;
		m_Wcode= Wcode;

		m_graph = new TransitionGraphGlobalSynch(m_N);
		CellularModel model;
		int leftN, rightN;
		if (K==3){
			Macro.print("ECA case");
			model = new ECAmodel(Wcode);
			leftN= -1; rightN= 1;
		} else {
			int TableSize= MathMacro.Power(2, K);
			model = new GeneralTabledBinaryModel(TableSize, Wcode);
			leftN=-(K-1)/2; rightN= K/2;
		}

		LinearTopology 	topo = new RadiusN_Topology(leftN,rightN);
		//topo.SetSize(N);

		m_samp = new LinearSampler(m_N, model, topo);
		RegularDynArray sys= m_samp.GetAutomaton();
		m_array= new GenericCellArray<OneRegisterIntCell>(sys);

		m_init= (BinaryInitializer)m_samp.GetInitializerAsSuper();
		m_init.ForceInitStyle(INITSTYLE.Pattern);

		m_nextSt=new int[m_N]; // for the computation of the next state
		// main
		Analysis();

	}




	/*---------------------------------------------------------------------------
	  - main part
	  --------------------------------------------------------------------------*/

	/** main function **/
	private void Analysis() {
		int SIZE= m_graph.m_size;
		Macro.print(" SIZE :" + SIZE);
		/// WARNING : ZERO and ONE configs are excluded !!
		m_graph.SpecialCaseZeroOne();// special case for ZERO and ONE
		// builds the transition graph
		for (int index=1; index< SIZE-1; index++){
			CalculateNodeSuccessor(index);
		}

		CalculateHeights();
		m_graph.SetUpSourceAndLaunchRecursiveSearchOfEquivalence();

	}

	/** calculates next state and updates graph **/
	private void CalculateNodeSuccessor(int index){
		ConfigNodeGlobalSynch src= m_graph.GetNode(index);
		String pattern= TableStateToString(src.m_state);

		//Macro.Debug( "index: "+ index + " PATT:" + pattern);
		m_init.SetInitPattern(pattern);
		m_init.ReceiveSignal(FLsignal.init);
		//m_samp.print();
		//Macro.Debug("curr: " + MathMacro.ArrayToString(src.m_state));
		// one step beyond !
		m_samp.sig_NextStep();

		// next state index 
		for (int i=0; i < m_N; i++){
			m_nextSt[i]= m_array.GetCell(i).GetState();	
		}

		int isucc= TableStateToIndex(m_nextSt);//cast here
		//Macro.Debug("next: " + MathMacro.ArrayToString(m_nextSt) + " i: " + isucc);

		// add link in graph
		m_graph.AddOrientedLink(index, isucc);		
		ConfigNodeA succ= m_graph.GetNode(isucc);
		// updates "activity array"
		src.ComputeActivityWithSuccessor(succ);

	}

	/*---------------------------------------------------------------------------
	  - coding
	  --------------------------------------------------------------------------*/

	static public String TableStateToString(int[] m_state) {
		return 	MathMacro.ArrayToStringNoFormatting(m_state);
	}

	/** from state to index **/
	static public int TableStateToIndex(int[] tabState) {
		return (int)MathMacro.InvBinaryTab2Decimal(tabState);
	}

	/** from state to index **/
	static public int StringToIndex(String pattern) {
		return Convert.BinaryStringtoDecimal(pattern);
	}


	public static int[] IndexToTableState(int N, int index) {
		return MathMacro.Decimal2BinaryTabInverted(index, N);
	}

	public static String IndexToStringConfig(int N, int index) {
		return MathMacro.Int2BinaryString(index, N);
	}

	/*---------------------------------------------------------------------------
	  - graph height 
	  --------------------------------------------------------------------------*/

	int m_maxFromZero, m_maxFromOne; // max heights  

	public void CalculateHeights() {
		m_maxFromZero= RecursiveCalculusWith(m_graph.m_zero,0);
		m_maxFromOne=RecursiveCalculusWith(m_graph.m_one,0);
	}

	private int RecursiveCalculusWith(ConfigNodeGlobalSynch node, int height) {
		int maxh= -1;
		if (node.m_height==-1){ // unvisited node
			node.m_height= height;
			maxh= height;
			for (ConfigNodeA predecessor : node.GetPredecessors()){
				int otherh = RecursiveCalculusWith((ConfigNodeGlobalSynch)predecessor, height+1); //cast here
				maxh = MathMacro.Max(maxh,otherh);
			}
		} 
		return maxh;
	}


	/*---------------------------------------------------------------------------
	  - states
	  --------------------------------------------------------------------------*/

	protected void PrintGraph() {
		m_graph.PrintGraph();
	}

	protected void PrintHeight(){
		Macro.fPrint(" max from O: %d max from 1: %d ", 
				m_maxFromZero, m_maxFromOne);
	}

	/*---------------------------------------------------------------------------
	  - I/O
	  --------------------------------------------------------------------------*/

	public void ExportData(){
		Macro.print(3,"exporting dot file...");
		String filenameA= SatGen.CODEPREFIX + m_Wcode + "-N-" + m_N + ".dot";
		DotFile.exportAsDotFileGlobalSynch(filenameA, m_N, true, m_graph);
	}


	/** prints the number of predecessors of zero and one
	 * detects rules that have predecessors from both zero and one */
	public void ReadSynchPoints(){
		int 
		npred0= m_graph.m_zero.GetPredecessors().size(),
		npred1= m_graph.m_one.GetPredecessors().size();
		int ncyc = m_graph.CountCyclicStates();

		String predZeroOneLine= 
				String.format("W=%s npred0:%d npred1:%d nCyc:%d",
						m_Wcode, npred0, npred1, ncyc);
		if ((npred0>0) && (npred1>0)){
			Macro.print("pred ALARM:" + m_Wcode);
			//PRN( m_graph.m_zero.GetPredecessors(),0 );
			//PRN( m_graph.m_one.GetPredecessors(),1 );
		}
		Macro.print(predZeroOneLine);
	}

	private void PRN(ArrayList<ConfigNodeGlobalSynch> predecessors, int source) {
		StringBuilder s= new StringBuilder(source + " >>");
		for (ConfigNodeA node : predecessors){
			s.append(" : ").append(node.toString());
		}
		Macro.print(s.toString());
	}


	/*---------------------------------------------------------------------------
	  - main & test
	  --------------------------------------------------------------------------*/

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();
	protected static final int NARG = 3;

	/** analysis of one dimension **/
	public static void main(String [] argv){
		FiatLuxProperties.SetVerboseLevel(3);
		CommandParser parser = new CommandParser(CLASSNAME, "", argv);
		/*if (argv.length==1){
			RuleCode Wcode= 
					RuleCode.FromString(argv[0]);
			AnalyseRule ana= new AnalyseRule(4, Wcode, 5);
			ana.ReadSynchPoints(Wcode);
			ana.ExportData(Wcode, 5);
		} else*/ 
		if (argv.length == NARG ){
			int K= parser.IParse("K=");
			RuleCode Wcode= 
					RuleCode.FromString(parser.SParse("W="));
			int N= parser.IParse("N=");
			AnalyseRuleGlobalSynchProblem ana= new AnalyseRuleGlobalSynchProblem(K, Wcode, N);
			ana.ReadSynchPoints();
			ana.PrintHeight();
			ana.ExportData();
		} else {
			parser.Usage("K=neighbSize W=rule N=size");
		}
	}


}
