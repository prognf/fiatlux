package cyclesMeasure.transitionGraph;

import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import main.Convert;
import main.Macro;
import main.MathMacro;
import models.CAmodels.tabled.LookUpTableBinary;
import components.types.IntC;
import components.types.RuleCode;

public class DeBruijnDiagram {

	// convention: least significant bit has index 0
	int TWO=2;
	final static int N=3, V=N+1; // V = neighb size
	final static int SZ = MathMacro.TwoToPowerIntResult(N);	
	final static int SZTABLE = 2*SZ; // size of the transition table 
	private static final int[] 
			X = {0, 1, 2, 5, 1, 4, 5, 6},
			Y = {2, 4, 2, 4, 0, 2, 0, 2};
	final private static String [] 
			EDGELABEL0= {"a", "c", "k", "i", "g", "e", "m", "o"},
			EDGELABEL1= {"b", "d", "l", "j", "h", "f", "n", "p"};


	private static final String[] EDGECOLOR = {"red","blue"};//GraphViz
	static Map<String,Integer> letterToBitPos;
	static Map<Integer,String> bitPosToLetter;

	//--------------------------------------------------------------------------
	//- attributes
	//--------------------------------------------------------------------------

	NodeInfoTypeB [] m_diagram= new NodeInfoTypeB[SZ];
	LookUpTableBinary m_table;

	//--------------------------------------------------------------------------
	//- inner class
	//--------------------------------------------------------------------------

	class NodeInfoTypeB extends DeBruijnNodeInfo {
		// depending on the bit that is met:
		NodeInfoTypeB [] m_succ= new NodeInfoTypeB[TWO]; // next
		int [] m_tr= new int[TWO]; // transition of the local rule 
		String [] m_transLabel= new String[TWO];

		public NodeInfoTypeB(int i) {
			super(TWO,i);
			// label
			String seq= MathMacro.Int2BinaryString(i, N);
			m_label= seq;
			// edge labels
			m_transLabel[0]= EDGELABEL0[i];
			m_transLabel[1]= EDGELABEL1[i];			
			// positions
			SetPos(X[i],Y[i]);
		}

		public void SetPos(int x, int y) {
			m_pos= new IntC(x,y);
		}
	}

	//--------------------------------------------------------------------------
	//- construction
	//--------------------------------------------------------------------------


	public DeBruijnDiagram(RuleCode W){
		m_diagram= new NodeInfoTypeB[SZ];
		m_table = new LookUpTableBinary(SZTABLE);
		m_table.SetRule(W);
		//Macro.Debug("SZ:"+SZ);
		for (int i=0; i< SZ; i++){
			m_diagram[i]= new NodeInfoTypeB(i);
		}	
		for (int i=0; i< SZ; i++){
			DealWithNode(m_diagram[i]);
		}		
	}

	//--------------------------------------------------------------------------
	//- construction of the diagram
	//--------------------------------------------------------------------------


	private void DealWithNode(NodeInfoTypeB node) {

		//Macro.fDebug(" i:%d s:%s",i,node.m_label);
		ComputeTransition(node,0);
		ComputeTransition(node,1);

	}

	private void ComputeTransition(NodeInfoTypeB node, int bit) {
		String stran= node.m_label + bit;
		Integer bittable= Convert.BinaryStringtoDecimal(stran);

		// output of the rule when a given bit is met (and transition is done)
		int trInTable= m_table.getBitTable(bittable);
		node.m_tr[bit]= trInTable; 

		// successor node
		String next= stran.substring(1);
		int inext= Convert.BinaryStringtoDecimal(next);
		node.m_succ[bit]= m_diagram[inext];
		//Macro.fDebug("%s %d -> %s %d", node.m_label, bit, next, inext);

	}


	//--------------------------------------------------------------------------
	//- static construction
	//--------------------------------------------------------------------------

	/** takes a list of letters with transitions to one
	 * a converts to RuleCode **/ 
	static RuleCode LettersTransOneToCode(String lettersToOne){
		LookUpTableBinary tab=new LookUpTableBinary(SZTABLE);
		tab.SetRule(RuleCode.NULLRULE);
		SetUpTransitionLabels();
		for (char c : lettersToOne.toCharArray()){
			int bitPos= letterToBitPos.get(""+c);
			tab.SetRuleBit(bitPos, 1);
		}
		RuleCode W= tab.GetRuleCode();
		return W;
	}

	/** CALL BEFORE USING OTHER METHODS **/
	static private void SetUpTransitionLabels() {
		Macro.fPrintNOCR(" buliding map of transitions... ");
		// first map
		Map<String,String> letterToNeighbSt= new TreeMap<String,String>();
		for (int i=0 ; i<SZ; i++){
			String seq= MathMacro.Int2BinaryString(i, N);
			letterToNeighbSt.put(EDGELABEL0[i], seq + "0");
			letterToNeighbSt.put(EDGELABEL1[i], seq + "1");
		}
		// second map : not optimised, but who cares ?
		letterToBitPos= new TreeMap<String, Integer>();
		bitPosToLetter= new TreeMap<Integer, String>();
		for ( Entry<String, String> entry: letterToNeighbSt.entrySet()){
			String neighb= entry.getValue();
			Integer bitpos= Convert.BinaryStringtoDecimal(neighb);
			String letter= entry.getKey();
			//Macro.fPrint(" %s -> %s (%d)", letter, neighb, bitpos);
			letterToBitPos.put(letter, bitpos);
			bitPosToLetter.put(bitpos, letter);
		}
		Macro.fPrintNOCR(" done. ");
	}

	//--------------------------------------------------------------------------
	//- DOT formatting
	//--------------------------------------------------------------------------



	private void ExportDotFile(String pattern) {
		String emphasisTcode = BinarytoTcode(pattern);
		DotFile dotfile= new DotFile();
		dotfile.AddDotFileHeadA(emphasisTcode + " : " + pattern);
		// nodes 
		for (NodeInfoTypeB node: m_diagram ){
			dotfile.AddNodeWithPos(node);
		}
		// edges
		for (NodeInfoTypeB node: m_diagram ){
			dotfile.AddTwoEdges(node, emphasisTcode);
		}
		//AddDotFilePlacement(dotfile);
		dotfile.AddTail();
		dotfile.Print();
		int len= emphasisTcode.length();
		dotfile.WriteToFile("diag-" + len + "-" + emphasisTcode + ".dot");
	}








	/*---------------------------------------------------------------------------
	  - rule & config. handling
	  --------------------------------------------------------------------------*/

	/** use SetUpTransitionLabels() before **/
	static public String BinarytoTcode(String binPattern){
		int len = binPattern.length();
		// append (by looping) at the end in order to apply the rule 
		binPattern = binPattern.substring(len - N , len) + binPattern;

		StringBuilder Tcode= new StringBuilder();
		for (int i=0; i < len; i++){
			String neighbSt= binPattern.substring(i, i+V);
			Integer bitPos= Convert.BinaryStringtoDecimal(neighbSt);
			String letter= bitPosToLetter.get(bitPos);
			Tcode.append(letter);
		}
		return Tcode.toString();
	}


	static void FindMysteryRule(String transToOne){
		LookUpTableBinary tab= new LookUpTableBinary(SZTABLE);
		for (char t : transToOne.toCharArray()){
			Integer bitPos= letterToBitPos.get(""+t);
			tab.SetRuleBit(bitPos, 1);
		}
		RuleCode W= tab.GetRuleCode();
		Macro.print(" candide:" + W);
	}

	//--------------------------------------------------------------------------
	//- main
	//--------------------------------------------------------------------------

	/** ALWAYS CALLED !!!**/
	static {
		SetUpTransitionLabels();
		//RuleCode W = LettersTransOneToCode("abcgkhd");
		//Macro.print(" muster: " + W);
		
		
	}

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();


	private static void CheckTcode(String binPattern) {
		Macro.print( binPattern + " =:=> " + BinarytoTcode(binPattern) );
	}

	static boolean showPattern= true;

	static int OLENMIN=3, OLENMAX=7;

	private static void GenerateDiagOfOriginals(int size){
		DeBruijnDiagram exp = new DeBruijnDiagram(RuleCode.NULLRULE);

		ConfigSpace sp= new ConfigSpace(size);
		ArrayList<Integer> originals= sp.GetStrongRepresentativeList();
		for (Integer configNum : originals){
			String pattern = Convert.DecimaltoBinaryString(configNum, size);
			Macro.print(" original :" + pattern);
			exp.BuildOneDotFile(pattern);
		}
	}

	private void BuildOneDotFile(String pattern) {
		Macro.SeparationLine();
		ExportDotFile(pattern);
		Macro.SeparationLine();
	}

	public static void main(String[] argv) {

		Macro.BeginTest(CLASSNAME);

		RuleCode W= new RuleCode(142);
		DeBruijnDiagram exp = new DeBruijnDiagram(W);
		exp.BuildOneDotFile("001");


		/*//TcodeTest();
		/*SetUpTransitionLabels();
		for (int i=OLENMIN; i<= OLENMAX; i++){
			GenerateDiagOfOriginals(i);
		}*/
		//BuildDotFile();*/
		//FindMysteryRule("ckgabj");
		Macro.EndTest();
	}







}
