package cyclesMeasure.transitionGraph;

import components.allCells.OneRegisterIntCell;
import components.arrays.GenericCellArray;
import components.arrays.RegularDynArray;
import components.types.FLsignal;
import components.types.RuleCode;
import experiment.samplers.LinearSampler;
import explore.satSolve.SatGen;
import initializers.CAinit.BinaryInitializer;
import initializers.CAinit.BinaryInitializer.INITSTYLE;
import main.Convert;
import main.FiatLuxProperties;
import main.Macro;
import main.MathMacro;
import main.commands.CommandParser;
import models.CAmodels.CellularModel;
import models.CAmodels.tabled.ECAmodel;
import models.CAmodels.tabled.GeneralTabledBinaryModel;
import topology.basics.LinearTopology;
import topology.zoo.RadiusN_Topology;

/* *
 * analysis of the transition graph of (a)synchronous rules 
 * **/
public class AnalyseRuleRegularSynchUpdate {


	public static final String CODEPREFIX = "rule";

	/*---------------------------------------------------------------------------
	  - attributes
	  --------------------------------------------------------------------------*/

	final RuleCode m_Wcode; //rule code
	final int m_N; // number of cells

	LinearSampler m_samp;
	private BinaryInitializer m_init; // for init patterns 
	private GenericCellArray<OneRegisterIntCell> m_array;
	TransitionGraphRegularSynch m_graph; 


	int [] m_nextSt; // for decomposition decimal<-> binary


	/*---------------------------------------------------------------------------
	  - construction
	  --------------------------------------------------------------------------*/


	/** K : neighb size **/
	public AnalyseRuleRegularSynchUpdate(int K, RuleCode Wcode, int cellSize){
		m_N= cellSize;
		m_Wcode= Wcode;

		m_graph = new TransitionGraphRegularSynch(m_N);
		CellularModel model;
		int leftN, rightN;
		if (K==3){
			Macro.print("ECA case");
			model = new ECAmodel(Wcode);
			leftN= -1; rightN= 1;
		} else {
			int TableSize= MathMacro.Power(2, K);
			model = new GeneralTabledBinaryModel(TableSize, Wcode);
			leftN=-(K-1)/2; rightN= K/2;
		}

		LinearTopology 	topo = new RadiusN_Topology(leftN,rightN);
		//topo.SetSize(N);

		m_samp = new LinearSampler(m_N, model, topo);
		RegularDynArray sys= m_samp.GetAutomaton();
		m_array= new GenericCellArray<OneRegisterIntCell>(sys);

		m_init= (BinaryInitializer)m_samp.GetInitializerAsSuper();
		m_init.ForceInitStyle(INITSTYLE.Pattern);

		m_nextSt=new int[m_N]; // for the computation of the next state
		// main
		buildTransitionGraph();
		identifyCycles();
		basicStats();

	}

	/*---------------------------------------------------------------------------
	  - main part
	  --------------------------------------------------------------------------*/

	private void basicStats() {
		int countFP=0, countCyclic=0;
		for (int index=0; index< m_graph.m_size; index++){
			if (m_graph.getNode(index).hasTagFixedPoint()) {
				countFP++;
			} else if (m_graph.getNode(index).hasCyclicTag()) {
				countCyclic++;
			}
		}
		Macro.fPrint(" STATS: rule:%s size:%d  nFP:%d nCyclic:%d", m_Wcode,m_N, countFP,countCyclic);
	}

	/** cycles and transient states identification **/
	void identifyCycles() {
		for (int index=0; index< m_graph.m_size; index++){
			ConfigNodeA src= m_graph.getNode(index);
			// has this node been visited?
			if (src.m_tag==0) {
				if (src.m_succ==src) {
					src.markTagFixedPoint(); // special case for fixed points
				} else {
					markCyclicAndTransient(src);
				}
			} //if
		}// for
	}

	/** marks transient and cyclic states **/
	private void markCyclicAndTransient(ConfigNodeA src) {
		// mark as cyclic
		src.markTagCyclic();
		ConfigNodeA current= src.m_succ;
		while (current.hasNoTag()) {
			current.markTagCyclic();
			current= current.m_succ;
		}
		// new part : transients
		ConfigNodeA stopTransient=current;
		current=src;

		// mark transients
		while (current!=stopTransient) {
			current.markTagTransient();
			current= current.m_succ;
		}
		
	}

	/** main function **/
	private void buildTransitionGraph() {
		int SIZE= m_graph.m_size;
		Macro.print(" building transition graph of size :" + SIZE);
		// builds the transition graph
		for (int index=0; index< SIZE; index++){
			calculateNodeSuccessor(index);
		}
	}

	/** calculates next state and updates graph **/
	private void calculateNodeSuccessor(int index){
		ConfigNodeA src= m_graph.getNode(index);
		String pattern= TableStateToString(src.m_state);

		//Macro.Debug( "index: "+ index + " PATT:" + pattern);
		m_init.SetInitPattern(pattern);
		m_init.ReceiveSignal(FLsignal.init);
		//m_samp.print();
		//Macro.Debug("curr: " + MathMacro.ArrayToString(src.m_state));
		// one step beyond !
		m_samp.sig_NextStep();

		// next state index 
		for (int i=0; i < m_N; i++){
			m_nextSt[i]= m_array.GetCell(i).GetState();	
		}

		int isucc= TableStateToIndex(m_nextSt);//cast here
		//Macro.Debug("next: " + MathMacro.ArrayToString(m_nextSt) + " i: " + isucc);

		// add link in graph
		m_graph.AddOrientedLink(index, isucc);		
		ConfigNodeA succ= m_graph.getNode(isucc);
		// updates "activity array"
		src.ComputeActivityWithSuccessor(succ);

	}

	/*---------------------------------------------------------------------------
	  - coding
	  --------------------------------------------------------------------------*/

	static public String TableStateToString(int[] m_state) {
		return 	MathMacro.ArrayToStringNoFormatting(m_state);
	}

	/** from state to index **/
	static public int TableStateToIndex(int[] tabState) {
		return (int)MathMacro.InvBinaryTab2Decimal(tabState);
	}

	/** from state to index **/
	static public int StringToIndex(String pattern) {
		return Convert.BinaryStringtoDecimal(pattern);
	}


	public static int[] IndexToTableState(int N, int index) {
		return MathMacro.Decimal2BinaryTabInverted(index, N);
	}

	public static String IndexToStringConfig(int N, int index) {
		return MathMacro.Int2BinaryString(index, N);
	}

	/*---------------------------------------------------------------------------
	  -  
	  --------------------------------------------------------------------------*/



	/*---------------------------------------------------------------------------
	  - states
	  --------------------------------------------------------------------------*/

	protected void PrintGraph() {
		m_graph.printGraph();
	}


	/*---------------------------------------------------------------------------
	  - I/O
	  --------------------------------------------------------------------------*/

	public void ExportData(){
		Macro.print(3,"exporting dot file...");
		String filenameA= SatGen.CODEPREFIX + m_Wcode + "-N-" + m_N + ".dot";
		DotFile.exportAsDotFileRegularSynchUpdate(filenameA, m_N, true, m_graph);
	}



	/*---------------------------------------------------------------------------
	  - main & test
	  --------------------------------------------------------------------------*/

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();
	protected static final int NARG = 3;

	/** analysis of one dimension **/
	public static void main(String [] argv){
		FiatLuxProperties.SetVerboseLevel(3);
		CommandParser parser = new CommandParser(CLASSNAME, "", argv);
		/*if (argv.length==1){
			RuleCode Wcode= 
					RuleCode.FromString(argv[0]);
			AnalyseRule ana= new AnalyseRule(4, Wcode, 5);
			ana.ReadSynchPoints(Wcode);
			ana.ExportData(Wcode, 5);
		} else*/ 
		if (argv.length == NARG ){
			int K= parser.IParse("K=");
			RuleCode Wcode= 
					RuleCode.FromString(parser.SParse("W="));
			int N= parser.IParse("N=");
			AnalyseRuleRegularSynchUpdate ana= new AnalyseRuleRegularSynchUpdate(K, Wcode, N);
			ana.ExportData();
		} else {
			parser.Usage("K=neighbSize W=rule N=size");
		}
	}


}
