package cyclesMeasure.transitionGraph;

import java.util.ArrayList;

import cyclesMeasure.transitionGraph.ConfigNodeGlobalSynch.EQUIVALENCE;
import main.Macro;
import main.MathMacro;


/**** used for studying global synch problem **/
public class TransitionGraphGlobalSynch  {

	// equivalence by shift ??? (removing shift-equivalent nodes)
	private static final boolean COMPRESSBYSHIFT = false;

	// special label for homogeneous states
	static final boolean SPECIALLABELZEROONE = false;
	
	
	ConfigNodeGlobalSynch m_zero, m_one; //special nodes
	protected int m_size;   // number of nodes
	ConfigNodeGlobalSynch [] m_nodeArray; //array of nodes

	TransitionGraphGlobalSynch(int N) {
		m_size= MathMacro.TwoToPowerIntResult(N);
		m_nodeArray = new ConfigNodeGlobalSynch[m_size];

		InitClassical(N);
		m_zero = GetNode(0); 
		m_one= GetNode(m_size -1);
	}

	public void InitClassical(int N){
		for (int pos=0; pos < m_size ; pos++){
			int [] state= AnalyseRuleGlobalSynchProblem.IndexToTableState(N,pos);
			m_nodeArray[pos]= new ConfigNodeGlobalSynch(pos, state); 
		}
	}
	/*---------------------------------------------------------------------------
	  - get / set
	  --------------------------------------------------------------------------*/

	/** adds successors between zero and one 
	 * but not predecessors (for height calculus) **/
	public void SpecialCaseZeroOne(){
		if(SPECIALLABELZEROONE){
			m_zero.SetSpecialLabel("zero");
			m_one.SetSpecialLabel("one");	
		}
		m_zero.AddSuccessor(m_one);
		m_one.AddSuccessor(m_zero);
	}

	/*---------------------------------------------------------------------------
	  - analysis
	  --------------------------------------------------------------------------*/


	public void SetUpSourceAndLaunchRecursiveSearchOfEquivalence() {
		EQUIVALENCE m= EQUIVALENCE.SRC;
		m_zero.m_mark= m;
		m_one.m_mark= m;
		MarkAncestorsFromPredecessors(m_zero,m);
		MarkAncestorsFromPredecessors(m_one,m);
		if (COMPRESSBYSHIFT)
			CompressCyclesByShift();
	}

	private void MarkAncestorsFromPredecessors(ConfigNodeGlobalSynch node, EQUIVALENCE m) {
		for (ConfigNodeA pred : node.GetPredecessors()){
			MarkAncestors((ConfigNodeGlobalSynch)pred, m);  //cast here
		}		
	}


	/** finding cycles for sigma-equivalence **/
	private void CompressCyclesByShift() {
		for (int searchIndex=1; searchIndex < m_size -1; searchIndex++){
			ConfigNodeGlobalSynch scanNode= m_nodeArray[searchIndex];
			CycleIdentification(scanNode);
		}
	}

	/** marks with state "CYC" **/
	private void CycleIdentification(ConfigNodeGlobalSynch scanNode) {
		if (scanNode.IsNotMarked()){
			// forward marking of cycle
			ForwardMarking(scanNode);
			// backwards recursive marking
			MarkAncestorsFromPredecessors(scanNode, EQUIVALENCE.CYC);
		}
	}


	/** move forward and mark until loop 
	 * iterative algo (could be recursive) **/
	private void ForwardMarking(ConfigNodeGlobalSynch scanNode) {
		ConfigNodeGlobalSynch currentNode= scanNode;
		while (currentNode.m_mark != EQUIVALENCE.CYC){
			currentNode.m_mark= EQUIVALENCE.CYC;
			MarkAncestorsFromPredecessors(currentNode, EQUIVALENCE.CYC);
			currentNode= (ConfigNodeGlobalSynch)currentNode.GetSucc(); //cast here
		}
	}

	/** recursive **/
	private void MarkAncestors(ConfigNodeGlobalSynch node, EQUIVALENCE mark) {
		if (node.IsNotMarked()){
			node.m_mark= mark;
			CleanNode(node);
			// launch recursivity
			for (ConfigNodeA pred : node.GetPredecessors()){
				MarkAncestors((ConfigNodeGlobalSynch)pred, mark); //cast here
			}
		}

	}

	/** removing equivalent nodes */
	private void CleanNode(ConfigNodeGlobalSynch node) {
		int sz= node.GetSize();
		int [] shifted = new int [sz];
		//make rotations
		for (int sigma=1; sigma < sz ; sigma++){ // rotations
			for (int i=0; i < sz; i++){
				int newi= (i+sigma)%sz;
				shifted[i]= node.m_state[newi];
			}
			int iEquiv= AnalyseRuleGlobalSynchProblem.TableStateToIndex(shifted);
			/*Macro.fDebug(" sigma %d :: src %d -> out : %d st: %s => %s",
					sigma,node.GetIndex(), iEquiv, 
					MathMacro.ArrayToString(node.m_state),
					MathMacro.ArrayToString(shifted));*/
			if (GetNode(iEquiv).IsNotMarked()){
				GetNode(iEquiv).RuleOut();
			}
		}
	}


	/** OUT OF SYNCH PB ??**/
	/*public ArrayList<KlapklapConfigNode> zzzGetListofRepresentativesBis() {
		ArrayList<KlapklapConfigNode> lst= new ArrayList<KlapklapConfigNode>();
		for (KlapklapConfigNode node: m_nodeArray){
			if (node.m_mark == EQUIVALENCE.SRC){
				lst.add(node);
			}
		}
		return lst;
	}*/


	/*---------------------------------------------------------------------------
	  - OUTPUT
	  --------------------------------------------------------------------------*/



	public ArrayList<ConfigNodeGlobalSynch> GetNodeList() {
		ArrayList<ConfigNodeGlobalSynch> list = new ArrayList<ConfigNodeGlobalSynch>();
		for (ConfigNodeGlobalSynch node : m_nodeArray){
			if (!(COMPRESSBYSHIFT && (node.m_mark != EQUIVALENCE.OUT) )){
				list.add(node);
			}
		}
		return list;
	}

	public void AddOrientedLink(int pos, int successorPos) {
		m_nodeArray[pos].AddSuccessor(m_nodeArray[successorPos]);
		m_nodeArray[successorPos].AddPredecessor(m_nodeArray[pos]);
	}

	protected ConfigNodeGlobalSynch GetNode(int pos) {
		return m_nodeArray[pos];
	}

	public void PrintGraph() {
		for (int pos=0; pos < m_size ; pos++){
			String s = String.format(" pos: %d | %s  ", 
					pos, GetNode(pos).toString());
			Macro.print(s);
		}		
	}
	
	
	/** counts the number of cyclic states **/
	public int CountCyclicStates(){
		int nCyclic=0;
		for (ConfigNodeGlobalSynch node : m_nodeArray){
			if (node.m_mark==EQUIVALENCE.CYC){
				nCyclic++;
			}
		}
		return nCyclic;
	}

	/** example for testing **/
	public void BuildForTest() {		
		for (int pos=2; pos < m_size ; pos++){
			for (int candidate=1; candidate < pos ; candidate++){
				if (pos % candidate == 0){
					AddOrientedLink(pos,candidate);
				}
			}
		}		
	}


}
