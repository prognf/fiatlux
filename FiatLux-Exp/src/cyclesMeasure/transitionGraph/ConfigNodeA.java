package cyclesMeasure.transitionGraph;

import java.util.ArrayList;

import main.Macro;
import main.MathMacro;

public class ConfigNodeA {

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();

	
	final static int TRANSIENT=1, CYCLIC=2, FIXEDPOINT=3; //tags
	
	//--------------------------------------------------------------------------
	//- attributes
	//--------------------------------------------------------------------------

	final public int [] m_state;  // states
	
	protected int m_index;        // int equivalent
	
	private boolean[] m_activity; // which cells are active?

	protected ConfigNodeA m_succ;
	protected ArrayList<ConfigNodeA> m_pred;

	protected String m_label;
	protected int m_tag; // transient, cyclic, etc.
	
	//--------------------------------------------------------------------------
	//- construction
	//--------------------------------------------------------------------------

	public ConfigNodeA(int index, int[] state) {
		m_index= index;
		m_state= state;
		//Macro.fDebug(" setting %d %s", index, MathMacro.ArrayToString(m_state));
		m_pred= new ArrayList<ConfigNodeA>();
		m_succ= null;
		m_label=null;
		m_tag=0;
	}

	//--------------------------------------------------------------------------
	//- dynamis
	//--------------------------------------------------------------------------


	/** computes "activity array" 
	 * that is the fact that a cell is stable or not
	 * this is done here by comparison with a successor **/
	public void ComputeActivityWithSuccessor(ConfigNodeA successor) {
		int N= m_state.length;
		m_activity= new boolean[N];
		for (int i=0; i<N; i++){
			m_activity[i]= (m_state[i]!=successor.m_state[i]);
		}
	}


	public static int StateToIndex(int [] state) {
		return (int)MathMacro.InvBinaryTab2Decimal(state);
	}

	public void AddSuccessor(ConfigNodeA succ){
		if (m_succ==null){
			m_succ= succ;
		} else {
			Macro.FatalError(" synch. version : only one successor allowed");
		}
	}

	public void AddPredecessor(ConfigNodeA pred){
		m_pred.add(pred);
	}

	//--------------------------------------------------------------------------
	//- get/set
	//--------------------------------------------------------------------------


	public ArrayList<ConfigNodeA> GetPredecessors() {
		return m_pred;
	}

	public int GetSize() {
		return m_state.length;
	}
	public int[] GetState() {
		return m_state;
	}
	public int GetIndex() {
		return m_index;
	}

	/** one succ only **/
	public ConfigNodeA GetSucc() {
		return m_succ;
	}
	
	//--------------------------------------------------------------------------
	//- I/O
	//--------------------------------------------------------------------------

	protected String getLabel() {
		StringBuilder label= new StringBuilder();
		//int N= m_state.length;
        for (int aM_state : m_state) { // dot syntax
            String s = /*"{" +*/ "" + aM_state /*+ "\\n"*/;
            //s+=m_activity[i]?"+":"."; // problem with ZERO and ONE
            //s+=/*"}"*/;
            label.append(s);
			/*if (i < (N-1)){ // Dot file : separate records
				label += "|";
			}*/
        }
		//		label += " |( " + m_index + ")"; 
		return label.toString();
	}

	
	public String toString(){
		StringBuilder s= new StringBuilder();
		for (int st : m_state){
			s.insert(0, st);
		}
		String  out= String.format(" %s (%d) ", s.toString(), m_index);
		return out;
	}

	public void markTagCyclic() {
		m_tag= CYCLIC;
	}

	public void markTagTransient() {
		m_tag= TRANSIENT;
	}

	public void markTagFixedPoint() {
		m_tag= FIXEDPOINT;
	}

	
	public boolean hasNoTag() {
		return (m_tag==0);
	}

	public boolean hasCyclicTag() {
		return (m_tag==CYCLIC);
	}

	public boolean hasTagFixedPoint() {
		return (m_tag==FIXEDPOINT);
	}


}
