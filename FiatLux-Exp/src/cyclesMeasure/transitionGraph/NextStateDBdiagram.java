package cyclesMeasure.transitionGraph;

import components.types.RuleCode;
import main.Macro;
import main.MathMacro;
import models.CAmodels.tabled.LookUpTableBinary;

/** for the global synchronisation problem (NACO paper) **/
public class NextStateDBdiagram {


	final static int NBIT=3;
	final static int SZ = MathMacro.TwoToPowerIntResult(NBIT);


	DeBruijnNodeInfo [] m_diagram;
	LookUpTableBinary m_table;
	RuleCode m_W;

	public NextStateDBdiagram(RuleCode W){
		m_W= W;
		m_table = new LookUpTableBinary(SZ);
		m_table.SetRule(W);

		m_diagram= new DeBruijnNodeInfo[SZ];
		for (int index=0; index < SZ; index++){
			m_diagram[index]= new DeBruijnNodeInfo(NBIT,index);
		}
		for (int index=0; index < SZ; index++){
			int next0= (index%4)*2;
			m_diagram[index].m_succ0= m_diagram[next0];
			m_diagram[index].m_succ1= m_diagram[next0+1];
		}
	}


	private void ExportDotFile() {
		String stitle = "rule"+ m_W.toString();
		DotFile dotfile= new DotFile();
		dotfile.AddDotFileHeadA(stitle+ "-"+ m_W.toTcode());
		// nodes 
		for (DeBruijnNodeInfo node: m_diagram ){
			dotfile.AddNodeWithPos(node);
		}
		// edges
		for (int index=0; index < SZ; index++){
			DeBruijnNodeInfo node= m_diagram[index];
			int bit= m_table.getBitTable(index);
			dotfile.AddTwoEdgesDifferentiated(node, bit);
		}
		dotfile.AddTail();
		//dotfile.Print();
		dotfile.WriteToFile(stitle + ".dot");
	}

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();
	private static final int [] CODELIST = {1,9,19, 25};

	//---------------------------------------------------
	public static void main(String[] argv) {
		Macro.BeginTest(CLASSNAME);

		for (int wcode : CODELIST){
			RuleCode W= RuleCode.FromInt(wcode);
			NextStateDBdiagram exp = new NextStateDBdiagram(W);
			exp.ExportDotFile();
		}
		Macro.EndTest();
	}

}
