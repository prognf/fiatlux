package cyclesMeasure.transitionGraph;

public class ConfigNodeGlobalSynch extends ConfigNodeA {

	enum EQUIVALENCE { WHT, SRC, CYC, OUT} //for "quotienting" by the shift


	public int m_height; 

	EQUIVALENCE m_mark;

	ConfigNodeGlobalSynch(int index, int [] state){
		super(index, state);
	
		m_mark= EQUIVALENCE.WHT;
		m_height=-1; 

		m_label= null; // special zero and one
	}

	public void RuleOut() {
		m_mark= EQUIVALENCE.OUT;
	}
	
	public boolean IsNotMarked() {
		return m_mark==EQUIVALENCE.WHT;
	}

	/** specific with height **/
	public String toString() {

		String ssucc=m_succ.GetIndex() + " ";
		StringBuilder spred= new StringBuilder();
		for (ConfigNodeA pred : m_pred){
			spred.append(pred.GetIndex()).append(" ");
		}
		String out = String.format("code:%d height:%d   succ:[%s] pred:[%s]", 
				m_index, m_height, ssucc, spred.toString());
		return out;
	}

	public void SetSpecialLabel(String lbl){
		m_label = lbl;
	}

	public String GetLabel() {
		if ((m_label!=null) && (TransitionGraphGlobalSynch.SPECIALLABELZEROONE)){
			return m_label;
		} else {
			return super.getLabel();
		}
	}

	

}
