package cyclesMeasure.transitionGraph;

import initializers.CAinit.BinaryInitializer;
import initializers.CAinit.BinaryInitializer.INITSTYLE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import main.Macro;
import main.MathMacro;
import main.commands.CommandParser;
import models.CAmodels.CellularModel;
import models.CAmodels.tabled.ECAmodel;
import models.CAmodels.tabled.GeneralTabledBinaryModel;
import models.CAmodels.tabled.ITabledModel;
import topology.basics.LinearTopology;
import topology.zoo.RadiusN_Topology;

import components.types.FLStringList;
import components.types.FLsignal;
import components.types.RuleCode;

import experiment.samplers.LinearSampler;
import explore.satSolve.SatGen;

/* *
 * analysis of the transition graph of (a)synchronous rules 
 * **/
public class CycleGraphPoiesis {


	public static final String CODEPREFIX = "rule";
	
	// for node coloring
	final static int[] NODECOLSIZE= { 2, 3, 4, 5, 7, 11};
	final static String[] NODECOLOR ={ "orange", "blue", "green", 
		"brown", "red", "darkgreen" };	
	static final Map<Integer, String> MAPCOL = new HashMap<Integer, String>();

	private static final boolean PRIMESELECT = false;
	
		
	/*---------------------------------------------------------------------------
	  - attributes
	  --------------------------------------------------------------------------*/

	FLStringList m_initialConditionList;

	LinearSampler m_samp;
	private BinaryInitializer m_init;


	int [] m_nextSt; // for decomposition decimal<-> binary

	CellularModel m_model;
	LinearTopology 	m_topo;

	// collection of Config. spaces for minimal representatives
	private ConfigSpace[] m_configSpaceArray; 

	// vertices
	Set<String> m_vertices;

	// links
	ArrayList<Transition> m_graphTr = new ArrayList<Transition>();

	//inner class
	class Transition {
		String m_source, m_succ;
		Transition(String src, String succ){
			m_source= src; 
			m_succ= succ;
		}
	}

	/*---------------------------------------------------------------------------
	  - construction
	  --------------------------------------------------------------------------*/


	public CycleGraphPoiesis(int K, RuleCode Wcode, int lenmax){
		
		BuildMapCol();
		BuildInitConfig(lenmax);
		int leftN, rightN;
		if (K==3){
			Macro.print("ECA case");
			m_model = new ECAmodel(Wcode);
			leftN= -1; rightN= 1;
		} else {
			int TableSize= MathMacro.Power(2, K);
			m_model = new GeneralTabledBinaryModel(TableSize, Wcode);
			leftN=-(K-1)/2; rightN= K/2;
		}

		m_topo = new RadiusN_Topology(leftN,rightN);

		m_graphTr= new ArrayList<CycleGraphPoiesis.Transition>();
		m_vertices= new TreeSet<String>();

		Analysis();
	}

	/*---------------------------------------------------------------------------
	  - main part
	  --------------------------------------------------------------------------*/

	private void BuildMapCol() {
		for (int i=0; i<NODECOLSIZE.length; i++){
			MAPCOL.put(NODECOLSIZE[i], NODECOLOR[i]);
		}
	}

	private void BuildInitConfig(int maxlen) {
		m_initialConditionList= new FLStringList();

		// build spaces
		m_configSpaceArray= new ConfigSpace[maxlen+1];
		for (int i=1; i<= maxlen; i++){ // maxlen included
			ConfigSpace sp= new ConfigSpace(i);
			m_configSpaceArray[i]= sp;
		}
		// find the strong minimal rep. to add
		for (int len=1; len<= maxlen; len++){ // maxlen included
			// ONLY PRIME SIZES ?
			if ( (!PRIMESELECT) || MathMacro.isPrime(len) ){
				AddInitialConditionsOfSize(len);
			}
		}
	}

	private void AddInitialConditionsOfSize(int len) {
		ConfigSpace sp= m_configSpaceArray[len];
		ArrayList<Integer> minRepList= sp.GetStrongRepresentativeList();
		for (Integer index : minRepList){
			String pattern= AnalyseRuleGlobalSynchProblem.IndexToStringConfig(len, index);
			//Macro.Debug( "PATTERN :" + pattern);
			m_initialConditionList.add(pattern);
		}		
	}

	private void Analysis() {
		for (String pattern : m_initialConditionList){
			CalculateNodeSuccessor(pattern);
		}
	}



	/** calculates next state and updates graph **/
	private void CalculateNodeSuccessor(String pattern){
		Init(pattern);
		// one step beyond !
		m_samp.sig_NextStep();

		String next = m_samp.GetAutomaton().getGlobalStateAsString();

		// add link in graph
		//Macro.fDebug(" trans. %s -> %s ",pattern, next);
		m_vertices.add(pattern);
		m_graphTr.add( new Transition(pattern, next));
	}

	private void Init(String pattern){
		int N= pattern.length();
		m_samp= new LinearSampler(N, m_model, m_topo);
		m_init= (BinaryInitializer)m_samp.GetInitializer();
		m_init.ForceInitStyle(INITSTYLE.Pattern);

		m_nextSt= new int[N]; // for the computation of the next state

		//Macro.Debug( "INIT PATT:" + pattern);
		m_init.SetInitPattern(pattern);
		m_init.ReceiveSignal(FLsignal.init);
		m_samp.print();
	}

	/*---------------------------------------------------------------------------
	  - coding
	  --------------------------------------------------------------------------*/





	/*---------------------------------------------------------------------------
	  - states
	  --------------------------------------------------------------------------*/



	private String GetConfigCode(String config) {
		int len = config.length();
		ConfigSpace sp= m_configSpaceArray[len];
		int index= AnalyseRuleGlobalSynchProblem.StringToIndex(config);
		//Macro.Debug("len "+len);
		int minrep= sp.GetRepresentative(index);
		int per= sp.GetSpatialPeriod(index);
		String out= String.format(" %2d(%2d)[%2d]",index,minrep,per);

		if (per>0){
			String sub= config.substring(0, per);
			out += "==> " + sub;
		}
		return out;
	}

	/** calculates the class rep. of a node, that is, 
	 * the representative with spatial division when periodic  */ 
	private String GetClassRepresentative(String config){
		int len = config.length();
		ConfigSpace sp= m_configSpaceArray[len];
		int index= AnalyseRuleGlobalSynchProblem.StringToIndex(config);
		int per= sp.GetSpatialPeriod(index);
		String filter= per>0? // we extract sub-pattern if the config is periodic 
				config.substring(0, per):
					config;	
				return GetMinRep(filter);
	}

	private String GetMinRep(String config) {
		int len = config.length();
		ConfigSpace sp= m_configSpaceArray[len];
		int index= AnalyseRuleGlobalSynchProblem.StringToIndex(config);
		int minrep= sp.GetRepresentative(index);
		String sMinRep= AnalyseRuleGlobalSynchProblem.IndexToStringConfig(len, minrep);
		return sMinRep;
	}

	/*---------------------------------------------------------------------------
	  - I/O
	  --------------------------------------------------------------------------*/



	protected void Print() {
		for (Transition trans : m_graphTr){
			String infoA= GetConfigCode(trans.m_source);
			String infoB= GetConfigCode(trans.m_succ);

			Macro.fPrint(" trans. %s : %s -> %s : %s", 
					infoA, trans.m_source, trans.m_succ, infoB);
		}
	}

	public void ExportData(RuleCode Wcode){
		String filenameA= SatGen.CODEPREFIX + Wcode + ".dot";
		ExportAsDotFile(filenameA);
	}

	public void ExportAsDotFile(String filename){
		FLStringList dotfile = new FLStringList();
		dotfile.Add("digraph testG {");
		dotfile.Add("node [shape=box];");
		
		dotfile.Add("labelloc=\"t\";");
		RuleCode W= ((ITabledModel)m_model).GetRuleCode();
		dotfile.Add("label=\" W="+ W + "  \" ;");
		// vertices
		for (String node : m_vertices){
			int len= node.length(); 
			if (MAPCOL.containsKey(len)){
				String col= MAPCOL.get(len);
				String nodeline= String.format(" %s [color=\"%s\"]", node, col);
				//Macro.Debug(" nodeline" + nodeline);
				dotfile.Add(nodeline);
			}
		}
		
		// edges
		for (Transition trans : m_graphTr){
			String sourceNode= trans.m_source;
			String succClassNode= GetClassRepresentative(trans.m_succ);
			String line= String.format(" %s -> %s ; ", sourceNode, succClassNode);
			dotfile.Add(line);
		}

		dotfile.Add("}");//end of file
		dotfile.WriteToFile(filename);
	}


	
	
	/*---------------------------------------------------------------------------
	  - main & test
	  --------------------------------------------------------------------------*/

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();
	protected static final int NARG = 3;

	public static void main(String [] argv){
		CommandParser parser = new CommandParser(CLASSNAME, "", argv);
		if (argv.length == NARG ){
			int K= parser.IParse("K=");
			RuleCode Wcode= 
					RuleCode.FromString(parser.SParse("W="));
			int LMAX= parser.IParse("LMAX=");
			CycleGraphPoiesis ana= new CycleGraphPoiesis(K, Wcode, LMAX);
			ana.Print();
			ana.ExportData(Wcode);
		} else {
			parser.Usage("K=neighbSize W=rule N=size");
		}
	}


}
