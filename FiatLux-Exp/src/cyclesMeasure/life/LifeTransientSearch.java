package cyclesMeasure.life;

import components.types.FLsignal;
import components.types.IntC;
import experiment.measuring.general.DensityMD;
import experiment.samplers.CAsimulationSampler;
import initializers.CAinit.BinaryInitializer;
import initializers.CAinit.BinaryInitializer.INITSTYLE;
import main.Macro;
import main.commands.CommandParser;
import models.CAmodels.CellularModel;
import models.CAmodels.binary.LifeModel;
import topology.basics.SamplerInfoPlanar;
import topology.basics.TopologyCoder;

public class LifeTransientSearch {

	final static int L=10, TLIM=1000;
	private static final int PIXSIZE = 20;
	private static final int NSAMPLE = 20000;
	private static final int SEEDI = 40000;

	CAsimulationSampler m_samp;
	DensityMD m_densMD;

	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------


	private LifeTransientSearch() {
		String topoCode= TopologyCoder.s_TM8;
		CellularModel myModel = new LifeModel();
		SamplerInfoPlanar info2D= new SamplerInfoPlanar(topoCode, myModel);
		
		info2D.sizeXY= new IntC(L,L);
		info2D.cellPixSize= new IntC(PIXSIZE,1);
		m_samp= info2D.GetSimulationSampler();
		
		BinaryInitializer init= (BinaryInitializer)m_samp.GetInitializer();
		init.ForceInitStyle(INITSTYLE.InSquare);
		//m_samp.ShowInWindow();
		m_densMD= new DensityMD();
		m_densMD.LinkTo(m_samp);
	}

	private int MeasureOneTime(int seed) {
		BinaryInitializer initB= (BinaryInitializer)m_samp.GetInitializer();
		initB.SetSeed(seed);
		initB.ReceiveSignal(FLsignal.init);
		int t=0;
		do {
			m_samp.sig_NextStep();
			t++;
		} while ((!m_densMD.isAllZeroState()) && (t<TLIM));
		return(t==TLIM)?-1:t;
	}

	private void TryRandomSampling() {
		int tmax=1;
		StringBuilder out= new StringBuilder();
		for (int seed=SEEDI; seed < SEEDI + NSAMPLE; seed++){
			int t=MeasureOneTime(seed);
			if (t>tmax){
				tmax=t;
				out.append(String.format("[%d:%d]\n", seed, t));
			}
		}
		Macro.print(out.toString());
	}

	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------


	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();
	protected static final int NARG = 0;

	public static void main(String [] argv){
		CommandParser parser = new CommandParser(CLASSNAME, "", argv);
		if (argv.length == NARG ){
			//int M= parser.IParse("M=");
			LifeTransientSearch exp= new LifeTransientSearch();
			exp.TryRandomSampling();
		} else {
			parser.Usage("?");
		}
	}





}
