package kuklo;

import main.Convert;


abstract class StateGraphStringN extends StateGraphPatron {

    abstract String CalculateSuccessor(String rep, int posUpdate);
	
	int m_Nsize;

	/** launch ConstructGraph to build object **/
	public StateGraphStringN(int N) {
		m_Nsize= N;

	}

	//--------------------------------------------------------------------------
	//-construction
	//--------------------------------------------------------------------------


	@Override
	void ConstructSuccessors() {
		for (NodePatron node: m_graph){ // for each node

			NodeList successorList = calculateSuccList(node);
			node.initSuccessors(successorList);
		}

	}


	//--------------------------------------------------------------------------
	//- code / rep
	//--------------------------------------------------------------------------


	private NodeList calculateSuccList(NodePatron node) {
		NodeList successorList= new NodeList();

		String rep= node.getRepresentation();
		for (int posUpdate=0; posUpdate < m_Nsize; posUpdate++){
			// call to main function
			String repSucc= CalculateSuccessor(rep, posUpdate);
			if (repSucc != null){ // not itself (fixed point)
				int codeSucc= representationToCode(repSucc);
				NodePatron nodeSucc= super.getNodeByCode(codeSucc);
				successorList.add(nodeSucc);	
			}
		}// all pos
		return successorList;
	}

	

	/** representation -> code **/
	Integer representationToCode(String representation){
		return Convert.BinaryStringtoDecimal(representation);
	}

	/** code ->  representation **/
	String CodeToRepresentation(Integer code) {
		String binS= Convert.DecimaltoBinaryString(code, m_Nsize);
		return binS;
	}


}
