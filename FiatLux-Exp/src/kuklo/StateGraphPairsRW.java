package kuklo;

import main.Convert;
import main.MathMacro;
import main.commands.CommandParser;

public class StateGraphPairsRW extends StateGraphStringN {

	private static final String RULENAME = "pairRW";

	public StateGraphPairsRW(int stringSize) {
		super(stringSize);
	}

	@Override
	void ConstructNodes() {  // only even number of one's
		int nodeNum= MathMacro.Power(2, m_Nsize);
		for (int code=0; code < nodeNum; code++){
			String rep= CodeToRepresentation(code);
			int numOne= CountOnes(rep);
			if (numOne % 2==0){
				super.AddNode(code,rep);
			}
		}
	}
	
		
	String CalculateSuccessor(String nodeRep, int posUpdate) {
		char [] tab = nodeRep.toCharArray();
		String outSuccRep=null;
		int next= (posUpdate+m_Nsize+1)%m_Nsize;
		char ci= tab[posUpdate];
		char cn= tab[next];
		if (ci==cn){ // successor
			tab[posUpdate]= Convert.charFlip(ci);
			tab[next]= Convert.charFlip(cn);
			outSuccRep= new String(tab);
		}
		return outSuccRep;
	}

	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------
	
	private int CountOnes(String rep) {
		char [] ctab= rep.toCharArray();
		int count=0;
		for (char c : ctab){
			if (c=='1') count++;
		}
		return count;
	}
	
	//--------------------------------------------------------------------------
	//- EXP
	//--------------------------------------------------------------------------

	public static void main(String [] argv){
		CommandParser parser = new CommandParser(CLASSNAME, "", argv);
		int N= parser.IParse("N=");
		DoExp(N);
	}

	private static void DoExp(int n) {
		StateGraphPairsRW exp = new StateGraphPairsRW(n);
		exp.ConstructGraph();
		exp.FindCommunicationClasses();
		//exp.print();
		String rulename=RULENAME+"-N-"+n;
		exp.ProduceDotFile(rulename);
		// exp.FindCommunicationClasses();
	}

	@Override
	String FormatNodeExtraInfo(NodePatron node) {
		// TODO Auto-generated method stub
		return null;
	}

	
	
	
	
}
