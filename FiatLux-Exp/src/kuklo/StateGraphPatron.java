package kuklo;

import java.util.ArrayList;
import java.util.Hashtable;

import main.Macro;
import cyclesMeasure.transitionGraph.DotFile;

/*-------------------------------------------------------------------------------
- 
------------------------------------------------------------------------------*/
abstract public class StateGraphPatron {

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();
	private static final boolean ADDXORREPRESENTATION = true;


	/** nodes **/
	abstract void ConstructNodes();
	abstract String FormatNodeExtraInfo(NodePatron node); /* extra info with \n */
	
	/** successors **/
	abstract void ConstructSuccessors();

	//--------------------------------------------------------------------------
	//- attributes
	//--------------------------------------------------------------------------

	int countNode;
	ArrayList<NodePatron> m_graph;
	ArrayList<CommunicationClass> m_commclassL; // for communication class analysis


	Hashtable<Integer, NodePatron> m_tableCodeNode;
	//--------------------------------------------------------------------------
	//- construction
	//--------------------------------------------------------------------------

	public void ConstructGraph() {
		countNode=0;
		m_graph= new ArrayList<NodePatron>();
		m_tableCodeNode= new Hashtable<Integer, NodePatron>();
		ConstructNodes();
		ConstructSuccessors();
	}

	void AddNode(int code, String representation){
		NodePatron node= new NodePatron(countNode, code, representation);
		//Macro.Debug(" Adding " + node);
		m_graph.add(node);
		m_tableCodeNode.put(code, node);
		countNode++;
	}


	//--------------------------------------------------------------------------
	//- get / set
	//--------------------------------------------------------------------------

	public NodePatron getNodeByCode(Integer code) {
		return m_tableCodeNode.get(code);
	}


	protected int getNodeSize() {
		return m_graph.size();
	}

	//--------------------------------------------------------------------------
	//- comm. classes
	//--------------------------------------------------------------------------

	public void FindCommunicationClasses(){
		// comm. classes
		m_commclassL= new ArrayList<CommunicationClass>();
		int communicationClassTag=1;
		for (NodePatron node : m_graph){ // we look at all nodes
			if (node.m_tag==0){
				CommunicationClass commclass= new CommunicationClass(this, node, communicationClassTag);
				commclass.Print();
				m_commclassL.add(commclass);
				communicationClassTag++;
			}
		}
	}

	//--------------------------------------------------------------------------
	//- DOT files
	//--------------------------------------------------------------------------


	/** for fully asynch. graph analysis **/
	public void ProduceDotFile(String rulename) {
		DotFile dotfile= new DotFile();
		dotfile.AddDotFileHeadB(rulename);

		// node labels
		// nodeInfo
		for (NodePatron node : m_graph){
			String nodeline= node.getCode() + " [ label=\""
					+ node.getRepresentation() ;
			nodeline += FormatNodeExtraInfo(node);
			nodeline += "\"]";
			dotfile.Add(nodeline);
		}

		// graph
		for (NodePatron node : m_graph){
			ArrayList<NodePatron> succList = node.getSuccessors();
			String line= DotFile.OneLineOneNode(node,succList);
			dotfile.Add(line);
		}

		// node positions
		for (CommunicationClass commclass : m_commclassL){
			dotfile.WriteNodePositions(commclass);
		}

		//closing
		dotfile.AddTail();
		String filename = rulename + ".dot";
		dotfile.WriteToFile(filename);	
	}

	/*** MAIN FORMATTING method ***/
	private String FormatNode(NodePatron node) {
		String nodeline= node.getCode() + " [ label=\""
				+ node.getRepresentation() ;
		if (ADDXORREPRESENTATION){
			nodeline += "\\n" + GetXORrep(node.getRepresentation());
			//nodeline += "\\n" + EvenOddCount(node.getRepresentation());
		}
		nodeline += "\"]";
		return nodeline;
	}
	//--------------------------------------------------------------------------
	//- I / O 
	//--------------------------------------------------------------------------



	/** from binary rep to XOR rep. (not opitmal) **/
	private String GetXORrep(String binRep) {
		StringBuilder xorRep= new StringBuilder();
		int nsize=binRep.length();
		for  (int i=0; i< nsize; i++){
			int next= (i+1)% nsize;
			xorRep.append((binRep.charAt(i) == binRep.charAt(next)) ? '.' : "X");
		}
		return xorRep.toString();
	}
	
	
	private String EvenOddCount(String binRep){
		int neven=0, nodd=0;
		int nsize=binRep.length();
		char [] tab = binRep.toCharArray();
		for (int i=0; i< nsize;i++){
			int next= (i+1)% nsize;
			if (tab[i]!=tab[next]){
				if (i%2==0){
					neven++;
				} else {
					nodd++;
				}
			}
		}
		return "E"+neven+ " O"+nodd;
	}
	
	public void print(){
		for (NodePatron node : m_graph){
			StringBuilder lineout= new StringBuilder(node.getRepresentation() + "->");
			NodeList succL = node.getSuccessors();
			String snode= ""+node;
			//Macro.fDebug("FILTER : node %s list: %s", snode, succL);
			for (NodePatron succ : succL ){
				if (succ==null){
					Macro.FatalError(" Node with non initialized succ. list:" + node);
				}
				lineout.append(succ.getRepresentation()).append(":");
			}
			Macro.print(lineout.toString());
		}
	}

	/*private NodePatron getNodeByIndex(int nodeindex) {
		return m_graph.get(nodeindex);
	}*/



}
