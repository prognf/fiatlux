package kuklo;

import main.Convert;
import main.MathMacro;
import main.commands.CommandParser;
import models.CAmodels.tabled.LookUpTableBinary;
import components.types.RuleCode;

public class StateGraphMonadicECA extends StateGraphStringN {

	private static final String RULENAME = "monadic";
	private static final boolean ADDEXTRAINFO = true;


	private static final int OCTOPUS = 8;
	private static final char CHARZERO='0', CHARONE = '1';

	LookUpTableBinary m_LocalRuleTable ;


	public StateGraphMonadicECA(RuleCode W, int N) {
		super(N);
		m_LocalRuleTable= new LookUpTableBinary(OCTOPUS);
		m_LocalRuleTable.SetRule(W);

	}

	@Override
	void ConstructNodes() {  
		int nodeNum= MathMacro.Power(2, m_Nsize);
		for (int code=0; code < nodeNum; code++){
			String rep= CodeToRepresentation(code);
			super.AddNode(code,rep);
		}
	}


	String CalculateSuccessor(String nodeRep, int posUpdate) {
		char [] tab = nodeRep.toCharArray();
		int 
		x= cellState(tab,posUpdate,-1),
		y= cellState(tab,posUpdate, 0),
		z= cellState(tab,posUpdate, +1);
		int t = 4* x + 2* y + z;
		int T= m_LocalRuleTable.getBitTable(t);
		//Macro.fDebug("%d %d %d -> %d", x,y,z,T);
		tab[posUpdate]= Convert.BinvalToChar(T);
		if (T==y){
			return null; // convention for fixed points
		} else {
			return Convert.CharArrayToString(tab);	
		}
		
	}

	private int cellState(char[] tab, int posUpdate, int delta) {
		int index= (posUpdate + delta + m_Nsize) % m_Nsize;
		return Convert.CharToBinVal( tab[index] );
	}

	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------

	@Override
	String FormatNodeExtraInfo(NodePatron node) {
		String info="";
			if (ADDEXTRAINFO){
				String binRep= node.getRepresentation();
				//info += "\\n" + GetXORrep(binRep);
				info += "\\n" + EvenOddCount(binRep);
				info += "\\n" + conservedQ(binRep);
			}
			
			return info;
	}
	
	private String conservedQ(String binRep){
		int count=0;
		int nsize=binRep.length();
		char [] tab = binRep.toCharArray();
		for (int i=0; i< nsize;i++){
			int next= (i+1)% nsize;
			if (tab[i]==CHARZERO){
				if (tab[next]==CHARZERO){
				count++;
				}
			}
		}
		return "Q"+(count);
	}
	
	private String EvenOddCount(String binRep){
		int neven=0, nodd=0;
		int nsize=binRep.length();
		char [] tab = binRep.toCharArray();
		for (int i=0; i< nsize;i++){
			int next= (i+1)% nsize;
			if (tab[i]!=tab[next]){
				if (i%2==0){
					neven++;
				} else {
					nodd++;
				}
			}
		}
		return "E"+neven+ " O"+nodd;
	}

	//--------------------------------------------------------------------------
	//- EXP
	//--------------------------------------------------------------------------


	public static void main(String [] argv){
		CommandParser parser = new CommandParser(CLASSNAME, "", argv);
		int Wint= parser.IParse("W=");
		RuleCode W = new RuleCode(Wint);
		int size= parser.IParse("N=");
		DoExp(W, size);
	}

	private static void DoExp(RuleCode W, int N) {
		StateGraphMonadicECA exp = new StateGraphMonadicECA(W, N);
		exp.ConstructGraph();
		exp.print();
		
		exp.FindCommunicationClasses();
		String rulename=RULENAME+"-W-"+W+"-N-"+N;
		exp.ProduceDotFile(rulename);

	}

	

}// end class
