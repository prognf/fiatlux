package kuklo;

import java.util.ArrayList;

import main.Macro;

public class CommunicationClass {

	public int m_tag;
	public ArrayList<NodeList>  m_level;
	
	private StateGraphPatron m_graph; 
	NodePatron m_source;
	
	
	
	public CommunicationClass(StateGraphPatron graph, NodePatron source, int tag){
		m_graph= graph;
		m_source= source;
		m_tag= tag;
		Build();
	}

	private void Build() {
		m_level= new ArrayList<NodeList>();
		NodeList listInit= new NodeList();
		AddTagNodeToList(m_source,listInit);
		m_level.add(listInit);
		
		boolean hasSuccessor= IterateLevel();
		while (hasSuccessor){
			hasSuccessor= IterateLevel();
		}
	}

	/** if node not seen (untagged) then tag it and add it to list **/
	private void AddTagNodeToList(NodePatron node, NodeList listInit) {
		if (node.m_tag ==0){ // before not seen 
			node.m_tag= this.m_tag;
			listInit.add(node);
		}
	}

	private boolean IterateLevel() {
		NodeList nextLevel= new NodeList();
		NodeList currentLevel= m_level.get(m_level.size() - 1);
		//Macro.Debug("Processing current list :" + currentLevel);
		for (NodePatron currentnode : currentLevel){
			
			NodeList succOfCurrentL= currentnode.getSuccessors();
			//Macro.Debug("found successors list :" + succOfCurrentL);
			for (NodePatron succ : succOfCurrentL){
				AddTagNodeToList(succ, nextLevel);
			}
			
		}
		boolean hasSuccessors= (nextLevel.size()>0);
		if (hasSuccessors){
			m_level.add(nextLevel);
		}
		return hasSuccessors; // has other successors
	}
	
	public void Print(){
		StringBuilder out= new StringBuilder("CC:" + m_tag + " ");
		int level=0;
		for (NodeList list : m_level){
			out.append(String.format(" %d >> %S", level, list));
			level++;
		}
		Macro.print(out.toString());
	}
	
	
}
