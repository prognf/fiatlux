package kuklo;

import java.util.ArrayList;

import main.Macro;

public class NodeList extends ArrayList<NodePatron> {

	public String toString(){
		StringBuilder out= new StringBuilder();
		for (NodePatron node : this){
			if (node==null){
				out.append("NULL!!!");
			} else{
				out.append(node.getRepresentation()).append(":");
			}
		}
		return out.toString();
	}

	@Override
	public boolean add(NodePatron node){
		if (node==null){
			Macro.SystemWarning(" Adding null Node !!");
			return false;
		} else{
			return super.add(node);
		}
	}
	
	
}
