package kuklo;


public class NodePatron {

	int m_index;
	Integer m_code;
	String m_representation;
	NodeList m_successorL;

	int m_tag; // for data processing (e.g. communication classes)
	
	public NodePatron(int index, int code, String representation) {
		m_index= index;
		m_code= code;
		m_representation= representation;
		m_successorL= new NodeList();
	}

	public Integer getCode() {
		return m_code;
	}
	
	public String getRepresentation() {
		return m_representation;
	}
	
	public NodeList getSuccessors() {
		return m_successorL;
	}

	public void initSuccessors(NodeList successorList) {
		m_successorL= successorList;
	}

	public boolean isCodeEqual(int codeSucc) {
		return m_code == codeSucc;
	}

	public String toString(){
		return String.format("%d:[%s, %d]",m_index, m_representation, m_code );
	}
	

}
