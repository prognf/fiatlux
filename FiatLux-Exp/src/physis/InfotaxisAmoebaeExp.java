package physis;

import experiment.measuring.amoebae.EmissionProbabilityMD;
import grafix.gfxTypes.imgFormat.ImgFormat;

import java.util.ArrayList;
import java.util.Locale;

import main.Macro;
import main.commands.CommandParser;
import models.MASmodels.amoebae.AmoebaeInfotaxisSampler;
import models.MASmodels.amoebae.AmoebaeInfotaxisSystem;

import components.types.FLString;
import components.types.FLStringList;
import components.types.IntC;
import components.types.IntegerList;

public class InfotaxisAmoebaeExp {

	private static final IntC PixSize1 = new IntC(30,0);
	private static final int MAX_T = 100000;

	AmoebaeInfotaxisSystem m_system;
	AmoebaeInfotaxisSampler m_sampler;

	InfotaxisAmoebaeExp(InfotaxisExpParam p){
		IntC gridSize= new IntC(p.L, p.L);
		m_system= new AmoebaeInfotaxisSystem(p.expType, gridSize, p.N, p.K, p.pA, p.pR, p.I);
		m_sampler= new AmoebaeInfotaxisSampler(m_system, PixSize1);
	}

	private void DoSampling(String filename, int Z) {
		FLStringList res= FLStringList.OpenAppendMode(filename);
		for (int i=0; i<Z; i++){
			Macro.print("Expérience : " + i + "/" + Z);
			String sample= DoOneExp();
			res.add(sample);
			if ((i>20) && (i%20==0)){
				res.WriteToFile(filename);
			}
		}
		res.WriteToFile(filename);
	}


	String DoOneExp(){
		m_sampler.sig_Init();
		Macro.fPrintNOCR(":i:"+m_system.GetSeed()+":");
		boolean detect= m_system.HasDetectionOccured();
		int deltaT=10, count=deltaT;
		while (!detect){
			count--;
			if (count==0){
				Macro.fPrintNOCR(":t:"+m_system.GetTime()+":");
				deltaT*=2;
				count=deltaT;
			}
			m_sampler.sig_NextStep();
			detect= m_system.HasDetectionOccured();
		}
		int timeDetection= m_system.GetTime();
		int seed= m_system.GetSeed();
		double bbr= m_system.GetBoudingBoxRatio();
		
		String result= String.format("%6d %10d %f", timeDetection, seed, bbr);
		Macro.print(" exp time: "+ timeDetection + " seed: " + seed);
		return result;
	}

	int [] TDIAGI= {0, 10, 15, 20, 30, 35, 100, 150};

	IntegerList TDIAG= new IntegerList(TDIAGI);

	private static final int SEED1 = 22;

	private void DoSpaceTimeDiagrams(InfotaxisExpParam param) {
		m_system.SetSeed(SEED1);
		m_sampler.sig_Init();
		m_sampler.io_SetRecordFormat(ImgFormat.SVG);
		for (int t=0; t<=TDIAG.GetLast(); t++){
			if (TDIAG.Contains(t)){
				Kloup(param);	
			}
			m_sampler.sig_NextStep();
		}
	}


	private void DoFilm(InfotaxisExpParam param) {
		m_system.SetSeed(SEED1);
		m_sampler.sig_Init();
		m_sampler.io_SetRecordFormat(ImgFormat.PNG);
		for (int t=0; t<=TDIAG.GetLast(); t++){
			Kloup(param);	
			//Macro.Debug("NEXT");
			m_sampler.sig_NextStep();
		}
	}
	
	/**
	 * Measures the emission probability at everystep and writes it to file
	 * @param name of the file where the data are written
	 */
	private void DoPEmission(String filename){
		//Model initialization
		//m_system.SetSeed(1820918220);
		m_sampler.sig_Init();
		
		//Measuring device init
		EmissionProbabilityMD measuringDevice = new EmissionProbabilityMD();
		measuringDevice.LinkTo(m_sampler);
		
		//Opening the file and writing the seed in it
		FLStringList res = FLStringList.OpenAppendMode(filename);
		res.add(String.format("%d",m_system.GetSeed()));
		
		boolean detect = m_system.HasDetectionOccured();
		double pEmission = 0;
		int time = 0;
		
		Macro.print(3, "Starting the PEmission experiment with seed : " + m_system.GetSeed());
		
		//While we have not detected the source, write the emission probability at every step
		while(!detect){
			pEmission = measuringDevice.GetMeasure();
			time = m_sampler.GetTime();
			res.add(String.format(Locale.US, "%5d %2.2f", time, pEmission));
			
			m_sampler.sig_NextStep();
			detect = m_system.HasDetectionOccured();
		}
		
		res.WriteToFile(filename);
	}
	
	/**
	 * Measures the mean of emission probability at everystep
	 * @param filename path of the file where the results are written
	 * @param Z number of experiments the mean is based on
	 */
	private void DoMeanPEmission(String filename, int Z, int max_t){
		//m_system.SetSeed(1820918220);
		m_sampler.sig_Init();
		
		EmissionProbabilityMD md = new EmissionProbabilityMD();
		md.LinkTo(m_sampler);
		
		int STANDARD_TIME = 1343;
		int DEVIATION = 40;
		
		boolean detect = m_system.HasDetectionOccured();
		double pEmission = 0;
		int time = 0;
		int effective_max_t = 0;
		int nb_z = 0;
		
		ArrayList<Double> results = new ArrayList<Double>(max_t);
		
		for(int i = 0; i < max_t; i++){
			results.add(0.0);
		}
		
		for(int i = 0; i < Z; i++){
			Macro.print(String.format("Experiment %d/%d", i, Z));
			
			ArrayList<Double> result_for_Z = new ArrayList<Double>(max_t);
			
			for(int j = 0; j < max_t; j++){
				result_for_Z.add(0.0);
			}
			
			while(!detect){
				pEmission = md.GetMeasure();
				time = m_sampler.GetTime();
							
				if(time < max_t){
					result_for_Z.set(time,  pEmission);
				}
				else{
					break;
				}
			
				m_sampler.sig_NextStep();
				detect = m_system.HasDetectionOccured();
			}
			
			if(Math.abs(time - STANDARD_TIME) < DEVIATION){
				if(time > effective_max_t){
					effective_max_t = time;
				}
				
				for(int j = 0; j < max_t; j++){
					results.set(j, results.get(j) + result_for_Z.get(j));
				}
				
				nb_z++;
			}
			
			detect = false;
			//m_system.SetSeed(1820918220);
			m_sampler.sig_Init();
		}
		
		FLStringList f = FLStringList.OpenAppendMode(filename);
		
		for(int i = 0; i < effective_max_t; i++){
			results.set(i, results.get(i) / nb_z);
			f.add(String.format(Locale.US, "%5d %2.2f", i, results.get(i)));
		}
		
		f.WriteToFile(filename);
	}

	/** save image **/
	private void Kloup(InfotaxisExpParam param) {
		String stime=FLString.FormatIntWithZeros(m_sampler.GetTime(),3); 
		String filenameO= param.GetFilename() +"-t-" + stime;
		String filename= filenameO.replace('.', 'p');
		m_sampler.io_SaveImage(filename, m_sampler.GetViewer());
	}


	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();

	public static void main(String[] argv) {
		Macro.BeginTest(CLASSNAME);
		CommandParser parser= new CommandParser(CLASSNAME, argv);
		InfotaxisExpParam param= new InfotaxisExpParam();
		param.expType= parser.CParse("EXP=");
		param.L= parser.IParse("L=");
		param.N= parser.IParse("N=");
		param.I= parser.IParse("I=");
		param.K= parser.DParse("K=");
		param.pA= parser.DParse("pA=");
		param.pR= parser.DParse("pR=");

		InfotaxisAmoebaeExp exp= new InfotaxisAmoebaeExp(param);
		
		if (param.expType=='S'){
			exp.DoSpaceTimeDiagrams(param);
		} else if (param.expType=='F'){
			exp.DoFilm(param);
		} else if (param.expType=='P'){
			param.L = 50;
			param.I = 0;
			param.K = 0.5;
			param.pR = 0.9;
			
			for(int i = 10; i < 160; i += 10){
				param.N = i;
				for(double j = 0.05; j < 0.5; j += 0.05){
					param.pA = j;
					
					exp= new InfotaxisAmoebaeExp(param);
					String filename = param.GetFilename() + ".dat";
					exp.DoSampling(filename, 100);
				}
			}
		} else if (param.expType == 'D') {
			// Measure the probability density at every step and puts the result in a dat file
			String filename = param.GetFilename() + ".dat";
			
			exp.DoPEmission(filename);
		} else if (param.expType == 'M') {
			String filename = param.GetFilename() + ".dat";
			int Z = parser.IParse("Z=");
			
			exp.DoMeanPEmission(filename, Z, MAX_T);
		
		} else {
			int Z=parser.IParse("Z=");	exp.DoOneExp();
			String filename= param.GetFilename() + ".dat";
			exp.DoSampling(filename, Z);
		}
		//
		Macro.EndTest();
	}
}

