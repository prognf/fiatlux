package physis;

import grafix.gfxTypes.imgFormat.ImgFormat;
import initializers.CAinit.BinaryInitializer;
import main.Macro;
import main.commands.CommandParser;
import models.CAmodels.CellularModel;
import models.CAmodels.binary.LifeModel;
import models.CAmodels.tabled.ECAmodel;
import topology.basics.SamplerInfoLinear;
import topology.basics.SamplerInfoPlanar;
import topology.basics.PlanarTopology;
import topology.basics.TopologyCoder;
import topology.zoo.Moore8TM;
import updatingScheme.AlphaScheme;

import components.types.FLString;
import components.types.IntC;
import components.types.RuleCode;

import experiment.samplers.CAsampler;
import experiment.samplers.CAsimulationSampler;

public class ToulouseTalkExp {

	// general
	private static final int PIXSIZE = 20;

	// randomness
	private static final int SEEDINI1 = 2806, SEEDSYS=1984, SEEDUPDATE=1984;

	private static final int M = 4;


	// attributes
	CAsampler m_sampler;

	// exp attributes
	final private int m_L;
	private double m_alpha;
	private double m_initdens;

	ToulouseTalkExp(int expNum, int L){
		m_L= L;
		CellularModel model;
		IntC pixCellSize= new IntC(PIXSIZE, 0);
		switch (expNum){
		case 1:
			// preparation of the system
			IntC XYsize=new IntC(L,L);
			PlanarTopology topo = new Moore8TM();
			model= new LifeModel();
			
			String topoCodeA= TopologyCoder.s_TM8;
			SamplerInfoPlanar info2D = 
					new SamplerInfoPlanar(XYsize, pixCellSize, model, topoCodeA);
			CAsimulationSampler sampA=	
					new CAsimulationSampler(info2D );
			//(XYsize, model, topo);
			//sampA.AddDefaultViewer(pixCellSize);
			m_sampler= sampA;
			break;
		case 2:
			RuleCode W=new RuleCode(50);
			int T= M*L;
			CellularModel 	rule = new ECAmodel(W); 
			String 	topoCodeB = TopologyCoder.s_TLR1;

			SamplerInfoLinear info = 
					new SamplerInfoLinear(L,T, pixCellSize, rule, topoCodeB);
			CAsimulationSampler sampB= new CAsimulationSampler(info);
			sampB.sig_Init();
			m_sampler= sampB;
			break;
		}
		
	}



	/** produces n+1 images starting from t=0 with interval "steps" **/ 
	private void SnapExp(String label, int n, int steps){
		Save(label);
		for (int i=1; i<=n;i++){
			m_sampler.NstepsAndUpdate(steps);
			Save(label);
		}
	}


	/*
	 * init / get / set
	 * 
	 */

	private String Lalpha() {
		return "L" + m_L + "-a" + FLString.ReplaceDotByLetterP(m_alpha);
	}

	private String GetExpConditions() {
		String info=
				String.format("# L=%d seed-ini=%d  seed-model=%d", 
						m_L, m_sampler.GetInitSeed(), m_sampler.GetModelSeed());
		info += " alpha=" +	m_alpha;
		return info;
	}


	private void InitConditions(double initdens, double alpha, 
			int seedini, int seedmodel, int seedupdate) {
		InitDensAlpha(initdens,alpha);
		m_sampler.SetSeeds(seedini, seedmodel, seedupdate);
		m_sampler.io_SetRecordFormat(ImgFormat.SVG);
		m_sampler.sig_Init();
		//m_sampler.ShowInWindow();
		//Macro.Break();
	}

	private void InitDensAlpha(double initdens, double alpha) {
		Macro.fPrint("exp conditions: ini-dens %.3f  alpha:%.3f",initdens, alpha);
		m_initdens = initdens;
		m_alpha = alpha;
		BinaryInitializer init = (BinaryInitializer)m_sampler.GetInitializer(); 
		init.SetInitRate(m_initdens);
		AlphaScheme scheme= (AlphaScheme)m_sampler.GetUpdatingScheme();
		scheme.SetSynchronyRate(m_alpha);
	}

	private String GetFileName(String snapLabel) {
		return String.format("snap-%s-%s-t%d",snapLabel, Lalpha(), m_sampler.GetTime());
	}

	private void Save(String snapLabel) {
		m_sampler.io_SaveImage(GetFileName(snapLabel),m_sampler.GetAutomatonViewer());
	}


	
	
	/** EXP 2 **/
	private void SnapECA50(){
		
		InitConditions(.5, .62, SEEDINI1, SEEDSYS, SEEDUPDATE);
		int steps= m_sampler.GetSize()*M; //TODO : change this
		for (int t=0; t<steps; t++){
			m_sampler.sig_NextStep();
			m_sampler.sig_Update();
		}
		m_sampler.sig_Update();
		Save("eca50");
	}
	
	/** EXP 1 **/
	private void SnapLife() {
		InitConditions(.5, 1, SEEDINI1, SEEDSYS, SEEDUPDATE);
		SnapExp("Life", 5, 100);
		InitConditions(.5, .95, SEEDINI1, SEEDSYS, SEEDUPDATE);
		SnapExp("Life", 5, 50);
		InitConditions(.5, .5, SEEDINI1, SEEDSYS, SEEDUPDATE);
		SnapExp("Life", 5, 50);
	}

	/**
	 * run
	 */
	void DoExp(int expNum){		
		switch (expNum){
		case 1:
			SnapLife(); break;
		case 2:
			SnapECA50(); break;
		default:
			Macro.FatalError("exp not found.");
		}
	}

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();

	public static void main(String[] argv) {
		Macro.BeginTest(CLASSNAME);

		CommandParser parser= new CommandParser(CLASSNAME, argv);
		int expNum= parser.IParse("EXP=");
		int L=parser.IParse("L=");
		ToulouseTalkExp exp = new ToulouseTalkExp(expNum, L);
		exp.DoExp(expNum);

		Macro.EndTest();
	}

}

