package physis;

import main.Macro;
import main.commands.CommandParser;
import architecture.dynamicalSystems.FragmentationSystem;

import components.types.FLStringList;

public class FragmentationExp {


	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------
	
	
	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------
	
	
	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------
	
	
	private static void Histo(int L, int nSample){
		FragmentationSystem sys = new FragmentationSystem(L);
		sys.m_Strobo=1;
		FLStringList distribution = new FLStringList();
		for (int i=0; i< nSample; i++){
			Macro.ProgressBar(i, nSample);
			int val= sys.OneExp();
			Macro.fPrint(" read : %d" ,val);
			distribution.Add(""+ val);
		}
		distribution.WriteToFile("koko-L"+L+".dat");
	}
	
	
	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();

	public static void main(String[] argv) {
		Macro.BeginTest(CLASSNAME);
		CommandParser parser= new CommandParser(CLASSNAME, argv);
		int L= parser.IParse("L=");
		int NS= parser.IParse("NS=");
		//OneExp();
		Histo(L,NS);

		Macro.EndTest();
	}

	
}
