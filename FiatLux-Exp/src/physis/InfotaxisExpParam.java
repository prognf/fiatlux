package physis;

import java.util.Locale;

class InfotaxisExpParam {
		int L, N, I;
		double K, pA, pR;
		public char expType;
		
		public String GetFilename() {
			String filename= "infotaxis";
			filename += "-" + expType ;
			filename += "-L-" + L;
			filename += "-N-" + N;
			filename += "-K-" + String.format(Locale.US, "%.2f", K);
			filename += "-pA-" + String.format(Locale.US, "%.2f",pA);
			filename += "-pR-" + String.format(Locale.US, "%.2f",pR);
			filename += "-I-" + I;
		//	filename += ".dat";
			return filename;
		}
		
	}