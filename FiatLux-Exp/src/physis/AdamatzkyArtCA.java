package physis;

import main.Macro;
import models.CAmodels.CellularModel;
import models.CAmodels.tabled.ECAmodel;
import topology.basics.SamplerInfoLinear;
import topology.basics.TopologyCoder;
import updatingScheme.AlphaScheme;

import components.randomNumbers.FLRandomGenerator;
import components.types.IntC;

import experiment.samplers.CAsimulationSampler;

public class AdamatzkyArtCA {

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();

	

	private static final int N = 4;
	private static final int T = 5;
	private static final double ALPHA = 0.9;

	private static final IntC CellPixSize = new IntC(1,0);
	private static final int W = 51;//148;
	
	private static void GenSpaceTimeOne() {
		
		CellularModel 	rule = ECAmodel.NewRule(W);
		String 	topoCode = TopologyCoder.s_TLR1;

		SamplerInfoLinear info = 
				new SamplerInfoLinear(N,T, CellPixSize, rule, topoCode);
		//Macro.Debug(" code :" + info.topoCode);
		CAsimulationSampler sampler= new CAsimulationSampler(info);
	
		//sampler.SetFullyAsynchronousUpdatingScheme();
		AlphaScheme US= (AlphaScheme) sampler.GetUpdatingScheme();
		US.SetSynchronyRate(ALPHA);

		sampler.sig_Init();
		sampler.NstepsWithNupdates(T-1);
		//String filename= "ECA-" + W;
		sampler.io_SaveImageSVGhomemade(true);	
		
	}

	
	public static void main(String[] argv) {
		Macro.BeginTest(CLASSNAME);
		FLRandomGenerator.SetDefaultSeed(1596);
		GenSpaceTimeOne();

		Macro.EndTest();
	}
	
}
