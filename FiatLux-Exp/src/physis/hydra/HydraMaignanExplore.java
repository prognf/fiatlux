package physis.hydra;

import main.Macro;
import models.CAmodels.CellularModel;
import models.CAmodels.autotile.HydraInitializer;
import models.CAmodels.autotile.PrecipitateModel;
import topology.basics.SamplerInfoPlanar;
import topology.basics.TopologyCoder;

import components.types.IntC;

import experiment.samplers.CAsimulationSampler;
import grafix.gfxTypes.imgFormat.ImgFormat;

public class HydraMaignanExplore {

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();

	private static final int L=16, TSAVE = 6;

	private static final IntC CellPixSize = new IntC(10,0), GridSize=new IntC(L,L);

	
	CAsimulationSampler m_sampler;
	HydraInitializer m_init;
	
	HydraMaignanExplore() {
		CellularModel 	rule = new PrecipitateModel();
		String 	topoCode = TopologyCoder.s_TV4;

		SamplerInfoPlanar info = 
				new SamplerInfoPlanar(GridSize, CellPixSize, rule, topoCode);
		m_sampler= new CAsimulationSampler(info);
		
		m_init= (HydraInitializer)m_sampler.GetInitializer();
		m_sampler.io_SetRecordFormat(ImgFormat.SVG);
		
		//sampler.io_SaveImageSVGhomemade(true);	
	}

	
	public static void main(String[] argv) {
		Macro.BeginTest(CLASSNAME);
		HydraMaignanExplore exp= new HydraMaignanExplore();
		exp.DoExplorationSys();
		Macro.EndTest();
	}


	private void DoExplorationSys() {
		for (int x=1; x<=4;x++){
			for (int y=0; y<=x; y++){
				DoExploration(x, y);
			}
		}
		
	}


	private void DoExploration(int x,int y) {
		String suffix=String.format("x-%d-y-%d", x, y);
		m_sampler.io_SetRecordSuffix(suffix);
		String initcommand= String.format("I=1 X=%d Y=%d", x, y);
		m_init.SetInitCond(initcommand);
		m_sampler.sig_Init();
		m_sampler.io_SaveImage();
		for (int t=0; t<TSAVE; t++){
			m_sampler.sig_NextStep();
			m_sampler.io_SaveImage();
		}
	}
	
}
