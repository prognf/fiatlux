package physis;

import grafix.gfxTypes.imgFormat.ImgFormat;

import java.util.Date;

import architecture.latticeKinesis.LatticeKinesisSampler;
import architecture.latticeKinesis.LatticeKinesisSystem;
import architecture.latticeKinesis.SwarmingLGCAsys;
import architecture.latticeKinesis.SwarmingLmigInit;
import architecture.latticeKinesis.measuringD.LatticeKalignmentMD;
import main.Macro;
import main.commands.CommandParser;
import components.types.FLString;
import components.types.FLStringList;
import components.types.IntC;
import components.types.IntegerList;

public class SwarmingExp {

	// parameters
	private static final double INITDENSLO = 0.2, INITDENSHI=0.4, INITDENSONEHALF=.5	;
	private static final double SIGMALO = 2.0, SIGMAHI=4.0;
	private static final double SIGMARECT1 = 1.5, SIGMARECT2=1.8, SIGMARECT3=2.;

	private static final String 
	LABELONE="strp", LABELTWO = "chkbd", LABELTHREE="clstr",
	LABELRECT="rect", LABELRECTNH="rectNH",
	LABELREFLECT="rflct";


	// sample time for time evol experiments
	private static final int TIMEONE = 2000, TIMETWO=5000, TIMETHREE=8000; 
	private static final String 
	FILENAME_EXP1= "gamma-time-strp.dat", 
	FILENAME_EXP2= "gamma-time-chkbd.dat",
	FILENAME_EXP3= "gamma-time-clstr.dat";


	//histograms
	private static final double GAMMALIMIT1= 0.8, GAMMALIMIT2=0.5, GAMMALIMIT3=0.8;
	private static final int TIMELIMIT1 = 5000, TIMELIMIT2=2000, TIMELIMIT3=5000;

	// general
	private static final int PIXSIZE = 20;
	private static final int BORDER = 0;
	private static final String ROSETABLE = "rosetable";
	private static final String SEP = " ";

	// randomness
	private static final int SEEDINI1 = 2806, SEEDINI2= 2803, SEEDSYS=1976;

	private static final boolean PRINTSAMPLE = true;

	private static final String LABELALPHAI = "alphaI", LABELTARTAN="trtn";
	private static final double ALPHAI = 0.95, ALPHAI2=.5;

	// attributes
	final LatticeKalignmentMD m_md;
	LatticeKinesisSampler m_sampler;

	// exp attributes
	final private int m_L;
	private double m_sigma;
	private double m_initdens;


	SwarmingExp(int expNum, int L){
		// do we need a viewer ?
		IntegerList EXPVIEW= new IntegerList();
		EXPVIEW.AddMulti(2,3,5,6,8,9); // excluded
		boolean withViewer= !EXPVIEW.Contains(expNum);
		m_L= L;
		m_md= new LatticeKalignmentMD();
		// preparation of the system
		// special case for EXP 10 : rectangular shape
		IntC xysize=	(expNum==10)?new IntC(5*L,L):
			((expNum==11)?new IntC(L,75*L/100):   
				new IntC(L,L));
		boolean reflectingB=(expNum==12);
		LatticeKinesisSystem system= new SwarmingLGCAsys(xysize,reflectingB);
		if (withViewer){
			IntC pixCellSize= new IntC(PIXSIZE, BORDER);
			m_sampler= 
					new LatticeKinesisSampler(system, pixCellSize);
		} else {
			m_sampler= new LatticeKinesisSampler(system);
		}
		m_md.LinkTo(m_sampler);
	}



	static private void MakeFigRoseTable() {
		int L=3;
		IntC xysize= new IntC(L,L);
		LatticeKinesisSystem system= new SwarmingLGCAsys(xysize);
		IntC pixCellSize= new IntC(PIXSIZE, BORDER);
		LatticeKinesisSampler sampler = 
				new LatticeKinesisSampler(system, pixCellSize);		
		SwarmingLmigInit init = (SwarmingLmigInit)sampler.GetInitializer();
		init.InitRoseTable();
		sampler.io_SetRecordFormat(ImgFormat.SVG);
		sampler.io_SaveImage(ROSETABLE, sampler.GetViewer());
		//sampler.ShowInWindow();
	}


	/*-----------------------------------------------
	 * SNAPSHOT experiments 
	 */

	private void SnapStripe() {
		InitConditions(INITDENSLO, SIGMALO, SEEDINI1, SEEDSYS);
		SnapExp(LABELONE, 10, 100);
	}

	private void SnapCheckerboard() {
		InitConditions(INITDENSHI, SIGMALO, SEEDINI1, SEEDSYS);
		SnapExp(LABELTWO,4,250);
	}

	private void SnapCluster() {
		InitConditions(INITDENSLO, SIGMAHI, SEEDINI2, SEEDSYS);
		SnapExp(LABELTHREE,4,50);
	}


	/*-----------------------------------------------
	 * other SNAPSHOT experiments 
	 */

	/** shape is set in constructor **/
	private void RectangularShapeStripe() {
		InitConditions(INITDENSLO, SIGMALO, SEEDINI1, SEEDSYS);
		m_sampler.NstepsAndUpdate(2000);
		Save(LABELRECT);
		m_sampler.NstepsAndUpdate(2000);
		Save(LABELRECT);
	}

	private void RectangularInharmonicStripe() {
		InitConditions(INITDENSLO, SIGMARECT1, SEEDINI1, SEEDSYS);
		m_sampler.NstepsAndUpdate(5000);
		Save(LABELRECTNH);
		InitConditions(INITDENSLO, SIGMARECT2, SEEDINI1, SEEDSYS);
		m_sampler.NstepsAndUpdate(5000);
		Save(LABELRECTNH);
		InitConditions(INITDENSLO, SIGMARECT3, SEEDINI1, SEEDSYS);
		m_sampler.NstepsAndUpdate(5000);
		Save(LABELRECTNH);

	}

	private void ReflectingBordersStripe() {
		InitConditions(INITDENSLO, SIGMALO, SEEDINI1, SEEDSYS);
		m_sampler.NstepsAndUpdate(1000);
		Save(LABELREFLECT);
		m_sampler.NstepsAndUpdate(1000);
		Save(LABELREFLECT);
		m_sampler.NstepsAndUpdate(2000);
		Save(LABELREFLECT);

	}

	private void AsynchronousInteraction() {
		InitConditions(INITDENSHI, SIGMALO, SEEDINI2, SEEDSYS);
		m_sampler.GetSystem().setAlphaI(1.);
		m_sampler.NstepsAndUpdate(1000);
		Save(LABELALPHAI);
		m_sampler.GetSystem().setAlphaI(ALPHAI);
		for (int i=0; i<4 ;i++){
			m_sampler.NstepsAndUpdate(25);
			Save(LABELALPHAI);
		}
	}

	int [] DELTAT = {0, 200, 800, 4000, 6000, 39000};
	private void TartanAsynchI() {
		double dini; String sini; 
		dini= INITDENSHI;
		InitConditions(dini, SIGMALO, SEEDINI1, SEEDSYS);
		m_sampler.GetSystem().setAlphaI(ALPHAI2);
		for (int dt : DELTAT){
			m_sampler.NstepsAndUpdate(dt);
			sini= FLString.ReplaceDotByLetterP(dini);
			Save(LABELTARTAN+sini);
		}	
		dini= INITDENSONEHALF;
		InitConditions(dini, SIGMALO, SEEDINI1, SEEDSYS);
		m_sampler.GetSystem().setAlphaI(ALPHAI2);
		for (int dt : DELTAT){
			m_sampler.NstepsAndUpdate(dt);
			sini= FLString.ReplaceDotByLetterP(dini);
			Save(LABELTARTAN+sini);
		}
	}



	/** produces n+1 images starting from t=0 with interval "steps" **/ 
	private void SnapExp(String label, int n, int steps){
		Save(label);
		for (int i=1; i<=n;i++){
			m_sampler.NstepsAndUpdate(steps);
			Save(label);
		}
	}


	/*---------------------------
	 * time experiments 
	 */


	private void TimeEvolutionOne() {
		TimeEvolution(INITDENSLO, SIGMALO, SEEDINI1, SEEDSYS, TIMEONE, FILENAME_EXP1);
	}

	private void TimeEvolutionTwo() {
		TimeEvolution(INITDENSHI, SIGMALO, SEEDINI1, SEEDSYS, TIMETWO, FILENAME_EXP2);
	}

	private void TimeEvolutionThree() {
		TimeEvolution(INITDENSLO, SIGMAHI, SEEDINI2, SEEDSYS, TIMETHREE, FILENAME_EXP3);
	}


	private void TimeEvolution(
			double initdens, double sigma, int seedini, int seedsys, 
			int timeSamp, String filenameOut) {
		InitConditions(initdens, sigma, seedini, seedsys);
		FLStringList data = new FLStringList();

		int i=0;
		boolean loop;
		do {
			String line="";
			line+= m_sampler.GetTime() + SEP;
			line+= m_md.GetMeasure();
			data.Add(line);
			loop = (i<timeSamp);
			if (loop){
				m_sampler.sig_NextStep();
				i++;
			}
		} while( loop  );

		data.WriteToFile(filenameOut);
	}


	/*---------------------------
	 * histogram experiments 
	 */


	private void StripeFormationHistogram(int nSAMPLE) {
		TimeFormationHistogram(nSAMPLE, INITDENSLO,SIGMALO, 1, GAMMALIMIT1, TIMELIMIT1);
	}

	private void CheckerboardFormationHistogram(int nSAMPLE) {
		//Macro.print("checkerboard histogram exp");
		TimeFormationHistogram(nSAMPLE, INITDENSHI, SIGMALO, -1, GAMMALIMIT2, TIMELIMIT2);
	}

	private void ClusterFormationHistogram(int nSAMPLE) {
		TimeFormationHistogram(nSAMPLE, INITDENSLO, SIGMAHI, 1, GAMMALIMIT3, TIMELIMIT3);
	}


	/** measuring time to stripe formation 
	 * gammaLimit : absolute value ; factor : +1 or -1 (checkerboard) **/
	private void TimeFormationHistogram(
			int nSample, double initdens, double sigma, 
			int factor, double gammaLimit, int timeLimit) {
		InitDensSigma(initdens, sigma);		

		FLStringList output= new FLStringList();
		//IntegerList lst = new IntegerList(); //for home-made processing

		for (int i=0; i< nSample; i++){
			Macro.ProgressBar(i, nSample);
			m_sampler.sig_Init();
			// HERE IS THE MEASUREMENT
			int time= SampleOneTime(factor, gammaLimit, timeLimit);
			String result= time + " >	" + GetExpConditions();
			if (PRINTSAMPLE){
				Macro.print(result);
			}
			output.Add(result);
			//lst.Add(time);
		}
		output.Print();
		String strG= FLString.ReplaceDotByLetterP(gammaLimit);
		String filenameD= String.format("distr-L%d-Gamma%s.raw",m_L, strG);	
		output.WriteToFile(filenameD);
		/*FLhistogram histo = 
				new FLhistogram(lst, 50, 100);
		FLStringList histodat= histo.GetFrequencies();
		histodat.Print();
		String filename= String.format("histo-%s.raw", Lsigma());
		histodat.WriteToFile(filename);*/
	}

	/** measuring time to stripe formation **/
	private int SampleOneTime(int factorPlusMinus, double gammaLimit, int timeLimit) {
		//Macro.Debug("exp : " + GetExpConditions());
		Date t0= Macro.GetTime();
		int time=m_sampler.GetTime();
		boolean flag = true;
		while (flag){
			double val= factorPlusMinus * m_md.GetMeasure();
			//Macro.fDebug(" val:%.2f  gammaL:%f", val, gammaLimit);
			flag= (val < gammaLimit) && (time<timeLimit);
			m_sampler.sig_NextStep();
			time= m_sampler.GetTime();
		}
		Date t1= Macro.GetTime();
		Macro.print("sampled in " + FLString.TimeFormat(t1, t0));
		return time;
	}



	/*
	 * init / get / set
	 * 
	 */

	private String Lsigma() {
		return "L" + m_L + "-S" + FLString.ReplaceDotByLetterP(m_sigma);
	}

	private String GetExpConditions() {
		String info=
				String.format("# L=%d seed-ini=%d  seed-sys=%d", 
						m_L, m_sampler.GetInitSeed(), m_sampler.GetSysSeed());
		info += " sigma=" +	m_sigma + " ini-dens=" + m_sampler.GetInitDens();
		return info;
	}


	private void InitConditions(double initdens, double sigma, int seedini, int seedsys) {
		InitDensSigma(initdens,sigma);
		m_sampler.SetSeedInitSys(seedini, seedsys);
		m_sampler.io_SetRecordFormat(ImgFormat.SVG);
		m_sampler.sig_Init();
	}

	private void InitDensSigma(double initdens, double sigma) {
		Macro.fPrint("exp conditions: ini-dens %.3f  sigma:%.3f",initdens, sigma);
		m_initdens = initdens;
		m_sigma = sigma;
		m_sampler.SetInitdensSigma(m_initdens,m_sigma);
	}

	private String GetFileName(String snapLabel) {
		return String.format("snap-%s-%s-t%d",snapLabel, Lsigma(), m_sampler.GetTime());
	}

	private void Save(String snapLabel) {
		m_sampler.io_SaveImage(GetFileName(snapLabel),m_sampler.GetViewer());
	}


	/*
	 * run
	 */

	void DoHistogram(int expNum, int NSAMPLE){
		switch (expNum){
		case 3:
			StripeFormationHistogram(NSAMPLE); break;
		case 6:
			CheckerboardFormationHistogram(NSAMPLE); break;
		case 9:
			ClusterFormationHistogram(NSAMPLE); break;
		}
	}

	void DoExp(int expNum){		
		switch (expNum){
		case 0:
			MakeFigRoseTable(); break;
		case 1:
			SnapStripe(); break;
		case 2:
			TimeEvolutionOne(); break;
		case 4:
			SnapCheckerboard(); break;
		case 5:
			TimeEvolutionTwo(); break;
		case 7:
			SnapCluster(); break;
		case 8: 
			TimeEvolutionThree(); break;
		case 10:
			RectangularShapeStripe(); break;
		case 11:
			RectangularInharmonicStripe(); break;
		case 12:
			ReflectingBordersStripe(); break;
		case 13:
			AsynchronousInteraction(); break;
		case 14:
			TartanAsynchI(); break;
		default:
			Macro.FatalError("exp not found.");
		}
	}


	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();

	public static void main(String[] argv) {
		Macro.BeginTest(CLASSNAME);
		CommandParser parser= new CommandParser(CLASSNAME, argv);
		int expNum= parser.IParse("EXP=");
		if (expNum==0){
			MakeFigRoseTable();
		} else if ((expNum==3)||(expNum==6)||(expNum==9)){
			int L=parser.IParse("L=");
			int N=parser.IParse("N=");
			SwarmingExp exp = new SwarmingExp(expNum, L);
			exp.DoHistogram(expNum,N);
		} else {
			int L=parser.IParse("L=");
			SwarmingExp exp = new SwarmingExp(expNum, L);
			exp.DoExp(expNum);
		}

		Macro.EndTest();
	}

}

