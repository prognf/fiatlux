package physis;

import main.Macro;
import main.commands.CommandParser;
import architecture.dtasep.ParticleBalanceMD;
import architecture.dtasep.TasepSampler;

import components.types.FLStringList;

public class DTasepNewModelExp {

	private static final int HISTSIZE = 100;
	private static final String FILENAME_PREFIX_HIST = "AttractorLifeExpectancy--";
	private static final double RHO2 = 0;
	private static final double RHO1 = 0.8;
	private static final double SIGMAMAX = 3.7;
	private ParticleBalanceMD m_md1;
	TasepSampler m_sampler;

	// exp attributes
	private int m_size;


	DTasepNewModelExp(int size){
		Constructor(size);
	
	}

	DTasepNewModelExp(){
	}

	void Constructor(int size){
		m_size= size;
		m_md1= new ParticleBalanceMD();
		m_sampler= new TasepSampler(m_size);
		m_md1.LinkTo(m_sampler);

	}

	public void Histogram(){
		Histogram(RHO1, RHO2, SIGMAMAX);
	}


	private void Histogram(double initdensT1, double initdensT2, double sigmamax){
		FLStringList data = new FLStringList();
		data.Add("#rho1 = "+initdensT1 + ", rho2 = "+initdensT2 + ", sigmamax = " + sigmamax);
		for (int i = 0; i<HISTSIZE; i++){
			double sigma = sigmamax * (double) i/ (double) HISTSIZE;
			InitConditionsRND(initdensT1, initdensT2, sigma);
			double time = AvgTimeEscapingAttractor(1, 0., 800000, 50);
			String line = sigma+" "+time;
			data.Add(line);
			Macro.HistogramProgress(i, HISTSIZE);
		}
		String filenameOut = 	FILENAME_PREFIX_HIST + 
				CreateFileNameSuffix(initdensT1, initdensT2, sigmamax, m_size);
		data.WriteToFile(filenameOut);
	}



	private double AvgTimeEscapingAttractor(int deltaInitialSign, double deltaLimit, int timeLimit, int nbSamples) {
		double avgValue = 0;
		for (int i = 0; i < nbSamples; i++){
			avgValue += SampleOneTime(deltaInitialSign, deltaLimit, timeLimit);
		}
		return avgValue /= nbSamples;
	}

	/** measuring necessary time to exit attractor **/
	private int SampleOneTime(int signeDeltaInitial, double deltaLimit, int timeLimit) {
		m_sampler.sig_Init();
		int time=m_sampler.GetTime();
		boolean loop = true;
		while (loop){
			double val= signeDeltaInitial * m_md1.GetMeasure();
			loop= (val > deltaLimit) && (time<timeLimit);
			m_sampler.sig_NextStep();
			time= m_sampler.GetTime();
		}
		time= m_sampler.GetTime();
		Macro.print(""+time);
		return time;
	}
	
	/////////////////////////////////////////
	//for files
	/////////////////////////////////////////
	


	private String CreateFileNameSuffix(double initdensT1, double initdensT2,
			double sigma, int i) {
		String s = "_s_" + sigma + 
				"_d1_" + initdensT1 + 
				"_d2_" + initdensT2 +
				" " + i +
				".dat";
		return s;
	}


	private String GetExpConditions() {
		String info=
				String.format("# L=%d seed-ini=%d  seed-sys=%d", 
						m_size, m_sampler.GetInitSeed(), m_sampler.GetSysSeed());
		info += " sigma=" +	m_sampler.GetSigma() +
				" ini-denst1=" + m_sampler.GetInitDensTasep1() +
				" ini-denst2=" + m_sampler.GetInitDensTasep2();
		return info;

	}

	private void InitConditionsRND(double initdensT1, double initdensT2,
			double sigma) {
			m_sampler.SetInitdensSigma(initdensT1, initdensT2,sigma);
			m_sampler.sig_Init();
	}


	///////////////////////////////
	//to launch experiments
	//////////////////////////////

	void DoExp(int expNum){		
		switch (expNum){

		case 1:
			//With dtasep 3rd movement mode
			Histogram(); 
			break;


		default:
			Macro.FatalError("exp not found.");
		}
	}


	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();

	public static void main(String[] argv) {


		Macro.BeginTest(CLASSNAME);
		CommandParser parser= new CommandParser(CLASSNAME, argv);
		int expNum= parser.IParse("EXP=");

		int size=parser.IParse("size=");
		DTasepNewModelExp  exp;
		exp = new DTasepNewModelExp(size);

		exp.DoExp(expNum);

		Macro.EndTest();

	}


}
