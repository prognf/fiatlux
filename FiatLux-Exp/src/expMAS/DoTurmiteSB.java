package expMAS;

import grafix.gfxTypes.elements.FLPanel;
import grafix.gfxTypes.imgFormat.ImgFormat;
import grafix.windows.SimulationWindowSingle;
import main.Macro;
import main.applet.FLPanelApplet;
import main.applet.OpenFrameButton;
import main.commands.CommandInterpreter;
import main.designPanels.MASdesignerPanel;
import models.MASmodels.turmite.TurmiteSystem;
import models.MASmodels.turmite.TurmiteSystemModel1;
import models.MASmodels.turmite.TurmiteSystemModel2;
import models.MASmodels.turmite.TurmitesGFXInitializer;
import architecture.multiAgent.tools.MultiAgentSampler;
import architecture.multiAgent.tools.MultiAgentSystem.EXCLUSIONMODE;
import architecture.multiAgent.tools.MultiAgentViewer;

import components.types.FLString;
import components.types.FLStringList;
import components.types.IntC;
import components.types.IntegerList;


/*--------------------
 * for automated Turmites experiments
 * @author Nazim Fates
*--------------------*/

public class DoTurmiteSB extends CommandInterpreter {

	final static String 
		CLASSNAME= "DoTurmiteFilm ",
		USAGE= "SEQ=<seq number>";
	final static int N_ARG= 1;

	final static int ONEAGENT=1, TWOAGENTS=2,
		SIMUTIME1A = 11500, SIMUTIME1B= 3000,
		SIMUTIME2= 20;
	
	private static final int 
		CellSizePix = 20,
		CellSizePixBig= 50;
	final static IntC 
		CellSize= new IntC(CellSizePix, 0),
		CellSizeBorders = new IntC(CellSizePix-1, 1),
		CellSizeBig= new IntC(CellSizePixBig, 0),
		SZ1= new IntC(80, 80),
		SZCLOCK= new IntC(14,14),
		SZGLI= new IntC(6,6),
		SZ3= new IntC(120,100);
	final static String 
		FL_SAVETIMES = "savedImages.txt",
		FILE_OUTPUT="Turmite" ;
	

	/*--------------------
	 * attributes
	 --------------------*/
	TurmiteSystem m_system;
	MultiAgentViewer m_viewer;
	TurmitesGFXInitializer m_Init;
	MultiAgentSampler m_sampler;
	
	FLStringList m_SavedImage = new FLStringList();	
	
	/*--------------------
	 * EXP
	 --------------------*/
	
	/** main * */
	private static void DoExp(final int expnum) {
		DoTurmiteSB cmd = new DoTurmiteSB();
		switch(expnum){
		case 0:
			cmd.Fig1();
			cmd.Fig3();
			cmd.Fig4();
			cmd.Fig5();
			break;
		case 1:			
			cmd.Fig1();
			break;
		case 2:
			cmd.Fig3();
			break;
		case 3:
			cmd.Fig4();
			break;
		case 4:
			cmd.Fig5();
			break;
		}
	}
	
	
	/** for applets */
	private static void DoApplet(int expnum) {
		DoTurmiteSB cmd = new DoTurmiteSB();
		switch(expnum){
		case 1:			
			//cmd.Applet1();
			cmd.Fig1();
			break;
		case 2:
			cmd.Fig3();
			break;
		case 3:
			cmd.Fig4();
			break;
		case 4:
			cmd.Fig5();
			break;
		}
	}
	
	/* one Turmite with two initial conditions */
	private void Applet1(){
		InitFig1();
		
		SimulationWindowSingle simu= new SimulationWindowSingle(" simu turmite", m_sampler);
		FLPanel panel= simu.GetSimulationPanel();
		OpenFrameButton frame= new OpenFrameButton("OPN", "try", panel);
		frame.DoAction();		
	}

	class MASappletFig1 extends FLPanelApplet {
		
		public final String NAME = "MAS Designer Panel";

		@Override
		public FLPanel getPanel() {
			return new MASdesignerPanel();
		}

		@Override
		public String getAppletTitle() {
			return NAME;
		}

	}
	
	
	/* one Turmite with two initial conditions */
	private void Fig1(){
		InitFig1();
		RecordOneConfig(11000);		
	}
	
	public void InitFig1(){
		CreateSampler(SZ1, CHOICE.MODEL1);
		SetExpName("Fig1");
		m_Init.SelectLineInit(ONEAGENT,0, 1, 0);		
	}
	
	int [] TFIG3= {0, 7, 8, 15, 30, 45, 52, 60};  
	
	
	
	/** Fig. 3 **/
	private void Fig3(){	
		InitFig3();
		RecordConfig(TFIG3);		
	}
	
	public void InitFig3() {
		CreateSampler(SZCLOCK, CHOICE.MODEL1);
		SetExpName("Fig3");
		m_Init.SetOffset(5, SZCLOCK.Y() / 2 - 1);
		m_Init.SelectLineInit(4, 1, 0, 0);		
	}

	int [] TFIG4= {0, 4, 8, 12};
	
	/** Fig. 4 **/
	private void Fig4(){	
		InitFig4();
		RecordConfig(TFIG4);		
	}
	
	
	private void InitFig4() {
		CreateSampler(SZGLI, CHOICE.MODEL1);
		SetExpName("Fig4");
		m_Init.SetOffset(1, SZGLI.Y() / 2 - 1);
		m_Init.SelectSquareInit(1, 0, 0);		
	}

	


	/** Fig. 5 **/
	private void Fig5(){	
		CreateSampler(SZ3, CHOICE.MODEL1,EXCLUSIONMODE.EXCLUDE_POLICY);
		
		InitFig5(52);
		RecordOneConfig(11539);
		
		InitFig5(56);
		RecordOneConfig((11539+104));
	}
	
	private void InitFig5(int k) {
		SetExpName("Fig5-k" + k);
		m_Init.SelectLineInit(2, k, 1, 3);
		m_Init.SetOffset(30, 10 + SZ3.Y() / 2);		
	}


	private void RecordOneConfig(int t) {
				IntegerList trec= new IntegerList();
				trec.addVal(t);
				RecordConfig(trec);
	}


	private void RecordConfig(int[] rectime) {
		RecordConfig(new IntegerList(rectime));		
	}


	
	/*--------------------
	 * Procedures
	 --------------------*/
	
	private void SetExpName(String in_ExpName){
		m_sampler.SetSaveName(in_ExpName);
	}

	enum CHOICE {MODEL1,MODEL2}

    private void CreateSampler(IntC SystemSize, CHOICE model,
			IntC in_CellSize){
		
		
		switch (model){
		case MODEL1:
			m_system= new TurmiteSystemModel1(SystemSize);
			break;
		case MODEL2:
			m_system= new TurmiteSystemModel2(SystemSize);
			break;
		}		
		m_sampler= new MultiAgentSampler(m_system, in_CellSize );
		m_Init= (TurmitesGFXInitializer)m_sampler.GetInitializer();
		m_Init.m_AbsorbingBorders= false; // NO ABSORBING BORDERS
	}

    private void CreateSampler(IntC SystemSize, CHOICE model){
		CreateSampler(SystemSize, model, CellSize);
		m_sampler.io_SetRecordFormat(ImgFormat.SVG);
	}
	
	private void CreateSampler(IntC SystemSize, CHOICE model, EXCLUSIONMODE exc){
		CreateSampler(SystemSize, model);
		m_system.SetExclusionMode(exc);
	}
	
	
	private void RecordConfig(IntegerList configTime){
		m_sampler.sig_Init();
		// sequence
		for (int rectime : configTime){
			
			while (m_sampler.GetTime() < rectime ){ 
				m_sampler.sig_NextStep();	
			}		
			m_sampler.io_SaveImage();				
			
		}		
	}
	
	
	
	
	/** main : processing command line * */
	public static void main(String[] argv) {
		LaunchMessage(CLASSNAME, USAGE);
		try{
			if (argv.length == N_ARG) {
				int seq= FLString.IParseWithControl(argv[0], "SEQ=");
				/*	DoExp(seq);*/
				DoApplet(seq);
			} else {
				CommandInterpreter.Usage(argv);
			}
		}
		catch(Exception e){
			Macro.print(" Exception caught at the top: ");
			e.printStackTrace();
		}	
		CommandInterpreter.ByeMessage();
	}
}
