package expMAS;

import main.Macro;
import main.commands.CommandInterpreter;
import models.MASmodels.turmite.TurmiteSystem;
import models.MASmodels.turmite.TurmiteSystemModel1;
import models.MASmodels.turmite.TurmiteSystemModel2;
import models.MASmodels.turmite.TurmitesGFXInitializer;
import architecture.multiAgent.tools.MultiAgentSampler;
import architecture.multiAgent.tools.MultiAgentSystem.EXCLUSIONMODE;
import architecture.multiAgent.tools.MultiAgentSystem.UPDATEPOLICY;
import architecture.multiAgent.tools.MultiAgentViewer;

import components.types.FLString;
import components.types.FLStringList;
import components.types.IntC;


/*--------------------
 * for automated Turmites experiments
 * @author Nazim Fates
*--------------------*/

public class DoTurmiteFilm extends CommandInterpreter {

	final static String 
		CLASSNAME= "DoTurmiteFilm ",
		USAGE= "SEQ=<seq number>";
	final static int N_ARG= 1;

	final static int ONEAGENT=1, TWOAGENTS=2,
		SIMUTIME1A = 11500, SIMUTIME1B= 3000,
		SIMUTIME2= 20;
	
	private static final int 
		CellSizePix = 10,
		CellSizePixBig= 50;
	final static IntC 
		CellSize= new IntC(CellSizePix, 0),
		CellSizeBorders = new IntC(CellSizePix-1, 1),
		CellSizeBig= new IntC(CellSizePixBig, 0),
		XXXL =  new IntC(400,400),
		XXL=  new IntC(200,200),
		XL = new IntC(120,120),
		L= new IntC(100, 100),
		MM= new IntC(50, 50),
		M= new IntC(30,30),
		SP = new IntC(15,15),
		S= new IntC(10,10);
	final static String 
		FL_SAVETIMES = "savedImages.txt",
		FILE_OUTPUT="Turmite" ;
	

	/*--------------------
	 * attributes
	 --------------------*/
	TurmiteSystem m_system;
	MultiAgentViewer m_viewer;
	TurmitesGFXInitializer m_Init;
	MultiAgentSampler m_sampler;
	
	FLStringList m_SavedImage = new FLStringList();	
	
	/*--------------------
	 * EXP
	 --------------------*/
	
	/** main * */
	private static void DoExp(int expnum) {
		DoTurmiteFilm cmd = new DoTurmiteFilm();
		switch(expnum){
			case 1:			
			cmd.Exp1();
			break;
		case 2:
			cmd.Exp2();
			break;
		case 3:
			cmd.Exp3();
			break;
		case 4:
			cmd.Exp4();
			break;
		case 5:
			cmd.Exp5();
			break;
		case 6:
			cmd.Exp6();
			break;
		case 7:
			cmd.Exp7();
			break;
		case 8:
			cmd.Exp8();
			break;
		case 9:
			cmd.Exp9();
			break;
		}
	}
	
	
	
	
	/* one Turmite with two initial conditions */
	private void Exp1(){	
		CreateSampler(L, CHOICE.MODEL1);
		SetExpName("OneTurmite-A");
		// exp A
		m_Init.SelectLineInit(ONEAGENT,0, 1, 0);
		MakeFilm(SIMUTIME1A,100);
		
		// exp BS
		SetExpName("OneTurmite-B");
		m_Init.SelectOneCellOneInit();
		MakeFilm(SIMUTIME1B,100);
		FinishExp();
	}
	
	/* model 1 : compare ALLOW / EXCLUDE 
	 * Fig. 3 */
	private void Exp2(){
		CreateSampler(S, CHOICE.MODEL1, CellSizeBig);
		m_Init.SelectLineInit(TWOAGENTS, 2, 2, 2);
		m_Init.ShiftOffset(-2, -1);
		// exp A1
		SetExpName("TwoTurmites-M1-Allow-ST");
		SelectAllowPolicy();
		MakeFilm(SIMUTIME2,1);
		
		// exp B1
		SetExpName("TwoTurmites-M1-Exclu-ST");
		SelectExcludePolicy();
		MakeFilm(SIMUTIME2,1);
		
		
		// exp C1
		CreateSampler(S, CHOICE.MODEL2, CellSizeBig);
		m_Init.SelectLineInit(TWOAGENTS, 2, 2, 2);
		m_Init.ShiftOffset(-2, -1);
		SetExpName("TwoTurmites-M2-Exclu-ST");
		SelectExcludePolicy();
		MakeFilm(SIMUTIME2,1);
		
		// *** LARGE GRID
		CreateSampler(L, CHOICE.MODEL1, CellSize);
		m_Init.SelectLineInit(TWOAGENTS, 2, 2, 2);
		// exp A1
		SetExpName("TwoTurmites-M1-Allow-LT");
		SelectAllowPolicy();
		MakeFilm(2500,100);
		
		// exp B1
		SetExpName("TwoTurmites-M1-Exclu-LT");
		SelectExcludePolicy();
		MakeFilm(6200,100);
		
		
		// exp C1
		CreateSampler(L, CHOICE.MODEL2, CellSize);
		m_Init.SelectLineInit(TWOAGENTS, 2, 2, 2);
		SetExpName("TwoTurmites-M2-Exclu-LT");
		SelectExcludePolicy();
		MakeFilm(7500,100);
		
	}
	
	/*
	 * Figure 4 in article
	 */
	private void Exp3(){
		
		CreateSampler(S, CHOICE.MODEL1);
		m_Init.SelectLineInit(TWOAGENTS, 1, 0, 0);
		// exp 1
		SetExpName("Twins-M1-Allow");
		SelectAllowPolicy();
		MakeFilm(100,1);
		
		// exp 2
		CreateSampler(L, CHOICE.MODEL1);
		m_Init.SelectLineInit(TWOAGENTS, 1, 0, 0);
		SetExpName("Twins-M1-Exclu");
		SelectExcludePolicy();
		MakeFilm(2000,20);
		
		
		// exp 3
		CreateSampler(M, CHOICE.MODEL2);
		m_Init.SelectLineInit(TWOAGENTS, 1, 0, 0);
		SetExpName("Twins-M2-Exclu");
		SelectExcludePolicy();
		MakeFilm(400,4);
		
	}
	
	/* Deadlock */	
	/*
	 * figure 5 in article
	 */
	
	private void Exp4(){
	CreateSampler(S, CHOICE.MODEL1);
		m_Init.SelectLineInit(TWOAGENTS, 1, 1, 0);
		m_Init.ShiftOffset(-1, -1);
		SetExpName("Deadlock-M1-Exclu");
		SelectExcludePolicy();
		MakeFilm(100,1);
    }
	
	/* square deadlock */
	private void Exp5() {
		/*
		 * Figure not in article
		 */
//		CreateSampler(S, CHOICE.MODEL1);
//		m_Init.SelectSquareInit(1, 1, -1);
//		int [] SIMUTIME= {0, 1, 2, 3, 4 ,5 };
//		SetExpName("Sym4Deadlock-M1-Exclu");
//		SelectExcludePolicy();
//		RunList(SIMUTIME);	
//		
//		CreateSampler(M, CHOICE.MODEL1);
//		m_Init.SelectSquareInit(1, 1, -1);
//		int [] SIMUTIME2= {100, 200, 400 };
//		SetExpName("Cross-M1-Allow");
//		SelectAllowPolicy();
//		RunList(SIMUTIME2);			
	}
	
	/* turning & retracting paths */
	private void Exp6() {
		/*
		 * figure 6 top and bottom
		 */
			
		CreateSampler(XL, CHOICE.MODEL1);
		m_Init.SelectLineInit(4, 3, 2, 2);
		SetExpName("RetractingPaths-M1-Allow");
		SelectAllowPolicy();
		MakeFilm(6577,100);	
		
		CreateSampler(XXL, CHOICE.MODEL1);
		m_Init.SelectLineInit(4, 3, 2, 2);
		SetExpName("TurningPaths-M1-Exclu");
		SelectExcludePolicy();
		MakeFilm(12000,500);
		
		/* CreateSampler(XXXL, CHOICE.MODEL1);
		m_Init.SelectLineInit(4, 3, 2, 2);
		int [] SIMUTIME3= {12000, 14000, 16000 };
		SetExpName("TurningPaths-M1-Exclu");
		SelectExcludePolicy();
		RunList(SIMUTIME2); */	
		
	}
	
	


	/* Diamond-4 */
	private void Exp7() {
		/*
		 * figure 7 in article
		 */
		CreateSampler(L, CHOICE.MODEL1);
		SetExpName("Diamond4-M1-Exclu");
		m_Init.SelectSquareInit(4, 0, -1);
		SelectExcludePolicy();
		MakeFilm(2000,100);	
		
		CreateSampler(L, CHOICE.MODEL2);
		SetExpName("Diamond4-M2-Exclu");
		m_Init.SelectSquareInit(4, 0, -1);
		SelectExcludePolicy();
		MakeFilm(2000,100);	
		
	}
	
	/* Gliders */
	private void Exp8() {
		/* figure 8 and figure 9
		 */
	
		CreateSampler(MM, CHOICE.MODEL1);
		SetExpName("Gliders-M1-Allow");

		m_Init.SelectSquareInit(4, 1, 1);
		SelectAllowPolicy();
		MakeFilm(700,5);
		
		//Fig 9
		CreateSampler(S, CHOICE.MODEL1);
		SetExpName("ZoomGlider-M1-Allow");
		
		m_Init.SelectGliderInit();
	
		SelectAllowPolicy();
		MakeFilm(30,1);	
		
	}	

	/* "Faces" with three updating modes
	 * Fig. 2 in the article */
	private void Exp9() {
			
		
		// MID SIZE EXP
		CreateSampler(SP, CHOICE.MODEL1);
		
		// synch
		m_Init.ShiftOffset(-2, -1);
		m_Init.SelectFaceInit(4, 0);
		int SIMUTIME_ST= 90;
		SelectAllowPolicy();
		SetExpName("Faces-M1-Allow-Synch-ST");
		MakeFilm(SIMUTIME_ST,1);
		
		// seq A 
		SelectSequentialUpdating();
		SetExpName("Faces-M1-Allow-SeqA-ST");
		MakeFilm(SIMUTIME_ST,1);

		// seq B 
		m_Init.SelectFaceInit(4, 1);
		SetExpName("Faces-M1-Allow-SeqB-ST");
		MakeFilm(SIMUTIME_ST,1);
		
		
		// final config, synch
		CreateSampler(XL, CHOICE.MODEL1, EXCLUSIONMODE.ALLOW_POLICY);
		// synch
		m_Init.SelectFaceInit(4, 0);
		
		SelectAllowPolicy();
		SetExpName("Faces-M1-Allow-Synch-LT");
		MakeFilm(13000,100);
		
		// seq A 
		SelectSequentialUpdating();
		SetExpName("Faces-M1-Allow-SeqA-LT");
		MakeFilm(2000,100);

		// seq B 
		m_Init.SelectFaceInit(4, 1);
		SetExpName("Faces-M1-Allow-SeqB-LT");
		MakeFilm(3000,100);
		
	
		
	}
		
	/*--------------------
	 * Procedures
	 --------------------*/
	
	private void SetExpName(String in_ExpName){
		m_sampler.SetSaveName(in_ExpName);
	}

	private void FinishExp(){
		m_SavedImage.WriteToFile(FL_SAVETIMES);
	}
	
	enum CHOICE {MODEL1,MODEL2}

    private void CreateSampler(IntC SystemSize, CHOICE model,
			IntC in_CellSize){
		
		
		switch (model){
		case MODEL1:
			m_system= new TurmiteSystemModel1(SystemSize);
			break;
		case MODEL2:
			m_system= new TurmiteSystemModel2(SystemSize);
			break;
		}		
		m_sampler= new MultiAgentSampler(m_system, in_CellSize );
		m_Init= (TurmitesGFXInitializer)m_sampler.GetInitializer();
		m_Init.m_AbsorbingBorders= false; // NO ABSORBING BORDERS
	}

    private void CreateSampler(IntC SystemSize, CHOICE model){
		CreateSampler(SystemSize, model, CellSize);
	}
	
	private void CreateSampler(IntC SystemSize, CHOICE model, EXCLUSIONMODE exc){
		CreateSampler(SystemSize, model);
		m_system.SetExclusionMode(exc);
	}
	
	
	private void MakeFilm(int simultime, int step){
		m_sampler.sig_Init();
		// sequence
		for (int time=0; time <= simultime; time++){
			if (time % step == 0){
				m_sampler.io_SaveImage();				
			}			
			// for IO
			m_sampler.sig_NextStep();
		}		
	}
	
	
	
	
	
	
	private void SelectAllowPolicy() {
		m_system.SetExclusionMode(EXCLUSIONMODE.ALLOW_POLICY);
	}

	private void SelectExcludePolicy() {
		m_system.SetExclusionMode(EXCLUSIONMODE.EXCLUDE_POLICY);
	}
	
	private void SelectSequentialUpdating(){
		m_system.SetUpdateMode(UPDATEPOLICY.SEQUENTIAL);
	}
	
	/** main : processing command line * */
	public static void main(String[] argv) {
		LaunchMessage(CLASSNAME, USAGE);
		try{
			if (argv.length == N_ARG) {
				int seq= FLString.IParseWithControl(argv[0], "SEQ=");
				DoExp(seq);
			} else {
				CommandInterpreter.Usage(argv);
			}
		}
		catch(Exception e){
			Macro.print(" Exception caught at the top: ");
			e.printStackTrace();
		}	
		CommandInterpreter.ByeMessage();
	}
}
