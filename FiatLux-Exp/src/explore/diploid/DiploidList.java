package explore.diploid;

import java.util.ArrayList;

import components.types.FLString;
import components.types.FLStringList;
import components.types.IntC;
import components.types.IntegerList;
import components.types.RuleCode;
import main.Macro;
import models.CAmodels.tabled.ECAlookUpTable;

public class DiploidList {

	final static String OUTFILE="x-list-diploids.txt";
	final static int NUMLIST= 256;

	int [][] m_status = new int [NUMLIST][NUMLIST];
	int m_mingen=0;
	ArrayList<IntC> m_listMinRepC= new ArrayList<IntC>(); 
	FLStringList m_listMinRepStr= new FLStringList();

	public void BuildList(){
		Macro.print(4, "Building min. rep. list for diploids");
		for (long p=0; p<NUMLIST; p++){
			for (long q=p+1; q<NUMLIST; q++){
				if (p==q) break;
				long pc = ECAlookUpTable.GetConjugateRule(p);
				long pr = ECAlookUpTable.GetReflexiveRule(p);
				long pt = ECAlookUpTable.GetReflexiveConjugateRule(p);
				long qc = ECAlookUpTable.GetConjugateRule(q);
				long qr = ECAlookUpTable.GetReflexiveRule(q);
				long qt = ECAlookUpTable.GetReflexiveConjugateRule(q);
				MarkIfNew(p,q);
				MarkSeen(pc,qc);
				MarkSeen(pt,qt);
				MarkSeen(pr,qr);
			}
		}
		Macro.print("Min gen:"+ m_mingen);
		FLStringList output= new FLStringList();
		for (IntC rule : m_listMinRepC) {
			int rx=rule.X(), ry= rule.Y();
			String tcx= ECAlookUpTable.GetTransitionCodePadded(new RuleCode(rx)),
					tcy= ECAlookUpTable.GetTransitionCodePadded(new RuleCode(ry));
			String outline=String.format("%d %d - %s %s", rx, ry, tcx, tcy);
			output.Add(outline);
		}
		output.WriteToFile(OUTFILE);
	}

	/** min. generators **/
	private void MarkIfNew(long pp, long qq) {
		int p=(int)pp, q= (int)qq;
		if 	(m_status[p][q]==0){
			m_status[p][q]=1;
			m_mingen++;
			// adding couple
			m_listMinRepC.add(new IntC(p,q));
			String sdiploid= String.format("(%d,%d) ",p,q);
			m_listMinRepStr.Add(sdiploid);
		}		
	}

	/** equivalent : only one of the two couples
	 *  needs to be considered**/
	private void MarkSeen(long pp, long qq) {
		int //invert if p>q
		p=(int)(pp<qq?pp:qq), 
		q=(int)(pp<qq?qq:pp);
		if 	(m_status[p][q]==0){
			m_status[p][q]=2;	
		}
	}


	/** autonomous loop for counting classes [???] **/
	final static void CountClasses(){

		boolean // testing d : direct , i : indirect
		Cd, Ci, 
		Rd, Ri,
		Td, Ti,
		C,R,T;

		int n=0;
		int // counting
		NA=0,
		NCd=0, NCi=0,
		NRd=0, NRi=0,
		NTd=0, NTi=0,
		NS=0;

		int 
		betad=0, betai=0,
		gammad=0, gammai=0,
		deltad=0, deltai=0; // invariance by C only

		for (long p=0; p<NUMLIST; p++){
			for (long q=0; q<NUMLIST; q++){
				long pc = ECAlookUpTable.GetConjugateRule(p);
				long pr = ECAlookUpTable.GetReflexiveRule(p);
				long pt = ECAlookUpTable.GetReflexiveConjugateRule(p);
				long qc = ECAlookUpTable.GetConjugateRule(q);
				long qr = ECAlookUpTable.GetReflexiveRule(q);
				long qt = ECAlookUpTable.GetReflexiveConjugateRule(q);
				boolean E=(p==q); // repetitive couples
				Cd= (p==pc) && (q==qc);
				Ci= (p==qc) && (q==pc) && (!Cd);
				Rd= (p==pr) && (q==qr);
				Ri= (p==qr) && (q==pr) && (!Rd);
				Td= (p==pt) && (q==qt);
				Ti= (p==qt) && (q==pt) && (!Td);
				C=Cd||Ci;
				R=Rd||Ri;
				T=Td||Ti;
				boolean A= C && R && (!E);
				NA += A?1:0;
				NCd += Cd?1:0;
				NCi += Ci?1:0;
				NRd += Rd?1:0;
				NRi += Ri?1:0;
				NTd += Td?1:0;
				NTi += Ti?1:0;
				NS += (!(C||R||T))?1:0;
				boolean inbetai = Ci && !R && !T && (!E);
				boolean inbetad = Cd && !R && !T && (!E);
				boolean ingammai = Ti && !R && !C && (!E);
				boolean ingammad = Td && !R && !C && (!E);
				boolean indeltad = Rd && !C && !T && (!E);
				boolean indeltai = Ri && !C && !T && (!E);
				if (Rd&&Ri){
					Macro.PrintEmphasis("doublon:"+p+","+q);
				}
				if (A){
					PrnCouple(p,q);
				}
				betad += inbetad?1:0;
				betai += inbetai?1:0;
				gammad += ingammad?1:0;
				gammai += ingammai?1:0;
				deltad += indeltad?1:0;
				deltai += indeltai?1:0;
				//if (!E)
				n++;
			}
		}

		Macro.fPrint(" NA:%d    NCd:%d NCi:%d    NTd:%d NTi:%d   NRd:%d NRI:%d",
				NA, NCd, NCi, NTd, NTi, NRd, NRi);
		Macro.fPrint(" NS:%d  n:%d", NS, n);
		Macro.fPrint(" beta d:%d   beta i:%d", betad, betai);
		Macro.fPrint(" gamma d:%d  gamma i:%d", gammad, gammai);
		Macro.fPrint(" delta d:%d  delta i:%d", deltad, deltai);

	}

	private static void PrnCouple(long p, long q) {
		String 
		TRp= ECAlookUpTable.GetTransitionCodePadded(new RuleCode(p)),
		TRq= ECAlookUpTable.GetTransitionCodePadded(new RuleCode(q));
		//String msg = String.format("(%d;%d) ",p,q);
		String msg = String.format("(%s,%s) ",TRp,TRq);
		Macro.print(msg);
	}




	/** list of x found in the couples (R,x) ou (x,R) */
	public IntegerList Filter(int R) {
		IntegerList out= new IntegerList();
		for (IntC couple: m_listMinRepC){
			if (couple.X()==R){
				out.addVal(couple.Y());
			} else if (couple.Y()==R){
				out.addVal(couple.X());
			}
		}
		return out;
	}

	
	private void ListTyphloticDiagonals() {
		Macro.PrintEmphasis(" exploring typhlotic ECAs...");
		IntegerList lstT= new IntegerList();
		for (long p=0; p<NUMLIST; p++){
			long pc = ECAlookUpTable.GetConjugateRule(p);
			if (p==pc) {
				lstT.add((int)p);
			}
		}
		String lT= " listT:";
		for (int Wt : lstT) {
			lT+= String.format("(%d,%s) ", Wt, ECAlookUpTable.GetTransitionCodePadded(new RuleCode(Wt)) );
		}
		Macro.print(lT);
		Macro.PrintEmphasis(" exploring typhlotic diploids...");
		for (long p=0; p<NUMLIST; p++){
			for (long q=p+1; q<NUMLIST; q++){
				long pc = ECAlookUpTable.GetConjugateRule(p);
				long qc = ECAlookUpTable.GetConjugateRule(q);
				//if (q!=qc) break;
				if ((p==pc)&&(q==qc)) {
					//Macro.fPrint(" typhlo dipl. : (%d,%d)",p,q);
					m_listMinRepC.add( new IntC((int)p,(int)q) );
				}
			}
		}
		String cpl=FLString.ToStringNoSep(m_listMinRepC);
		Macro.fPrint("size:%d >> %s", m_listMinRepC.size(), cpl);
	}




	final static void PrnECAminimals(){
		IntegerList lstECAmin= ECAlookUpTable.GetEcaMinimalsList();
		lstECAmin.Print();
	}

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();
	public static void main(String[] argv) {
		Macro.BeginTest(CLASSNAME);
		DiploidList exp = new DiploidList();
		exp.BuildList();
		//exp.CountClasses();
		//exp.ListTyphloticDiagonals();
		Macro.EndTest();
		//PrnECAminimals();
	}





}
