package explore.diploid;

import java.util.Locale;

import components.types.FLString;
import components.types.FLStringList;
import components.types.IntegerList;
import components.types.RuleCode;
import experiment.measuring.MeasuringDevice;
import experiment.measuring.general.DecimalParameterMeasurer;
import experiment.measuring.general.DensityMD;
import experiment.measuring.linear.BlocMD;
import experiment.measuring.linear.KinksMD;
import experiment.samplers.CAsampler;
import experiment.samplers.LinearSampler;
import main.Macro;
import main.commands.CommandParser;
import models.CAmodels.stochastic.DiploidECAmodel;
import models.CAmodels.tabled.ECAlookUpTable;
import topology.basics.SamplerInfoLinear;
import topology.basics.TopologyCoder;

public class DiploidSpaceExp {
	
	final static int AFINE=5, ASTP=5;
	final static int N=2000;
	private static final int TMAX = 10000, TPOST=2000;
	
	CAsampler m_sampler;
	private DecimalParameterMeasurer m_densMD, m_kinksMD, m_blocMD;
	private DiploidECAmodel m_model;

	String m_output_filename;

	
	public DiploidSpaceExp() {
	}
	
	final int [] WITHNOT51a= {1, 3, 5, 7, 9, 11, 13, 15, 25, 27, 
			29, 33, 37, 45, 73, 77};
	final int [] WITHNOT51b= {32, 34, 40, 42, 56, 57, 58, 104, 106, 122,   	
			160, 162, 168, 170, 232};
	
	
	private void exploreAll(){
		FLStringList rulelist=  FLStringList.OpenFile(DiploidList.OUTFILE);
		for (String rule : rulelist){
			String [] token=rule.split(" ");
			int ra= FLString.ParseInt(token[0]),
				rb= FLString.ParseInt(token[1]);
			Macro.fPrint(" ********* dealing with couple (%d,%d)", ra, rb);
			DealWithCouple(ra, rb);
		}
	}
	
	private void ExploreCouplesWithNot(){
		for (int Wc : WITHNOT51b){
			RuleCode W= new RuleCode(Wc);
			String Tcode= ECAlookUpTable.GetTransitionCodePadded(W);
			Macro.print( Wc + ":" + Tcode + "--");
			//DealWithCouple(Wc, 51);
		}
	}
	
	private void ExploreRCinvariants() {
		IntegerList RCinv= ECAlookUpTable.GetRCinvariants();
		for (int R1 : RCinv){
			for (int R2:RCinv){
				if (R1<R2){
					Macro.fPrint("diagonal : %d,%d", R1, R2);
					DealWithCouple(R1, R2);
				}
			}
		}

	}

	private static void MakeBatchProcessRCinv() {
		FLStringList out= new FLStringList();
		IntegerList RCinv= ECAlookUpTable.GetRCinvariants();
		for (int R1 : RCinv){
			for (int R2:RCinv){
				if (R1<R2){
					String cmd= "python zproduceGraph.py "+R1 + " " + R2;
					out.Add(cmd);
				}
			}
		}
		out.WriteToFile("ybatch.sh");
		Macro.ExecuteSystemCommand("chmod u+x ybatch.sh");
	}

	

	private static void MakeBatchProcess(int W) {
		DiploidList lst= new DiploidList();
		lst.BuildList();
		IntegerList antiL= lst.Filter(W);
		for (int wanti : antiL){
			if (wanti%2==0){
			//Macro.print("python zToPlot.py "+wanti + " " + W);
				Macro.print("python zToPlot.py "+wanti);
			}
		}
	}

	private void ExploreCouplesWith(int W){
		DiploidList lst= new DiploidList();
		lst.BuildList();
		IntegerList antiList= lst.Filter(W);
		//REMOVED antiList.WriteToFile("Xanti-"+W+".txt");
		for( int Wanti : antiList){
			DealWithCouple(Wanti,W);
		}
	}
	
	
	void ExploreTyphloticD() {
		for (int p=0; p<256; p++){
			for (int q=p+1; q<256; q++){
				int pc = (int)ECAlookUpTable.GetConjugateRule(p);
				int qc = (int)ECAlookUpTable.GetConjugateRule(q);
				//if (q!=qc) break;
				if ((p==pc)&&(q==qc)) {
					//Macro.fPrint(" typhlo dipl. : (%d,%d)",p,q);
					DealWithCouple(p, q);
				}
			}
		}
	}
	

	private void ExploreCrossingsWithZero(){
		DiploidList lst= new DiploidList();
		lst.BuildList();
		IntegerList anti= lst.Filter(0);
		anti.Print();
		for( int W : anti){
			if (W%2==1){ // only 
				DealWithCouple(0,W);
			}
		}
	}

	
	private void ExplorePositiveRateCouples(){
		IntegerList eca= ECAlookUpTable.GetEcaMinimalsList();
		//REMOVED eca.WriteToFile("x-eca-list.txt");
		/*for( int W : eca){
				DealWithCouple(W,255-W);
		}*/
	}
	
	/** init for the exploration of a diagonal **/
	private void DealWithCouple(int W1, int W2){
		Macro.print("****** Dealing with diploid :" + W1 + "," + W2);
		BuildSampler(W1,W2);
		FLStringList out= MeasureCurrentRule(); //main
		out.Print();
		String filename= "asymptotic-"+ W1 + "-" + W2 + ".dat";
		out.WriteToFile(filename);
	}


	public void BuildSampler(int W1, int W2){
		RuleCode R1= new RuleCode(W1), R2= new RuleCode(W2);

		String 	topoCode = TopologyCoder.s_TLR1;
		m_model= new DiploidECAmodel(R1, R2);
		SamplerInfoLinear info= new SamplerInfoLinear(topoCode, m_model, N);
		m_sampler= new LinearSampler(info);
		m_densMD=  new DensityMD();
		m_kinksMD= new KinksMD();
		m_blocMD=  new BlocMD();
		((MeasuringDevice)m_densMD).LinkTo(m_sampler);
		((MeasuringDevice)m_kinksMD).LinkTo(m_sampler);
		((MeasuringDevice)m_blocMD).LinkTo(m_sampler);
	}

	private String DoExp(double alpha){
		m_model.SetWeightRule2(alpha);
		m_sampler.sig_Init();
		int 
		seedI= m_sampler.GetInitSeed(),
		seedM= m_sampler.GetModelSeed();
		for (int t=0; t<TMAX; t++){
			m_sampler.sig_NextStep();
		}
		double 	densA= m_densMD.GetMeasure(),
				kinksA= m_kinksMD.GetMeasure(),
				blocA=m_blocMD.GetMeasure();
		for (int t=0; t<TPOST; t++){
			m_sampler.sig_NextStep();
		}

		double 
		densB= m_densMD.GetMeasure(),
		kinksB= m_kinksMD.GetMeasure(),
		blocB=m_blocMD.GetMeasure();
		// ************* OUTPUT *************
		String strMeasure= String.format(Locale.ENGLISH, 
				"%.3f  %.3f %.3f %.3f post: %.3f %.3f %.3f seedIM: %d %d",
				alpha,
				densA, kinksA, blocA,
				densB, kinksB, blocB,
				seedI, seedM);
		return strMeasure;
	}



	/** sampling one given rule **/
	private FLStringList MeasureCurrentRule() {
		FLStringList output= new FLStringList();
		for (int ialpha=0; ialpha<AFINE; ialpha++){
			Sample(ialpha, output); 
		}
		for (int ialpha=AFINE; ialpha<100-AFINE; ialpha+=ASTP){
			Sample(ialpha, output); 
		}
		for (int ialpha=100-AFINE; ialpha<=100; ialpha++){
			Sample(ialpha, output); 
		}
		return output;
	}

	private void Sample(int ialpha, FLStringList output) {
		double alpha=ialpha/(double)100;
		output.Add( DoExp(alpha) );
	}


	private void ListEquivalenceTwoNeighbECA() {
		for (int index=1;index<16;index++){
			ECAlookUpTable.IdentifyECARule(index);
		}
	}

	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();

	final static int [] PtrWithZero = { 18, 22, 26, 28, 30, 50, 54, 58, 60, 62, 78, 90, 94, 110,
		122, 126, 146, 150, 154, 156, 158, 178, 182, 186, 188, 190, 202, 206, 218, 222, 234, 238, 250, 254};
	
	public void prnWithZero() {
		for (int W : PtrWithZero){
			Macro.print(W + " " + ECAlookUpTable.GetTransitionCodePadded(new RuleCode(W)) );
		}
	}
	
	public static void main(String [] argv){
		CommandParser parser = new CommandParser(CLASSNAME, "", argv);
		if (argv.length == 0 ){
			//int M= parser.IParse("M=");
			DiploidSpaceExp exp= new DiploidSpaceExp();
			exp.exploreAll();		
			
		} else if (argv.length == 1){
			int W=parser.IParse("W=");
			MakeBatchProcess(W);
			//MakeBatchProcessRCinv();
		} else {
			parser.Usage("?");
		}
	}

	
	
	//exp.ExploreTyphloticD();
	//exp.ExploreCrossingsWithZero();
	//exp.ExploreCouplesWith(90);
	//exp.ExploreCouplesWith(51);
	//exp.ExploreCrossingsWithZero();
	//exp.ExploreCouplesWith(184);
	
	//exp.ExploreRCinvariants();
	//ECAlookUpTable.GetCinvariants().Print();
	//exp.ExploreCouplesWithNot();
	//exp.ListEquivalenceTwoNeighbECA();
	//exp.DealWithCouple(60,3);
	//exp.DealWithCouple(90,5);
	//exp.ExplorePositiveRateCouples();
	
}
