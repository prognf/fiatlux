package explore.dichteGelessenheit;

import components.randomNumbers.FLRandomGenerator;
import components.types.FLString;
import components.types.FLStringList;
import main.Macro;
import main.MathMacro;

public class DirectDichteExp {

	final FLRandomGenerator m_randInit, m_randTrans;
	final static int TR=32; // number of transitions
	final static int N=31;
	final static int SAMP=2000; // number of samples for the accentuation
	private static final int TRUN = 22;

	double [] transP= new double[TR]; // # transition table

	int [] m_stateE= new int[N], m_stateO=new int[N];
	int m_time;

	/** -------------------------- **/

	public static void main(String [] argv){
		//DoSearch();
		DoAnalysis();
	}

	/** -------------------------- **/

	private DirectDichteExp() {
		m_randInit= FLRandomGenerator.GetNewRandomizer();
		m_randTrans= FLRandomGenerator.GetNewRandomizer();
		m_time=0;
	}

	public DirectDichteExp(String filename) {
		this();
		readSample(filename);
	}


	final static String CLASSNAME= Thread.currentThread().getStackTrace()[1].getClassName();
	private static final int ACCSAMPLE = 10;

	public static void DoSearch(){
		Macro.BeginTest(CLASSNAME);
		boolean flg;
		do {
			DirectDichteExp exp= new DirectDichteExp();
			exp.setRandomTransitions();
			double meanLight= exp.runOneRule(true);
			double meanDark= exp.runOneRule(false);
			flg = (meanLight<0.) || (meanDark<0.);
		} while (flg);
		Macro.EndTest();
	}

	public static void DoAnalysis(){
		Macro.BeginTest(CLASSNAME);
		String filename="output-trans-randRule.txt";
		//sample1.tabtxt";
		DirectDichteExp exp= new DirectDichteExp(filename);
		double meanLight= exp.runOneRule(true);
		double meanDark= exp.runOneRule(false);
		Macro.EndTest();
	}

	/** reads tranition table from file s**/
	private void readSample(String filename) {
		FLStringList dataTr=FLStringList.OpenFile(filename);
		for (int i=0; i<TR; i++) {
			double t= FLString.ParseDouble(dataTr.Get(i));
			transP[i]=t;
			//Macro.fPrint("%d: val:%.2f", i,t);
		}
	}

	/** reads tranition table from file s**/
	private void setRandomTransitions() {
		transP[0]=0.; 
		transP[TR-1]=1.; 
		for (int i=1; i<TR-1; i++) {
			double t= FLRandomGenerator.GetGlobalRandomizer().RandomDouble();
			transP[i]=t;
			//Macro.fPrint("%d: val:%.2f", i,t);
		}

		FLStringList transL= new FLStringList();
		for (int i=0; i<TR; i++) {
			transL.Add(""+transP[i]);
		}
		transL.WriteToFile("output-trans-randRule.txt");
	}

	/** one time step **/
	void iterate() {
		int [] src= getArray(m_time), dst= getArray(m_time+1);
		for (int i=0; i<N; i++) {
			int 
			a=src[(i+N-2)%N], b=src[(i+N-1)%N],
			c=src[(i+N+0)%N],
			d=src[(i+N+1)%N], e=src[(i+N+2)%N];

			dst[i]=makeTrans(a,b,c,d,e);
		}
		m_time++;
	}


	/** one transition **/
	final private int makeTrans(int a, int b, int c, int d, int e) {
		int t=16*a + 8*b + 4*c + 2*d + 1*e;
		double prob= transP[t];
		return Bernoulli(m_randTrans,prob);
	}

	final private int Bernoulli(FLRandomGenerator rand, double prob) {
		return rand.RandomEventDouble(prob)?1:0;
	}

	/* initialises the system **/
	void initConfig(int seed, boolean setLightConfig) {
		m_time=0;
		m_randInit.setSeedFixedAndReset(seed);
		int count=0;
		for (int i=0; i<N; i++) {
			m_stateE[i]=Bernoulli(m_randInit, .5);
			count+=m_stateE[i];
		}
		boolean isConfigLight = (2*count<N); // d < 1/2
		if (setLightConfig ^ isConfigLight) { // XOR operator
			for (int i=0; i<N; i++) {
				m_stateE[i]=1-m_stateE[i];
			}
		}
	}

	/* one mean accentuation for one init condition **/
	private double calculateMeanAccentuation(int seed, boolean setLightConfig) {
		double sum=0.;
		for (int s=0; s<SAMP; s++) {
			sum += calculateAccentuation(seed,setLightConfig,TRUN);
		}
		double meanAcc= sum/SAMP;
		Macro.fPrint(" mean acc:%.3f",meanAcc);
		return meanAcc;
	}

	/** setLightConfig controls if d<1/2 or d>1/2 is imposed 
	 * returns the mean accentuation **/
	final double runOneRule(boolean setLightConfig) {
		int seed=1502;// init start for the seeds
		double [] sample= new double[ACCSAMPLE];
		for (int r=0; r<ACCSAMPLE;r++) {
			sample[r]= calculateMeanAccentuation(seed,setLightConfig);
			seed++;
		}
		double meanAcc= MathMacro.mean(sample);
		Macro.fPrint("          RULE AVR. %.3f", meanAcc);
		return meanAcc;
	}

	/** for one init condition defined by seed
	 * calculates the difference in density after S time steps **/
	private double calculateAccentuation(int seed, boolean isLightConfig, int S) {
		initConfig(seed,isLightConfig);
		//prnConfig();
		double d0= getDensity();
		for (int t=0; t<S; t++) {
			iterate();
			//prnConfig();
		}
		double d1= getDensity();
		if (d1>0.999){
			//Macro.fPrint("all-0 reached from d:%.2f seed:%d",d0,seed);
			d1=0.999;
		}
		if (d1<0.001){
			//Macro.fPrint("all-0 reached from d:%.2f seed:%d",d0,seed);
			d1=0.001;
		}
		//double delta= (d0 > .5)? d1-d0 : d0-d1; // inversion of the sign for dini < 1/2

		if (d0>.5) {
			d0= 1.-d0; d1= 1.-d1;
		}
		double delta=1./d1-1./d0;
		//Macro.fPrint("%.2f %.2f -> %.2f", d0,d1,delta);
		return delta;
	}

	/** current density ***/
	private double getDensity() {
		int count=0;
		for (int i=0; i<N; i++) {
			count+=getArray(m_time)[i];
		}
		return count/(double)N;
	}

	private void prnConfig() {
		int [] config=getArray(m_time);
		String stab= FLString.ToString(config);
		Macro.fPrint("%d :: %s ", m_time, stab);
	}

	/* which array according to time ? **/
	private int[] getArray(int time) {
		return (time%2==0)?m_stateE:m_stateO;
	}

}
