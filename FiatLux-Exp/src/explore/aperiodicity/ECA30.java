package explore.aperiodicity;

import main.Macro;

public class ECA30 {

	private static final String INIP1="OO", INIP2 = "OX";

	private static final int DELTATPRINT = 2;// if we start not from t=0 (display only)

	static int T= 40000;//4096*16;
	
	private static void runExp() {
		PatternArray list= new PatternArray();
		Pattern p1= new Pattern(INIP1), p2= new Pattern(INIP2);
		
		//int t=0;
		list.Init(p1,p2);
		for (int t=0; t<T; t++){
			list.ComputeNextState();	
		}
		
		Macro.PrintEmphasis("hiha");
		for (int t=395; t< list.size(); t++){
			list.get(t).PrintProd(t+DELTATPRINT);
		}
		
	}

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();

	public static void main(String[] argv) {
		Macro.BeginTest(CLASSNAME);
		//CommandParser parser= new CommandParser(CLASSNAME, argv);

		runExp();

		Macro.EndTest();
	}

}
