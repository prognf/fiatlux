package explore.aperiodicity;

import java.util.ArrayList;

import components.types.FLString;
import main.Macro;

public class Pattern {

	private static final char O='O', X='X', A='A', B='B';
	private static final boolean RIGHTSIDE = false; // left or right transition ?

	enum metabole { i, h, f, b;

	public String ToStr() {
		switch (this){
		case i:
			return "=";
		case f:
			return ".";
		case h:
			return "x";
		case b:
			return "o";
		}
		return null;
	} }


	enum STATUS { DET, UNs, UNd}

	// data
	StringBuffer m_str;
	final int m_len;

	// transformed data in alphabet i,f,h,b
	ArrayList<metabole> m_metabolism= new ArrayList<metabole>();
	private STATUS m_status;

	public Pattern(String pattern) {
		m_str= new StringBuffer(pattern);
		m_len = m_str.length();
	}

	private static Pattern EmptyPattern(int len) {
		String nihil= FLString.NcopiesOfChar(len, 'E');
		return new Pattern(nihil);
	}


	public Pattern ComputeNextState(Pattern previous) {
		if ((m_status==STATUS.DET)
				||(RIGHTSIDE && (m_status==STATUS.UNs))
			){
			return nextStateNonDoubling();
		} else if (m_status==STATUS.UNd){
			return nextStateDoubling();
		} else {
			Macro.FatalError(" NON handled status" + m_status);
			return null;
		}
	}

	/** computes metabolism & status**/
	void produceMetabolismAndStatus(Pattern previousP){
		// creating metabolism
		for (int pos=0; pos < m_len; pos++){
			char a = previousP.RingCharAt(pos), b= this.CharAt(pos);
			metabole m= TransitionL(a, b);  // LEFT OR RIGHT side
			m_metabolism.add(m);
		}

		// from metabole to status
		m_status = computeStatusLeftSide();
		//Macro.fDebug(" ]]]> str:%s    status found:%s", m_str,m_status);
	}

	private Pattern nextStateDoubling() {
		char current= O;
		int groundpos=0;
		return computeNextPattern(groundpos, current, 2*m_len);
	}

	static public metabole TransitionL(char cL, char cR){
		if (cL=='O'){
			return cR=='O'?metabole.i:metabole.h;
		} else {
			return cR=='O'?metabole.f:metabole.b;
		}
	}

	static public metabole TransitionR(char cL, char cR){
		if (cL=='O'){
			return cR=='O'?metabole.i:metabole.f;
		} else {
			return cR=='O'?metabole.f:metabole.f;
		}
	}


	private Pattern nextStateNonDoubling(){
		// initialisation with the 'ground letter'
		int delta= FindDelta();
		metabole mini= GetM(delta);
		//char current= 'Z';
		char current=O;
		if (mini==metabole.b){
			current= O;
		} 
		if (mini==metabole.h){
			current= X;
		}
		int groundpos = RIGHTSIDE?0:(delta +1) % m_len;
		//Macro.fDebug(" delta %d current %s gp %d",delta,current, groundpos);
		return computeNextPattern(groundpos, current, m_len);
	}


	/** starts filling with groundpos in state chstart **/
	private Pattern computeNextPattern(int groundpos, char chstart, int len) {
		Pattern nextP= Pattern.EmptyPattern(len);
		nextP.SetChar(groundpos, chstart);

		char current= chstart;
		for (int i=0; i<len-1;i++){
			int posRead = (groundpos+i) % len;
			metabole m= GetM(posRead);
			current= Transition(m, current);
			int posWrite = (posRead+1) % len;
			nextP.SetChar(posWrite, current);
			//Macro.fDebug(" %d|%d >> %s ", i, len, current);

		}
		return nextP;

	}

	/** decide of the status of the pattern **/
	private STATUS computeStatusRightSide() {
		int nI=0, nF=0;
		for (metabole m : m_metabolism){ // we do check only for the "original" message
			if ((m==metabole.b) || (m==metabole.h)){
				return STATUS.DET;
			}
			nI += (m==metabole.i)?1:0;
			nF += (m==metabole.f)?1:0;
		}
		if (nF+nI != m_metabolism.size()){
			Macro.FatalError(" Not coherent");
		}
		return (nF%2==1)?STATUS.UNd:STATUS.UNs;
	}
	
	/** decide of the status of the pattern **/
	private STATUS computeStatusLeftSide() {
		for  (char c : m_str.toString().toCharArray()) {
			if (c!='O') {
				return STATUS.DET;
			}
		}
		return STATUS.UNd;
	}
	

	private char Transition(metabole m, char current) {
		switch(m){
		case i:
			return current;
		case h:
			return X;
		case f:
			return Flip(current);
		case b:
			return O;
		}
		return (char)Macro.ERRORint;
	}

	private char Flip(char current) {
		switch (current){
		case O:
			return X;
		case X:
			return O;
		case A:
			return B;
		case B:
			return A;
		}
		Macro.SystemWarning("state error:" + current);
		return 'Z';
	}

	/** prints the current state **/
	public void PrintProd(int t) {
		String smetab= ToStr(m_metabolism);
		if (RIGHTSIDE){
			Macro.print(""+m_str);
			Macro.print(smetab);
		} else {
		Macro.fPrint("%3d %s : %s > %s", 
				t, m_str, smetab, m_status);
		}
		//Macro.print("+----+");
	}

	private String ToStr(ArrayList<metabole> mlist) {
		StringBuilder s= new StringBuilder();
		for (metabole m : mlist){
			s.append(m.ToStr());
		}
		return s.toString();
	}

	private metabole GetM(int pos) {
		int warp= pos%m_len;
		return m_metabolism.get(warp);
	}

	String SEP=" ";

	/** finds the position of the "ground letter" (delta) 
	 * the undetermined case should not happen **/
	public int FindDelta() {
		int delta=-1;
		boolean exit= false;
		while (!exit){
			delta++;
			metabole val= GetM(delta);
			boolean undetermined = (delta == m_len ); // no ground letter 
			exit= (val==metabole.b) ||  (val==metabole.h) || undetermined;
		}
		return delta;
	}

	public boolean IsDetermined() {
		return m_status==STATUS.DET;
	}

	private char CharAt(int pos) {
		return m_str.charAt(pos);
	}

	private char RingCharAt(int pos) {
		return m_str.charAt(pos%m_len);
	}

	private void SetChar(int pos, char ch) {
		m_str.setCharAt(pos, ch);
	}

}
