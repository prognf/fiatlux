package explore.aperiodicity;

import java.util.ArrayList;

public class PatternArray extends ArrayList<Pattern> {

	public void Init(Pattern p1, Pattern p2) {
		p2.produceMetabolismAndStatus(p1);
		addNewPattern(p1,0);
		addNewPattern(p2,1);
	}

	public void ComputeNextState() {
		int t= this.size();
		Pattern pA= get(t-2);
		Pattern pB= get(t-1);
		Pattern pC= pB.ComputeNextState(pA);
		pC.produceMetabolismAndStatus(pB);
		addNewPattern(pC, t);
	}

	private void addNewPattern(Pattern p, int t) {
		add(p);
		//p.PrintProd(t);
	}

}
