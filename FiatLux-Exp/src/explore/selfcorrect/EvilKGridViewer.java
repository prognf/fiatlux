package explore.selfcorrect;

import components.types.IntC;
import grafix.gfxTypes.FLColor;
import grafix.viewers.GridViewer;

public class EvilKGridViewer extends GridViewer {

	static IntC cellPixSize=new IntC(10,1);
	
	EvilConfigurationExplore m_system;
	
	public EvilKGridViewer(int L, EvilConfigurationExplore system) {
		super(cellPixSize, new IntC(2*L,2*L));
		m_system= system;
		CreatePalette( 
				FLColor.c_black, FLColor.c_red, 
				FLColor.c_green, FLColor.c_yellow, 
				FLColor.c_white, FLColor.c_lightgrey);
	}

	@Override
	protected void DrawSystemState() {
		for (int x = 0; x < getXsize(); x++)
			for (int y = 0; y < getYsize(); y++) {
				int state = m_system.GetColor(x,y);//(x+y)%4; 
				SetColor(GetColor(state));
				if (state != 0) {
					DrawSquare(x, y);	
				}
			}
		
	}

	
	
}
