package explore.selfcorrect;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import components.types.FLString;
import components.types.FLStringList;
import components.types.IntC;
import grafix.gfxTypes.elements.FLFrame;
import main.Macro;
/*-------------------------------------------------------------------------------
- 
------------------------------------------------------------------------------*/

public class EvilConfigurationExplore {

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();


	final static int BLANK=4, CENTER=5;
	final static int L=24, M=2*L;
	private static final String PATH="src/explore/", FILEDAT = "EvilConf.dat";

	private static final int NITERSEARCHALGO = 8000;


	private static final boolean EXPLORE =false;
	
	final String SymCFG="1221030310112212200321033103031122101122003212200331032103311221030311220032101122003310321220033112210321033112200321030311220033103210112200331122103212200331122003210321033112200331032103031122003311221032101122003311220032103210200331122003310321";
	/*final String 
	CFG187="120032211113203300320021111221320331130032003302111122002132033112213003200"
			+ "3311302111122003302132033112200213003200331122130211112200331130213203311220033021"
			+ "300320033112120213012101220030",
			CFG261="1200322111132033003200211112213203311300320033021111220021320331122130032003311302"
					+ "111122003302132033112200213003200331122130211112200331130213203311220033021300"
					+ "320033112200213021111220033112213021320331122003311302130032003311220033021301"
					+ "21012200331121202130120" ;
	
	final String NCFG="120031312012032203113031200131203220120311332203120011303120322001312031133220120312001133220312032200113031203113322001312031200113322002031203220011332";
	*/

	final Integer [] COLOURS=  {0, 1, 2, 3};

	//--------------------------------------------------------------------------
	//- data structure
	//--------------------------------------------------------------------------

	class CellE {
		int colour=BLANK;
		final IntC coord;

		EnumSet<QUADRA> freeNeighb= EnumSet.allOf(QUADRA.class);
		List<Integer> unseenColours= new ArrayList<Integer>();
		public CellE(IntC coordIJ) {
			coord= coordIJ;
			unseenColours.addAll( Arrays.asList(COLOURS) ); // hack for transforming the array !
		}

		public void RemoveCol(int col) {
			unseenColours.remove(new Integer(col)); // avoid confusion with remonve(index)
		}

		public String toString(){
			return String.format("C%s",coord.toString());
		}
	}

	enum QUADRA { N(0,1), E(1,0), S(0,-1), W(-1,0);
		private int dx, dy;

		QUADRA(int x, int y){
			dx=x;
			dy=y;
		}
	};

	//--------------------------------------------------------------------------
	//- attributes
	//--------------------------------------------------------------------------


	CellE [][] m_cell = new CellE [M][M];
	ArrayList<IntC> m_cellPath; // order of cells to fill with colours

	//--------------------------------------------------------------------------
	//- construction
	//--------------------------------------------------------------------------

	/** construction **/
	EvilConfigurationExplore(){
		m_cellPath= new ArrayList<IntC>();
		//cell paths
		for (int diag=1; diag<L; diag++) {
			for (int i=0; i<diag;i++) {
				IntC pos= new IntC(i,diag-i);
				m_cellPath.add(pos);
				//Macro.fPrint(" added %s in pos : %d",pos,m_cellPath.size());
			}
		}
		//InitialiseCellArray();
	}


	/** initialisation of all cells **/
	private void InitialiseCellArray() {
		for (int j=0; j<M;j++) {
			for (int i=0; i<M;i++) {
				m_cell[i][j]=new CellE(new IntC(i-L,j-L));
			}
		}
		SetValCenter(); // special case for center cell
	}




	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------


	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------


	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------


	private void ParseConfig(StringBuffer config) {
		InitialiseCellArray();
		for (int i=0; i< config.length(); i++) {
			try {
				int color = CharToCol( config.charAt(i) );
				// Macro.fPrint(" setting %s to %d:", m_cellPath.get(i), color );
				addValFourSymmetric(m_cellPath.get(i),color);
			} catch (Exception e) {
				Macro.FatalError(e, "bad parsing : " + config);
			}
		}
	}

	private int CharToCol(char ch) {
		int color= ch - '0'; 
		if ((color<0) || (color>3)) {
			Macro.SystemWarning(" parsing error with char : "+ch);
		}
		return color;
	}


	/** auto completion of one line **/
	private void CompleteLine(int dist) {
		for (int k=1; k<=dist; k++) {
			String res= deduceNewStates();
			if (res==null) {
				makeChoice(k,dist-k);
			} else {
				FeedLine(res);
			}
		}
	}

	private void makeChoice(int i, int j) {
		int col= Macro.ERRORint;
		List<Integer> colavaible = GetCell(i, j).unseenColours;
		/*if (colavaible.size()>1) {
			int colTOAVOID= GetCell(i-1,j-1).colour;
			col = colavaible.get(0);
			if (col==colTOAVOID) {
				col = colavaible.get(1); // take second choice
			}
		} else { // only one colour available*/
		col = colavaible.get(0);
		//}
		String line= String.format("%d %d  %d   #a", i, j, col);
		FeedLine (line);
	}

	//** TODO : optimise this one by checking only the relevant cells... */
	private boolean isConfigurationValid(final boolean prnError) {
		for (int j=0; j<L-1;j++) {
			for (int i=0; i<L-1; i++) {
				if (!isCellStable(i, j, prnError)) {
					return false;
				};
			}
		}
		return true;
	}

	private void ReadInputFile() {
		FLStringList data= FLStringList.OpenFile(PATH + FILEDAT);
		for (String line : data) {
			if (line.startsWith("%")) { // breaking condition
				return;
			}
			FeedLine(line);
		}
	}

	private void FeedLine(String line) {
		Macro.print(" reading:" + line);
		if ( (line.length()>0) && //empty lines
				(line.charAt(0)!='#') ){/* line starts with # ignored*/ 
			String [] parsed= line.split("( )+");
			int 
			i= FLString.ParseInt(parsed[0]),
			j= FLString.ParseInt(parsed[1]),
			val= FLString.ParseInt(parsed[2]);
			addValFourSymmetric(i,j,val);		
		}

	}

	private void DrawState() {
		EvilKGridViewer view= new EvilKGridViewer(L, this);
		FLFrame frame= new FLFrame("text EvilK");
		frame.addPanel(view);
		frame.packAndShow();
	}


	//public static final String ANSI_YELLOW_BACKGROUND = "\u001B[43m";
	//public static final String ANSI_RESET = "\u001B[0m";



	private void PrnState() {
		for (int j=M-1; j>=0;j--) {
			String line="";
			for (int i=0; i<M;i++) {
				int val= m_cell[i][j].colour;
				String strval="";
				if (val==BLANK) {
					strval=".";
				} else if (val==CENTER) {
					strval="X";
				} else {
					strval= ""+val;
				}
				line+= strval + "";
			}
			/*if (j>L) {
				System.out.print(ANSI_YELLOW_BACKGROUND);
			} else {
				System.out.print(ANSI_RESET);
			}*/
			Macro.print(line);
		}
	}

	private void prnFreeNeighb() {
		for (int j=M-1; j>=0;j--) {
			String line="";
			for (int i=0; i<M;i++) {
				int val= m_cell[i][j].freeNeighb.size();
				String strval="";
				if (val==4) {
					strval=".";
				} else {
					strval= ""+val;
				}
				line+= strval + "";
			}

			Macro.print(line);
		}

	}

	private void prnNumUnseen() {
		for (int j=M-1; j>=0;j--) {
			String line="";
			for (int i=0; i<M;i++) {
				int val= m_cell[i][j].unseenColours.size();
				String strval="";
				if (val==4) {
					strval=".";
				} else {
					strval= ""+val;
				}
				line+= strval + "";
			}

			Macro.print(line);
		}

	}


	private void printunSeenColours() {
		for (int j=0; j<L;j++) {
			for (int i=0; i<L; i++) {
				List<Integer> colseen= GetCell(i,j).unseenColours;
				String strSeen= FLString.ToString( colseen.toArray() );
				Macro.fPrint("%d %d -> %s", i,j,strSeen);
			}
		}

	}

	/** main for adding values **/
	private void addValFourSymmetric(int i, int j, int k) {
		SetVal(i,j,k);
		/*SetVal(j,-i,k/+1);
		SetVal(-i,-j,k+2);
		SetVal(-j,i,k+3);*/
		if (i==0) {
			SetVal(j,0,k);
		}
		//SetVal(j,-i,k);
		//SetVal(-i,-j,k);
		//SetVal(-j,i,k);
	}


	/** special case **/
	private void SetValCenter() {
		GetCell(0,0).colour= CENTER;
		removeFreeNeighb(0,0);
	}

	/** general set val ; col will be taken modulo K **/
	private void SetVal(int i, int j, int col) {
		int colmod= col%4;
		GetCell(i,j).colour= colmod;
		removeFreeNeighb(i,j);
		updateSeenColours(i,j,colmod);
	}

	void addValFourSymmetric(IntC ij, int col){
		addValFourSymmetric(ij.X(), ij.Y(), col);
	}

	/** verifies if the two constraints are validated 
	 * @param prnError : prints the origin of errors **/
	private boolean isCellStable(int i, int j, boolean prnError) {
		int valC= GetCell(i, j).colour;
		// condition a)
		if ( 	(valC<4)&&(
				(GetCell(i+1, j).colour==valC)||
				(GetCell(i, j+1).colour==valC)||
				(GetCell(i-1, j).colour==valC)||
				(GetCell(i, j-1).colour==valC))) {
			if (prnError)
				Macro.fPrint("************ Neighb.-identical-state problem  with (%d,%d):%d *******",i,j,valC);
			return false;
		}
		// condition b)
		if (GetCell(i, j).freeNeighb.isEmpty() && (!GetCell(i,j).unseenColours.isEmpty())){
			if (prnError)
				Macro.fPrint("************ Non-complete neighb. in (%d,%d):%d *******",i,j,valC);
			return false;
		}
		return true;
	}

	private void updateSeenColours(int i, int j, int col) {
		GetCell(i,j).RemoveCol(col);
		GetCell(i+1,j).RemoveCol(col);
		GetCell(i,j+1).RemoveCol(col);
		GetCell(i-1,j).RemoveCol(col);
		GetCell(i,j-1).RemoveCol(col);
	}


	private void removeFreeNeighb(int i, int j) {
		GetCell(i+1,j).freeNeighb.remove(QUADRA.W);
		GetCell(i,j+1).freeNeighb.remove(QUADRA.S);
		GetCell(i-1,j).freeNeighb.remove(QUADRA.E);
		GetCell(i,j-1).freeNeighb.remove(QUADRA.N);
	}

	private CellE GetCell(int i, int j) {
		return m_cell[i+L][j+L];
	}

	/** returns a new line to feed if there is an obligation by deduction **/
	String deduceNewStates() {
		for (int j=1; j<L;j++) { // area for fixing states begins at j=1
			for (int i=0; i<L; i++) {
				int numunseen= GetCell(i,j).unseenColours.size();
				int numfreeNeighb= GetCell(i, j).freeNeighb.size();
				if ((numunseen==1) && (numfreeNeighb==1)) {
					String strcol= ""+ GetCell(i,j).unseenColours.get(0);
					QUADRA dir= UniqueElementOfS(GetCell(i, j).freeNeighb);
					int newi= i + dir.dx, newj = j + dir.dy;
					return String.format("%d %d  %s # fixed state (%d,%d) %s to %s",
							newi, newj, strcol, i,j,dir,strcol);
				}
			}
		}//
		return null;
	}

	<X extends Object> X UniqueElementOfS(Set<X> singleton) {
		if (singleton.size()!=1) {
			Macro.FatalError("Unique element called with a set which is not a singleton");
		}
		Iterator<X> iter= singleton.iterator();
		return iter.next();	
	}

	public static void main(String[] args) {
		EvilConfigurationExplore exp = new EvilConfigurationExplore();
		exp.Test();
	}


	/** FIRST RESEARCH ALGO **/
	void DepthFirstSearch() {
		StringBuffer testconfig= new StringBuffer("120");
		int maxlen=0;
		for( int iter=0; iter<NITERSEARCHALGO; iter++) {
			ParseConfig(testconfig);
			//PrnState();
			boolean valid= isConfigurationValid(false);
			if (valid && (testconfig.length()>maxlen)) {
				maxlen= testconfig.length();
				//OLD : deduction String newstate= deduceNewStates();
				//if (newstate==null) newstate="@";
				Macro.fPrint("len:%d C:%s",
						maxlen, testconfig);
			}
			// next step
			if (isConfigurationValid(false)) {
				testconfig.append("0");
			} else {
				Increment(testconfig);
			}
		}
	}



	private void Increment(StringBuffer config) {
		int endindex= config.length() - 1;
		int collast= CharToCol( config.charAt( endindex) );
		if (collast<3) {
			char newcolor=(char) ('0'+(collast+1));
			config.setCharAt(endindex, newcolor);
		} else {
			config.deleteCharAt(endindex);
			Increment(config);
		}

	}


private void Test() {
		System.out.println("Working Directory = " +
				System.getProperty("user.dir"));
		//ReadInputFile();
		//ProcessConfig(TESTCONFIG);
		//Macro.LineForSeparation();
		//ProcessConfig(TESTCONFIG+"30201123");
		// new auto update
		//int dist=3;
		/*CompleteLine(3);
		CompleteLine(4);
		CompleteLine(5);*/


		/*prnFreeNeighb();
		Macro.LineForSeparation();
		prnNumUnseen();
		Macro.LineForSeparation();
		Macro.LineForSeparation();*/
		if (EXPLORE) {
			DepthFirstSearch();
		} else {
			ProcessConfig(new StringBuffer(SymCFG));
		}
	}


	private void ProcessConfig(StringBuffer config) {
		ParseConfig(config);
		Macro.fPrint("C:"+config);
		Macro.fPrint("validity:%s",isConfigurationValid(true));
		PrnState();
		DrawState();
	}


	public int GetColor(int x, int y) {
		return m_cell[x][y].colour;
	}



}
