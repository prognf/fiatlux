package explore.selfcorrect;

import components.types.IntC;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.viewers.RegularAutomatonViewer;

public class ConstructEvil4Viewer extends RegularAutomatonViewer {

	private ConstructEvil4 m_system;
	FLColor [] COL={ 
			FLColor.c_white, 
			FLColor.c_yellow, FLColor.c_red, 
			FLColor.c_brown, FLColor.c_darkgreen, FLColor.c_cyan};

	public ConstructEvil4Viewer(ConstructEvil4 system, IntC in_CellPixSize, IntC in_Gridsize) {
		super(in_CellPixSize, in_Gridsize);
		m_system = system;
		PaintToolKit palette= new PaintToolKit(COL);
		SetPalette(palette);
		m_StabilityDisplay= true;
	}

	@Override
	public int GetCellColorNumXY(int x, int y) {
		return m_system.GetState(x,y);
	}

	@Override
	protected boolean IsStableXY(int x, int y) {
		return m_system.isFine(x,y);
	}

	/**
	 * Override this if post drawing is needed
	 * @param gc the Graphics2D
	 */
	protected void DrawPostEffect() {
		for (int x=0; x < getXsize(); x++){
			for (int y=0; y< getYsize(); y++){
				int Xpos = getXpos(x) + m_CellDist/2;
				int Ypos = getYpos(y) + m_CellDist/2;
				String state= ""+ (m_system.GetState(x,y)-1);
				m_g2D.drawString(state, Xpos, Ypos);
			}	
		}
	}

}
