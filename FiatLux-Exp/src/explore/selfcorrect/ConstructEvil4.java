package explore.selfcorrect;

import components.arrays.GridSystem;
import components.types.IntC;
import components.types.IntegerList;
import grafix.gfxTypes.elements.FLFrame;
import main.Macro;

public class ConstructEvil4 extends GridSystem {

	private static final int L = 10;
	private static final IntC XYsize = new IntC(L,L);
	private static final int CELLPIXSIZE = 30;

	int [][] m_state= new int[L][L];

	public ConstructEvil4() {
		super(XYsize);
		Init();
	}

	public void Init() {
		int X= L;
		int Y= L;
		for (int y=1; y<Y; y++) {
			int state= ((y-1)%4)+1;
			SetStateXY(0, y, state);
		}
		for (int x=1; x<X; x++) {
			int state= ((x)%4)+1;
			SetStateXY(x, 0, state);
		}
		// cell (1,1°
		SetStateXY(1,1,4);
	}

	private void SetStateXY(int x, int y, int state) {
		m_state[x][y]= state;
	}

	public int GetState(int x, int y) {
		return m_state[x][y];
	}

	/** local transition rule */
	int LocalRule(int x, int y) {
		int C= GetState(x,y);
		if (C>0) 
			return C;
		int W= GetState(x-1,y);
		int S= GetState(x,y-1);
		int D= GetState(x-1,y-1);

		//C==0
		IntegerList absentStates= IntegerList.NintegersFromOne(4);
		absentStates.RemoveItem(D);
		absentStates.RemoveItem(S);
		absentStates.RemoveItem(W);
		//present.remove(new Integer(0));
		if (absentStates.size()==1) {
			return absentStates.Get(0);
		} else if (S==W) {
			return expedia(D,S) + 1;
			//return absentStates.ChooseOneAtRandom(this.GetRandomizer());
		} else {
			return C;
		}
	}

	/** D: diagonal (SW, center of the growth ; E  :e external (=WEST & SOUTH) **/
	private int expedia(int Dp, int Ep) {
		int D= Dp -1, E=Ep-1;
		//Macro.fPrint("handling D:%d E:%d", D,E);
		if ((D==1) && (E==2))
				return 0;
		if ((D==0) && (E==1))
			return 0;
		if ((D==3) && (E==2))
			return 1;
		if ((D==2) && (E==3))
			return 0;
		if ((D==2) && (E==1))
			return 0;	
		if ((D==3) && (E==0))
			return 1;
		Macro.fPrint(" unhandled D:%d E:%d", D,E);
		return 0;
	}

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();

	private static final IntC CellPixSize = new IntC(CELLPIXSIZE,1);
	public static void main(String[] argv) {
		Macro.BeginTest(CLASSNAME);
		FLFrame frame= new FLFrame("Test Evil 4");
		ConstructEvil4 sys= new ConstructEvil4();
		ConstructEvil4Viewer viewer= new ConstructEvil4Viewer(sys, CellPixSize, XYsize);
		frame.add(viewer);

		sys.OneStep();

		frame.packAndShow();

		Macro.EndTest();
	}

	private void OneStep() {
		for (int x=1; x<L; x++) {
			for (int y=1; y<L;y++) {
				ApplyLocalRule(x, y);
			}
		}

	}

	private void ApplyLocalRule(int x, int y) {
		int state= LocalRule(x, y);
		SetStateXY(x, y, state);
	}

	// says if it respects the 4-col constraint
	public boolean isFine(int x, int y) {
		if ((x==0)||(y==0))
			return true;
		if ((x==L-1)||(y==L-1))
			return true;
		
		IntegerList cols= IntegerList.NintegersFromOne(4);
		int C= GetState(x,y);
		int W= GetState(x-1,y);
		int S= GetState(x,y-1);
		int E= GetState(x+1,y);
		int N= GetState(x,y+1);
		cols.RemoveItem(C);
		cols.RemoveItem(W);
		cols.RemoveItem(S);
		cols.RemoveItem(E);
		cols.RemoveItem(N);
		
		return cols.isEmpty();
	}


}
