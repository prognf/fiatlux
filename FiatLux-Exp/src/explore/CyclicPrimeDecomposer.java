package explore;

import java.util.ArrayList;

import main.Macro;

import components.types.IntegerList;

public class CyclicPrimeDecomposer {


	private static final int LIM = 40000000, SROOT= (int)Math.sqrt(LIM);
	private static final int LIM_ANALYSE = LIM;
	private ArrayList<Counter> m_pascaline;
	IntegerList m_factorList;
	int m_rest; // current rest
	IntegerList m_multiplicities;

	void TestPrimes(){
		IntegerList primes = NombresParfaits.FindPrimes(SROOT);
		int szPrimes= primes.size();
		m_pascaline= new ArrayList<Counter>(szPrimes);
		for (int primeNumber : primes ){
			m_pascaline.add( new Counter(primeNumber));
		}
		
		// testing numbers and printing results
		for (int nbTst=2; nbTst <= LIM_ANALYSE; nbTst++){

			m_factorList= new IntegerList();
			m_rest= nbTst;
			DistributeSignal();	
			long sum= SumDivisors();
			//prn = tst==33550336;
			long netsum = sum - nbTst;
			long diff= netsum - nbTst;
		
			boolean prn=false;
			prn = prn || ((0<=diff) && (diff<=4));
			
			if (prn){

				StringBuilder decomposition= new StringBuilder(":");
				int szFactNum = m_factorList.GetSize();
				for (int pos=0; pos < szFactNum; pos++){
					int fact = m_factorList.Get(pos);
					int mult= m_multiplicities.Get(pos);
					decomposition.append(fact).append((mult > 0) ? ("^" + (mult + 1)) : "").append(":");
				}
				String s = 
						String.format(" %d>%d (%d)  %d>>%d %s",
								nbTst,netsum,diff,
								2*nbTst,sum, decomposition.toString());
					Macro.print(s );
			}

		}
	}

	public long SumDivisors(){
		long sum=1; // implicit divisor
		for (int pos=0; pos < m_factorList.GetSize(); pos++){
			int exponent = m_multiplicities.Get(pos)+1;
			long val= m_factorList.Get(pos);
			long newval= sum * Series(val,exponent); 
			sum += newval;
		}
		return sum;
	}

	/** sums of the power of x by recurrence **/
	private long Series(long x, int exponent) {
		return (exponent==1)?x:x*(Series(x, exponent-1)+1);
	}


	/** distribute signal and tics */
	private void DistributeSignal() {
		for (Counter counter : m_pascaline){
			counter.Tic();
		}
		int numFact= m_factorList.GetSize();
		m_multiplicities= new IntegerList(numFact,0);
		for (int pos=0 ; pos < numFact; pos++){
			int fact= m_factorList.Get(pos);
			boolean loop= m_rest!=1; 
			while (loop){
				loop= (m_rest % fact ==0);
				if (loop){
					m_multiplicities.IncrementValAtPos(pos);
					m_rest/= fact;
				}
			}
		}
		if (m_rest!=1){
			m_factorList.addVal(m_rest);
			m_multiplicities.addVal(0);
		}
	}

	class Counter {
		int m_val; // val of the prime number
		int m_angle;

		Counter(int val){
			m_val= val;
			m_angle=1;
		}

		public void Tic() {
			m_angle++;
			if (m_angle==m_val){
				m_angle=0;
				m_factorList.add(m_val);
				m_rest/= m_val;
			}
		}

	}


	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();
	public static void main(String[] argv) {
		Macro.BeginTest(CLASSNAME);
		Macro.print(" biggest int" + Integer.MAX_VALUE);
		CyclicPrimeDecomposer exp = new CyclicPrimeDecomposer();
		exp.TestPrimes();
		Macro.EndTest();
	}



}
