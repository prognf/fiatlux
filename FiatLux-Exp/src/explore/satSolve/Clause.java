package explore.satSolve;

import components.types.IntegerList;

/*
 * a clause is a conjunction of atoms
 */
class Clause {

	private static final String SEPCMNT= "" ;

	final private IntegerList m_atom; // atoms of the clause 
	private String m_comment="";

	/** constructs a simple clause with one var **/
	Clause (Integer varNum){
		this();
		m_atom.addVal(varNum);
	}

	Clause (){
		m_atom= new IntegerList();
	}

	/** copy of a clause **/
	public Clause(Clause clause) {
		this();
		AddOr(clause);
		m_comment ="";//+= clause.m_comment;
	}

	public String toString(){
		return m_atom.toString();
	}

	public String GetVal() {
		return m_atom.toString();
	}


	/** adding an atom to a clause **/
	void AddAtom(Integer var) {
		m_atom.addVal(var);	
	}

	/** adds another clause with an OR (on the left) **/
	void AddOr(Clause clause){
		for (Integer var: clause.m_atom){
			AddAtom(var);
		}
		m_comment += SEPCMNT + clause.GetComment();
	}

	/** adds another clause with an OR (on the right) **/
	void AddOrRight(Clause clause){
		for (Integer var: clause.m_atom){
			m_atom.addVal(var);
		}
		m_comment = clause.GetComment() + SEPCMNT + m_comment;
	}

	public void SetComment(String comment){
		m_comment= comment + " " +SEPCMNT;
	}

	public String GetComment() {
		return m_comment;
	}



	/** "Or" operation on a sequence of SVARS **/
	static Clause FormClause(Integer ... clauseSeq) {
		Clause clause = new Clause();
		for (Integer read : clauseSeq){
			clause.AddAtom(read);
		}
		return clause;
	}


	static String cnf; // DANGER !! memory optim !!

	/** final conversion to CNF **/
	public String toCNF() {
		cnf= "";
		for (Integer varNum : m_atom){
			cnf+= varNum + " "; 
		}
		cnf += " 0";
		if (SatGen.WriteComment){
			cnf += " c " + m_comment;
		}
		return cnf;
	}



} 