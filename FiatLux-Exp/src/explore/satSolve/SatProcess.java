package explore.satSolve;

import main.Macro;

import components.types.FLString;
import components.types.FLStringList;

public class SatProcess {

	
	
	/**
	 * processes current solution
	 */
	static public void ReadSolution(String filenameIn, String filenameOut) {
		FLStringList solRead= new FLStringList(filenameIn);
		if (solRead.size() <= 1){
			FLStringList outsol = new FLStringList();
			outsol.Add("nihil oualou nada");
			outsol.WriteToFile(filenameOut);
		} else {
			String solution = solRead.Get(1);
			ProcessSolution(solution);
		}		
	}
	
	
	/** puts solution in human-readable form **/
	static private void ProcessSolution(String data) {
	
		FLStringList intparse= FLString.ParseString(data);

		// generating the transition table
		int tabSize= SatGenQ3.SZTRANSTAB;
		int [] solution= new int[tabSize]; 
		for (int pos=0; pos < tabSize; pos++){
			int 
				readIa= FLString.ParseInt( intparse.Get(2 * pos)   ),
				readIb= FLString.ParseInt( intparse.Get(2 * pos+1) );
			boolean vA= readIa > 0 , vB= readIb > 0 ;
			solution[pos] = SatGenQ3.TwoBoolCodeToState(vA, vB);
			//String spos= SatGenQ3.DecimalToSTrans(pos);
			//Macro.fDebug("trans: %s -> %d", spos, solution[pos]);
			
		}
		// output with all transitions stacked
		StringBuilder tableOutCollect = new StringBuilder();
		for (int pos=0; pos < tabSize; pos++){
			tableOutCollect.append(solution[pos]);//solution[tabSize -1 - pos ];
		}
		Macro.print(" RULE TAB :" + tableOutCollect);
	
	}	
}
