package explore.satSolve;

import java.util.ArrayList;

import main.Macro;

class Formula extends  ArrayList<Clause> {


	
	public Formula(){
		
	}
	
	
	public void AddClause(Clause clause) {
		this.add(clause);				
	}

	public void AddClause(Clause clause,String comment) {
		clause.SetComment(comment);
		this.add(clause);				
	}

	/** adds a sequence of clauses **/
	public void AddClause(Clause ... clauseList) {
		for (Clause c : clauseList){
			AddClause(c);
		}
	}	

	

	public Formula Distribute(Clause clause) {
		for (Clause read : this){
			read.AddOrRight(clause);
		}
		return null;
	}

	public void Print() {
		for (Clause clause : this){
			Macro.print("[ " + clause + " ]");
		}
	}

	static Clause Or(Clause clause1, Clause clause2) {
		Clause niou= new Clause(clause1);
		niou.AddOr(clause2);
		return niou;
	}

	static Formula DistributeC(Clause elem, Formula formula) {
		Formula niou = new Formula();
		for (Clause cl: formula){
			niou.AddClause(Formula.Or(elem,cl));
		}
		return niou;
	}

	static Formula Distribute(Formula f1, Formula formula) {
		Formula niou = new Formula();
		for (Clause elem : f1){
			Formula dist= Formula.DistributeC(elem, formula);
			niou.AddFormula(dist);
		}
		return niou;
	}

	static Formula Diff(Variable v1, Variable v2){
		Formula f = new Formula();
		String comment = "<>" ;
		f.AddClause(Clause.FormClause(v1.Pos(),v2.Pos()),comment);
		f.AddClause(Clause.FormClause(v1.Neg(), v2.Neg()),comment);
		return f;
	}

	/** Formula that states that two vars are equal **/
	static Formula Equal(Variable v1, Variable v2){
		Formula f = new Formula();
		//String comment = v1.GetVal() + "=" + v2.GetVal();
		f.AddClause(Clause.FormClause(v1.Pos(),v2.Neg()));
		f.AddClause(Clause.FormClause(v1.Neg(), v2.Pos()));
		return f;
	}

	/** adds a sequence of clauses **/
	public void AddFormula(Formula f) {
		for (Clause cl: f){
			AddClause(cl);
		}			
	}


}
