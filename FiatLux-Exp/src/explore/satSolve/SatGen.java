package explore.satSolve;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import main.Macro;
import main.MathMacro;
import main.commands.CommandParser;

import components.types.FLString;
import components.types.FLStringList;
import components.types.IntegerList;
import components.types.RuleCode;

/*-------------------------------------------------------------------------------
 * MAIN CLASS for the global synch problem
 ------------------------------------------------------------------------------*/
public class SatGen {

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();

	// do we write the comments in the CNF file ?
	public static final boolean WriteComment = false;

	/*
	 * PARAMETERS FOR RUNNING THE EXPERIMENT 
	 */

	// number of transitions

	// for init. cond generation
	static IntegerList SIZEARRAY= IntegerList.Interval(2,11);

	//other constants 
	private static final boolean SIMPLEPRINTSOL = true;

	//filenames
	private static final String FILETSTSATSOLVE = "satSolve.cnf";
	static final String SOLUTION_SAT = "solution.cnf";
	static final String SOLUTION_TXT = "rule.txt";
	private static final String FILEFOUNDSOLUTIONS = "found-solutions.txt", FILERULE="rule.txt";
	private static final String ICFILE = "IC.dat";

	//	private static final boolean AUTOLAUNCH = false; // automatic launch of the solver
	public static final String CODEPREFIX = "rule";
	private static final String codeC = "c"; // for "c(i,t)"
	private static final String CMDSATSOLVER = "./minisat ";
	private static final String CMDREPLACE = "Replace", CMDREPLACEB = " "; //" with replace use " --";



	private static final String NonQuiescence = "non-quiescence";
	private static final String HOMOGENEOUSFINALSTATE = " homog. final st.";


	/*----------------------------------------------------------------------------
	 - attributes
	 --------------------------------------------------------------------------*/
	int m_nbits = -99; // number of bits of the transition rule
	int m_deltapos = -999; // leftmost index of the neighbourhood

	int m_ntrans;

	int m_size= -1;
	int m_varCount= 0;
	VarTable m_tabC, m_tabT; 



	private int m_initCond=-1; 


	private ArrayList<int []> m_BinTab; // transition table
	private Store m_Store;
	/*---------------------------------------------------------------------------
	- construction
	--------------------------------------------------------------------------*/
	SatGen(int nbits){
		m_nbits= nbits;
		m_deltapos=-(m_nbits-1)/2;
		m_ntrans = Ntrans(nbits);
		m_tabT= new VarTable();
		m_tabC= new VarTable(); 
		CreateVarTransitions();
		m_BinTab= new ArrayList<int[]>(m_ntrans);
		for( int trans=0; trans < m_ntrans; trans ++){
			int [] binT = MathMacro.Decimal2BinaryTab(trans, nbits);
			m_BinTab.add(binT);
		}		

	}

	/** creates variables relative to initial cond 
	 * Nsize is the size of init. cond. ; Tsize : time horizon**/
	private void CreateCikVarsForNewInitialCond(int Nsize, int Tsize) {
		m_size= Nsize;
		m_initCond++;
		m_Store.AddInitCondSize(Nsize);
		m_tabC= new VarTable(); // we reuse tabC 
		// Cik
		for (int k=0; k<Tsize; k++){
			for (int i=0; i<m_size; i++){
				String varName= VarNameC(m_initCond, i,k);
				m_varCount++;
				m_tabC.AddVariable(varName, m_varCount);
			}
		}
	}


	final static int [] TIMESYNCH_HORIZON=
		{ 1, 2, 4, 6, 12, 18, 34, 58, 106, 186, 250, 630, 1180, 2190, 4114}; 

	/** this is the maximum synch time associated to a given ring size
	 * the time horizon should not be greater than this value 
	 * the number of lines of the sat conditions is the time horizon +1 */
	private int GetTimeSynchHorizon(int nsize) {
		int index= nsize+2;
		if (index>=TIMESYNCH_HORIZON.length){
			Macro.SystemWarning("init cond. with size greater than 16, time horizon not set properly");
			int Tsynch= MathMacro.TwoToPowerIntResult(nsize);
			return Tsynch;
		}
		return TIMESYNCH_HORIZON[nsize];
	}

	private void CreateVarTransitions() {
		// transitions 
		for (int i=0; i< m_ntrans; i++){
			String varName = Variable.VarTransition(i);
			m_varCount++;
			m_tabT.AddVariable(varName,m_varCount);			
		}		
	}

	private Variable GetVarTransition(int trans) {
		String key= Variable.VarTransition(trans); 
		Variable v= m_tabT.GetVarByKey(key);
		if (v == null){
			Macro.SystemWarning(" null var " );
		}
		return v;
	}

	private Variable GetVarC(int i, int k) {
		String key= VarNameC(m_initCond, i, k); 
		Variable v= m_tabC.GetVarByKey(key);
		if (v == null){
			String msg = String.format(" cond:%d i:%d k:%d", m_initCond,i,k);
			Macro.SystemWarning(msg);
		}
		return v;
	}

	/*---------------------------------------------------------------------------
	  - I/O
	  --------------------------------------------------------------------------*/

	static private void LaunchSATsolver(String filename) {
		String cmd = CMDSATSOLVER + filename + " " + SOLUTION_SAT;
		Macro.ExecuteSystemCommand(cmd);		
	}

	/** generate CNF file of the problem 
	 * from ICFILE to FILETSTSATSOLVE 		**/
	void generateSATsolveFile(int Tsize){
		ProcessInitialConditions(Tsize);
	}

	private void ProcessInitialConditions(int Tsize) {
		Macro.print(2, " generating the formula...");
		m_Store = new Store(FILETSTSATSOLVE);
		AddCondNoQuescientState();
		try{
			FLStringList initCondList = FLStringList.ReadFile(ICFILE);
			for (String ic : initCondList){
				AddCondForOneInitialCondition(ic, Tsize);		
			}
		} catch (Exception e){
			Macro.FatalError("Error when reading file : " + ICFILE+ " check existence");
		}
		m_Store.Close();
		UpdateVariables();
	}

	private void UpdateVariables() {
		// updating variables with the system's replace command
		Macro.print(" Used " + GetNumVar() + " variables " + m_Store.m_nclause + " clauses.");
		String cmd1 = CMDREPLACE + " NVAR " + GetNumVar() + CMDREPLACEB + FILETSTSATSOLVE;
		String cmd2 = CMDREPLACE + " NCLAUSES " + m_Store.m_nclause + CMDREPLACEB + FILETSTSATSOLVE;
		Macro.ExecuteSystemCommand(cmd1);
		Macro.ExecuteSystemCommand(cmd2);
	}
	// updating variables with the system's replace command
	/*Macro.print(" Used " + GetNumVar() + " variables " + m_Store.m_nclause + " clauses.");
			String cmd1 = "replace NVAR " + GetNumVar() + " -- " + FILETSTSATSOLVE;
			String cmd2 = "replace NCLAUSES " + m_Store.m_nclause + " -- " + FILETSTSATSOLVE;
			Macro.ExecuteSystemCommand(cmd1);
			Macro.ExecuteSystemCommand(cmd2);*/


	/*---------------------------------------------------------------------------
	- CNF conditions
	-------------------------------------------------------------------------*/

	/** bitArray describes the init cond. ; Tmax is the maximum synch. time **/
	private void AddCondForOneInitialCondition(final String bitArray, int Tmax) {
		int size = bitArray.length();

		//the number of lines of the sat conditions is the time horizon +1
		int Tsynch= GetTimeSynchHorizon(size) + 1;
		if (Tsynch>Tmax){
			Macro.fPrint(" initial condition of size %d," +
					" setting synch. time horizon to %d", Tsynch, Tmax);
			Tsynch = Tmax + 1; 
		}
		Macro.fPrint(" IC> sz: %2d ; nlines=tmax+1: %2d ; MEM: %s ",size, Tsynch, Macro.GetMemoryState());		

		CreateCikVarsForNewInitialCond(size, Tsynch);
		// conditions
		AddCondInitialState(bitArray);
		/*AddCondNoRotation(bitArray,0);
		AddCondNoRotation(bitArray,1);
		AddCondNoRotation(bitArray,2);
		AddCondNoRotation(bitArray,-1);*/
		AddCondHomogeneousFinalState(Tsynch);
		AddCondConsistencyConditions(Tsynch);					
	}

	private void AddCondInitialState(final String bitArray) {
		String comment= "IC#" + m_initCond;
		for (int i=0; i < m_size; i++){
			// k=0
			int t=0; 
			char bit=bitArray.charAt(i);
			Integer I= (bit=='1')?GetVarC(i, t).Pos():GetVarC(i, t).Neg();
			Clause clause= new Clause(I);
			clause.SetComment(comment  + String.format("C(%d,%d)=%s",i,t,bit));
			m_Store.InsertClause(clause);
		}
	}

	/** no translation of size delta **/
	private void AddCondNoRotation(final String bitArray, int delta) {
		String comment= "IC#" + m_initCond;
		Clause clause= new Clause();

		for (int i=0; i < m_size; i++){
			int pos= (i + m_size +delta) % m_size;//translation
			int t=1; // inverting each bit
			char bit=bitArray.charAt(i);
			Integer I= (bit=='1')?GetVarC(pos, t).Neg():GetVarC(pos, t).Pos();
			clause.AddAtom(I);
		}
		clause.SetComment(comment  + String.format("anti-rot. %d",delta));
		m_Store.InsertClause(clause);
	}

	/** 0 and 1 not quiescent **/
	private void AddCondNoQuescientState() {
		SetRuleTransition(0, 1,NonQuiescence);
		SetRuleTransition(m_ntrans - 1, 0,NonQuiescence);
	}

	/*---------------------------------------------------------------------------
	- "coherence" condition
	-------------------------------------------------------------------------*/

	/** adds to f coherence conditions **/
	private void AddCondConsistencyConditions(int Tsize) {
		for (int k=0; k < Tsize - 1; k++){
			for (int pos= 0 ; pos < m_size ; pos ++){
				EvolveConditionCik(pos, k);
			}
		}
	}


	/** adds to f coherence condition on cell i time k to k+ 1 **/
	private void EvolveConditionCik(int i, int k) {
		for (int trans= 0 ; trans < m_ntrans ; trans ++){
			OneEvolveCond(i, k, trans);	
		}
	}

	/** coherence condition from cell i from time k to k+1 for transition trans **/
	void OneEvolveCond(int i, int k, int trans){

		Formula equal=  Formula.Equal(GetVarC(i,k+1),GetVarTransition(trans));

		Clause clause= new Clause();
		for (int ptrn=0; ptrn < m_nbits; ptrn++){
			int bit = 1 - GetBitTrans(trans,ptrn);
			int j= i + m_deltapos +ptrn;  // DELTAPOS is negative !!!
			clause.AddAtom( ValCik(j,k,bit) );
		}
		clause.SetComment(String.format("T[%d,%d]=t%d",i,k,trans));
		equal.Distribute(clause);
		m_Store.AddFormula( equal );
	}



	/** returns the bit corresponding to the transition trans in pos ptrn **/
	private int GetBitTrans(int trans, int ptrn) {
		int [] binT = m_BinTab.get(trans);  
		return binT[binT.length -1 - ptrn];
	}

	final private Integer ValCik(int i, int k, int bit) {
		return (bit==0)?GetVarC(i,k).Neg():GetVarC(i,k).Pos();
	}		




	/*---------------------------------------------------------------------------
	- final state conditions
	-------------------------------------------------------------------------*/

	/** final configuration is homogeneous 
	 * devlops into all combinations of [a_i or (not a_j)] with i <> j**/
	void AddCondHomogeneousFinalState(int Tsize){
		int k= Tsize - 1;
		for (int i=0; i< m_size; i++){
			for (int j=0; j< m_size; j++){
				if (i!=j){
					Clause c= new Clause (GetVarC(i, k).Pos());
					c.AddAtom( GetVarC(j, k).Neg() );
					c.SetComment(HOMOGENEOUSFINALSTATE);
					m_Store.InsertClause(c);
				}
			}			
		}				
	}

	/*---------------------------------------------------------------------------
	- get & set
	--------------------------------------------------------------------------*/

	/** human readable **/
	String VarNameC(int cond, int i, int k){
		i = (i + m_size) % m_size ;		
		return cond + codeC + i + k ;
	}

	int GetNumVar(){
		return m_varCount;
	}

	/*---------------------------------------------------------------------------
	- special ?
	-------------------------------------------------------------------------*/

	/** setting the rule to a particular value **/
	private void SetRule(int Wcode) {
		int bit [] = MathMacro.Decimal2BinaryTab(Wcode, m_ntrans);
		for (int trans=0; trans < m_ntrans ; trans++){
			SetRuleTransition(trans, bit[trans]," (forcing a given rule code)");
		}
	}

	/** setting the rule to a particular value **/
	private void SetRuleTransition(int trans, int bit, String comment) {
		Integer I=(bit==1)?GetVarTransition(trans).Pos():GetVarTransition(trans).Neg();
		Clause T= new Clause(I);
		T.SetComment(" t"+ trans + "=" + bit + " " + comment);
		m_Store.InsertClause(T);
	}

	/*	private void AddCondT2T5() {
		SetRuleTransition(2, 1);
		SetRuleTransition(5, 1);
	}*/

	/*---------------------------------------------------------------------------
	- static & co.
	--------------------------------------------------------------------------*/


	private void testEvolve() {
		m_Store= new Store("testEvolve");
		CreateCikVarsForNewInitialCond(3,5);
		OneEvolveCond(1, 0, 1);
		m_Store.Close();		
	}

	/** source for all the initial conditions which are a "representative"
	 * that is, non-equivalent by rotations
	 */
	static private void GenerateInitialConditions(IntegerList sizeList) {
		FLStringList initCond= new FLStringList(); // list of IC to construct
		for (int nlen : sizeList) 	{
			initCond.addAll( MathMacro.GenerateNonEquivalentBinWords(nlen));	
		}
		initCond.WriteToFile(ICFILE);
	}

	
	/** source for all the initial conditions which are a "representative"
	 * that is, non-equivalent by rotations
	 */
	static private void GenerateInitialConditionsBlackNWhite(int len) {
		FLStringList initCond= new FLStringList(); // list of IC to construct
		for (int i=1; i<=len;i++) 	{
				String s= FLString.NcopiesOfChar(i, '0');
				s+= FLString.NcopiesOfChar(i, '1');
				initCond.Add(s);
		}
		initCond.WriteToFile(ICFILE);
	}


	private static void FilterRules(int nbits) {
		/*IntegerList ruleList= IntegerList.ReadFromFile(FILEFOUNDSOLUTIONS);
		ruleList.SortBubble();
		ruleList.WriteToFile(FILEFOUNDSOLUTIONS);*/
		try{
			FLStringList ruleList= FLStringList.ReadFile(FILEFOUNDSOLUTIONS);
			ArrayList<RuleCode> sortL= new ArrayList<RuleCode>();
			Map<RuleCode, String> codeFamily= new HashMap<RuleCode, String>();
			
			
			for (String line: ruleList){//for each line
				String [] item= line.split(" ");
				long Wcode= FLString.ParseLong(item[0]);
				RuleCode R= new RuleCode(Wcode);
				codeFamily.put(R, item[2]);
				sortL.add(R);
			}
			Collections.sort(sortL);
			
			// printing families
			StringBuilder concat= new StringBuilder();
			for (RuleCode R: sortL){
				concat.append(R).append(" ");
				String str= R.toString() ;//+ " >-> " + RuleCode.GetSymmetrics(R, nbits);
				str+= " ]-> " + R.GetCanonicalSymmetricsAsString(nbits);
				Macro.print(str);
			}
			// printing list of M.R
			Macro.print("MRLIST: " + concat);
			
		} catch (Exception e) {
			Macro.FatalError(" problem with file" + FILEFOUNDSOLUTIONS);
		}

	}


	/** used for the compilation of found rules
	 * uses symmetries in order not search again for the symmetric rules
	 */
	private static void ConvertRuleForIterating(String satFile, String compilRuleFile, int nbits) {
		FLStringList rule= FLStringList.OpenFile(FILERULE);
		String sWcode= rule.Get(0);
		RuleCode W= RuleCode.FromString(sWcode);
		
		ArrayList<RuleCode> family= RuleCode.GetSymmetrics(W, nbits);

		for (RuleCode toexclude : family){ // ruling out all the symmetrics
			Exclude(toexclude, satFile, nbits);
		}
		
		String str= MathMacro.FindMin(family).toString();
		str+= "<<MR--family>> " + MathMacro.ArrayToStringWithFormatting(family.toArray());
		FLStringList.AppendToFile(compilRuleFile, str);
	}

	public static int Ntrans(int nbits) {
		return MathMacro.TwoToPowerIntResult(nbits);
	}

	private static void Exclude(RuleCode W, String satFile, int nbits) {
		int ntrans= Ntrans(nbits);
		//Macro.Debug(" dealing with solution W=" + W);
		// CNF coding of transition table var.
		int [] bitarray = W.ToBinaryArray(ntrans);
		StringBuilder out= new StringBuilder();
		for (int i=0; i< ntrans; i++){
			out.append((1 - 2 * bitarray[i]) * (i + 1)).append(" "); // from 0/1 to -1/1
		}
		out.append("0");
		out.append(" c exclusion of rule ").append(W);
		Macro.print(out.toString());
		// appending info in file
		FLStringList.AppendToFile(satFile, out.toString());
	}


	/**
	 * processes current solution
	 */
	static public void ReadSolution(int nbits, String CNFfilenameIn, String filenameOut) {
		Macro.print("Reading file:" + CNFfilenameIn);
		FLStringList solRead= new FLStringList(CNFfilenameIn);
		if (solRead.size() <= 1){
			FLStringList outsol = new FLStringList();
			outsol.Add(" nihil oualou nada");
			outsol.WriteToFile(filenameOut);
		} else {
			String solution = solRead.Get(1);
			int ntrans= Ntrans(nbits);
			ProcessSolution(solution, SIMPLEPRINTSOL, ntrans);
		}		
	}

	/** puts solution in human-readable form **/
	static private void ProcessSolution(String data, boolean simplePrint, int ntrans) {
		FLStringList intparse= FLString.ParseString(data);
		int varSz= intparse.GetSize() - 1; // should be equal to number of variables

		// generating the "truth table"
		int [] solution= new int[varSz]; 
		for (int pos=0; pos < varSz; pos++){
			int readI= FLString.ParseInt( intparse.Get(pos) );
			solution[pos] = (readI > 0)?1:0;
			if (readI < 0){
				readI= - readI;
			} 
		}

		// output of the solution
		FLStringList outsol= new FLStringList();
		int [] transTable= Arrays.copyOf(solution, ntrans);

		long Wcode = MathMacro.BinaryTab2Decimal(transTable);
		String output;
		if (simplePrint){
			output = "" +Wcode;
		} else {
			output = " rule : " + Wcode	+ " -> " +MathMacro.ArrayToStringWithFormatting(transTable);
		}
		outsol.Add( output );
		// for relaunching with this solution
		// writing "cnf form" of this solution
		StringBuilder lT= new StringBuilder();
		int pos=0;
		// writing transitions first
		while( pos < ntrans){
			lT.append(solution[pos]).append(" ");
			pos ++;
		}
		outsol.Add(lT.toString());
		/*
		// writing evolve VARs
		String line= "" ;
		int count =0;
		int posIndex=0; // for reading in the initial condition register
		while (pos < varSz){
			line += "  " + solution[pos] + " ";
			pos++;
			count++;
			if (count % m_initCondSize.Get(posIndex) == 0 ){
				outsol.Add(line);
				line="";
			}
			if (count % (SatGen.Tsize * m_initCondSize.Get(posIndex)) == 0 ){ 
				count=0;
				posIndex++;
				outsol.Add("---");
			}
		}
		if (count != 0){
			outsol.Add(line);
		}
		 */
		outsol.WriteToFile(SOLUTION_TXT);

	}

	/*---------------------------------------------------------------------------
	  - MAIN
	  --------------------------------------------------------------------------*/






	public static void main(String[] argv) {
		CommandParser parser= new CommandParser(CLASSNAME, argv);
		IntegerList lst= new IntegerList();

		if (parser.ArgEquals(0,"icdef")){
			GenerateInitialConditions(SIZEARRAY);
		} else if (argv[0].equals("icbw")){ // black n' white init. cond.
			int len= FLString.String2Int( argv[1] );
			GenerateInitialConditionsBlackNWhite(len);
		} else if (argv[0].equals("icl")){// list of sizes for initial condition
			int len= argv.length;
			for (int i=1; i<len; i++){
				int lenconfig= FLString.String2Int( argv[i] );
				lst.addVal(lenconfig);
			}
			GenerateInitialConditions(lst);
		} else if (parser.ArgEquals(0,"con")){ // convert for compiling foundsolutions
			int nbits= FLString.String2Int(argv[1]);
			String satFile= argv[2], compilRuleFile= argv[3];
			ConvertRuleForIterating(satFile,compilRuleFile,nbits);
		} else if (parser.ArgEquals(0,"gen")){
			int nbits= FLString.String2Int(argv[1]);
			int Tsize= FLString.String2Int(argv[2]);
			SatGen gen = new SatGen(nbits);
			gen.generateSATsolveFile(Tsize);					
		} else if (parser.ArgEquals(0,"sol")){
			int nbits= FLString.String2Int(argv[1]);
			//LaunchSATsolver(FILETSTSATSOLVE);
			ReadSolution(nbits,SOLUTION_SAT,SOLUTION_TXT);
		}  else if (parser.ArgEquals(0,"fil")){
			int nbits= FLString.String2Int(argv[1]);
			FilterRules(nbits);
		} else {
			Macro.print(" Usage : <icl sizelist/ gen / con / sol / fil > ");
		}
	}


}