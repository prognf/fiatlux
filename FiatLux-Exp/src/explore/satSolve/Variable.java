package explore.satSolve;

class Variable {
	
	final Integer m_varNum;
	
	public Variable(int varNum) {
		m_varNum= varNum;
	}

	public Integer Pos() {
		return m_varNum;
	}
	
	public Integer Neg() {
		return -m_varNum;
	}
	
	static String VarTransition(int transition) {
		return "t" + transition;
	}
	
	/** case with three states */
	static String VarTransition(int transition, int l) {
		String letter=(l==0)?"A":"B";
		return "t" + transition + letter;
	}
	
}