package explore.satSolve;

import java.util.ArrayList;

import components.types.FLString;
import components.types.FLStringList;
import components.types.IntegerList;
import main.Convert;
import main.Macro;
import main.MathMacro;

/*-------------------------------------------------------------------------------
 * formulation of 3-state problem  
------------------------------------------------------------------------------*/
public class SatGenQ3 {

	static final String SOLUTION_SAT = "solution.cnf";
	static final String SOLUTION_TXT = "rule.txt";

	/*
	 * PARAMETERS FOR RUNNING THE EXPERIMENT 
	 */
	public static final int Q=3; // number of states
	public static final int L=2; // number of "bit layers"
	public static final int NBITS = 3;  // size of the neighb.
	public static final int DELTAPOS = -1; // leftmost index of the neighbourhood
	// number of transitions
	static final int 
	SZTRANSTAB = MathMacro.Power(Q,NBITS); // size of the transition table




	//other constants 

	private static final String FILETSTSATSOLVE = "satSolve.cnf";
	private static final String ICFILE = "IC.dat";
	//	private static final boolean AUTOLAUNCH = false; // automatic launch of the solver
	public static final String CODEPREFIX = "rule";
	private static final String codeC = "c"; // for "c(i,t)"
	private static final String CMDSATSOLVER = "./minisat ";
	private static final String 
		FILEFOUNDSOLUTIONS = "found-solutions.txt", 
			FILERULE="rule.txt";
	private static final String CMDREPLACE = "Replace", CMDREPLACEB = " "; //" with replace use " --";
	private static final Boolean VALTRUE = true;

	/*---------------------------------------------------------------------------
	  - I/O
	  --------------------------------------------------------------------------*/

	static private void LaunchSATsolver(String filename) {
		String cmd = CMDSATSOLVER + filename + " " + SOLUTION_SAT;
		Macro.ExecuteSystemCommand(cmd);		
	}

	/** generate CNF file of the problem 
	 * from ICFILE to FILETSTSATSOLVE 		**/
	void generateSATsolveFile(){
		Macro.print(2, "perparing...");
		InitGen();
		Macro.print(2, " generating the formula...");
		m_Store = new Store(FILETSTSATSOLVE);
		// building CNF FILE 
		AddCondNilpotency();
		AddCondThreeStatesOnly();
		FLStringList initCondList = new FLStringList(ICFILE);
		for (String ic : initCondList){
			String [] cut= ic.split(" ");
			if (cut.length!=2){
				Macro.FatalError(" irregular line format :" + ic);
			}
			int tsize= FLString.String2Int( cut[0] ) + 1;
			String initcond= cut[1];
			
			AddCondForOneInitialCondition(tsize, initcond);		
		}
		// hack : only the last initial condition should survive to last step
		AddCondSurvival();
		m_Store.Close();
		UpdateVariables();
	}

	/** CONSTRUCTION **/
	private void InitGen() {
		m_tabT= new VarTable();
		m_tabC= new VarTable(); 
		CreateVarTransitions();
		m_transitionTab= new ArrayList<String>(SZTRANSTAB);
		for( int trans=0; trans < SZTRANSTAB; trans ++){
			String strTrans= DecimalToSTrans(trans);
			//Macro.fDebug(" %d --> %s ", trans, strTrans);
			m_transitionTab.add(strTrans);
		}		
	}

	private void UpdateVariables() {
		// updating variables with the system's replace command
		Macro.print(" Used " + GetNumVar() + " variables " + m_Store.m_nclause + " clauses.");
		String cmd1 = CMDREPLACE + " NVAR " + GetNumVar() + CMDREPLACEB + FILETSTSATSOLVE;
		String cmd2 = CMDREPLACE + " NCLAUSES " + m_Store.m_nclause + CMDREPLACEB + FILETSTSATSOLVE;
		Macro.ExecuteSystemCommand(cmd1);
		Macro.ExecuteSystemCommand(cmd2);
	}
	// updating variables with the system's replace command
	/*Macro.print(" Used " + GetNumVar() + " variables " + m_Store.m_nclause + " clauses.");
			String cmd1 = "replace NVAR " + GetNumVar() + " -- " + FILETSTSATSOLVE;
			String cmd2 = "replace NCLAUSES " + m_Store.m_nclause + " -- " + FILETSTSATSOLVE;
			Macro.ExecuteSystemCommand(cmd1);
			Macro.ExecuteSystemCommand(cmd2);*/

	/*----------------------------------------------------------------------------
	 - attributes
	 --------------------------------------------------------------------------*/
	int m_szCurrent= -1; // size of the CURRENT initial condition !
	private int m_Tsize = 5	; // Tsize = height + 1	

	
	int m_varCount= 0;
	VarTable m_tabC, m_tabT; 

	private int m_initCond=-1; 


	// transition table with 
	private ArrayList<String> m_transitionTab; 

	private Store m_Store;
	/*---------------------------------------------------------------------------
	- construction
	--------------------------------------------------------------------------*/
	SatGenQ3(){
		
	}

	static public String DecimalToSTrans(int trans) {
		return Convert.Decimal2StringBaseN(trans, NBITS, Q);
	}

	private void CreateVarTransitions() {
		for (int i=0; i< SZTRANSTAB; i++){
			for (int l=0; l<L; l++){
				String varName = Variable.VarTransition(i, l);
				m_varCount++;
				m_tabT.AddVariable(varName,m_varCount);
			}
		}		
	}

	/** creates variables relative to initial cond **/
	private void CreateCikVarsForNewInitialCond() {
		m_initCond++;
		m_Store.AddInitCondSize(m_szCurrent);
		m_tabC= new VarTable(); // we "forget the past" 
		// Cik
		for (int k=0; k<m_Tsize; k++){
			for (int i=0; i<m_szCurrent; i++){
				CreateCik(i,k);			
			}
		}
	}

	private void CreateCik(int i, int k) {
		for (int l=0 ; l<L; l++){
			String varName= VarNameC(m_initCond, i,k,l);
			m_varCount++;
			m_tabC.AddVariable(varName, m_varCount);
		}
	}

	private Variable GetVarTransition(int trans, int l) {
		String key= Variable.VarTransition(trans,l); 
		Variable v= m_tabT.GetVarByKey(key);
		if (v == null){
			Macro.SystemWarning(" null var, key= " + key );
		}
		return v;
	}

	private Variable GetVarC(int i, int k, int l) {
		String key= VarNameC(m_initCond, i, k, l); 
		Variable v= m_tabC.GetVarByKey(key);
		if (v == null){
			String msg = String.format(" cond:%d i:%d k:%d", m_initCond,i,k);
			Macro.SystemWarning(msg);
		}
		return v;
	}


	/*---------------------------------------------------------------------------
	- CNF conditions
	-------------------------------------------------------------------------*/
	private void AddCondForOneInitialCondition(int tsize, final String bitArray) {
		m_szCurrent = bitArray.length();
		m_Tsize= tsize;
		Macro.print(" IC sz: " + m_szCurrent + " " + Macro.GetMemoryState());		
		CreateCikVarsForNewInitialCond();
		AddCondInitialState(bitArray);
		//AddCondSurvival();
		AddCondNullFinalState();
		AddCondConsistencyConditions();					
	}

	private void AddCondInitialState(final String initialCond) {
		String comment= "IC#" + m_initCond;
		for (int i=0; i < m_szCurrent; i++){
			int k=0;
			for (int l=0; l<L; l++){
				char st = initialCond.charAt(i);
				boolean boolCode= GetStateCodeLayerFromChar(st, l);
				Integer I= ValCikl(i, k, l, boolCode);
				Clause clause= new Clause(I);
				clause.SetComment(comment  + String.format("C(%d,%d)=%s",i,0, "" + st));
				m_Store.InsertClause(clause);
			}
		}
	}

	/** 0 and 1 not quiescent 
	 * TODO : recode this in a more robust way **/
	private void AddCondNilpotency() {
		SetRuleTransition(0, 0);
	}
	
	private void AddCondThreeStatesOnly(){
		for (int tr=0; tr < SZTRANSTAB; tr++){
			Clause c = new Clause();
			c.SetComment(" no fourth state");
			Integer 
				trA= GetVarTransition(tr, 0).Neg(),
				trB= GetVarTransition(tr, 1).Neg();
			c.AddAtom(trA);
			c.AddAtom(trB);
			m_Store.InsertClause(c);
		}
	}

	/** condition that the null point is not reached before time t **/
	private void AddCondSurvival(){
		for (int k=1; k< m_Tsize-1; k++){
			Clause survivesK= new Clause();
			survivesK.SetComment(" survival at time " + k);
			for (int i=0; i < m_szCurrent; i++){
				for (int l=0; l < L; l++){
					Integer var= ValCikl(i, k, l, VALTRUE);
					survivesK.AddAtom(var);
				}
			}
			m_Store.InsertClause(survivesK);
		}
	}

	/*---------------------------------------------------------------------------
	- "coherence" condition
	-------------------------------------------------------------------------*/

	/** adds to f coherence conditions **/
	private void AddCondConsistencyConditions() {
		for (int k=0; k < m_Tsize - 1; k++){
			for (int pos= 0 ; pos < m_szCurrent ; pos ++){
				EvolveConditionCik(pos, k);
			}
		}
	}


	/** adds to f coherence condition on cell i time k to k+ 1 **/
	private void EvolveConditionCik(int i, int k) {
		for (int trans= 0 ; trans < SZTRANSTAB ; trans ++){
			OneEvolveCond(i, k, trans);	
		}
	}

	/** coherence condition from cell i from time k to k+1 for transition trans **/
	void OneEvolveCond(int i, int k, int trans){
		final int KPLUSONE = k+1; // transition from time k to time k+1 
		Variable y2a= GetVarC(i,KPLUSONE,0), y2b=  GetVarC(i,KPLUSONE,1);
		Variable 
		tA= GetVarTransition(trans,0), 
		tB= GetVarTransition(trans,1);
		Formula equalA=  Formula.Equal(y2a, tA);
		Formula equalB=  Formula.Equal(y2b, tB);

		Clause clause= new Clause();
		// for each bit of the transition
		for (int bitpos=0; bitpos < NBITS; bitpos++){
			for (int l=0 ; l < L; l++){ // for each layer
				int j= i + DELTAPOS +bitpos;  // DELTAPOS is negative
				// inversion comes from the traduction of 
				// "A implies B" into "(not A) or B"
				String  stran = m_transitionTab.get(trans); 
				boolean bit = ! GetStateCodeLayerFromStringAndPos(stran,bitpos,l); 
				clause.AddAtom( ValCikl(j,k,l,bit) );
			}
		}
		clause.SetComment(String.format("T[%d,%d]=t%d",i,KPLUSONE,trans));
		equalA.Distribute(clause);
		equalB.Distribute(clause);
		m_Store.AddFormula( equalA );
		m_Store.AddFormula( equalB );
	}


	/** returns the bit corresponding to 
	 * the transition trans in pos statepos d bit layer l */
	private boolean GetStateCodeLayerFromChar(char c, int l) {
		String strState= "" + c;
		int state= FLString.String2Int(strState);
		return StateLayer(state,l);
	}


	/** returns the bit corresponding to 
	 * the transition trans in pos statepos d bit layer l */
	private boolean GetStateCodeLayerFromStringAndPos(String str, int pos, int l) {
		return GetStateCodeLayerFromChar( str.charAt(pos), l);
	}

	final private Integer ValCikl(int i, int k, int l, Boolean boolCode) {
		return (boolCode)?GetVarC(i,k,l).Pos():GetVarC(i,k,l).Neg();
	}		




	/*---------------------------------------------------------------------------
	- final state conditions
	-------------------------------------------------------------------------*/

	/** final configuration is null **/
	private void AddCondNullFinalState(){
		int k= m_Tsize - 1;
		for (int i=0; i< m_szCurrent; i++){
			for (int l=0; l < L; l++){
				Clause c= new Clause ( GetVarC(i, k, l).Neg() );
				String comment= String.format(" nilpotency condition (%d,%d)",i,l); 
				c.SetComment(comment);
				m_Store.InsertClause(c);
			}
		}				
	}

	/*---------------------------------------------------------------------------
	- get & set
	--------------------------------------------------------------------------*/

	/** human readable : l= layer **/
	String VarNameC(int cond, int i, int k, int l){
		i = (i + m_szCurrent) % m_szCurrent ;		
		return cond + codeC + i + k + l ;
	}

	int GetNumVar(){
		return m_varCount;
	}

	/*---------------------------------------------------------------------------
	- special ?
	-------------------------------------------------------------------------*/

	/** setting the rule to a particular value **/
	private void SetRuleTransition(int trans, int state) {
		boolean 
		bitA= StateLayer(state,0), bitB= StateLayer(state,1);
		Integer 
		Ia= VarTransition(trans, 0, bitA), Ib= VarTransition(trans, 1, bitB); 
		AddAtomicClauseWithComment(Ia, "t"+ trans + "A" +  "=" + bitA);
		AddAtomicClauseWithComment(Ib, "t"+ trans + "B" +  "=" + bitB);
	}

	private void AddAtomicClauseWithComment(Integer I, String comment) {
		Clause T= new Clause(I);
		T.SetComment(comment);
		m_Store.InsertClause(T);

	}

	private Integer VarTransition(int trans, int l, boolean bitA) {
		return bitA? GetVarTransition(trans, l).Pos():GetVarTransition(trans, l).Neg();
	}


	/*---------------------------------------------------------------------------
	- static & co.
	--------------------------------------------------------------------------*/



	/** state code 
	 * associates to a state two booleans (one for each layer) 
	 * special case for Q=3 */
	static private boolean StateLayer(int state, int layer) {
		return (layer==0)?(state==1):(state==2);
	}
	
	static public int TwoBoolCodeToState(boolean va, boolean vb){
		return (va?1:0)+(vb?2:0);
	}


	private static void FilterRules() {
		IntegerList ruleList= IntegerList.ReadFromFile(FILEFOUNDSOLUTIONS);
		ruleList.SortBubble();
		//check this one (modified)
		ruleList.WriteToFileCol(FILEFOUNDSOLUTIONS);
	}

	//check this one (modified)

	/*private static void ConvertRuleForIterating(String satFile, String compilRuleFile) {
		FLStringList rule= FLStringList.OpenFile(FILERULE);
		String sWcode= rule.Get(0);
		RuleCode W= RuleCode.FromString(sWcode);
		ArrayList<RuleCode> family= RuleCode.GetSymmetrics(W, NBITS);
		for (RuleCode toexclude : family){
			Exclude(toexclude, satFile);
		}
		String str= W + MathMacro.ArrayToStringWithFormatting(family.toArray());
		FLStringList.AppendToFile(compilRuleFile, str);
	}*/
	
	
	
	
	

	/*---------------------------------------------------------------------------
	  - MAIN
	  --------------------------------------------------------------------------*/

	/*private static void Exclude(RuleCode W, String satFile) {
		Macro.Debug(" dealing with solution W=" + W);
		// CNF coding of transition table var.
		int [] bitarray = W.ToBinaryArray(NTRANS);
		String out= ""; 
		for (int i=0; i< NTRANS; i++){
			out += (1 - 2 *bitarray[i]) * (i+1) + " "; // from 0/1 to -1/1
		}
		out += "0";
		out += " c exclusion of rule " + W;
		Macro.print(out);
		// appending info in file
		FLStringList.AppendToFile(satFile, out);	
	}*/

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();

	public static void main(String[] argv) {
		// special
		if (argv[0].equals("con")){
			String 
			satFile= argv[1],
			compilRuleFile= argv[2];
			//Macro.Debug("DESACTIVATED");
			//	ConvertRuleForIterating(satFile,compilRuleFile);
		} else if (argv.length == 1){
			if (argv[0].equals("ic")){
				//TODO GenerateInitialConditions();
			} else if (argv[0].equals("gen")){
				SatGenQ3 gen = new SatGenQ3();
				gen.generateSATsolveFile();					
			} else if (argv[0].equals("sol")){
				//LaunchSATsolver(FILETSTSATSOLVE);
				SatProcess.ReadSolution(SOLUTION_SAT,SOLUTION_TXT);
			}  else if (argv[0].equals("fil")){
				FilterRules();
			}
		} else {
			Macro.print(" Usage : <gen/con/sol/fil> ");
		}
	}


	

	public static void DoTest(){
		Macro.BeginTest(CLASSNAME); 
		SatGenQ3 gen = new SatGenQ3();
		gen.generateSATsolveFile();
		//gen.testEvolve();
		//gen.testDiff();
		//gen.testRec();
		//gen.testRec2();
		//gen.testSATsolve();
		//gen.testOneRule(861);
		Macro.EndTest();
	}

}
