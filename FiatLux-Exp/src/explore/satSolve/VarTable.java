package explore.satSolve;

import java.util.Hashtable;

import main.Macro;

/*-------------------------------------------------------------------------------
- provides a double one-to-one mapping String and Int
- @author Nazim Fates
------------------------------------------------------------------------------*/
public class VarTable {

	/*----------------------------------------------------------------------------
	  - attributes
	  --------------------------------------------------------------------------*/

	private Hashtable<String, Variable> m_map; 
	
	/*---------------------------------------------------------------------------
	  - construction
	  --------------------------------------------------------------------------*/

	public VarTable(){
		m_map = new Hashtable<String, Variable>(); 
	}

	
	/*---------------------------------------------------------------------------
	  - actions
	  --------------------------------------------------------------------------*/
	public void AddVariable(String sVal, int varNum){
		Variable v = new Variable(varNum); 
		m_map.put(sVal,v);
	}

	

	/*---------------------------------------------------------------------------
	  - get & set
	  --------------------------------------------------------------------------*/
	
	/** Key -> Variable  **/
	public Variable GetVarByKey(String sVal){
		return m_map.get(sVal);
	}

	/** Key -> VarNum  **/
	public int GetVarNumByKey(String sVal) {
		Variable v = GetVarByKey(sVal);
		return v.m_varNum;
	}
	
	
	/*---------------------------------------------------------------------------
	  - test
	  --------------------------------------------------------------------------*/


	static public void main(String [] s){
		DoTest();
	}


	final static String CLASSNAME= Thread.currentThread().getStackTrace()[1].getClassName();
	public static void DoTest(){
		Macro.BeginTest(CLASSNAME);
		
		Macro.EndTest();
	}

	
	
}
