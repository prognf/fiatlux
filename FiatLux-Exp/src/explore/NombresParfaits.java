package explore;

import java.util.ArrayList;
import java.util.HashMap;

import components.types.IntegerList;
import main.Macro;

public class NombresParfaits {

	private static final long NLONG=100;//1000000;
	private static final int N = 100, MAXSEARCH= N/2;
	private static final boolean CRIBLE = false;
	private static final boolean PRINTALL = true; // print them all ?
	private static final boolean DIVISEURS = true; // analyse les diviseurs ?
	final ArrayList<Entree> m_crible;

	enum QUALITE {PARFAIT, DEFICIENT, GENEREUX}

	class Entree {

		IntegerList diviseurs;
		final int nombre;
		int sommediv;
		QUALITE qualite;

		Entree (int inNombre){
			if (DIVISEURS){
				diviseurs= new IntegerList();
			}
			nombre= inNombre;
		}
		/*public void CalculeQualite() {
			qualite=(sommediv<)
		}*/
		public void CalculeQualite() {
			qualite= (nombre==sommediv)?
					QUALITE.PARFAIT:(sommediv>nombre)?
							QUALITE.GENEREUX:QUALITE.DEFICIENT;
		}
		
		public void Print() {
			Macro.fPrint(""+this);	
		}

		public String toString(){
			String s= String.format("i:%d q:%s sum:%d : ", 
					nombre, qualite, sommediv);
			if (CRIBLE){
				s+= diviseurs;
			}
			return s;
		}
		public void AjouteDiviseur(int i) {
			if (DIVISEURS){
				diviseurs.addVal(i);
			}
			sommediv+= i;
		}

	}

	public NombresParfaits() {
		if (CRIBLE){
			m_crible = new ArrayList<Entree>(N+1);	

			m_crible.add(null);
			for (int i=1; i<= N ;i++ ){
				Macro.ProgressBar(i, N);
				m_crible.add( new Entree(i) );
			}
		} else {
			m_crible= null;
		}
	}

	private void HiHa(){		
		for (int i=1; i<= MAXSEARCH ;i++ ){
			for (int j=2*i; j<=N; j+=i){
				Entree e= m_crible.get(j);
				e.AjouteDiviseur(i);
			}
		}
	}

	private void HoHo(){
		for (int i=2; i<=N ;i++ ){
			Entree e= m_crible.get(i);
			e.CalculeQualite();
			if (PRINTALL){
				e.Print();
			}
		}
		Macro.SeparationLine();
		Macro.print("PERFECTO:");
		for (int i=2; i<=N ;i++ ){
			Entree e= m_crible.get(i);
			if (e.qualite==QUALITE.PARFAIT){
				e.Print();
			}
		}
	}

	
	static class MutableInteger {
		int m_val;
		MutableInteger(int val){
			m_val= val;
		}
		public int getValue() {
			return m_val;
		}
		public void setValue(int val) {
			m_val= val;
		}
	}

	static class FactorList extends HashMap<Long, MutableInteger>{

		public void AddFactor(long i) {
			if (containsKey(i)){
				MutableInteger ival= get(i);
				ival.setValue(ival.getValue()+1);
			} else {
				this.put(i,new MutableInteger(1));
			}
		}

		public String toString(){
			StringBuilder s= new StringBuilder();
			s.append("sum: ").append(SumDivisors());
			s.append(" fact:");
			for ( java.util.Map.Entry<Long, MutableInteger> entry : this.entrySet()){
				int exponent = entry.getValue().getValue();
				s.append(" ").append(entry.getKey());
				if (exponent>1){
					s.append("^").append(exponent);
				}
			}

			return s.toString();
		}

		public long SumDivisors(){
			long sum=1; // implicit divisor
			for ( java.util.Map.Entry<Long, MutableInteger> entry : this.entrySet()){
				int exponent = entry.getValue().getValue();
				long val= entry.getKey();
				long newval= sum * Series(val,exponent); 
				sum += newval;
			}
			return sum;
		}

		/** sums of the power of x by recurrence **/
		private long Series(long x, int exponent) {
			return (exponent==1)?x:x*(Series(x, exponent-1)+1);
		}

	}

	/** decomposition of a number into its factors */
	static FactorList Decompose(long nombre){
		FactorList factors= new FactorList();
		long RACINE= (long)(Math.sqrt(nombre)+1);
		int i=2;
		while(i<=RACINE){
			if (nombre%i==0){
				factors.AddFactor(i);
				nombre /= i;
				//RACINE= (int)Math.sqrt(nombre);
				i=1;
			}
			i++;
		}
		if (nombre>1){
			factors.AddFactor(nombre);
		}
		return factors;
	}

	private void TestDecompose() {
		Macro.print(" decomposing...");
		long nbr=2;
		while(nbr < NLONG){
			if (nbr%100000==0){
				Macro.print(" step :" + nbr);
			}
			FactorList factors= Decompose(nbr);
			long sumDiv= factors.SumDivisors();
			boolean perf=PRINTALL||(sumDiv==2L*nbr);
			if (perf){
				Macro.fPrint("i: %d %s %s", nbr, perf, sumDiv);
			}
			nbr+=2;
		}
	}


	final static int LIM_ERATHOSTENE=10000;


	/*** find the prime numbers up to lim **/
	static IntegerList FindPrimes(int lim){
		Macro.print("searching prime numbers to" + lim);

		int sroot= (int)Math.sqrt(lim);
		boolean [] prime = new  boolean[lim+1];
		for (int i=2; i<= lim; i++){
			prime[i]= true;
		}

		for (int i=2; i<= sroot; i++){
			//Macro.print(" dealing with : " + i);
			Decime(prime, i, lim);
		}

		// putting primes in a list
		IntegerList primes= new IntegerList();
		for (int i=2; i<= lim; i++){
			if (prime[i]){
				primes.addVal(i);
			}
		}
		return primes;
	}

	/*------------------------*/
	private static void Decime(boolean[] prime, int i, int lim) {
		for (int hole=2*i; hole <= lim; hole+=i){
			prime[hole]= false;
		}
	}

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();
	public static void main(String[] argv) {
		Macro.BeginTest(CLASSNAME);
		int maxint= Integer.MAX_VALUE;
		Macro.print(" max int:" + maxint);
		//CommandParser parser= new CommandParser(CLASSNAME, argv);
		NombresParfaits exp= new NombresParfaits();
		if (CRIBLE){
			exp.HiHa();
			exp.HoHo();}
		else {
			exp.TestDecompose();
		}
		/*FindPrimes(LIM_ERATHOSTENE);*/
		Macro.EndTest();
	}





}
