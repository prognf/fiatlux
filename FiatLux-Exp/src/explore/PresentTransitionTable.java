package explore;

import main.Macro;
import main.MathMacro;
import models.CAmodels.tabled.LookUpTableBinary;
import components.types.FLStringList;
import components.types.RuleCode;

public class PresentTransitionTable {


	private static final RuleCode W= new RuleCode(5419);
	private static final int SZ = 4, NTRANS= MathMacro.TwoToPowerIntResult(SZ);
	private static final String FILENAME = "trans" + W + ".tex";

	private static String FormatEntry(LookUpTableBinary table, int i, int placeoutput) {
		return FormatEntryLatex(table,i,placeoutput);
	}

	private static String FormatEntryLatex(LookUpTableBinary table, int bitpos, int placeoutput) {
		int t= table.getBitTable(bitpos);
		String sbin = MathMacro.Int2BinaryString(bitpos, SZ);
		int [] bit= MathMacro.Decimal2BinaryTabInverted(bitpos, SZ);
		boolean active= (t != bit[placeoutput]);

		String ln="";
		ln+=(active)?"{\\bf":"";
		ln+= "\\begin{tabular}{l} " + sbin + " \\\\ "; 
		StringBuilder space= new StringBuilder();
		for (int i=0; i< placeoutput; i++){
			space.append("?");
		}
		ln+= "\\phantom{"+ space + "}" + t ;
		//Macro.Debug("sp:"+space);
		ln= ln.replace('1', 'X');
		ln+= " \\end{tabular}";
		ln+= (active)?"}":"";

		// jump
		if (bitpos%4==3){
			ln += " \\\\";
		}

		return ln;
	}

	private static String FormatEntrySimple(LookUpTableBinary table, int i) {
		int t= table.getBitTable(i);
		String bin = MathMacro.Int2BinaryString(i, SZ);
		return bin + " -> " + t;
	}

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();
	
	public static void main(String[] argv) {
		Macro.BeginTest(CLASSNAME);
		LookUpTableBinary table= new LookUpTableBinary(NTRANS);
		table.SetRule(W);
		FLStringList list= new FLStringList();
		for (int shiftoutput=0; shiftoutput < SZ; shiftoutput++){
			for (int i=0; i < NTRANS; i++){
				list.Add(FormatEntry(table,i,shiftoutput));
			}
			list.Add("\\\\");
			list.Add("\\\\");
		}
		Macro.SeparationLine();
		list.Print();
		list.WriteToFile(FILENAME);
		Macro.SeparationLine();
		Macro.EndTest();
	}
}
