package alesia.erste;

import components.types.IntC;
import experiment.measuring.general.DensityMD;
import experiment.samplers.CAsampler;
import experiment.samplers.LinearSampler;
import grafix.viewers.LineAutomatonViewer;
import main.Macro;
import main.MathMacro;
import models.CAmodels.alesia.AlesiaModelQiqiHouhou;
import models.CAmodels.alesia.GateCAinitializer;
import topology.basics.LinearTopology;
import topology.zoo.RadiusN_Topology;

/* elpis = hope !, after q2h2 */
public class PlayerAlesiaQ2H2 extends Player {

	private static final int Ncell = 2*AlesiaGameEngine.N+10;
	private static final int Nsteps = 2*AlesiaGameEngine.N + 4 + AlesiaGameEngine.F;
	
	CAsampler m_samp;
	GateCAinitializer m_init;
	DensityMD m_dens= new DensityMD();

	public PlayerAlesiaQ2H2(double pVol, double pTrans, double pAbs) {
		super(MakeName(pVol, pTrans, pAbs));
		LinearTopology topo= new RadiusN_Topology(1);
		AlesiaModelQiqiHouhou model= new AlesiaModelQiqiHouhou();
		model.setpVpTpA(pVol,pTrans,pAbs);
		
		m_samp= new LinearSampler(Ncell, model, topo);
		IntC cellPixSize= new IntC(10,0);
		LineAutomatonViewer av= 
				model.GetLineAutomatonViewer(
						cellPixSize, topo, m_samp.GetAutomaton(),40);
		m_samp.SetAutomatonViewer(av);
		m_init= new GateCAinitializer();
		m_samp.SetInitDeviceFromOutside(m_init);	
		m_dens.LinkTo(m_samp);
	}

	private static String MakeName(double pVol, double pTrans, double pAbs) {
		return String.format("elpis-pV%d-pT%d-pA%d",
				MathMacro.percentFromRatio(pVol),
				MathMacro.percentFromRatio(pTrans),
				MathMacro.percentFromRatio(pAbs));
	}

	public int Play(){
		int a=MyVal(), front= FrontWithOffset()+1, b=OpponentVal();
		
		//Macro.fPrint("front with offset :%d  === a,b:%d,%d",FrontWithOffset(),a,b );
		m_init.SetInput(a,front,b); 
		m_samp.sig_Init();
		//m_samp.GetAutomaton().PrintStateInfo();
		m_samp.NstepsAndUpdate(Nsteps);
		//Macro.print("evolved to>>>");
		//m_samp.GetAutomaton().PrintStateInfo();
		int nV= m_dens.CountState(AlesiaModelQiqiHouhou.V);
		int playIA= (nV==0)?1:nV; // plays at least one token
		//String sPlayCA= String.format(" $$$$ playing %d / %d init  (%d,%d,%d)", 
		//		playIA, a, a, front, b);
		//Macro.print(sPlayCA);
		
		if (playIA>a) {
						
				//m_samp.GetAutomaton().PrintStateInfo();
			Macro.FatalError("playing more than one has!!");
		}
		return playIA;
	}

}
