package alesia.erste;

import components.types.IntC;
import experiment.samplers.CAsimulationSampler;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.imgFormat.ImgFormat;
import grafix.viewers.LineAutomatonViewer;
import grafix.viewers.LineAutomatonViewerDefault;
import main.Macro;
import models.CAmodels.CellularModel;
import models.CAmodels.alesia.AlesiaModel;
import models.CAmodels.alesia.AlesiaModelQiqiHouhou;
import models.CAmodels.alesia.GateCAinitializer;
import topology.basics.SamplerInfoLinear;
import topology.basics.TopologyCoder;

/** not tournament  ???*/
public class ExpAlesiaForArticle {

	final static int NARG=1;
	final static String CLASSNAME= 
			new Macro.CurrentClassGetter().getClassName();

	private static final int Ncells = 2*AlesiaGameEngine.N + 10;
	private static final int T = 30; //TODO : set this properly !!!!!!!!!!!!
	private static final IntC CellPixSize = new IntC(4,0);
	private static final int SEEDI = 14, SEEDM=29;
	
	/** main : processing command line * */
	public static void main(String[] argv) {
		//FiatLuxProperties.SetVerboseLevel(3);
		DoDiag1();
	}
	
	/** fully asynch. case **/
	private static void DoDiag1() {

		CellularModel rule = new AlesiaModelQiqiHouhou();
		String 	topoCode = TopologyCoder.s_TLR1;

		SamplerInfoLinear info = 
				new SamplerInfoLinear(Ncells, T, CellPixSize, rule, topoCode);
		CAsimulationSampler sampler= new CAsimulationSampler(info);
		
		
		LineAutomatonViewer viewer= new LineAutomatonViewerDefault(CellPixSize, sampler, T);
		viewer.SetPalette( new PaintToolKit( AlesiaModel.palette ) );
		sampler.SetAutomatonViewer(viewer);

		setInitCond(sampler);
		
		// running the dyn. sys.
		sampler.sig_Init();
		for (int t=0; t<T-1; t++){
			sampler.sig_NextStep();
		}
		String filename= "diag-q2h2";
		sampler.io_SetRecordFormat(ImgFormat.SVG);
		sampler.io_SaveImage(filename, viewer);	
		sampler.io_SetRecordFormat(ImgFormat.PNG);
		sampler.io_SaveImage(filename, viewer);	
	}

	private static void setInitCond(CAsimulationSampler sampler) {
		sampler.SetSeeds(SEEDI, SEEDM, 0);
		GateCAinitializer init= (GateCAinitializer)sampler.GetInitializer();
		int ga1=8, fr1=2, gb1=12;
		init.SetInput(ga1, fr1, gb1);
	}
	
	
}
