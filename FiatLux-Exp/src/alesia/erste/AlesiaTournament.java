package alesia.erste;

import java.util.Locale;

import alesia.erste.AlesiaGameEngine.outcome;
import components.types.Couple;
import main.FiatLuxProperties;
import main.Macro;
import main.MathMacro;
import main.commands.CommandInterpreter;
import main.commands.CommandParser;

public class AlesiaTournament extends CommandInterpreter {

	private static final int 
	MIL=1000, DIMIL=10000, CENMIL=100000;
	static int m_prnlevel=3;
	final static String USAGE="???";

	final static String CLASSNAME= 
			new Macro.CurrentClassGetter().getClassName();

	private static final String PRECISION = "%.3f";

	private static final boolean PROGRESSBAR = false;
	private static final String MATCHRES = "Match results:";

	private static final boolean TWOGAMES = false;
	//	private static final int W = GameEngine.W, N= GameEngine.N;

	static Player FRmax= new PlayerAlesiaUnifRange(AlesiaGameEngine.N);
	static Player BeatFRmax= new PlayerBeatFixedRange();

	static int [] VALDUMMY= { 10, 12, 14, 16, 18, 20, 22, 24}; 
	static int [] VALUNIF2= { 8, 10, 12, 14, 16, 18, 20};

	static int [] PvolontaryQQHH2 = { 2, 4, 6, 8, 10};
	//static int [] PvolontaryQQHH5 = {  10, 20, 30, 40};
	
	
	//-------------------
	static int [] Ptrans2 = { 0, 2, 5, 10, 15};
	static int [] BinomialRHOVAL2 = { 22, 24, 26, 28, 30, 32, 34 };

	static int [] BinomialRHOVAL3a = { 15, 20, 25, 30};
	static int [] BinomialRHOVAL3b = { 20, 22, 24, 26};
	
	
	static int [] Ptrans4 = { 10, 20, 30, 40, 50};
	static int [] PvolontaryQQHH4 = {  0, 5, 10, 15, 20};
	
	
	static int [] BinomialRHOVAL5= { 15, 20, 25, 30, 35};
	//static int [] BinomialRHOVAL5b= { 28, 30, 32, 35, 40};
	static int [] Ptrans5 = { 0, 5, 10, 15, 20, 25, 30};
	protected static final double PTDEF5 = 0.20, PTDEF5b = 0.05;
	//static int [] Ptrans5b = MakeInterval(0, 60, 10);
	protected static final double PVOL5=0.25;
	
	static int [] Ptrans6  = MakeInterval(10,60,10);
	static int [] Ptrans6b  = MakeInterval(0,20,5);
	static int [] PvolontaryQQHH6 = MakeInterval(0,50,10);

	static int [] PvolontaryQQHH7 = { 10, 15, 20, 25, 30};

	protected static final double PT7=0.4;
	
	static int [] Pabs8 = { 0, 20, 40, 60, 80, 100};
	static int [] BinomialRHOVAL8 = { 8, 10, 12, 14, 20};
	static int [] PvolontaryQQHH8a = { 10, 15, 20, 25};
	static int [] PvolontaryQQHH8b = { 0, 5, 10, 15, 20, 25};
	
	protected static final double PVOL8=0.3, PT8a=0.25, PT8ba=.4;

	static int [] Pabs9 = { 0, 20, 40, 60, 80, 100};
	static int [] Pvol9 = { 0, 20, 40, 60, 80, 100};
	
	protected static final double PVC9=0.12, PTC9=.0, PAC9=1;

	static int [] Pvol10 = { 0, 2, 4, 6, 8, 10};
	static int [] Pabs10 = { 20, 40, 60, 80, 100};
	protected static final double PTC10=.25;
	
	protected static final double RHOCHAMPION = 0.22, RHO6a=0.30,RHO6b=0.35;

	//-----------------------------------------------------
	final int m_NSAMPLE;

	public AlesiaTournament(int nsample) {
		m_NSAMPLE= nsample;
	}

	private static int[] MakeInterval(int deb, int fin, int delta) {
		int size= (fin - deb ) / delta + 1;
		int [] tab= new int[size];
		for (int i=0; i<size; i++) {
			tab[i]= deb + i*delta;
		}
		return tab;
	}

	final static int NARG=2;
	/** main : processing command line * */
	public static void main(String[] argv) {
		Locale.setDefault(new Locale("en", "US"));
		FiatLuxProperties.SetVerboseLevel(3);
		m_prnlevel=1;
		CommandParser parser = new CommandParser(CLASSNAME, "", argv);
		if (argv.length != NARG ){
			parser.Usage(USAGE);
		}
		//TestFirstStochasticAgainstDummy();
		//
		int exp= parser.IParse("EXP=");
		int NS= parser.IParse("NS=");
		AlesiaTournament doudou = new AlesiaTournament(NS);
		switch (exp){
		case -2:
			Player plyyA= new zzPlayerHuman("humano");
			Player plyyB= new PlayerAlesiaQ2H2(PVC9, PTC9, PAC9);
			OnePlay(plyyA, plyyB);
			break;
		case -1:
			Player plyA= 
			//new PlayerAlesiaElpis(.2, .1, 0.);
			BeatFRmax;
			Player plyB= FRmax;
					//new PlayerAlesiaBinomial(.35);
			OnePlay(plyA, plyB);
			break;
		case 0:
			Player playerA= 
			//new PlayerAlesiaElpis(.2, 0.1, 0.);
			BeatFRmax;
			//new PlayerAlesiaBinomial(.22);
			Player playerB= FRmax;
				//new PlayerAlesiaBinomial(.22);
				//new PlayerAlesiaBinomial(.35);
				//new PlayerAlesiaNashMatrix();
			doudou.StatsTwoPlayers(playerA,playerB); 
			break;
		case 1:
			doudou.CompareVariousUnif();
			break;
		case 2:
			doudou.testBinomialAgainstUnif();
			break;	
		case 3:
			doudou.testBinomialAgainstBinomial();
			break;
		case 4:
			doudou.testQ2H2VsBinomChampion();
			break;
		case 5:
			doudou.testQ2H2VsBinom2();
			break;
		case 6:
			doudou.testQ2H2VsBinomBestOpponent();
			break;
		case 7:
			doudou.testQ2H2vsQ2H2();
			break;
		case 8:
			doudou.testElpisvsBinomial();
			break;
		case 9:
			doudou.testElpisChampion();
			break;
		case 10:
			doudou.testElpisVsNash();
			break;
		case 11:
			doudou.testBinomVsNash();
			break;
		}
		parser.Bye();
	}

	
	/** experiment 11 (April-18) **/
	private void testBinomVsNash() {
		ScoreTable score = new ScoreTable(Pvol10, new int [] {99}, "11","pV","ZZ"){
			@Override
			Couple<Player> GetPlayers(int valA, int valB) {
				double rho= (valA/MathMacro.HUNDRED);
				return new Couple<Player>(
						new PlayerAlesiaBinomial(rho),
						new PlayerAlesiaNashMatrix()); 
			}
		};
		score.computeAndOutputTable(false, m_NSAMPLE);
	}
	

	/** experiment 10 (April-18) **/
	private void testElpisVsNash() {
		final double pTrans= PTC10;
		String expName= "10-pT"+MathMacro.percentFromRatio(pTrans);
		
		ScoreTable score = new ScoreTable(Pabs10, Pvol10,expName,"spA","spV"){
			@Override
			Couple<Player> GetPlayers(int valA, int valB) {
				double pAbs= (valA/MathMacro.HUNDRED);
				double pVol= (valB/MathMacro.HUNDRED);
				
				return new Couple<Player>(
						new PlayerAlesiaQ2H2(pVol,pTrans,pAbs),
						new PlayerAlesiaNashMatrix()); 
			}
		};
		score.computeAndOutputTable(false, m_NSAMPLE);
	}
	
	/** experiment 9 (April-18) **/
	private void testElpisChampion() {
		final double pTrans= PTC9;
		
		ScoreTable score = new ScoreTable(Pabs9, Pvol9,"9","pA","pV"){
			@Override
			Couple<Player> GetPlayers(int valA, int valB) {
				double pAbs= (valA/MathMacro.HUNDRED);
				double pVol= (valB/MathMacro.HUNDRED);
				
				return new Couple<Player>(
						new PlayerAlesiaQ2H2(pVol,pTrans,pAbs),
						new PlayerAlesiaQ2H2(PVC9, PTC9, PAC9)); 
			}
		};
		score.computeAndOutputTable(false, m_NSAMPLE);
	}
	

	/** experiment 8 (April-18) **/
	private void testElpisvsBinomial() {
		final double pTrans= PT8a;
		String expName= "8-pT"+MathMacro.percentFromRatio(pTrans);
		ScoreTable score = new ScoreTable(Pabs8, PvolontaryQQHH8a,expName,"spA","spV"){
			@Override
			Couple<Player> GetPlayers(int valA, int valB) {
				double pAbs= (valA/MathMacro.HUNDRED);
				double pVol= (valB/MathMacro.HUNDRED);
				
				return new Couple<Player>(
						new PlayerAlesiaQ2H2(pVol, pTrans, pAbs),
						new PlayerAlesiaBinomial(RHO6a)); 
			}
		};
		score.computeAndOutputTable(false, m_NSAMPLE);
	}

	/** experiment 7 (April-18) **/
	private void testQ2H2vsQ2H2() {
		ScoreTable score = new ScoreTable(PvolontaryQQHH7,PvolontaryQQHH7,"7","spV","spV"){
			@Override
			Couple<Player> GetPlayers(int valA, int valB) {
				double pVolA= (valA/MathMacro.HUNDRED);
				double pVolB= (valB/MathMacro.HUNDRED);

				double pTrans= PT7;//(valB/MathMacro.HUNDRED);
				return new Couple<Player>(
						new PlayerAlesiaQ2H2(pVolA,pTrans,0),
						new PlayerAlesiaQ2H2(pVolB,pTrans,0) );
			}
		};
		score.computeAndOutputTable(true, m_NSAMPLE);
	}

	/** experiment 6 (April-18) **/
	private void testQ2H2VsBinomBestOpponent() {
		final double rho=RHO6b;
		String expName="6-bnm"+MathMacro.percentFromRatio(rho);
		ScoreTable score = new ScoreTable(PvolontaryQQHH6, Ptrans6,expName,"spV","spT" ){
			@Override
			Couple<Player> GetPlayers(int valA, int valB) {
				double pVol= (valA/MathMacro.HUNDRED);
				double pTrans= (valB/MathMacro.HUNDRED);
				Player playA= new PlayerAlesiaQ2H2( pVol, pTrans, 0 );
				Player playB= new PlayerAlesiaBinomial(rho);
				return new Couple<Player>(playA,playB);
			}
        };
		score.computeAndOutputTable(false, m_NSAMPLE);
	}


	/** experiment 5 (April-18) **/
	private void testQ2H2VsBinom2() {
		ScoreTable score = new ScoreTable(BinomialRHOVAL5, Ptrans5, "5","bnm","spT" ){
		//ScoreTable score = new ScoreTable(BinomialRHOVAL5b, Ptrans5b, "5b","bnm","spT" ){
			@Override
			Couple<Player> GetPlayers(int valA, int valB) {
				double rho= valA/MathMacro.HUNDRED;
				//A
				Player playA= new PlayerAlesiaBinomial(rho);
				//B
				double pVol= PVOL5;
				double pTrans= valB/MathMacro.HUNDRED;
				Player playB= new PlayerAlesiaQ2H2( pVol, pTrans, 0.0 );
				return new Couple<Player>(playA,playB);
			}
        };
		score.computeAndOutputTable(false, m_NSAMPLE);
	}


	/** experiment 4 (April-18) **/
	private void testQ2H2VsBinomChampion() {
		ScoreTable score = new ScoreTable(PvolontaryQQHH4, Ptrans4,"4","spV","spT"){
			@Override
			Couple<Player> GetPlayers(int valA, int valB) {
				double pVol= (valA/MathMacro.HUNDRED);
				double pTrans= (valB/MathMacro.HUNDRED);
				//Player playA= new PlayerAlesiaQiqiHouhou( pVol, pTrans );
				Player playA= new PlayerAlesiaQ2H2( pVol, pTrans,0.0 );
				Player playB= new PlayerAlesiaBinomial(RHOCHAMPION);
				return new Couple<Player>(playA,playB);
			}
        };
		score.setInfoInFirstLine("$\\pV$","$\\pT$");
		score.computeAndOutputTable(false, m_NSAMPLE);
	}

	/** experiment 3 (April-18) **/
	private void testBinomialAgainstBinomial() {
		ScoreTable scoreA = new ScoreTable(BinomialRHOVAL3a, BinomialRHOVAL3a,"3a","bnm","bnm"){
			@Override
			Couple<Player> GetPlayers(int valA, int valB) {
				return new Couple<Player>(
						new PlayerAlesiaBinomial(valA/MathMacro.HUNDRED),
						new PlayerAlesiaBinomial(valB/MathMacro.HUNDRED) );
			}
		};
		scoreA.computeAndOutputTable(true, m_NSAMPLE);
		ScoreTable scoreB = new ScoreTable(BinomialRHOVAL3b, BinomialRHOVAL3b,"3b","bnm","bnm"){
			@Override
			Couple<Player> GetPlayers(int valA, int valB) {
				return new Couple<Player>(
						new PlayerAlesiaBinomial(valA/MathMacro.HUNDRED),
						new PlayerAlesiaBinomial(valB/MathMacro.HUNDRED) );
			}
		};
		scoreB.computeAndOutputTable(true, m_NSAMPLE);
	}


	/** experiment 2 (April-18) **/
	private void testBinomialAgainstUnif() {
		ScoreTable score = new ScoreTable(BinomialRHOVAL2, VALUNIF2,"2","bnm","uni"){
			@Override
			Couple<Player> GetPlayers(int valA, int valB) {
				return new Couple<Player>(
						new PlayerAlesiaBinomial(valA/100.),
						new PlayerAlesiaUnifRange(valB) );
			}
		};
		score.computeAndOutputTable(false, m_NSAMPLE);
	}

	/** experiment 1 (April-18) **/
	private void CompareVariousUnif() {
		ScoreTable score = new ScoreTable(VALDUMMY, VALDUMMY,"1","uni","uni"){

			@Override
			Couple<Player> GetPlayers(int valA, int valB) {
				return new Couple<Player>(
						new PlayerAlesiaUnifRange(valA),
						new PlayerAlesiaUnifRange(valB)  );
			}

		};
		score.computeAndOutputTable(true, m_NSAMPLE);
	}

	/** information of the game is in var. game **/
	MatchStatistics MakeStats(AlesiaGameEngine game){
		int sz= outcome.values().length;
		int [] stat= new int[sz];
		// main loop
		for (int sample=0; sample < m_NSAMPLE; sample++){
			outcome res= game.Play();
			if (m_prnlevel>=2)
				game.PrintRes(res );
			int index= res.ordinal();
			stat[index]++;
			if(PROGRESSBAR){
				if (sample%10==0){
					Macro.printNOCR(".");
				}		
				if (sample%100==0){
					String percent= String.format("%.2f", 100.*sample/(float)m_NSAMPLE);
					Macro.printNOCR("["+ percent + "%]" );
				}}
		}
		Macro.CR();
		Macro.SeparationLine();
		Macro.print(MATCHRES);
		String matchAB= String.format("A:%s vs. B:%s ", 
				game.m_playerA.GetName(), game.m_playerB.GetName());
		Macro.print(matchAB);	
		Macro.CR();
		for (int p=1; p < outcome.values().length;p++  ){
			int count= stat[p];
			if (m_prnlevel>=1){
				String type= outcome.values()[p].toString();
				Macro.fPrint(" %.5s -> %4d / %d ", type, count, m_NSAMPLE);
			}
		}
		// digest
		int wa= stat[outcome.Awin.ordinal()] + 
				stat[outcome.AWbE.ordinal()] ;
		int wb= stat[outcome.Bwin.ordinal()] + 
				stat[outcome.BWbE.ordinal()] ;
		int d= stat[outcome.DRAW.ordinal()];
		MatchStatistics res= new MatchStatistics(wa,wb,d,m_NSAMPLE);
		return res;
	}

	static class MatchStatistics {

		static MatchStatistics EMPTY= new MatchStatistics(0, 0, 0, 1);

		public MatchStatistics(double wA, double wB, double drawAB, int nsample) {
			winA= wA/ (float)nsample;
			winB= wB/ (float)nsample;
			draw= drawAB/ (float)nsample;			
		}

		public double avGain() {
			return winA - winB;
		}

		public void PrintInLine() {
			Macro.SeparationLine();
			double g = avGain();
			String 
			s1= String.format("  egA:%.3f", g),
			s2 = String.format(PRECISION + " [" + PRECISION + "]  "+ 
					PRECISION + "  ",winA, draw, winB);
			Macro.print(s2 + s1);
			//Macro.print(s1);
			Macro.SeparationLine();
		}

		double winA, winB, draw;

	}


	/** tests one game **/
	static void OnePlay(Player playA, Player playB) {
		m_prnlevel= 3;
		AlesiaGameEngine game = new AlesiaGameEngine();
		game.InitPlayers(playA,playB);
		outcome res=game.Play();
		game.PrintRes(res);	
	}

	void StatsTwoPlayers(Player playA, Player playB) {
		MakeAndPrintStats(playA, playB);		
	}

	MatchStatistics MeasureStatTwoPlayers(Player playerA, Player playerB) {
		AlesiaGameEngine game = new AlesiaGameEngine();
		game.InitPlayers(playerA,playerB);
		return MakeStats(game);
	}


	/** two opponents only **/
	void MakeAndPrintStats(Player playerA, Player playerB) {
		MatchStatistics stats= MeasureStatTwoPlayers(playerA, playerB);
		stats.PrintInLine();				
	}


}
