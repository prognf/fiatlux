package alesia.erste;

public class PlayerAlesiaUnifRange extends Player {

	int m_range; //max value parameter
	
	protected PlayerAlesiaUnifRange(int maxval) {
		super("Range"+maxval);
		m_range= maxval;
	}

	int Play(){
		int range= Math.min(MyVal(), m_range);
		int playIA= RandomInt(range)+1;		
		return playIA;
	}
	
}
