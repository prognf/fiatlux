package alesia.erste;

public class PlayerBeatFixedRange extends Player {

	protected PlayerBeatFixedRange() {
		super("BeatRange");
	}

	int Play(){
		int playIA;
		if (Front()==-2){
			int opval= OpponentVal();
			playIA=Math.min(MyVal(), opval);
		} else {
			playIA=Math.min(MyVal(),1);
		}
		return playIA;
	}

}
