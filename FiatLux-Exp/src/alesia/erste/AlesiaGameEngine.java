package alesia.erste;

import java.util.concurrent.atomic.AtomicInteger;

import alesia.erste.Player.POSITION;
import components.randomNumbers.FLRandomGenerator;
import main.Macro;
import models.CAmodels.alesia.AlesiaModel;

/*-------------------------------------------------------------------------------
- 
- @author Nazim Fates
------------------------------------------------------------------------------*/
public class AlesiaGameEngine {

	//final static String CLASSNAME= 
	//		new Macro.CurrentClassGetter().getClassName();

	// parameters of the game
	public static final int W= AlesiaModel.W, N= 50, F= 2 * W +1;

	private static final boolean CHEAT = false;

	enum outcome {GOON, DRAW, Awin, Bwin, AWbE, BWbE}

	//--------------------------------------------------------------------------
	// attributes
	//--------------------------------------------------------------------------

	final int m_iniA, m_iniB, m_iniF; 
	private AtomicInteger m_troopA, m_troopB;
	protected Player m_playerA, m_playerB;
	int m_front;
	int m_time;

	FLRandomGenerator m_rand= 
			FLRandomGenerator.GetGlobalRandomizer();

	/*---------------------------------------------------------------------------
		  - construction
		  --------------------------------------------------------------------------*/

	/** initial condition of the game **/
	public AlesiaGameEngine() {
		this(N,0,N);
	}

	AlesiaGameEngine(int iniA, int iniF, int iniB){
		m_troopA= new AtomicInteger();
		m_troopB= new AtomicInteger();
		m_iniA= iniA;
		m_iniF= iniF;
		m_iniB= iniB;
	}

	void InitPlayers(Player playerA, Player playerB){
		//m_playerA= new HumanPlayer("NzM");
		m_playerA= playerA;
		m_playerB= playerB;
		// mandatory init
		m_playerA.SetInfo(this, m_troopA, m_troopB, POSITION.Astand);
		m_playerB.SetInfo(this, m_troopB, m_troopA, POSITION.Bstand);
	}


	/** default init **/
	public void InitGame(){		
		m_time=0;
		m_front= m_iniF;	
		m_troopA.set(m_iniA);
		m_troopB.set(m_iniB);
		m_playerA.sig_Reset();
		m_playerB.sig_Reset();
	}

	//---------------------------------------------------------------------------
	//- game
	// --------------------------------------------------------------------------*/

	void PrintRes(outcome res) {
		String s= " Result: " + 
				ReadResult(res,GetNameA(),GetNameB());
		Macro.print("---");
		Macro.print(s);
		Macro.print("---");
		Macro.CR();
	}

	private String GetNameA() {
		return m_playerA.m_name;
	}
	private String GetNameB() {
		return m_playerB.m_name;
	}

	outcome Play(){
		InitGame();
		if (AlesiaTournament.m_prnlevel>=3){
			PrintState();
		}
		outcome game= outcome.GOON;
		while (game == outcome.GOON){
			m_time++;
			//PrintState();
			int pA= m_playerA.Play();
			if( CHEAT){
				Macro.print("plays>  A:" + pA);
			}
			int pB= m_playerB.Play();
			if (AlesiaTournament.m_prnlevel>=3)
				Macro.fPrint("%2d)                                (%2d", pA, pB);
			if (CheckValid(pA,pB)){
				Combat(pA,pB);	
				game= WhoWins();
				if (AlesiaTournament.m_prnlevel>=3){
					this.PrintState();
				}
			} else {
				String s= String.format(
						" Error !!! Non valid move !! A(%s) has:%d  plays:%d || B(%s) has:%d plays:%d",
						m_playerA.GetName(), m_troopA.get(), pA, 
						m_playerB.GetName(), m_troopB.get(), pB);
				Macro.FatalError(s);
			}
		}
		return game; 
	}

	private boolean CheckValid(int pA, int pB) {
		return (pA>0) && (pB>0) && 
				(pA <= m_troopA.get()) && (pB<=m_troopB.get());
	}
	
	/*---------------------------------------------------------------------------
	- core
	--------------------------------------------------------------------------*/

	/** strong convention : game ends when ONE of the player is ZERO **/
	private outcome WhoWins() {

		if (m_front> W){
			return outcome.Awin;
		} else if (m_front< -W){
			return outcome.Bwin;
		} else if ( (TrA()<=0) || (TrB()<=0) ){//et le combat cessa...
			if (m_front-TrB() < -W){
				return outcome.BWbE;
			} else if (m_front+ TrA() > W){
				return outcome.AWbE;
			} else{
				return outcome.DRAW;
			}				
		}
		return outcome.GOON;
	}

	/*---------------------------------------------------------------------------
	 *- get & set
	 *--------------------------------------------------------------------------*/

	/* absolute = advantage of player A (-2 to 2) */
	public int AbsoluteFrontPos() {
		return m_front;
	}

	private int TrA() {
		return m_troopA.get();
	}

	private int TrB() {
		return m_troopB.get();
	}

	void PrintState() {
		StringBuilder front= new StringBuilder();
		front.append("[");
		for (int i=0; i < 2 * W + 1;i++){ 
			front.append('.');
		}
		front.append("]");
		front.setCharAt(m_front + 1 + W, 'X');

		Macro.fPrint("%d |>%s<|| %s |||>%s<| %d", 
				TrA(), GetNameA(),
				front.toString(), 
				GetNameB(), TrB());

	}

	//---------------------------------------------------------------------------
	//- actions
	//--------------------------------------------------------------------------*/



	private void Combat(int pA, int pB) {
		if (pA>pB){
			m_front= m_front + 1;
		} else if (pA<pB) {
			m_front= m_front - 1;
		}
		m_troopA.set( TrA()- pA );
		m_troopB.set( TrB()- pB );
	}



	private static String ReadResult(outcome res, String nameA, String nameB) {
		switch(res){
		case DRAW:		
			return "Draw";
		case Awin:
			return nameA + " wins";
		case Bwin:
			return nameB + " wins";
		case AWbE: 
			return nameA + " wins by exhausting " + nameB;
		case BWbE:
			return nameB + " wins by exhausting " + nameA;
		default:
			return "problem !!!";
		}
	}

	static int NTEST=100000;
	public int [] TestDistributionPlayerA() {
		int [] stat = new int[AlesiaGameEngine.N+1];
		for (int i=0; i< NTEST; i++){
			int p = m_playerA.Play();
			stat[p]++;
		}
		return stat;
	}	

}
