package alesia.erste;

import java.util.Scanner;

import main.Macro;

public class zzPlayerHuman extends Player {

	private static final boolean CHEAT = true; // cheat for human player

	protected zzPlayerHuman(String name) {
		super(name);
	}

	
	@Override
	int Play() {
		if (CHEAT)
			Macro.print(" Mise du joueur " + m_name + ":");
		Scanner scan= new Scanner(System.in); 
		int val = scan.nextInt(); 
		return val;
	}

	
}
