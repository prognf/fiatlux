package alesia.erste;

import java.util.concurrent.atomic.AtomicInteger;

import main.Macro;
abstract public class Player {

	abstract int Play();
	
	
	enum POSITION { Astand, Bstand}

    POSITION m_position; // A or B ?
	String m_name;
	private AlesiaGameEngine m_engine;
	private AtomicInteger m_myTroop, m_OpponentTroop;
	

	protected Player(String name) {
		m_name= name;		
	}

	/** part of the construction : mandatory call */
	public void SetInfo(AlesiaGameEngine engine, 
			AtomicInteger me, AtomicInteger other, POSITION position){
		m_engine= engine;
		m_myTroop= me;
		m_OpponentTroop= other;
		m_position= position;
	}
	
	
	protected int MyVal() {
		return m_myTroop.get();
	}

	protected int OpponentVal(){
		return m_OpponentTroop.get();
	}
	
	/** returns the RELATIVE position of the front **/
	protected int Front(){
		switch (m_position){
		case Astand:
			return m_engine.AbsoluteFrontPos();
		case Bstand:
			return - m_engine.AbsoluteFrontPos();
		}
		Macro.FatalError("It seems that m_position was not initialized ");
		return -999999;
	}
	
	/** relative position of the front + gamewidth */
	protected int FrontWithOffset(){
		return Front() + AlesiaGameEngine.W;
	}
	
	protected int RandomInt(int range) {
		return m_engine.m_rand.RandomInt(range);
	}
	
	//called by NashPlayer 
	protected float RandomFloat() {
		return m_engine.m_rand.RandomFloat();
	}
	
	
	public Object GetName() {
		return m_name;
	}

	/** override if needed **/
	/*public void InformMoveOpponent(int pB) {
		// nothing		
	}*/

	/** override if needed **/
	public void sig_Reset() {
		// nothing		
	}

}
