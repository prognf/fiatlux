package alesia.erste;

import experiment.measuring.general.DensityMD;
import experiment.samplers.CAsampler;
import main.Macro;
import main.MathMacro;
import models.CAmodels.alesia.AlesiaModelQiqiHouhou;
import models.CAmodels.alesia.GateCAinitializer;

public class PlayerAlesiaQiqiHouhou extends Player {

	protected PlayerAlesiaQiqiHouhou(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	private static final int Ncell = 2*AlesiaGameEngine.N+10;
	private static final int Nsteps = AlesiaGameEngine.N + 4;
	
	CAsampler m_samp;
	GateCAinitializer m_init;
	DensityMD m_dens= new DensityMD();

	/** Q2H2 model with pA= 0 **/
	/*public PlayerAlesiaQiqiHouhou(double pVol, double pTrans) {
		super(MakeName(pVol,pTrans));
		LinearTopology topo= new RadiusN_Topology(1);
		AlesiaModelQiqiHouhou model= new AlesiaModelQiqiHouhou();
		double pAbs=0.; // 
		model.setpVpTpA(pVol, pTrans, pAbs);
		
		m_samp= new LinearSampler(Ncell, model, topo);
		m_init= new GateCAinitializer();
		m_samp.SetInitDeviceFromOutside(m_init);	
		m_dens.LinkTo(m_samp);
	}*/

	private static String MakeName(double pVol, double pTrans) {
		return String.format("QiqiHouhou-pV%d-pT%d",
				MathMacro.percentFromRatio(pVol),MathMacro.percentFromRatio(pTrans));
	}

	public int Play(){
		int a=MyVal(), front=1, b=OpponentVal();     // WARNING front ==1 puts only one cell
		m_init.SetInput(a,front,b); 
		//m_samp.GetAutomaton().PrintStateInfo();
		m_samp.sig_Init();
		m_samp.NstepsAndUpdate(Nsteps);
		int nV= m_dens.CountState(AlesiaModelQiqiHouhou.V);
		int playIA= (nV==0)?1:nV; // plays at least one token
		if (playIA>a) {
			String sPlayCA= String.format(" $$$$ playing %d / %d init  (%d,%d,%d)", 
					playIA, a, a, front, b);
						
			Macro.print(sPlayCA);
			//m_samp.GetAutomaton().PrintStateInfo();
			Macro.FatalError("playing more than one has!!");
		}
		return playIA;
	}

}
