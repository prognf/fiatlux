package alesia.erste;

import java.io.IOException;

import main.Macro;
import main.MathMacro;

import components.types.FLString;
import components.types.FLStringList;

public class PlayerAlesiaNashMatrix extends Player {

	private static final String NAME = "NashMatrix";
	private static final String FILEDAT = "brunoplayer/matrix-m"+AlesiaGameEngine.N+"-n"+AlesiaGameEngine.F+ ".dat";

	PolicyDat [][][] m_policy ;

	protected PlayerAlesiaNashMatrix() {
		super(NAME);
		try {
			LoadPolicy();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void LoadPolicy() throws IOException {
		FLStringList rawdata= FLStringList.ReadFile(FILEDAT);
		m_policy = new PolicyDat [AlesiaGameEngine.N+1][AlesiaGameEngine.N+1][AlesiaGameEngine.F];
		for (String readline : rawdata){
			int probapos=readline.indexOf('[');
			String probatab=readline.substring(probapos+2, readline.length()-2);
			String entry=readline.substring(0, probapos-1);
			String [] token = entry.split(" ");
			int pA= FLString.String2Int(token[0]);
			int f= /*AlesiaGameEngine.F - 1 -*/ FLString.String2Int(token[1]);		
			int pB= FLString.String2Int(token[2]);
			//String input= FLString.ToString(token,"|");
		 	//Macro.fDebug("feed:(%d %d %d)>> $%s$", pA, f, pB, probatab );
			try{
				m_policy[pA][pB][f]= new PolicyDat(probatab,pA);
			} catch( Exception e){
				String str= String.format(" problem in entry: (%d,%d,%d)", pA, f, pB);
				Macro.fPrint(str);
			}
		}
		//PrintPolicy(7,2,8);
	}

	private void PrintPolicy(int pA, int f, int pB) {
		String s = String.format(" (%d,%d,%d) ", pA, f, pB);
		double [] pol= GetPolicy(pA, pB, f);
		
		Macro.fPrint(" %s >> %s ",s, MathMacro.arraydoubleToString(pol,":") );
	}

	private double [] GetPolicy(int pA, int pB, int f) {
		try{
			return m_policy[pA][pB][f].m_prob;
		} catch( Exception e){
			Macro.fPrint(" problem when calling policy item: (%d,%d,%d)", pA, f, pB);
			return null;
		}

	}


	@Override
	int Play() {
		////// UNCERTAINTY HERE !!!
		int pA= MyVal(), pB= OpponentVal(), f= AlesiaGameEngine.F - 1 - FrontWithOffset();
		double [] probaTab = GetPolicy(pA, pB, f);
		//String distr= FLString.ToString(probaTab);
		//String stg= String.format(" %d,%d <%d>",pA,pB,f);
		//Macro.Debug(stg + " dd:" + distr);
		int play= MathMacro.RandomSelect(probaTab, RandomFloat());
		if ((play>1000) || (play<=0)) {
			Macro.fPrint("oulala bad play ; check (%d,%d,%d)",pA,f,pB);
			Macro.FatalError("stopping everything...");
		}
		return play;
	}

	static int NTEST=100000;
	public void TestDistributions() {
		int [] stat = new int[AlesiaGameEngine.N+1];
		for (int i=0; i< NTEST; i++){
			int p = Play();
			stat[p]++;
		}
		String st= FLString.ToString(stat);
		Macro.print(" Distrib :" + st);
		
	}

/** OLD FIX
 * 
 * 	//ad hoc correction
		int pA= MyVal(), pB= OpponentVal(), f= FrontWithOffset();
		if ((pA==42)&&(pB==44)&&(f==2*AlesiaGameEngine.W)){
			f--;
		}
		if ((pA==44)&&(pB==42)&&(f==0)){
			f++;
		}
	
 */

}
