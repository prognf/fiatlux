package alesia.erste;

import alesia.erste.AlesiaTournament.MatchStatistics;
import components.types.Couple;
import components.types.FLString;
import components.types.FLStringList;


/** inner class */
abstract class ScoreTable {

	abstract Couple<Player> GetPlayers(int valA, int valB);

	MatchStatistics [][] stat;
	int [] m_ival, m_jval;
	int m_isize, m_jsize;
	AlesiaTournament m_tournament;

	// additional info
	String m_suffix="";
	String m_nameParA="", m_nameParB="";
	private String m_infoLines="", m_infoCol=""; // first line of Latex table
	
	ScoreTable(int [] ival, int [] jval, String suffix, String nameParA, String nameParB){
		m_ival= ival; 
		m_jval= jval;
		m_isize= ival.length;
		m_jsize= jval.length;
		stat= new MatchStatistics[m_isize][m_jsize];
		m_suffix= suffix; // for the output
		m_nameParA= nameParA;
		m_nameParB= nameParB;
	}

	/** "old style" **/
	public ScoreTable(int[] ival, int[] jval, String suffix) {
		this(ival, jval, suffix, "hc"+suffix, "hl"+suffix);
	}

	public void computeAndOutputTable(boolean removeDiag, int nsample) {
		m_tournament= new AlesiaTournament(nsample);
		for (int i=0; i<m_isize; i++){
			for (int j=0; j<m_jsize; j++){
				if ( (removeDiag) && (i==j) ){
					stat[i][j]= MatchStatistics.EMPTY;
				}
				else {
					int valI= m_ival[i], valJ= m_jval[j];
					Couple<Player> players= GetPlayers(valI,valJ);

					stat[i][j]= m_tournament.MeasureStatTwoPlayers(players.e1, players.e2);
				}
			}
		}			
		outputGainTable(removeDiag);
	}

	public MatchStatistics Get(int i, int j) {
		return stat[i][j];
	}	

	/** prints table **/
	void outputGainTable(boolean removeDiag) {
		FLStringList tbl = new FLStringList();
		// head
		tbl.add("\\begin{tabular}{c | " + FLString.repeatStr("r",m_jsize)  + "} ");
		StringBuilder head= new StringBuilder(String.format("%14s &", m_infoLines)); // left col
		for (int j=0; j<m_jsize; j++){
			int colval= m_jval[j];
			String scol = String.format("\\%s{%d}",m_nameParB, colval);// columns tag
			head.append(String.format("%12s%s ", scol, Separ(j)));
		}
		tbl.Add(head.toString());
		tbl.Add("\\hline");
		// lines
		for (int i=0; i<m_isize; i++){
			int lineval=m_ival[i];
			StringBuilder s = new StringBuilder(String.format("\\%s{%d} &", m_nameParA, lineval)); // left col, lines tag
			s = new StringBuilder(String.format("%16s", s.toString()));
			for (int j=0; j<m_jsize; j++){
				if ((j==i) && removeDiag ){
					s.append(FLString.repeatStr(" ", 14)).append(Separ(j));
				} else {
					double gain= Get(i,j).avGain();
					s.append(String.format("%11.3f  ", gain));
					s.append(Separ(j));
				}
			}
			tbl.Add(s.toString());
		}		
		tbl.Add("\\end{tabular}");
		tbl.Add("%NS=" + m_tournament.m_NSAMPLE);
		tbl.WriteToFile("exp-output-"+ m_suffix + ".dat");
		tbl.Print();
	}

	private String Separ(int j) {
		return (j<m_jsize-1)?" &":"\\\\";
	}

	public void setInfoInFirstLine(String infoLines, String infoCol) {
		m_infoLines=infoLines;
		m_infoCol=infoCol;
	}


}

