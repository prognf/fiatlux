package alesia.erste;

import components.types.FLString;
import main.Macro;
/** array of double **/
public class PolicyDat {

	static boolean prnMsg; //for showing problems

	double [] m_prob; 

	/** proba P_k to play k, with k in [0..playSize]; convention P_0=0 **/
	public PolicyDat(int playSize){
		m_prob= new double[playSize+1];
	}

	/** parsing the String probatab 
	 * format BrunoScherrer data **/
	public PolicyDat(String lineprobatab, int dataSize) {
		this(dataSize);
		String [] probatab= lineprobatab.split(" ");
		double sum=0;
		// WARNING : we start from index 1 !!!!
		for (int i=1; i <= dataSize ;i++){
			m_prob[i]= FLString.String2Double( probatab[i-1] );
			sum+= m_prob[i];
		}
		if (sum<1.-Macro.EPSILON){
			if (!prnMsg){
				Macro.fPrint(" Policydat : completing to 1 the sum : %.10g  for line:%s", sum, lineprobatab);
				//prnMsg=true;
			}
			int idmax=0;
			for (int id=1;id<m_prob.length;id++){
				if (m_prob[id]>m_prob[idmax]){
					idmax=id;
				}
				m_prob[idmax] += 1. - sum ; // adding the difference
			}
		}
	}

	/** construction from counts to frequencies given the number of samples Z**/
	public PolicyDat(int a,int[] countTab, int Z) {
		this(a);
		if (countTab.length != a+1) {
			Macro.FatalError("check convention with countTab");
		}
		if (countTab[0]!=0) {
			Macro.FatalError(" The stat array should have value at index 0 equal to zero");
		}
		for (int k=0; k<countTab.length; k++) {
			m_prob[k]= (double)countTab[k]/Z;
		}
	}

	public String toString() {
		String s="[";
		for (int i=1;i<m_prob.length-1; i++) {
			s+= m_prob[i] + " ";
		}
		s+= m_prob[m_prob.length-1] + "]";
		return s;
	}

}

