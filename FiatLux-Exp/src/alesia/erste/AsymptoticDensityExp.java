package alesia.erste;

import components.types.FLStringList;
import components.types.RuleCode;
import experiment.measuring.general.DensityMD;
import experiment.samplers.CAsampler;
import experiment.samplers.LinearSampler;
import main.Macro;
import main.commands.CommandParser;
import models.CAmodels.stochastic.DiploidECAmodel;
import topology.basics.SamplerInfoLinear;
import topology.basics.TopologyCoder;

public class AsymptoticDensityExp {

	CAsampler m_sampler;
	private DensityMD m_MeasuringDevice;
	private DiploidECAmodel m_model;
	
	String m_output_filename;
	
	final static int N=1000;
	private static final RuleCode 
	R1= new RuleCode(204),
	R2= new RuleCode(50);
	private static final int TMAX = 1000;

	public AsymptoticDensityExp() {
		String 	topoCode = TopologyCoder.s_TLR1;
		m_model= new DiploidECAmodel(R1, R2);
		SamplerInfoLinear info= new SamplerInfoLinear(topoCode, m_model, N);
		m_sampler= new LinearSampler(info);
		m_MeasuringDevice= new DensityMD();
		m_MeasuringDevice.LinkTo(m_sampler);
	}
	
	public String DoExp(double alpha){
		m_model.SetWeightRule2(alpha);
		m_sampler.sig_Init();
		int seedI= m_sampler.GetInitSeed(),
			seedM= m_sampler.GetModelSeed();
		for (int t=0; t<TMAX; t++){
			m_sampler.sig_NextStep();
		}
		double densAsympt= m_MeasuringDevice.GetMeasure();
		String strMeasure= String.format("alpha:%.3f  dens:%.3f   seed I,M: %d %d", 
				alpha, densAsympt, seedI, seedM);
		return strMeasure;
	}
	
	final static int NDIV=10;
	
	private void Test() {
		FLStringList output= new FLStringList();
		for (int i=0; i<=NDIV;i++){
			double alpha=i/(double)NDIV;
			output.Add( DoExp(alpha) );
		}
		output.Print();
	}
	
	
	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();
	protected static final int NARG = 0;

	public static void main(String [] argv){
		CommandParser parser = new CommandParser(CLASSNAME, "", argv);
		if (argv.length == NARG ){
			//int M= parser.IParse("M=");
			AsymptoticDensityExp exp= new AsymptoticDensityExp();
			exp.Test();
		} else {
			parser.Usage("?");
		}
	}

}
