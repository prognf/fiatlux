package alesia.erste;

import experiment.measuring.general.DensityMD;
import experiment.samplers.CAsampler;
import experiment.samplers.LinearSampler;
import initializers.CAinit.BinaryInitializer;
import main.MathMacro;
import models.CAmodels.alesia.AlesiaBinomialCAmodel;
import topology.basics.LinearTopology;
import topology.zoo.RadiusN_Topology;

public class PlayerAlesiaBinomial extends Player {

	private static final int Ncell = AlesiaGameEngine.N;
	private static final int Nsteps = 1;
	private static final String NAME = "Binomial";
		
	final double m_rho;
	
	CAsampler m_samp;
	BinaryInitializer m_init;
	DensityMD m_dens= new DensityMD();
	AlesiaBinomialCAmodel m_model;
	
	/** rho = damping param. **/
	public PlayerAlesiaBinomial(double rho) {
		super(NAME+MathMacro.percentFromRatio(rho));
		m_rho= rho;
		LinearTopology topo= new RadiusN_Topology(1);
		m_model= new AlesiaBinomialCAmodel();
		m_model.SetDamping(m_rho);
		m_samp= new LinearSampler(Ncell, m_model, topo);
		m_init= new BinaryInitializer();
		
		m_samp.SetInitDeviceFromOutside(m_init);	
		m_dens.LinkTo(m_samp);
	}

	int Play(){
		//return NormalCAPlay();
		return cheatPlay();
	}

	private int NormalCAPlay() {
		m_init.SetInitCode("R"+MyVal());
		m_samp.sig_Init();
		//int d0= m_dens.CountNonZero();
		m_samp.NstepsAndUpdate(Nsteps);
		//double dm = m_dens.GetMeasure();
		int d1= m_dens.CountNonZero();
		//String sPlayCA= String.format(" count: %d -> %d %.2f",d0,d1,dm);
		//Macro.print(sPlayCA);
		int playIA=(d1==0?1:d1);
		return playIA;
	}
	
	private int cheatPlay() {
		int sum=0;
		for (int i=0; i<MyVal();i++) {
			sum+=m_model.RandomEventDouble(m_rho)?1:0;
		}
		int playIA=(sum==0?1:sum);
		return playIA;
	}


}
