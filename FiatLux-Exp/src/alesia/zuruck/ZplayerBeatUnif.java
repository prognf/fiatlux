package alesia.zuruck;

public class ZplayerBeatUnif extends ZplayerAlesia {

	private static final String NAME = "BeatUnif";

	public ZplayerBeatUnif() {
		super(NAME);
	}

	@Override
	/** convention for w : w=0 position of loosing, w=F-1 position of winning **/
	int PlayZ(int a, int w, int b) {
		return (w==0)?Math.min(a,b):Math.min(a,4);
	}

}
