package alesia.zuruck;

public class ZplayerUnifM extends ZplayerAlesia {

	private static final String NAME = "UnifM";

	int m_valSat; /// saturation= M
	
	public ZplayerUnifM(int valSat) {
		super(NAME+valSat);
		m_valSat = valSat;
	}

	@Override
	/** convention for w : w=0 position of loosing, w=F-1 position of winning **/
	int PlayZ(int a, int w, int b) {
		int rnd= 1 + RandomInt(a);
		return Math.min(rnd,m_valSat);
	}

}
