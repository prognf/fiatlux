package alesia.zuruck;

import alesia.erste.PolicyDat;
import main.Macro;
import main.commands.CommandInterpreter;
import main.commands.CommandParser;
import models.CAmodels.alesia.AlesiaModel;

/*-------------------------------------------------------------------------------
- 
------------------------------------------------------------------------------*/
class AlesiaDecomposition extends CommandInterpreter {

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();

	// parameters of the game
	public static final int W= AlesiaModel.W, N= 50, F= 2 * W +1;
	// for empirical distributions
	public static final int Z= 1000;

	//--------------------------------------------------------------------------
	//- attributes
	//--------------------------------------------------------------------------

	double [][][] m_gainExpect; // gain expectation of a given state
	PolicyDat [][][] m_playAemp, m_playBemp; // empirical playing distributions
	PolicyDat [][][] m_playAana, m_playBana; // analytical playing distributions

	//--------------------------------------------------------------------------
	//- construction
	//--------------------------------------------------------------------------

	public AlesiaDecomposition() {
		m_gainExpect= new double[N+1][F][N+1];
		m_playAemp= new PolicyDat[N+1][F][N+1];
		m_playBemp= new PolicyDat[N+1][F][N+1];
		m_playAana= new PolicyDat[N+1][F][N+1];
		m_playBana= new PolicyDat[N+1][F][N+1];
	}

	//--------------------------------------------------------------------------
	//- building tables
	//--------------------------------------------------------------------------

	void BuildEmpiricalTables(ZplayerAlesia playA, ZplayerAlesia playB) {
		for (int a=1; a<=N; a++) {
			for (int w=0; w<F; w++) {
				for (int b=1; b<=N; b++) {
					BuildTableState(playA, playB, a, w, b);
				}
			}
		}
	}

	/** convention for w : w=0 position of loosing w=F position of winning**/
	private void BuildTableState(ZplayerAlesia playA, ZplayerAlesia playB, int a, int w, int b) {
		int [] countTab= new int[a+1];
		for (int i=0; i<Z; i++) {
			// experimenting...
			int tokenPlayed= playA.PlayZ(a,w,b);
			countTab[tokenPlayed]++;
		}
		PolicyDat distrA= new PolicyDat(a, countTab, Z);
		m_playAemp[a][w][b]= distrA; 
		Macro.fPrint(" building state (%d,%d,%d) with %d samples >> %S", a,w,b, Z, distrA);
	}


	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------

	static int [] UNIFVAL = { 10, 15, 20, 25 };
	
	private static void Test4(int nSample) {
		for (int i=0; i< UNIFVAL.length; i++) {
			for (int j=i+1; j<UNIFVAL.length; j++) {
				ZplayerAlesia 
					playA= new ZplayerUnifM(i),
					playB= new ZplayerUnifM(j);
//				double resAB= ;
			}
		}
	}
	

	private static void Test3(int nSample) {
		ZplayerAlesia 
			playA= new ZplayerUnif(),
			playB= new ZplayerBeatUnif();
		ZaleziaGameEngine game= new ZaleziaGameEngine(playA, playB);
		ZaleziaGameEngine.FOLLOWGAME= false; // no prn
		ZMatchStatistics.MakeStats(game, nSample);
	}

	private static void Test2() {
		ZplayerAlesia 
		playA= new ZplayerUnif(),
		playB= new ZplayerBeatUnif();
		ZaleziaGameEngine game= new ZaleziaGameEngine(playA, playB);
		game.Initgame();
		game.PlayGame();
	}

	private static void Test1() {
		AlesiaDecomposition exp = new AlesiaDecomposition();
		ZplayerAlesia unif= new ZplayerUnif();
		ZplayerAlesia beatUnif= new ZplayerBeatUnif();

		exp.BuildEmpiricalTables(beatUnif, null);
	}

	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------

	/** main : processing command line * */
	public static void main(String[] argv) {
		//Test1();	
		//Test2(); // one game
		CommandParser parser = new CommandParser(CLASSNAME, "", argv);
		int Z=parser.IParse("Z="); 
		Test3(Z); // stats
	}


}
