package alesia.zuruck;

import alesia.zuruck.ZaleziaGameEngine.GAMESTATE;
import main.Macro;

public class ZMatchStatistics {


	public ZMatchStatistics() {
	}

	/** information of the game is in var. game **/
	static double MakeStats(ZaleziaGameEngine game, int nSample){
		int sz= GAMESTATE.values().length;
		int [] stat= new int[sz];
		// main loop
		for (int sample=0; sample < nSample; sample++){
			GAMESTATE res= game.PlayGame();
			int index= res.ordinal();
			stat[index]++;
			// use progress bar ??
		}
		// printing result
		for (int p=1; p < GAMESTATE.values().length;p++  ){//state 0=GOON
			int count= stat[p];
			String type= GAMESTATE.values()[p].toString();
			Macro.fPrint(" %.5s -> %4d / %d ", type, count, nSample);
		}
		// digest
		int wa= stat[GAMESTATE.Awin.ordinal()] + 
				stat[GAMESTATE.AWbE.ordinal()] ;
		int wb= stat[GAMESTATE.Bwin.ordinal()] + 
				stat[GAMESTATE.BWbE.ordinal()] ;
		int d= stat[GAMESTATE.DRAW.ordinal()];
		double avrGain=(double) (wa - wb) / nSample; 
		Macro.fPrint(" DIGEST avgGain:%.3f W:%d L:%d  D:%d", avrGain, wa, wb, d);
		return avrGain;
	}

}
