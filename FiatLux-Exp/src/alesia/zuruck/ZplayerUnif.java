package alesia.zuruck;

public class ZplayerUnif extends ZplayerAlesia {

	private static final String NAME = "Unif";

	public ZplayerUnif() {
		super(NAME);
	}

	@Override
	/** convention for w : w=0 position of loosing, w=F-1 position of winning **/
	int PlayZ(int a, int w, int b) {
		return 1+RandomInt(a);
	}

}
