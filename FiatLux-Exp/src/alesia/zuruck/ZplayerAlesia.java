package alesia.zuruck;

import components.randomNumbers.StochasticSource;

abstract public class ZplayerAlesia extends StochasticSource {

	abstract int PlayZ(int a, int w, int b);
	
	String m_name;
	
	public ZplayerAlesia(String name) {
		m_name= name;
	}
	
}
