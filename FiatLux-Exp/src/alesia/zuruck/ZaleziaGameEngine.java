package alesia.zuruck;

import main.Macro;
import models.CAmodels.alesia.AlesiaModel;

public class ZaleziaGameEngine {

	
	static boolean FOLLOWGAME= true;
	
	// parameters of the game
	public static final int W= AlesiaModel.W, F= 2 * W +1;
	public static final int N= 50;	
	
	ZplayerAlesia m_playerA, m_playerB;
	private int m_a, m_w, m_b;

	enum GAMESTATE {GOON, DRAW, Awin, AWbE, Bwin, BWbE}

	
	public ZaleziaGameEngine(ZplayerAlesia playerA, ZplayerAlesia playerB) {
		m_playerA= playerA;
		m_playerB= playerB;
		Macro.fPrint("New game : %s VS. %s", playerA.m_name, playerB.m_name);
	}
	
	
	public void Initgame() {
		setState(N,W,N);
		Macro.fPrint(FOLLOWGAME, "Init game : %s", getGameState());
	}
	
	private String getGameState() {
		return String.format(" (%d,%d,%d)", m_a, m_w, m_b);
	}


	public GAMESTATE PlayGame() {
		Initgame();
		GAMESTATE gameStateType= GAMESTATE.GOON;
		while (gameStateType==GAMESTATE.GOON) {
			int pA= m_playerA.PlayZ(m_a, m_w, m_b);
			int a2= m_b, w2= F-1-m_w, b2= m_a;
			int pB= m_playerB.PlayZ(a2, w2, b2); // inversion of the state
			//UPDATING STATE
			m_a-= pA;
			m_b-= pB;
			if (pA>pB) { // A wins
				m_w++;
			} else if (pA<pB){ // B wins
				m_w--;
			} else { // equality
				// nothing to update
			}
			gameStateType= CheckState(m_a, m_w, m_b);
			Macro.fPrint(FOLLOWGAME, "play: [%2d,%2d] --> state: %s ", pA, pB, getGameState());
		}
		Macro.fPrint(FOLLOWGAME, "OUTCOME:" + gameStateType);
		return gameStateType;
	}

	private GAMESTATE CheckState(int a, int w, int b) {
		if (w==F) {
			return GAMESTATE.Awin;
		}
		if (w==-1) {
			return GAMESTATE.Bwin;
		}
		if (b==0) {
			return (a+w>F)?GAMESTATE.AWbE:GAMESTATE.DRAW;
		}
		if (a==0) {
			return (w-b<0)?GAMESTATE.BWbE:GAMESTATE.DRAW;
		}
		return GAMESTATE.GOON;
	}

	private void setState(int a, int w, int b) {
		m_a=a;
		m_w=w;
		m_b=b;
	}
	
}
