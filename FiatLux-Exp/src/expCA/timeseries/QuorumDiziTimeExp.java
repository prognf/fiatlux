package expCA.timeseries;

import java.util.Locale;

import components.types.FLStringList;
import components.types.IntC;
import experiment.measuring.general.DensityMD;
import experiment.samplers.PlanarSampler;
import main.Macro;
import main.commands.CommandParser;
import models.CAmodels.decentralisedDiagnosis.QuorumDModel;
import models.CAmodels.decentralisedDiagnosis.QuorumFrontIzi;
import models.CAmodels.decentralisedDiagnosis.QuorumFrontLacaInit;
import topology.basics.PlanarTopology;
import topology.zoo.vonNeumannTM;

public class QuorumDiziTimeExp {


	private static final int Tsim=1000;
	private static final int PMIN = 40,PMAX=70,PSTP=5;

	PlanarSampler m_sampler; // main component
	DensityMD m_measure;
	QuorumFrontIzi m_model;
	IntC m_XYsize;


	QuorumDiziTimeExp(int L){
		m_model= new QuorumFrontIzi();
		m_XYsize= new IntC(L,L);
		PlanarTopology topology= new vonNeumannTM();
		m_sampler= new PlanarSampler(m_XYsize, m_model, topology);
		m_measure= new DensityMD();
		m_measure.LinkTo(m_sampler);
	}


	void initExp(double lambda) {
		m_model.setP(lambda);
		QuorumFrontLacaInit initializer = (QuorumFrontLacaInit)m_sampler.GetInitializer();
		initializer.setInitVal(0); //one seed
		m_sampler.sig_Init();

	}

	
	/** asymptotic dens exp **/
	String sampleOne(double valP) {
		initExp(valP);
		double initD= m_measure.GetMeasure();
		//double initDA= m_measure.measureStateDensity(QuorumDModel.A);
		double initDD= m_measure.measureStateDensity(QuorumDModel.D);

		for (int i=0; i<Tsim; i++) {
			m_sampler.sig_NextStep();

		}
		double finalDA= m_measure.measureStateDensity(QuorumDModel.N);
		//double finalDD= m_measure.measureStateDensity(QuorumDModel.D);
		String res=String.format("%f %.4f %.4f", valP, finalDA, initDD);
		Macro.fPrint("%s + control init d:%.4f", res,initD);
		return res;
	}


	/*** time exp **/
	FLStringList sampleOneTimeEvolution(double lambda) {
		initExp(lambda);
		FLStringList outputRes= new FLStringList();

		addTimeRecord(outputRes);
		for (int i=0; i<Tsim; i++) {
			m_sampler.sig_NextStep();
			if (i%10==0) {
				addTimeRecord(outputRes);	
			}
		}
		return outputRes;
	}

	/** rec **/
	private void addTimeRecord(FLStringList outputRes) {
		double dN= m_measure.measureStateDensity(QuorumDModel.N);
		double dD= m_measure.measureStateDensity(QuorumDModel.D);
		int t=m_sampler.GetTime();
		String onetimesample=String.format("%d %.4f %.4f", t, dN, dD);
		outputRes.Add(onetimesample);
		Macro.print("t:"+onetimesample);
	}

	/** EXP **/
	void densityVStimeExp(){
		
		for (int pval=PMIN; pval<=PMAX;pval+=PSTP) {
			FLStringList oneTimeEvolution= sampleOneTimeEvolution(pval/100.);
			assert(m_XYsize.X() == m_XYsize.Y()); //X=Y
			int L=m_XYsize.X(); 
			String filename= String.format("dens-L%dTsim%d-p%d.txt", L, Tsim,pval);
			oneTimeEvolution.WriteToFile(filename);
		}
		
	}
	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();

	public static void main(String [] argv){
		CommandParser parser = new CommandParser(CLASSNAME, "", argv);
		int L= parser.IParse("L=");

		Locale.setDefault(Locale.US);

		QuorumDiziTimeExp exp= new QuorumDiziTimeExp(L);
		//exp.asymptoticDensExp();
		exp.densityVStimeExp();
	}


}
