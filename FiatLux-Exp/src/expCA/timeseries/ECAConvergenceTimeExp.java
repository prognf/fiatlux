package expCA.timeseries;

import components.types.FLStringList;
import components.types.RuleCode;
import experiment.measuring.general.DensityMD;
import experiment.samplers.LinearSampler;
import initializers.CAinit.BinaryInitializer;
import initializers.CAinit.BinaryInitializer.INITSTYLE;
import main.Macro;
import main.commands.CommandParser;
import models.CAmodels.tabled.ECAmodel;
import topology.basics.SamplerInfoLinear;
import topology.basics.TopologyCoder;
import updatingScheme.FullyAsynchronousScheme;

public class ECAConvergenceTimeExp {
	
	private static final String FILENAME = "convergenceTime";


	private static final RuleCode Wcode = new RuleCode(90);


	private static final int NSAMPLES = 100000;

	private static final int TMAX = 1000000;
	int N=5; // number of cells
	
	/*--------------------
	 * attributes
	 --------------------*/

	LinearSampler m_sampler;
	private DensityMD m_MeasuringDevice;
	BinaryInitializer m_init;
	double m_dini;

	

	public void buildSampler() {
		String 	topoCode = TopologyCoder.s_TLR1;
		ECAmodel model= new ECAmodel(Wcode);
		SamplerInfoLinear info= new SamplerInfoLinear(topoCode, model, N);
		m_sampler= new LinearSampler(info);
		FullyAsynchronousScheme fas= new FullyAsynchronousScheme();
		fas.m_updateMode= FullyAsynchronousScheme.UPDATEMODE.oneByone;
		m_sampler.SetUpdatingScheme(fas);
		m_init = (BinaryInitializer) m_sampler.GetInitializer();
		m_init.ForceInitStyle(INITSTYLE.Pattern);
		m_init.SetInitPattern("01000");
		m_MeasuringDevice= new DensityMD();
		m_MeasuringDevice.LinkTo(m_sampler);
	}

	/*--------------------
	 * main
	 * @return 
	 --------------------*/
	public void RunSamples(int Z){
		FLStringList samples= new FLStringList(); // for keeping track of each sample

		//int seedIni= FLRandomGenerator.GetNewSeed();
		int seedIni=18;
		m_init=(BinaryInitializer)m_sampler.GetInitializer();
		for (int sample=0; sample < Z; sample++){			
			Macro.ProgressBar(sample,Z); // 
			m_init.SetSeed(seedIni);
			// initialisation
			m_sampler.sig_Init();
	
			m_sampler.print();
			int n1= m_MeasuringDevice.CountState(1);
			Macro.fPrint("N1ini:%d", n1);
			
			int time=0;
			while ((n1 > 0 ) && (time < TMAX)){
				m_sampler.sig_NextStep();
				time++;
				n1= m_MeasuringDevice.CountState(1);
				//m_sampler.print();
			}
			String out= String.format("seedIni:%d Tconv:%d",seedIni,time);
			if (time==TMAX) {
				Macro.print("** TMAX was reached !!! ****");
			}
			Macro.print(out);
			samples.Add(""+time);
		}//for

		samples.WriteToFile(FILENAME);
	}

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();


	public static void main(String[] argv) {
		Macro.BeginTest(CLASSNAME);
		CommandParser parser= new CommandParser(CLASSNAME, argv);
		ECAConvergenceTimeExp  exp= new ECAConvergenceTimeExp();
		exp.buildSampler();
		exp.RunSamples(NSAMPLES);
		Macro.EndTest();
	}
	

}
