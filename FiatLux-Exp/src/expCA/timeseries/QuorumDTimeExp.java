package expCA.timeseries;

import java.util.Locale;

import components.types.FLStringList;
import components.types.IntC;
import experiment.measuring.general.DensityMD;
import experiment.samplers.PlanarSampler;
import main.Macro;
import main.commands.CommandParser;
import models.CAmodels.decentralisedDiagnosis.QuorumDInit;
import models.CAmodels.decentralisedDiagnosis.QuorumDModel;
import topology.basics.PlanarTopology;
import topology.zoo.vonNeumannTM;

public class QuorumDTimeExp {


	private static final int L = 100, Tsim=10000; // important
	private static final int ILMIN = 80,ILMAX=400,ILSTP=80;
	private static final int KMIN = 1, KMAX=7, KSTP=2;
	private static final double [] KAPPA_FIXEDEXP2 = {0.03,0.04,0.05,0.06};
	private static final double [] LAMBDA_FIXEDEXP3 ={0.8,1.6,2.4,3.2};
	private static final boolean PRNLINES = false;
	PlanarSampler m_sampler; // main component
	DensityMD m_measure;
	QuorumDModel m_model;


	QuorumDTimeExp(){
		m_model= new QuorumDModel();
		IntC XYsize= new IntC(L,L);
		PlanarTopology topology= new vonNeumannTM();
		m_sampler= new PlanarSampler(XYsize, m_model, topology);
		m_measure= new DensityMD();
		m_measure.LinkTo(m_sampler);
	}


	void initExp(double kappa, double lambda) {
		Macro.fPrint("Initializing exp with kappa=%.2f lambda=%.2f",kappa,lambda);
		m_model.setLambda(lambda);
		QuorumDInit initializer = (QuorumDInit)m_sampler.GetInitializer();
		initializer.setDfailure(kappa);
		m_sampler.sig_Init();

	}

	/*** time exp **/
	FLStringList sampleOneTimeEvolution(double kappa, double lambda) {
		initExp(kappa,lambda);
		FLStringList outputRes= new FLStringList();
		addHeader(outputRes);
		addTimeRecord(outputRes);
		for (int t=0; t<Tsim; t++) {
			m_sampler.sig_NextStep();
			if (t%10==0) {
				addTimeRecord(outputRes);	
			}
			if (t%1000==0) {
				Macro.fPrint("      sim. time:%d", t);
			}
		}
		return outputRes;
	}

	private void addHeader(FLStringList results) {
		results.Add("TIME,DNEUTRAL,DDEFECT");
	}


	/** rec **/
	private void addTimeRecord(FLStringList results) {
		double dN= m_measure.measureStateDensity(QuorumDModel.N);
		double dD= m_measure.measureStateDensity(QuorumDModel.D);
		int t=m_sampler.GetTime();
		String onetimesample=String.format("%d,%.4f,%.4f", t, dN, dD);
		results.Add(onetimesample);
		if (PRNLINES)
			Macro.print("t:"+onetimesample);
	}



	/** EXP 2 **/
	void densityVStimeExp2(){
		Macro.print("------- starting EXP2 -----------");
		for (double kappa : KAPPA_FIXEDEXP2) {
			for (int ilambda=ILMIN; ilambda<=ILMAX;ilambda+=ILSTP) {
				double lambda=ilambda/100.;
				FLStringList oneTimeEvolution= sampleOneTimeEvolution(kappa, lambda);
				oneTimeEvolution.WriteToFile( getFileName(kappa,lambda) );
			}
		}
	}


	/** EXP 3 **/
	void densityVStimeExp3(){
		Macro.print("------- starting EXP3 -----------");
		for (int i =0; i < LAMBDA_FIXEDEXP3.length; i++) {
			double lambda=LAMBDA_FIXEDEXP3[i]/1.;
			for (int ikappa=KMIN; ikappa<=KMAX; ikappa+=KSTP) {
				double kappa=ikappa/100.;
				Macro.fPrint(" **lauching exp with kappa=%.2f LAMBDA=%.2f", kappa,lambda);
				FLStringList oneTimeEvolution= sampleOneTimeEvolution(kappa,lambda);
				oneTimeEvolution.WriteToFile( getFileName(kappa,lambda) );
			}
		}
	}


	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------

	private String getFileName(double kappa, double lambda) {
		int iKappa=(int)(kappa*1000), iLambda=(int)(lambda*100);
		return String.format("dens-L%d-K%d-l%d.csv", L, iKappa ,iLambda);
	}


	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();

	public static void main(String [] argv){
		Locale.setDefault(Locale.US);// decimal numbers 
		CommandParser parser = new CommandParser(CLASSNAME, "", argv);
		int expType= parser.IParse("EXP=");
		QuorumDTimeExp exp= new QuorumDTimeExp();
		if (expType==2) {
			exp.densityVStimeExp2();
		} else if (expType==3){
			exp.densityVStimeExp3();
		} else {
			Macro.FatalError("EXPTYPE not recog.");
		}
	}


}


