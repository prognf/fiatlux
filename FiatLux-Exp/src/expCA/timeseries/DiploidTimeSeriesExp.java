package expCA.timeseries;

import java.io.IOException;
import java.util.ArrayList;

import components.types.FLString;
import components.types.FLStringList;
import components.types.IntegerList;
import components.types.RuleCode;
import experiment.measuring.linear.KinksMD;
import experiment.samplers.CAsampler;
import experiment.samplers.LinearSampler;
import main.Macro;
import main.commands.CommandParser;
import models.CAmodels.stochastic.DiploidECAmodel;
import topology.basics.SamplerInfoLinear;
import topology.basics.TopologyCoder;

public class DiploidTimeSeriesExp {
	// exp parameters
	private static final int N=10000;
	final static double ratioSamp= 1.1;
	final static int TMAX=1000000;
	private static final int W1=150, W2=232;
	private static final int NSAMP = 10;
	private static final int TMINAFTERZERO = 10; // where sampling begins

	private DiploidECAmodel m_model;
	CAsampler m_sampler;
	private KinksMD m_kinksMD;

	DiploidTimeSeriesExp(){
	}

	public void BuildSampler(int W1, int W2){
		RuleCode R1= new RuleCode(W1), R2= new RuleCode(W2);

		String 	topoCode = TopologyCoder.s_TLR1;
		m_model= new DiploidECAmodel(R1, R2);
		SamplerInfoLinear info= new SamplerInfoLinear(topoCode, m_model, N);
		m_sampler= new LinearSampler(info);
		m_kinksMD= new KinksMD();
		m_kinksMD.LinkTo(m_sampler);
	}

	IntegerList CalculateSampTimes() {
		IntegerList timesampling = new IntegerList();
		int time=0;
		timesampling.add(time);
		time=TMINAFTERZERO;
		timesampling.add(time);
		do { // we go one step beyond Tmax
			time*=ratioSamp;
			timesampling.add(time);
		} while (time<TMAX);
		return timesampling;
	}

	/** lambda : weight R1/R2 ; timesampling : moments where to sample 
	 * @throws IOException **/ 
	private void SampleForLambda(double lambda, IntegerList timesampling) throws IOException {
		//String huhu= FLString.ReplaceDotByLetterP(lambda);
		int slambda=(int)(lambda*1000);
		String filename= String.format("kinks-lambda-%d.dat",slambda);
		// if file does not exist then find
		if (!Macro.FileExists(filename)) {
			String oneline= timesampling.ToOneLine();
			FLStringList dataOut= new FLStringList();
			dataOut.Add(oneline);
			dataOut.WriteToFile(filename);
		}

		FLStringList dataOut= FLStringList.ReadFile(filename);
		// run
		m_model.SetWeightRule2(lambda);
		ArrayList<Double> series= DoOneTimeSeries(timesampling);
		// add one line 
		String dataInLine= FLString.ToStringSep(series," ");
		dataOut.Add(dataInLine);
		dataOut.WriteToFile(filename);
	}


	private void DoExp(double lambda) {
		BuildSampler(W1,W2);		
		IntegerList timesampling= CalculateSampTimes();
		timesampling.Print();
		try {
			for (int i=0; i<NSAMP; i++) {
				SampleForLambda(lambda, timesampling);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** one measure 
	 * @return **/
	private ArrayList<Double> DoOneTimeSeries(IntegerList timesampling) {
		m_sampler.sig_Init();
		int seedI= m_sampler.GetInitSeed();
		int seedM= m_sampler.GetModelSeed();
		Macro.fPrint(" seed-ini:%d   seed-model:%d", seedI, seedM);
		ArrayList<Double> series= new ArrayList<Double>();
		for (int tSamp : timesampling) {
			while (m_sampler.GetTime() < tSamp) { // go to next samp time
				m_sampler.sig_NextStep(); // one step
				//Macro.fPrintNOCR(".");
			}
			//Macro.CR();

			double samp= m_kinksMD.GetMeasure();
			//Macro.fPrint("%4d > %.3f",m_sampler.GetTime(),samp);
			series.add(samp);
		}
		return series;
	}

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();

	public static void main(String[] argv) {
		Macro.BeginTest(CLASSNAME);
		CommandParser parser= new CommandParser(CLASSNAME, argv);
		// int expNum= parser.IParse("EXP=");
		int lbd=parser.IParse("lambda=");
		double dlbd= lbd / 1000.;
		DiploidTimeSeriesExp  exp= new DiploidTimeSeriesExp();
		exp.DoExp(dlbd);
		Macro.EndTest();
	}

}