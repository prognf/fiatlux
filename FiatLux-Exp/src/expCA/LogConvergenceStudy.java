package expCA;

import components.allCells.OneRegisterIntCell;
import components.arrays.GenericCellArray;
import components.arrays.RegularDynArray;
import components.types.FLStringList;
import components.types.FLsignal;
import components.types.IVector;
import components.types.IntegerList;
import components.types.RuleCode;
import experiment.samplers.LinearSampler;
import initializers.CAinit.BinaryInitializer;
import initializers.CAinit.BinaryInitializer.INITSTYLE;
import main.FiatLuxProperties;
import main.Macro;
import main.MathMacro;
import main.commands.CommandInterpreter;
import main.commands.CommandParser;
import models.CAmodels.CellularModel;
import models.CAmodels.tabled.ECAlookUpTable;
import models.CAmodels.tabled.ECAmodel;
import topology.basics.LinearTopology;
import topology.zoo.RadiusN_Topology;

/*-------------------------------------------------------------------------------
- @author Nazim Fates
------------------------------------------------------------------------------*/

public class LogConvergenceStudy extends CommandInterpreter {

	//--------------------------------------------------------------------------
	//- SETTINGS
	//--------------------------------------------------------------------------

	//private static final String ZERULE = "DGH";
	//	potential of :        a  b  c  d  e  f  g  h 
	int [] m_POTENTIAL = new int [8];
	//m_POTENTIAL = {0, 0, 0, 3, 0, 0, 1, 1 };
	//--------------------------------------------------------------------------
	//- constants
	//--------------------------------------------------------------------------



	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();
	final static String USAGE=" hi ha";

	private static final int N = 5;
	private static final String SEP = " | ", SEPL=" & ";
	private static final String FIN = "", FINL= "\\\\";
	
	private static final String SEP2="}{", FIN2 = "}"; //latex
	private static final boolean POT = true; // print or not the potential ?


	// DO NOT MODIFY THIS !!!
	final static int A=0, B=1, C=2, D=3, E=4, F=5, G=6, H=7;
	private static final char CHAR_CODEAsmall = 97;
	private static final char CHAR_CODEAbig = 65;
	private static final int OCTO = 8;


	//--------------------------------------------------------------------------
	//- attributes
	//--------------------------------------------------------------------------

	private BinaryInitializer m_init;
	private GenericCellArray<OneRegisterIntCell> m_array;
	int [] m_deltaMin = new int[OCTO]; 
	Entry [][] m_entry;
	private String m_ECAT; // code of the rule under study
	private IntegerList m_coucou= new IntegerList();

	//--------------------------------------------------------------------------
	//- construction
	//--------------------------------------------------------------------------

	public LogConvergenceStudy(RuleCode Wcode) {
		this(ECAlookUpTable.GetTransitionCodeRaw(Wcode));
	}

	/** construction **/
	public LogConvergenceStudy(String Tcode) {
		m_ECAT= Tcode;
		RuleCode Wcode = ECAlookUpTable.TcodeToWcode(m_ECAT);
		CellularModel model = new ECAmodel(Wcode);
		LinearTopology 	topo = new RadiusN_Topology(1);
		topo.SetSize(N);

		LinearSampler samp = new LinearSampler(N, model, topo);
		RegularDynArray sys= samp.GetAutomaton();
		m_array= new GenericCellArray<OneRegisterIntCell>(sys);


		m_init= (BinaryInitializer)samp.GetInitializer();
		m_init.ForceInitStyle(INITSTYLE.Pattern);
		m_entry= new Entry [OCTO][4];

		char [] charTcode = Tcode.toCharArray();
		for (char c : charTcode){
			int posI= (int)c - CHAR_CODEAbig;
			if (posI<0) {
				Macro.FatalError(" Tcode seems wrong:"+Tcode);
			}
			//Macro.Debug("ADDchar: " + c + " pos I:" + posI);
			m_coucou.addVal(posI); // list of the positions for active transitions
		}

		CalCulatePotential();

	}

	/** each active transition has potential 1 **/
	private void SetPotentialForStrictMonotony() {
		for (int pos=0; pos<8; pos++) {
			char transition= (char)('A' + pos);
			if ( m_ECAT.indexOf(transition)>=0 ) { //Tcode contains char (transition)
				m_POTENTIAL[pos]=1;
			} else {
				m_POTENTIAL[pos]=0;
			}
		}
		CalCulatePotential();
	}

	private void SetPotentialByWeightList(String wlist) {
		for (int pos=0; pos<8; pos++) {
			m_POTENTIAL[pos]=(wlist.charAt(pos) - '0');
		}
		CalCulatePotential();
	}
	
	

	//--------------------------------------------------------------------------
	//- for verification
	//--------------------------------------------------------------------------

	/** says if all the local deltas are strictly negative **/
	private boolean CheckPotential() {
		for (int ipattern : m_coucou){
			for (int ext=0; ext < 4 ; ext++){
				if (Delta(ipattern,ext) >= 0)
					return false;
			}
		}
		return true;
	}

	//--------------------------------------------------------------------------
	//- printing info
	//--------------------------------------------------------------------------


	private void LatexPrintTable(){
		for (int ext=0; ext < 4 ; ext++){
			StringBuilder str= new StringBuilder(" \\hdln {" + ext + "}" + SEPL);
			for (int ipattern : m_coucou){
				String 
				sa= m_entry[ipattern][ext].m_aliph,
				sb= m_entry[ipattern][ext].m_ba,
				sep= ((ipattern<7)?SEPL:FINL);
				str.append(String.format("\\qami{%s}{%s} %s", sa, sb, sep));
			}
			Macro.fPrint(str.toString());
		}
	}

	/* use CalcPot before **/
	private void PrintTable() {
		// prints table for active transitions
		for (int ext=0; ext < 4 ; ext++){		
			StringBuilder s_a= new StringBuilder();
			StringBuilder s_b= new StringBuilder();
			StringBuilder s_c= new StringBuilder(" ");
			for (int i=0; i<m_coucou.GetSize(); i++){
				//Macro.Debug("ipattern : " + ipattern);
				//alpha
				int ipattern = m_coucou.Get(i);
				s_a.append(m_entry[ipattern][ext].m_aliph);
				s_a.append(POT ? " " + PotA(ipattern, ext) : "").append(i < m_coucou.GetSize() - 1 ? SEP : FIN);
				// beta
				s_b.append(m_entry[ipattern][ext].m_ba);
				s_b.append(POT ? " " + PotB(ipattern, ext) : "").append(i < m_coucou.GetSize() - 1 ? SEP : FIN);
				//delta
				s_c.append(String.format("  %2d    ", Delta(ipattern, ext)));
			}

			Macro.print(s_a.toString());
			Macro.print(s_b.toString());
			Macro.print(s_c.toString());
			Macro.print("");
			//Macro.print("\\zloup");
		}

	}

	private void PrintTableLatex() {
		// prints table for active transitions
		for (int ext=0; ext < 4 ; ext++){		
			StringBuilder s_a= new StringBuilder();
			StringBuilder s_b= new StringBuilder();
			StringBuilder s_c= new StringBuilder(" ");
			for (int i=0; i<m_coucou.GetSize(); i++){
				//Macro.Debug("ipattern : " + ipattern);
				//alpha
				int ipattern = m_coucou.Get(i);
				String 	ppA=
						String.format("\\pot{%s}{%d}", 
						m_entry[ipattern][ext].m_aliph, PotA(ipattern, ext)),
						ppB= String.format("\\pot{%s}{%d}", 
						m_entry[ipattern][ext].m_ba, PotB(ipattern, ext));							;
				s_a.append(ppA);
				s_a.append(i < m_coucou.GetSize() - 1 ?  "&" : "\\\\");
				// beta
				s_b.append(ppB);
				s_b.append(i < m_coucou.GetSize() - 1 ?  "&" : "\\\\");
				//delta
				s_c.append(String.format("\\deltaW{%2d} ", Delta(ipattern, ext)));
				s_c.append(i < m_coucou.GetSize() - 1 ?  "&" : "\\\\");
			}

			Macro.print(s_a.toString());
			Macro.print(s_b.toString());
			Macro.print(s_c.toString());
			Macro.print("\\hline");
			//Macro.print("\\zloup");
		}


	}

	//--------------------------------------------------------------------------
	//- inequalities
	//--------------------------------------------------------------------------

	/** prints the inequalities for each elementary cycle */
	private void GenerateInequalities(){
		// cycle H
		//	String 	sH1= m_aliph[H][3],
		//		sH2= m_ba[H][3];
		IVector vH= new IVector(8);
		ComputeCyclePart(vH,H,4);
		IVector vDE= new IVector(8);
		ComputeCyclePart(vDE,D,1);
		ComputeCyclePart(vDE,E,4);
		IVector vBEC= new IVector(8);
		ComputeCyclePart(vBEC,B,3);
		ComputeCyclePart(vBEC,E,1);
		ComputeCyclePart(vBEC,C,2);
		IVector vGDF= new IVector(8);
		ComputeCyclePart(vGDF,G,2);
		ComputeCyclePart(vGDF,D,4);
		ComputeCyclePart(vGDF,F,3);
		IVector vBFGC= new IVector(8);
		ComputeCyclePart(vBFGC,B,4);
		ComputeCyclePart(vBFGC,F,1);
		ComputeCyclePart(vBFGC,G,1);
		ComputeCyclePart(vBFGC,C,4);

		PrintZ(vH, vDE, vBEC, vGDF, vBFGC);

		Recompute(vH);
		Recompute(vDE);
		Recompute(vBEC);
		Recompute(vGDF);
		Recompute(vBFGC);
		Macro.CR();
		PrintZ(vH, vDE, vBEC, vGDF, vBFGC);

	}


	private void PrintZ(IVector vH, IVector vDE, IVector vBEC, IVector vGDF,
			IVector vBFGC) {
		String sH= ToReadableForm(vH);
		String sDE= ToReadableForm(vDE);
		String sBEC= ToReadableForm(vBEC);
		String sGDF= ToReadableForm(vGDF);
		String sBFGC= ToReadableForm(vBFGC);
		Macro.fPrint(" H: %s  DE:%s  ", sH, sDE);
		Macro.fPrint(" BEC: %s  GDF:%s  BFGC:%s  ", sBEC, sGDF, sBFGC);		
	}

	/** we assume balancedness **/
	private void Recompute(IVector vect) {
		int d= vect.GetVal(1);
		if (d==vect.GetVal(2)){
			vect.AddVal(0, d);
			vect.AddVal(3, d);
			vect.AddVal(1, -d);
			vect.AddVal(2, -d);
		}
		d= vect.GetVal(5);
		if (d==vect.GetVal(6)){
			vect.AddVal(4, d);
			vect.AddVal(7, d);
			vect.AddVal(5, -d);
			vect.AddVal(6, -d);
		}		
	}

	/** for viewing inequalities **/
	private String ToReadableForm(IVector vectIneq) {
		StringBuilder s= new StringBuilder(":");
		for (int pos=0; pos < OCTO; pos++){
			int val= vectIneq.GetVal(pos);
			if (val != 0){ // pretty print of inequalities 
				char chr= ILetterToChar( pos );
				String mult = (val==1?"": (val==-1?"-":""+val) );
				s.append(mult).append((char) chr).append(":");
			}
		}
		return s.toString();
	}

	/** for building inequalities **/
	private void ComputeCyclePart(IVector vectIneq, int letter, int rank) {

		if (RuleContains(letter)){ // compute only for active transitions
			for (int pos=0; pos < 3; pos++){
				int letterIndexA= CharToILetter(
						m_entry[letter][rank-1].m_aliph.charAt(pos) );
				vectIneq.DecrementPos(letterIndexA);
				int letterIndexB= CharToILetter(
						m_entry[letter][rank-1].m_ba.charAt(pos) );
				vectIneq.IncrementPos(letterIndexB);
			}		
		}
	}

	/** prints the potential of each elementary cycle **/
	private void PrintPotentialCycles() {
		//if (izBalanced())
		{
			int pH= TabDelta(H,4);
			int pDE= TabDelta(D,1) + TabDelta(E,4);
			int pBEC= TabDelta(B,3) + TabDelta(E,1) + TabDelta(C,2);
			int pGDF= TabDelta(G,2) + TabDelta(D,4) + TabDelta(F,3);
			int pBFGC= TabDelta(B,4) + TabDelta(F,1) + TabDelta(G,1) + TabDelta(C,4);
			String prn = String.format(
					"var cycle balanced >> H:%d DE:%d  BEC:%d  GDF:%d  BFGC:%d", 
					pH, pDE, pBEC, pGDF, pBFGC);
			Macro.print(prn);
		}/* else*/ {
			int mH= Min(H);
			int mDE= Min(D) + Min(E); 
			int mBEC= Min(B) + Min(E) + Min(C);
			int mGDF= Min(G) + Min(D) + Min(F);
			int mBFGC= Min(B) + Min(F) + Min(G) + Min(C);
			String prn2 = String.format(
					"var cycle min      >> H:%d DE:%d  BEC:%d  GDF:%d  BFGC:%d", 
					mH, mDE, mBEC, mGDF, mBFGC);
			Macro.print(prn2);
		}
	}

	boolean izBalanced(){
		return  (Pot(A)+Pot(D)==Pot(B) + Pot(C)) &&
				(Pot(E)+Pot(H)==Pot(F) + Pot(G));
	}

	private int Pot(int i) {
		return m_POTENTIAL[i];
	}

	private int Min(int iPattern) {
		return m_deltaMin[iPattern];
	}

	private int TabDelta(int letter, int rank) {
		if (RuleContains(letter)){
			return Delta(letter,rank-1); 
		} else {
			return 0; 
		}
	}

	private boolean RuleContains(int letter) {
		return m_coucou.Contains(letter);
	}

	private void PrintPotential() {
		Macro.print(" Tab pot.:" + MathMacro.ArrayToStringWithFormatting(m_POTENTIAL));
		StringBuilder s_potential = new StringBuilder("potential : ");
		for (int i=0; i<OCTO; i++){
			s_potential.append("").append(ILetterToChar(i)).append("=").append(m_POTENTIAL[i]).append(" ");
		}
		Macro.print(s_potential.toString());
		Macro.print("***");
	}

	//central patterns
	String [] PATTERN = 
		{ "000", "001", "100", "101", "010", "011", "110", "111" }; 

	/** calculating the 32 elem. var. of pot. */
	private void CalCulatePotential() {		
		//Macro.Debug("MCOUCOU:" + m_coucou);
		for (int ipattern : m_coucou){
			m_deltaMin[ipattern]= Integer.MIN_VALUE;
			for (int ext=0; ext < 4 ; ext++){
				ProcessEntry(ipattern,ext);
			}
		}
	}

	/** computes one entry */
	private void ProcessEntry(int iPattern, int ext) {
		String pattern= PATTERN[iPattern];//MathMacro.Int2Binary(iPattern, 3);
		//Macro.Debug( "ip: "+ iPattern + " PATT:" + pattern);
		m_init.SetInitPattern((ext/2)+ pattern+  (ext%2));
		m_init.ReceiveSignal(FLsignal.init);


		String stA=ReadState();
		//Macro.Debug ("STA : " + stA);
		int potA= CalcPotential(stA);

		// evolving central cell
		OneRegisterIntCell cell =  m_array.GetCell(N/2);
		cell.sig_UpdateBuffer();
		cell.sig_MakeTransition();

		String stB= ReadState();
		int potB= CalcPotential(stB);

		//delta
		int delta= potB- potA;
		m_entry[iPattern][ext]= new Entry(potA, potB, delta, stA, stB);

		//delta min
		if (delta > m_deltaMin[iPattern]){
			m_deltaMin[iPattern]= delta;
		}
	}

	/** from t-configuration to potential **/
	private int CalcPotential(String state) {
		int pot=0;
		char [] ctab= state.toCharArray();
		for (char val : ctab){
			int index = CharToILetter(val); // from char to int pos  
			pot +=  m_POTENTIAL[index];
		}
		return pot;
	}


	//--------------------------------------------------------------------------
	//- parsing operations 
	//--------------------------------------------------------------------------

	private int CharToILetter(char val) {
		return val - CHAR_CODEAsmall;
	}

	private char ILetterToChar(int pos) {
		return (char)(CHAR_CODEAsmall + pos);
	}

	/** we have to "invert" the sense of reading **/
	private String ReadState() {

		int [] stArray = new int[N];
		for (int i=0; i < N; i++){
			stArray[i]= m_array.GetCell(N- 1 - i).GetState();	
		}
		// stArray.Print();
		String Tstate= Tparse(stArray);
		return Tstate;
	}

	final static String [] TCODE= { "a", "b", "c", "d", "e", "f", "g", "h" };

	/** from binary 5-seq to Tcode
	 * the LSB is the first item of the array
	 * config 00010 -> 5 -> [0,1,0,0,0] 
	 **/
	private static String Tparse(int [] stArray) {
		StringBuilder out= new StringBuilder();
		int sz= stArray.length;
		for (int pos=1; pos < sz-1; pos++){
			int bitPos= 
					4 * stArray[(sz - 1) - pos]   + 
					2 * stArray[(sz - 1) - (pos - 1)] + 
					1 * stArray[(sz - 1) - (pos + 1)] ;
			out.append(TCODE[bitPos]);
		}
		return out.toString();
	}

	// from binary val to Tcode
	private static String Tparse(int val5) {
		int [] stArray= MathMacro.Decimal2BinaryTab(val5, 5);
		return Tparse(stArray);
	}

	final static int NTRIPLETS=32;



	private static final boolean LATEXPRN = false;

	private int Delta(int ipattern, int ext) {
		return m_entry[ipattern][ext].m_delta;
	}

	private int PotA(int ipattern, int ext) {
		return m_entry[ipattern][ext].m_potA;
	}

	private int PotB(int ipattern, int ext) {
		return m_entry[ipattern][ext].m_potB;
	}


	class Entry {
		public Entry(int potA, int potB, int delta, String stA, String stB) {
			m_potA= potA;
			m_potB= potB;
			m_delta= delta;
			m_aliph= stA;
			m_ba= stB;
		}

		int m_potA, m_potB, m_delta;
		String m_aliph, m_ba;
	}

	//--------------------------------------------------------------------------
	//- main
	//--------------------------------------------------------------------------


	private void Experir() {
		PrintTable();

		PrintPotential();


		PrintPotentialCycles();
		Macro.print("*****");
		GenerateInequalities();

	}

	/** main : processing command line * */


	/** WlistA,WlistB : weights for a,b,c,d & e,f,g,h, resp. **/
	private static void AnalyseWmonotonicity(String Tcode, String wlistA,String wlistB) {
		String wlist = wlistA + wlistB;
		LogConvergenceStudy exp = new LogConvergenceStudy(Tcode);
		exp.SetPotentialByWeightList(wlist);
		if(!LATEXPRN) {
			exp.PrintTable();
		} else {
			exp.PrintTableLatex();
		}
		exp.PrintPotential();
	}

	static private void AnalyseStrictMonotonicity(String Tcode) {
		LogConvergenceStudy exp = new LogConvergenceStudy(Tcode);
		exp.SetPotentialForStrictMonotony();
		exp.PrintPotential();
		exp.PrintTable();
		//TestStrictlyMonotonousAll();
	}

	private static void TestStrictlyMonotonousAll() {
		FLStringList filter= new FLStringList();
		IntegerList ecalist= ECAlookUpTable.GetEcaMinimalsList();
		for (Integer eca : ecalist) {
			LogConvergenceStudy exp = new LogConvergenceStudy(new RuleCode(eca));
			exp.SetPotentialForStrictMonotony();
			boolean good= exp.CheckPotential();
			String res= String.format(" Rule %d-%s : %s", eca, exp.m_ECAT, good?"*GUT**":"iznogood");
			if (good) {
				filter.add(res);
			}
		}
		Macro.SeparationLine();
		filter.Print();
	}

	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------

	public static void main(String[] argv) {
		FiatLuxProperties.SetVerboseLevel(3);
		LaunchMessage(CLASSNAME, USAGE);
		CommandParser parser = new CommandParser(CLASSNAME, "", argv);
		String rule= parser.SParse("R=");
		String W0= parser.SParse("W0=");
		String W1= parser.SParse("W1=");
		AnalyseWmonotonicity(rule, W0, W1); 

		//ZZZZZZZZZZZZ TODO Macro.
		/*		try{
			//LogConvergenceStudy log= new LogConvergenceStudy(ZERULE);
			//log.LatexPrintTable();
			//log.Experir();
			AnalyseWmonotonicity("EGH","0000","1021");
			AnalyseWmonotonicity("AFGH","1000","0112");
			AnalyseWmonotonicity("EH","0000","1003");
			AnalyseWmonotonicity("AGH","1000","0022");
			AnalyseStrictMonotonicity("BDE",);
			//log.BuildTripletsGraph();
			//MathMacro.DoTest();
		}

		catch(Exception e){
			Macro.print(" Exception caught at the top: ");
			e.printStackTrace();
		}	
		 */
		CommandInterpreter.ByeMessage();
	}

}//end class
