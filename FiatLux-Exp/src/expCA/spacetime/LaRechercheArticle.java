package expCA.spacetime;

import main.Macro;
import main.MathMacro;
import main.commands.CommandInterpreter;
import models.CAmodels.CellularModel;
import models.CAmodels.binary.LifeModel;
import models.CAmodels.tabled.ECAmodel;
import topology.basics.SamplerInfoLinear;
import topology.basics.SamplerInfoPlanar;
import topology.basics.TopologyCoder;
import updatingScheme.AlphaScheme;

import components.types.IntC;

import experiment.samplers.CAsampler;
import experiment.samplers.CAsimulationSampler;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.imgFormat.ImgFormat;
import grafix.viewers.LineAutomatonViewer;
import grafix.viewers.LineAutomatonViewerDefault;


/*--------------------
 * @author Nazim Fates
 *--------------------*/

public class LaRechercheArticle extends CommandInterpreter {

	private static final int N = 25, T=72; //asynch
	private static final int N2= 50, T2= 80; //det.
	private static final IntC CellPixSize = new IntC(10,0);

	private static final int SEEDI = 1976, SEEDI2=1234, SEEDA= 2016;
	private static final int Wtapis = 106;
	private static final int W1=40, W2=94, W3=30, W4=54;
	private static CAsampler m_sampler;
	private static LineAutomatonViewer m_viewer;

	// life
	private static final IntC GridSize = new IntC(20,20);

	
	private static void DoLifeSnap(int L) {
	
		CellularModel 	rule = new LifeModel();

		SamplerInfoPlanar info2D= 
				new SamplerInfoPlanar(GridSize, CellPixSize, rule, TopologyCoder.MOORE8);
				
		CAsimulationSampler sampler= info2D.GetSimulationSampler();
	}
	
	
	/** produces a space-time diagram for asynch ECA **/
	private static void DoSTdiagRecAsynch(int W, double alpha, int nCell, int tSamp) {
		MakeSampler(W,nCell,tSamp);
		AlphaScheme US= (AlphaScheme) m_sampler.GetUpdatingScheme();
		US.SetSynchronyRate(alpha);
		m_sampler.SetUpdatingScheme(US);


		m_sampler.SetSeeds(SEEDI, 0, SEEDA);

		AddViewer(tSamp);
		// running the simulation
		m_sampler.sig_Init();
		for (int t=0; t<tSamp-1; t++){
			m_sampler.sig_NextStep();
		}
		// recording
		String filename= "asynch-" + W + "-A" + MathMacro.percentFromRatio(alpha);
		Record(filename);
	}

	/** produces a space-time diagram for SYNCHR. ECA **/
	private static void DoSTdiagRec(int W, int nCell, int tSamp, int seedI) {
		MakeSampler(W,nCell,tSamp);

		m_sampler.SetSeeds(seedI, 0, 0);

		AddViewer(tSamp);
		// running the simulation
		m_sampler.sig_Init();
		for (int t=0; t<tSamp-1; t++){
			m_sampler.sig_NextStep();
		}
		// recording
		String filename= "ECA-" + W ;
		Record(filename);
	}

	private static void Record(String filename) {
		m_viewer.SetMargins(0, 0);
		m_sampler.io_SetRecordFormat(ImgFormat.SVG);
		m_sampler.io_SaveImage(filename, m_viewer);	
		//m_sampler.io_SetRecordFormat(ImgFormat.PNG);
		//m_sampler.io_SaveImage(filename, m_viewer);	
	}

	private static void AddViewer(int tsamp) {
		m_viewer= new LineAutomatonViewerDefault(CellPixSize, m_sampler, tsamp);
		m_sampler.SetAutomatonViewer(m_viewer);
		PaintToolKit palette= new PaintToolKit();
		palette.AddColor( new FLColor(0, 150, 153) );// dark turquoise = 0
		palette.AddColor(FLColor.c_orange); // orange = 1
			
		m_viewer.SetPalette(palette);
	}

	private static void MakeSampler(int W, int nCell, int tsamp) {
		CellularModel rule = ECAmodel.NewRule(W);
		String 	topoCode = TopologyCoder.s_TLR1;
		SamplerInfoLinear info = 
				new SamplerInfoLinear(nCell, tsamp, CellPixSize, rule, topoCode);
		//Macro.Debug(" code :" + info.topoCode);
		m_sampler= new CAsimulationSampler(info);
	}



	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();
	/** main : processing command line * */
	public static void main(String[] argv) {
		LaunchMessage(CLASSNAME);
		try{
			fourDiagTapis();
			fourDiagClassif();
		}
		catch(Exception e){
			Macro.print(" Exception caught at the top: ");
			e.printStackTrace();
		}	
		CommandInterpreter.ByeMessage();
	}

	private static void fourDiagTapis() {
		DoSTdiag1(1.0);
		DoSTdiag1(0.9);
		DoSTdiag1(0.5);
		DoSTdiag1(0.25);
	}

	private static void DoSTdiag1(double alpha) {
		DoSTdiagRecAsynch(Wtapis,alpha,N,T);
	}

	private static void fourDiagClassif() {
		DoSTdiag2(W1,SEEDI);
		DoSTdiag2(W2,SEEDI2);
		DoSTdiag2(W3,SEEDI);
		DoSTdiag2(W4,SEEDI);
	}

	private static void DoSTdiag2(int W, int seedI) {
		DoSTdiagRec(W,N2,T2,seedI);
	}

}

