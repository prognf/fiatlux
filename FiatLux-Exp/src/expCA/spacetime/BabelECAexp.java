package expCA.spacetime;

import components.types.FLStringList;
import main.Macro;
import main.commands.CommandInterpreter;
import main.commands.CommandParser;

public class BabelECAexp extends CommandInterpreter {

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();
	final static String USAGE=" hi ha";
	private static final char ZERO = '0', ONE='1';
	
	FLStringList m_stateL;
	private int m_T ;

	
	public BabelECAexp(int L) {
		m_stateL= new FLStringList();
		m_T= L;
	}
	
	
	private void runExp() {
		String state="1";
		m_stateL.Add(state);
		for (int t=0; t<m_T; t++) {
			state= iterate(state);	
			m_stateL.add(state);
		}
		Macro.SeparationLine();
		//m_stateL.PrintRaw();
		Macro.SeparationLine();
		FLStringList tr= getRawTransposed();
		tr.WriteToFile("babel-tr.txt");
	}
	
	
	private static String iterate(String state) {
		String line=ZERO + "" + ZERO +state+ ZERO + "" + ZERO;
		int len= line.length();
		StringBuilder newline= new StringBuilder();
		for (int i=1; i<len-1;i++) {
			char cadd;
			char x= line.charAt(i-1), y= line.charAt(i), z=line.charAt(i+1);
			if ((y==ZERO) && (z==ZERO)) {
				cadd= x;
			} else {
				cadd= (x==ZERO?ONE:ZERO); // 1 - x
			}
			newline.append(cadd);
		}
		return newline.toString();
	}

	
	/** 
	 * FOR BABEL STUDY 
	 * prints with a transposition of the data, which is supposed to be "triangular"  */
	public FLStringList getRawTransposed() {
		FLStringList transposed= new FLStringList();
		int L= m_stateL.GetSize();
		int maxlen=m_stateL.Get(L-1).length();
		for (int i=0; i<maxlen ; i++) {
			String line="";
			for (int j=0; j < L ; j++) {
				String inputline= m_stateL.Get(j);
				if (i>=inputline.length()) { // inputline = row j
					line= concatenate(' ',line);
				} else {
					line=concatenate(inputline.charAt(i), line); 
				}
				if (  ( j==(L-8-1) ) || ( j==(L-16-1) )  ) {
				//if (  ( j==(8-1+(i+1)/2) ) || ( j==(16-1+(i+1)/2) )  ) {
					line= concatenate('.',line);
				}
			}
			String newline=String.format("%3d:%s", i, line);
			transposed.Add( newline );
		}//
		return transposed;
	}	

	// allows to change the sense of presentation	
	private String concatenate(char ch, String line) {
		return line + ch;
		//return ch + line;
	}

 static int NARG=1;
	/** main : processing command line * */
	public static void main(String[] argv) {
		LaunchMessage(CLASSNAME, USAGE);

		CommandParser parser = 
				new CommandParser(CLASSNAME, USAGE, argv, NARG);
		
		int L = parser.IParse("L=");
		
		BabelECAexp exp = new BabelECAexp(L);
		exp.runExp();
		CommandInterpreter.ByeMessage();
	}

	
}
