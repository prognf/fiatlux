package expCA.spacetime;

import components.randomNumbers.FLRandomGenerator;
import components.types.FLString;
import components.types.IntC;
import components.types.IntegerList;
import components.types.RuleCode;
import experiment.samplers.CAsimulationSampler;
import grafix.gfxTypes.imgFormat.ImgFormat;
import grafix.viewers.LineAutomatonViewer;
import grafix.viewers.LineAutomatonViewerDefault;
import initializers.CAinit.CenterZoneInitializer;
import main.Macro;
import main.commands.CommandInterpreter;
import models.CAmodels.CellularModel;
import models.CAmodels.tabled.ECAlookUpTable;
import models.CAmodels.tabled.ECAmodel;
import topology.basics.SamplerInfoLinear;
import topology.basics.TopologyCoder;
import updatingScheme.AlphaScheme;


/*--------------------
 * @author Nazim Fates
 *--------------------*/

public class DoSTdiagECA extends CommandInterpreter {

	private static final int N = 20, T = 10; // size of the STD
	private static final boolean HALFINIMODE = false; // type of initial condition
	private static final boolean FULLYASYNCH_UPDATE = false; // update mode
	private static final int SEEDI = 1976, SEEDA= 2016;
	private static final IntC CellPixSize = new IntC(4,0);

	/** fully asynch. case **/
	private static void DoSTdiagRec(int W) {

		CellularModel rule = ECAmodel.NewRule(W);
		String 	topoCode = TopologyCoder.s_TLR1;

		SamplerInfoLinear info = 
				new SamplerInfoLinear(N, T, CellPixSize, rule, topoCode);
		//Macro.Debug(" code :" + info.topoCode);
		CAsimulationSampler sampler= new CAsimulationSampler(info);

		if (FULLYASYNCH_UPDATE) {
			sampler.SetFullyAsynchronousUpdatingScheme();
			//AlphaScheme US= (AlphaScheme) sampler.GetUpdatingScheme();
			//US.SetSynchronyRate(0);
		}

		SetInit(sampler);

		LineAutomatonViewer viewer= new LineAutomatonViewerDefault(CellPixSize, sampler, T);
		sampler.SetAutomatonViewer(viewer);

		//watch out !!!! 
		// SigNsteps and sigInit are NOT exchangeable !
		sampler.sig_Init();
		for (int t=0; t<T-1; t++){
			sampler.sig_NextStep();
			//sampler.sig_Nsteps(N);
		}
		String filename= "ECA-" + W;
		/*sampler.io_SetRecordFormat(ImgFormat.SVG);
		sampler.io_SaveImage(filename, viewer);	
		sampler.io_SetRecordFormat(ImgFormat.PNG);
		sampler.io_SaveImage(filename, viewer);*/
		sampler.io_SetRecordFormat(ImgFormat.TXT);
		sampler.io_SaveImage(filename, viewer);
	}

	private static void SetInit(CAsimulationSampler sampler) {
		if (HALFINIMODE){
			CenterZoneInitializer init = new CenterZoneInitializer();
			sampler.SetInitDeviceFromOutside(init);
			init.SetZoneLengthHalf();
		}
		sampler.SetSeeds(SEEDI, 0, SEEDA);
	}

	private static void AsynchDiagVarAlpha(int W, int A) {
		int seedini= 4*A, seedupdate = 4*A+1234; //arbitrary vals
		CAsimulationSampler sampler = GetECAsampler(W, N, T,CellPixSize);
		double alpha= A/100.;
		AlphaScheme update= (AlphaScheme) sampler.GetUpdatingScheme();
		update.SetSynchronyRate(alpha);
		sampler.SetUpdatingScheme(update);
		sampler.SetSeeds(seedini, 0, seedupdate);


		sampler.sig_Init();
		sampler.NstepsWithNupdates(T-1);
		ImgFormat format= ImgFormat.SVG;
		sampler.io_SetRecordFormat(format);
		String filename = GetECAName(W,N,T,A);
		sampler.io_SaveImageSVGhomemade(filename, true);	
	}

	private static String GetECAName(int w, int n, int t, int A) {
		return String.format("ECA-%d-N%d-T-%d-A-%d", w, n, t, A);
	}

	private static CAsimulationSampler GetECAsampler(int W, int n, int t,
			IntC cellpixsize) {
		CellularModel 	rule = ECAmodel.NewRule(W);
		String 	topoCode = TopologyCoder.s_TLR1;
		SamplerInfoLinear info = 
				new SamplerInfoLinear(N, T, CellPixSize, rule, topoCode);
		//Macro.Debug(" code :" + info.topoCode);
		CAsimulationSampler sampler= new CAsimulationSampler(info);
		return sampler;
	}

	/* records STD for all minimal ECAs **/
	private static void DoSTdiagRecAllMin() {
		IntegerList minimals = ECAlookUpTable.GetEcaMinimalsList();
		for (int W : minimals){
			DoSTdiagRec(W);
		}
	}

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();


	private static void DoListMinRep() {
		IntegerList minrep = ECAlookUpTable.GetEcaMinimalsList();
		String line1= FLString.ToStringSep(minrep," ");
		Macro.fPrint("%s", line1);
		String line2="";
		for (int rule : minrep) {
			line2+= ECAlookUpTable.GetTransitionCodeUnPadded(new RuleCode(rule)) +" ";
		}
		Macro.fPrint("%s", line2);

	}

	/** main : processing command line * */
	public static void main(String[] argv) {
		FLRandomGenerator.SetDefaultSeed(280676);
		//DoListMinRep();
		DoSTdiagRecAllMin();
		//DoSTdiagRec(170);
		CommandInterpreter.ByeMessage();
	}

}

