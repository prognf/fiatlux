package expCA.spacetime;

import components.types.IntC;
import components.types.RuleCode;
import experiment.samplers.CAsimulationSampler;
import grafix.gfxTypes.imgFormat.ImgFormat;
import initializers.CAinit.ArrayInitializer;
import initializers.CAinit.CenterZoneInitializer;
import main.Macro;
import main.MathMacro;
import main.commands.CommandInterpreter;
import models.CAmodels.CellularModel;
import models.CAmodels.binary.LifeModel;
import models.CAmodels.tabled.ECAmodel;
import topology.basics.SamplerInfoLinear;
import topology.basics.SamplerInfoPlanar;
import topology.basics.TopologyCoder;
import updatingScheme.AlphaScheme;
import updatingScheme.UpdatingScheme;


/*--------------------
 *--------------------*/


/*-------------------------------------------------------------------------------
- 
------------------------------------------------------------------------------*/





public class EncyclopediaACA extends CommandInterpreter {

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();
	final static String USAGE=" hi ha";

	private static final IntC GridSize = new IntC(20,20);
	private static final IntC CellPixSize = new IntC(4,0);
	private static final int NSTP = 4, STP=25;
	private static final int SEEDUS = 1984;
	private static final int SEEDINI1D = 15022016;
	private static final int NECAEXP=20, TECAEXP = 45;

	CAsimulationSampler m_samplerExpLife, m_samplerECA;
	ArrayInitializer m_initExpLife;
	private ECAmodel m_ECArule;

	public EncyclopediaACA() {
		m_ECArule= ECAmodel.NewRuleDefault();
	}

	//--------------------------------------------------------------------------
	//- LIF EXP
	//--------------------------------------------------------------------------


	private void runLifeExp() {
		initLifeExp();
		RecordLife(100);
		RecordLife(98);
		RecordLife(50);
	}

	private void initLifeExp() {
		CellularModel rule = new LifeModel();

		SamplerInfoPlanar info2D= 
				new SamplerInfoPlanar(GridSize, CellPixSize, rule, TopologyCoder.s_TM8);

		m_samplerExpLife= info2D.GetSimulationSampler();
		m_samplerExpLife.io_SetRecordFormat(ImgFormat.SVG);	

		//AutomatonViewer viewer= 
		//new OneRegisterAutomatonViewer(CellPixSize, GridSize, m_sampler.GetAutomaton());
		//viewer.SetPalette(PaintToolKit.GetDefaultPalette());
		//m_sampler.SetAutomatonViewer(viewer);

		m_initExpLife = (ArrayInitializer) m_samplerExpLife.GetInitializer();

	}

	private void RecordLife(int A){
		m_initExpLife.SetSeed(2806);
		m_samplerExpLife.sig_Init();
		UpdatingScheme us = m_samplerExpLife.GetUpdatingScheme();
		AlphaScheme usa= (AlphaScheme)us;
		usa.SetSeed(SEEDUS);
		double alpha= (double)A/MathMacro.HUNDRED;
		usa.SetSynchronyRate(alpha);
		for (int n=0; n<=NSTP; n++){
			int time= m_samplerExpLife.GetTime();
			String filename= String.format("Gol-A%d-t%d", A, time);
			m_samplerExpLife.io_SaveImage(filename);
			m_samplerExpLife.NstepsAndUpdate(STP);
		}
	}

	//--------------------------------------------------------------------------
	//- ECA EXP
	//--------------------------------------------------------------------------

	private void runQuadraExp() {
		initSamplerWithSize(NECAEXP);	
		recordQuadra();	
	}
	
	private void initSamplerWithSize(int size) {
		m_ECArule = ECAmodel.NewRuleDefault();
		String 	topoCode = TopologyCoder.s_TLR1;
		SamplerInfoLinear info = 
				new SamplerInfoLinear(size, TECAEXP, CellPixSize, m_ECArule, topoCode);
		m_samplerECA= new CAsimulationSampler(info);
		m_samplerECA.SetFullyAsynchronousUpdatingScheme();
	}

	public void recordQuadra(){
		recordECA(170, 0 ,"vA");
		recordECA(170, 12,"vB");
		recordECA(170, 3 ,"vC");
		recordECA(170, 90,"vD");
		
		recordECA(184,206190294, "quadra");
		recordECA(152, 0, "quadra");
		recordECA(178,30, "quadra");
		recordECA(146,21, "quadra");
	}
	
	/***********
	 * LIN
	 */
	
	public void runLinExp(){
		initSamplerWithSize(40);
		CenterZoneInitializer init= new CenterZoneInitializer();
		m_samplerECA.SetInitDeviceFromOutside(init);
		init.AddZone(2,10);
		init.AddZone(23,14);
		recordECA(168, 0, "lin");
		recordECA(168, 1, "lin-2");
		init.resetZones();
		init.SetZoneCentered(30);
		recordECA(162, 1, "lin");
		recordECA(162, 3, "lin-2");
		
	}
	
	public void recordECA(int W, int seedUS, String suffix){
		m_ECArule.SetRule(new RuleCode(W));
		m_samplerECA.SetSeeds(SEEDINI1D, 0, seedUS);
		m_samplerECA.sig_Init();
		for (int t=0; t<TECAEXP-1; t++){
			m_samplerECA.sig_NextStep();
		}
		String filename= m_samplerECA.GetModelName() + "-" + suffix;
		m_samplerECA.io_SaveImage(filename, ImgFormat.SVG);	

	}
	
	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------


	/** main : processing command line * */
	public static void main(String[] argv) {
		LaunchMessage(CLASSNAME, USAGE);
		try{
			EncyclopediaACA exp = new EncyclopediaACA();
			//exp.runLifeExp();
			//exp.runQuadraExp();
			exp.runLinExp();
		}
		catch(Exception e){
			Macro.print(" Exception caught at the top: ");
			e.printStackTrace();
		}	
		CommandInterpreter.ByeMessage();
	}

}//end class
