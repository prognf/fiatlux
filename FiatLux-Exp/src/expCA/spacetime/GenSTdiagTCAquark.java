package expCA.spacetime;

import components.randomNumbers.FLRandomGenerator;
import components.types.IntC;
import experiment.samplers.CAsimulationSampler;
import grafix.gfxTypes.imgFormat.ImgFormat;
import grafix.viewers.LineAutomatonViewer;
import grafix.viewers.LineAutomatonViewerDefault;
import main.Macro;
import main.commands.CommandInterpreter;
import models.CAmodels.ternary.TernaryTableModel;
import models.CAmodels.ternary.QuarkWcodeControl;
import topology.basics.SamplerInfoLinear;
import topology.basics.TopologyCoder;


/*--------------------
 * @author Nazim Fates
 *--------------------*/

public class GenSTdiagTCAquark extends CommandInterpreter {

	private static final int N = 200, T = 100; // size of the STD
	private static final boolean FULLYASYNCH_UPDATE = true; // update mode
	private static final int SEEDI = 1976, SEEDA= 2016;
	private static final IntC CellPixSize = new IntC(10,0);

	/** fully asynch. case **/
	private static void DoSTdiagRec(int W) {

		TernaryTableModel rule = new TernaryTableModel();
		QuarkWcodeControl qc= new QuarkWcodeControl(rule);
		qc.setQuarkCode(W);
		
		String 	topoCode = TopologyCoder.s_TLR1;

		SamplerInfoLinear info = 
				new SamplerInfoLinear(N, T, CellPixSize, rule, topoCode);
		//Macro.Debug(" code :" + info.topoCode);
		CAsimulationSampler sampler= new CAsimulationSampler(info);

		if (FULLYASYNCH_UPDATE) {
			sampler.SetFullyAsynchronousUpdatingScheme();
		}

		SetSeedInit(sampler);

		LineAutomatonViewer viewer= new LineAutomatonViewerDefault(CellPixSize, sampler, T);
		sampler.SetAutomatonViewer(viewer);
		viewer.SetPalette(rule.GetPalette());

		// watch out !!!! 
		// SigNsteps and sigInit are NOT exchangeable !
		sampler.sig_Init();
		for (int t=0; t<T-1; t++){
			sampler.sig_NextStep();
			//sampler.sig_Nsteps(N);
		}
		String filename= "quark-" + W;
		sampler.io_SetRecordFormat(ImgFormat.PNG);
		sampler.io_SaveImage(filename, viewer);
		
		/*sampler.io_SetRecordFormat(ImgFormat.SVG);
		sampler.io_SaveImage(filename, viewer);*/
		/*sampler.io_SetRecordFormat(ImgFormat.TXT);
		sampler.io_SaveImage(filename, viewer);*/
	}

	private static void SetSeedInit(CAsimulationSampler sampler) {
		sampler.SetSeeds(SEEDI, 0, SEEDA);
	}
	
	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();

	/** main : processing command line * */
	public static void main(String[] argv) {
		FLRandomGenerator.SetDefaultSeed(280676);
		for (int w=0; w<576;w++) {
			DoSTdiagRec(w);
		}
		CommandInterpreter.ByeMessage();
	}

}

