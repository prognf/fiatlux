package expCA.spacetime;

import java.util.Arrays;

import components.types.IntC;
import components.types.RuleCode;
import experiment.samplers.CAsimulationSampler;
import grafix.gfxTypes.PaintToolKit;
import grafix.gfxTypes.imgFormat.ImgFormat;
import grafix.viewers.AutomatonViewer;
import grafix.viewers.OneRegisterAutomatonViewer;
import initializers.CAinit.CenterZoneInitializer;
import main.Macro;
import main.MathMacro;
import main.commands.CommandInterpreter;
import main.commands.CommandParser;
import models.CAmodels.CellularModel;
import models.CAmodels.tabled.TotalisticModel;
import topology.basics.SamplerInfoPlanar;
import topology.basics.TopologyCoder;


/*--------------------
 * @author Nazim Fates
 *--------------------*/

public class SnapTimeTot5 extends CommandInterpreter {

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();
	final static String USAGE=" hi ha";

	private static final IntC GridSize = new IntC(20,20);
	private static final IntC CellPixSize = new IntC(4,0);
	private static final boolean HALFINIMODE = false; // sets the initial condition
	private static final int NSTP = 4, STP=100;
	private static final int NARG = 5;

	/** main : processing command line * */
	public static void main(String[] argv) {
		LaunchMessage(CLASSNAME, USAGE);
		try{

			CommandParser parser = 
					new CommandParser(CLASSNAME, USAGE, argv, NARG);

			int W = parser.IParse("W=");
			int T1= parser.IParse("T1=");
			int T2= parser.IParse("T2=");
			int T3= parser.IParse("T3=");
			int T4= parser.IParse("T4=");


			//SortIR();
			DoSTdiagRec(W,T1,T2,T3,T4);
		}
		catch(Exception e){
			Macro.print(" Exception caught at the top: ");
			e.printStackTrace();
		}	
		CommandInterpreter.ByeMessage();
	}

	// to be verified
	private static void DoSTdiagRec(int W, int t1, int t2, int t3, int t4) {

		String Tcode= TopologyCoder.s_TV5;
		CellularModel 	rule = TotalisticModel.createNewRule(new RuleCode(W),5+1);

		SamplerInfoPlanar info= 
				new SamplerInfoPlanar(GridSize, CellPixSize, rule, Tcode);
		CAsimulationSampler sampler= info.GetSimulationSampler();
		sampler.io_SetRecordFormat(ImgFormat.SVG);	

		sampler.SetFullyAsynchronousUpdatingScheme();
		//AlphaScheme US= (AlphaScheme) sampler.GetUpdatingScheme();
		//US.SetSynchronyRate(0);

		if (HALFINIMODE){
			CenterZoneInitializer init = new CenterZoneInitializer();
			sampler.SetInitDeviceFromOutside(init);
			init.SetZoneLengthHalf();
		}

		AutomatonViewer viewer= 
				new OneRegisterAutomatonViewer(CellPixSize, GridSize, sampler.GetAutomaton());
		viewer.SetPalette(PaintToolKit.GetDefaultPalette());
		sampler.SetAutomatonViewer(viewer);

		sampler.sig_Init();

		sampler.NstepsAndUpdate(t1); Rec(W,sampler, viewer);
		sampler.NstepsAndUpdate(t2-t1); Rec(W,sampler, viewer);
		sampler.NstepsAndUpdate(t3-t2); Rec(W,sampler, viewer);
		sampler.NstepsAndUpdate(t4-t3); Rec(W,sampler, viewer);


	}

	private static void Rec(int W, CAsimulationSampler sampler, AutomatonViewer viewer) {
		String filename= "Tot-" + W + "t-" + sampler.GetTime();
		sampler.io_SaveImage(filename, viewer);

	}

	static int [] IRrules = 
		{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,20,21,24,25,28,29,31,37,
				39,45,47,53,55,61,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,
				80,81,84,85,87,88,89,90,92,93,94,95,101,103,109,111,117,119,122,125,127,
				133,135,141,143,149,151,157,159,160,161,162,164,165,167,168,170,173,175,
				176,181,183,184,186,187,189,191,197,199,205,207,213,215,218,221,223,224,
				226,229,231,232,234,237,239,240,242,245,247,248,250,253,255};


	static void SortIR() {
		Arrays.sort(IRrules);
		Macro.print( MathMacro.ArrayToString2(IRrules));
	} 

}







