
package expCA.spacetime;

import components.randomNumbers.FLRandomGenerator;
import components.types.IntC;
import components.types.RuleCode;
import experiment.samplers.CAsimulationSampler;
import grafix.gfxTypes.imgFormat.ImgFormat;
import grafix.viewers.LineAutomatonViewer;
import grafix.viewers.LineAutomatonViewerDefault;
import initializers.CAinit.CenterZoneInitializer;
import main.Macro;
import main.commands.CommandInterpreter;
import models.CAmodels.binary.SampleNeighbTTLModel;
import topology.basics.SamplerInfoLinear;
import topology.basics.TopologyCoder;


/*--------------------
 * @author Nazim Fates
 *--------------------*/

public class DoSTdiagSampledTTL extends CommandInterpreter {

	private static final int N = 60, T = 80; // size of the STD
	private static final boolean HALFINIMODE = false; // type of initial condition

	//final static String TOPOCODE = "T"+TopologyCoder.COMP; 
	final static String TOPOCODE = TopologyCoder.s_TLR2; // 
	final static int 		RADIUS = 2, MVAL = 2*RADIUS+1;
	private static final String 
		PREFIXFILENAME = "sampledTTL",
		SUFFIXFILENAME= "fA-BerL";

	private static final int SIZEMAX = 4; // variation of k
	
	private static final boolean FULLYASYNCH_UPDATE = true; // update mode
	private static final int SEEDI = 1976, SEEDA= 2016;
	private static final IntC CellPixSize = new IntC(4,0);

	
	/** fully asynch. case **/
	private static void genOneSpaceTimeDiag(int W, int k) {

		SampleNeighbTTLModel rule = new SampleNeighbTTLModel();
		rule.setKval(k);
		rule.SetRuleWcode(new RuleCode(W));

		SamplerInfoLinear info = 
				new SamplerInfoLinear(N, T, CellPixSize, rule, TOPOCODE);
		//Macro.Debug(" code :" + info.topoCode);
		CAsimulationSampler sampler= new CAsimulationSampler(info);

		if (FULLYASYNCH_UPDATE) {
			sampler.SetFullyAsynchronousUpdatingScheme();
			//AlphaScheme US= (AlphaScheme) sampler.GetUpdatingScheme();
			//US.SetSynchronyRate(0);
		}

		SetInit(sampler);

		LineAutomatonViewer viewer= new LineAutomatonViewerDefault(CellPixSize, sampler, T);
		sampler.SetAutomatonViewer(viewer);

		sampler.sig_Init();
		for (int t=0; t<T-1; t++){
			sampler.sig_NextStep();
		}
		String filename= String.format(PREFIXFILENAME + "-%d-M%d-k%d-" + SUFFIXFILENAME, W, MVAL, k);
		sampler.io_SetRecordFormat(ImgFormat.PNG);
		sampler.io_SaveImage(filename, viewer);
		//sampler.io_SetRecordFormat(ImgFormat.TXT);
		//sampler.io_SaveImage(filename, viewer);
	}

	private static void SetInit(CAsimulationSampler sampler) {
		if (HALFINIMODE){
			CenterZoneInitializer init = new CenterZoneInitializer();
			sampler.SetInitDeviceFromOutside(init);
			init.SetZoneLengthHalf();
		}
		sampler.SetSeeds(SEEDI, 0, SEEDA);
	}


	/* records STD for all minimal ECAs **/
	private static void DoSTdiagRecAll(int k) {
		int Wmax= (1 << (k+1)) ; //2^(k+1)
		int Wmin= 2;//(1 << k) ; //2^k
		
		for (int W=Wmin; W<Wmax; W+=2){ // only DQ rules
			genOneSpaceTimeDiag(W,k);
		}
	}

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();

	/** main : processing command line * */
	public static void main(String[] argv) {
		FLRandomGenerator.SetDefaultSeed(280676);
		for (int size=1; size<=SIZEMAX; size++) {
			DoSTdiagRecAll(size);	
		}
		CommandInterpreter.ByeMessage();
	}

}

