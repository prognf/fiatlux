package expCA.spacetime;

import components.types.FLStringList;
import components.types.IntegerList;
import components.types.RuleCode;
import main.Macro;
import main.commands.CommandInterpreter;
import models.CAmodels.tabled.ECAlookUpTable;


/*--------------------
 * @author Nazim Fates
 *--------------------*/

public class FilterTransitionCode extends CommandInterpreter {

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();
	final static String USAGE=" hi ha";

	static boolean MINRULES = true; 

	/** main : processing command line * */
	public static void main(String[] argv) {
		//LaunchMessage(CLASSNAME, USAGE);
		//PrintECAminimals();
			Explorerules(); // customize this one
			//PrintRules();
		//CommandInterpreter.ByeMessage();
	}


	private static void Explorerules() {

				FLStringList res= new FLStringList();
				IntegerList ruleList = MINRULES?ECAlookUpTable.GetEcaMinimalsList():IntegerList.NintegersFromZero(256);
				
				for (int W : ruleList){
					RuleCode Wr= new RuleCode(W);
					String resW=
							FilterOneFPOnly(Wr);
					//FilterNonDecreasingZones(Wr);
					//FilterStronglyIrreversibleACA(W)?""+W:"";
					//FilterReversibleACA(Wr);
					//FilterNoFP(Wr);
					if (resW!=null) { res.Add(resW); }
				}
				res.PrintRaw(); 
				//res.PrintOnOneLine();
				Macro.print("Found : "+ res.size() );		
	}

	static void PrintECAminimals(){
		IntegerList ruleList = MINRULES?ECAlookUpTable.GetEcaMinimalsList():IntegerList.NintegersFromZero(256);
		FLStringList tab= new FLStringList();
		for (int W : ruleList){
			RuleCode Wcode= RuleCode.FromInt(W);
			String Tcode = ECAlookUpTable.GetTransitionCodePadded(Wcode);
			Macro.fPrint("%d %s",W, Tcode);
		}
	}


	private static void PrintRules(){
		IntegerList ruleList = IntegerList.NintegersFromZero(256);
		FLStringList res= new FLStringList();
		int count=1;
		StringBuilder output = new StringBuilder();
		for (int W : ruleList){
			boolean resW= !FilterStronglyIrreversibleACA(W) &&
					!FilterRecurrent(W);
			boolean endl= ((W==255) || (count%12==0));
			if (resW) {
				boolean minR= W == ECAlookUpTable.GetMinimalRepresentative(W) ; 
				output.append(minR ? "\\MR{" : "\\nr{");
				output.append(W);
				output.append("}");
				output.append(endl ? "\\\\" : " & ");
				count++;
			}
			if (endl){
				res.Add(output.toString());
				output = new StringBuilder();
			}
		}

		Macro.print("Found : "+ count );	
		res.Print(); 		
	}


	/** returns "null" or rule number + information **/
	private static String FilterSynch(RuleCode Wcode) {

		ECAlookUpTable table = new ECAlookUpTable(Wcode);
		String Tcode = ECAlookUpTable.GetTransitionCodePadded(Wcode);
		String equivClass= table.GetSymmetries(Wcode.GetCodeAsLong());
		int 
		t0= table.getBitTable(0), t1= table.getBitTable(1),
		t2= table.getBitTable(2), t3= table.getBitTable(3),
		t4= table.getBitTable(4), t5= table.getBitTable(5),
		t6= table.getBitTable(6), t7= table.getBitTable(7);
		int bfgc = t1 +t3 + t4 + t6;					
		boolean c1= (t0 == 1) && (t7 == 0);
		boolean c2= (t2 == t5);
		int s1 = t1 + t2 + t4 , s2= t3 + t5 + t6; 
		boolean c3= (s1 != 1 ) &&  ( s2 != 2 ) &&  ! ((s1==2) && (s2==1));

		boolean res = c1 && c2 && c3;
		String stab= table.GetScode();
		String txt = String.format ("%s W %d - %s %d", stab, Wcode, Tcode, bfgc);
		return (res)? txt : null;
	}


	private static String FilterNoFP(RuleCode W) {

		String Tcode = ECAlookUpTable.GetTransitionCodePadded(W);
		boolean 
		c1 = In(Tcode,"A") && In(Tcode,"H"),
		c2 = In(Tcode,"D") || In(Tcode,"E"),
		c3=  In(Tcode,"B") || In(Tcode,"E") ||  In(Tcode,"C"),
		c4=  In(Tcode,"G") || In(Tcode,"D") ||  In(Tcode,"F"),
		c5=  In(Tcode,"B") || In(Tcode,"F") ||  In(Tcode,"G") || In(Tcode,"C");
		boolean res = c1 && c2 && c3 && c4 && c5;
		String txt = String.format ("W %s - %s : %b",W, Tcode, res);
		return res?txt:null;
	}

	private static String FilterNonDecreasingZones(RuleCode W) {

		String Tcode = ECAlookUpTable.GetTransitionCodePadded(W);
		boolean 
		c1 = In(Tcode,"A") && In(Tcode,"H"),
		c2 = In(Tcode,"D") || In(Tcode,"E"),
		c3=  In(Tcode,"B") || In(Tcode,"E") ||  In(Tcode,"C"),
		c4=  In(Tcode,"G") || In(Tcode,"D") ||  In(Tcode,"F"),
		c5=  In(Tcode,"B") || In(Tcode,"F") ||  In(Tcode,"G") || In(Tcode,"C");
		boolean res = !c2;
		String txt = String.format ("W %s - %s : %b",W, Tcode, res);
		return res?txt:null;
	}




	private static String FilterOneFPOnly(RuleCode W) {

		String Tcode = ECAlookUpTable.GetTransitionCodePadded(W);
		boolean 
		c1a = In(Tcode,"A"),
		c1h = In(Tcode,"H"),
		c2 = In(Tcode,"D") || In(Tcode,"E"),
		c3=  In(Tcode,"B") || In(Tcode,"E") ||  In(Tcode,"C"),
		c4=  In(Tcode,"G") || In(Tcode,"D") ||  In(Tcode,"F"),
		c5=  In(Tcode,"B") || In(Tcode,"F") ||  In(Tcode,"G") || In(Tcode,"C");

		FLStringList strCycles= new FLStringList();
		ADDcycle("a",c1a,strCycles);
		ADDcycle("h",c1h,strCycles);
		ADDcycle("de",c2,strCycles);
		ADDcycle("bec",c3,strCycles);
		ADDcycle("gdf",c4,strCycles);
		ADDcycle("bfgc",c5,strCycles);

		boolean res= (strCycles.size()==1);
		if (!res)
			return null;
		else {
			String txt = String.format ("W %s - %s : %s",W, Tcode, strCycles.ToOneString());
			return txt;
		}
	}

	private static void ADDcycle(String strC, boolean cond, FLStringList strCycles ) {
		if (!cond){
			strCycles.add(strC);
		}
	}


	/** collaboration with S. Das **/
	private static boolean FilterStronglyIrreversibleACA(int Wcode){
		ECAlookUpTable table = new ECAlookUpTable(RuleCode.FromInt(Wcode));
		boolean 
		a0= table.AC(0), a1= table.AC(1),
		a2= table.AC(2), a3= table.AC(3),
		a4= table.AC(4), a5= table.AC(5),
		a6= table.AC(6), a7= table.AC(7);
		boolean c1= a0 && (!a2) ;
		boolean d1= a7 && (!a5) ;
		boolean c2= a2 && a5 && (!a0) && (!a7);
		boolean c3= a1 && a2 && a4 && (!a0) && (!a3) && (!a6);
		boolean d3= a6 && a5 && a3 && (!a7) && (!a4) && (!a1);
		boolean res = (c1 || d1 || c2 || c3 || d3);
		return res;
	}

	/** collaboration with S. Das **/
	private static boolean FilterRecurrent(int Wcode){
		ECAlookUpTable table = new ECAlookUpTable(RuleCode.FromInt(Wcode));
		boolean 
		a0= table.AC(0), a1= table.AC(1),
		a2= table.AC(2), a3= table.AC(3),
		a4= table.AC(4), a5= table.AC(5),
		a6= table.AC(6), a7= table.AC(7);
		boolean c1= a0 != a2 ;
		boolean d1= a7 != a5 ;
		boolean c2= !a0 && !a1 && !a2 && !a4 && (a3 || a6);
		boolean d2= !a7 && !a6 && !a5 && !a3 && (a4 || a1);
		boolean c3= !a0 && !a2 && !a3 && !a6 && (a1 || a4);
		boolean d3= !a7 && !a5 && !a4 && !a1 && (a6 || a3);
		boolean res = (c1 || c2 || c3 || d1 || d2 || d3);
		return !res;
	}

	/** collaboration with S. Das **/
	private static String FilterReversibleACA(RuleCode Wcode){
		ECAlookUpTable table = new ECAlookUpTable(Wcode);
		String Tcode = ECAlookUpTable.GetTransitionCodePadded(Wcode);
		boolean 
		a0= table.AC(0), a1= table.AC(1),
		a2= table.AC(2), a3= table.AC(3),
		a4= table.AC(4), a5= table.AC(5),
		a6= table.AC(6), a7= table.AC(7);
		boolean c1= a0 != a2 ;
		boolean d1= a7 != a5 ;
		boolean c2= !a0 && !a1 && !a2 && !a4 && (a3 || a6);
		boolean d2= !a7 && !a6 && !a5 && !a3 && (a4 || a1);
		boolean c3= !a0 && !a2 && !a3 && !a6 && (a1 || a4);
		boolean d3= !a7 && !a5 && !a4 && !a1 && (a6 || a3);
		boolean res = (c1 || c2 || c3 || d1 || d2 || d3);
		String stab= table.GetScode();
		String txt = String.format ("%s W %s - %s", stab, Wcode, Tcode);
		return (res)? txt : null;
	}

	/** collaboration with S. Das **/
	private static String FilterReversibleACAv2(RuleCode Wcode){
		ECAlookUpTable table = new ECAlookUpTable(Wcode);
		String Tcode = ECAlookUpTable.GetTransitionCodePadded(Wcode);
		boolean 
		a0= table.AC(0), a1= table.AC(1),
		a2= table.AC(2), a3= table.AC(3),
		a4= table.AC(4), a5= table.AC(5),
		a6= table.AC(6), a7= table.AC(7);
		boolean sym= (a0 == a2) && (a7 == a5) ;
		boolean a= a2 && a5;
		boolean b= a1 && a6;
		boolean c= a3 && a4;
		boolean d= a1 && a3;
		boolean e= a4 && a6;
		boolean f= !a1 && !a4 && !a3 && !a6;
		boolean res = sym && (a || b || c || d || e || f);
		String stab= table.GetScode();
		String txt = String.format ("%s W %s - %s", stab, Wcode, Tcode);
		return (res)? txt : null;
	}

	/** collaboration with S. Das **/
	private static String FilterReversibleACAv3(RuleCode W){
		ECAlookUpTable table = new ECAlookUpTable(W);
		String Tcode = ECAlookUpTable.GetTransitionCodePadded(W);
		boolean 
		a0= table.AC(0), a1= table.AC(1),
		a2= table.AC(2), a3= table.AC(3),
		a4= table.AC(4), a5= table.AC(5),
		a6= table.AC(6), a7= table.AC(7);
		boolean sym= (a0 == a2) && (a7 == a5) ;
		boolean a= a2 && a5;
		boolean b= a1 && a4;
		boolean c= a3 && a6;
		boolean z= !a1 && !a4 && !a3 && !a6;
		boolean res = sym && (a || b || c || z);
		String stab= table.GetScode();
		String txt = String.format ("%s W %s - %s", stab, W, Tcode);
		return (res)? txt : null;
	}




	private static boolean In(String tcode, String letter) {
		return tcode.contains(letter);
	}


	static int [] IRrules = 
		{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,20,21,24,25,28,29,31,37,
				39,45,47,53,55,61,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,
				80,81,84,85,87,88,89,90,92,93,94,95,101,103,109,111,117,119,122,125,127,
				133,135,141,143,149,151,157,159,160,161,162,164,165,167,168,170,173,175,
				176,181,183,184,186,187,189,191,197,199,205,207,213,215,218,221,223,224,
				226,229,231,232,234,237,239,240,242,245,247,248,250,253,255};



}







