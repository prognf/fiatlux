package expCA.spacetime;

import initializers.CAinit.CenterZoneInitializer;
import main.Macro;
import main.commands.CommandInterpreter;
import models.CAmodels.CellularModel;
import models.CAmodels.multiRegister.CoalescenceModel;
import models.CAmodels.multiRegister.CoalescenceViewer;
import models.CAmodels.tabled.ECAlookUpTable;
import models.CAmodels.tabled.ECAmodel;
import topology.basics.SamplerInfoLinear;
import topology.basics.TopologyCoder;
import updatingScheme.AlphaScheme;

import components.types.IntC;
import components.types.IntegerList;
import components.types.RuleCode;

import experiment.samplers.CAsimulationSampler;
import grafix.gfxTypes.imgFormat.ImgFormat;


/*--------------------
 * @author Nazim Fates
 *--------------------*/

public class DoSTdiagCoalescence extends CommandInterpreter {

	private static final int N = 40;
	private static final int T = 50;
	private static final IntC CellPixSize = new IntC(4,0);
	private static final boolean HALFINIMODE = false; // sets the initial condition

	private static final int SEEDI = 1976, SEEDA= 2016;

	/** fully asynch. case **/
	private static void DoSTdiagRec(int W) {
		RuleCode Wc= new RuleCode(W);
		CellularModel rule = new CoalescenceModel(Wc);
		String 	topoCode = TopologyCoder.s_TLR1;

		SamplerInfoLinear info = 
				new SamplerInfoLinear(N, T, CellPixSize, rule, topoCode);
		//Macro.Debug(" code :" + info.topoCode);
		CAsimulationSampler sampler= new CAsimulationSampler(info);

		sampler.SetFullyAsynchronousUpdatingScheme();
		//AlphaScheme US= (AlphaScheme) sampler.GetUpdatingScheme();
		//US.SetSynchronyRate(0);

		SetInit(sampler);
		
		CoalescenceViewer coalViewer= (CoalescenceViewer)sampler.GetAutomatonViewer(); 
		
		//watch out !!!! 
		// SigNsteps and sigInit are NOT exchangeable !
		sampler.sig_Init();
		for (int t=0; t<T-1; t++){
			sampler.sig_NextStep();
			//sampler.sig_Nsteps(N);
		}
		sampler.io_SetRecordFormat(ImgFormat.SVG);
		
		String filename;
		coalViewer.DisplayBoth();
		filename="coal-agree-" + W;
		sampler.io_SaveImage(filename, coalViewer);
		
		filename="coal-layerA-" + W;
		coalViewer.DisplayAOnly();
		sampler.io_SaveImage(filename, coalViewer);
		
		filename="coal-layerB-" + W;
		coalViewer.DisplayBOnly();
		sampler.io_SaveImage(filename, coalViewer);
		
		
		//sampler.io_SaveImage();
		//sampler.io_SetRecordFormat(ImgFormat.PNG);
		//sampler.io_SaveImage(filename, viewer);	
	}

	private static void SetInit(CAsimulationSampler sampler) {
		if (HALFINIMODE){
			CenterZoneInitializer init = new CenterZoneInitializer();
			sampler.SetInitDeviceFromOutside(init);
			init.SetZoneLengthHalf();
		}
		sampler.SetSeeds(SEEDI, 0, SEEDA);
	}

	private static void AsynchDiagVarAlpha(int W, int A) {
		int seedini= 4*A, seedupdate = 4*A+1234; //arbitrary vals
		CAsimulationSampler sampler = GetECAsampler(W, N, T,CellPixSize);
		double alpha= A/100.;
		AlphaScheme update= (AlphaScheme) sampler.GetUpdatingScheme();
		update.SetSynchronyRate(alpha);
		sampler.SetUpdatingScheme(update);
		sampler.SetSeeds(seedini, 0, seedupdate);
		

		sampler.sig_Init();
		sampler.NstepsWithNupdates(T-1);
		ImgFormat format= ImgFormat.SVG;
		sampler.io_SetRecordFormat(format);
		String filename = GetECAName(W,N,T,A);
		sampler.io_SaveImageSVGhomemade(filename, true);	
	}

	private static String GetECAName(int w, int n, int t, int A) {
		return String.format("ECA-%d-N%d-T-%d-A-%d", w, n, t, A);
	}

	private static CAsimulationSampler GetECAsampler(int W, int n, int t,
			IntC cellpixsize) {
		CellularModel 	rule = ECAmodel.NewRule(W);
		String 	topoCode = TopologyCoder.s_TLR1;
		SamplerInfoLinear info = 
				new SamplerInfoLinear(N, T, CellPixSize, rule, topoCode);
		//Macro.Debug(" code :" + info.topoCode);
		CAsimulationSampler sampler= new CAsimulationSampler(info);
		return sampler;
	}

	private static void DoSTdiagRec() {
		IntegerList minimals = ECAlookUpTable.GetEcaMinimalsList();
		for (int W : minimals){
			DoSTdiagRec(W);
		}
	}
	
	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();

	
	/** main : processing command line * */
	public static void main(String[] argv) {
		LaunchMessage(CLASSNAME);
		try{
			//DoSTdiagRec();
			DoSTdiagRec(46);
		}
		catch(Exception e){
			Macro.print(" Exception caught at the top: ");
			e.printStackTrace();
		}	
		CommandInterpreter.ByeMessage();
	}




}







