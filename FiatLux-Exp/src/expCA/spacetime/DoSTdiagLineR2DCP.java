package expCA.spacetime;

import components.types.FLString;
import components.types.IntC;
import experiment.samplers.CAsimulationSampler;
import grafix.gfxTypes.imgFormat.ImgFormat;
import grafix.viewers.LineAutomatonViewer;
import initializers.CAinit.BinaryInitializer;
import main.Macro;
import main.commands.CommandInterpreter;
import main.commands.CommandParser;
import models.CAmodels.stochastic.ProbabilisticLineR2Model;
import topology.basics.SamplerInfoLinear;
import topology.basics.TopologyCoder;


/*--------------------
 * @author Nazim Fates
 *--------------------*/

public class DoSTdiagLineR2DCP extends CommandInterpreter {

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();
	final static String USAGE="rulefile=...";

	
	//private static final boolean HALFINIMODE = false; // sets the initial condition

	private static final int NARG=3;
	/** main : processing command line * */
	public static void main(String[] argv) {
		CommandParser parser =new CommandParser(CLASSNAME,USAGE,argv);
		parser.checkArgNum(NARG); 
		
		String filename=  parser.SParse("rulefile=");
		int N= parser.IParse("N=");
		int T= parser.IParse("T=");
		DoSTdiagRec(filename,N,T);
		CommandInterpreter.ByeMessage();
	}

	private static void DoSTdiagRec(String filename, int N, int T) {

		// creation
		 ProbabilisticLineR2Model rule = new ProbabilisticLineR2Model(filename);
		//rule.SetRuleCode(new RuleCode(30));
		IntC CellPixSize = new IntC(400/N+1,0); //size of the png : constant width
		SamplerInfoLinear info1D= new SamplerInfoLinear(N,T,CellPixSize,rule,TopologyCoder.s_TLR2);
		CAsimulationSampler sampler= info1D.GetSimulationSampler();
		//rule.SetRuleCode(new RuleCode(30));
		
		BinaryInitializer init= (BinaryInitializer)sampler.GetInitializer();
		init.SetInitRate(0.4);
		
		
		// dynamics
		sampler.sig_Init();
		for (int t=0; t<T-1; t++){
			sampler.sig_NextStep();
		}

		// rec
		String filenameout= String.format("DCP-d04-%s-N%d", FLString.RemoveExtension(filename), N);
		LineAutomatonViewer viewer= (LineAutomatonViewer)sampler.GetAutomatonViewer();
//		sampler.io_SetRecordFormat(ImgFormat.SVG);
//		sampler.io_SaveImage(filenameout, viewer);	
		sampler.io_SetRecordFormat(ImgFormat.PNG);
		sampler.io_SaveImage(filenameout, viewer);	
	}

}







