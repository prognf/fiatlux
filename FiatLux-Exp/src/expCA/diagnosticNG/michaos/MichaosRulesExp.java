package expCA.diagnosticNG.michaos;

import components.types.FLStringList;
import components.types.IntC;
import experiment.measuring.general.DecimalParameterMeasurer;
import experiment.measuring.specific.NonDefectiveActivityMD;
import experiment.samplers.CAsimulationSampler;
import grafix.gfxTypes.imgFormat.ImgFormat;
import main.Macro;
import main.commands.CommandInterpreter;
import models.CAmodels.binary.NGBinaryInitializer;
import models.CAmodels.decentralisedDiagnosis.NGChaosMajorityModel;
import topology.basics.SamplerInfoPlanar;
import topology.basics.TopologyCoder;

/**
 * @author Nicolas Gauville
 * Mesures de l'évolution des de l'activité
 */
public class MichaosRulesExp extends CommandInterpreter {
	final static String CLASSNAME = new Macro.CurrentClassGetter().getClassName();

	private static final int SIZE = 100;
	private static final IntC GridSize = new IntC(SIZE,SIZE);
	private static final IntC CellPixSize = new IntC(1,0);
	private static final int NB_EXP = 100; // mesures (100)
	private static final int NB_ITS = 500; // itérations (500)

	CAsimulationSampler m_samplerExp;
	NGBinaryInitializer m_initExp;

	private NGChaosMajorityModel model;
	private NonDefectiveActivityMD m_activityMD;

	private double activity[] = new double[NB_EXP];

	public MichaosRulesExp() {
	}

	private void initExp() {
		model = new NGChaosMajorityModel();

		// PlanarSamplerInfo info2D = new PlanarSamplerInfo(GridSize, CellPixSize, model, TopologyCoder.s_TM8);
		SamplerInfoPlanar info2D = new SamplerInfoPlanar(GridSize, CellPixSize, model, TopologyCoder.s_TV5);

		m_samplerExp = info2D.GetSimulationSampler();
		m_samplerExp.io_SetRecordFormat(ImgFormat.SVG);
		m_initExp = new NGBinaryInitializer();
		m_samplerExp.SetInitDeviceFromOutside(m_initExp);
		m_activityMD = new NonDefectiveActivityMD();
		m_activityMD.LinkTo(m_samplerExp);
	}
	
	private void runLinExp(){
		initExp();

		int nbNeighbours = 4;

		for (int a = 0; a < nbNeighbours; a++) {
			for (int b = a; b < nbNeighbours; b++) {
				for (int c = 0; c < nbNeighbours; c++) {
					for (int d = c; d < nbNeighbours; d++) {
						runExp(a, b, c, d);
					}
				}
			}
		}
	}

	private void runExp(int a, int b, int c, int d) {
		System.out.println("Rule " + a + "" + b + "" + c + "" + d);

		((NGChaosMajorityModel) m_samplerExp.GetCellularModel()).setRules(a, b, c, d);

		for (int i = 0; i < NB_EXP; i++) {
			System.out.println(i + "/" + NB_EXP);
			double defRatio = ((double) i / (double) NB_EXP);

			((NGBinaryInitializer) m_samplerExp.GetInitializer()).setKoCells(defRatio);

			m_samplerExp.sig_Init();
			for (int t = 0; t < NB_ITS - 1; t++) {
				m_samplerExp.sig_NextStep();
			}

			activity[i] = m_activityMD.GetMeasure(NGChaosMajorityModel.FAILURE);

			if (activity[i] == 0 && defRatio < 0.3) {
            }
		}

		if (activity[NB_EXP - 1] == 0) {
			save("michaos", a + "" + b + "" + c + "" + d);
		}
	}

	private void save(String filename, String rule) {
		FLStringList list = new FLStringList();
		list.add("D A");

		for (int i = 0; i < NB_EXP; i++) {
			double defRatio = ((double) i / (double) NB_EXP);
			StringBuilder sb = new StringBuilder();
			sb.append(defRatio);

			list.add(defRatio + " " + (activity[i]));
		}

		list.WriteToFile(filename + "-" + rule + "-" + SIZE + ".dat");
	}
	
	public static void main(String[] argv) {
		LaunchMessage(CLASSNAME);

		try{
			MichaosRulesExp exp = new MichaosRulesExp();
			exp.runLinExp();
		} catch(Exception e){
			Macro.print(" Exception caught at the top: ");
			e.printStackTrace();
		}	
		CommandInterpreter.ByeMessage();
	}
}
