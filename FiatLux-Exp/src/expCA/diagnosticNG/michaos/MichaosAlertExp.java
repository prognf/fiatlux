package expCA.diagnosticNG.michaos;

import components.types.FLStringList;
import components.types.IntC;
import experiment.measuring.specific.AlertDensityMD;
import experiment.samplers.CAsimulationSampler;
import main.Macro;
import main.commands.CommandInterpreter;
import models.CAmodels.multiRegister.NG3StateInitializer;
import models.CAmodels.multiRegister.NGBinaryInitializer;
import models.CAmodels.multiRegister.NGMichaosDiagModel;
import models.CAmodels.multiRegister.NGReactDiffDiagModel;
import topology.basics.SamplerInfoPlanar;
import topology.basics.TopologyCoder;
import updatingScheme.AlphaScheme;

/**
 * @author Nicolas Gauville
 */
public class MichaosAlertExp extends CommandInterpreter {
	final static String CLASSNAME = new Macro.CurrentClassGetter().getClassName();

	private static final int SIZE = 100; // 500
	private static final IntC GridSize = new IntC(SIZE,SIZE);
	private static final IntC CellPixSize = new IntC(1,0);
	private static final int NB_AVG = 100; // 100 // nombre de test par taux de défaillance
	private static final int NB_EXP = 30; // 30 // nombre de taux de défaillance testés
	private static final int NB_STEPS = 500; // 500 // nombre d'itérations avant chaque mesure
	private static final double ALPHA = 1;
	private static final double LAMBDA = 0.3; // 0.63

	private CAsimulationSampler m_samplerExp;

	private AlertDensityMD m_alertMD;

	private double density[] = new double[NB_EXP];

	public MichaosAlertExp() {
	}

	private void initExp() {
		NGMichaosDiagModel model = new NGMichaosDiagModel();

		SamplerInfoPlanar info2D = new SamplerInfoPlanar(GridSize, CellPixSize, model, TopologyCoder.s_TM8);

		m_samplerExp = info2D.GetSimulationSampler();
		NGBinaryInitializer m_initExp = new NGBinaryInitializer();
		m_samplerExp.SetInitDeviceFromOutside(m_initExp);
		m_alertMD = new AlertDensityMD(NGMichaosDiagModel.ALERT_B);
		m_alertMD.LinkTo(m_samplerExp);

		for (int i = 0; i < NB_EXP; i++) {
			density[i] = 0;
		}
	}
	
	private void runLinExp() {
		initExp();
		runExp();
	}

	private double getDefRatio(int i) {
		return ((double) i / (double) NB_EXP) * 0.7;
		// return 0.3 + ((double) i / (double) NB_EXP) * 0.25;
	}

	private void runExp() {
		for (int am = 0; am < NB_AVG; am++) {
			for (int i = 0; i < NB_EXP; i++) {
				double defRatio = getDefRatio(i);

				((NGBinaryInitializer) m_samplerExp.GetInitializer()).setKOCells(defRatio);
				((NGMichaosDiagModel) m_samplerExp.GetCellularModel()).setProbabilities(0, 0, LAMBDA);
				AlphaScheme updatingScheme = (AlphaScheme) m_samplerExp.GetUpdatingScheme();
				updatingScheme.SetSynchronyRate(ALPHA);

				m_samplerExp.sig_Init();
				for (int t = 0; t < NB_STEPS - 1; t++) {
					m_samplerExp.sig_NextStep();
				}

				density[i] += (m_alertMD.GetMeasure(NGMichaosDiagModel.ALERT_B, NGMichaosDiagModel.FAILURE) > 0.1 ? 1 : 0);
				System.out.println("Ratio : " + defRatio + " (" + am + "/" + NB_AVG + ", " + i + "/" + NB_EXP + ") : " + m_alertMD.GetMeasure(NGMichaosDiagModel.ALERT_B, NGMichaosDiagModel.FAILURE));
			}

			saveExp(am);
		}

	}

	private void saveExp(int nbExps) {
		FLStringList list = new FLStringList();
		list.add("DF A");

		for (int i = 0; i < NB_EXP; i++) {
			double defRatio = getDefRatio(i);
			list.add(defRatio + " " + (density[i] / (double) (nbExps + 1)));
		}

		list.WriteToFile("r2636-50-" + nbExps + ".dat");
	}
	
	public static void main(String[] argv) {
		LaunchMessage(CLASSNAME);

		try {
			MichaosAlertExp exp = new MichaosAlertExp();
			exp.runLinExp();
		} catch(Exception e) {
			Macro.print(" Exception caught at the top: ");
			e.printStackTrace();
		}

		CommandInterpreter.ByeMessage();
	}
}
