package expCA.diagnosticNG.michaos;

import components.types.FLStringList;
import components.types.IntC;
import experiment.measuring.specific.AlertDensityMD;
import experiment.samplers.CAsimulationSampler;
import main.Macro;
import main.commands.CommandInterpreter;
import models.CAmodels.multiRegister.NGBinaryInitializer;
import models.CAmodels.multiRegister.NGMichaosDiagModel;
import topology.basics.SamplerInfoPlanar;
import topology.basics.TopologyCoder;
import updatingScheme.AlphaScheme;

/**
 * @author Nicolas Gauville
 */
public class MichaosAlertTimeExp extends CommandInterpreter {
	final static String CLASSNAME = new Macro.CurrentClassGetter().getClassName();

	private static final int SIZE = 200; // 500
	private static final IntC GridSize = new IntC(SIZE,SIZE);
	private static final IntC CellPixSize = new IntC(1,0);
	private static final int NB_AVG = 100; // 100 // nombre de test par taux de défaillance
	private static final int NB_STEPS = 1000; // nombre d'itérations avant chaque mesure
	private static final double ALPHA = 1;
	private static final double DEF_RATIO = 0.2;

	private CAsimulationSampler m_samplerExp;
	private AlertDensityMD m_alertMD;
	private double density[] = new double[NB_STEPS];

	public MichaosAlertTimeExp() {
	}

	private void initExp() {
		NGMichaosDiagModel model = new NGMichaosDiagModel();

		SamplerInfoPlanar info2D = new SamplerInfoPlanar(GridSize, CellPixSize, model, TopologyCoder.s_TM8);

		m_samplerExp = info2D.GetSimulationSampler();
		NGBinaryInitializer m_initExp = new NGBinaryInitializer();
		m_samplerExp.SetInitDeviceFromOutside(m_initExp);
		m_alertMD = new AlertDensityMD(NGMichaosDiagModel.ALERT_B);
		m_alertMD.LinkTo(m_samplerExp);

		for (int i = 0; i < NB_STEPS; i++) {
			density[i] = 0;
		}
	}
	
	private void runLinExp(){
		initExp();
		runExp();
	}

	private void runExp() {
		for (int am = 0; am < NB_AVG; am++) {
			System.out.println("Ratio : " + DEF_RATIO + " (" + am + "/" + NB_AVG + ")");

			((NGBinaryInitializer) m_samplerExp.GetInitializer()).setKOCells(DEF_RATIO);
			((NGMichaosDiagModel) m_samplerExp.GetCellularModel()).setProbabilities(0, 0, 0.338);
			AlphaScheme updatingScheme = (AlphaScheme) m_samplerExp.GetUpdatingScheme();
			updatingScheme.SetSynchronyRate(ALPHA);

			m_samplerExp.sig_Init();
			for (int t = 0; t < NB_STEPS; t++) {
				m_samplerExp.sig_NextStep();

				density[t] += (m_alertMD.GetMeasure(NGMichaosDiagModel.ALERT_B, NGMichaosDiagModel.FAILURE) > 0.1 ? 1 : 0);
			}

		}

		saveExp(NB_AVG - 1);
	}

	private void saveExp(int nbExps) {
		FLStringList list = new FLStringList();
		list.add("T A");

		for (int i = 0; i < NB_STEPS; i++) {
			list.add(i + " " + (density[i] / (double) (nbExps + 1)));
		}

		list.WriteToFile("michaos2-" + nbExps + "-" + DEF_RATIO + ".dat");
	}
	
	public static void main(String[] argv) {
		LaunchMessage(CLASSNAME);

		try {
			MichaosAlertTimeExp exp = new MichaosAlertTimeExp();
			exp.runLinExp();
		} catch(Exception e) {
			Macro.print(" Exception caught at the top: ");
			e.printStackTrace();
		}

		CommandInterpreter.ByeMessage();
	}
}
