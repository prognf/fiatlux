package expCA.diagnosticNG.michaos;

import components.types.FLStringList;
import components.types.IntC;
import experiment.measuring.specific.NonDefectiveActivityMD;
import experiment.samplers.CAsimulationSampler;
import grafix.gfxTypes.imgFormat.ImgFormat;
import main.Macro;
import main.commands.CommandInterpreter;
import models.CAmodels.binary.NGBinaryInitializer;
import models.CAmodels.decentralisedDiagnosis.NGChaosMajorityModel;
import topology.basics.SamplerInfoPlanar;
import topology.basics.TopologyCoder;

/**
 * @author Nicolas Gauville
 * Mesures de l'évolution des de l'activité
 */
public class MichaosActivityExp extends CommandInterpreter {
	final static String CLASSNAME = new Macro.CurrentClassGetter().getClassName();

	private static final int SIZE = 400;
	private static final IntC GridSize = new IntC(SIZE,SIZE);
	private static final IntC CellPixSize = new IntC(1,0);
	private static final int NB_EXP = 100; // mesures (100)
	private static final int NB_ITS = 500; // itérations (500)
	private static final int NB_AVG = 1; // tests a faire pour chaque regle (10)

	CAsimulationSampler m_samplerExp;
	NGBinaryInitializer m_initExp;

	private NGChaosMajorityModel model;
	private NonDefectiveActivityMD m_activityMD;

	private double activity[] = new double[NB_EXP];

	public MichaosActivityExp() {
	}

	private void initExp() {
		model = new NGChaosMajorityModel();

		SamplerInfoPlanar info2D = new SamplerInfoPlanar(GridSize, CellPixSize, model, TopologyCoder.s_TM8);
		// PlanarSamplerInfo info2D = new PlanarSamplerInfo(GridSize, CellPixSize, model, TopologyCoder.s_TV5);

		m_samplerExp = info2D.GetSimulationSampler();
		m_samplerExp.io_SetRecordFormat(ImgFormat.SVG);
		m_initExp = new NGBinaryInitializer();
		m_samplerExp.SetInitDeviceFromOutside(m_initExp);
		m_activityMD = new NonDefectiveActivityMD();
		m_activityMD.LinkTo(m_samplerExp);

		for (int i = 0; i < NB_EXP; i++) {
			activity[i] = 0;
		}
	}
	
	private void runLinExp(){
		initExp();
		runExp();
	}

	private void runExp() {
		int nbCells = SIZE * SIZE;

		for (int am = 0; am < NB_AVG; am++) {
			for (int i = 0; i < NB_EXP; i++) {
				System.out.println(am + "/" + NB_AVG + "  " + i + "/" + NB_EXP);
				double defRatio = ((double) i / (double) NB_EXP);

				((NGBinaryInitializer) m_samplerExp.GetInitializer()).setKoCells(defRatio);

				m_samplerExp.sig_Init();
				for (int t = 0; t < NB_ITS - 1; t++) {
					m_samplerExp.sig_NextStep();
				}

				activity[i] += m_activityMD.GetMeasure();
			}

			save("michaos-ou-zero", am + 1);
		}
	}

	private void save(String filename, int pAvg) {
		FLStringList list = new FLStringList();
		list.add("D A");

		double avg = (double) pAvg;
		for (int i = 0; i < NB_EXP; i++) {
			double defRatio = ((double) i / (double) NB_EXP);
			StringBuilder sb = new StringBuilder();
			sb.append(defRatio);

			list.add(defRatio + " " + (activity[i] / avg));
		}

		list.WriteToFile(filename + ".dat");
	}
	
	public static void main(String[] argv) {
		LaunchMessage(CLASSNAME);

		try{
			MichaosActivityExp exp = new MichaosActivityExp();
			exp.runLinExp();
		} catch(Exception e){
			Macro.print(" Exception caught at the top: ");
			e.printStackTrace();
		}	
		CommandInterpreter.ByeMessage();
	}
}
