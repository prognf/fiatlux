package expCA.diagnosticNG.egc2019;

import components.types.FLStringList;
import components.types.IntC;
import experiment.measuring.general.DensityMD;
import experiment.samplers.CAsimulationSampler;
import grafix.gfxTypes.imgFormat.ImgFormat;
import main.Macro;
import main.commands.CommandInterpreter;
import models.CAmodels.decentralisedDiagnosis.NGIsingStabInitializer;
import models.CAmodels.decentralisedDiagnosis.NGMajorityFailInitializer;
import models.CAmodels.nState.IsingStab;
import topology.basics.SamplerInfoPlanar;
import topology.basics.TopologyCoder;
import updatingScheme.AlphaScheme;

/**
 * @author Nicolas Gauville
 */
public class Exp1Ising extends CommandInterpreter {
	final static String CLASSNAME = new Macro.CurrentClassGetter().getClassName();

	private static final int SIZE = 100;
	private static final IntC GridSize = new IntC(SIZE,SIZE);
	private static final IntC CellPixSize = new IntC(1,0);
	private static final int NB_STEPS = 3000;

	CAsimulationSampler m_samplerExp;
	NGIsingStabInitializer m_initExp;

	private IsingStab model;
	private DensityMD m_densityMD;

	private double densities[][] = new double[2][NB_STEPS];

	public Exp1Ising() {
	}

	private void initExp() {
		model = new IsingStab();

		SamplerInfoPlanar info2D = new SamplerInfoPlanar(GridSize, CellPixSize, model, TopologyCoder.s_FM9);

		m_samplerExp = info2D.GetSimulationSampler();
		m_samplerExp.io_SetRecordFormat(ImgFormat.SVG);
		m_initExp = new NGIsingStabInitializer();

		m_samplerExp.SetInitDeviceFromOutside(m_initExp);

		m_densityMD = new DensityMD();
		m_densityMD.LinkTo(m_samplerExp);
	}
	
	private void runLinExp(){
		initExp();
		runExp();
	}

	private void runExp() {
		AlphaScheme updatingScheme = (AlphaScheme) m_samplerExp.GetUpdatingScheme();
		updatingScheme.SetSynchronyRate(1.0);

		FLStringList list = new FLStringList();
		list.add("T D A");

		m_samplerExp.sig_Init();

		for (int t = 0; t < NB_STEPS; t++) {
			m_samplerExp.sig_NextStep();
			densities[0][t] = m_densityMD.measureStateDensity(IsingStab.FAIL);
			densities[1][t] = m_densityMD.CountStateDensityWithoutFailure(IsingStab.ALERT, IsingStab.FAIL);
		}

		for (int i = 0; i < NB_STEPS; i++) {
			StringBuilder line = new StringBuilder();

			line.append(i).append(" ").append(densities[0][i]).append(" ").append(densities[1][i]);
			list.add(line.toString());
		}

		list.WriteToFile("Exp1Ising-2.dat");
	}
	
	public static void main(String[] argv) {
		LaunchMessage(CLASSNAME);

		try{
			Exp1Ising exp = new Exp1Ising();
			exp.runLinExp();
		} catch(Exception e){
			Macro.print(" Exception caught at the top: ");
			e.printStackTrace();
		}	
		CommandInterpreter.ByeMessage();
	}
}
