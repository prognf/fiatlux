package expCA.diagnosticNG.egc2019;

import components.types.FLStringList;
import components.types.IntC;
import experiment.measuring.general.DensityMD;
import experiment.samplers.CAsimulationSampler;
import main.Macro;
import main.commands.CommandInterpreter;
import models.CAmodels.decentralisedDiagnosis.NGIsingStabInitializer;
import models.CAmodels.nState.IsingStab;
import topology.basics.SamplerInfoPlanar;
import topology.basics.TopologyCoder;
import updatingScheme.AlphaScheme;

/**
 * @author Nicolas Gauville
 */
public class Exp2QuorumD extends CommandInterpreter {
	final static String CLASSNAME = new Macro.CurrentClassGetter().getClassName();

	private static final int SIZE = 100;
	private static final IntC GridSize = new IntC(SIZE,SIZE);
	private static final IntC CellPixSize = new IntC(1,0);
	private static final int NB_AVG = 100; // nombre d'échantillons
	private static final int NB_EXP = 30; // nombre de taux de défaillance testés
	private static final int NB_STEPS = 2000; // nombre d'itérations avant chaque mesure
	private static final int LAMBDA = 50;
	private static final double ALPHA = 1;

	private CAsimulationSampler m_samplerExp;
	private DensityMD m_alertMD;
	private double density[] = new double[NB_EXP];

	private void initExp() {
		IsingStab model = new IsingStab();

		SamplerInfoPlanar info2D = new SamplerInfoPlanar(GridSize, CellPixSize, model, TopologyCoder.s_FM8);

		m_samplerExp = info2D.GetSimulationSampler();
		NGIsingStabInitializer m_initExp = new NGIsingStabInitializer();
		m_samplerExp.SetInitDeviceFromOutside(m_initExp);
		m_alertMD = new DensityMD();
		m_alertMD.LinkTo(m_samplerExp);

		for (int i = 0; i < NB_EXP; i++) {
			density[i] = 0;
		}
	}
	
	private void runLinExp(){
		initExp();
		runExp();
	}

	private double getDefRatio(int i) {
		return ((double) i / (double) NB_EXP) * 0.14;
	}

	private void runExp() {
		for (int am = 0; am < NB_AVG; am++) {
			for (int i = 0; i < NB_EXP; i++) {
				double defRatio = getDefRatio(i);
				System.out.println("Ratio : " + defRatio + " (" + am + "/" + NB_AVG + ", " + i + "/" + NB_EXP + ")");

				((NGIsingStabInitializer) m_samplerExp.GetInitializer()).setProportions(0, defRatio);
				((IsingStab) m_samplerExp.GetCellularModel()).setLambda(LAMBDA);
				AlphaScheme updatingScheme = (AlphaScheme) m_samplerExp.GetUpdatingScheme();
				updatingScheme.SetSynchronyRate(ALPHA);

				m_samplerExp.sig_Init();
				for (int t = 0; t < NB_STEPS - 1; t++) {
					m_samplerExp.sig_NextStep();
				}

				density[i] += (m_alertMD.CountStateDensityWithoutFailure(IsingStab.ALERT, IsingStab.FAIL) > 0.95 ? 1 : 0);
			}

			saveExp(am);
		}

	}

	private void saveExp(int nbExps) {
		FLStringList list = new FLStringList();
		list.add("DF A");

		for (int i = 0; i < NB_EXP; i++) {
			double defRatio = getDefRatio(i);
			list.add(defRatio + " " + (density[i] / (double) (nbExps + 1)));
		}

		String filename= String.format("Exp2quorum-%d-lambda%d.dat",nbExps,LAMBDA);
		list.WriteToFile(filename);
	}
	
	public static void main(String[] argv) {
		LaunchMessage(CLASSNAME);

		try {
			Exp2QuorumD exp = new Exp2QuorumD();
			exp.runLinExp();
		} catch(Exception e) {
			Macro.print(" Exception caught at the top: ");
			e.printStackTrace();
		}

		CommandInterpreter.ByeMessage();
	}
}
