package expCA.diagnosticNG.egc2019;

import components.types.FLStringList;
import components.types.IntC;
import experiment.measuring.specific.AlertDensityMD;
import experiment.samplers.CAsimulationSampler;
import main.Macro;
import main.commands.CommandInterpreter;
import models.CAmodels.multiRegister.NG3StateInitializer;
import models.CAmodels.multiRegister.NGReactDiffDiagModel;
import topology.basics.SamplerInfoPlanar;
import topology.basics.TopologyCoder;
import updatingScheme.AlphaScheme;

/**
 * @author Nicolas Gauville
 */
public class Exp1GreenbergHastings extends CommandInterpreter {
	final static String CLASSNAME = new Macro.CurrentClassGetter().getClassName();

	private static final int SIZE = 100;
	private static final IntC GridSize = new IntC(SIZE,SIZE);
	private static final IntC CellPixSize = new IntC(1,0);
	private static final int NB_STEPS = 3000;
	private static final double P_FAIL = 0.00005;
	private static final double P_TAU = 0.31;

	private CAsimulationSampler m_samplerExp;

	private AlertDensityMD m_alertMD;

	private double densities[][] = new double[2][NB_STEPS];

	public Exp1GreenbergHastings() {
	}

	private void initExp() {
		NGReactDiffDiagModel model = new NGReactDiffDiagModel();

		SamplerInfoPlanar info2D = new SamplerInfoPlanar(GridSize, CellPixSize, model, TopologyCoder.s_FM8);

		m_samplerExp = info2D.GetSimulationSampler();
		NG3StateInitializer m_initExp = new NG3StateInitializer();
		m_samplerExp.SetInitDeviceFromOutside(m_initExp);
		m_alertMD = new AlertDensityMD(NGReactDiffDiagModel.ALERT_B);
		m_alertMD.LinkTo(m_samplerExp);
	}
	
	private void runLinExp() {
		initExp();
		runExp();
		saveExp("Exp1GH-c");
	}

	private void runExp() {
		int nbCells = SIZE * SIZE;

		((NG3StateInitializer) m_samplerExp.GetInitializer()).setKOCells(0);
		((NGReactDiffDiagModel) m_samplerExp.GetCellularModel()).setProbabilities(P_FAIL, 0, P_TAU);
		AlphaScheme updatingScheme = (AlphaScheme) m_samplerExp.GetUpdatingScheme();
		updatingScheme.SetSynchronyRate(1.0);

		m_samplerExp.sig_Init();
		for (int t = 0; t < NB_STEPS; t++) {
			m_samplerExp.sig_NextStep();
			densities[0][t] = m_alertMD.GetMeasure(NGReactDiffDiagModel.FAILURE);
			densities[1][t] = m_alertMD.GetMeasure(NGReactDiffDiagModel.ALERT_B, NGReactDiffDiagModel.FAILURE);
		}
	}

	private void saveExp(String filename) {
		FLStringList list = new FLStringList();

		list.add("T D A");

		for (int i = 0; i < NB_STEPS; i++) {
			StringBuilder sb = new StringBuilder();
			sb.append(i);
			sb.append(" ");
			sb.append(densities[0][i]);
			sb.append(" ");
			sb.append(densities[1][i]);

			list.add(sb.toString());
		}

		list.WriteToFile(filename + ".dat");
	}
	
	public static void main(String[] argv) {
		LaunchMessage(CLASSNAME);

		try {
			Exp1GreenbergHastings exp = new Exp1GreenbergHastings();
			exp.runLinExp();
		} catch(Exception e) {
			Macro.print(" Exception caught at the top: ");
			e.printStackTrace();
		}

		CommandInterpreter.ByeMessage();
	}
}
