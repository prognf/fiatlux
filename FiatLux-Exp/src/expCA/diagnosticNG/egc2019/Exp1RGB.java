package expCA.diagnosticNG.egc2019;

import components.types.FLStringList;
import components.types.IntC;
import experiment.measuring.general.DecimalParameterMeasurer;
import experiment.measuring.general.DensityMD;
import experiment.measuring.specific.AlertDensityMD;
import experiment.measuring.specific.NonDefectiveActivityMD;
import experiment.samplers.CAsimulationSampler;
import grafix.gfxTypes.imgFormat.ImgFormat;
import main.Macro;
import main.commands.CommandInterpreter;
import models.CAmodels.decentralisedDiagnosis.NGRgbInitializer;
import models.CAmodels.decentralisedDiagnosis.NGRgbModel;
import models.CAmodels.multiRegister.NG3StateInitializer;
import models.CAmodels.multiRegister.NGRgbDiagModel;
import topology.basics.SamplerInfoPlanar;
import topology.basics.TopologyCoder;

/**
 * @author Nicolas Gauville
 * Mesures de l'évolution des de l'activité de différentes règles
 * sur le modèle NGRgbModel
 */
public class Exp1RGB extends CommandInterpreter {
	final static String CLASSNAME = new Macro.CurrentClassGetter().getClassName();

	private static final int SIZE = 100;
	private static final IntC GridSize = new IntC(SIZE,SIZE);
	private static final IntC CellPixSize = new IntC(1,0);
	private static final double P_FAIL = 0.00005;
	private static final int NB_STEPS = 3000;

	CAsimulationSampler m_samplerExp;
	NG3StateInitializer m_initExp;

	private NGRgbDiagModel model;
	private AlertDensityMD m_densityMD;

	private double densities[][] = new double[2][NB_STEPS];

	public Exp1RGB() {
	}

	private void initExp() {
		model = new NGRgbDiagModel();

		SamplerInfoPlanar info2D = new SamplerInfoPlanar(GridSize, CellPixSize, model, TopologyCoder.s_FM8);

		m_samplerExp = info2D.GetSimulationSampler();
		m_samplerExp.io_SetRecordFormat(ImgFormat.SVG);
		m_initExp = new NG3StateInitializer();
		m_samplerExp.SetInitDeviceFromOutside(m_initExp);
		m_densityMD = new AlertDensityMD();
		m_densityMD.LinkTo(m_samplerExp);
	}

	private void runLinExp(){
		initExp();
		runExp(3, 0);

		save("Exp1RGB-b");
	}

	private void runExp(int a, int b) {
		((NGRgbDiagModel) m_samplerExp.GetCellularModel()).setProbabilities(P_FAIL, 0.0, true, 1.0);
		((NG3StateInitializer) m_samplerExp.GetInitializer()).setKOCells(0);

		m_samplerExp.sig_Init();
		for (int t = 0; t < NB_STEPS; t++) {
			m_samplerExp.sig_NextStep();

			densities[0][t] = m_densityMD.GetMeasure(NGRgbDiagModel.FAILURE);
			densities[1][t] = m_densityMD.GetMeasure(NGRgbDiagModel.ALERT_B, NGRgbDiagModel.FAILURE);
		}
	}

	private void save(String filename) {
		FLStringList list = new FLStringList();

		list.add("T D A");

		for (int i = 0; i < NB_STEPS; i++) {
			StringBuilder sb = new StringBuilder();
			sb.append(i);
			sb.append(" ");
			sb.append(densities[0][i]);
			sb.append(" ");
			sb.append(densities[1][i]);

			list.add(sb.toString());
		}

		list.WriteToFile(filename + "-2.dat");
	}
	
	public static void main(String[] argv) {
		LaunchMessage(CLASSNAME);

		try{
			Exp1RGB exp = new Exp1RGB();
			exp.runLinExp();
		} catch(Exception e){
			Macro.print(" Exception caught at the top: ");
			e.printStackTrace();
		}	
		CommandInterpreter.ByeMessage();
	}
}
