package expCA.diagnosticNG.egc2019;

import components.types.FLString;
import components.types.FLStringList;
import components.types.IntC;
import experiment.measuring.specific.AlertDensityMD;
import experiment.samplers.CAsimulationSampler;
import main.Macro;
import main.commands.CommandInterpreter;
import models.CAmodels.multiRegister.NG3StateInitializer;
import models.CAmodels.multiRegister.NGReactDiffDiagModel;
import topology.basics.SamplerInfoPlanar;
import topology.basics.TopologyCoder;
import updatingScheme.AlphaScheme;

/**
 * @author Nicolas Gauville
 */
public class Exp2GreenbergHastings extends CommandInterpreter {
	final static String CLASSNAME = new Macro.CurrentClassGetter().getClassName();

	private static final int SIZE = 100;
	private static final IntC GridSize = new IntC(SIZE,SIZE);
	private static final IntC CellPixSize = new IntC(1,0);
	private static final int NB_AVG = 100; // nombre d'échantillons
	private static final int NB_EXP = 30; // nombre de points de la courbe
	private static final int NB_STEPS = 2000; // nombre d'itérations avant chaque mesure
	private static final double ALPHA = 1;

	private static final double PTAU_GH = 0.37; 

	private CAsimulationSampler m_samplerExp;
	private AlertDensityMD m_alertMD;
	private double density[] = new double[NB_EXP];

	private void initExp() {
		NGReactDiffDiagModel model = new NGReactDiffDiagModel();

		SamplerInfoPlanar info2D = new SamplerInfoPlanar(GridSize, CellPixSize, model, TopologyCoder.s_FM8);

		m_samplerExp = info2D.GetSimulationSampler();
		NG3StateInitializer m_initExp = new NG3StateInitializer(NGReactDiffDiagModel.NORMAL, NGReactDiffDiagModel.EXCITED, NGReactDiffDiagModel.REFRACTORY, NGReactDiffDiagModel.FAILURE);
		m_samplerExp.SetInitDeviceFromOutside(m_initExp);
		m_alertMD = new AlertDensityMD();
		m_alertMD.LinkTo(m_samplerExp);

		for (int i = 0; i < NB_EXP; i++) {
			density[i] = 0;
		}
	}
	
	private void runLinExp(){
		initExp();
		runExp();
	}

	private double getDefRatio(int i) {
		return ((double) i / (double) NB_EXP) * 0.14;
	}

	private void runExp() {
		((NGReactDiffDiagModel) m_samplerExp.GetCellularModel()).setProbabilities(0.0, 0.0, PTAU_GH);
		for (int am = 0; am < NB_AVG; am++) {
			for (int i = 0; i < NB_EXP; i++) {
				double defRatio = getDefRatio(i);
				System.out.println("Ratio : " + defRatio + " (" + am + "/" + NB_AVG + ", " + i + "/" + NB_EXP + ")");

				((NG3StateInitializer) m_samplerExp.GetInitializer()).setKOCells(defRatio);
				AlphaScheme updatingScheme = (AlphaScheme) m_samplerExp.GetUpdatingScheme();
				updatingScheme.SetSynchronyRate(ALPHA);

				m_samplerExp.sig_Init();
				for (int t = 0; t < NB_STEPS - 1; t++) {
					m_samplerExp.sig_NextStep();
				}

				density[i] += (m_alertMD.GetMeasure(NGReactDiffDiagModel.ALERT_A, NGReactDiffDiagModel.FAILURE) + m_alertMD.GetMeasure(NGReactDiffDiagModel.ALERT_B, NGReactDiffDiagModel.FAILURE) > 0.95 ? 1 : 0);
			}

			saveExp(am);
		}

	}

	private void saveExp(int nbExps) {
		FLStringList list = new FLStringList();
		list.add("DF A");

		for (int i = 0; i < NB_EXP; i++) {
			double defRatio = getDefRatio(i);
			list.add(defRatio + " " + (density[i] / (double) (nbExps + 1)));
		}

		String parval= FLString.FormatIntWithZeros((int)(PTAU_GH*100.0), 2);
		String filename= String.format("Exp2GH-%d-ptau%s.dat",nbExps,parval);
		list.WriteToFile(filename);
	}
	
	public static void main(String[] argv) {
		Macro.BeginTest(CLASSNAME);

		try {
			Exp2GreenbergHastings exp = new Exp2GreenbergHastings();
			exp.runLinExp();
		} catch(Exception e) {
			Macro.print(" Exception caught at the top: ");
			e.printStackTrace();
		}

		Macro.EndTest();
	}
}
