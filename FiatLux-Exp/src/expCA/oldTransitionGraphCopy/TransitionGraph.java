package expCA.oldTransitionGraphCopy;

import main.Macro;
import main.MathMacro;
import models.CAmodels.CellularModel;
import topology.basics.SuperTopology;
import expCA.oldTransitionGraphCopy.Config.SYNCH;
/*-------------------------------------------------------------------------------
- 
- @author Nazim Fates
------------------------------------------------------------------------------*/
public class TransitionGraph {

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();
	
	/*----------------------------------------------------------------------------
		  - attributes
		  --------------------------------------------------------------------------*/

	Config [] m_config;
	int m_size; 
	SynchronisationSampler m_sampler;
	BinaryCodeInitializer m_init= new BinaryCodeInitializer();
	int [] m_stats = new int[3]; // how many converge to 0,1 or 2=cycles


	/*---------------------------------------------------------------------------
		  - construction
		  --------------------------------------------------------------------------*/

	TransitionGraph(int Lsize, CellularModel model, SuperTopology topo){
		m_size= MathMacro.Power(2, Lsize);
		m_config= new Config [m_size];
		for (int i=0; i< m_size; i++){
			m_config[i]= new Config(i);
		}
		m_sampler= new SynchronisationSampler(model, topo, Lsize);
		m_sampler.SetInitDeviceFromOutside(m_init);
		BuildGraph();
	}

	/*---------------------------------------------------------------------------
		  - actions
	  --------------------------------------------------------------------------*/


	public void BuildGraph(){
		// 0* and 1* are not considered at first 
		for (int code=1; code< m_size -1; code++){
			Config current= m_config[code];
			Config next= ComputeNext(current);
			current.SetSuccessor(next);
			int nextCode = next.GetCode();
			m_config[nextCode].AddPredecessor(current);
		}
		MarkSynch();
		MakeStats();
		// semi-hack
		AllZero().SetSuccessor( AllOne() );
		AllOne().SetSuccessor( AllZero() );
	}

	private Config ComputeNext(Config current) {
		m_init.SetInitConfiguration(current);
		m_sampler.sig_Init();
		m_sampler.sig_NextStep();
		int currentConfigCode= (int)m_sampler.GetCurrentConfigCode();
		Config next= m_config[ currentConfigCode ];
		return next;
	}

	/** we should not launch the recursion with all zero and all one **/
	public void recMarkConfig(Config config, Config.SYNCH state){
		if (config.getSynchroState() == SYNCH.none ){ // to avoid cycles
			config.setSynchroState( state );
			for (Config pred : config.GetPredecessors() ){
				recMarkConfig(pred, state);
			}
		}
	}

	private void MarkSynch() {
		recMarkConfig(m_config[0], SYNCH.Zero);
		recMarkConfig(m_config[m_size-1], SYNCH.One);
	}

	private void MakeStats(){
		for (Config current : m_config){
			if (current.getSynchroState() == SYNCH.Zero){
				m_stats[0]++;
			} else if (current.getSynchroState() == SYNCH.One){
				m_stats[1]++;
			} else {
				m_stats[2]++;
			}
		}
	}


	/*---------------------------------------------------------------------------
	  - get & set
	  --------------------------------------------------------------------------*/

	Config AllZero(){
		return m_config[0];
	}

	Config AllOne(){
		return m_config[m_size - 1];
	}

	private boolean hasNoCyle() {
		return m_stats[2]==0;
	}

	String getStats(){
		String s = String.format("<%d,%d>(%d)", m_stats[0] , m_stats[1], m_stats[2]);
		return s;
	}

	public void PrintGraph(){
		Macro.SeparationLine();
		for (Config current : m_config){
			Macro.print(""+ current.toString() );
		}
		Macro.SeparationLine();
		Macro.print(getStats());
		Macro.SeparationLine();

	}

	public String GetConfigInfo(int configCode){
		Config current= m_config[configCode];
		return ""+ current.toString();		
	}

	/*---------------------------------------------------------------------------
		  - test
		  --------------------------------------------------------------------------*/



}// end class

