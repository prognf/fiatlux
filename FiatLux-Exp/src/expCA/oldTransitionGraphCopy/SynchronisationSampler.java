package expCA.oldTransitionGraphCopy;

import main.Macro;
import main.MathMacro;
import models.CAmodels.CellularModel;
import topology.basics.LinearTopology;
import topology.basics.SuperTopology;

import components.allCells.OneRegisterIntCell;
import components.arrays.GenericCellArray;

import experiment.samplers.LinearSampler;


/*-------------------------------------------------------------------------------
- 
- @author Nazim Fates
------------------------------------------------------------------------------*/

public class SynchronisationSampler extends LinearSampler {

	final static String CLASSNAME= Thread.currentThread().getStackTrace()[1].getClassName();
	
	/*----------------------------------------------------------------------------
	  - attributes
	  --------------------------------------------------------------------------*/
	GenericCellArray<OneRegisterIntCell> m_cellArray;
	
	/*---------------------------------------------------------------------------
	  - construction
	  --------------------------------------------------------------------------*/
	

	public SynchronisationSampler(CellularModel model, SuperTopology topo, int size) {
		super();
		((LinearTopology)topo).SetSize(size); // cast here !
		super.CreateNewSampler(model, size, topo);
		m_cellArray= new GenericCellArray<OneRegisterIntCell>(GetAutomaton());
	}

	
	/*---------------------------------------------------------------------------
	  - actions
	  --------------------------------------------------------------------------*/
	
	/*---------------------------------------------------------------------------
	  - get & set
	  --------------------------------------------------------------------------*/
	
	/*---------------------------------------------------------------------------
	  - test
	  --------------------------------------------------------------------------*/
	
	public static void DoTest(){
		Macro.BeginTest(CLASSNAME);
		Macro.EndTest();
	}

	public long GetCurrentConfigCode() {
		int size= GetSize();
		int [] config= new int[size];
		for (int pos=0; pos< size; pos++){
			config[pos]= m_cellArray.GetCell(pos).GetState();
		}
		long bCode= MathMacro.BinaryTab2Decimal(config);
		return bCode;
	}

}// end class
