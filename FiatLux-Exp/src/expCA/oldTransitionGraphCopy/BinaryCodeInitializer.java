package expCA.oldTransitionGraphCopy;

import components.types.IntPar;
import grafix.gfxTypes.elements.FLPanel;
import initializers.CAinit.OneRegisterInitializer;
import main.MathMacro;

public class BinaryCodeInitializer extends OneRegisterInitializer {

	IntPar m_initCond= new IntPar("Init Bin. code:", 5);
	
	@Override
	public FLPanel GetSpecificPanel() {
		return m_initCond.GetControl();
	}

	@Override
	protected void SubInit() {
		int size= GetLsize();
		String sequence= MathMacro.Int2BinaryString(m_initCond.GetVal(), size);
		for (int pos=0; pos < GetLsize(); pos++){
			if (sequence.charAt(size - 1 - pos) == '1'){
				InitState(pos, 1);
			} else {
				InitState(pos, 0);
			}
		}
	}

	/** code = binary transcription of the initial conditon **/ 
	public void SetInitConfiguration(Config current){
		m_initCond.SetVal(current.GetCode());
	}

	
		
	
}
