package expCA.oldTransitionGraphCopy;

import java.awt.event.MouseEvent;

import components.types.IntC;
import grafix.gfxTypes.FLColor;
import grafix.viewers.GridViewer;
import grafix.viewers.MouseInteractive;
import main.Macro;

public class RuleCycleViewer extends GridViewer 
	implements MouseInteractive {
	
	DoExploreSynch m_exp;

	private int m_NCOLORS;

	final static int Lsize = 128;
	final static IntC xy= new IntC(Lsize, Lsize);
		public RuleCycleViewer(IntC inCellPixSize) {
		super(inCellPixSize, xy);
		ActivateMouseFollower(this);
		//ECAexp();	
		/*LV4exp();*/
	}

	/*
	private void ECAexp() {
		m_NCOLORS = 6;
		m_Lsize = 8;
		SetXYsize(new IntC(m_Lsize, m_Lsize));
		//SetPalette( PaintToolKit.GetRandomPalette(NCOLORS));
		SetPalette( PaintToolKit.GetBW2palette(m_NCOLORS+1)); //TODO +1 ?
		//SetPalette( PaintToolKit.GetRainbow(NCOLORS));
		m_exp= new DoExploreSynch();
		m_exp.ExploreECASpace();
		
	}*/
	

/*	
	private void LV4exp() {
		m_NCOLORS = 7;
		//SetPalette( PaintToolKit.GetRandomPalette(NCOLORS));
		SetPalette( PaintToolKit.GetBW2palette(m_NCOLORS+1)); //TODO +1 ?
		//SetPalette( PaintToolKit.GetRainbow(NCOLORS));
		m_exp= new DoExploreSynch();
		m_exp.ExploreLV4Space();		
	}
*/	

	
	protected void DrawSystemState() {
		for (int x = 0; x < getXsize(); x++) {
			for (int y = 0; y < getYsize(); y++) {
				int state = GetSmallestCycleLength(x,y);
				//int Wcode = GetWcode(x,y);
				FLColor col;
				col = GetColor(state);
				SetColor(col);								
				DrawSquare(x, y);
				
				//gc.setColor(FLColor.black);
				//String msg= String.format("%d", Wcode);
				//WriteTextInCell(x,y,msg);
				
			}
		}
	}

	private int GetSmallestCycleLength(int x, int y) {
		int Wcode = GetWcode(x,y);
		return m_exp.getSmallestCycle(Wcode);
	}

	private int GetWcode(int x, int y) {
		int calc= FromNestedToLinear(x, y);
		int Wcode= 2 * calc +1 ;
		return Wcode;
	}

	@Override
	public void ProcessMouseEventXY(int x, int y, MouseEvent event) {
		int code= FromNestedToLinear(x,y);
		int Wcode= 1+ 2* code;
		String msg= String.format(" rule %d: %d", Wcode, GetSmallestCycleLength(x,y));
		
		Macro.print(2, this, "processXY " + msg);		
	}

	private int FromNestedToLinear(int x, int y) {
		int result=0;
		int multFactor= 1;
		while (x>0 || y>0){
			int add= (x & 1) + ((y & 1) << 1);
			result += add * multFactor;
			x >>= 1 ; y >>=1 ; // divide by two
			multFactor <<= 2 ; // multiply by 4
		}
		return result;
	}

/*	
	private static void Run() {
		FLFrame frame= new FLFrame("cycle viewer");
		IntC CellPixSize= new IntC(5, 0);
		RuleCycleViewer viewer= new RuleCycleViewer(CellPixSize);
		frame.AddPanel(viewer);
		frame.PackAndShow();		
	}

	public static void main (String[] args){
		Run();		
	}
*/
	
}
