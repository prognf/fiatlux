package expCA.oldTransitionGraphCopy;

import java.util.ArrayList;
// to update ?
public class Config {

	public enum SYNCH {none, Zero, One}

	private int m_value; // associated binary code
	Config m_sucessor;
	ArrayList<Config> m_predecessors;
	private SYNCH m_SynchroState= SYNCH.none; // by default no convergence
	

	Config(int bCode){
		m_value= bCode;
		m_predecessors= new ArrayList<Config>();
	}

	public void SetSuccessor(Config succ){
		m_sucessor= succ;
	}
	
	public void AddPredecessor(Config pred){
		m_predecessors.add(pred);
	}
	
	public ArrayList<Config> GetPredecessors() {
		return m_predecessors;
	}
	
	public String toString() {
		StringBuilder s= new StringBuilder("code : " + GetCode());
		if (m_sucessor == null){
			s.append("no succ. set !!");
		} else {
			s.append(" succ: ").append(m_sucessor.m_value);
		}
		s.append(" pred: ");
		s.append("[ ");
		for (Config pred : m_predecessors){
			s.append(pred.m_value).append(" ");
		}
		s.append("]");
		s.append(" -> ").append(m_SynchroState);
		return s.toString();
	}

	public int GetCode() {
		return m_value;
	}

	public SYNCH getSynchroState() {
		return m_SynchroState;
	}

	public void setSynchroState(SYNCH synchroState) {
		m_SynchroState = synchroState;
	}

}
