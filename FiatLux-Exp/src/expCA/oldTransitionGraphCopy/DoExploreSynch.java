package expCA.oldTransitionGraphCopy;

import components.randomNumbers.FLRandomGenerator;
import components.types.FLStringList;
import components.types.IntC;
import components.types.RuleCode;
import grafix.gfxTypes.elements.FLFrame;
import grafix.viewers.SimpleExampleGridViewer;
import main.Macro;
import main.MathMacro;
import models.CAmodels.tabled.GeneralTabledBinaryModel;
import models.CAmodels.tabled.TabledModel;
import topology.basics.SuperTopology;
import topology.zoo.RadiusN_Topology;

public class DoExploreSynch {

	final static String CLASSNAME= Thread.currentThread().getStackTrace()[1].getClassName();
	private static final int SIZEMAX = 10;
	private boolean PRINTGRAPH = false; // for detailed analysis

	/*----------------------------------------------------------------------------
	  - attributes
	  --------------------------------------------------------------------------*/
	SuperTopology m_topo; 
	private String m_RuleCode; // ECA, LV4 ,... 

	TabledModel m_model;

	FLRandomGenerator m_rand= FLRandomGenerator.GetGlobalRandomizer();
	private String m_filename;

	private int m_smallestCycle [];// for presenting results as an image code -> X,Y

	FLStringList m_results = new FLStringList(); // storing results


	/*---------------------------------------------------------------------------
	  - construction
	  --------------------------------------------------------------------------*/


	private void Prepare(SuperTopology topo, TabledModel in_model, String spaceCode, int nbits) {
		m_topo= topo;
		m_RuleCode= spaceCode;
		m_model= in_model;
		m_filename= m_RuleCode + ".txt";
		int size= (int)MathMacro.TwoToPowerLongResult(nbits - 2); // cast here
		m_smallestCycle = new int[size];

	}

	private void Prepare(RadiusN_Topology topo, String spaceCode, int nbits) {
		Prepare(topo, GeneralTabledBinaryModel.EmptyRule(nbits), spaceCode, nbits);
	}

	private void EndExp() {
		m_results.WriteToFile(m_filename);		
	}

	/*---------------------------------------------------------------------------
	  - actions
	  --------------------------------------------------------------------------*/
	public void TestUniversalConfiguration(){
		Prepare(new RadiusN_Topology(1), "ECA", 8);
		for (int Wcode= 1; Wcode<= 127; Wcode+=2){
			m_model.SetRuleWcode(new RuleCode(Wcode));
			TransitionGraph grph= new TransitionGraph(8, m_model, m_topo);			
			//TransitionGraph grph= new TransitionGraph(8, new ECAmodel((int)Wcode), new RadiusN_Topology(1));
			grph.BuildGraph();
			Macro.print( "W: " + Wcode + " > " + grph.GetConfigInfo(23) );
		}
		EndExp();
	}
	
	/* OLD
	public void ExploreOTV3Space(){
		int nbits= 12;
		LinearTopology topo= new RadiusN_Topology(3);
		BinaryTabledModel model= new OuterTotalisticModel(topo, 0); // TODO CHANGE THIS ???? 
		Prepare(topo, model, "OTV3",nbits);
		long nrules= MathMacro.TwoToPowerInt(nbits);
		for (long Wcode= 1; Wcode< nrules /2 ; Wcode+=2){
			ExploreOneRule(Wcode,"OTV3");			
		}
		*/


/*
	public void ExploreLV5Space(){
		Prepare(new RadiusN_Topology(2),"LR2",32);		
		long nrules= MathMacro.Power(2, 32);
		for (int N= 1; N< 100000; N++){
			int maxRuleNum= (int) nrules/4; // to avoid overflow !!! 
			int Wcode = 2* m_rand.RandomInt(maxRuleNum) + 1;
			ExploreOneRule(Wcode,"NC5");			
		}
		EndExp();
	}


	public void ExploreLV4Space(){
		Prepare(new RadiusN_Topology(-1,2), "LV4", 16);
		for (long Wcode= 1; Wcode< 32768; Wcode+=2){
			ExploreOneRule(Wcode,"NC3");			
		}
		EndExp();
	}

	public void ExploreECASpace(){
		Prepare(new RadiusN_Topology(1), "ECA", 8);
		for (long Wcode= 1; Wcode<= 127; Wcode+=2){
			ExploreOneRule(Wcode,"ECA");			
		}
		EndExp();
	}
*/


	/*
	private void ExploreOneRule(long Wcode, String condition) {
		m_model.SetRuleWcode(new RuleCode (Wcode));
		String transitionTable= m_model.GetLookUpTable().GetBinaryDigits();
		String out = String.format("%s %d [%s] ",m_RuleCode, Wcode,transitionTable);
		int Lsize=3;
		boolean noCycle=true;
		while ((Lsize<=SIZEMAX) && noCycle ){
			Lsize++;			
			TransitionGraph grph= new TransitionGraph(Lsize, m_model, m_topo);
			if (PRINTGRAPH)
				grph.PrintGraph();
			if (grph.m_stats[2] == 0){
				out += String.format("NC%d %s ", Lsize,grph.getStats());
			} else{
				noCycle= false;
			}			
		}
		if (out.contains(condition)){
			m_results.Add(out);	
		}
		int posCode = (int)((Wcode - 1) / 2);
		m_smallestCycle[posCode]= Lsize - 1;
	}
*/

	private static void VisualExploration() {
		FLFrame frame= new FLFrame("test simple grid");
		IntC CellPixSize= new IntC(40, 2);
		SimpleExampleGridViewer viewer= new SimpleExampleGridViewer(CellPixSize);
		frame.addPanel(viewer);
		frame.packAndShow();		
	}

	/*---------------------------------------------------------------------------
	  - get & set
	  --------------------------------------------------------------------------*/

	public int getSmallestCycle(int Wcode) {
		int posCode= (Wcode - 1)/ 2 ;
		return m_smallestCycle[posCode];
	}

	/** transformation by suppressing extremal bits **/
	static public IntC MapCode(long decCode){
		int multFactor=1;
		int x=0, y=0;
		long code= (decCode -1) / 2;  
		while (code > 0) {
			x += multFactor * (code & 1);
			y += multFactor * ((code & 2) >>1);
			code >>= 2;
		multFactor<<=1;
		}
		//Macro.print(String.format(" code %d -> %d,%d",decCode,x,y));
		return new IntC(x, y);
	}
	/*---------------------------------------------------------------------------
	  - main & test
	  --------------------------------------------------------------------------*/

/*
	private void Debug861(){
		int Wcode= 861;
		m_model.SetRuleWcode(Wcode);
		String transitionTable= m_model.GetLookUpTable().GetBinaryDigits();
		String out = String.format("LV4 %d [%s] ",Wcode,transitionTable);
		TransitionGraph grph= new TransitionGraph(6, m_model, m_topo);
		SynchronisationSampler samp= grph.m_sampler;
		Config test1= grph.m_config[11];
		String conf1= test1.toString();
		grph.m_init.SetInitConfiguration(test1);
		int currentConfigCode= (int)samp.GetCurrentConfigCode();
		Config next= grph.m_config[ currentConfigCode ];
		String conf2= next.toString();
		//Macro.Debug( conf1 + " -|- " + conf2 ) ;
	}*/


/*
	public static void main(String [] argv){
		//Test();
		CommandParser parser = new CommandParser(CLASSNAME, "", argv);
		DoExploreSynch exp= new DoExploreSynch();
		if (argv.length == 1 ){
			exp.PRINTGRAPH= true;
			int Wcode= parser.IParse("W=");		
			exp.Prepare(new RadiusN_Topology(2),"LR2", 32);
			exp.ExploreOneRule((long)Wcode,"NC");
			exp.EndExp();
		} else{
			exp.TestUniversalConfiguration();
			//exp.ExploreOTV3Space();
			//exp.ExploreECASpace();
			//exp.ExploreLV5Space();
			//exp.ExploreLV4Space();
			//exp.Debug861();
		}
		parser.Bye();
	}

*/
	private static void Test() {
		for (int i=0;i<=33;i++){
			Macro.print( i + " " + MathMacro.TwoToPowerIntResult(i));//buildMap(i);
		}

	}




}
