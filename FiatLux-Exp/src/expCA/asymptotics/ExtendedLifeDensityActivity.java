package expCA.asymptotics;

import java.util.Locale;

import components.types.FLString;
import components.types.FLStringList;
import components.types.IntC;
import experiment.measuring.general.ActivityMD;
import experiment.measuring.general.DensityMD;
import experiment.samplers.CAsampler;
import experiment.samplers.PlanarSampler;
import initializers.CAinit.BinaryInitializer;
import main.Macro;
import main.commands.CommandParser;
import models.CAmodels.binary.BinaryModel;
import models.CAmodels.binary.LifeModelParameters;
import topology.basics.PlanarTopology;
import topology.zoo.Moore8TM;

public class ExtendedLifeDensityActivity {

	private static final int TTRANS = 4000, TSAMP=20;

	CAsampler m_sampler;
	private DensityMD m_densMD;
	private ActivityMD m_activityMD;
	private BinaryModel m_model;
	private BinaryInitializer m_init;

	String m_output_filename;

	public ExtendedLifeDensityActivity(int A, int B, int C, int D, int L) {
			m_model= new LifeModelParameters(A, B, C, D);
			IntC XYsize= new IntC(L,L);
			PlanarTopology topology= new Moore8TM();
			m_sampler= new PlanarSampler(XYsize, m_model, topology);
			m_densMD= new DensityMD();
			m_densMD.LinkTo(m_sampler);
			m_activityMD= new ActivityMD();
			m_activityMD.LinkTo(m_sampler);
			m_init=(BinaryInitializer)m_sampler.GetInitializer();
	}

	private FLStringList doOneSampling(int seed){
		FLStringList output= new FLStringList();
		Macro.fPrint("sampling; seed:%s", seed);
		m_init.SetSeed(seed);
		m_sampler.sig_Init(); 
		
		int 
		seedI= m_sampler.GetInitSeed();
		//seedM= m_sampler.GetModelSeed();
		for (int t=0; t<TTRANS-1; t++){
			m_sampler.sig_NextStep();
		}
		for (int t=0; t<TSAMP; t++){
			m_sampler.sig_NextStep();
			double dens= m_densMD.GetMeasure(), act= m_activityMD.GetMeasure();
			int time= m_sampler.GetTime();
			String strMeasure= String.format(Locale.ENGLISH, 
					"%4d,%.3f,%.3f,%8d",
					time, dens, act, seedI);
			output.Add(strMeasure);
		}
		return output;
	}


	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();


	public static void main(String [] argv){
		CommandParser parser = new CommandParser(CLASSNAME, "", argv);
		String R=parser.SParse("R=");
		int A= FLString.String2Int(R.substring(0, 1));
		int B= FLString.String2Int(R.substring(1, 2));
		int C= FLString.String2Int(R.substring(2, 3));
		int D= FLString.String2Int(R.substring(3, 4));
		int L=parser.IParse("L=");
		int seed= parser.IParse("SEED=");
		ExtendedLifeDensityActivity exp= new ExtendedLifeDensityActivity(A,B,C,D,L);
		
		Macro.print("****** starting exp *****");
		FLStringList out= exp.doOneSampling(seed);
		out.Print();
		String filename= String.format("Life%d%d%d%d-L%d.dat",A,B,C,D,L);
		out.WriteToFile(filename);
	}

}
