package expCA.asymptotics;

import java.util.Locale;

import components.types.FLStringList;
import components.types.IntC;
import experiment.measuring.general.DensityMD;
import experiment.samplers.CAsampler;
import experiment.samplers.PlanarSampler;
import main.Macro;
import main.MathMacro;
import main.commands.CommandParser;
import models.CAmodels.decentralisedDiagnosis.QuorumDInit;
import models.CAmodels.decentralisedDiagnosis.QuorumDModel;
import topology.basics.PlanarTopology;
import topology.zoo.vonNeumannTM;

public class QuorumDasymptoticDexp {


	final static int L=100;
	private static final int TTRANS = 40000, TPOST=2000;
	private static final int Lmin=100,Lmax=360,Lstp=10; //lambda

	CAsampler m_sampler;
	private DensityMD m_densMD;
	private QuorumDModel m_model;
	private QuorumDInit m_init;


	String m_output_filename;


	public QuorumDasymptoticDexp() {
		m_model= new QuorumDModel();
		IntC XYsize= new IntC(L,L);
		PlanarTopology topology= new vonNeumannTM();
		m_sampler= new PlanarSampler(XYsize, m_model, topology);
		m_densMD= new DensityMD();
		m_densMD.LinkTo(m_sampler);
		m_init = (QuorumDInit)m_sampler.GetInitializer();
	}

	private String doOneSampling(int iKappa, int iLambda){
		double kappa = iKappa/MathMacro.HUNDRED;
		double lambda= iLambda/MathMacro.HUNDRED;
		Macro.fPrint("sampling : Kappa=%.2f Lambda=%.2f", kappa, lambda);
		
		m_init.setDfailure(kappa);
		m_sampler.sig_Init(); 
		
		m_model.setLambda(lambda);
		
		int 
		seedI= m_sampler.GetInitSeed(),
		seedM= m_sampler.GetModelSeed();
		for (int t=0; t<TTRANS; t++){
			m_sampler.sig_NextStep();
		}
		double densA= m_densMD.measureStateDensity(QuorumDModel.N);
		for (int t=0; t<TPOST; t++){
			m_sampler.sig_NextStep();
		}
		double densB= m_densMD.measureStateDensity(QuorumDModel.N);
		String strMeasure= String.format(Locale.ENGLISH, 
				"%d,%d,%.3f,%.3f,%d,%d",
				iKappa, iLambda,densA, densB, seedI, seedM);
		return strMeasure;
	}

	/** sampling one given rule **/
	private FLStringList MeasureCurrentRule(int iKappa) {
		FLStringList output= new FLStringList();
		String header="KAPPA,P,DNT1,DNT2,seedI,seedM";
		output.Add(header);
		for (int iLambda=Lmin; iLambda<=Lmax; iLambda+=Lstp){
			String res= doOneSampling(iKappa,iLambda);
			output.Add( res );
			Macro.fPrint(" sampled kappa:%d p:%d > %s", iKappa, iLambda, res); 
		}
		return output;
	}

	/** exploration of one value **/
	private void Process(int kappaPercent){
		Macro.print("****** starting exp *****");
		FLStringList out= MeasureCurrentRule(kappaPercent);
		out.Print();
		String filename= "quorumd-K"+ kappaPercent + ".dat";
		out.WriteToFile(filename);
	}


	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();


	public static void main(String [] argv){
		CommandParser parser = new CommandParser(CLASSNAME, "", argv);
		QuorumDasymptoticDexp exp= new QuorumDasymptoticDexp();
		int KAPPA= parser.IParse("KAPPA=");
		exp.Process(KAPPA);
	}

}
/*for (int W : PtrWithZero){
Macro.print(W + " " + ECAlookUpTable.GetTransitionCodePadded(new RuleCode(W)) );
}*/