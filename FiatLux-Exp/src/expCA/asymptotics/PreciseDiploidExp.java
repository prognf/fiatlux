package expCA.asymptotics;

import java.util.Locale;

import components.types.FLStringList;
import components.types.RuleCode;
import experiment.measuring.MeasuringDevice;
import experiment.measuring.general.DecimalParameterMeasurer;
import experiment.measuring.general.DensityMD;
import experiment.measuring.linear.BlocMD;
import experiment.measuring.linear.KinksMD;
import experiment.samplers.CAsampler;
import experiment.samplers.LinearSampler;
import main.Macro;
import main.commands.CommandParser;
import models.CAmodels.stochastic.DiploidECAmodel;
import models.CAmodels.tabled.ECAlookUpTable;
import topology.basics.SamplerInfoLinear;
import topology.basics.TopologyCoder;

public class PreciseDiploidExp {

	
	final static int Amin=36, Amax=44, Astp=1;
	final static int N=10000;
	private static final int TMAX = 200000, TPOST=20000;

	
	CAsampler m_sampler;
	private DecimalParameterMeasurer m_densMD, m_kinksMD, m_blocMD;
	private DiploidECAmodel m_model;

	String m_output_filename;

	
	public PreciseDiploidExp() {
	}
	
	final int [] WITHNOT51a= {1, 3, 5, 7, 9, 11, 13, 15, 25, 27, 
			29, 33, 37, 45, 73, 77};
	final int [] WITHNOT51b= {32, 34, 40, 42, 56, 57, 58, 104, 106, 122,   	
			160, 162, 168, 170, 232};
	
		
	/** exploration of a diagonal **/
	private void DealWithCouple(int W1, int W2){
		Macro.print("****** Dealing with diploid :" + W1 + "," + W2);
		BuildSampler(W1,W2);
		FLStringList out= MeasureCurrentRule();
		out.Print();
		String filename= "asymptotic-"+ W1 + "-" + W2 + ".dat";
		out.WriteToFile(filename);
	}


	public void BuildSampler(int W1, int W2){
		RuleCode R1= new RuleCode(W1), R2= new RuleCode(W2);

		String 	topoCode = TopologyCoder.s_TLR1;
		m_model= new DiploidECAmodel(R1, R2);
		SamplerInfoLinear info= new SamplerInfoLinear(topoCode, m_model, N);
		m_sampler= new LinearSampler(info);
		m_densMD=  new DensityMD();
		m_kinksMD= new KinksMD();
		m_blocMD=  new BlocMD();
		((MeasuringDevice)m_densMD).LinkTo(m_sampler);
		((MeasuringDevice)m_kinksMD).LinkTo(m_sampler);
		((MeasuringDevice)m_blocMD).LinkTo(m_sampler);
	}

	private String DoExp(double alpha){
		m_model.SetWeightRule2(alpha);
		m_sampler.sig_Init();
		int 
		seedI= m_sampler.GetInitSeed(),
		seedM= m_sampler.GetModelSeed();
		for (int t=0; t<TMAX; t++){
			m_sampler.sig_NextStep();
		}
		double 	densA= m_densMD.GetMeasure(),
				kinksA= m_kinksMD.GetMeasure(),
				blocA=m_blocMD.GetMeasure();
		for (int t=0; t<TPOST; t++){
			m_sampler.sig_NextStep();
		}

		double 
		densB= m_densMD.GetMeasure(),
		kinksB= m_kinksMD.GetMeasure(),
		blocB=m_blocMD.GetMeasure();
		String strMeasure= String.format(Locale.ENGLISH, 
				"%.3f  %.3f %.3f %.3f post: %.3f %.3f %.3f seedIM: %d %d",
				alpha,
				densA, kinksA, blocA,
				densB, kinksB, blocB,
				seedI, seedM);
		return strMeasure;
	}



	/** sampling one given rule **/
	private FLStringList MeasureCurrentRule() {
		FLStringList output= new FLStringList();
		for (int ialpha=Amin; ialpha<=Amax; ialpha+=Astp){
			Sample(ialpha, output); 
		}
		return output;
	}

	private void Sample(int ialpha, FLStringList output) {
		double alpha=ialpha/(double)100;
		String res= DoExp(alpha);
		output.Add( res  );
		Macro.fPrint(" sampled :%d > %s", ialpha, res);
	}


	private void ListEquivalenceTwoNeighbECA() {
		for (int index=1;index<16;index++){
			ECAlookUpTable.IdentifyECARule(index);
		}
	}

	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();

	final static int [] PtrWithZero = { 18, 22, 26, 28, 30, 50, 54, 58, 60, 62, 78, 90, 94, 110,
		122, 126, 146, 150, 154, 156, 158, 178, 182, 186, 188, 190, 202, 206, 218, 222, 234, 238, 250, 254};
	
	public static void main(String [] argv){
		CommandParser parser = new CommandParser(CLASSNAME, "", argv);
			PreciseDiploidExp exp= new PreciseDiploidExp();
			exp.DealWithCouple(51,77);
	}

}
/*for (int W : PtrWithZero){
Macro.print(W + " " + ECAlookUpTable.GetTransitionCodePadded(new RuleCode(W)) );
}*/