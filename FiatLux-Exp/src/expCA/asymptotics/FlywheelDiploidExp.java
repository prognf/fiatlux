package expCA.asymptotics;

import architecture.dynamicalSystems.DiploidFlywheel;
import main.Macro;
import main.commands.CommandParser;

public class FlywheelDiploidExp {


	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();
	private static final int N = 100;
	private static final int TEXP = 1000;

	public FlywheelDiploidExp() {
	}

	public static void main(String [] argv){
		CommandParser parser = new CommandParser(CLASSNAME, "", argv);
		if (argv.length == 0 ){
			TestFirst();
		} else {
			Macro.FatalError("???");
		}
	}

	private static void TestFirst() {
		DiploidFlywheel exp= new DiploidFlywheel(N);
		exp.PrnState();
		for (int i=1; i<TEXP; i++) {
		exp.sig_NextStep();
		exp.PrnState();
		}
		
	}

}
