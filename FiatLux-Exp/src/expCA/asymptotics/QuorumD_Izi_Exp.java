package expCA.asymptotics;

import java.util.Locale;

import components.types.FLStringList;
import components.types.IntC;
import experiment.measuring.MeasuringDevice;
import experiment.measuring.general.DensityMD;
import experiment.samplers.CAsampler;
import experiment.samplers.PlanarSampler;
import main.Macro;
import main.MathMacro;
import main.commands.CommandParser;
import models.CAmodels.decentralisedDiagnosis.QuorumDModel;
import models.CAmodels.decentralisedDiagnosis.QuorumFrontIzi;
import models.CAmodels.decentralisedDiagnosis.QuorumFrontLacaInit;
import topology.basics.PlanarTopology;
import topology.zoo.NCE_Topology;

public class QuorumD_Izi_Exp {


	final static int L=100;
	private static final IntC XYsize = new IntC(L,L);

	private static final int TTRANS = 20000, TPOST=2000;
	private static final int Kmin=2,Kmax=10,Kstp=2;
	private static final int Pmin=10,Pmax=60,Pstp=2;

	CAsampler m_sampler;
	private DensityMD m_densMD;
	private QuorumFrontIzi m_model;
	private QuorumFrontLacaInit m_init;


	String m_output_filename;


	public QuorumD_Izi_Exp() {
		m_model= new QuorumFrontIzi();
	}

	public void BuildSampler(int kappaPercent){
		PlanarTopology topo2D= new NCE_Topology();
		m_sampler= new PlanarSampler(XYsize, m_model, topo2D);
		m_init=(QuorumFrontLacaInit)m_sampler.GetInitializer();
		m_densMD=  new DensityMD();
		((MeasuringDevice)m_densMD).LinkTo(m_sampler);

		m_init.setInitVal(-1);
		m_init.setKappa(kappaPercent);

	}

	private String doOneSampling(int kappaPercent, int pPercent){
		Macro.fPrint("sampling : %d %d", kappaPercent, pPercent);
		m_sampler.sig_Init();
		m_model.setP(pPercent/MathMacro.HUNDRED);
		int 
		seedI= m_sampler.GetInitSeed(),
		seedM= m_sampler.GetModelSeed();
		for (int t=0; t<TTRANS; t++){
			m_sampler.sig_NextStep();
		}
		double densA= m_densMD.measureStateDensity(QuorumDModel.N);
		for (int t=0; t<TPOST; t++){
			m_sampler.sig_NextStep();
		}
		double densB= m_densMD.measureStateDensity(QuorumDModel.N);
		String strMeasure= String.format(Locale.ENGLISH, 
				"%d,%d,%.3f,%.3f,%d,%d",
				kappaPercent, pPercent,densA, densB, seedI, seedM);
		return strMeasure;
	}

	/** sampling one given rule **/
	private FLStringList MeasureCurrentRule(int kappaPercent) {
		BuildSampler(kappaPercent);
		FLStringList output= new FLStringList();
		String header="KAPPA,P,DNT1,DNT2,seedI,seedM";
		output.Add(header);
		for (int pPercent=Pmin; pPercent<=Pmax; pPercent+=Pstp){
			String res= doOneSampling(kappaPercent,pPercent);
			output.Add( res );
			Macro.fPrint(" sampled kappa:%d p:%d > %s", kappaPercent, pPercent, res); 
		}
		return output;
	}

	/** exploration of one value **/
	private void Process(int kappaPercent){
		Macro.print("****** starting exp *****");
		FLStringList out= MeasureCurrentRule(kappaPercent);
		out.Print();
		String filename= "quorumd-K"+ kappaPercent + ".dat";
		out.WriteToFile(filename);
	}


	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();


	public static void main(String [] argv){
		CommandParser parser = new CommandParser(CLASSNAME, "", argv);
		QuorumD_Izi_Exp exp= new QuorumD_Izi_Exp();
		int KAPPA= parser.IParse("KAPPA=");
		exp.Process(KAPPA);
	}

}
/*for (int W : PtrWithZero){
Macro.print(W + " " + ECAlookUpTable.GetTransitionCodePadded(new RuleCode(W)) );
}*/