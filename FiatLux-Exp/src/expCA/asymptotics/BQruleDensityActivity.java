package expCA.asymptotics;

import java.util.Locale;

import components.types.FLStringList;
import components.types.IntC;
import experiment.measuring.general.ActivityMD;
import experiment.measuring.general.DensityMD;
import experiment.samplers.CAsampler;
import experiment.samplers.PlanarSampler;
import main.Macro;
import main.commands.CommandParser;
import models.CAmodels.decentralisedDiagnosis.BiasedQuiesentModelToom;
import models.CAmodels.decentralisedDiagnosis.QuorumDInit;
import topology.basics.PlanarTopology;
import topology.zoo.Moore8TM;

public class BQruleDensityActivity {

	private static final int TTRANS = 10000;
	private static final int PMIN= 10, PMAX = 50, PSTP=1;
	
	CAsampler m_sampler;
	private DensityMD m_densMD;
	private ActivityMD m_activityMD;
	private BiasedQuiesentModelToom m_model;
	private QuorumDInit m_init;

	String m_output_filename;

	public BQruleDensityActivity(int L) {
			m_model= new BiasedQuiesentModelToom();
			IntC XYsize= new IntC(L,L);
			PlanarTopology topology= new Moore8TM();
			m_sampler= new PlanarSampler(XYsize, m_model, topology);
			m_densMD= new DensityMD();
			m_densMD.LinkTo(m_sampler);
			m_activityMD= new ActivityMD();
			m_activityMD.LinkTo(m_sampler);
			m_init=(QuorumDInit)m_sampler.GetInitializer();
	}

	private String doOneSampling(int pvalint, int kappaInt){
		Macro.fPrint("sampling p:%d (in p.c.)", pvalint);
		double pval = pvalint/100.;
		m_model.setBiasPval(pval);
		double kappa= kappaInt / 100.;
		m_init.setDfailure(kappa);
		m_sampler.sig_Init(); 
		
		int 
		seedI= m_sampler.GetInitSeed();
		//seedM= m_sampler.GetModelSeed();
		for (int t=0; t<TTRANS-1; t++){
			m_sampler.sig_NextStep();
		}
		double 
			dens= m_densMD.measureStateDensity(BiasedQuiesentModelToom.N), //neutral 
			act= m_activityMD.GetMeasure();
		String strMeasure= String.format(Locale.ENGLISH, 
				"%.3f %.3f %.3f %.3f %8d",
				kappa, pval, dens, act, seedI);
		return strMeasure;
	}

	private FLStringList doSampling(int kappa) {
		FLStringList output = new FLStringList();
		for (int pval=PMIN ; pval<=PMAX ; pval+=PSTP) {
			String res= doOneSampling(pval,kappa);
			output.Add(res);
		}
		return output;
	}
	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();


	public static void main(String [] argv){
		CommandParser parser = new CommandParser(CLASSNAME, "", argv);
		int L=parser.IParse("L=");
		int KAPPA=parser.IParse("KAPPA=");
		
		Macro.print("****** starting exp *****");
		BQruleDensityActivity exp = new BQruleDensityActivity(L);
		FLStringList out= exp.doSampling(KAPPA);
		String filename= String.format("BQ-dens-L%d-k%d.dat",L,KAPPA);
		out.WriteToFile(filename);
	}

	

}
