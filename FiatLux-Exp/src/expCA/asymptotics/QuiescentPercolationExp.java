package expCA.asymptotics;

import java.util.Locale;

import components.types.FLStringList;
import components.types.IntC;
import experiment.measuring.MeasuringDevice;
import experiment.measuring.general.DensityMD;
import experiment.samplers.CAsampler;
import experiment.samplers.PlanarSampler;
import main.Macro;
import main.MathMacro;
import main.commands.CommandParser;
import models.CAmodels.decentralisedDiagnosis.BiasedQuiescentModelGeneric;
import topology.basics.PlanarTopology;
import topology.zoo.vonNeumannTM;

public class QuiescentPercolationExp {


	final static int L=100;
	private static final IntC XYsize = new IntC(L,L);

	private static final int TTRANS = 20000, TPOST=2000;
	private static final int Pmin=10,Pmax=90,Pstp=2;
	
	CAsampler m_sampler;
	private DensityMD m_densMD;
	// VERIFY THIS ONE !!!
	private BiasedQuiescentModelGeneric m_model;

	String m_output_filename;


	public QuiescentPercolationExp() {
		m_model= new BiasedQuiescentModelGeneric();
	}

	public void BuildSampler(){
		PlanarTopology topo2D= new vonNeumannTM();
		m_sampler= new PlanarSampler(XYsize, m_model, topo2D);
		m_densMD=  new DensityMD();
		((MeasuringDevice)m_densMD).LinkTo(m_sampler);

	}

	private String doOneSampling(int pPercent){
		Macro.fPrint("sampling... p:%d", pPercent);
		m_sampler.sig_Init();
		m_model.setPval(pPercent/MathMacro.HUNDRED);
		int 
		seedI= m_sampler.GetInitSeed(),
		seedM= m_sampler.GetModelSeed();
		for (int t=0; t<TTRANS; t++){
			m_sampler.sig_NextStep();
		}
		double densA= m_densMD.GetMeasure();
		for (int t=0; t<TPOST; t++){
			m_sampler.sig_NextStep();
		}
		double densB= m_densMD.GetMeasure();
		String strMeasure= String.format(Locale.ENGLISH, 
				"%d,%.3f,%.3f,%d,%d",
				pPercent, densA, densB, seedI, seedM);
		return strMeasure;
	}

	/** sampling one given rule **/
	private FLStringList Measure() {
		BuildSampler();
		FLStringList output= new FLStringList();
		String header="P,DNT1,DNT2,seedI,seedM";
		output.Add(header);
		for (int pPercent=Pmin; pPercent<=Pmax; pPercent+=Pstp){
			String res= doOneSampling(pPercent);
			output.Add( res );
			Macro.fPrint(" sampled p:%d > %s", pPercent, res); 
		}
		return output;
	}

	/** exploration of one value **/
	private void Process(){
		Macro.print("****** starting exp *****");
		FLStringList out= Measure();
		out.Print();
		String filename= String.format("quiescentPercolation-L%d.dat",L);
		out.WriteToFile(filename);
	}


	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();


	public static void main(String [] argv){
		CommandParser parser = new CommandParser(CLASSNAME, "", argv);
		QuiescentPercolationExp exp= new QuiescentPercolationExp();
		exp.Process();
	}

}
/*for (int W : PtrWithZero){
Macro.print(W + " " + ECAlookUpTable.GetTransitionCodePadded(new RuleCode(W)) );
}*/