package expCA.asymptotics;

import architecture.latticeKinesis.LatticeKinesisSampler;
import architecture.latticeKinesis.SwarmingLGCAsys;
import architecture.latticeKinesis.measuringD.LatticeKalignmentMD;
import components.types.IntC;
import main.Macro;
import main.commands.CommandParser;

public class ExpLGCASwarming{

	private static final int TSIMU = 10000;
	// attributes
	final LatticeKalignmentMD m_md;
	LatticeKinesisSampler m_sampler;

	
	public ExpLGCASwarming(){
		m_md= new LatticeKalignmentMD();
	}
	

	public void doExpTest() {
		IntC xysize=new IntC(20,20);
		boolean reflectingB= false;
		SwarmingLGCAsys system= new SwarmingLGCAsys(xysize,reflectingB);
		if (system!=null) {
			Macro.print("non-null system");
		}
		m_sampler= new LatticeKinesisSampler(system);
		m_md.LinkTo(m_sampler);
	
		double initdens=0.1;
		double sigma=2.5;
		m_sampler.SetInitdensSigma(initdens, sigma);
		m_sampler.sig_Init();
		for (int t=0; t<TSIMU;t++) {
			m_sampler.sig_NextStep();
		}
		double df= m_md.GetMeasure();
		Macro.fPrint("alignement final:%.2f", df);
		
	}
	
	
	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();

	public static void main(String [] argv){
		CommandParser parser = new CommandParser(CLASSNAME, "", argv);
		if (argv.length == 0 ){
			Macro.print("premier test");
			ExpLGCASwarming exp = new ExpLGCASwarming();
			exp.doExpTest();
		} else {
			parser.Usage("?");
		}
	}

}
