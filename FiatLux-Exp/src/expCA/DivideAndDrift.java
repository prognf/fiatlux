package expCA;

import main.Macro;

import components.randomNumbers.FLRandomGenerator;
import components.types.IntegerList;

public class DivideAndDrift {


	private static final Integer LENINIT = 1000;
	FLRandomGenerator m_rand= FLRandomGenerator.GetGlobalRandomizer();
	IntegerList m_list= new IntegerList();
	private int m_time;

	public DivideAndDrift() {
		m_list.add(LENINIT);
	}

	public void Run(){
		Macro.print("--- start ---");
		boolean lives= IzStateAlive();
		while (lives){
			RunOneStep();

			//m_list.Print();
			//CleanList();

			m_time++;
			lives= IzStateAlive();
		}

		Macro.print("--- end --- t:" + m_time);
	}

	private void CleanList() {
		while (m_list.remove(new Integer(0))) {}
	}

	private boolean IzStateAlive() {		
		StringBuilder st= new StringBuilder("[");
		boolean izAlive= false;
		for (int val : m_list){
			st.append(val).append(",");
			izAlive= izAlive || (val > 0);
		}
		st.append("]");
		Macro.print("t:"+ m_time + " > " + st );
		return izAlive;
	}

	public void RunOneStep(){
		int sz= m_list.size();
		for (int i=0; i< sz; i++){
			int len= m_list.Get(i);
			int newlen = -99;
			if ((len == 1) || (len == 2)) {
				newlen = len + Drift();
				m_list.set(i, newlen);
			} else if (len>2) {
				int rnd= m_rand.RandomInt(len+1);
				if (rnd==0){
					newlen= len - 1;
				} else if (rnd == 1){
					newlen= len + 1;
				} else if (rnd == len){
					newlen= len + 1;
				} else { // two offspring
					newlen= rnd - 1;
					m_list.addVal(len -1 - newlen);
				}
				m_list.set(i, newlen);
			}
		}

	}


	final private int Drift() {
		return 2 * m_rand.RandomInt(2) - 1;
		//return m_rand.RandomInt(3) - 1;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		DivideAndDrift exp = new DivideAndDrift();
		exp.Run();

	}




}
