package expCA.densclassiffailureNG;

import components.types.FLStringList;
import components.types.IntC;
import experiment.measuring.general.DensityMD;
import experiment.samplers.CAsimulationSampler;
import main.Macro;
import main.commands.CommandInterpreter;
import models.CAmodels.decentralisedDiagnosis.NGMajorityFailInitializer;
import models.CAmodels.nState.IsingStab;
import topology.basics.SamplerInfoPlanar;
import topology.basics.TopologyCoder;

/**
 * @author Nicolas Gauville
 */
public class DensityClassifFaillureDynamicExp extends CommandInterpreter {
	final static String CLASSNAME = new Macro.CurrentClassGetter().getClassName();

	private static final int SIZE = 400;
	private static final int TMAX = 2000;
	private static final IntC GridSize = new IntC(SIZE,SIZE);
	private static final IntC CellPixSize = new IntC(1,0);
	CAsimulationSampler m_samplerExp;
	NGMajorityFailInitializer m_initExp;

	private IsingStab model;
	private DensityMD m_densityMD;

	private double alertDensity[] = new double[TMAX];
	private double failDensity[] = new double[TMAX];

	public DensityClassifFaillureDynamicExp() {
	}

	private void initExp() {
		model = new IsingStab();

		SamplerInfoPlanar info2D = new SamplerInfoPlanar(GridSize, CellPixSize, model, TopologyCoder.s_TM9);

		m_samplerExp = info2D.GetSimulationSampler();
		m_initExp = new NGMajorityFailInitializer();

		m_samplerExp.SetInitDeviceFromOutside(m_initExp);

		m_densityMD = new DensityMD();
		m_densityMD.LinkTo(m_samplerExp);
	}
	
	private void runLinExp(){
		initExp();
		runExp(0.0001);
		runExp(0.001);
		runExp(0.01);
	}

	private void runExp(double pfail) {
		((NGMajorityFailInitializer) m_samplerExp.GetInitializer()).setProportions(1, 0);
		((IsingStab) m_samplerExp.GetCellularModel()).setFailureProbability(pfail);

		m_samplerExp.sig_Init();
		for (int t = 0; t < TMAX; t++) {
			m_samplerExp.sig_NextStep();

			alertDensity[t] = m_densityMD.CountStateDensityWithoutFailure(IsingStab.ALERT, IsingStab.FAIL);
			failDensity[t] = m_densityMD.measureStateDensity(IsingStab.FAIL);
		}

		FLStringList list = new FLStringList();
		list.add("T D A");

		for (int t = 0; t < TMAX; t++) {
			list.add(t + " " + failDensity[t] + " " + alertDensity[t]);
		}

		list.WriteToFile("dens-" + pfail + ".dat");
	}
	
	public static void main(String[] argv) {
		LaunchMessage(CLASSNAME);

		try{
			DensityClassifFaillureDynamicExp exp = new DensityClassifFaillureDynamicExp();
			exp.runLinExp();
		} catch(Exception e){
			Macro.print(" Exception caught at the top: ");
			e.printStackTrace();
		}	
		CommandInterpreter.ByeMessage();
	}
}
