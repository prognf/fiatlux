package expCA.densclassiffailureNG;

import components.types.FLStringList;
import components.types.IntC;
import experiment.measuring.general.DensityMD;
import experiment.samplers.CAsimulationSampler;
import grafix.gfxTypes.imgFormat.ImgFormat;
import main.Macro;
import main.commands.CommandInterpreter;
import models.CAmodels.decentralisedDiagnosis.NGMajorityFailInitializer;
import models.CAmodels.nState.IsingStab;
import topology.basics.SamplerInfoPlanar;
import topology.basics.TopologyCoder;
import updatingScheme.AlphaScheme;

/**
 * @author Nicolas Gauville
 */
public class DensityClassifFaillureRotatingExp extends CommandInterpreter {
	final static String CLASSNAME = new Macro.CurrentClassGetter().getClassName();

	private static final IntC GridSize = new IntC(100,100);
	private static final IntC CellPixSize = new IntC(1,0);
	private static final int NB_ITERATIONS = 2000;
	private static final int NB_EXPERIMENTS = 5;
	private static final double ALPHA = 1;

	CAsimulationSampler m_samplerExp;
	NGMajorityFailInitializer m_initExp;

	private IsingStab model;
	private DensityMD m_densityMD;

	private double density[][] = new double[NB_EXPERIMENTS][NB_ITERATIONS];

	public DensityClassifFaillureRotatingExp() {
	}

	private void initExp() {
		model = new IsingStab();

		SamplerInfoPlanar info2D = new SamplerInfoPlanar(GridSize, CellPixSize, model, TopologyCoder.s_TM9);

		m_samplerExp = info2D.GetSimulationSampler();
		m_samplerExp.io_SetRecordFormat(ImgFormat.SVG);
		m_initExp = new NGMajorityFailInitializer(0.1, 1);

		m_samplerExp.SetInitDeviceFromOutside(m_initExp);

		m_densityMD = new DensityMD();
		m_densityMD.LinkTo(m_samplerExp);
	}
	
	private void runLinExp(){
		initExp();

		//runExp(1, 0.4);
		for (double j = 0; j <= 0.4; j += 0.04) {
			runExp(0, j);
		}
	}

	private void runExp(double prctAlive, double prctFail) {
		((NGMajorityFailInitializer) m_samplerExp.GetInitializer()).setProportions(prctAlive, prctFail);
		AlphaScheme updatingScheme = (AlphaScheme) m_samplerExp.GetUpdatingScheme();
		updatingScheme.SetSynchronyRate(ALPHA);

		FLStringList list = new FLStringList();
		list.add("T E");

		for (int am = 0; am < NB_EXPERIMENTS; am++) {
			m_samplerExp.sig_Init();

			for (int t = 0; t < NB_ITERATIONS; t++) {
				density[am][t] = m_densityMD.CountStateDensityWithoutFailure(IsingStab.ALERT, IsingStab.FAIL);
				m_samplerExp.sig_NextStep();
			}
		}

		for (int i = 0; i < NB_ITERATIONS; i++) {
			StringBuilder line = new StringBuilder();
			double avg = 0.0;

			for (int j = 0; j < NB_EXPERIMENTS; j++) {
				avg += density[j][i];
			}

			avg /= (double) NB_EXPERIMENTS;

			line.append(i).append(" ").append(avg);
			list.add(line.toString());
		}

		list.WriteToFile("DCF-s10-" + (int) (100 * prctAlive) + "-" + (int) (100 * prctFail) + ".dat");
	}
	
	public static void main(String[] argv) {
		LaunchMessage(CLASSNAME);

		try{
			DensityClassifFaillureRotatingExp exp = new DensityClassifFaillureRotatingExp();
			exp.runLinExp();
		} catch(Exception e){
			Macro.print(" Exception caught at the top: ");
			e.printStackTrace();
		}	
		CommandInterpreter.ByeMessage();
	}
}
