package expCA.densclassiffailureNG;

import components.types.FLStringList;
import components.types.IntC;
import experiment.measuring.general.DensityMD;
import experiment.samplers.CAsimulationSampler;
import main.Macro;
import main.commands.CommandInterpreter;
import models.CAmodels.decentralisedDiagnosis.NGMajorityFailInitializer;
import models.CAmodels.nState.IsingStab;
import topology.basics.SamplerInfoPlanar;
import topology.basics.TopologyCoder;

/**
 * @author Nicolas Gauville
 * Mesure de t1 (temps a partir duquel dD(xt) > rho) et t2 (temps a partir duquel la config est en alerte)
 */
public class DensityClassifFaillureDynamic2Exp extends CommandInterpreter {
	final static String CLASSNAME = new Macro.CurrentClassGetter().getClassName();

	private static final int SIZE = 200;
	private static final int TMAX = 50000;
	private static final int NB_EXP = 20;
	private static final int NB_PDEF = 100;
	private static final double RHO = 0.22;
	private static final IntC GridSize = new IntC(SIZE,SIZE);
	private static final IntC CellPixSize = new IntC(1,0);
	CAsimulationSampler m_samplerExp;
	NGMajorityFailInitializer m_initExp;

	private IsingStab model;
	private DensityMD m_densityMD;

	private double pdef[][] = new double[NB_PDEF][NB_EXP];
	private double t1[][] = new double[NB_PDEF][NB_EXP];
	private double t2[][] = new double[NB_PDEF][NB_EXP];
	private double t3[][] = new double[NB_PDEF][NB_EXP];
	private double d[][] = new double[NB_PDEF][NB_EXP];

	public DensityClassifFaillureDynamic2Exp() {
	}

	private void initExp() {
		model = new IsingStab();

		SamplerInfoPlanar info2D = new SamplerInfoPlanar(GridSize, CellPixSize, model, TopologyCoder.s_TM9);

		m_samplerExp = info2D.GetSimulationSampler();
		m_initExp = new NGMajorityFailInitializer();

		m_samplerExp.SetInitDeviceFromOutside(m_initExp);

		m_densityMD = new DensityMD();
		m_densityMD.LinkTo(m_samplerExp);

		for (int i = 0; i < NB_PDEF; i++) {
			for (int j = 0; j < NB_EXP; j++) {
				pdef[i][j] = 0;
				t1[i][j] = 0;
				t2[i][j] = 0;
				t3[i][j] = 0;
				d[i][j] = 0;
			}
		}
	}

	private double getPDef(int i) {
		return 0.00001 + 0.1 * ((double) i / (double) NB_PDEF);
	}
	
	private void runLinExp(){
		initExp();

		for (int i = 0; i < NB_PDEF; i++) {
			runExp(i, getPDef(i));
		}

		saveExp();
	}

	private void runExp(int nb, double pfail) {
		for (int ii = 0; ii < NB_EXP; ii++) {
			pdef[nb][ii] = pfail;
			((NGMajorityFailInitializer) m_samplerExp.GetInitializer()).setProportions(1, 0);
			((IsingStab) m_samplerExp.GetCellularModel()).setFailureProbability(pfail);

			m_samplerExp.sig_Init();
			for (int t = 0; t < TMAX; t++) {
				m_samplerExp.sig_NextStep();

				if (m_densityMD.measureStateDensity(IsingStab.FAIL) > RHO && t1[nb][ii] == 0) {
					t1[nb][ii] = t;
				} else if (m_densityMD.CountStateDensityWithoutFailure(IsingStab.ALERT, IsingStab.FAIL) == 1 && t2[nb][ii] == 0) {
					t2[nb][ii] = t;
					t3[nb][ii] = t2[nb][ii] - t1[nb][ii];
					break;
				}
			}
		}
	}

	private void saveExp() {
		FLStringList list = new FLStringList();
		list.add("DF T1 T2 D");

		for (int t = 0; t < NB_PDEF; t++) {
			double st1 = 0;
			double st2 = 0;
			double st3 = 0;
			double spf = 0;

			for (int i = 0; i < NB_EXP; i++) {
				st1 += t1[t][i];
				st2 += t2[t][i];
				st3 += Math.max(0, t3[t][i]);
				spf += pdef[t][i];
			}

			st1 /= (double) NB_EXP;
			st2 /= (double) NB_EXP;
			st3 /= (double) NB_EXP;
			spf /= (double) NB_EXP;

			list.add(spf + " " + st1 + " " + st2 + " " + st3);
		}

		list.WriteToFile("t1t2-f.dat");
	}
	
	public static void main(String[] argv) {
		LaunchMessage(CLASSNAME);

		try{
			DensityClassifFaillureDynamic2Exp exp = new DensityClassifFaillureDynamic2Exp();
			exp.runLinExp();
		} catch(Exception e){
			Macro.print(" Exception caught at the top: ");
			e.printStackTrace();
		}	
		CommandInterpreter.ByeMessage();
	}
}
