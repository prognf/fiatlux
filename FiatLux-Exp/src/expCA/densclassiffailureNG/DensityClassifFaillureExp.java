package expCA.densclassiffailureNG;

import components.types.FLStringList;
import components.types.IntC;
import experiment.measuring.general.DensityMD;
import experiment.samplers.CAsimulationSampler;
import grafix.gfxTypes.imgFormat.ImgFormat;
import main.Macro;
import main.commands.CommandInterpreter;
import models.CAmodels.decentralisedDiagnosis.NGMajorityFailInitializer;
import models.CAmodels.decentralisedDiagnosis.NGMajorityFailModel;
import topology.basics.SamplerInfoPlanar;
import topology.basics.TopologyCoder;

/**
 * @author Nicolas Gauville
 */
public class DensityClassifFaillureExp extends CommandInterpreter {
	final static String CLASSNAME = new Macro.CurrentClassGetter().getClassName();

	private static final IntC GridSize = new IntC(200,200);
	private static final IntC CellPixSize = new IntC(1,0);
	private static final int NB_ITERATIONS = 200;
	private static final int NB_EXPERIMENTS = 5;

	CAsimulationSampler m_samplerExp;
	NGMajorityFailInitializer m_initExp;

	private NGMajorityFailModel model;
	private DensityMD m_densityMD;

	private double density[][] = new double[NB_EXPERIMENTS][NB_ITERATIONS];

	public DensityClassifFaillureExp() {
	}

	private void initExp() {
		model = new NGMajorityFailModel();

		SamplerInfoPlanar info2D = new SamplerInfoPlanar(GridSize, CellPixSize, model, TopologyCoder.s_TNCE);

		m_samplerExp = info2D.GetSimulationSampler();
		m_samplerExp.io_SetRecordFormat(ImgFormat.SVG);
		m_initExp = new NGMajorityFailInitializer();

		m_samplerExp.SetInitDeviceFromOutside(m_initExp);

		m_densityMD = new DensityMD();
		m_densityMD.LinkTo(m_samplerExp);
	}
	
	private void runLinExp(){
		initExp();

		for (double i = 0.2; i <= 0.6; i += 0.1) {
			for (double j = 0; j <= 0.2; ) {
				runExp(i, j);
			}
		}
	}

	private void runExp(double prctAlive, double prctFail) {
		((NGMajorityFailInitializer) m_samplerExp.GetInitializer()).setProportions(prctAlive, prctFail);

		FLStringList list = new FLStringList();
		StringBuilder sb = new StringBuilder("T");

		for (int i = 0; i < NB_EXPERIMENTS; i++) {
			sb.append(" E").append(i);
		}

		list.add(sb.toString());

		for (int am = 0; am < NB_EXPERIMENTS; am++) {
			m_samplerExp.sig_Init();

			for (int t = 0; t < NB_ITERATIONS; t++) {
				density[am][t] = m_densityMD.CountStateDensityWithoutFailure(NGMajorityFailModel.ONE, NGMajorityFailModel.FAIL);
				m_samplerExp.sig_NextStep();
			}
		}

		for (int i = 0; i < NB_ITERATIONS; i++) {
			StringBuilder line = new StringBuilder();
			line.append(i);

			for (int j = 0; j < NB_EXPERIMENTS; j++) {
				line.append(" ").append(density[j][i]);
			}

			list.add(line.toString());
		}

		list.WriteToFile("DCF-eq-" + prctAlive + "-" + prctFail + ".dat");
	}
	
	public static void main(String[] argv) {
		LaunchMessage(CLASSNAME);

		try{
			DensityClassifFaillureExp exp = new DensityClassifFaillureExp();
			exp.runLinExp();
		} catch(Exception e){
			Macro.print(" Exception caught at the top: ");
			e.printStackTrace();
		}	
		CommandInterpreter.ByeMessage();
	}
}
