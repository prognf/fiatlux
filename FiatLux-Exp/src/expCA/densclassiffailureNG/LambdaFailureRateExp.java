package expCA.densclassiffailureNG;

import components.types.FLStringList;
import components.types.IntC;
import experiment.measuring.general.DensityMD;
import experiment.samplers.CAsimulationSampler;
import main.Macro;
import main.commands.CommandInterpreter;
import models.CAmodels.decentralisedDiagnosis.NGMajorityFailInitializer;
import models.CAmodels.nState.IsingStab;
import topology.basics.SamplerInfoPlanar;
import topology.basics.TopologyCoder;

/**
 * @author Nicolas Gauville
 */
public class LambdaFailureRateExp extends CommandInterpreter {
	final static String CLASSNAME = new Macro.CurrentClassGetter().getClassName();

	private static final int SIZE = 100;
	private static final int TMAX = 3000;

	private static final IntC GridSize = new IntC(SIZE,SIZE);
	private static final IntC CellPixSize = new IntC(1,0);
	private static final int NB_LAMBDA = 20;
	private static final int MAX_LAMBDA = 20;

	private static final double DEF_MAX = 0.6;
	private static final int NB_DEF_RATIO = 20;

	CAsimulationSampler m_samplerExp;
	NGMajorityFailInitializer m_initExp;

	private IsingStab model;
	private DensityMD m_densityMD;

	private double firstFailureRate[] = new double[NB_LAMBDA];

	public LambdaFailureRateExp() {
	}

	private void initExp() {
		model = new IsingStab();

		SamplerInfoPlanar info2D = new SamplerInfoPlanar(GridSize, CellPixSize, model, TopologyCoder.s_TM9);

		m_samplerExp = info2D.GetSimulationSampler();
		m_initExp = new NGMajorityFailInitializer();

		m_samplerExp.SetInitDeviceFromOutside(m_initExp);

		m_densityMD = new DensityMD();
		m_densityMD.LinkTo(m_samplerExp);

		for (int i = 0; i < NB_LAMBDA; i++) {
			firstFailureRate[i] = 0;
		}
	}

	private double getLambda(int i) {
		return ((i + 1.0) / (double) NB_LAMBDA) * MAX_LAMBDA;
	}
	private double getDefRatio(int i) {
		return ((i+1.0) / (double) NB_DEF_RATIO) * DEF_MAX;
	}

	private void runLinExp(){
		initExp();
		runExp();
	}

	private void runExp() {
		for (int l = 0; l < NB_LAMBDA; l++) {
			double lambda = getLambda(l);
			IsingStab.LAMBDA = lambda;

			for (int df = 0; df < NB_DEF_RATIO; df++) {
				double defRatio = getDefRatio(df);

				System.out.println("lambda = " + lambda + "/" + MAX_LAMBDA + ", fail = " + defRatio + "/" + DEF_MAX);
				((NGMajorityFailInitializer) m_samplerExp.GetInitializer()).setProportions(1, defRatio);

				m_samplerExp.sig_Init();
				for (int t = 0; t < TMAX; t++) {
					m_samplerExp.sig_NextStep();
				}

				if (m_densityMD.CountStateDensityWithoutFailure(IsingStab.ALERT, IsingStab.FAIL) == 1) {
					firstFailureRate[l] = defRatio;
					System.out.println("lambda = " + lambda + "/" + MAX_LAMBDA + ", fail = " + df + "/" + DEF_MAX + ", res = " + defRatio);
					break;
				}
			}
		}

		FLStringList list = new FLStringList();
		list.add("LAMBDA FDR");

		for (int i = 0; i < NB_LAMBDA; i++) {
			list.add(getLambda(i) + " " + firstFailureRate[i]);
		}

		list.WriteToFile("lambda=f(D).dat");
	}
	
	public static void main(String[] argv) {
		LaunchMessage(CLASSNAME);

		try{
			LambdaFailureRateExp exp = new LambdaFailureRateExp();
			exp.runLinExp();
		} catch(Exception e){
			Macro.print(" Exception caught at the top: ");
			e.printStackTrace();
		}	
		CommandInterpreter.ByeMessage();
	}
}
