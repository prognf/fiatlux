package expCA.densclassiffailureNG;

import experiment.measuring.general.DensityMD;
import main.Macro;
import main.commands.CommandInterpreter;
import components.types.FLStringList;
import components.types.IntC;
import experiment.samplers.CAsimulationSampler;
import models.CAmodels.decentralisedDiagnosis.NGMajorityFailInitializer;
import models.CAmodels.nState.IsingStab;
import topology.basics.SamplerInfoPlanar;
import topology.basics.TopologyCoder;
import updatingScheme.AlphaScheme;

/**
 * @author Nicolas Gauville
 */
public class DensityClassifFaillureRotatingDiagExp extends CommandInterpreter {
	final static String CLASSNAME = new Macro.CurrentClassGetter().getClassName();

	private static final int SIZE = 100;
	private static final int TMAX = 2000;
	private static final double ALPHA = 1;
	private static final IntC GridSize = new IntC(SIZE,SIZE);
	private static final IntC CellPixSize = new IntC(1,0);
	private static final int NB_F_DENSITIES = 15;
	CAsimulationSampler m_samplerExp;
	NGMajorityFailInitializer m_initExp;

	private IsingStab model;
	private DensityMD m_densityMD;

	private double density[] = new double[NB_F_DENSITIES];

	public DensityClassifFaillureRotatingDiagExp() {
	}

	private void initExp() {
		model = new IsingStab();

		SamplerInfoPlanar info2D = new SamplerInfoPlanar(GridSize, CellPixSize, model, TopologyCoder.s_TM9);

		m_samplerExp = info2D.GetSimulationSampler();
		m_initExp = new NGMajorityFailInitializer();

		m_samplerExp.SetInitDeviceFromOutside(m_initExp);

		m_densityMD = new DensityMD();
		m_densityMD.LinkTo(m_samplerExp);

		for (int i = 0; i < NB_F_DENSITIES; i++) {
			density[i] = 0;
		}
	}

	private double getDefRatio(int i) {
		return ((double) i / (double) NB_F_DENSITIES) * 0.4;
	}
	
	private void runLinExp(){
		initExp();

		for (int i = 0; i < 100; i++) {
			runExp(i);
		}
	}

	private void runExp(int nb) {
		for (int df = 0; df < NB_F_DENSITIES; df++) {
			double defRatio = getDefRatio(df);

			System.out.println(nb + "/100, " + df + "/" + NB_F_DENSITIES);

			((NGMajorityFailInitializer) m_samplerExp.GetInitializer()).setProportions(1, defRatio);

			AlphaScheme updatingScheme = (AlphaScheme) m_samplerExp.GetUpdatingScheme();
			updatingScheme.SetSynchronyRate(ALPHA);

			m_samplerExp.sig_Init();

			for (int t = 0; t < TMAX; t++) {
				m_samplerExp.sig_NextStep();
			}

			// > 0.95 --> condition moins restrictive !!
			density[df] += (m_densityMD.CountStateDensityWithoutFailure(IsingStab.ALERT, IsingStab.FAIL) > 0.99 ? 1 : 0);
		}

		FLStringList list = new FLStringList();
		list.add("DF A");

		for (int i = 0; i < NB_F_DENSITIES; i++) {
			list.add(getDefRatio(i) + " " + (density[i] / (double) (nb + 1)));
		}

		list.WriteToFile("rex-t" + TMAX + "-x" + SIZE + "-a" + ((int) (ALPHA * 10)) + "-"+ (nb + 1) + ".dat");
	}
	
	public static void main(String[] argv) {
		LaunchMessage(CLASSNAME);

		try{
			DensityClassifFaillureRotatingDiagExp exp = new DensityClassifFaillureRotatingDiagExp();
			exp.runLinExp();
		} catch(Exception e){
			Macro.print(" Exception caught at the top: ");
			e.printStackTrace();
		}	
		CommandInterpreter.ByeMessage();
	}
}
