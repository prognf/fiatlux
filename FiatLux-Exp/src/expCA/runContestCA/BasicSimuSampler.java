package expCA.runContestCA;


import main.Macro;
import updatingScheme.UpdatingScheme;
import experiment.samplers.CAsampler;
import grafix.gfxTypes.elements.FLPanel;

/*--------------------
 * used for simulation experiments 
 * it creates the visual context 
 * 
 * @author Nazim Fates
*--------------------*/

abstract public class BasicSimuSampler 
	extends CAsampler 
{
	
	/*--------------------
	 * attributes
	 --------------------*/

	FLPanel m_samplerpanel;
	
	/*--------------------
	 * constructor
	 --------------------*/

	public BasicSimuSampler(){
		super();
	}
	
	
	/*
	 * returns a panel with all the subpanels concerning the Sampler i.e.,
	 * Automaton + Cellular + Initializer + Topo + Viewer
	 */
	public FLPanel GetSimulationPanel() {
		FLPanel SamplerPanel = new FLPanel();
		SamplerPanel.SetBoxLayoutY();

		// model panel
		FLPanel modelpanel = GetCellularModel().GetSpecificPanel();
		if (modelpanel != null)
			SamplerPanel.add(modelpanel);

		// system panel (update mode)

		/** View/Control * */
		FLPanel automatonpanel=null;
		try{ 
			UpdatingScheme scheme = GetUpdatingScheme();
			automatonpanel = scheme.GetSpecificPanel();
		} catch (NullPointerException e){
			Macro.FatalError(e,"Updating scheme might not have been initialized - check Sampler construction");
		}

		if (automatonpanel != null)
			SamplerPanel.add(automatonpanel);

		// init panel
		//FLPanel myInitPanel = new InitPanel(this);
		//SamplerPanel.Add(myInitPanel);

		//	viewer panel
		FLPanel viewerpanel = m_viewer.GetSpecificPanel();
		if (viewerpanel != null) {
			SamplerPanel.add(viewerpanel);
		}

		SamplerPanel.add(m_viewer);
			
		
		m_samplerpanel= SamplerPanel;
		return SamplerPanel;
	}

		
}
