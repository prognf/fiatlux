package expCA.runContestCA;

import grafix.gfxTypes.elements.FLPanel;
import grafix.viewers.AutomatonViewer;
import grafix.viewers.LineAutomatonViewer;

import javax.swing.JComponent;

import models.CAmodels.CellularModel;
import topology.basics.LinearTopology;

import components.types.IntC;

/*
 * implements the Drop function 
 */
public class ECAbasicSampler extends BasicSimuSampler {

	// time buffer
	int m_Tsize;
	LineAutomatonViewer m_av;
	/*--------------------
	 * Constructor
	 --------------------*/
	
	/* we assume the size of the TopoManager is NOT already set */
	public ECAbasicSampler(int Lsize, int Tsize, 
			IntC CellPixSize,
			CellularModel in_CellularModel, LinearTopology in_TopologyManager){
		
		m_Tsize= Tsize;
		// topo
		in_TopologyManager.SetSize(Lsize);
		super.CreateNewSampler(in_CellularModel, Lsize, in_TopologyManager);
		
		
		// automaton Viewer management
		// cast here
		m_av= GetCellularModel().GetLineAutomatonViewer(
					CellPixSize, (LinearTopology)m_topologyManager, GetAutomaton(),Tsize);
		this.SetAutomatonViewer(m_av);
	}
	
	/*--------------------
	 * signals
	 --------------------*/
	
	/* makes exactly Tsize - 1 steps forward 
	 * it "drops" at the botttom of space-time diagrams 
	 */
	public void sig_Drop() {
		for (int i = 0; i < GetTsize() - 1; i++) {
			sig_NextStep();
		}
	}
	
	
	
	

	/*--------------------
	 * get / set
	 --------------------*/
	
	public int GetTsize(){
		return m_Tsize;
	}

	@Override
	public String GetSimulationInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String GetTopologyInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FLPanel GetSimulationWidgets() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JComponent GetSimulationView() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AutomatonViewer GetViewer() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] GetPlotterSelection() {
		// TODO Auto-generated method stub
		return null;
	}

	

			
	
}
