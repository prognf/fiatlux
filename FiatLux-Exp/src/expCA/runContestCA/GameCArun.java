package expCA.runContestCA;

import main.Macro;
import main.commands.CommandInterpreter;

import components.types.IntC;

public class GameCArun extends CommandInterpreter {


	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();
	final static String USAGE=" hi ha";

	public static final int Lsize= 20, Tsize = 50;
	public static final IntC CellPixSize = new IntC(10,1);

	
	/** main : processing command line * */
	public static void main(String[] argv) {
		LaunchMessage(CLASSNAME, USAGE);
		try{
			Run();
			do {
				//Macro.print("coucou");
			} while (true);
		}
		catch(Exception e){
			Macro.print(" Exception caught at the top: ");
			e.printStackTrace();
		}	
		CommandInterpreter.ByeMessage();
	}

	private static void Run() {
		ContestWindow w = new ContestWindow();
		w.packAndShow();
	}


	

}
