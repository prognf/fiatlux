package expCA.runContestCA;

import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.elements.FLFrame;
import grafix.gfxTypes.elements.FLPanel;
import grafix.windows.ChronosWindow;
import initializers.CAinit.BinaryInitializer;
import initializers.CAinit.NpointsInitializer;
import main.Macro;


public class ContestWindow extends ChronosWindow {

	public static final int HITTARGET = 100;

	
	BasicSimWindow m_simuA, m_simuB, m_simuC; 
	
	public ContestWindow(){
		super("example for ChronosWindow");
		FLPanel houhou= new FLPanel();	
		houhou.SetBoxLayoutY();
		houhou.add( super.GetDynamicsPanel() );
		houhou.add( super.GetControlPanel() );

		addPanel(houhou);
		pack();

		m_simuA= new	BasicSimWindow("Init config A");
		m_simuB= new	BasicSimWindow("Init config B");
		m_simuC= new	BasicSimWindow("Init config C");
		
		SetInitA(m_simuA);
		SetInitB(m_simuB);
		SetInitC(m_simuC);
		
		houhou.add( m_simuA.m_hitCountBar );
		m_simuA.m_hitCountBar.setBackground(FLColor.c_green);
		m_simuA.m_hitCountBar.setForeground(FLColor.c_darkgreen);
		houhou.add( m_simuB.m_hitCountBar );
		m_simuB.m_hitCountBar.setBackground(FLColor.c_red);
		m_simuB.m_hitCountBar.setForeground(FLColor.c_darkred);
		houhou.add( m_simuC.m_hitCountBar );
		
		FLFrame viewerFrame= new FLFrame("dpace-time diagrams"); 
		FLPanel viewers= new FLPanel();
		viewers.Add( m_simuA.m_sampler.GetAutomatonViewer() );
		viewers.Add( m_simuB.m_sampler.GetAutomatonViewer() );
		viewers.Add( m_simuC.m_sampler.GetAutomatonViewer() );
		
		viewerFrame.addPanel( viewers);
		viewerFrame.packAndShow();
		
		FLFrame controlsFrame= new FLFrame("controls"); 
		controlsFrame.addPanel(m_simuA);
		controlsFrame.addPanel(m_simuB);
		controlsFrame.addPanel(m_simuC);
		controlsFrame.packAndShow();
		
	}	

	private void SetInitA(BasicSimWindow simu) {
		BinaryInitializer init= new BinaryInitializer();
		init.SetInitCode("R5");
		simu.m_sampler.SetInitDeviceFromOutside(init);
	}
	
	private void SetInitB(BasicSimWindow simu) {
		NpointsInitializer init= new NpointsInitializer();
		init.SetNPoints(5);
		simu.m_sampler.SetInitDeviceFromOutside(init);
	}

	private void SetInitC(BasicSimWindow simu) {
		BinaryInitializer init= new BinaryInitializer();
		init.SetInitCode("R6");
		simu.m_sampler.SetInitDeviceFromOutside(init);
	}
	
	public void sig_Init() {
		m_simuA.sig_Init();
		m_simuB.sig_Init();
		m_simuC.sig_Init();
	}
	
	public void sig_NextStep() {
		
		boolean go= (m_simuA.GetHit()<HITTARGET) && 
				(m_simuB.GetHit()<HITTARGET) && 
				(m_simuC.GetHit()<HITTARGET);
		if (go){
			m_simuA.sig_NextStep();
			m_simuB.sig_NextStep();
			m_simuC.sig_NextStep();
		}
	}

	public void sig_UpdateView() {
		//Macro.print("coucou");
	}

	/* saving  */
	public void sig_Save(){
		Macro.print("saving not implemented");
	}

	protected void sig_Close() {
		CloseWindow();
		Macro.print("bye");
		Macro.systemExit();
	}

	



}
