package expCA.runContestCA;

import javax.swing.JProgressBar;

import experiment.measuring.general.UniformStateMDgen;
import grafix.gfxTypes.elements.FLLabel;
import grafix.gfxTypes.elements.FLPanel;
import grafix.gfxTypes.elements.FLTextArea;
import main.tables.PlotterSelectControl;
import models.CAmodels.CellularModel;
import models.CAmodels.tabled.ECAmodel;
import topology.basics.LinearTopology;
import topology.zoo.RadiusN_Topology;
import updatingScheme.AlphaScheme;

/******************
 * 
 * class constructs a simulation window 
 * Left: Divided into regions Actions, Simulations, Info, Plots
 * Right: Viewer for current configuration 
 */

public class BasicSimWindow extends FLPanel {

	private static final String TIP_SIMUTIME = "How many generations have elapsed...";
	private static final int TEXTAREASIZE = 5;

	// hitbar !!
	JProgressBar m_hitCountBar= new JProgressBar(0, ContestWindow.HITTARGET);
	int m_count= 0;
	boolean m_reset = false; // do we do a "rest" operation ???
	UniformStateMDgen m_md;
	
	
	// main linked object
	public ECAbasicSampler m_sampler;
	// display info
	FLTextArea m_TopologyInfo, m_SimulationState;
	PlotterSelectControl m_DataPlotterSelector = null; // object
	FLPanel m_PlotterSelectionPanel= new FLPanel(); // panel for selecting the plotter

	
	public BasicSimWindow(String label) {
		Add( new FLLabel(label));
		CellularModel myModel = ECAmodel.NewRule(170);
		LinearTopology topo = new RadiusN_Topology(1);
		m_sampler = 
				new ECAbasicSampler(GameCArun.Lsize, GameCArun.Tsize, 
						GameCArun.CellPixSize, myModel, topo);
	
		AlphaScheme update= (AlphaScheme)m_sampler.GetUpdatingScheme();
		update.SetSynchronyRate(0.5);

		
		m_TopologyInfo = new FLTextArea(TEXTAREASIZE);
		m_SimulationState = new FLTextArea(TEXTAREASIZE);
		m_SimulationState.SetHelp(TIP_SIMUTIME);
		m_SimulationState.SetFontMid();
		this.Add(GetSimulationPanel());
		
		m_hitCountBar.setStringPainted(true);
		
		m_md = new UniformStateMDgen();
		m_md.LinkTo(m_sampler);

		
	}
	
	/** Creates all that appears in an experiment 
	 * can be called externally (e.g. for applets) **/
	final public FLPanel GetSimulationPanel() {

	
		// RIGHT - SAMPLER
		FLPanel gridAndInit = new FLPanel() ;
		gridAndInit.add( m_sampler.GetSimulationPanel() );
		//gridAndInit.setBackground(COL_SAMPLER);

		// main arrangement of panels
		FLPanel MainPanel = new FLPanel();
		MainPanel.SetBoxLayoutX();

		MainPanel.Add(gridAndInit);		
		return MainPanel;
	}

	/*-----------------------------------------------------------------------
	 * OBJECTS, BUTTONS, FIELDS
	 *---------------------------------------------------------------------*/


	
	public void Hit(){
		m_count++;
		m_hitCountBar.setValue(m_count);
	//	String msg="" + count + " over " + HITTARGET;
		//m_hitCountBar.setString(msg);
		repaint();
	}

	public void ResetHit() {
		m_count=0;
		m_hitCountBar.setValue(0);
	}

	public int GetHit() {
		return m_count;
	}
	
	public void sig_Init(){
		m_sampler.sig_Init();
		ResetHit();
	}

	public void sig_NextStep() {
		if (!m_reset){
			m_sampler.sig_NextStep();
		} else {
			m_sampler.sig_Init();
		}
		int uniform= m_md.IsUniformState();
		m_reset = (uniform !=-1);
		if /*(uniform==0)*/ (m_reset){
			Hit();
		}
	}
	




}
