package densClassif;

import java.math.BigDecimal;
import java.util.ArrayList;

import components.types.FLStringList;
import components.types.IntC;
import experiment.measuring.general.DensityMD;
import experiment.measuring.general.UniformStateMDgen;
import experiment.samplers.CAsampler;
import experiment.samplers.PlanarSampler;
import initializers.CAinit.BinaryInitializer;
import main.Macro;
import main.StatsMacro;
import main.commands.CommandParser;
import models.CAmodels.binary.BinaryModel;
import models.CAmodels.binary.ConsensusModel;
import topology.basics.PlanarTopology;
import topology.zoo.Moore8TM;
import updatingScheme.AlphaScheme;
/*-------------------- 
 * experimentation for the density classification problem
 * @author Nazim Fatès
*--------------------*/
public class ConsensusConvergenceExp {

	final static int MAXTIME=100000;
	private static final int NSTATES = 3; // for stats
	/*--------------------
	 * attributes
	 --------------------*/

	CAsampler m_sampler;
	private UniformStateMDgen m_uniformMD;
	private DensityMD m_densMD;
	String m_output_filename;
	BinaryInitializer m_init;
	private BinaryModel m_model;

	/*--------------------
	 * construction
	 --------------------*/

	/* constructor */
	public ConsensusConvergenceExp(String modelName, String in_filename, int L) {

		m_model= new ConsensusModel();
		// output
		m_output_filename= in_filename;

		// topo 
		
		PlanarTopology topology = new Moore8TM();
		// sampler
		IntC XY = new IntC(L, L);
		m_sampler= new PlanarSampler(XY, m_model, topology);
		// measures		
		m_uniformMD= new UniformStateMDgen();
		m_uniformMD.LinkTo(m_sampler);
		m_densMD= new DensityMD();
		m_densMD.LinkTo(m_sampler);
		// init
		m_init = new BinaryInitializer();
		double rdensity= 0.5;
		m_init.SetInitRate(rdensity);

		m_sampler.SetInitDeviceFromOutside(m_init);

	}



	/*--------------------
	 * main
	 --------------------*/
	private void SetAlpha(double alpha){
		AlphaScheme scheme= (AlphaScheme) m_sampler.GetUpdatingScheme();
		scheme.SetSynchronyRate(alpha);
	}


	public void RunSamples(int Z, ArrayList<BigDecimal> alphaList){
		FLStringList output= new FLStringList();
		for (BigDecimal alpha: alphaList){
			String line = MakeZsamplesForAlphaVal(alpha, Z);
			output.Add(line);
		}
		output.WriteToFile(m_output_filename);
	}

	/** one experiment for various alpha values **/
	public String MakeZsamplesForAlphaVal(BigDecimal alpha, int Z){

		double alphaVal= alpha.doubleValue();
		SetAlpha(alphaVal);

		int goodclassif =0;
		int  [] freq012 = new int [NSTATES];

		int [] convTime = new int [Z];
		for (int sample=0; sample < Z; sample++){			
			Macro.ProgressBar(sample,Z); // 
			m_sampler.sig_Init();
			double initdens = m_densMD.GetMeasure();
			//Macro.print(" Init dens: " + initdens);
			if (initdens==0.5){
				Macro.FatalError("Equal initial repartition - set L to odd value");
			}
			int initmajstate=(initdens < 0.5) ?  0 : 1;
			//Macro.print(" sample : " + (sample+1) + "  idens: " + initdens);

			int uniform = m_uniformMD.IsUniformState(); // -1 means not uniform
			int time=0;
			while ((uniform < 0 ) && (time < MAXTIME)){
				m_sampler.sig_NextStep();
				time++;
				double dens= m_densMD.GetMeasure();
				uniform = m_uniformMD.IsUniformState();
				//Macro.print("uniform " + uniform + " dens :" + dens);
			}
			// counting good classifications
			if (uniform == initmajstate){
				goodclassif++;
			}
			// stats : 0, 1, 2 = out of time
			int statindex = (uniform<0) ? 2 : uniform; 
			freq012[statindex]++;
			convTime[sample]= time;
			//Macro.print("Converged to " + statindex + " in time : " + time);
		}//for

		// printing output
		// result.Print();
		Macro.print("Results : ");
		for (int i=0; i < NSTATES; i++){
			Macro.fPrintNOCR("[ "  + i + " : " +freq012[i]+ " ] ");
		}
		Macro.CR();
		Macro.print("alpha : " + alpha + " good classif :" + goodclassif + "/"+ Z );

		// stats
		float success= goodclassif/(float)Z;
		double averageTime= StatsMacro.Mean(convTime),
		stdDevT= StatsMacro.StdDev(convTime, averageTime);

		String firstLine= " alpha " + " good " +  " T " + "sdev" + " out " + "Ns"; 
		// output
		String line = "" 
			+ alpha +  " " + 
			success + "  " + 
			averageTime + "  " +
			stdDevT + "  " +
			freq012[2] +  "  " +
			Z  ;
		return line;

	}

	public void SetInitialMeasure(double dini) {
		m_init.SetInitRate(dini);
	}

	public double OneMajRun(){
		m_sampler.sig_Init();
		m_sampler.NstepsAndUpdate(50);
		return m_densMD.GetMeasure();
	}

	

	/*
	 * PARSING command line argument
	 */

	final static String ClassName= Thread.currentThread().getStackTrace()[1].getClassName();

	final static String USAGE= "L Z MIN MAX STP";
	final static int NARG=5;


	/** main : processing command line * */
	public static void main(String[] argv) {

		CommandParser parser =new CommandParser(ClassName,USAGE,argv);
		parser.checkArgNum(NARG); 
		// by default dir is current directory
		int L= parser.IParse("L=");
		int Z= parser.IParse("Z=");
		BigDecimal min= parser.BIParse("MIN=");
		BigDecimal max= parser.BIParse("MAX=");
		BigDecimal stp= parser.BIParse("STP=");

		//		BigDecimal min= new BigDecimal(0.66), max= new BigDecimal(0.80), stp= new BigDecimal(0.02);

		String modelName= "Consensus";
		String s = " Size : " + L + " Model: " + modelName;
		Macro.print(1, ">*> Doing DensityTime experiment for " + s);

		String fileName= modelName + "-L-" + L + ".dat";
		ConsensusConvergenceExp exp = new ConsensusConvergenceExp(modelName, fileName, L);
		ArrayList<BigDecimal> alphaList= GetList(min,max,stp);
		exp.RunSamples(Z, alphaList);

		parser.Exit();
	}



	

	final static int PRECISION=6; // 10 ^-6

	private static ArrayList<BigDecimal> GetList(BigDecimal alphaMin, BigDecimal alphaMax, BigDecimal alphaStp) {
		ArrayList<BigDecimal> list= new ArrayList<BigDecimal>();
		BigDecimal alphaVal= alphaMin;	
		while (alphaVal.compareTo(alphaMax) <=0 ){
			alphaVal= alphaVal.setScale(PRECISION, BigDecimal.ROUND_HALF_DOWN);
			list.add(alphaVal);
			alphaVal = alphaVal.add(alphaStp);
			
		}

		return list;
	}



}
