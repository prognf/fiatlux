package densClassif;

import main.FiatLuxProperties;
import main.Macro;
import main.commands.CommandParser;
import architecture.interactingParticleSystems.IPSsamplerAbstract;
import architecture.interactingParticleSystems.IPSsamplerBimodal;
import architecture.interactingParticleSystems.InteractingParticleSystemsBimodal;

import components.types.FLStringList;
import components.types.IntC;
import experiment.measuring.planar.DensityIpsMD;
import experiment.measuring.planar.PairEnergyMD;
import grafix.gfxTypes.imgFormat.ImgFormat;
/*-------------------- 
 * experimentation for the density classification problem
 * @author Nazim Fatès
 *--------------------*/
public class FerreroDensityClassifExp {

	final static int MAXTIME=100000;

	// EXP TYPE
	private static final String HOMOPHASE = "HomoPhase";
	private static final String TOFIXEDPOINT = "ToFP";
	enum EXPTYPE  { HomoPhase, ToFP}

	/*--------------------
	 * attributes
	 --------------------*/

	IPSsamplerAbstract m_sampler;
	private PairEnergyMD m_ArchipelagoMD;
	private DensityIpsMD m_FixedPointMD;

	/*--------------------
	 * construction
	 --------------------*/

	/** Ferrero */
	public FerreroDensityClassifExp(EXPTYPE expType, 
			int L, double lambda, double chi, double epsilon) {
		// model		
		//m_modelTYPE= MODEL.valueOf(modelName);
		// sampler
		IntC dim= new IntC(L,L);
		m_sampler= new IPSsamplerBimodal(dim);

		InteractingParticleSystemsBimodal system= (InteractingParticleSystemsBimodal)m_sampler.GetSystem();
		system.SetParValues(lambda, chi, epsilon);


		// measures
		switch (expType){
		case HomoPhase:
			m_ArchipelagoMD= new PairEnergyMD();
			m_ArchipelagoMD.LinkTo(m_sampler);
			break;
		case ToFP:
			m_FixedPointMD= new DensityIpsMD();
			m_FixedPointMD.LinkTo(m_sampler);
			break;
		}

	}


	/*--------------------
	 * main
	 --------------------*/
	private void RunSamplesHomoPhase(final EXPTYPE expT, int Z, String outputFilename) {
		FLStringList sampling= new FLStringList();
		for (int sampleI=0; sampleI<Z; sampleI++){
			String oneResult= OneRunToArchipelago();
			sampling.Add(oneResult);
			Macro.ProgressBar(sampleI, Z);
		}
		sampling.WriteToFile(outputFilename);
	}

	private void RunSamplesToFP(final EXPTYPE expT, int Z, String filename) {
		StatsReceiverWriter stat= new StatsReceiverWriter(filename, Z);
		int count=0;
		for (int sampleI=0; sampleI<Z; sampleI++){
			boolean goodClassif= OneRunToFP(stat);
			if (goodClassif) {
				count++;
			}
			Macro.ProgressBar(sampleI, Z);
		}
		float quality= (count)/(float)Z;
		String statSampling= GetExpConditions() + String.format(" quality %.3f ",quality); 
		Macro.print(" RES. STAT: " + statSampling);
	}


	private String GetExpConditions() {
		return m_sampler.GetExpConditions();
	}


	private boolean OneRunToFP(StatsReceiverWriter stat) {
		int majState=2, seedI=-1, seedS=-1;
		do { // relaunch init in case of EQUALITY
			InitSample();
			seedI= m_sampler.GetInitializer().GetSeed();
			seedS= m_sampler.GetSystem().GetSeed();
			majState= m_FixedPointMD.GetMajorityState();
			if (majState==2){
				Macro.print("EQUALITY CASE... new random init cond.");
			}
		} while (majState==2);

			int fp = m_FixedPointMD.isHomogeneousPointAttained();
		int time=0;
		// until we reach goal or maxtime
		while ( (fp<0)  && (time < MAXTIME) ){
			m_sampler.sig_NextStep();
			time++;
			fp = m_FixedPointMD.isHomogeneousPointAttained();
		}
		boolean timeout= (fp<0);
		String timeoutStr= timeout?"<timeout>":"="+fp+"=";
		boolean goodClassif= (fp==majState);
		String resClassif=goodClassif?"1":"0";
		String sampleResult=
				String.format("%s %4d maj: %d %s is: %10d ss: %10d", 
						resClassif, time, majState, timeoutStr, seedI, seedS );
		stat.AddSample(sampleResult);
		return goodClassif;
	}


	private String OneRunToArchipelago(){
		InitSample();
		int seedI= m_sampler.GetInitializer().GetSeed();
		int seedS= m_sampler.GetSystem().GetSeed();

		boolean end = m_ArchipelagoMD.isArchipelagoAttained();
		int time=0;
		// until we reach goal or maxtime
		while ( (!end)  && (time < MAXTIME) ){
			m_sampler.sig_NextStep();
			time++;
			end = m_ArchipelagoMD.isArchipelagoAttained();
		}
		boolean timeout= !end;
		String timeoutStr= timeout?"<timeout>":"===";
		String sampleResult=
				String.format("%4d %s init-s: %10d sys-s: %10d", 
						time, timeoutStr, seedI, seedS );
		return sampleResult;
	}


	private String OneRunToFixedPoint(){
		InitSample();
		int seedI= m_sampler.GetInitializer().GetSeed();
		int seedS= m_sampler.GetSystem().GetSeed();

		boolean end = m_ArchipelagoMD.isArchipelagoAttained();
		int time=0;
		// until we reach goal or maxtime
		while ( (!end)  && (time < MAXTIME) ){
			m_sampler.sig_NextStep();
			time++;
			end = m_ArchipelagoMD.isArchipelagoAttained();
		}
		boolean timeout= !end;
		String timeoutStr= timeout?"<timeout>":"===";
		String sampleResult=
				String.format("%4d %s init-s: %10d sys-s: %10d", 
						time, timeoutStr, seedI, seedS );
		return sampleResult;
	}


	/** initialisation of one sample **/
	private void InitSample() {
		m_sampler.sig_Init();
	}

	/*
	 * PARSING command line argument
	 */
	final static String ClassName= Thread.currentThread().getStackTrace()[1].getClassName();

	final static String USAGE= "MODEL L lambda chi eps Z";
	final static int NARG=6;

	/** main : processing command line * */
	public static void main(String[] argv) {
		FiatLuxProperties.SetVerboseLevel(3);

		//SnapExpC();
		//SnapExpD();
		CommandParser parser =new CommandParser(ClassName,USAGE,argv);
		parser.checkArgNum(NARG); 
		// by default dir is current directory
		String modelName=  parser.SParse("MODEL=");
		int L= parser.IParse("L=");
		double lambda= parser.DParse("lambda=");
		double chi= parser.DParse("chi=");
		double eps= parser.DParse("eps=");
		int Z= parser.IParse("Z=");

		String s = "size : " + L + " model: " + modelName;
		Macro.print(1, ">*> running experiment for: " + s);
		String fileName= modelName + "-L-" + L + 
				fParC("lambda",lambda)+ fParC("chi",chi) + fPardM("eps",eps)+
				".dat";

		EXPTYPE expT= null;
		if (modelName.equals(HOMOPHASE)){
			expT= EXPTYPE.HomoPhase;
		} else if (modelName.equals(TOFIXEDPOINT)){
			expT= EXPTYPE.ToFP;
		}
		FerreroDensityClassifExp exp = 
				new FerreroDensityClassifExp(expT, L, lambda, chi, eps);
		//exp.OneRun();
		if (expT==EXPTYPE.HomoPhase){
			exp.RunSamplesHomoPhase(expT, Z, fileName);
		} else {
			exp.RunSamplesToFP(expT, Z, fileName);
		}
		parser.Exit();
	}

	private static String fPardM(String label, double dval) {
		return "-"+label+"-"+(int)(dval*10000);
	}
	
	private static String fParC(String label, double dval) {
		return "-"+label+"-"+(int)(dval*100);
	}

	static int SEEDINI=1502, SEEDSYS=2015;
	static int SEEDINI2=2806, SEEDSYS2=1976;
	static int NS=6, T=20;
	static IntC PixSZ= new IntC(40,1);

	static int [] T1 = {20,30,50}; 
	static int [] T2 = {200,300,500}; 


	public static void SnapExpC(){
		int L=8;
		IntC dim= new IntC(L,L);

		// init with viewer
		IPSsamplerBimodal sampler= new IPSsamplerBimodal(dim,PixSZ);

		InteractingParticleSystemsBimodal system= (InteractingParticleSystemsBimodal)sampler.GetSystem();
		double lambda=0.25, chi=0.1, epsilon=0.0;
		system.SetParValues(lambda, chi, epsilon);
		sampler.GetInitializer().SetSeed(SEEDINI);
		system.SetSeed(SEEDSYS);
		sampler.sig_Init();
		SaveImage("snap-homophase", sampler);
        for (int aT1 : T1) {
            for (int t = 0; t < aT1; t++) {
                sampler.sig_NextStep();
            }
            SaveImage("snap-homophase", sampler);
        }
	}
	public static void SnapExpD(){
		int L=8;
		IntC dim= new IntC(L,L);

		// init with viewer
		IPSsamplerBimodal sampler= new IPSsamplerBimodal(dim,PixSZ);

		InteractingParticleSystemsBimodal system= (InteractingParticleSystemsBimodal)sampler.GetSystem();
		double lambda=0.25, chi=0.1, epsilon=0.001;
		system.SetParValues(lambda, chi, epsilon);
		sampler.GetInitializer().SetSeed(SEEDINI2);
		system.SetSeed(SEEDSYS2);
		sampler.sig_Init();
		DensityIpsMD md= new DensityIpsMD();
		md.LinkTo(sampler);
		Macro.print("init N1:" + md.GetMeasure());
		SaveImage("snap-dc",sampler);
        for (int aT2 : T2) {
            for (int t = 0; t < aT2; t++) {
                sampler.sig_NextStep();
            }
            SaveImage("snap-dc", sampler);
        }
	}


	private static void SaveImage(String prefix, IPSsamplerBimodal sampler) {
		String filename= prefix + "-t-" + sampler.GetTime();
		sampler.io_SetRecordFormat(ImgFormat.PNG);
		sampler.io_SaveImage(filename, sampler.GetViewer());
		sampler.io_SetRecordFormat(ImgFormat.SVG);
		sampler.io_SaveImage(filename, sampler.GetViewer());
	}

	
}
