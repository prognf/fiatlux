package densClassif;

import java.util.Date;

import main.Macro;

import components.types.FLString;
import components.types.FLStringList;

/*-------------------------------------------------------------------------------
- for handling statistics
------------------------------------------------------------------------------*/
public class StatsReceiverWriter {


	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();

	// if for each sample we write the progress... 
	public static final boolean PRINTPROGRESSEACHSAMPLE = false;

	//--------------------------------------------------------------------------
	//- attributes
	//--------------------------------------------------------------------------

	public boolean PRN;

	int m_Z; // number of samples
	String m_filename; // target file
	FLStringList m_list; // data
	java.util.Date m_startTime;

	//--------------------------------------------------------------------------
	//- construction
	//--------------------------------------------------------------------------

	public StatsReceiverWriter(String filename, int Z) {
		m_Z= Z;
		m_filename= filename;
		m_startTime= Macro.GetTime();
		m_list= new FLStringList();
	}

	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------

	/** return true when finished */
	public boolean AddSample(String strSample){
		m_list.Add(strSample);
		int S= GetCurrentSize();
		if (PRINTPROGRESSEACHSAMPLE){
			Macro.fPrint("%d / %d ",S,m_Z);
		} else {
			Macro.ProgressBar(S, m_Z);
		}
		boolean finished= (S>=m_Z);
		if (finished){ // "flush"
			
			Date tEnd= Macro.GetTime();
			String info= "output file: " + m_filename ;
			info+= String.format(" with %d samples ; exp time: %s",
					m_Z, FLString.TimeFormat(tEnd,m_startTime));
			Macro.print(info);
			m_list.WriteToFile(m_filename);

		}
		return finished;
	}

	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------


	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------


	public int GetCurrentSize(){
		return m_list.GetSize();
	}






}
