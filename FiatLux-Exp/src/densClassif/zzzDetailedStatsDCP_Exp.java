package densClassif;

import initializers.CAinit.BinaryInitializer;
import initializers.IPSinit.BernoulliIPSinit;
import main.Macro;
import main.commands.CommandParser;
import architecture.interactingParticleSystems.IPSsampler;

import components.types.FLString;

/*-------------------- 
 * experimentation for the density classification problem
 * @author Nazim Fatès
*--------------------*/
public class zzzDetailedStatsDCP_Exp extends DCPexpMachinery {

	final static int MAXTIME = 10000;


	IPSsampler m_ips; // for debug

	/*--------------------
	 * construction
	 --------------------*/

	/* XYsize is the dimension of the grid */
	public zzzDetailedStatsDCP_Exp(String modelName, int L) {
		// model: will be used by all samplers (danger ?)
		m_modelTYPE= MODEL.valueOf(modelName);
	
		PrepareSampler(L);
		PrepareInitializer();
	}



	void PrepareInitializer() {
		// initializer
		switch (m_modelTYPE){
		case IPS4:
		case IPS8:
			//TODO m_init= (BernoulliIPSinit)m_sampler.GetInitializer();
			((BernoulliIPSinit)m_init).SetInitDens(0.5);
			break;
		default:
			//TODO m_init= ((CAsampler)m_sampler).GetInitializer();
			((BinaryInitializer)m_init).SetInitRate(0.5);
			break;
		}
	}

	/*--------------------
	 * implementations
	 --------------------*/


	private static String GetFileName(String modelName, int L, double eps) {
		String s_eps= FLString.ReplaceDotByLetterP(eps);
		return 
		"det-" + modelName + "-L-" + L + 
		"-eps-" + s_eps +
		".dat";
	}
	
	@Override
	int GetMaxTime() {
		return MAXTIME;		
	}
	
	/*
	 * PARSING command line argument
	 */
	final static String ClassName= Thread.currentThread().getStackTrace()[1].getClassName();
	final static String USAGE= "MODEL L Z";
	final static int NARG=3;

	/** main : processing command line * */
	public static void main(String[] argv) {

		// init
		CommandParser parser= new CommandParser(ClassName,USAGE,argv);
		//parser.checkArgNum(NARG); 

		// parsing
		String modelName=  parser.SParse("MODEL=");
		int L= parser.IParse("L=");
		int Z= parser.IParse("Z=");
		double eps= parser.DParse("eps=");

		// info
		String s = " Model: " + modelName ;
		Macro.print(1, ">*> Experiment with param: " + s);

		// exp
		zzzDetailedStatsDCP_Exp exp = new zzzDetailedStatsDCP_Exp(modelName, L);
		exp.SetModelEps(eps);
		ExpOutputDCP samples= exp.RunSamples(Z);
		// output
		String filename= GetFileName(modelName, L, eps);
	
		samples.WriteToFile(filename);
		parser.Exit();
	}



	private void PrintCurrentState() {
		//double dens= m_Dens.GetMeasure();
		//Macro.print(" dens: " + dens);
		//Macro.Debug(" t: " + m_sampler.GetTime() + " s:" + m_ips.GetCurrentState());
	}

}
