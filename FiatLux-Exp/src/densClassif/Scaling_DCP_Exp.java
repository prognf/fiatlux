package densClassif;

import components.types.FLString;
import components.types.FLStringList;
import components.types.IntC;
import components.types.IntegerList;
import experiment.measuring.general.UniformStateMDgen;
import experiment.samplers.CAsampler;
import experiment.samplers.PlanarSampler;
import initializers.CAinit.NpointsInitializer;
import main.Macro;
import main.StatsMacro;
import main.commands.CommandParser;
import models.CAmodels.binary.BinaryModel;
import models.CAmodels.binary.MajorityModel;
import models.CAmodels.densClassif.FuksDensityStudyModel;
import models.CAmodels.densClassif.GKL_Model;
import models.CAmodels.densClassif.KariModel;
import models.CAmodels.densClassif.SchueleDensityStudyModel;
import models.CAmodels.densClassif.TraMajDensityStudyModel;
import topology.basics.PlanarTopology;
import topology.zoo.NCE_Topology;
/*-------------------- 
 * experimentation for the density classification problem
 * @author Nazim Fatès
*--------------------*/
public class Scaling_DCP_Exp {

	final static int MAXTIME=5000;
	private static final int NSTATES = 3; // for stats
	
	private static final String FILENAME_LATTICESIZES="sizes.set";
	
	enum MODEL {GKL, Fuks, Schuele, TraMaj, Kari, Maj} // also codes for filenames !!
	/*--------------------
	 * attributes
	 --------------------*/

	CAsampler m_sampler;
	private UniformStateMDgen m_MeasuringDevice;
	NpointsInitializer m_init;
	double m_dini;
	private MODEL m_modelTYPE;
	private BinaryModel m_model;
	
	String m_output_filename;
	
	
	/*--------------------
	 * construction
	 --------------------*/

	/* XYsize is the dimension of the grid */
	public Scaling_DCP_Exp(String modelName, double dini, double eps) {

		// output
		m_output_filename= GetFileName(modelName, dini, eps);
		
		// model: will be used by all samplers (danger ?)
		m_modelTYPE= MODEL.valueOf(modelName);
		m_model = GetModel(modelName);
		SetEps(eps);

		// initializer: will be used by all samplers (danger ?) 
		//m_init = new StandardInitializer();
		m_init = new NpointsInitializer();
		m_dini= dini;

		// measuring device: will be used by all samplers (danger ?)	
		m_MeasuringDevice= new UniformStateMDgen();
				
	
	}

	/** construction of the sampler **/
	private void PrepareSampler(int L, double dini) {
		
		//String code = m_model.GetDefaultAssociatedTopology();
		//LinearTopology topology = (LinearTopology)TopologyCoder.GetTopologyFromCode(code);
		//m_sampler= new LinearSampler(L, m_model, topology);
		PlanarTopology topology= new NCE_Topology();
		IntC XYsize= new IntC(L,L);
		m_sampler= new PlanarSampler(XYsize, m_model, topology);
		m_MeasuringDevice.LinkTo(m_sampler);
		m_sampler.SetInitDeviceFromOutside(m_init);
		m_init.SetByDensity(dini);
	}

	private void SetEps(double eps){
		switch(m_modelTYPE){
		case Maj:
			break;
		case GKL:
			break;
		case Fuks:
			((FuksDensityStudyModel)m_model).SetpShift(eps);
			break;
		case Schuele:
			((SchueleDensityStudyModel)m_model).SetEps(eps);
			break;
		case TraMaj:
			((TraMajDensityStudyModel)m_model).SetEps(eps);
			break;
		case Kari:
			break;
		}
	}

	private BinaryModel GetModel(String modelName) {
		switch(m_modelTYPE){
		case Maj:
			return new MajorityModel();
		case GKL:
			return new GKL_Model();
		case Fuks:
			return new FuksDensityStudyModel();
		case Schuele:
			return new SchueleDensityStudyModel();
		case TraMaj:
			return new TraMajDensityStudyModel();
		case Kari:
			return new KariModel();
		}
		return null;
	}



	/*--------------------
	 * main
	 --------------------*/
	public void RunSamples(int Z){
		FLStringList output= new FLStringList();
		IntegerList lst = IntegerList.ReadFromFile(FILENAME_LATTICESIZES);
		for (int L: lst){
			String line = MakeZsamples(L, Z);
			output.Add(line);
		}
		output.WriteToFile(m_output_filename);
	}

	/** one experiment for various alpha values **/
	public String MakeZsamples(int L, int Z){
		
		PrepareSampler(L, m_dini);
		
		
		int  [] freq012 = new int [NSTATES]; // how many time 0, 1, 2 are attained
		int [] convTime = new int [Z]; // convergence time of each sample

		int countGoodclassif =0;
		for (int sample=0; sample < Z; sample++){			
			Macro.ProgressBar(sample,Z); // 
			
			// initialisation
			m_sampler.sig_Init();
			int initmajstate= m_MeasuringDevice.MajState();
			//Macro.print(6, " majstate: " + initmajstate);
			if (initmajstate == -1){
				Macro.FatalError("Equal initial repartition - set L to odd value");
			}
			
			int uniform = m_MeasuringDevice.IsUniformState(); // -1 means not uniform
			int time=0;
			while ((uniform < 0 ) && (time < MAXTIME)){
				m_sampler.sig_NextStep();
				time++;
				//double dens= m_Dens.GetMeasure();
				//Macro.print(" dens: " + dens);
				uniform = m_MeasuringDevice.IsUniformState();
				//Macro.print("uniform " + uniform + " dens :" + dens);
			}
			//TODO CHECK that time is NOT NECESSARY 
			// counting good classifications
			if (uniform == initmajstate){
				countGoodclassif++;
			}
			// stats : 0, 1, 2 = out of time
			int statindex = (uniform<0) ? 2 : uniform; 
			freq012[statindex]++;
			convTime[sample]= time;
		}//for

		// printing output
		// result.Print();
		Macro.print("Results : ");
		for (int i=0; i < NSTATES; i++){
			Macro.fPrintNOCR("[ "  + i + " : " +freq012[i]+ " ] ");
		}
		Macro.CR();
		Macro.print(2, "L: " + L + " good classif: " + countGoodclassif + "/"+ Z );
		Macro.CR();
		// stats
		float success= countGoodclassif/(float)Z;
		double averageTime= StatsMacro.Mean(convTime),
		stdDevT= StatsMacro.StdDev(convTime, averageTime);
		
		String firstLine= " L " + " good " +  " T " + "sdev" + " out " + "Ns"; 
		// output
		return MakeOutput(L, success, averageTime, stdDevT, freq012, Z);
		 
	}

	private String MakeOutput(int L, float success, double averageTime,
			double stdDevT, int [] freq012, int Z) {
		String line = "" + L +  " " + 
			 success + "  " + 
			 averageTime + "  " +
			 stdDevT + "  " +
			 freq012[2] +  "  " + Z  ;
		return line;
	}


	
	
	
	
	/*
	 * PARSING command line argument
	 */
	
	final static String ClassName= Thread.currentThread().getStackTrace()[1].getClassName();
	final static String USAGE= "MODEL DINI EPS Z";
	final static int NARG=4;
	
	/** main : processing command line * */
	public static void main(String[] argv) {

		// init
		CommandParser parser= new CommandParser(ClassName,USAGE,argv);
		parser.checkArgNum(NARG); 
		
		// parsing
		String modelName=  parser.SParse("MODEL=");
		double dini= parser.DParse("DINI=");
		double eps= parser.DParse("EPS=");
		int Z= parser.IParse("Z=");
		
		// info
		String s = " Model: " + modelName + " dini : " + dini + " eps: " + eps;
		Macro.print(1, ">*> Experiment with param: " + s);

		// exp
		Scaling_DCP_Exp exp = new Scaling_DCP_Exp(modelName, dini, eps);
		exp.RunSamples(Z);
		parser.Exit();
	}


	 
	private static String GetFileName(String modelName, double dini,
			double alpha) {
		String 
			s_dini= FLString.ReplaceDotByLetterP(dini),
			s_alpha= FLString.ReplaceDotByLetterP(alpha);
		return 
			"Scale-" + modelName + 
			"-D-" + s_dini + 
			"-A-" + s_alpha +".dat";
	}



	final static int PRECISION=6; // 10 ^-6
	
	
	
}
