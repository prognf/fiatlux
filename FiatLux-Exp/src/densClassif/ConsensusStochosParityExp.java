package densClassif;

import architecture.interactingParticleSystems.IPSlinearSystem;
import architecture.interactingParticleSystems.InteractingParticleSystemOneDSampler;
import components.types.FLStringList;
import main.Macro;
import main.StatsMacro;
import main.commands.CommandParser;
/*-------------------- 
 * experimentation for the density classification problem
 * @author Nazim Fatès
 *--------------------*/
public class ConsensusStochosParityExp {

	final static int MAXTIME=500000;
	private static final int NSTATES = 2; // for stats
	/*--------------------
	 * attributes
	 --------------------*/

	InteractingParticleSystemOneDSampler m_sampler;
	IPSlinearSystem m_system;
	//final InitializerEvenOdd m_init;

	/*--------------------
	 * construction
	 --------------------*/

	/* constructor */
	public ConsensusStochosParityExp(int L) {

		m_sampler= new InteractingParticleSystemOneDSampler(L);
		m_system = m_sampler.getSystem();

	}



	/*--------------------
	 * main
	 --------------------*/


	public void RunSamples(int Z){
		FLStringList output= new FLStringList();
		MakeZsamples(Z,output);
		String modelType= InteractingParticleSystemOneDSampler.m_modeltype.toString();
		String fileName= String.format("stP-%s-L-%d.dat", modelType, m_sampler.GetSize());
		output.WriteToFile(fileName);
	}

	/** one experiment for various alpha values **/
	public void MakeZsamples(int Z, FLStringList output){

		int  [] freq012 = new int [NSTATES];

		int [] convTime = new int [Z];
		for (int sample=0; sample < Z; sample++){			
			Macro.ProgressBar(sample,Z); // 
			m_sampler.sig_Init();
			boolean uniform = m_system.hasUniformState(); 
			int time=0;
			while ((!uniform) && (time < MAXTIME)){
				m_sampler.sig_NextStep();
				time++;
				uniform = m_system.hasUniformState();
			}
			// stats : 0, 1, 2 = out of time
			int statindex = uniform? 0 : 1 ; 
			freq012[statindex]++;
			convTime[sample]= time;
			//Macro.print("Converged to " + statindex + " in time : " + time);
		}//for

		// printing output
		// result.Print();
		Macro.print("Results : ");
		for (int i=0; i < NSTATES; i++){
			Macro.fPrintNOCR("[ "  + i + " : " +freq012[i]+ " ] ");
		}
		Macro.CR();

		// stats
		double averageTime= StatsMacro.Mean(convTime);
		double	stdDevT= StatsMacro.StdDev(convTime, averageTime);

		 
		// output
		String rawtimes="";
		for (int i=0; i<Z; i++) {
			rawtimes+= convTime[i] + " ";
		}
		output.Add(rawtimes);
		String firstLine= " T    " + "sdev" + " out " + "Ns";
		String line = String.format("%.1f   %.3f %d %d",	averageTime, stdDevT,freq012[1],Z);
		output.Add(firstLine,line);
	}

	/*
	 * PARSING command line argument
	 */

	final static String ClassName= Thread.currentThread().getStackTrace()[1].getClassName();

	final static String USAGE= "L Z";
	final static int NARG=2;


	/** main : processing command line * */
	public static void main(String[] argv) {

		CommandParser parser =new CommandParser(ClassName,USAGE,argv);
		parser.checkArgNum(NARG); 
		// by default dir is current directory
		int L= parser.IParse("L=");
		int Z= parser.IParse("Z=");

	
		ConsensusStochosParityExp exp = new ConsensusStochosParityExp(L);
		exp.RunSamples(Z);

		parser.Exit();
	}




}
