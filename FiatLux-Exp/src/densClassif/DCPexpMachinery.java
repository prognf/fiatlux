package densClassif;

import architecture.interactingParticleSystems.IPSsampler;
import components.randomNumbers.FLRandomGenerator;
import components.types.FLStringList;
import components.types.IntC;
import experiment.measuring.general.UniformStateMDgen;
import experiment.samplers.PlanarSampler;
import experiment.samplers.SuperSampler;
import initializers.SuperInitializer;
import main.Macro;
import main.StatsMacro;
import models.SuperModel;
import models.CAmodels.binary.BinaryModel;
import models.CAmodels.binary.MajorityModel;
import models.CAmodels.densClassif.TraMajGen2DModel;
import models.CAmodels.densClassif.ReynagaModel;
import models.IPSmodels.AbstractDensClassifIPSmodel;
import models.IPSmodels.IPSmodel.CODE_IPS_MODEL;
import topology.basics.PlanarTopology;
import topology.zoo.Moore8TM;
import topology.zoo.NCE_Topology;
import topology.zoo.NESW_TM;

abstract public class DCPexpMachinery {
	

	abstract void PrepareInitializer();
	abstract int GetMaxTime();
	
	private static final int NRESULTSTATES = 3; // for stats
	private static final boolean PRINTDETAIL = true;
	
	
	enum MODEL {Maj, Reynaga4, Reynaga8, TraMaj, IPS4, IPS8} // also codes for filenames !!

	/*--------------------
	 * attributes
	 --------------------*/

	SuperSampler m_sampler;
	private SuperModel m_model;
	SuperInitializer m_init;
	double m_dini;

	private UniformStateMDgen m_md;

	protected MODEL m_modelTYPE;

	/** construction of the sampler 
	 * CA and IPS are separated here **/
	protected void PrepareSampler(int L) {
		PlanarTopology topology = GetAssociatedTopo();
		IntC XYsize= new IntC(L,L);
		switch(m_modelTYPE){
		case IPS4:			
			String c_topoP4="TV4";
			String c_topoE4="TV4";
			CODE_IPS_MODEL modelCode4= CODE_IPS_MODEL.DensClassif4;
			m_sampler= new IPSsampler(c_topoP4, c_topoE4, XYsize, modelCode4);
			m_model= ((IPSsampler)m_sampler).GetIPSModel();
			//MD
			m_md= new UniformStateMDgen();
			break;
		case IPS8:			
			String c_topoP8="TM8";
			String c_topoE8="TM8";
			CODE_IPS_MODEL modelCode8= CODE_IPS_MODEL.DensClassif8;
			m_sampler= new IPSsampler(c_topoP8, c_topoE8, XYsize, modelCode8);
			m_model= ((IPSsampler)m_sampler).GetIPSModel();
			
			//MD
			m_md= new UniformStateMDgen();
			break;
			
		default:
			m_model = GetCAModel(); 		
			BinaryModel model2= (BinaryModel) m_model;
			m_sampler= new PlanarSampler(XYsize, model2, topology);		//2D ?
			//MD
			m_md= new UniformStateMDgen();
			break;
		}
	
		// MD

		m_md.LinkTo(m_sampler);
		
	}
	// DEBUG
//	Macro.Debug(" We are using ips model " + m_model.GetName());
//	m_ips= (IPSsampler)m_sampler;
	//IPSAutomatonViewer view = new IPSAutomatonViewer(, XYsize);
	//m_ips.AddViewer(new IntCouple(40,1));
	//SimulationWindowSingle simu= new SimulationWindowSingle("debug ips", m_sampler);
	//simu.BuildAndDisplay();

	private PlanarTopology GetAssociatedTopo() {
		switch(m_modelTYPE){
		case Maj:
			return new NCE_Topology(); 
		case Reynaga4:
			return new NESW_TM(); 
		case Reynaga8:
			return new Moore8TM();	
		case TraMaj:
			return new Moore8TM(); 
		}
		return null;
	}

	private SuperModel GetCAModel() {
		switch(m_modelTYPE){
		case Maj:
			return new MajorityModel();
		case Reynaga4:
		case Reynaga8:
			return new ReynagaModel();
		case TraMaj:
			return new TraMajGen2DModel();
		default:
			Macro.FatalError("problem with model type");
			break;
		}
		return null;
	}
	
	protected void SetModelEps(double eps) {
		if (m_model instanceof TraMajGen2DModel){
			Macro.print(" eps set to :" + eps);
			((TraMajGen2DModel) m_model).SetEps(eps);
		} else if (m_model instanceof AbstractDensClassifIPSmodel){
			Macro.print(" IPS eps set to :" + eps);
			AbstractDensClassifIPSmodel model = (AbstractDensClassifIPSmodel) ((IPSsampler)m_sampler).GetIPSModel();
			model.SetNoiseRate(eps);
		}	
	}




	/*--------------------
	 * main
	 * @return 
	 --------------------*/
	public ExpOutputDCP RunSamples(int Z){
		FLStringList samples= new FLStringList(); // for keeping track of each sample

		int  [] freq012 = new int [NRESULTSTATES]; // how many time 0, 1, 2 are attained
		int [] convTime = new int [Z]; // convergence time of each sample

		int countGoodclassif =0;
		for (int sample=0; sample < Z; sample++){			
			Macro.ProgressBar(sample,Z); // 

			int seedIni= FLRandomGenerator.GetNewSeed();
			m_init.SetSeed(seedIni);
			/*int xyz=1;
			while (xyz==1){
				
			}*/
			
			int seedMod= FLRandomGenerator.GetNewSeed();
			m_model.SetSeed(seedMod);

			// initialisation
			m_sampler.sig_Init();
			//PrintCurrentState();
			int initmajstate= m_md.MajState();
			//Macro.print(6, " majstate: " + initmajstate);
			if (initmajstate == -1){
				Macro.FatalError("Equal initial repartition - set L to odd value");
			}

			int uniform = m_md.IsUniformState(); // -1 means not uniform
			int time=0;
			while ((uniform < 0 ) && (time < GetMaxTime())){
				m_sampler.sig_NextStep();
				time++;
				//PrintCurrentState();
				uniform = m_md.IsUniformState();
				//Macro.print("uniform " + uniform + " dens :" + dens);
			}
			// stats : fixed point attained 0, 1, 2 = out of time
			int statindex = (uniform<0) ? 2 : uniform; 
			freq012[statindex]++;
			//Macro.Debug( "res : " + statindex + " count "  + freq012[statindex]);
			convTime[sample]= time;

			// counting good classifications
			boolean good= false;
			if (uniform == initmajstate){
				countGoodclassif++;
				good= true;
			}
			String result= (good?"+ ":". " ) + statindex + " T:" + time + " I:" + seedIni +" M:" + seedMod;
			if (PRINTDETAIL)
				samples.Add(result);
		}//for


		// stats
		Macro.print("Results : ");
		for (int i=0; i < NRESULTSTATES; i++){
			Macro.fPrintNOCR("[ "  + i + " : " +freq012[i]+ " ] ");
		}
		Macro.CR();
	
		float success= countGoodclassif/(float)Z;		
		double 
			averageTime= StatsMacro.Mean(convTime),
			stdDevT= StatsMacro.StdDev(convTime, averageTime);		
		ExpOutputDCP output= 
			new ExpOutputDCP(samples, success, averageTime, stdDevT, freq012[2]);
		output.Print();
		return output;
	}

	

}
