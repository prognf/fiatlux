package densClassif;

import main.MathMacro;
import main.commands.CommandParser;

import components.types.FLString;
import components.types.FLStringList;

public class Binomial {

	/*
	 * PARSING command line argument
	 */
	
	final static String ClassName= Thread.currentThread().getStackTrace()[1].getClassName();
	final static String USAGE= "N D";
	final static int NARG=2;
	
	/** main : processing command line * */
	public static void main(String[] argv) {

		// init
		CommandParser parser= new CommandParser(ClassName,USAGE,argv);
		parser.checkArgNum(NARG); 
		
		// parsing
		int N= parser.IParse("N=");
		double dens= parser.DParse("d=");		
		//String inFileName= parser.SParse("IN=");
			
		// main
		double[] binom= GenBinomial(N, dens);
		//double[] quality = GetQuality(inFileName);
		
		//FLStringList result = synthetize(binom, quality);
		FLStringList result = synthetize1(binom);
		String sdens = FLString.ReplaceDotByLetterP(dens);
		String fileName = "mult-N-" + N + "-d-" + sdens + ".gdat"; 
		result.WriteToFile(fileName);
		parser.Exit();
	}	
	
	private static FLStringList synthetize(double[] binom, double[] quality) {
		FLStringList out= new FLStringList();
		int size= binom.length; 
		for (int k=0; k< size;k++){
			double prod = binom[k] * quality[k];
			
			String line =  FLString.FormatNumericLine(k, prod, binom[k], quality[k]);
			out.add(line);
		}
		return out;
	}

	private static FLStringList synthetize1( double[] binom) {
		FLStringList out= new FLStringList();
		int size= binom.length; 
		for (int k=0; k< size;k++){
			
			String line = k + " " + binom[k];
			out.add(line);
		}
		return out;
	}
	
	/* private static double[] GetQuality(String inFileName) {
		FLStringList parsed= Macro.ParseFileOneColumn(inFileName, 1);
		double[] doubleVal = parsed.toDoubleArray();
		return doubleVal;
	}*/

	private static double[] GenBinomial(int n,double dens){
		double[] output = new double [n+1];
		for (int k=0; k<=n; k++){
			double d= MathMacro.combination(n, k) * Math.pow(dens, k) * Math.pow(1.-dens,n-k);
			output[k]= d;
		}
		return output;
	}
	
}
