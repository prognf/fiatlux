package densClassif;

import components.types.FLString;
import components.types.FLStringList;
import experiment.measuring.general.DensityMD;
import experiment.measuring.general.UniformStateMDgen;
import experiment.samplers.CAsampler;
import experiment.samplers.LinearSampler;
import initializers.CAinit.BinaryInitializer;
import main.Macro;
import main.StatsMacro;
import main.commands.CommandParser;
import models.CAmodels.binary.BinaryModel;
import models.CAmodels.stochastic.ProbabilisticLineR2Model;
import topology.basics.LinearTopology;
import topology.basics.TopologyCoder;
/*-------------------- 
 * experimentation for the density classification problem
 * @author Nazim Fatès
*--------------------*/
public class DensityClassifExpLineR2 {

	private static final int NOUTCOMES = 3; // for stats
	/*--------------------
	 * attributes
	 --------------------*/
	private static final boolean VERBOSE = false;

	private CAsampler m_sampler;
	private BinaryModel m_model;
	private BinaryInitializer m_init;

	private UniformStateMDgen m_uniformityMD;
	private DensityMD m_densMD;
	
	/*--------------------
	 * construction
	 --------------------*/


    /* XYsize is the dimension of the grid */
	public DensityClassifExpLineR2(String filename, int L, int Tmax) {

		m_model = new ProbabilisticLineR2Model(filename);
		// topo 
		String code = m_model.GetDefaultAssociatedTopology();
		LinearTopology topology = (LinearTopology)TopologyCoder.GetTopologyFromCode(code);
		// sampler
		m_sampler= new LinearSampler(L, m_model, topology);
		// measures		
		m_uniformityMD= new UniformStateMDgen();
		m_uniformityMD.LinkTo(m_sampler);
		m_densMD= new DensityMD();
		m_densMD.LinkTo(m_sampler);
		// init
		m_init = new BinaryInitializer();
		double rdensity= 0.5;
		m_init.SetInitRate(rdensity);

		m_sampler.SetInitDeviceFromOutside(m_init);

	}

	/*--------------------
	 * main
	 --------------------*/
	public void RunSamples(int Z, int Tmax, String filename){
		FLStringList output= new FLStringList();
		String line = MakeZsamplesForAlphaVal(Z,Tmax);
		output.Add("rule:"+filename+" "+line);
		output.WriteToFile(filename);
	}

	/** one experiment : Z samples, for Tmax time steps max**/
	public String MakeZsamplesForAlphaVal(int Z, int Tmax){

		statisticalResult stat= new statisticalResult(Z);
		for (int sample=0; sample < Z; sample++){			
			Macro.ProgressBar(sample,Z); // 
			// init phase
			m_sampler.sig_Init();
			double initdens = m_densMD.GetMeasure();
			if (VERBOSE)
				Macro.print(" Init dens: " + initdens);
			if (initdens==0.5){
				Macro.FatalError("Equal initial repartition - set L to odd value");
			}
			int initmajstate=(initdens < 0.5) ?  0 : 1;
			if (VERBOSE)
				Macro.print(" sample : " + (sample+1) + "  idens: " + initdens);

			// go to uniform config or to max time
			int uniform = m_uniformityMD.IsUniformState(); // -1 means not uniform
			int time=0;
			while ((uniform < 0 ) && (time < Tmax)){
				m_sampler.sig_NextStep();
				time++;
				//double dens= m_Dens.GetMeasure();
				//Macro.print(" dens: " + dens);
				uniform = m_uniformityMD.IsUniformState();
				//Macro.print("uniform " + uniform + " dens :" + dens);
			}
			boolean good= (uniform == initmajstate);
			// stats : 0, 1, 2 = out of time
			int statindex = (uniform<0) ? 2 : uniform; // -1 -> 2
			stat.absorb(good, statindex, time);
		}//for

		return stat.asString();
	}

	// for ALL THE SAMPLES
	class statisticalResult {
		int goodClassif=0, isample=0;
		int  [] freq012 = new int [NOUTCOMES];
		int [] convTime;

		public statisticalResult(int Z) {
			convTime = new int [Z];
		}

		public void absorb(boolean good, int statindex, int time) {
			if (good){
				goodClassif++;
			}
			freq012[statindex]++;
			convTime[isample]= time;
			isample++;
		}

		public String asString() {
			// calculating
			float success= goodClassif/(float)isample;
			double averageTime= StatsMacro.Mean(convTime);
//			stdDevT= StatsMacro.StdDev(convTime, averageTime);
			// output
			String line =  String.format("Q=%.3f (%d/%d) T=%.1f O1E:%d/%d/%d",
					success,goodClassif,isample, averageTime, freq012[0], freq012[1],freq012[2]);
			return line;
		}

		
	}
	
	
	/*
	 * PARSING command line argument
	 */	
	final static String ClassName= Thread.currentThread().getStackTrace()[1].getClassName();
	final static String USAGE= "N TMAX Z";
	final static int NARG=4;
	
	/** main : processing command line * */
	public static void main(String[] argv) {
		
		CommandParser parser =new CommandParser(ClassName,USAGE,argv);
		parser.checkArgNum(NARG); 
		
		String filename=parser.SParse("file=");
		int L= parser.IParse("L=");
		int Tmax= parser.IParse("T=");
		int Z= parser.IParse("Z=");
		
		Macro.fPrint(">*> Doing DensityTime experiment for size:%d",L);

		DensityClassifExpLineR2 exp = new DensityClassifExpLineR2(filename, L, Tmax);
		String filenameout= "stat-" + FLString.RemoveExtension(filename) + ".stt";
		exp.RunSamples(Z, Tmax, filenameout);
		
		parser.Exit();
	}

}
