package densClassif;

import architecture.interactingParticleSystems.InteractingParticleSystemOneDSampler;
import components.arrays.RegularDynArray;
import components.types.FLStringList;
import experiment.measuring.general.UniformStateMDgen;
import experiment.samplers.CAsampler;
import experiment.samplers.LinearSampler;
import main.Macro;
import main.StatsMacro;
import main.commands.CommandParser;
import models.CAmodels.binary.RuleBasedBFObinaryModel;
import topology.basics.LinearTopology;
import topology.basics.TopologyCoder;
/*-------------------- 
 * experimentation for the density classification problem
 * @author Nazim Fatès
 *--------------------*/
public class TestBFOparityExp {

	final static int MAXTIME=1000;
	private static final int NSTATES = 2; // for stats
	/*--------------------
	 * attributes
	 --------------------*/
	private static final boolean VERBOSE = false;

	final CAsampler m_sampler;
	final RuleBasedBFObinaryModel m_model;
	RegularDynArray m_system;
	private UniformStateMDgen m_uniformityMD;
	//final InitializerEvenOdd m_init;

	/*--------------------
	 * construction
	 --------------------*/

	/* constructor */
	public TestBFOparityExp(int L) {

		m_model= new RuleBasedBFObinaryModel();
		// topo
		String code = m_model.GetDefaultAssociatedTopology();
		LinearTopology topology = (LinearTopology)TopologyCoder.GetTopologyFromCode(code);
		// sampler
		m_sampler= new LinearSampler(L, m_model, topology);
		m_system = m_sampler.GetAutomaton();
		m_uniformityMD= new UniformStateMDgen();
		m_uniformityMD.LinkTo(m_sampler);

	}

	/*--------------------
	 * main
	 --------------------*/


	public void RunSamples(int Z){
		FLStringList output= new FLStringList();
		MakeZsamples(Z,output);
		String modelType= InteractingParticleSystemOneDSampler.m_modeltype.toString();
		String fileName= String.format("stP-%s-L-%d.dat", modelType, m_sampler.GetSize());
		output.WriteToFile(fileName);
	}

	/** one experiment for various alpha values **/
	public void MakeZsamples(int Z, FLStringList output){

		int  [] freq012 = new int [NSTATES];

		int [] convTime = new int [Z];
		for (int sample=0; sample < Z; sample++){			
			Macro.ProgressBar(sample,Z); //
			int time=0;
			m_sampler.sig_Init();
			if (VERBOSE)
				Macro.fPrint("t:%d config:%s",time, m_system.getGlobalStateAsString());
			String iniconfig= m_system.getGlobalStateAsString();
			int parityIni= m_uniformityMD.countParity();
			int uniform = m_uniformityMD.IsUniformState();

			while ((uniform<0) && (time < MAXTIME)){
				m_sampler.sig_NextStep();
				time++;
				int parity= m_uniformityMD.countParity();
				if (parity!=parityIni) {
					Macro.fPrint(" $$$$$$$$$$$$ WRONG CLASSIFICATION WITH initconfig:%s ", iniconfig);
					Macro.systemExit(1);
				}
				uniform = m_uniformityMD.IsUniformState();
				if (VERBOSE)
					Macro.fPrint("t:%4d config:%s",time, m_system.getGlobalStateAsString());
			}
			if (uniform!=parityIni) {
				Macro.fPrint(" $$$$$$$$$$$$ WRONG CLASSIFICATION WITH initconfig:%s ", iniconfig);
				Macro.systemExit(1);
			}
			Macro.print("Converged to " + uniform + " in time : " + time);
		}//for

		// printing output
		// result.Print();
		Macro.print("Results : ");
		for (int i=0; i < NSTATES; i++){
			Macro.fPrintNOCR("[ "  + i + " : " +freq012[i]+ " ] ");
		}
		Macro.CR();

		// stats
		double averageTime= StatsMacro.Mean(convTime);
		double	stdDevT= StatsMacro.StdDev(convTime, averageTime);


		// output
		String rawtimes="";
		for (int i=0; i<Z; i++) {
			rawtimes+= convTime[i] + " ";
		}
		output.Add(rawtimes);
		String firstLine= " T    " + "sdev" + " out " + "Ns";
		String line = String.format("%.1f   %.3f %d %d",	averageTime, stdDevT,freq012[1],Z);
		output.Add(firstLine,line);
	}

	/*
	 * PARSING command line argument
	 */

	final static String ClassName= Thread.currentThread().getStackTrace()[1].getClassName();

	final static String USAGE= "L Z";
	final static int NARG=2;


	/** main : processing command line * */
	public static void main(String[] argv) {

		CommandParser parser =new CommandParser(ClassName,USAGE,argv);
		parser.checkArgNum(NARG); 
		// by default dir is current directory
		int L= parser.IParse("L=");
		int Z= parser.IParse("Z=");


		TestBFOparityExp exp = new TestBFOparityExp(L);
		exp.RunSamples(Z);

		parser.Exit();
	}




}
