package densClassif;

import components.types.FLStringList;
import experiment.measuring.general.DensityMD;
import experiment.measuring.general.UniformStateMDgen;
import experiment.samplers.CAsampler;
import experiment.samplers.LinearSampler;
import initializers.CAinit.BinaryInitializer;
import main.Macro;
import main.StatsMacro;
import models.CAmodels.CellularModel;
import models.CAmodels.binary.MajorityModel;
import models.CAmodels.densClassif.FuksDensityStudyModel;
import models.CAmodels.densClassif.GKL_Model;
import models.CAmodels.densClassif.SchueleDensityStudyModel;
import models.CAmodels.densClassif.TraMajDensityStudyModel;
import topology.basics.LinearTopology;
import topology.basics.TopologyCoder;
import topology.zoo.RadiusN_Topology;
/*-------------------- 
 * experimentation for the density classification problem
 * @author Nazim Fatès
*--------------------*/
public class oldDensityClassifExp {

	final static int MAXTIME=100000;
	private static final int NSTATES = 3; // for stats
	private static final double dini = 0.5;
	private static final int DBEG= 0, DEND = 100, DSTEP=10;
	/*--------------------
	 * attributes
	 --------------------*/

	CAsampler m_sampler;
	private UniformStateMDgen m_MeasuringDevice;
	private DensityMD m_Dens;
	String m_output_filename;
	BinaryInitializer m_init;

	/*--------------------
	 * construction
	 --------------------*/

	enum MODEL {GKL, Fuks, Schuele, Fates}

    /* XYsize is the dimension of the grid */
	public oldDensityClassifExp(String modelName, String in_filename, int L, double alpha) {

		MODEL modelTYPE= MODEL.valueOf(modelName);
		// output
		m_output_filename= in_filename;
		// model
		CellularModel model= null;
		switch(modelTYPE){
		case GKL:
			model = new GKL_Model();
			break;
		case Fuks:
			model = new FuksDensityStudyModel();
			((FuksDensityStudyModel)model).SetpShift(alpha);
			break;
		case Schuele:
			model = new SchueleDensityStudyModel();
			((SchueleDensityStudyModel)model).SetEps(alpha);
			break;
		case Fates:
			model= new TraMajDensityStudyModel();
			((TraMajDensityStudyModel)model).SetEps(alpha);
			break;
		}


		// topo 
		String code = model.GetDefaultAssociatedTopology();
		LinearTopology topology = (LinearTopology)TopologyCoder.GetTopologyFromCode(code);
		// sampler
		m_sampler= new LinearSampler(L, model, topology);
		// measures		
		m_MeasuringDevice= new UniformStateMDgen();
		m_MeasuringDevice.LinkTo(m_sampler);
		m_Dens= new DensityMD();
		m_Dens.LinkTo(m_sampler);
		// init
		m_init = new BinaryInitializer();
		m_sampler.SetInitDeviceFromOutside(m_init);

	}


	
	/*--------------------
	 * main
	 --------------------*/


	public void RunSamples(int Z){
		FLStringList output= new FLStringList();
		for (int dini= DBEG; dini <= DEND; dini += DSTEP){
			int newZ= Z;
			if ((dini==0) || (dini ==100)){
				newZ=1;
			} else if (dini==50){
				newZ = 10* Z;
			}
			String line = RunSamplesFixedDensity(newZ, dini);
			output.Add(line);
		}
		output.WriteToFile(m_output_filename);
	}

	public String RunSamplesFixedDensity(int Z, int dini){

		//StringList result= new StringList();

		double rdensity= (double)dini / 100.;
		m_init.SetInitRate(rdensity);

		int goodclassif =0;
		int  [] freq012 = new int [NSTATES];

		int [] convTime = new int [Z];
		for (int sample=0; sample < Z; sample++){			

			m_sampler.sig_Init();
			double initdens = m_Dens.GetMeasure();
			//Macro.print(" Init dens: " + initdens);
			if (initdens==0.5){
				Macro.FatalError("Equal initial repartition - set L to odd value");
			}
			int initmajstate=(initdens < 0.5) ?  0 : 1;
			//Macro.print(" sample : " + (sample+1) + "  idens: " + initdens);

			int uniform = m_MeasuringDevice.IsUniformState();
			int time=0;
			while ((uniform < 0 ) && (time < MAXTIME)){
				m_sampler.sig_NextStep();
				time++;
				double dens= m_Dens.GetMeasure();
				//Macro.print(" dens: " + dens);
				uniform = m_MeasuringDevice.IsUniformState();
				//Macro.print("uniform " + uniorm + " dens :" + dens);
			}
			String res;
			if (uniform == initmajstate){
				goodclassif++;
				res= "++" + time;
			} else{
				res = "--" + time;
			}
			// stats
			int statindex = (uniform<0) ? 2 : uniform;
			freq012[statindex]++;
			convTime[sample]= time;
		}//for

		// printing output
		// result.Print();
		for (int i=0; i < NSTATES; i++){
			Macro.print("Res : " + i + " Freq : " +freq012[i]);
		}
		Macro.print("good classif :" + goodclassif + "/"+ Z );

		double averageTime= StatsMacro.Mean(convTime),
		stdDev= StatsMacro.StdDev(convTime, averageTime);
		float success= goodclassif/(float)Z;
		String line = rdensity + " " + success + " " + freq012[2] + " " + Z + " " + averageTime + " " + stdDev;
		return line;

	}

	public void SetInitialMeasure(double dini) {
		m_init.SetInitRate(dini);
	}
	
	public double OneMajRun(){
		m_sampler.sig_Init();
		m_sampler.NstepsAndUpdate(50);
		return m_Dens.GetMeasure();
	}

	public void RunMajority(int L, int Z){
		FLStringList output= new FLStringList();
		LinearTopology topology = new RadiusN_Topology(1);
		MajorityModel model= new MajorityModel();
		m_sampler= new LinearSampler(L, model, topology);
		m_Dens= new DensityMD();
		m_Dens.LinkTo(m_sampler);
		m_init = new BinaryInitializer();
		m_sampler.SetInitDeviceFromOutside(m_init);

		for (int d=10; d<=90; d+=10 ){
			double initdens=  (d/100.); 
			m_init.SetInitRate(initdens);
			double [] expect = new double [Z]; 
			for (int run=0; run < Z; run++){
				double dinf = OneMajRun();
				expect[run] = Math.max(dinf, 1 - dinf);
			}
			double average=  StatsMacro.Mean(expect);

			String line=initdens + " " + average;  
			output.Add(line);
		}

		output.WriteToFile("majorityIO.dat");


	}
	


}
