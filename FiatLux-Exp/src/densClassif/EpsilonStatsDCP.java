package densClassif;

import architecture.interactingParticleSystems.IPSsampler;
import components.types.FLStringList;
import experiment.samplers.PlanarSampler;
import initializers.CAinit.BinaryInitializer;
import initializers.IPSinit.BernoulliIPSinit;
import main.Macro;
import main.commands.CommandParser;

/*-------------------- 
 * experimentation for the density classification problem
 * OLD ? 2D IPS ???
 * @author Nazim Fatès
*--------------------*/
public class EpsilonStatsDCP extends DCPexpMachinery {

	final static int MAXTIME = 5000;

	final static double [] EPSVAL = {0.1, 0.05, 0.02, 0.01, 0.005};

	IPSsampler m_ips; // for debug

	/*--------------------
	 * construction
	 --------------------*/

	/* LxL is the dimension of the grid */
	public EpsilonStatsDCP(String modelName, int L) {
		// model: will be used by all samplers (danger ?)
		m_modelTYPE= MODEL.valueOf(modelName);

		PrepareSampler(L);
		PrepareInitializer();
	}


	void PrepareInitializer() {
		// initializer
		switch (m_modelTYPE){
		case IPS4:
		case IPS8:
			//TODO m_init= ((IPSsampler)m_sampler).GetInitializer();
			((BernoulliIPSinit)m_init).SetInitDens(0.5);
			break;
		default:
			Macro.print("Default Init from CAsampler");
			PlanarSampler samp=(PlanarSampler)m_sampler;
			m_init= samp.GetInitializer();
			((BinaryInitializer)m_init).SetInitRate(0.5);
			break;
		}
	}

	/*--------------------
	 * implementations
	 --------------------*/


	private static String GetFileName(String modelName, int L) {
		return 
		"stat-" + modelName + "-L-" + L + 
		".dat";
	}

	@Override
	int GetMaxTime() {
		return MAXTIME;		
	}

	/*
	 * PARSING command line argument
	 */
	final static String ClassName= Thread.currentThread().getStackTrace()[1].getClassName();
	final static String USAGE= "MODEL L Z";
	final static int NARG=3;

	private static FLStringList MakeExperiment(EpsilonStatsDCP exp, int Z) {
		FLStringList expResults= new FLStringList();
		for (double eps : EPSVAL){
			exp.SetModelEps(eps);
			ExpOutputDCP oneResult= exp.RunSamples(Z);
			expResults.Add( eps + "" + oneResult.m_result );
		}
		return expResults;	

	}

	/*private void PrintCurrentState() {
		//double dens= m_Dens.GetMeasure();
		//Macro.print(" dens: " + dens);
		//Macro.Debug(" t: " + m_sampler.GetTime() + " s:" + m_ips.GetCurrentState());
	}*/

	
	/** MAIN : processing command line * */
	public static void main(String[] argv) {

		// init
		CommandParser parser= new CommandParser(ClassName,USAGE,argv);
		//parser.checkArgNum(NARG); 

		// parsing
		String modelName=  parser.SParse("MODEL="); // should be 
		int L= parser.IParse("L=");
		int Z= parser.IParse("Z=");

		// info
		String s = " Model: " + modelName ;
		Macro.print(1, ">*> Experiment with param: " + s);
		// exp
		EpsilonStatsDCP exp = new EpsilonStatsDCP(modelName, L);
		FLStringList output= MakeExperiment(exp, Z);
		String filename= GetFileName(modelName, L);
		output.WriteToFile(filename);
		parser.Exit();
	}
	
	
}
