package densClassif;

import java.math.BigDecimal;
import java.util.ArrayList;

import components.types.FLStringList;
import experiment.measuring.general.DensityMD;
import experiment.measuring.general.UniformStateMDgen;
import experiment.samplers.CAsampler;
import experiment.samplers.LinearSampler;
import initializers.CAinit.BinaryInitializer;
import main.Macro;
import main.StatsMacro;
import main.commands.CommandParser;
import models.CAmodels.binary.BinaryModel;
import models.CAmodels.binary.MajorityModel;
import models.CAmodels.densClassif.FuksDensityStudyModel;
import models.CAmodels.densClassif.GKL_Model;
import models.CAmodels.densClassif.SchueleDensityStudyModel;
import models.CAmodels.densClassif.TraMajDensityStudyModel;
import topology.basics.LinearTopology;
import topology.basics.TopologyCoder;
import topology.zoo.RadiusN_Topology;
/*-------------------- 
 * experimentation for the density classification problem
 * @author Nazim Fatès
*--------------------*/
public class DensityClassifOneDExp {

	final static int MAXTIME=100000;
	private static final int NOUTCOMES = 3; // for stats
//	private static final int DBEG= 0, DEND = 100, DSTEP=10;
	/*--------------------
	 * attributes
	 --------------------*/
	private static final boolean VERBOSE = false;

	private CAsampler m_sampler;
	private BinaryModel m_model;
	private BinaryInitializer m_init;

	private UniformStateMDgen m_uniformityMD;
	private DensityMD m_densMD;
	
	private MODEL m_modelTYPE;
	private String m_output_filename;

	/*--------------------
	 * construction
	 --------------------*/

	enum MODEL {GKL, Fuks, Schuele, TraMaj}


    /* XYsize is the dimension of the grid */
	public DensityClassifOneDExp(String modelName, String in_filename, int L) {

		m_modelTYPE= MODEL.valueOf(modelName);
		// output
		m_output_filename= in_filename;
		// model
		switch(m_modelTYPE){
		case GKL:
			m_model = new GKL_Model();
			break;
		case Fuks:
			m_model = new FuksDensityStudyModel();
			break;
		case Schuele:
			m_model = new SchueleDensityStudyModel();
			break;
		case TraMaj:
			m_model= new TraMajDensityStudyModel();
			break;
		}
		//TODO SetAlpha(double alpha);


		// topo 
		String code = m_model.GetDefaultAssociatedTopology();
		LinearTopology topology = (LinearTopology)TopologyCoder.GetTopologyFromCode(code);
		// sampler
		m_sampler= new LinearSampler(L, m_model, topology);
		// measures		
		m_uniformityMD= new UniformStateMDgen();
		m_uniformityMD.LinkTo(m_sampler);
		m_densMD= new DensityMD();
		m_densMD.LinkTo(m_sampler);
		// init
		m_init = new BinaryInitializer();
		double rdensity= 0.5;
		m_init.SetInitRate(rdensity);

		m_sampler.SetInitDeviceFromOutside(m_init);

	}


	
	/*--------------------
	 * main
	 --------------------*/
	private void SetAlpha(double alpha){
		switch(m_modelTYPE){
		case GKL:
			break;
		case Fuks:
			((FuksDensityStudyModel)m_model).SetpShift(alpha);
			break;
		case Schuele:
			((SchueleDensityStudyModel)m_model).SetEps(alpha);
			break;
		case TraMaj:
			((TraMajDensityStudyModel)m_model).SetEps(alpha);
			break;
		}
	}


	public void RunSamples(int Z, ArrayList<BigDecimal> alphaList){
		FLStringList output= new FLStringList();
		for (BigDecimal alpha: alphaList){
			String line = MakeZsamplesForAlphaVal(alpha, Z);
			output.Add(line);
		}
		output.WriteToFile(m_output_filename);
	}

	/** one experiment for various alpha values **/
	public String MakeZsamplesForAlphaVal(BigDecimal alpha, int Z){
		
		double alphaVal= alpha.doubleValue();
		SetAlpha(alphaVal);
		
		int goodclassif =0;
		int  [] freq012 = new int [NOUTCOMES];

		int [] convTime = new int [Z];
		for (int sample=0; sample < Z; sample++){			
			Macro.ProgressBar(sample,Z); // 
			m_sampler.sig_Init();
			double initdens = m_densMD.GetMeasure();
			if (VERBOSE)
				Macro.print(" Init dens: " + initdens);
			if (initdens==0.5){
				Macro.FatalError("Equal initial repartition - set L to odd value");
			}
			int initmajstate=(initdens < 0.5) ?  0 : 1;
			if (VERBOSE)
				Macro.print(" sample : " + (sample+1) + "  idens: " + initdens);

			int uniform = m_uniformityMD.IsUniformState(); // -1 means not uniform
			int time=0;
			while ((uniform < 0 ) && (time < MAXTIME)){
				m_sampler.sig_NextStep();
				time++;
				//double dens= m_Dens.GetMeasure();
				//Macro.print(" dens: " + dens);
				uniform = m_uniformityMD.IsUniformState();
				//Macro.print("uniform " + uniform + " dens :" + dens);
			}
			// counting good classifications
			if (uniform == initmajstate){
				goodclassif++;
			}
			// stats : 0, 1, 2 = out of time
			int statindex = (uniform<0) ? 2 : uniform; 
			freq012[statindex]++;
			convTime[sample]= time;
		}//for

		// printing output
		// result.Print();
		Macro.print("Results : ");
		for (int i=0; i < NOUTCOMES; i++){
			Macro.fPrintNOCR("[ "  + i + " : " +freq012[i]+ " ] ");
		}
		Macro.CR();
		Macro.print("alpha : " + alpha + " good classif :" + goodclassif + "/"+ Z );

		// stats
		float success= goodclassif/(float)Z;
		double averageTime= StatsMacro.Mean(convTime),
		stdDevT= StatsMacro.StdDev(convTime, averageTime);
		
		String firstLine= " alpha " + " good " +  " T " + "sdev" + " out " + "Ns"; 
		// output
		String line = "" 
			 + alpha +  " " + 
			 success + "  " + 
			 averageTime + "  " +
			 stdDevT + "  " +
			 freq012[2] +  "  " +
			 Z  ;
		return line;

	}

	public void SetInitialMeasure(double dini) {
		m_init.SetInitRate(dini);
	}
	
	public double OneMajRun(){
		m_sampler.sig_Init();
		m_sampler.NstepsAndUpdate(50);
		return m_densMD.GetMeasure();
	}

	/** stats on the majority rule for comparison with Schuele model **/
	public void RunMajority(int L, int Z){
		FLStringList output= new FLStringList();
		LinearTopology topology = new RadiusN_Topology(1);
		MajorityModel model= new MajorityModel();
		m_sampler= new LinearSampler(L, model, topology);
		m_densMD= new DensityMD();
		m_densMD.LinkTo(m_sampler);
		m_init = new BinaryInitializer();
		m_sampler.SetInitDeviceFromOutside(m_init);

		for (int d=10; d<=90; d+=10 ){
			double initdens=  (d/100.); 
			m_init.SetInitRate(initdens);
			double [] expect = new double [Z]; 
			for (int run=0; run < Z; run++){
				double dinf = OneMajRun();
				expect[run] = Math.max(dinf, 1 - dinf);
			}
			double average=  StatsMacro.Mean(expect);

			String line=initdens + " " + average;  
			output.Add(line);
		}

		output.WriteToFile("majorityIO.dat");


	}
	
	/*
	 * what is this for ?
	 */
	
	
	final static int PRECISION=6; // 10 ^-6
	
	private static ArrayList<BigDecimal> GetList(BigDecimal alphaMin, BigDecimal alphaMax, BigDecimal alphaStp) {
		ArrayList<BigDecimal> list= new ArrayList<BigDecimal>();
		BigDecimal alphaVal= alphaMin;	
		while (alphaVal.compareTo(alphaMax) <=0 ){
			alphaVal= alphaVal.setScale(PRECISION, BigDecimal.ROUND_HALF_DOWN);
			list.add(alphaVal);
			alphaVal = alphaVal.add(alphaStp);
			//Macro.Debug(" alpha : " + alphaVal );
		}
		
		return list;
	}
	

	
	/*
	 * PARSING command line argument
	 */
	
	final static String ClassName= Thread.currentThread().getStackTrace()[1].getClassName();

	final static String USAGE= "MODEL L Z MIN MAX STP";
	final static int NARG=6;

	
	/** main : processing command line * */
	public static void main(String[] argv) {
		
		CommandParser parser =new CommandParser(ClassName,USAGE,argv);
		parser.checkArgNum(NARG); 
		
		String modelName=  parser.SParse("MODEL=");
		int L= parser.IParse("L=");
		int Z= parser.IParse("Z=");
		BigDecimal min= parser.BIParse("MIN=");
		BigDecimal max= parser.BIParse("MAX=");
		BigDecimal stp= parser.BIParse("STP=");

//		BigDecimal min= new BigDecimal(0.66), max= new BigDecimal(0.80), stp= new BigDecimal(0.02);
		
		String s = " Size : " + L + " Model: " + modelName;
		Macro.print(1, ">*> Doing DensityTime experiment for " + s);

		String fileName= modelName + "-L-" + L + ".dat";
		DensityClassifOneDExp exp = new DensityClassifOneDExp(modelName, fileName, L);
		ArrayList<BigDecimal> alphaList= GetList(min,max,stp);
		exp.RunSamples(Z, alphaList);
		
		parser.Exit();
	}



}
