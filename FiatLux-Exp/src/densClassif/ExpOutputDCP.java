package densClassif;

import components.types.FLStringList;
import main.Macro;

/*** see DCPexpMapchinery **/
public class ExpOutputDCP {

	String m_result, m_prn;
	FLStringList m_samples;
	
	public ExpOutputDCP(FLStringList samples, float success,
			double averageTime, double stdDevT, int outruns) {
		m_samples= samples;
		m_result = 
			String.format( " %.3f %.2f %d", success, averageTime, outruns); 
		m_prn =  String.format( " quality:%.4f  T:%.2f  [std Dev.: %.1f] outruns %d",
				success, averageTime, stdDevT, outruns );	
	}

	public void Print(){		
		Macro.print(m_prn);
	}

	public void WriteToFile(String filename) {
		m_samples.WriteToFile(filename);	
	}
}
