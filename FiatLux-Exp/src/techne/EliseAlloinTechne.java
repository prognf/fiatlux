package techne;

import main.Macro;

import components.randomNumbers.FLRandomGenerator;
import components.randomNumbers.StochasticSource;
import components.types.FLStringList;
import components.types.IntegerList;

public class EliseAlloinTechne extends StochasticSource {

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();

	private static final String FILENAME = "texteEA.txt";

	private static final int T = 300;

	private static final String TXTINIT = 
			//"Les aurores boréales dansent toujours dans le ciel.";
	/*"Quant à la première, " +
	"celle qui suppose que le sage peut savoir toutes choses, " +
	"il est clair que cette supériorité appartient surtout à celui " +
	"qui possède le plus complètement la science générale.";*/
	"L’île, dans sa petitesse, est tellement variée dans ses terrains et " +
	"ses aspects qu’elle offre toutes sortes de sites et " +
	"souffre toutes sortes de cultures. " +
	"On y trouve des champs, des vignes, des bois, des vergers, " +
	"de gras pâturages ombragés de bosquets et bordés d’arbrisseaux de toute espèce, " +
	"dont le bord des eaux entretient la fraîcheur";		

	private static int NALPHA=28;
	int [][][] m_dic= new int[NALPHA][NALPHA][NALPHA];
	String [][] m_occDico= new String [NALPHA][NALPHA];
	StringBuilder m_strCurrent;
	
	private static String removeAccents(String s){
		    s = s.replaceAll("[èéêë]","e");
		    s = s.replaceAll("[ûù]","u");
		    s = s.replaceAll("[ïî]","i");
		    s = s.replaceAll("[àâ]","a");
		    s = s.replaceAll("Ô","o");

		    s = s.replaceAll("[ÈÉÊË]","E");
		    s = s.replaceAll("[ÛÙ]","U");
		    s = s.replaceAll("[ÏÎ]","I");
		    s = s.replaceAll("[ÀÂ]","A");
		    s = s.replaceAll("Ô","O");
		    return s;
	}
	
	
	
	private void BuildReplacementTab() {
		FLStringList text = FLStringList.OpenFile( FILENAME );
		for (String str : text){
			eatStr(str);
		}
		buildDicoOccurrences();
		//text.Print();
	}
	
	
	/** converts string to chararray in lowercase without accents **/
	static private char[] ParseString(String str) {
		String strlowercase= str.toLowerCase();
		String removedA= removeAccents(strlowercase);
		Macro.print(removedA);
		char[] charA = removedA.toCharArray();
		return charA;
	}
	
	private void eatStr(String txt){
		char [] charA= ParseString(txt);
		IntegerList lst= new IntegerList();
		for (int i=1; i<charA.length-1; i++){
			int g = charToIntSpecial(charA[i-1]), 
				c = charToIntSpecial(charA[i]), 
				d = charToIntSpecial(charA[i+1]);
			AddOcurrence(g,c,d);
			lst.addVal(c);
		}
		Macro.fPrint(" str : %s -> IntList : %s", txt, lst.ToString());
	}

	char intToChar(int val){
		if (val==0)
			return '_';
		return (char)(val+(int)'a'-1);
	}
	
	
	void buildDicoOccurrences(){
		
		for (int g=0; g<NALPHA-1;g++){
			for (int d=0; d<NALPHA-1; d++){
				String occurrencesL= "";
				for (int c=0; c<NALPHA-1; c++){
					if (m_dic[g][d][c]>0){
						occurrencesL+= intToChar(c);
					}
				}
				String sg= "" + intToChar(g), sd = "" + intToChar(d);
				m_occDico[g][d]= occurrencesL;
				Macro.fPrint(" (%s.%s): [%s]", sg, sd, occurrencesL);
			}
		}
		
	}
	
	
	
 	private void AddOcurrence(int g, int c, int d) {
		m_dic[g][d][c]++;
	}


	private static int charToIntSpecial(char c) {
		if (c==' '){
			return 27;
		}
		if ((c>='a') && (c<='z')){
			return (int)c - (int)'a' + 1; 
		}
		return 0;
	}

	/** applies one random mutation at position pos **/
	void oneMutation(StringBuilder s, int pos){
		// do not modify a whitespace
		if (s.charAt(pos)==' '){
			return ;
		}
		int 
			ig= charToIntSpecial(s.charAt(pos-1) ),
			id= charToIntSpecial(s.charAt(pos+1) );
		String possibilities= m_occDico[ig][id];
		//Macro.fPrint(" modif at : %d %d  %s", ig, id, possibilities);
		if ( (possibilities!=null) 
				&& (!possibilities.isEmpty()) ){
			int selectedR= RandomInt(possibilities.length());
			char newletter= possibilities.charAt(selectedR);
			s.setCharAt(pos, newletter);
		}
	}
	
	private void oneMutationRandom() {
		int pos= RandomInt(m_strCurrent.length()-2);
		// avoid first and last letter
		oneMutation(m_strCurrent, pos+1);
	}
	
	private void InitString(String txtinit) {
		m_strCurrent= new StringBuilder(txtinit);
	}
	
	public static void main(String[] argv) {
		Macro.BeginTest(CLASSNAME);
		EliseAlloinTechne exp = new EliseAlloinTechne();
		exp.BuildReplacementTab();
		exp.InitString(TXTINIT);
		for (int t=0; t<T; t++){
			Macro.fPrint("%d: %s", t, exp.m_strCurrent);
			exp.oneMutationRandom();
			Macro.CR();
		}
		Macro.fPrint("%s", exp.m_strCurrent);
		//ParseString("ABCDabcd.WwyYzZ.");
		//exp.printDicoState();
		FLRandomGenerator.SetDefaultSeed(1596);
		Macro.EndTest();
	}



	



	



	




	





	
}
