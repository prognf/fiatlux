package techne.retina;

import main.FiatLuxProperties;
import main.Macro;
import main.commands.CommandInterpreter;
import main.commands.CommandParser;
import models.CAmodels.stochastic.RetinaModel;
import topology.basics.SamplerInfoLinear;
import topology.basics.TopologyCoder;
import updatingScheme.AlphaScheme;

import components.randomNumbers.FLRandomGenerator;
import components.types.FLStringList;
import components.types.IntC;
import components.types.IntPar;

import experiment.samplers.CAsimulationSampler;
import grafix.gfxTypes.elements.FLActionButton;
import grafix.gfxTypes.elements.FLFrame;
import grafix.gfxTypes.elements.FLPanel;
import grafix.gfxTypes.imgFormat.ImgFormat;


/*--------------------
 * @author Nazim Fates
 *--------------------*/

public class TissageRetina extends CommandInterpreter {

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();
	
	final static String USAGE="N=size T=time A=noise rate";
	//private static final int Ndeffail = 130, Tdef= 72, 
	private static final int Ndef= FiatLuxProperties.Nsize1D[1];
	private static final int Tdef= FiatLuxProperties.Tsize1D[1];
	private static final int Adef=0;

	private static final int LPIX = 5;
	private static final IntC CellPixSize = new IntC(LPIX,0);


	private static void DiagSeptMilleDeuxCent(int N, int T, int noiseRate) {

		RetinaModel rule = RetinaModel.NewModelFromFile();
		//rule.m_colorMode= RetinaModel.colorMode.FROMFILE;
		String 	topoCode = TopologyCoder.s_TLR1;

		SamplerInfoLinear info = 
				new SamplerInfoLinear(N,T, CellPixSize, rule, topoCode);
		//Macro.Debug(" code :" + info.topoCode);
		CAsimulationSampler sampler= new CAsimulationSampler(info);

		//sampler.SetFullyAsynchronousUpdatingScheme();
		AlphaScheme US= (AlphaScheme) sampler.GetUpdatingScheme();
		double noise = noiseRate / 100.;
		US.SetSynchronyRate(1. - noise);

		//LineAutomatonViewer viewer= new LineAutomatonViewerDefault(CellPixSize, sampler, T);
		//sampler.SetAutomatonViewer(viewer);


		FLStringList recordF= new FLStringList();

		sampler.sig_Init();
		String state= sampler.GetAutomaton().GetStateAsStringTEMP();
		recordF.Add(state);
		//sampler.NstepsAndUpdate(T);
		for (int t=0; t<T-1; t++){
			//Macro.print("t: "+ t + " st: " + state);
			sampler.sig_NextStep();
			state= sampler.GetAutomaton().GetStateAsStringTEMP();
			recordF.Add(state);
		}

		String filename= "retina-" + "asynch-" + noiseRate;
		//sampler.io_SetRecordFormat(ImgFormat.BMP);
		//sampler.io_SaveImage(filename);	
		sampler.io_SetRecordFormat(ImgFormat.PNG);
		sampler.io_SaveImage(filename);	
		//sampler.io_SetRecordFormat(ImgFormat.SVG);
		//sampler.io_SaveImage(filename);	
		recordF.WriteToFile(filename+".dat");

	}



	private static void Inline(String[] argv) {
		FLRandomGenerator.SetDefaultSeed(280676);
		CommandParser parser = 
				new CommandParser(CLASSNAME, USAGE, argv, NARG);
		int N= parser.IParse("N=");
		int T= parser.IParse("T=");
		int A= parser.IParse("A=");
		DiagSeptMilleDeuxCent(N,T,A);
		CommandInterpreter.ByeMessage();		
	}


	private void Kloupa() {
		FLFrame frame = new FLFrame("FiatLux - projet rétin< - M. Barthélémy, N. Fates, F. Jubin, R. Sultra" );
		Kloupi kloupi = new Kloupi();
		FLPanel p= FLPanel.NewPanelVertical(kloupi, frame.GetCloseButton("au revoir"));
		frame.addPanel(p);
		frame.packAndShow();
	}

	class Kloupi extends FLPanel {
		IntPar m_N, m_T, m_A;
		Kloupi(){
			SetBoxLayoutY();
			m_N= new IntPar("nombre de cellules", Ndef);
			m_T= new IntPar("nombre d'étapes", Tdef);
			m_A= new IntPar("ajout d'asynchronisme (en %)", Adef);
			this.Add(m_N.GetControl(), m_T.GetControl(), m_A.GetControl());
			Add(new ButtonGenImage());
		}

		class ButtonGenImage extends FLActionButton {
			public ButtonGenImage() {
				super("tissage d'image");
			}

			@Override
			public void DoAction() {
				Macro.print("tissage de l'image...");
				DiagSeptMilleDeuxCent(m_N.GetVal(),m_T.GetVal(),m_A.GetVal());
			}
		}
	}

	private static final int NARG = 3;

	/** main : processing command line * */
	public static void main(String[] argv) {
		if (argv.length==0){
			TissageRetina exp= new TissageRetina();
			exp.Kloupa();
		} else {
			Inline(argv);
		}
	}



}


