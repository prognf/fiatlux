package techne.sokoban;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import components.types.IntC;
import main.Macro;

/*-------------------------------------------------------------------------------
- 
------------------------------------------------------------------------------*/

public class SokoGestell extends JFrame implements KeyListener {

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();
	private static final int LEFTKEY = 37,UPKEY=38,RIGHTKEY=39,DOWNKEY=40;
	private static final int UNDOKEY = 87; /* W */
	private static final IntC XYsize = new IntC(15,5);
	private static final IntC pixSize = new IntC(40,1);

	public static final char LEFT = 'L', RIGHT='R', UP='U', DOWN='D';
	public static final char UNDO = '<';
	//--------------------------------------------------------------------------
	//- attributes
	//--------------------------------------------------------------------------

	SokoLand m_array;
	private SokoView m_sview;
	static JTextField typingArea;

	//--------------------------------------------------------------------------
	//- construction
	//--------------------------------------------------------------------------

	public SokoGestell(String name) {
		this(name, new SokoLand(XYsize));
	}


	public SokoGestell(String name, SokoLand sland) {
		super(name);
		m_array= sland;
	}
	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------

	public static void main(String[] args) {
		/* Use an appropriate Look and Feel */
		try {
			//UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
			//UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
			UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
		} catch (UnsupportedLookAndFeelException ex) {
			ex.printStackTrace();
		} catch (IllegalAccessException ex) {
			ex.printStackTrace();
		} catch (InstantiationException ex) {
			ex.printStackTrace();
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		}
		/* Turn off metal's use of bold fonts */
		UIManager.put("swing.boldMetal", Boolean.FALSE);

		//Schedule a job for event dispatch thread:
		//creating and showing this application's GUI.
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				SokoGestell sgest= new SokoGestell("Sokosmos");
				sgest.createAndShowGUI();
			}
		});
	}

	/**
	 * Create the GUI and show it.  For thread safety,
	 * this method should be invoked from the
	 * event-dispatching thread.
	 */
	public void createAndShowGUI() {

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		//Set up the content pane.
		//      frame.addComponentsToPane();
		this.addContent();	        

		//Display the window.
		this.pack();
		this.setVisible(true);
	}

	private void addContent() {
		typingArea = new JTextField(20);
		typingArea.addKeyListener(this);
		getContentPane().add(typingArea);		

		m_sview= new SokoView(pixSize, m_array); 
		getContentPane().add(m_sview);		

	}

	/** Handle the key typed event from the text field. */
	public void keyTyped(KeyEvent e) {
		//displayInfo(e, "KEY TYPED: ");
	}

	/** Handle the key pressed event from the text field. */
	public void keyPressed(KeyEvent e) {
		//  displayInfo(e, "KEY PRESSED: ");
		int code=e.getKeyCode();
		char mv= codeTochar(code);
		char res=m_array.produce(mv);
		Macro.fPrint( "prod %s res %s %s", 	""+mv, ""+res, m_array.m_playerPos);
		Macro.fPrint("CHAIN:%s",m_array.m_movesPlayer);
		Macro.fPrint("CHRES:%s",m_array.m_movesType);
		
		m_sview.repaint();
	}



	private char codeTochar(int code) {
		switch(code){
		case LEFTKEY: 	return LEFT;
		case RIGHTKEY: 	return RIGHT; 
		case UPKEY: 	return UP;
		case DOWNKEY: 	return DOWN;
		case UNDOKEY:		return UNDO;
		}
		return 0;
	}

	/** Handle the key released event from the text field. */
	public void keyReleased(KeyEvent e) {
		// displayInfo(e, "KEY RELEASED: ");
	}

	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------


	//--------------------------------------------------------------------------
	//- 
	//--------------------------------------------------------------------------





}
