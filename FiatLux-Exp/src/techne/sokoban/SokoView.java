package techne.sokoban;

import components.arrays.GridSystem;
import components.types.IntC;
import grafix.gfxTypes.FLColor;
import grafix.gfxTypes.PaintToolKit;
import grafix.viewers.GridViewer;

public class SokoView extends GridViewer {

	final static FLColor [] col = {
			FLColor.c_white, FLColor.c_brown, FLColor.c_blue, FLColor.c_lightbrown,
			FLColor.c_red};


	final static FLColor colGoal= FLColor.c_red;
	final static FLColor colSoko= FLColor.c_darkgreen;

	SokoLand m_sokLand;


	public SokoView(IntC pixSize, GridSystem system) {
		super(pixSize, system);
		m_sokLand= (SokoLand)system;
		//SetPalette(PaintToolKit.)
		SetPalette(new PaintToolKit(col));
	}

	@Override
	protected void DrawSystemState() {
		for (int x = 0; x < getXsize(); x++)
			for (int y = 0; y < getYsize(); y++) {
				//int y2= GetYsize() - 1 - y;
				// we reverse  NORTH and SOUTH ! 
				int state = getStateColor(x,y); 
				DrawSquareColor(x, y, state);
			}
		// sokoban
		SetColor(colSoko);
		DrawCellCircle(m_sokLand.GetPlayerPos());
		//goals
		SetColor(colGoal);
		for (int x = 0; x < getXsize(); x++)
			for (int y = 0; y < getYsize(); y++) {
				// we reverse  NORTH and SOUTH ! 
				//int y2= GetYsize() - 1 - y;
				if (m_sokLand.isGoalC(x,y)){ 
					DrawCellCircle(x, y, 2.F/5.F);
				}
			}
	}


	/** from state to color : plots state in (x,y) **/
	public int getStateColor(int x, int y) {
		char st= m_sokLand.getState(x,y);
		switch (st){
		case 0 : return 0;
		case SokoLand.WALL: return 1;
		case SokoLand.BOX1: 
			if ( m_sokLand.isGoalC(x, y) ) {
				return 3; // box on goal
			} else {
				return 2; // free box
			}

		case SokoLand.BOX2:
			return 4;
		default:
			return -1;
		}
	}
	
	
}//end class
