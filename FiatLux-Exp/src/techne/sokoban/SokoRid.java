package techne.sokoban;

import java.io.IOException;

import components.types.FLStringList;
import components.types.IntC;
import main.Macro;
import main.commands.CommandParser;

/** TODO: differentiate "classic" and "modern" sokoban **/
public class SokoRid {

	final static String CLASSNAME= new Macro.CurrentClassGetter().getClassName();
	final static String USAGE="...";
	private static final int NARG = 1;


	/**
	 * @param args
	 */
	public static void main(String[] argv) {
		CommandParser parser = 
				new CommandParser(CLASSNAME, USAGE, argv, NARG);
		String filename=parser.SParse("F=");
		FLStringList flrid=null;
		try {
			flrid= FLStringList.ReadFile(filename);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		SokoLand sokoland= AnalyseStruct(flrid);
		SokoGestell g= new SokoGestell("puzzle:"+filename, sokoland);
		g.createAndShowGUI();
		//flrid.Print();

	}


	private static SokoLand AnalyseStruct(FLStringList sokfile) {
		int sz= sokfile.GetSize(); // number of lines to read
		boolean flag= true; // reading again or stop ?
		int lenMax=0;// maximum len of reading
		int height=0;
		int lin=0;
		FLStringList sokdata= new FLStringList();//filtering data
		while(flag){ 
			String strL= sokfile.Get(lin);
			if (strL.startsWith(";")){
				Macro.fPrint(" file finishes with:%s" , strL);
				flag=false;
			} else {
				if (strL.contains("#")) {//all lines should contain two wall symbols '#'
					//updates lenmax
					lenMax= (strL.length()>lenMax)?strL.length():lenMax;
					height++;
					Macro.fPrint(" line:%2d [%s] len:%d", height, strL, strL.length());
					sokdata.Add(strL);
				} else {
					
				}
				lin++;
			}

			flag= flag && (lin<sz);
		}//while
		Macro.fPrint("nline:%d lenmax:%d",height,lenMax);

		SokoLand sokoland= ReadData(sokdata,height,lenMax);
		return sokoland;
	}

	/** reading initial condition of the puzzle
	 * lenL : width ; sz : height **/
	private static SokoLand ReadData(FLStringList sokfile, int sz, int lenL) {
		//creating the array
		SokoLand sokoland= new SokoLand(new IntC(lenL,sz));
		//seetting the initial condition line by line
		for (int lin=0; lin<sz;lin++){
			String line= sokfile.Get(lin);
			initLandLine(sokoland,lin,line,lenL);
		}
		return sokoland;
	}

	/** converts classical sokoban input to our format **/
	private static void initLandLine(SokoLand sokoland, int lin, String line, int lenL) {
		char [] data= new char[lenL];
		for (int pos=0; pos<lenL;pos++){
			char outC=SokoLand.VOID;
			if ( pos<line.length()){// reading inside line
				switch(line.charAt(pos)){
				//wall
				case '#':outC= SokoLand.WALL; break;
				//player
				case '@':sokoland.setInitPlayerPos(pos,lin);break;
				//player on goal square
				case '+':
					sokoland.setInitPlayerPos(pos,lin);  
					sokoland.setInitGoalC(pos,lin); break;
				//box
				case '$':outC= SokoLand.BOX1; break;
				//box on goal square
				case '*':
					outC= SokoLand.BOX1; 
					sokoland.setInitGoalC(pos,lin); break;
				//goal square
				case '.':
					sokoland.setInitGoalC(pos,lin); break;
				}
			}
			data[pos]= outC;
		}
		sokoland.initLandLine(lin, data);
	}

}
