package techne.sokoban;


import components.arrays.GridSystem;
import components.types.IntC;
import main.Macro;

public class SokoLand extends GridSystem {

	public static final char WALL = 'W', VOID=0;
	public static final char BOX1= 'A', BOX2='B', BOX3='C', BOX4='D';
	
	// reactions of the system
	private static final char OUT = 'O';
	private static final char 
	MOVEACCEPTED = '.', PUSHACCEPTED= 'P', BOUNCE='!', MOVEUNDO= '<', UNDOFINISHED='|';
	private static final char EMPTY = 0;
	private static final int GOALVAL1 = 1;
	
	//directions
	private final IntC 
	DLEFT= new IntC(-1,0), DRIGHT= new IntC(1,0), DUP= new IntC(0,1), DDOWN= new IntC(0,-1);

	/*------- ATTRIBUTES */
	int [][] m_goal; // does not move : 2D array
	char [][] m_cell;    // what the cell contains (empty, wall, box...)
	IntC m_playerPos;    // player pos
	StringBuilder m_movesPlayer;// chain of moves
	StringBuilder m_movesType;// chain of movesTypes

	public SokoLand(IntC XYsize) {
		super(XYsize);

		m_goal = new int[m_XYsize.X()][m_XYsize.Y()];
		m_cell = new char[m_XYsize.X()][m_XYsize.Y()];
		m_playerPos= new IntC(1,1); // init pos

		m_movesPlayer= new StringBuilder();
		m_movesType= new StringBuilder();

		setCell(3,1, WALL);
		setCell(5,1, BOX1);
		setCell(5,3, BOX2);
	}


	/** transferring a box from nxy to wxy 
	 * safe with regad to references **/
	private void MoveBox(IntC oldpos, IntC newpos) {
		//Macro.fDebug("Moving box %s -> %s", oldpos, newpos);
		char boxst= getState(oldpos);
		setCell(oldpos, EMPTY);
		setCell(newpos, boxst);
	}


	private char Status(IntC nxy) {
		if ( !nxy.inBounds(m_XYsize)) 
			return OUT; 
		else 
			return getState(nxy);
	}


	public void prnState() {
		String st= String.format(" %s ", m_playerPos);
		Macro.print(" > " + st);		
	}

	public IntC GetPlayerPos() {
		return m_playerPos;
	}


	public char produce(char cmd) {
		if (cmd==SokoGestell.UNDO) {	
			return undo();
		} else {
			IntC dxy=dir(cmd);
			char resprod= tryMove(dxy);
			if (resprod ==MOVEACCEPTED || resprod ==PUSHACCEPTED) {
				m_movesPlayer.append(cmd);
				m_movesType.append(resprod);
			}
			return resprod;	
		}
	}

	/** returns if move is accepted or bounces */
	private char tryMove(IntC dxy) {
		IntC nxy = IntC.AddNew(m_playerPos, dxy);
		IntC wxy= IntC.AddNew(nxy, dxy);
		char next= Status(nxy);
		char welcoming= Status(wxy);
		boolean free= (next==0);
		if (free){ // free move
			m_playerPos.Add(dxy);
			return MOVEACCEPTED;
		} else {
			if (isBoxType(next)){ // pushing
				if (welcoming==0){
					MoveBox(nxy,wxy);
					m_playerPos.Add(dxy);
					return PUSHACCEPTED;
				}
			}
		}
		return BOUNCE;
	}

	private boolean isBoxType(char next) {
		return (BOX1<=next) && (next<=BOX2);
	}


	private char undo() {
		int lastindex= m_movesPlayer.length()-1;
		if (lastindex >= 0) {
			char lastmove= m_movesPlayer.charAt(lastindex); // last index
			IntC dxy= dir(lastmove), invdxy= revertdir(lastmove);
			// box move (before)
			if (m_movesType.charAt(lastindex)==PUSHACCEPTED) {
				IntC boxpos= IntC.AddNew(m_playerPos, dxy);
				MoveBox(boxpos, m_playerPos);
			}
			// player move (after)
			displacePlayer(invdxy);
			// delete from movelists
			m_movesPlayer.deleteCharAt(lastindex);
			m_movesType.deleteCharAt(lastindex);
			return MOVEUNDO;
		} else {
			return UNDOFINISHED;
		}
	}

	private void revertpushbox(char lastmove) {
		// TODO Auto-generated method stub
		
	}


	private void displacePlayer(IntC deltaPos) {
		m_playerPos.Add(deltaPos);
	}


	private char getState(IntC xy) {
		return getState(xy.X(), xy.Y());
	}

	private void setCell(int i, int j, char state) {
		m_cell[i][j]= state;
	}

	private void setCell(IntC xy, char state) {
		setCell(xy.X(), xy.Y(), state);
	}


	char getState(int x, int y){
		return m_cell[x][y];
	}


	public void initLandLine(int i, char[] data) {
		int line = m_XYsize.Y() - 1 - i; // reverting Y : line 0 is at the BOTTOM
		for (int x=0; x<m_XYsize.X();x++) {
			setCell(x, line, data[x]);
		}

	}

	public void setInitPlayerPos(int pos, int lin) {
		m_playerPos= new IntC(pos,lin);		
	}

	/** general : init goal  */
	public void setInitGoal(int pos, int lin, int goalVal) {
		m_goal[pos][lin]= goalVal;
	}
	
	/** classical : init goal  */
	public void setInitGoalC(int pos, int lin) {
		m_goal[pos][lin]= GOALVAL1;
	}

	public int getGoalVal(int x, int y) {
		return m_goal[x][y];
	}
	
	/** classical : is cell (x,y) a goal */ 
	public boolean isGoalC(int x, int y) {
		return m_goal[x][y]==GOALVAL1;
	}

	private IntC dir(char cmd) {
		switch (cmd){
		case SokoGestell.LEFT: return DLEFT;
		case SokoGestell.RIGHT: return DRIGHT;
		case SokoGestell.UP: return DUP; 
		case SokoGestell.DOWN: return  DDOWN; 
		}
		return null;
	}
	
	private IntC revertdir(char lastmove) {
		switch (lastmove){
		case SokoGestell.LEFT: return DRIGHT; 
		case SokoGestell.RIGHT: return DLEFT;
		case SokoGestell.UP: return DDOWN;
		case SokoGestell.DOWN: return DUP;
		}
		return null;
	}

}
